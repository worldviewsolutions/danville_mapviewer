﻿define([
    "dojo/_base/declare"
], function(declare){
        return declare(null, {
	        field: '',
	        _aVal: -1,
	        _bVal: 1,
	
	        compareField: function(a, b){
		
		        a = a.attributes[this.field];
		        b = b.attributes[this.field];
		
		        if(a == null) return this._aVal;
		        if(b == null) return this._bVal;
			
		        if(typeof a != 'string'){
			        if (a < b) {return this._aVal}
			        if (a > b) {return this._bVal}
		        }else{
			        a = a != null ? a.toLowerCase() : '';
			        b = b != null ? b.toLowerCase() : '';
			        if (a < b) {return this._aVal}
			        if (a > b) {return this._bVal}
		        }
		        return 0;
	        },
	
	        sort: function(features, field, direction){
		        this.field = field;
	
		        if(direction == 'asc'){
			        this._aVal = -1;
			        this._bVal = 1;
		        }else{
			        this._aVal = 1;
			        this._bVal = -1;
		        }
		

		        features.sort(createDelegate(this, this.compareField));		
	        }
        });
});