console.log("wvs/dijits/TocGraphicsLayerNode.js");

define([
    "dojo/_base/declare"
    , "dojo/text!./templates/_TocNode.html"
    , "dojo/_base/lang"
    , "./_TocNode"
    , "./TocGraphicsNode"
    , "dijit/form/HorizontalSlider"
    , "dojo/dom-style"
    , "dojo/dom-construct"
    , "dojo/dom-attr"
    , "dojo/on"
    , "dijit/Menu"
    , "dijit/MenuItem"
    , "dijit/MenuSeparator"
    , "dijit/PopupMenuItem"
    , "dijit/TooltipDialog"
    , "dojo/_base/array"
    , "dojo/dom-class"
    , "dojo/has"
    , "dojo/dom"
    , "esri/geometry/Polygon"
    , "esri/geometry/Polyline"
    , "esri/geometry/Point"
    , "esri/graphicsUtils"
    , "../extensions/esri/graphicsUtils"
    , "dojo/_base/sniff"
], function (declare, template, lang, _TocNode, TocGraphicsNode, HorizontalSlider, domStyle, domConstruct, domAttr, on, Menu, MenuItem, MenuSeparator, PopupMenuItem, TooltipDialog, array, domClass, has, dom, Polygon, Polyline, Point, graphicsUtils) {
    return declare('', [_TocNode], {

        templateString: template,

        // refreshDelay: Integer
        //      Number of milliseconds to delay between clicking a checkbox and refreshing the map service. A longer delay allow the user to check/uncheck multiple
        //      layers before refreshing the map service."dojo/ready",
        refreshDelay: 500,

        // useSpriteImage: Boolean
        //      Use the custom sprite image for the legend icons. When false, the "Legend" REST service will be used for 10.01 and newer services.
        useSpriteImage: true,

        declaredClass: "wvs.dijits.TocGraphicsLayerNode",

        hasDropDown: true,

        constructor: function (params, srcNodeRef) {
            console.log(this.declaredClass + " constructor");
            params = params || {};
            if (!params.mapService) {
                throw new Error('mapService not defined in params for TocGraphicsLayerNode');
            }
            this.title = this.mapService.getServiceName();

            lang.mixin(this, params);
        },
        // extension point called by framework
        postCreate: function () {
            console.log(this.declaredClass + " postCreate");

            // TODO: Enabled Legend to work with feature layers
            //this.mapService.getLegend(lang.hitch(this, this._createChildNodes), this.useSpriteImage);
            this._createChildNodes();
            
            // Initialize layer defaults to tie to the TOC node (i.e. opacity)
            this.mapService.opacity = this.mapService.opacity ? this.mapService.opacity : 1;
            var transparency = (1 - this.mapService.opacity).toPrecision(1);
            this.set("transparency", transparency);

            // Set up events
            on(this.mapService, "graphic-add", lang.hitch(this, function (evt) {
                var node = new TocGraphicsNode({
                    _parent: this,
                    graphic: evt.graphic,
                    map: this.map
                });
                node.startup();
                node.placeAt(this.childrenNode);
                this.nodes.push(node);
            }));



            on(this.mapService, "graphic-remove", lang.hitch(this, function(evt){
                var graphic = evt.graphic;
                var removeIndex = -1;
                array.forEach(this.nodes, function (node, index) {
                    if (node.graphic == graphic) {
                        removeIndex = index;
                    }
                });

                this.nodes[removeIndex].destroy();
                this.nodes.splice(removeIndex, 1);
            }));

            this._createContextMenu();

            this.inherited(arguments);
        },
        setVisible: function (visible) {
            // return if the visibility has not changed
            if (this._state.visible == visible) return;

            this._state.visible = visible;

            // update the map service
            if (visible) {
                this.mapService.show();
                this._enableChildNodes();
                this._showSwatch();
            } else {
                this.mapService.hide();
                this._disableChildNodes();
                this._hideSwatch();
            }
        },
        _checkboxOnClick: function (e) {
            this.setVisible(this._checkbox && this._checkbox.checked);
        },
        _createChildNodes: function () {
            console.log(this.declaredClass + " _createChildNodes");
            array.forEach(this.mapService.graphics, function (graphic) {
                var node = new TocGraphicsNode({
                    _parent: this,
                    graphic: graphic,
                    map: this.map
                });
                node.startup();
                node.placeAt(this.childrenNode);
                this.nodes.push(node);
            }, this);

            this._updateVerticalAlignment();
        },
        _refreshLayer: function () {

            if (this._refreshTimer) {
                window.clearTimeout(this._refreshTimer);
                this._refreshTimer = null;
            }

            var mapService = this.mapService;

            this._refreshTimer = window.setTimeout(function () {
                mapService.refresh();
            }, this.refreshDelay);
        },
        zoomToNode: function (node) {
            var geometry = node.graphic.geometry;
            if (geometry.isInstanceOf(Polygon) || geometry.isInstanceOf(Polyline)) {
                this.map.setExtent(geometry.getExtent(), true);
            }
            else if (geometry.isInstanceOf(Point)) {
                this.map.setExtent(graphicsUtils.pointToExtent(geometry));
            }
            else {
                throw new Error("unsupported geometry type detected for " + this.declaredClass);
            }
        },
        _createContextMenu: function () {
            var self = this;

            var mapServiceOptionsMenu = new Menu({
                leftClickToOpen: true
            });

            // NOTE: esri doesn't support changing the opacity of feature layers in IE8 and below. Stoopid.
            var transparencyDialog = new TooltipDialog({
                content: "<label><span id='GraphicsLayerNode_transparencyLabel_" + this.dijitIndex + "'>" + this.transparency + "</span>% transparent</label>"
            });
            var transparencySlider = new HorizontalSlider({
                name: "transparencySlider",
                value: self.transparency,
                minimum: 0,
                maximum: 1,
                showButtons: true,
                style: "width:300px",
                onChange: lang.hitch(self, self._onTransparencySliderChange)
            });
            var transparencyMenuItem = new PopupMenuItem({
                label: "Transparency",
                popup: transparencyDialog,
                disabled: has("ie") <= 8,
                title: has("ie") <= 8 ? "Transparency for feature layers not supported in Internet Explorer 8 and below" : "Change the transparency of this feature layer",
                iconClass: "jstoolkit-icon jstoolkit-icon-transparency"
            });
            transparencyDialog.addChild(transparencySlider);
            mapServiceOptionsMenu.addChild(transparencyMenuItem);

            mapServiceOptionsMenu.addChild(new MenuSeparator());


            mapServiceOptionsMenu.addChild(new MenuItem({
                label: "Remove",
                iconClass: "jstoolkit-icon jstoolkit-icon-delete",
                title: "Remove this layer from the map",
                onClick: lang.hitch(self._parent, self._parent._removeServiceLayer, self.mapService)
            }));
            mapServiceOptionsMenu.bindDomNode(this.dropDownMenuIcon);
            mapServiceOptionsMenu.startup();
            this.mapServiceOptionsMenu = mapServiceOptionsMenu;
        },
        // Event Handlers

        /*
        *  Changes the transparency of the associated map layer
        *  @author: Eddie Stathopoulos
        */
        _onTransparencySliderChange: function (value) {
            console.log(this.declaredClass + " _onTransparencySliderChange");
            var opacity = 1 - value;
            this.mapService.setOpacity(opacity);
            dom.byId("GraphicsLayerNode_transparencyLabel_" + this.dijitIndex).innerHTML = "" + (Math.round(value * 10000) / 100);
        }

    });
});