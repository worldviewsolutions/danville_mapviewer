console.log("wvs/dijits/_TocNode.js");

define([
    "dojo/_base/declare"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dojo/text!./templates/_TocNode.html"
    , "dojo/_base/lang"
    , "dojo/on"
    , "dojo/_base/array"
    , "dojo/fx/Toggler"
    , "dojo/Evented"
    , "dojo/_base/event"
    , "../_base/errors"
    , "dojo/dom-style"
    , "dojo/fx"
    , "dojo/dom-class"
    , "dojo/dom-construct"
    , "dijit/MenuItem"
    , "dijit/PopupMenuItem"
    , "dijit/TooltipDialog"
    , "dijit/form/HorizontalSlider"
    , "dojo/has"
], function (declare
    , _WidgetBase
    , _TemplatedMixin
    , template
    , lang
    , on
    , array
    , Toggler
    , Evented
    , event
    , errors
    , domStyle
    , coreFx
    , domClass
    , domConstruct
    , MenuItem
    , PopupMenuItem
    , TooltipDialog
    , HorizontalSlider
    , has) {
    var _TocNode = declare([_WidgetBase, _TemplatedMixin, Evented], {

        templateString: template,

        declaredClass: 'wvs.dijits._TocNode',

        // isFromWebMap: Boolean
        //      states whether or not this service is part of a web map
        isFromWebMap: false,

        // hasDropDown: Boolean
        //      signifies whether or not this node has a dropdown menu/icon
        hasDropDown: false,
        // allowVisibilityChange: Boolean
        //      When true, a checkbox will be displayed that allows the user to change the visibility of the node (layer) and all of it's child nodes. Default is true.
        allowVisibilityChange: true,
        // maxScale: Number
        //      The maximum scale (highest resolution) that the layer is visible. This property is disregarded if a resource and layer has been assigned to the node.
        maxScale: Number.NEGATIVE_INFINITY,
        // minScale: Number
        //      The minimum scale (lowest resolution) that the layer is visible. This property is disregarded if a resource and layer has been assigned to the node.
        minScale: Number.POSITIVE_INFINITY,
        // title: String
        //      Text to display in the TOC for this node
        title: null,
        // isGroupNode: Boolean
        //      does this node signify a group node?
        isGroupNode: false,

        getToc: function () {
            if (this._parent == null) {
                return this;
            }

            if (this._parent && this._parent.declaredClass == "wvs.dijits.Toc") {
                return this._parent;
            }

            //move up the tree
            return this._parent.getToc();
        },

        constructor: function (params, srcNodeRef) {

            // Object Instance Variables
            // Primitive values may be declared as above, but since objects are passed by reference, anything declared above will be shared with any instance of this class.
            var nodeDefaults = {
                // nodes: _TocNode[]
                //      the array of children nodes
                nodes: [],
                _checkbox: null,
                // _parent: Object
                //      either the TOC or another TOCNode
                _parent: null,
                // _state: Object
                //      Used to preserve state information of the node
                _state: {
                    enabled: true,
                    visible: true
                },
                mapService: null,
                layer: null
            };

            lang.mixin(this, nodeDefaults);
            lang.mixin(this, params);

            this.domNode = srcNodeRef;
            this._state.visible = this.mapService && typeof this.mapService.visible === "boolean" ? this.mapService.visible : true;
        },

        postCreate: function () {
            if (!this.allowVisibilityChange) {
                domStyle.set(this.checkboxNode, "visibility", "hidden");
            } else {
                this._createCheckBox();
            }

            if (this.childrenNode) {
                this.toggler = new Toggler({
                    node: this.childrenNode,
                    showFunc: coreFx.wipeIn,
                    hideFunc: coreFx.wipeOut
                });
            }
            // If this layer doesn't have a drop-down menu, hide the icon
            if (this.hasDropDown) {
                domClass.remove(this.dropDownMenuIcon, "jstoolkit-hidden");
            }
            if (this.isGroupNode) {
                domClass.remove(this.expandoNode, "jstoolkit-hidden");
                domClass.add(this.labelNode, "group-title");
            }
            if (!this._state.visible) {
                this._disableChildNodes();
            }

            this._adjustToMapScale();
        },
        getVisible: function () {
            // summary:
            //      true if the the node is checked and it's parent(s) are visible
            var vis = (this._state.visible && this._state.enabled);
            if (this._parent) {
                vis = (this._parent.getVisible() && vis);
            }
            return vis; // Boolean
        },
        /* Start Interface functions */
        setVisible: function (visible) {
            throw errors.notImplemented + " 'setVisible'";
        },

        _checkboxOnClick: function (e) {
            throw errors.notImplemented + " '_checkboxOnClick'";
        },
        /* End Interface functions */

        _setSwatchWidth: function (width) {
            // dummy function that should be overridden when needed.
        },
        _getSwatchWidth: function (width) {
            return 0;
        },
        _toggleChildrenNode: function (visible) {
            // summary:
            //      toggles the child nodes div based on visibility
            if (this.toggler) {
                var en = this.expandoNode;
                if (domClass.contains(en, 'jstoolkit-icon-minus-sign')) {
                    domClass.remove(en, 'jstoolkit-icon-minus-sign');
                    domClass.add(en, 'jstoolkit-icon-plus-sign');
                    this.toggler.hide();
                } else {
                    domClass.remove(en, 'jstoolkit-icon-plus-sign');
                    domClass.add(en, 'jstoolkit-icon-minus-sign');
                    this.toggler.show();
                }
            }
        },
        _createIcon: function (legend, parentNode) {
            if (!legend) return;

            if (legend.yPosition !== undefined) {
                return domConstruct.create('img', {
                    src: this._blankGif,
                    style: {
                        backgroundImage: "url( " + this.mapService.legendSpriteUrl + ")",
                        backgroundPosition: "0px " + legend.yPosition + "px",
                        backgroundRepeat: "no-repeat",
                        width: legend.width + "px",
                        height: legend.height + "px"
                    }
                }, parentNode);
            } else if (legend.url !== undefined) {
                var src = legend.url;
                if (src != null && src.indexOf('data') == -1) {
                    // TODO: Removed !dojo.isIE as the below check works in IE8+. Determine if this is adequate
                    if (legend.imageData && legend.imageData.length > 0) {
                        src = "data:image/png;base64," + legend.imageData;
                    } else {
                        if (src.indexOf('http') !== 0) {
                            // resolve relative url
                            src = this.serviceLayer.url + '/' + this.layer.id + '/images/' + src;
                        }
                    }
                }
                return domConstruct.create('img', {
                    src: src
                }, parentNode);
            }
        },

        _hideSwatch: function () {
            // summary:
            //      swatches will have their visibility set to hidden in order to maintain proper DOM spacing (width/height)
            if (this.iconNode != null) {
                domClass.add(this.iconNode, "jstoolkit-hidden");
            }
        },

        _showSwatch: function () {
            // summary:
            //      enable the icon visibility
            if (this.iconNode != null) {
                domClass.remove(this.iconNode, "jstoolkit-hidden");
            }
        },

        disable: function () {
            //console.log(this.title + " (root _tocNode.disable()");

            // summary:
            //      disables this node and its children
            //commented out by aric..this can't happen the first time.  todo: revisit
            //if (this._state.enabled === false) return;

            // Update the state object
            this._state.enabled = false;

            // Apply disabled class (does nothing now)
            //domClass.add(this.domNode, "disabled");
            domClass.add(this.labelNode, "tocItemDisabled");

            // disable checkbox/radiobutton
            if (this._checkbox) {
                this._checkbox.disabled = true;
                //dojo.addClass(this._checkbox.parentNode, "disabled");
                dojo.addClass(this._checkbox.parentNode, 'dijitCheckBoxDisabled dijitDisabled');
            }

            if (this.hasDropDown) {
                dojo.addClass(this.dropDownMenuIcon, "tocItemDisabled");
            }
            this._hideSwatch();
            this._disableChildNodes();

            //console.log(this.title + " (root _tocNode.disable() completed successfully!");
        },

        enable: function () {
            // summary:
            //      enables this node and its children
            //if (this._state.enabled === true) return;

            // Update the state object
            this._state.enabled = true;

            // Remove disabled class (does nothing now)
            //domClass.remove(this.domNode, "disabled");
            domClass.remove(this.labelNode, "tocItemDisabled");

            var checked = true;
            // enable checkbox/radiobutton
            if (this._checkbox) {
                this._checkbox.disabled = false;
                checked = this._checkbox.checked;

                //dojo.removeClass(this._checkbox.parentNode, "disabled");
                dojo.removeClass(this._checkbox.parentNode, 'dijitCheckBoxDisabled dijitDisabled');
            }

            if (this.hasDropDown) {
                dojo.removeClass(this.dropDownMenuIcon, "tocItemDisabled");
            }

            if (checked) {
                this._showSwatch();
            } else {
                this._hideSwatch();
            }

            this._enableChildNodes();
        },

        _enableChildNodes: function () {
            // summary:
            //      enable child nodes
            this._expandCollapseChildren(true);
            for (var i = 0; i < this.nodes.length; i++) {
                this.nodes[i].enable();
            }
        },
        _disableChildNodes: function () {
            // summary:
            //      disable child nodes
            this._expandCollapseChildren(true);
            for (var i = 0; i < this.nodes.length; i++) {
                this.nodes[i].disable();
            }
        },
        _adjustToMapScale: function (scale) {

            //todo: minimize the calls to this
            if (scale == null) {
                if (this.getToc() && this.getToc().map) {
                    scale = this.getToc().map.getScale();
                }
                else {
                    return;
                }
            }

            console.log("_adjustToMapScale(): " + this.title + "->parent node = ", this._parent);
            //console.log(this.title + " (" + this.declaredClass + ")->parent declaredClass->" + this._parent.declaredClass);

            ////if parent is disabled, so am i
            if (this._parent && this._parent.declaredClass !== "wvs.dijits.Toc" && !this._parent.getVisible()) { //!this._parent._state.enabled) {
                this.disable(); 
            }
            //parent is enabled, see if I am
            else {
                console.log("checking " + this.title + "->scale:" + scale + ", min:" + this.minScale + ", max:" + this.maxScale);

                var outOfScale = (this.maxScale != 0 && scale < this.maxScale) || (this.minScale != 0 && scale > this.minScale);
                if (outOfScale) {
                    console.log("disabling");

                    //this will disable all child nodes
                    this.disable();

                    //var en = this.expandoNode;
                    //this._toggleCheckbox(en);
                } else {
                    console.log("enabling");

                    //this will enable child nodes but they may be disabled individually when they are processed
                    this.enable();

                    for (var i = 0; i < this.nodes.length; i++) {
                        this.nodes[i]._adjustToMapScale(scale);
                    }
                }
            }
        },
        _toggleCheckbox: function(en){
            if (!this._checkbox.checked) {
                console.log("_toggleCheckbox(): " + this.title + ", enabled: " + this._state.enabled);
                dojo.removeClass(this._checkbox.parentNode, 'dijitCheckBoxChecked dijitChecked');
                if (en) {
                    domClass.remove(en, 'jstoolkit-icon-minus-sign');
                    domClass.add(en, 'jstoolkit-icon-plus-sign');
                }
                if(this.toggler)
                    this.toggler.hide();
            } else {
                //console.log(this._checkbox.parentNode);
                dojo.addClass(this._checkbox.parentNode, 'dijitCheckBoxChecked dijitChecked');
                if (en) {
                    domClass.remove(en, 'jstoolkit-icon-plus-sign');
                    domClass.add(en, 'jstoolkit-icon-minus-sign');
                }
                if (this.toggler)
                    this.toggler.show();
            }
        },
        _expandCollapseChildren: function (recursive) {
            if (recursive !== false) {
                recursive = true;
            }

            if (this.allowVisibilityChange && this._checkbox && this.childrenNode) {
                var en = this.expandoNode;
                this._toggleCheckbox(en);
            }

            if (recursive) {
                for (var i = 0; i < this.nodes.length; i++) {
                    this.nodes[i]._expandCollapseChildren(true);
                }
            }
        },

        _updateVerticalAlignment: function () {
            // summary:
            //      Get the width of the widest swatch in this layers child nodes. Then set the width of each node to this max width. 
            //      Doing this provides a clean straight line for the layer names
            var maxWidth = 0;
            array.forEach(this.nodes, function (node) {
                var w = node._getSwatchWidth();
                if (w > maxWidth) {
                    maxWidth = w;
                }
            }, this);

            array.forEach(this.nodes, function (node) {
                node._setSwatchWidth(maxWidth);
            });
        },

        _createCheckBox: function () {
            //console.log(this.declaredClass + " _createCheckBox with visibility ", this._state.visible);
            this._checkbox = domConstruct.create('input', {
                type: 'checkbox',
                checked: this._state.visible
            }, this.checkboxNode);
            dojo.addClass(this._checkbox, 'dijitReset dijitCheckBoxInput');  //dijitCheckBoxChecked dijitChecked
            
            this.own(on(this._checkbox, "click", lang.hitch(this, "_checkboxOnClick")));

            // Remove on 4/22/14
            if (this._state.visible == true) {
                dojo.addClass(this._checkbox.parentNode, 'dijitCheckBoxChecked dijitChecked');
            }
            //else {
            //    //dojo.addClass(this.domNode, "disabled");
            //    dojo.addClass(this._checkbox.parentNode, 'dijitCheckBoxDisabled dijitDisabled');
            //}
        }

    });

    _TocNode.ContextMenuItems = {
        Transparency: function (scope) {
            var label = domConstruct.create("label", { innerHTML: "% transparent" });
            scope.transparencyLabel = domConstruct.create("span", { innerHTML: scope.transparency }, label);
            var transparencyDialog = new TooltipDialog({
                content: scope.transparencyLabel
            });
            var transparencySlider = new HorizontalSlider({
                name: "transparencySlider",
                value: scope.transparency,
                minimum: 0,
                maximum: 1,
                showButtons: true,
                style: "width:300px",
                onChange: function (value) {
                    var opacity = 1 - value;
                    // TODO: Remove the call to a custom extension method since esri (at 3.6) doesn't properly support changing opacity in IE8 and below
                    if (has("ie") <= 8) {
                        scope.mapService.setOpacityIE(opacity);
                    }
                    else {
                        scope.mapService.setOpacity(opacity);
                    }
                    scope.transparencyLabel.innerHTML = Math.round(value * 10000) / 100;
                }
            });
            transparencyDialog.addChild(transparencySlider);
            var transparencyMenuItem = new PopupMenuItem({
                label: "Transparency",
                popup: transparencyDialog,
                iconClass: "jstoolkit-icon jstoolkit-icon-transparency"
            });

            return transparencyMenuItem;
        },
        Remove: function (scope) {
            return new MenuItem({
                label: "Remove",
                iconClass: "jstoolkit-icon jstoolkit-icon-delete",
                onClick: lang.hitch(scope._parent, scope._parent._removeServiceLayer, scope.mapService)
            })
        }

    };

    return _TocNode;
});