﻿define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dojo/text!../templates/AttributeSearch.html",
    "./Filter",
    "dijit/form/Form",
    "dojo/_base/array",
    "dojo/store/Memory",
    "dojo/dom-construct",
    "dojo/dom",
    "dojo/_base/lang",
    "dijit/form/FilteringSelect",
    "dijit/form/TextBox",
    "dijit/form/Button",
    "dijit/form/RadioButton",
    "esri/tasks/QueryTask",
    "esri/tasks/query",
    "../MapLayerDropDown",
    "esri/layers/LayerInfo",
    "dojo/on",
    "dijit/Tooltip",
    "esri/layers/FeatureLayer",
    "../../common/SearchResultsAdapter",
    "dojo/fx/Toggler",
    "dojo/fx",
    "dojo/has",
    "../../_base/config",
    "../../common/Common",
    "dojo/_base/connect"
], function (
    declare,
    _WidgetBase,
    _TemplatedMixin,
    template,
    Filter,
    Form,
    array,
    Memory,
    domConstruct,
    dom,
    lang,
    FilteringSelect,
    TextBox,
    Button,
    RadioButton,
    QueryTask,
    Query,
    MapLayerDropDown,
    LayerInfo,
    on,
    Tooltip,
    FeatureLayer,
    SearchResultsAdapter,
    Toggler,
    coreFx,
    has,
    wvsConfig,
    Common,
    connect
) {
    // module:
    //      wvs/dijits/Filter/AttributeSearch
    // description:
    //      search for specific values on dynamic layers given user input
    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,
        filters: [],
        form: null,
        layers: null,
        layerSelectOptions: [],
        fieldSelectOptions: [],
        codedValueSelectOptions: [],
        joinOperatorSelect: null,
        layerSelect: null,
        fieldsToIgnore: [
            'esriFieldTypeGeometry',
            'esriFieldTypeRaster',
            'esriFieldTypeBlob'
        ],
        insertFilterAfter: 'filterControls',
        filterManagerForm: null,

        // useGlobalSpatialFilter: Boolean
        //      indicates to use the global spatial filter for the search, if there is one
        useGlobalSpatialFilter: false,

        // requireGlobalSpatialFilter: Boolean
        //      indicates that the global spatial filter must be defined to initiate a search
        requireGlobalSpatialFilter: false,

        // globalSpatialFilter: Extent
        //      The geometry defining the spatial filter in which to apply global attribute searches.
        globalSpatialFilter: null,

        // noGlobalSpatialFilterMessage: String
        //      the error message to show to the user if there is no global spatial filter and it is required
        noGlobalSpatialFilterMessage: "A global spatial filter is required to perform this search",

        constructor: function (params) {
            if (!params.map) {
                throw new Error("no map provided for " + this.declaredClass);
            }

            if (!params.store) {
                throw new Error("No store provided for Attribute Search");
            }

            lang.mixin(this, params);

            this.enableSaveAsQuickSearch = false;
        },

        postCreate: function () {
            var self = this;
            this.inherited(arguments);

            // Create internal widgets
            this.layerSelect = new MapLayerDropDown({
                map: this.map,
                exclusionList: ["ArcGISTiledMapServiceLayer"],
                style: "display:inline-block;",
                onLayerSelect: lang.hitch(this, function (item, node, evt) {
                    if (item.layer.isInstanceOf(LayerInfo) && item.type !== "ArcGISDynamicMapServiceGroupLayer" || item.layer.isInstanceOf(FeatureLayer)) {
                        this.set("activeLayer", item.layer);
                        this.layerSelect.dropdownButton.set("label", item.name);
                        this.layerSelect.dropdownButton.closeDropDown();
                    }
                    else {
                        Tooltip.show("Searches may only be applied to feature layers", this.layerSelect.domNode);

                        setTimeout(function () {
                            Tooltip.hide(self.layerSelect.domNode);
                        }, 3000);

                    }
                })
            }, this.layerSelect);

            this.joinOperatorSelect = new FilteringSelect({
                required: true
            }, this.joinOperatorSelect);

            var disableSearch = this.useGlobalSpatialFilter && this.requireGlobalSpatialFilter && !this.globalSpatialFilter;

            console.log("AttributeSearch-> this.useGlobalSpatialFilter:" + this.useGlobalSpatialFilter + ", this.requireGlobalSpatialFilter:" + this.requireGlobalSpatialFilter);

            this.applyButton = new Button({
                onClick: lang.hitch(this, function () {
                    var where = this.getWhereClause();
                    if (where) {
                        this._initiateQuery(where);
                    }
                }),
                label: "Search",
                disabled: disableSearch
            }, this.applyButton);



            this.joinOperatorSelect.set({
                store: new Memory({
                    data: [
                        { name: "Any", id: ' OR ' },
                        { name: "All", id: ' AND ' }
                    ]
                })
            });
            // Make All the default
            this.joinOperatorSelect.set("value", ' AND ');
            this.filterManagerForm = new Form({}, this.filterManagerForm);

            if (this.enableSaveAsQuickSearch) {

                this.quickSearchLabelTextBox = new TextBox({
                    placeHolder: "Quick search name",
                    style: "display:none;",
                    trim: true,
                    onKeyUp: function (evt) {
                        if (evt.which === 13 && this.displayedValue) {
                            self.saveAsQuickSearchButton.focus();
                            self._saveAsQuickSearch();
                        }
                    }
                }, this.quickSearchLabelTextBox);

                this.toggler = new Toggler({
                    node: this.quickSearchLabelTextBox.domNode,
                    showFunc: coreFx.wipeIn,
                    hideFunc: coreFx.wipeOut
                });

                this.saveAsQuickSearchButton = new Button({
                    label: "Save as Quick Search",
                    onClick: lang.hitch(this, function () {
                        var saveReady = true;
                        if (this.filterManagerForm.validate() && this._filtersPassValidation() && this.filters.length && this.activeLayer) {
                            if (this.quickSearchLabelTextBox.value) {
                                this._saveAsQuickSearch();
                            }
                            else {
                                this._initiateSaveAsQuickSearch();
                            }
                        }

                    })
                }, this.saveAsQuickSearchButton);
            }

            this._registerEventsAndWatches();


            this._subscription = connect.subscribe("globalSpatialFilterChanged", lang.hitch(this, function (filter) {
                if (!filter && this.useGlobalSpatialFilter && this.requireGlobalSpatialFilter) {
                    this.applyButton.set("disabled", true);
                    return;
                }
                this.applyButton.set("disabled", false);
            }));
        },
        destroy: function () {
            connect.unsubscribe(this._subscription);
            this.inherited(arguments);
        },
        _registerEventsAndWatches: function () {
            // Events
            this.own(on(this.addFilterLink, "click", lang.hitch(this, function (evt) {
                this.addFilter();
            })));


            // Watches
            this.watch("activeLayer", lang.hitch(this, function (object, oldVal, newVal) {
                // Clear all current filters
                array.forEach(this.filters, function (filter, index) {
                    this.removeFilter(filter);
                }, this);

                if (this.enableSaveAsQuickSearch) {
                    this.quickSearchLabelTextBox.set("value", "");
                    this.radios = [];
                }

                // Add the intial filter
                this.addFilter();
            }));
        },
        addFilter: function () {
            if (this.activeLayer != null) {

                // Create new filter and add it to our list of filters
                var filter = new Filter({
                    filterManager: this,
                    fieldSelectOptions: this.activeLayer.fields
                });
                this.filters.push(filter);

                on(filter.filterTextBox, "keyup", lang.hitch(this, function (evt) {
                    if (evt.which === 13) {
                        var newFilter = this.addFilter();
                        newFilter.fieldSelect.focus();
                    }
                }));

                domConstruct.place(filter.filterContainer, this.filterDiv);
                var href = domConstruct.create("a", { href: "#", innerHTML: "<span class='icon-trash icon-large'></span>" }, filter.filterForm.domNode);
                on(href, "click", lang.hitch(this, function (evt) {
                    this.removeFilter(filter);
                }));

                return filter;
            }
        },

        removeFilter: function (filter) {

            // Remove the filter node from the DOM
            domConstruct.destroy(filter.filterContainer);

            // Remove filter
            this.filters = array.filter(this.filters, function (item) {
                return filter.id != item.id;
            });
        },
        _filtersPassValidation: function () {
            var filtersPassValidation = true;
            // Check that all the filters pass validation
            for (var i = 0 ; i < this.filters.length; i++) {
                if (!this.filters[i].validate()) {
                    filtersPassValidation = false;
                    break;
                }
            }

            return filtersPassValidation;
        },
        _initiateSaveAsQuickSearch: function () {
            var self = this;
            // Show the text box for naming the quick search
            this.toggler.show();

            if (this.filters.length > 1) {
                this.radios = [];
                array.forEach(this.filters, function (filter, i) {
                    var radio = new RadioButton({
                        checked: i == 0,
                        name: "idProperty",
                        style: "inline-block;",
                        _filter: filter,
                        onClick: function () {
                            array.forEach(self.radios, function (radio) {
                                if (radio !== this) {
                                    radio.set("checked", false);
                                }
                            }, this);
                        }
                    });
                    this.radios.push(radio);
                    domConstruct.place(radio.domNode, filter.filterForm.domNode, "first");
                }, this);

                Tooltip.show("Select the field you want to quick search on. Other fields will retain their current values.", this.radios[0].domNode);

                setTimeout(function () {
                    Tooltip.hide(self.radios[0].domNode);
                }, 3000);
            }
            else {
                this.quickSearchLabelTextBox.focus();
            }
        },
        _saveAsQuickSearch: function () {
            var self = this;

            var queryAction = {
                type: "query",
                query: {
                    where: "",
                    outFields: []
                },
                mainWhere: "",
                idProperty: "",
                joinOperator: this.joinOperatorSelect.value,
                endPoint: this.activeLayer.url
            };



            // idProperty and mainWhere for QueryAction are implied
            if (this.filters.length == 1) {
                queryAction.idProperty = this.filters[0].fieldSelect.value;
                queryAction.mainWhere = this.filters[0].getQuery(true);
                queryAction.query.outFields.push(this.filters[0].fieldSelect.value);
            }
            else {
                var where = "";
                array.forEach(this.radios, function (radio, i) {
                    var filter = radio._filter;
                    if (radio.checked) {
                        queryAction.mainWhere = filter.getQuery(true);
                        queryAction.idProperty = filter.fieldSelect.value;
                    }
                    else {
                        if (i < self.filters.length - 1) {
                            where += filter.getQuery() + self.joinOperatorSelect.item.id;
                        }
                        else {
                            where += filter.getQuery();
                        }
                    }
                    queryAction.query.outFields.push(filter.fieldSelect.value);
                }, this);
                queryAction.query.where = where;
            }

            if (has("local-storage")) {
                if (localStorage.getItem("simpleSearch")) {
                    var simpleSearchActions = JSON.parse(localStorage.getItem("simpleSearch"));
                    simpleSearchActions.push({ label: this.quickSearchLabelTextBox.value, value: queryAction });
                    localStorage.setItem("simpleSearch", JSON.stringify(simpleSearchActions));
                }
                else {
                    localStorage.setItem("simpleSearch", JSON.stringify([{ label: this.quickSearchLabelTextBox.value, value: queryAction }]));
                }
            }

            this.quickSearchLabelTextBox.set("value", "");
            this.toggler.hide();
            return queryAction;
        },
        getWhereClause: function () {
            var self = this;
            var whereClause = '';

            if (this.filterManagerForm.validate() && this._filtersPassValidation()) {

                // Build the query
                array.forEach(this.filters, function (filter, index) {
                    var fQuery = filter.getQuery();
                    if (fQuery != null) {
                        if (index < self.filters.length - 1) {
                            whereClause += fQuery + self.joinOperatorSelect.item.id;
                        }
                        else {
                            whereClause += fQuery;
                        }
                    }
                });

                return whereClause;
            }

            return null;
        },
        _initiateQuery: function (whereClause, activeLayer) {
            var self = this,
                adapter = new SearchResultsAdapter();

            if (activeLayer == null) {
                activeLayer = this.activeLayer;
            }

            console.log("AttributeSearch._initiateQuery()");
            console.log(activeLayer);

            this.store.clear();
            this.store.setLoadingData(true);

            var queryTask = new QueryTask(activeLayer.url); // Add ID of the layer
            var query = new Query();
            if (this.useGlobalSpatialFilter && this.globalSpatialFilter) {
                query.geometry = this.globalSpatialFilter;
            }
            else if (this.useGlobalSpatialFilter && this.requireGlobalSpatialFilter && !this.globalSpatialFilter) {
                Common.errorDialog(this.noGlobalSpatialFilterMessage);
                return;
            }
            query.where = whereClause;
            query.outFields = ["*"];
            query.returnGeometry = true;
            queryTask.execute(query,
                function (featureSet) {
                    adapter.featureSetToResultSet(featureSet, activeLayer.url).then(function (searchResult) {
                        self.store.put(searchResult);
                    });
                },
                function (error) {
                    self.store.setLoadingData(false);
                }
            );
        }

    });
});