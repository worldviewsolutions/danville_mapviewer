﻿define([
    "dojo/_base/declare"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dojo/text!./templates/FormatExport.html"
    , "dojo/_base/lang"
    , "dojo/on"
    , "dojo/_base/array"
    , "dijit/form/Select"
    , "dijit/form/ComboBox"
    , "dijit/form/Button"
    , "../common/Common"
    , "esri/symbols/jsonUtils"
    , "esri/geometry/Polygon"
    , "esri/graphic"
    , "dojo/dom-class"
    , "dojo/dom"
    , "../_base/config"
    , "../utilities/jsonConverters"
    , "dojo/request/xhr"
    , "dojo/store/JsonRest"
    , "esri/geometry/webMercatorUtils"
    , "dojo/dom-style"
    , "dojo/dom-attr"
], function (declare, _WidgetBase, _TemplatedMixin, template, lang, on, array, Select, ComboBox, Button, Common, jsonUtils, Polygon, Graphic, domClass, dom, wvsConfig, jsonConverters, xhr, JsonRest, webMercatorUtils, domStyle, domAttr) {
    var FormatExport =  declare([_WidgetBase, _TemplatedMixin], {

        templateString: template,

        declaredClass: "wvs.dijits.FormatExport",

        inSrWkid: null,

        _exportLink: null,

        constructor: function(params, srcNodeRef){
            if (!params.exportService && !wvsConfig.defaults.services.exportService) {
                throw new Error("No export service defined for " + this.declaredClass);
            }
            if (!params.features) {
                throw new Error("No features defined for " + this.declaredClass);
            }
            lang.mixin(this, params);

            this.exportService = this.exportService ? this.exportService : wvsConfig.defaults.services.exportService;

            this.title = "Export " + this.features.length + " Features";
        },

        postCreate: function () {
            var self = this;
            // get fields for geometry types
            // NOTE: we only convert features that are of the same geometry and have the same attributes/fields
            var idFieldOptions = [];
            for (var key in this.features[0].attributes) {
                idFieldOptions.push({ label: key, value: key });
            }

            // Our server-side component doesn't currently understand wkid 102100 but it does the equivalent 3857
            this.inSrWkid = this.features[0].geometry.spatialReference.isWebMercator() ? 3857 : this.features[0].geometry.spatialReference.wkid;

            //this.idFieldSelect = new Select({
            //    options: idFieldOptions
            //}, this.idFieldSelect);


            this.outputFormatSelect = new Select({
                options: this._getValidExportOptions(),
                onChange: lang.hitch(this, this._formatChange)
            }, this.outputFormatSelect);


            var jsonRestStore = new JsonRest({
                target: this.exportService + "/GetSupportedWkid",
                idProperty: "wkid"
            });

            this.outputSrSelect = new ComboBox({
                store: jsonRestStore,
                searchAttr: "title",
                searchDelay: 100,
                autocomplete: true,
                placeHolder: "Enter a WKID",
                queryExpr: "${0}*",
                labelType: "html",
                style: "width:250px;",
                autoWidth: true
            }, this.outputSrSelect);
            

            this.exportButton = new Button({
                label: "Export",
                onClick: lang.hitch(this, this._beginExport)
            }, this.exportButton);

            this._formatChange(this.outputFormatSelect.value);

            this.own(on(this.exportLink, "click", lang.hitch(this, function () {
                this.exportButton.set("iconClass", "");
                this.exportButton.set("disabled", false);
                domStyle.set(this.exportButton.domNode, { display: "inline-block" });
                domStyle.set(this.exportLink, { display: "none" });
                window.location.href = self._exportLink;
            })));
        },
        _formatChange: function(format){
            if (format === FormatExport.EXPORT_KML || format === FormatExport.EXPORT_EXCEL) {
                this.outputSrSelect.set("disabled", true);
            }
            else {
                this.outputSrSelect.set("disabled", false);
            }
            
        },
        _beginExport: function () {
            var self = this;
            var postData = {
                ExportFormat: this.outputFormatSelect.value,
                JsonFeature: null,
                InputSpatialReference: 4326,
                OutputSpatialReference: !this.outputSrSelect.disabled && this.outputSrSelect.value ? this.outputSrSelect.item.wkid : this.inSrWkid
            };

            var FeatureCollection = {
                type: "FeatureCollection",
                features: []
            }
            var converter = new jsonConverters.esriConverter() ;
            for (var i = 0; i < this.features.length; i++) {
                var copiedFeature = new Graphic(webMercatorUtils.webMercatorToGeographic(this.features[i].geometry), this.features[i].symbol, lang.clone(this.features[i].attributes));
                
                // Remove any 'custom' properties like '__acordionId'
                delete copiedFeature.attributes.__acordionId;

                var feature = converter.toGeoJson(copiedFeature);

                // we must have ids for our features and they MUST start at 1 as the back-end starts counting from 1
                feature.id = i + 1;
                FeatureCollection.features.push(feature)
            };

            postData.JsonFeature = JSON.stringify(FeatureCollection);


            this.exportButton.set("iconClass", "icon-spinner icon-spin");
            this.exportButton.set("disabled", true);


            var dfd = xhr.post(this.exportService + "/ConvertFeature", {
                handleAs: "json",
                data: JSON.stringify(postData),
                headers: { 'Content-Type': "application/json; charset=UTF-8" }
            });

            dfd.then(
                function (data) {
                    console.log(data);
                    domStyle.set(self.exportButton.domNode, { display: "none" });
                    self._exportLink = data.Message;
                    domStyle.set(self.exportLink, { display: "inline" });
                },
                function (err) {
                    self.exportButton.set("iconClass", "");
                    self.exportButton.set("disabled", false);
                    throw err;
                }
            );
        },
        _getValidExportOptions: function () {
            // summary:
            //      creates an options array for a Select depending on the properties of the features
            var exportOptions = [
                { label: "Excel (XLSX)", value: FormatExport.EXPORT_EXCEL },
                { label: "KML", value: FormatExport.EXPORT_KML },
                { label: "GeoJSON", value: FormatExport.EXPORT_GEOJSON }
                //{ label: "ESRI File Geodatabase", value: FormatExport.FILEGEODATABASE } // currently working out some errors in FGDB export
            ];
            if(this.features.length > 1){
                var geomType = this.features[0].geometry.declaredClass,
                    mixedGeometry = false;
                for (var i = 0, il = this.features.length; i < il; i++) {
                    if (this.features[i].geometry.declaredClass != geomType)
                        mixedGeometry = true;
                }
                if (!mixedGeometry) {
                    exportOptions.push({ label: "Shapefile", value: FormatExport.EXPORT_SHAPEFILE});
                }
            }
            else
                exportOptions.push({ label: "Shapefile", value: FormatExport.EXPORT_SHAPEFILE });

            return exportOptions; // Array
        }
    });

    FormatExport.EXPORT_SHAPEFILE =1;
    FormatExport.EXPORT_KML = 2;
    FormatExport.EXPORT_EXCEL = 3;
    FormatExport.EXPORT_GEOJSON = 4;
    FormatExport.FILEGEODATABASE = 5;

    return FormatExport;
});