﻿console.log("wvs/dijits/ConfigurationDialog.js");

//This widget is unused in the application.

/*define([
    "dojo/_base/declare"
    , "dojo/_base/lang"
    , "dojo/_base/array"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dojo/text!./templates/ConfigurationDialog.html"
    , "dijit/layout/TabContainer"
    , "dijit/layout/ContentPane"
    , "dijit/form/Button"
    , "dijit/Dialog"
    , "dojo/string"
    , "dojo/dom-construct"
    , "dojo/query"
    , "dojo/dom-attr"
    , "require"
    , "dijit/form/CheckBox"
    , "dijit/form/TextBox"
    , "../common/Common"
], function (declare, lang, array, _WidgetBase, _TemplatedMixin, template, TabContainer, ContentPane, Button, Dialog, string, domConstruct, query, domAttr, require,CheckBox, TextBox, Common) {
    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,
        
        declaredClass: "wvs.dijits.ConfigurationDialog",

        constructor: function (params, srcNodeRef) {

            if (!params.config) {
                throw new Error("no config object provided for " + this.declaredClass);
            }

            lang.mixin(this, params);
            
            this.configurationTypeMap = {
                "boolean": this._booleanMapping,
                "string": this._stringMapping,
                "number": this._numberMapping,
                "symbolSelector": 2,
                "date": 3,
                "object": this._objectMapping
            };
            
            this._internalConfig = {};
            this._idCounter = 0;
            this._valueMappings = {};

            lang.setObject("window.activeConfig", this._internalConfig);
        },
        postCreate: function () {
            console.log(this.declaredClass + " postCreate");
            this._parseConfigObject();
            this.saveButton = new Button({
                onClick: lang.hitch(this, this.save)
            }, this.saveButton);
            this._createTabs();
        },
        resize: function(){
            this.tabs.resize();
        },
        _parseConfigObject: function () {
            console.log(this.declaredClass + " _parseConfigObject");
            var config = this.config,
                internalConfig = this._internalConfig;
            // All root objects will become tabs
            for (var key in config) {
                internalConfig[key] = lang.clone(config[key]);
            }
        },
        _createTabs: function () {
            console.log(this.declaredClass + " _createTabs");
            var internalConfig = this._internalConfig;
            
            this.tabs = new TabContainer({
                style: "height:400px; width:500px;"
            }, this.tabs);

            for (var rootKey in internalConfig) {
                var rootObject = internalConfig[rootKey];

                // Add tab for root object
                var cp = new ContentPane({
                    title: Common.camelCaseToHumanReadableString(rootKey),
                    content: ""
                });

                var objectContents = []
                
                for (var propertyKey in rootObject) {
                    if (!propertyKey.match(/^_/)) {
                        var property = rootObject[propertyKey],
                            propertyType = typeof property;

                        if (typeof this.configurationTypeMap[propertyType] === "function") {
                            var mappingFunction = lang.hitch(this, this.configurationTypeMap[propertyType], rootKey, propertyKey, cp);
                            objectContents.push(mappingFunction());
                        }
                        else {
                            throw new Error("unsupported config property type in " + this.declaredClass);
                        }
                    }
                }
                //array.forEach(objectContents, function (content) {
                //    cp.addChild(content);
                //}, this);
                this.tabs.addChild(cp);
            }
        },        
        _validate: function(){
            // Validation
        },
        _setPropertyFromString: function(propertyString, object, value){
            var propertyPath = propertyString.split("."),
                property = object;
    
            for(var i = 0; i < propertyPath.length; i++){
                if(i === propertyPath.length - 1){
                    property[propertyPath[i]] = value;
                }
                else{
                    property = property[propertyPath[i]];        
                }

            }
        },
        _mapInputToConfig: function(){
            var self = this,
                internalConfig = this._internalConfig,
                tabs = this.tabs.getChildren();

            array.forEach(tabs, function(tab){
                var nl = query(".jstoolkit-configuration-value", tab.containerNode);
                console.log("nl", nl);
                array.forEach(nl, function (configEntry) {
                    var configType = domAttr.get(configEntry,"data-jstoolkit-configuration-type");
                    switch (configType) {
                        case "boolean":
                            var radios = query("input", configEntry),
                                value;

                            array.forEach(radios, function (radio) {

                            });
                            
                            break;
                        case "string":
                            var input = query("input", configEntry)[0];
                            self._setPropertyFromString(input.name, internalConfig, input.value);
                            break;
                    }
                });
            });
            
        },
        // Type Mappings

        // If object, we assume this has extra information for us to parse
        _objectMapping: function(parentKey, childKey){

        },
        _stringMapping: function (parentKey, childKey, contentPane) {
            var self = this;
            var value = this._internalConfig[parentKey][childKey];
            var div = domConstruct.create("div");
            var label = domConstruct.create("label", { innerHTML: Common.camelCaseToHumanReadableString(childKey) }, div);
            var textbox = new TextBox({
                value: value,
                onKeyUp: function () { self._internalConfig[parentKey][childKey] = this.get("value"); }
            });
            textbox.placeAt(div);
            domConstruct.place(div, contentPane.domNode);
        },
        _numberMapping: function (parentKey, childKey) {
            var self = this;
            var value = this._internalConfig[parentKey][childKey];
            var div = domConstruct.create("div");
            var label = domConstruct.create("label", { innerHTML: Common.camelCaseToHumanReadableString(childKey) }, div);
            var textbox = new TextBox({
                value: value,
                onKeyUp: function () { self._internalConfig[parentKey][childKey] = this.get("value"); }
            });
            textbox.placeAt(div);
            domConstruct.place(div, contentPane.domNode);
        },
        _booleanMapping: function (parentKey, childKey, contentPane) {
            var self = this;
            var value = this._internalConfig[parentKey][childKey];
            var div = domConstruct.create("div");
            var label = domConstruct.create("label", { innerHTML: Common.camelCaseToHumanReadableString(childKey) }, div);
            var checkbox = new CheckBox({
                checked: value,
                onChange: function () { self._internalConfig[parentKey][childKey] = this.get("checked"); }
            });
            checkbox.placeAt(div);
            domConstruct.place(div, contentPane.domNode);
            //var content = string.substitute(booleanTemplate, { trueSelected: value ? 'checked="checked"' : '', falseSelected: !value ? 'checked="checked"' : '', propertyName: childKey + ": ", name: 'name="' + parentKey + "." + childKey + '"' });
            //return content;
        },
        
        save: function (evt) {
            var valid = true;
            // TODO: Validate


            // If valid, mixin the config object
            if (valid) {
                lang.mixin(this.config, this._internalConfig);

                if (this.onSave) {
                    this.onSave();
                }
            }
        }
    });

});*/