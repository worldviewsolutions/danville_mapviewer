﻿define([
    "dojo/_base/declare"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dojo/text!./templates/WhatNearby.html"
    , "dojo/_base/lang"
    , "dojo/_base/array"
    , "dojo/on"
    , "esri/units"
    , "esri/tasks/BufferParameters"
    , "esri/tasks/IdentifyTask"
    , "esri/tasks/IdentifyResult"
    , "esri/tasks/IdentifyParameters"
    , "esri/tasks/GeometryService"
    , "./MapLayerDropDown"
    , "../_base/config"
    , "dijit/form/Button"
    , "dijit/form/Select"
    , "dijit/form/TextBox"
    , "esri/geometry/Point"
    , "../common/Common"
    , "esri/layers/ArcGISDynamicMapServiceLayer"
    , "esri/layers/FeatureLayer"
    , "esri/layers/LayerInfo"
    , "../common/SearchResultsAdapter"
    , "esri/symbols/jsonUtils"
    , "esri/graphic"
], function (declare, _WidgetBase, _TemplatedMixin, template, lang, array, on, Units, BufferParameters, IdentifyTask, IdentifyResult, IdentifyParameters, GeometryService, MapLayerDropDown, wvsConfig, Button, Select, TextBox, Point, wvsCommon, ArcGISDynamicMapServiceLayer, FeatureLayer, LayerInfo, SearchResultsAdapter, symbolJsonUtils, Graphic) {
    // module:
    //      wvs/dijits/WhatNearby
    return declare([_WidgetBase, _TemplatedMixin], {

        templateString: template,

        declaredClass: "wvs.dijits.WhatNearby",

        constructor: function (params, srcNodeRef) {
            // map: Map
            //      a map instance to bind to
            this.map = null;
            
            // graphic: Graphic
            //      a graphic to define the geometry to buffer on
            this.graphic = null;

            // required params
            if (!params.map) {
                throw new Error("map required for " + this.declaredClass);
            }
            if (!params.graphic) {
                throw new Error("graphic required for " + this.declaredClass);
            }

            lang.mixin(this, params || {});

            // store: SearchResultsStore
            //      the store to bind to when initiating an identify
            this.store = params.store ? params.store : wvsConfig.defaults.searchResultStore ? wvsConfig.defaults.searchResultStore : null;

            if (!this.store) {
                throw new Error("search result store required for " + this.declaredClass);
            }

            // geometryService: String
            //      a geometry service to use when buffering on Polygons or Polylines (not currently supported client-side)
            this.geometryService = params.geometryService ? params.geometryService : wvsConfig.defaults.services.geometry ? wvsConfig.defaults.services.geometry : null;

            if(!this.geometryService && !this.graphic.isInstanceOf(Point)){
                throw new Error("geometry service required for " + this.declaredClass + " when the graphic is not a Point");
            }

            // markupLayer: MarkupLayer
            //      a markup layer to save the buffer to (if applicable)
            this.markupLayer = params.markupLayer ? params.markupLayer : wvsConfig.defaults.markupLayer ? wvsConfig.defaults.markupLayer : null;

            // if we have a markup layer to save to, we need default symbols
            if (this.markupLayer) {
                this.pointSymbol = symbolJsonUtils.fromJson(wvsConfig.defaults.symbols.point);
                this.polygonSymbol = symbolJsonUtils.fromJson(wvsConfig.defaults.symbols.polygon);
            }
            
            // _bufferGeometry: Geometry
            //      points to the buffered geometry to be saved to markup
            this._bufferGeometry = null;
            
        },
        postCreate: function () {
            this.inherited(arguments);

            this.layerSelect = new MapLayerDropDown({
                map: this.map,
                includeAllVisible: true,
                showOnlyVisibleLayers: true,
                exclusionList: ["ArcGISTiledMapServiceLayer", "FeatureLayer"],
                style: "display:block;"                
            }, this.layerSelect);
            this.layerSelect.startup();


            this.bufferRadius = new TextBox({
                placeHolder: "0",
                style: "width: 50px;"
            }, this.bufferRadius);

            this.bufferUnits = new Select({
                options: [{ label: "Feet", value: GeometryService.UNIT_FOOT },
                    { label: "Kilometers", value: GeometryService.UNIT_KILOMETER },
                    { label: "Meters", value: GeometryService.UNIT_METER },
                    { label: "Miles", value: GeometryService.UNIT_STATUTE_MILE }
                ]
            }, this.bufferUnits);

            this.searchButton = new Button({
                disabled: true,
                onClick: lang.hitch(this, this._searchBuffer)
            }, this.searchButton);

            this.saveButton = new Button({
                disabled: true,
                onClick: lang.hitch(this, this._saveBuffer),
                tooltip: this.markupLayer ? "" : "Cannot save buffer without a markup layer"
            }, this.saveButton);

            this._setEventsAndWatches();
        },
        destroy: function () {
            this._removeBufferFromMap();
            this.inherited(arguments);
        },
        _setEventsAndWatches: function () {
            // summary:
            //      set events and watches for this widget
            this.own(on(this.layerSelect, "layer-selected", lang.hitch(this, function (layer) {
                this.set("_identifySelection", layer);
            })));

            this.watch("_identifySelection", lang.hitch(this,function (name, oldVal, newVal) {
                this.searchButton.set("disabled", false);
            }));

            this.own(on(this.map.infoWindow, "hide", lang.hitch(this, function () {

            })));
        },
        _searchBuffer: function () {
            // summary:
            //      generates the buffer from the given geometry to feed to the identify task
            var geometry = this.graphic.geometry,
                bufferUnits = this.bufferUnits.get("value"),
                radius = this.bufferRadius.get("value") ? parseInt(this.bufferRadius.get("value")) : 0;
                
                if(isNaN(radius) || radius < 0){
                    wvsCommon.errorDialog("Radius is not valid. It must be a number 0 or above.");
                    return;
                }
                else if (radius > 0) {
                    var bufferParams = new BufferParameters();
                    bufferParams.distances = [radius];
                    bufferParams.unit = bufferUnits;
                    bufferParams.bufferSpatialReference = geometry.spatialReference;
                    bufferParams.outSpatialReference = this.map.spatialReference;
                    bufferParams.geometries = [geometry];
                    console.log("bufferParams", bufferParams);
                    var geomService = new GeometryService(this.geometryService);
                    geomService.buffer(bufferParams,
                        // callback
                        lang.hitch(this, function (geometries) {
                            this._bufferGeometry = geometries[0];
                            this._addBufferToMap();
                            this._identifyBuffer(geometries[0]);
                            if (this.markupLayer) {
                                this.saveButton.set("disabled", false);
                            }
                        }),
                        // errback
                        function (error) {
                            wvsCommon.errorDialog(error);
                            throw new Error("buffer failed");
                        }
                    );

                }
                else {
                    this._bufferGeometry = geometry;
                    this._identifyBuffer(geometry);
                    this._addBufferToMap();
                    if (this.markupLayer) {
                        this.saveButton.set("disabled", false);
                    }
                }
        },
        _addBufferToMap: function () {
            // summary:
            //      add the buffer to the map
            var graphic;
            if (this._bufferGeometry.isInstanceOf(Point)) {
                graphic = new Graphic(this._bufferGeometry, this.pointSymbol, { description: "" });
            }
            else {
                graphic = new Graphic(this._bufferGeometry, this.polygonSymbol, { description: ""});
            }
            graphic.setEditing(true);
            // TODO: For some reason, if I import InfoTemplateManager I get an empty object. This is currently a workaround so I can actually get to it.
            graphic.setInfoTemplate(this._parent.getTabularAttributeTemplate({
                title: "Buffer - " + this.bufferRadius.get("value") + " " + array.filter(this.bufferUnits.options, function (option) { return option.value === this.bufferUnits.value; }, this)[0].label
                , createWidgetsFromGeometry: true
            }));
            graphic.save();
            this.map.graphics.add(graphic);
            this._graphic = graphic;
        },
        _removeBufferFromMap: function(){
            if (this._graphic) {
                this.map.graphics.remove(this._graphic);
            }
        },
        _saveBuffer: function () {
            // summary:
            //      Saves the buffer to a markup layer
            if (this._graphic && this.markupLayer) {
                this._removeBufferFromMap();
                this.markupLayer.add(this._graphic);
            }
            else {
                wvsCommon.errorDialog("Cannot save markup due to a missing geometry and/or markup layer");
            }
        },
        _identifyBuffer: function (geometry) {
            // summary:
            //      performs an identify task with the given geometry
            // geometry: Geometry
            //      the geometry to identify on
            var self = this;
            var processIdentify = lang.hitch(this, function (map, geometry, identifySelection) {
                 var identifyParameters = new IdentifyParameters();
                 identifyParameters.mapExtent = map.extent;
                 identifyParameters.height = map.height;
                 identifyParameters.width = map.width;
                identifyParameters.geometry = geometry;
                identifyParameters.tolerance = 0;
                identifyParameters.returnGeometry = true;
                identifyParameters.layerOption = IdentifyParameters.LAYER_OPTION_ALL;

                var identifyUrl = "";
                var identifyTask;
                var searchResultsAdapter = new SearchResultsAdapter();

                if (identifySelection.layer) {
                    // This means we are identifying on all visible layers within a MapServer
                    identifyUrl = identifySelection.layer.url;
                    identifyParameters.layerIds = array.map(identifySelection.visibleSubLayers, function (visibleSubLayer) {
                        return visibleSubLayer.id;
                    }, this);
                }
                else if (identifySelection.isInstanceOf(LayerInfo)) {
                    var layerIds = [];

                    // Only get visible layers from group layers
                    if (identifySelection.subLayerIds) {
                        layerIds = layerIds.concat(array.map(
                            array.filter(identifySelection._subLayerInfos, function (subLayerInfo) {
                                return subLayerInfo.visible;
                            }),
                            function (visibleSubLayerInfo) {
                                return visibleSubLayerInfo.id;
                            }));
                    }
                    else {
                        // If this isn't a group layer, disregard visiblity
                        layerIds.push(identifySelection.id);
                    }
                    var parentLayerInfo = identifySelection._parentLayerInfo;
                    while (!parentLayerInfo.isInstanceOf(ArcGISDynamicMapServiceLayer)) {
                        parentLayerInfo = parentLayerInfo._parentLayerInfo;
                    }

                    identifyUrl = parentLayerInfo.url;
                    identifyParameters.layerIds = layerIds;
                }
                else if (identifySelection.isInstanceOf(ArcGISDynamicMapServiceLayer)) {
                    // Identify all visible non-group layers within the given MapServer
                    identifyUrl = identifySelection.url;
                    var layerIds = array.map(
                        array.filter(identifySelection.layerInfos, function (layerInfo) {
                            return !layerInfo.subLayerIds && layerInfo.visible;
                        }),
                        function (visibleLayerInfo) {
                            return visibleLayerInfo.id;
                        });
                    identifyParameters.layerIds = layerIds;
                }
                else {
                    throw new Error("unsupported identify type for " + this.declaredClass);
                }

                identifyTask = new IdentifyTask(identifyUrl);

                this.store.clear();
                this.store.setLoadingData(true);
                
                identifyTask.execute(identifyParameters,
                    function (identifyResults) {
                        var transformedResults = searchResultsAdapter.identifyToResultSet(identifyResults, identifyUrl);
                        transformedResults.then(
                            function (searchResult) {
                                self.store.put(searchResult);
                            }
                        );
                    },
                    function (error) {
                        self.store.setLoadingData(false);
                    }
                );
            });
            if (this._identifySelection instanceof Array) {
                array.forEach(this._identifySelection, function (selection) {
                    processIdentify(this.map, geometry, selection);
                }, this);
            }
            else {
                processIdentify(this.map, geometry, this._identifySelection);
            }
        }
    });
});