﻿console.log("wvs/dijits/Accordion.js");

define([
    "dojo/_base/declare"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dojo/text!./templates/Accordion.html"
    , "dojo/_base/lang"
    , "dojo/on"
    , "dojo/_base/array"
    , "dojo/query"
    , "dgrid/List"
    , "dgrid/extensions/Pagination"
    , "dijit/layout/TabContainer"
    , "dijit/layout/ContentPane"
    , "dojo/store/Memory"
    , "require"
    , "dojo/dom-construct"
    , "dojo/dom-attr"
    , "dojo/dom-class"
    , "../common/SearchResultsStore"
    , "dojo/dom-style"
    , "dijit/DropDownMenu"
    , "dijit/MenuItem"
    , "dijit/form/DropDownButton"
    , "dijit/form/Button"
    , "../_base/config"
    , "esri/symbols/jsonUtils"
    , "esri/symbols/PictureMarkerSymbol"
    , "esri/symbols/SimpleLineSymbol"
    , "esri/geometry/Point"
    , "esri/geometry/Polygon"
    , "esri/geometry/Polyline"
    , "esri/geometry/Multipoint"
    , "esri/InfoTemplate"
    , "esri/graphic"
    , "esri/graphicsUtils"
    , "esri/layers/GraphicsLayer"
    , "dijit/form/Select"
    , "dojo/fx/Toggler"
    , "dojo/fx"
    , "dijit/layout/AccordionContainer"
    , "../common/InfoTemplateManager"
    , "dojo/dom-geometry"
    , "./FormatExport"
    , "../common/Common"
    , "esri/lang"
    , "./SimpleButton"
    , "../common/esriUtilities"
    , "esri/tasks/RelationshipQuery"
    , "../common/SearchResultsAdapter"
    , "./ResultsConsole"
    , "esri/geometry/webMercatorUtils"
    , "dojo/Evented"
    , "../extensions/esri/graphicsUtils"
], function (declare, _WidgetBase, _TemplatedMixin, template, lang, on, array, query, List, Pagination, TabContainer, ContentPane, Memory, require, domConstruct, domAttr, domClass, SearchResultsStore, domStyle, DropDownMenu, MenuItem, DropDownButton, Button, wvsConfig, symbolJsonUtils, PictureMarkerSymbol, SimpleLineSymbol, Point, Polygon, Polyline, Multipoint, InfoTemplate, Graphic, graphicsUtils, GraphicsLayer, Select, Toggler, coreFx, AccordionContainer, InfoTemplateManager, domGeom, FormatExport, Common, esriLang, SimpleButton, esriUtilities, RelationshipQuery, SearchResultsAdapter, ResultsConsole, webMercatorUtils, Evented) {
    // module:
    //      wvs/dijits/Accordion
    var Accordion = declare([_WidgetBase, _TemplatedMixin], {

        templateString: template,

        declaredClass: "wvs.dijits.Accordion",

        // defaultColumnsShown: Integer
        //      The default number of columns to show in a dgrid (others will be hidden by default)
        defaultColumnsShown: 10,

        // exportService: String
        //      the service to perform export actions against
        exportService: null,

        //layerAttributeRestrictions
        //      The restricted attributes that need to be defined on a layer-by-layer basis. Stored in a map
        //      of URLs for the layers mapped to an array of attributes to hide for features from that layer
        layerAttributeRestrictions: null,

        //globalAttributeRestrictions
        //      An array of attributes to restrict from every feature that is added to the accordion container
        globalAttributeRestrictions: null,

        // _selectAllLimit: Integer
        //      the number of features that can be selected (highlighted) at once
        _selectAllLimit: 100,

        constructor: function (params, srcNodeRef) {
            // Array of integers declaring paging options
            this.pagingSizeOptions = [10, 25, 50];

            this.layerAttributeRestrictions = {};
            this.globalAttributeRestrictions = [];

            if (!params.map) {
                throw new Error("no map defined for " + this.declaredClass);
            }
            lang.mixin(this, params || {});

            // Required parameters            
            this.store = params.store ? params.store : wvsConfig.defaults.searchResultStore ? wvsConfig.defaults.searchResultStore : null;
            this.polygonSymbol = this.polygonSymbol ? symbolJsonUtils.fromJson(this.polygonSymbol) : symbolJsonUtils.fromJson(wvsConfig.defaults.symbols.polygon);
            this.pointSymbol = this.pointSymbol ? symbolJsonUtils.fromJson(this.pointSymbol) : symbolJsonUtils.fromJson(wvsConfig.defaults.symbols.point);
            this.lineSymbol = this.lineSymbol ? symbolJsonUtils.fromJson(this.lineSymbol) : symbolJsonUtils.fromJson(wvsConfig.defaults.symbols.line);
            this.exportService = wvsConfig.defaults.services.exportService ? wvsConfig.defaults.services.exportService : null;
            if (!this.store) {
                throw new Error("no store defined for " + this.declaredClass);
            }


            // tracks the currently highlighted/selected features
            this.featureHighlights = [];
            this.gridMap = {};
            this.gridIds = 0;
            this.graphicsLayer = new GraphicsLayer();
            this.graphicsLayer._visibleInToc = false;
        },
        postCreate: function () {
            domConstruct.create("img", { src: require.toUrl("./images/loading_big.gif") }, this.resultsLoadingMessage);

            // get all results
            this.resultSets = this.store.query();

            // observe the result set from our store
            var observeHandler = function (object, removedFrom, insertedInto) {
                if (removedFrom > -1) {
                    this._removeSearchResult(object);
                }
                if (insertedInto > -1) {
                    this._addResultSetToDropdown(object);
                }
            };
            this._observeHandle = this.resultSets.observe(lang.hitch(this, observeHandler));

            this.map.addLayer(this.graphicsLayer);

            this.own(
                on(this.store, "data-loading", lang.hitch(this, function () {
                    this._clearHighlights();
                    this._clearGrids();
                    domStyle.set(this.resultsTabs.domNode, "display", "none");
                    domStyle.set(this.noResultsMessage, "display", "none");
                    domStyle.set(this.resultsLoadingMessage, "display", "block");
                })),
                on(this.store, "data-loaded", lang.hitch(this, function () {
                    domStyle.set(this.resultsTabs.domNode, "display", "block");
                    domStyle.set(this.resultsLoadingMessage, "display", "none");
                }))
            );
        },
        startup: function () {
            this.resultsTabs = new AccordionContainer({
                style: "height:100%;position:relative;",
                isLayoutContainer: true
            }, this.resultsTabs);
            this.resultsTabs.startup();
            this.inherited(arguments);
        },

        /****************************************************************************************************************
        * configureHiddenAttributes
        *   This function will define internal properties for the Accordion to know when to display or not display
        *   attributes based on individual layer and global restrictions configured in the mapSettings file.
        *@param settings: Object
        *   The globalOptions object in the mapSettings file.
        ****************************************************************************************************************/
        configureHiddenAttributes: function (settings) {
            if (settings.globalAttributeRestrictions) {
                for (var i = 0; i < settings.globalAttributeRestrictions.length; i++) {
                    this.globalAttributeRestrictions.push(settings.globalAttributeRestrictions[i]);
                }
            }
            for (var i = 0; i < settings.layers.length; i++) {
                var lyr = settings.layers[i];
                if (lyr.hiddenAttributeSettings && lyr.hiddenAttributeSettings.length) {
                    for (var j = 0; j < lyr.hiddenAttributeSettings.length; j++) {
                        //If the hiddenAttributSettings item is an object with a layer id, construct the appropriate url and
                        //add hidden attributes accordingly.
                        if (lyr.hiddenAttributeSettings[j].layerId) {
                            var fullUrl = lyr.url + "/" + lyr.hiddenAttributeSettings[j].layerId;
                            if (this.layerAttributeRestrictions[fullUrl] == undefined) {
                                this.layerAttributeRestrictions[fullUrl] = [];
                            }
                            for (var k = 0; k < lyr.hiddenAttributeSettings[j].hiddenFields.length; k++) {
                                this.layerAttributeRestrictions[fullUrl].push(lyr.hiddenAttributeSettings[j].hiddenFields[k]);
                            }
                        //If it's a FeatureLayer let the url for the layer object be the url for the mapping.
                        } else {
                            if (this.layerAttributeRestrictions[lyr.url] == undefined) {
                                this.layerAttributeRestrictions[lyr.url] = [];
                            }
                            this.layerAttributeRestrictions[lyr.url].concat(lyr.hiddenAttributeSettings[j].hiddenFields);
                        }
                    }
                }
            }
        },

        destroy: function () {
            this.map.removeLayer(this.graphicsLayer);
            this.inherited(arguments);
        },
        resize: function () {
            // summary:
            //      Resizes the results console (used for when the results console is a child of a layout widget)
            // returns:
            //      undefined
            this.resultsTabs.resize();
        },
        _showResults: function (resultsFound) {
            // summary:
            //      Controls the visual representation of the results console based upon wheter or not a result was found
            // returns:
            //      undefined
            domStyle.set(this.resultsLoadingMessage, "display", "none");

            if (resultsFound) {
                domStyle.set(this.resultsTabs.domNode, "display", "block");
                domStyle.set(this.noResultsMessage, "display", "none");
                domStyle.set(this.resultsLoadingMessage, "display", "none");
                this.emit("result-found");
            }
            else {
                this._clearHighlights();
                this._clearGrids();
                domStyle.set(this.resultsTabs.domNode, "display", "none");
                domStyle.set(this.noResultsMessage, "display", "block");
            }
        },
        _zoomToFeature: function (feature) {
            // summary:
            //      Zooms to a given feature
            // description:
            //      Zooms to a given feature. Polygon, Polyline, and Point have been tested. Multipoint has not been tested.
            // returns:
            //      undefined
            var geometry = webMercatorUtils.webMercatorToGeographic(feature.geometry),
                extent = null;

            if (geometry.isInstanceOf(Point)) {
                extent = graphicsUtils.pointToExtent(geometry);
            }
            else if (geometry.isInstanceOf(Multipoint)) {
                extent = geometry.points.length > 1 ? geometry.getExtent() : graphicsUtils.pointToExtent(geometry.points[0]);
            }
            else {
                extent = geometry.getExtent();
            }

            if (extent != null) {
                this.map.setExtent(extent, true);
            }
            else {
                console.warn("a null extent was found for " + feature);
            }
        },
        _clearHighlights: function () {
            // summary:
            //      Clear highlight graphics from the result console's graphic layers
            // returns:
            //      undefined
            this.graphicsLayer.clear();
            this.featureHighlights = [];
        },
        _clearGrids: function () {
            // summary:
            //      Destroys all grid instances
            // returns:
            //      undefined
            for (var id in this.gridMap) {
                this._destroyGrid(this.gridMap[id]);
            }
            gridMap = {};
        },
        _toggleGeometryHighlight: function (feature, highlight) {
            // summary:
            //      Adds/removes a highlight graphic for a particular feature.
            // description:
            //      Adds/removes a highlight graphic for a particular feature. Polygon, Polyline, and Point have been tested. Multipoint has not been tested.
            // returns:
            //      undefined
            var geometry = webMercatorUtils.webMercatorToGeographic(feature.geometry),
                highlightGraphic = null;

            // highlight the feature if highlight is true and the feature is not already highlighted
            if (highlight && !array.some(this.featureHighlights, function (featureHighlight) { return featureHighlight.feature === feature })) {
                if (geometry.type === "point") {
                    highlightGraphic = new Graphic(geometry, this.pointSymbol);
                }
                else if (geometry.type === "polygon") {
                    highlightGraphic = new Graphic(geometry, this.polygonSymbol);
                }
                else if (geometry.type === "polyline") {
                    highlightGraphic = new Graphic(geometry, this.lineSymbol);
                }

                if (highlightGraphic != null) {
                    highlightGraphic.attributes = feature.attributes;
                    highlightGraphic.setInfoTemplate(this.infoTemplate);
                    this.featureHighlights.push({ feature: feature, highlight: highlightGraphic });
                    this.graphicsLayer.add(highlightGraphic);
                }
            } else if (!highlight) {
                // Otherwise, find & remove the highlight
                var featureIndex = -1,
                    featureHighlights = this.featureHighlights;
                for (var i = 0, il = featureHighlights.length; i < il; i++) {
                    if (featureHighlights[i].feature === feature) {
                        this.graphicsLayer.remove(featureHighlights[i].highlight);
                        featureIndex = i;
                        break;
                    }
                }
                if (featureIndex !== -1) {
                    this.featureHighlights.splice(featureIndex, 1);
                }
            }
        },
        _addResultSetToDropdown: function (searchResult) {
            // summary:
            //      Takes a SearchResult and creates a pane for the result
            // returns:
            //      undefined

            // if there are results, show the proper GUI elements and create the tab/grid
            if (searchResult.searchResult.resultsSet.length > 0) {
                this._showResults(true);
                array.forEach(searchResult.searchResult.resultsSet, function (resultSet) {
                    this._createList(resultSet);
                    this.gridIds++;
                }, this);
            }
            else {
                if (!this.resultSets.length || !array.some(this.resultSets, function (resultSet) { console.log(resultSet); return resultSet.searchResult.resultsSet.length; }));
                this._showResults(false);
            }
        },
        _removeSearchResult: function (searchResult) {
            // summary:
            //      Removes a tab/grid based upon the given SearchResult
            // returns:
            //      undefined
            var gridMap = this.gridMap;

            array.forEach(searchResult.searchResult.resultsSet, function (resultSet) {
                for (var id in gridMap) {
                    var gridObject = gridMap[id];
                    if (gridObject.resultSet === resultSet) {
                        this._destroyGrid(id);
                    }
                }
            }, this);
        },
        _destroyGrid: function (id) {
            // summary:
            //      Destroys a grid instance
            // description:
            //      Destroys a grid instance. Removes grid events, destroys the dgrid, removes the associated tab and deletes the object from the grid map.
            // returns:
            //      undefined
            var gridInfo = this.gridMap[id];
            if (gridInfo) {
                //Make sure to remove all highlights currently highlighted for items in the grid else we'll never be
                //able to get rid of those highlights.
                for (var i = 0; i < gridInfo.grid.accordion.featureHighlights.length; i++) {
                    gridInfo.grid.accordion.graphicsLayer.remove(gridInfo.grid.accordion.featureHighlights[i].highlight);
                }
                array.forEach(gridInfo.events, function (gridEvent) {
                    gridEvent.remove();
                });
                gridInfo.grid.destroy();
                this.resultsTabs.removeChild(gridInfo.contentPane);
                delete this.gridMap[id];
            }
        },
        _createList: function (resultSet) {
            // summary:
            //      Creates a grid for a given SearchResult
            // returns:
            //      undefined
            var self = this;

            var accordionResult = new _AccordionResult({ resultSet: resultSet, accordion: this, exportMenu: this.exportService != null, style: "height:100%" });

            var cp = new ContentPane({
                title: "(" + resultSet.data.length + ") " + resultSet.name,
                closable: true,
                style: "padding:0px;height:100%;width:100%;position:relative;overflow:hidden;"
            });
            cp.addChild(accordionResult);
            on.once(cp, "show", function () { setTimeout(function () { self.resultsTabs.resize(); }, 200); });
            cp.startup();
            this.resultsTabs.addChild(cp);



            var gridId = this.gridIds;
            this.gridMap[gridId] = {
                grid: accordionResult,
                resultSet: resultSet,
                contentPane: cp
            };

            // Custom modification to make things closable
            var tp = cp._wrapperWidget.button;
            domStyle.set(tp.focusNode, { display: "inline-block" });
            var closeIcon = domConstruct.create("span", { style: "float:right;cursor:pointer;", innerHTML: "x" }, tp.domNode);
            this.own(on(closeIcon, "click", lang.hitch(this, function (evt) {
                this._destroyGrid(gridId);
            })));

        }
    });

    var _AccordionResult = declare([_WidgetBase], {

        PaginatedList: declare([List, Pagination]),

        displayField: "",

        sortfield: "",

        sortDescending: false,

        pagingSizeOptions: [10, 25, 50],

        exportMenu: true,

        _featureIds: 0,

        postCreate: function () {
            var self = this;

            // a map to associate a feature with its associated _INTERNAL_ id
            // Reasoning: before, I hackily inserted the feature into its own attributes to provide a link between an attributes object and its associated feature.
            // This has the side-effect of breaking JSON serialization. We will create _internal_ IDs for our features and set up a 'map' 
            // This has to be done because dgrid takes key/val pairs as its data. 
            this._idToFeatureMap = {};

            // Populate our map with internal ids
            for (var i = 0, il = this.resultSet.data.length; i < il; i++) {
                this._setInternalId(this.resultSet.data[i].attributes);
                this._idToFeatureMap[this.resultSet.data[i].attributes.__accordionId] = this.resultSet.data[i];
            }




            this.displayField = this.resultSet.displayField;
            this.store = new Memory({ data: array.map(this.resultSet.data, function (f) { return f.attributes; }) });

            this.fieldSortSelect = new Select({
                options: array.map(this.resultSet.fields, function (field) {
                    return { label: field.alias, value: field.alias, type: field.type, selected: field.alias === self.displayField };
                }),
                onChange: lang.hitch(this, function (value) { this.list.set("sort", value, this.sortDescending); })
            });



            this.list = new this.PaginatedList({
                store: this.store,
                renderRow: lang.partial(this._renderRow, this),
                pagingSizeOptions: this.pagingSizeOptions,
                sort: this.displayField,
                showLoadingMessage: false
            });

            // These two css rules fix some styling issues with the grid inside of an accordion container
            //this.list.addCssRule(".dgrid", "position:static;");
            //this.list.addCssRule(".dgrid-scroller", "top:31px");

            this.list.on("dgrid-refresh-complete", lang.hitch(this, this._listRefresh));

            var toolDiv = domConstruct.create("div", {}, this.domNode);//{ style: "height:32px;top:0;right:0;left:0;bottom:0;position:absolute;" }, this.domNode);
            this._createActionMenu(this.list, toolDiv);

            domConstruct.place(this.fieldSortSelect.domNode, toolDiv);
            //this.fieldSortSelect.placeAt(this.domNode);

            var sortButton = new Button({
                iconClass: "icon-sort-by-alphabet",
                onClick: lang.hitch(this, function (evt) {
                    // toggle ascending/descending sort
                    this.sortDescending = !this.sortDescending;
                    // set iconClass based on asc/desc
                    sortButton.set("iconClass", this.sortDescending ? "icon-sort-by-alphabet-alt" : "icon-sort-by-alphabet");

                    console.log("sort value", this.fieldSortSelect.value);

                    this.list.set("sort", this.fieldSortSelect.value, this.sortDescending);
                })
            });
            sortButton.placeAt(toolDiv);

            this.tools = toolDiv;


            domConstruct.place(this.list.domNode, this.domNode);

            // Fix for layout within accordion container
            var nl;
            nl = query(".dgrid", this.domNode);
            array.forEach(nl, function (n) { domStyle.set(n, { position: "static" }); });
            nl = query(".dgrid-scroller", this.domNode);
            array.forEach(nl, function (n) { domStyle.set(n, { top: "31px" }); });
        },
        startup: function () {
            //domStyle.set(this.list.domNode, { position: "absolute", top: "32px", left: 0, right: 0, bottom: 0 });
            this.list.startup();
            this.inherited(arguments);
        },
        _setInternalId: function (attributes) {
            //  summary:
            //      sets an internal id to allow associating a feature with its attributes object
            attributes.__accordionId = this._featureIds++;
        },
        _renderRow: function (self, item, options) {
            // the most complicated part of all of this. my apologies but most of this was written in a day so...
            var list = self.list;
            var displayField = self.displayField;
            var container = domConstruct.create("div", { "class": "row-fluid" });
            var checkBoxDiv = domConstruct.create("div", { "class": "colspan1" }, container);
            var highlightCheckBox = domConstruct.create("input", { type: "checkbox" }, checkBoxDiv);
            var feature = self._idToFeatureMap[item.__accordionId];

            var onClick = function () {
                var checked = this.checked;
                feature._checked = checked;
                var row = list.row(item);
                self.accordion._toggleGeometryHighlight(feature, checked);
            };

            on(highlightCheckBox, "click", onClick);
            self.accordion._toggleGeometryHighlight(feature, typeof feature._checked === "boolean" ? feature._checked : false);


            if (feature._checked) {
                domAttr.set(highlightCheckBox, "checked", "checked");
                setTimeout(function () {
                    var row = list.row(item);
                }, 100);
            }

            var firstKey = null;

            if (options.sort) {
                var span = domConstruct.create("span", { innerHTML: options.sort[0].attribute + ": " }),
                    value = Common.getHyperlink(options.sort[0].attribute, item[options.sort[0].attribute]);
                if (typeof value !== "object") {
                    span.innerHTML = span.innerHTML + value;
                }
                else
                    domConstruct.place(value, span);
                firstKey = span;
            }

            var infoContent = [];
            var i = 0;
            var excludes = ["toolkitSelectGraphic", "toolkitZoomTo", "__accordionId"];
            for (var key in item) {
                var skipProp = false;
                for (var i = 0; i < self.accordion.globalAttributeRestrictions.length; i++) {
                    if (key.toLowerCase() == self.accordion.globalAttributeRestrictions[i].toLowerCase()) {
                        skipProp = true;
                    }
                }
                if (!skipProp && self.accordion.layerAttributeRestrictions[self.resultSet.url] != undefined) {
                    for (var i = 0; i < self.accordion.layerAttributeRestrictions[self.resultSet.url].length; i++) {
                        if (key.toLowerCase() == self.accordion.layerAttributeRestrictions[self.resultSet.url][i].toLowerCase()) {
                            skipProp = true;
                        }
                    }
                }
                if (skipProp) {
                    continue;
                }
                if (array.indexOf(excludes, key) === -1) {
                    var span = domConstruct.create("span", { innerHTML: key + ": " }),
                        value = Common.getHyperlink(key, item[key]);
                    if (typeof value !== "object")
                        span.innerHTML = span.innerHTML + value;
                    else {
                        domConstruct.place(value, span);
                    }
                    // Add a break tag
                    domConstruct.create("br", null, span);
                    infoContent.push(span);
                    if (i === 0 && !options.sort) {
                        firstKey = lang.clone(span);
                        i++;
                    }

                }
            }

            var contentDiv = domConstruct.create("div", { "class": "colspan10" }, container);  //, innerHTML: item[displayField] + " <br />" + firstKey             
            domConstruct.place(firstKey, contentDiv);
            var link = domConstruct.create("a", { innerHTML: "more info...", style: "display:block;cursor:pointer;" }, contentDiv);


            var zoomDiv = domConstruct.create("div", { "class": "colspan1" }, container);
            // Zoom Icon
            var img = domConstruct.create("img", { src: require.toUrl('./images/zoom.gif'), title: "Zoom To", style: "cursor:pointer;" }, zoomDiv);
            on(img, "click", function (evt) {
                self.accordion._zoomToFeature(feature);
            });


            var infoDiv = domConstruct.create("div", { style: "display:none;overflow:scroll;max-height:300px;float:left;width:100%;" }, container);
            array.forEach(infoContent, function (ic) {
                domConstruct.place(ic, infoDiv);
            });

            // Append Related Data
            if (self.resultSet.relationships && self.resultSet.relationships.length) {
                domConstruct.create("span", { innerHTML: "<br/><strong>Related Data</strong><br/>" }, infoDiv);
                array.forEach(self.resultSet.relationships, function (r) {
                    var relLink = domConstruct.create("span", { "class": "action-link", innerHTML: r.name + "<br/>" }, infoDiv);
                    on(relLink, "click", function () {
                        // create relationship query
                        var rQuery = new RelationshipQuery();
                        rQuery.outFields = ["*"];
                        rQuery.relationshipId = r.id;
                        rQuery.returnGeometry = true;
                        rQuery.objectIds = [item[r.keyField] || item["OBJECTID"]];

                        // query related features
                        var u = new esriUtilities();
                        u.getRelatedFeaturesFromEndpoint(self.resultSet.url, rQuery,
                            function (relatedFeatures) {
                                var count = 0;
                                for (var key in relatedFeatures) {
                                    count++;
                                    var fs = relatedFeatures[key];
                                    var adapter = new SearchResultsAdapter();
                                    var rUrl = self.resultSet.url.replace(/\/[0-9]+/g, "") + "/" + r.relatedTableId;
                                    adapter.featureSetToResultSet(fs, rUrl).then(function (searchResult) {
                                        var rc = wvsConfig.defaults.mapViewerController.getWidget(ResultsConsole, "bottom", { bindToStore: false });
                                        rc.clear();
                                        rc.add(searchResult);
                                    });
                                }
                                if (count === 0)
                                    Common.errorDialog("No related data was found for this record.", "Oops");
                            }
                            , function (err) {
                                Common.errorDialog(err.message);
                            });
                    });
                });

            }


            var toggler = new Toggler({
                node: infoDiv,
                showFunc: coreFx.wipeIn,
                hideFunc: coreFx.wipeOut
            });

            var opened = false;
            on(link, "click", function (evt) {
                if (opened) {
                    toggler.hide();
                    link.innerHTML = "more info...";
                }
                else {
                    toggler.show();
                    link.innerHTML = "less info...";
                }
                opened = !opened;
            });

            return container;
        },
        selectAll: function () {
            var features = this._idToFeatureMap;

            // Mark all graphics as checked
            for (var key in features) {
                features[key]._checked = true;
            }

            // Refresh list
            this.list.refresh();
        },
        _createActionMenu: function (list, containingNode) {
            // summary:
            //      Creates an 'action bar' for a given grid
            // returns:
            //      undefined

            var selectDropDown = new DropDownMenu({ style: "display:none;" });

            // Select/highlight all features for the given list (within _selectAllLimit)
            var selectAll = new MenuItem({
                label: "All",
                onClick: lang.hitch(this, function () {
                    this.selectAll();
                })
            });

            selectDropDown.addChild(selectAll);

            // Clear/unhighlight all features for the given list
            var clearSelection = new MenuItem({
                label: "None",
                onClick: lang.hitch(this, function () {
                    var features = this._idToFeatureMap;

                    // Mark all graphics as checked
                    for (var key in features) {
                        features[key]._checked = false;
                    }

                    list.refresh();
                })
            });

            selectDropDown.addChild(clearSelection);

            var selectDropDownButton = new DropDownButton({
                dropDown: selectDropDown,
                label: "Select"
            });

            selectDropDownButton.placeAt(containingNode);
            //domConstruct.place(selectDropDownButton.domNode, containingNode);

            if (this.exportMenu) {

                // Export menu
                var dropdownMenu = new DropDownMenu({ style: "display:none;" });

                dropdownMenu.addChild(new MenuItem({
                    label: "Export All",
                    onClick: lang.hitch(this, function () {
                        var self = this;
                        var features = array.map(this.list.store.data, function (item) {
                            var feature = self._idToFeatureMap[item.__accordionId];
                            return new Graphic(feature.geometry, feature.symbol, feature.attributes);
                        });
                        Common.showWidgetInDialog(FormatExport, { features: features });
                    })
                }));

                // Export Selected
                dropdownMenu.addChild(new MenuItem({
                    label: "Export Selected",
                    onClick: lang.hitch(this, function () {
                        var self = this;
                        var features = array.map(array.filter(this.list.store.data, function (i) {
                            return self._idToFeatureMap[i.__accordionId]._checked;
                        }), function (item) {
                            var feature = self._idToFeatureMap[item.__accordionId];
                            return new Graphic(feature.geometry, feature.symbol, feature.attributes);// esriLang.filter(item.resultGraphic.attributes, function(value) { return typeof value !== "object";}));
                        });
                        if (features.length)
                            Common.showWidgetInDialog(FormatExport, { features: features });
                        else
                            Common.errorDialog("Please make a selection");
                    })
                }));


                var dropdownButton = new DropDownButton({
                    dropDown: dropdownMenu,
                    label: "Export"
                });
                dropdownButton.placeAt(containingNode);

            }
        },
        _listRefresh: function (evt) {
            var features = this._idToFeatureMap;

            // Mark all graphics as checked
            for (var key in features) {
                var feature = features[key];
                this.accordion._toggleGeometryHighlight(feature, typeof feature._checked !== "undefined" ? feature._checked : false);
            }
        }
    });

    Accordion.AccordionResult = _AccordionResult;

    return Accordion;
});