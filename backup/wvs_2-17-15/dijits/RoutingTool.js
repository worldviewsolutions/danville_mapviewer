﻿// comment for test
console.log("wvs/dijits/RoutingTool.js");

define([
      "dojo/_base/declare",
      "dojo/_base/array",
      "dojo/dom-attr",
      "dojo/dom-style",
      "dojo/dom",
      "dojo/on",
      "dojo/Evented",
      "dijit/_WidgetBase",
      "dijit/_TemplatedMixin",
      "dijit/_WidgetsInTemplateMixin",
      "dojo/text!./templates/RoutingTool.html",
      "dojo/_base/lang",
      "dojo/dom-construct",
      "esri/request",
      "wvs/libs/proj4js/main",
      "wvs/common/Common"
], function (
        declare,
        array,
        domAttr,
        domStyle,
        dom,
        on,
        Evented,
        _WidgetBase,
        _TemplatedMixin,
        _WidgetsInTemplateMixin,
        template,
        lang,
        domConstruct,
        esriRequest,
        Proj4js,
        Common
    ) {
    // module:
    //      wvs/dijits/ZoomToXY
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, Evented], {

        templateString: template,

        declaredClass: "wvs.dijits.RoutingTool",

        //Used to identify the geolocation 'watch' operation so that it can be cleared when a sufficiently accurate position is returned.
        _currentWatchId: -1,

        //originCoords: Object
        //  {lat, lon} object representing the lat/long coords for the starting point of the route task.
        originCoords: null,

        //destCoords: Object
        //  {lat, lon} object representing the lat/long coords for the destination point of the route task.
        destCoords: null,

        //originAddress: String
        //  The address of the starting point for the route task. Will be geocoded and transformed into originCoords
        originAddress: null,

        //destAddress: String
        //  The address of the destination point to be geocoded and transformed into destCoords
        destAddress: null,

        //Used to define the maximum range for what would be considered a useable location for the device.
        //Any higher values will be thrown out and the 'watchPosition' cycle will continue.
        maximumValidAccuracy: 50,

        _units: null,

        //default marker for display of point
        _marker: null,

        //default spatial reference for the XY coordinates when a new point object is created.
        _spatRef: null,

        map: null,

        /*
            @param params hash of properties to be mixed in to the widget.
        */
        constructor: function (params, srcNodeRef) {

            lang.mixin(this, {
                _eventHandles: [],
                nodes: []
            });

            params = params || {};
            if (!params.map) {
                throw new Error('Map not defined in params for Routing Tool');
            }

            if (!navigator.geolocation) {
                domConstruct.destroy(this.useLocationBtn);
            }

            this.map = params.map;
            this._spatRef = this.map.spatialReference;
            lang.mixin(this, params);

        },
        postCreate: function () {
            var self = this;
        },

        _getCurrentLocation: function () {
            this._currentWatchId = navigator.geolocation.watchPosition(this._locationRetrieved.bind(this), this._locationError, { enableHighAccuracy: true });
        },

        _locationRetrieved: function (pos) {
            if (pos.coords.accuracy <= this.maximumValidAccuracy) {
                navigator.geolocation.clearWatch(this._currentWatchId);
            }
        },

        _toggleInputType: function (evt) {
            console.warn(evt);
        },

        _locationError: function (err) {
            Common.errorDialog(err, "Location Error");
        }

    });
});