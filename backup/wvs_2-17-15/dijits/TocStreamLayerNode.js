﻿console.log("wvs/dijits/TocStreamLayerNode.js");

define([
    "dojo/_base/declare"
    , "dojo/text!./templates/_TocNode.html"
    , "dojo/_base/lang"
    , "dojo/_base/array"
    , "dojox/gfx"
    , "./_TocNode"
    , "dijit/form/HorizontalSlider"
    , "dojo/dom-style"
    , "dojo/dom-construct"
    , "dojo/dom-attr"
    , "dojo/on"
    , "dijit/Menu"
    , "dijit/MenuItem"
    , "dijit/MenuSeparator"
    , "dijit/PopupMenuItem"
    , "dijit/TooltipDialog"
    , "dojo/dom-class"
    , "dojo/has"
    , "dojo/dom"
    , "./Filter/FilterManager"
    , "dojo/_base/sniff"
], function (declare, template, lang, array, gfx, _TocNode, HorizontalSlider, domStyle, domConstruct, domAttr, on, Menu, MenuItem, MenuSeparator, PopupMenuItem, TooltipDialog, domClass, has, dom, FilterManager) {
    return declare([_TocNode], {

        declaredClass: 'wvs.dijits.TocStreamLayerNode',

        templateString: template,

        // refreshDelay: Integer
        //      Number of milliseconds to delay between clicking a checkbox and refreshing the map service. A longer delay allow the user to check/uncheck multiple
        //      layers before refreshing the map service."dojo/ready",
        refreshDelay: 500,

        // useSpriteImage: Boolean
        //      Use the custom sprite image for the legend icons. When false, the "Legend" REST service will be used for 10.01 and newer services.
        useSpriteImage: true,

        filtersEnabled: true,

        constructor: function (params, srcNodeRef) {
            params = params || {};
            if (!params.mapService) {
                throw new Error('mapService not defined in params for TocFeatureLayerNode');
            }
            if (!params.featureLayerType) {
                throw new Error('featureLayerType not defined in params for TocFeatureLayerNode');
            }

            lang.mixin(this, params);

            if (this.title === null) {
                var titleStrings = {
                    esriGeometryPolygon: 'polygon',
                    esriGeometryPolyline: 'polyline',
                    esriGeometryPoint: 'point'
                };
                if (this._isPartOfGroupLayer() && titleStrings[this.mapService.geometryType]) {
                    this.title = this._parent.title + "_" + titleStrings[this.mapService.geometryType];
                }
                else {
                    // call the extension method in jstoolkit/layers/Layer.js to get the service name
                    this.title = this.mapService.getServiceName();
                }
            }

            this.minScale = this.mapService.minScale;
            this.maxScale = this.mapService.maxScale;

            this.hasDropDown = !this._isPartOfGroupLayer();

        },
        // extension point called by framework
        postCreate: function () {
            this.inherited(arguments);

            // Initialize layer defaults to tie to the TOC node (i.e. opacity)
            var transparency = (1 - this.mapService.opacity).toPrecision(1);
            this.set("transparency", transparency || 1);

            if (this.hasDropDown)
                this._createContextMenu();

            this._createIcon(this.iconNode);
        },
        _hideSwatch: function () {
            // Empty - not implemented to override icon hiding
        },
        _showSwatch: function () {
            // Empty - not implemented to override icon hiding
        },
        setVisible: function (visible) {
            console.log(this.declaredClass + ' setVisible');
            // return if the visibility has not changed
            if (this._state.visible == visible) return;

            this._state.visible = visible;

            // update the map service
            if (visible) {
                this.mapService.show();
                this._showSwatch();
            } else {
                this.mapService.hide();
                this._hideSwatch();
            }
        },
        _checkboxOnClick: function (e) {
            console.log(this.declaredClass + ' _checkboxOnClick');
            this.setVisible(this._checkbox && this._checkbox.checked);
            this._toggleCheckbox();
        },
        _isPartOfGroupLayer: function () {
            var groupLayerTypes = ["georss", "kml", "group"];
            return array.some(groupLayerTypes, lang.hitch(this, function (glType) { return this.featureLayerType === glType; }))
        },
        _refreshLayer: function () {
            if (this._refreshTimer) {
                window.clearTimeout(this._refreshTimer);
                this._refreshTimer = null;
            }

            var mapService = this.mapService;

            this._refreshTimer = window.setTimeout(function () {
                mapService.refresh();
            }, this.refreshDelay);
        },
        _createIcon: function (parentNode) {
            if (this.mapService.renderer && this.mapService.renderer.symbol.type === "picturemarkersymbol") {
                domConstruct.create("img", { src: this.mapService.renderer.symbol.url }, parentNode);
                domClass.remove(parentNode, "jstoolkit-hidden");
            } else if (this.mapService.renderer && this.mapService.renderer.symbol.type === 'simplemarkersymbol') {
                var w = 20,
                    h = 20,
                    symbolDiv = domConstruct.create("div", { display: "inline" }),
                    surface = gfx.createSurface(symbolDiv, w, h),
                    symbol = this.mapService.renderer.symbol;

                if (this.mapService.geometryType === "esriGeometryPolygon") {
                    var rect = surface.createRect({ x: 0, y: 0, width: w, height: h });
                    rect.setFill(symbol.getFill());
                    rect.setStroke(symbol.getStroke());
                }
                else if (this.mapService.geometryType === "esriGeometryPolyline" && !has("ie") || has("ie") > 8) {
                    var line = surface.createLine({ x1: 0, y1: h / 2, x2: w, y2: h / 2 });
                    line.setStroke(symbol.getStroke());
                }
                else if (this.mapService.geometryType === "esriGeometryPoint") {
                    // TODO: Finish implemnting point
                    var offset = ((h / 2) > (symbol.size * 1.414)) ? (symbol.size * 1.414) : (h / 2),
                        radius = ((h / 2) > (symbol.size / 2)) ? (symbol.size / 2) : (h / 2);
                    var circle = surface.createCircle({ cx: offset, cy: offset, r: radius });
                    circle.setFill(symbol.getFill());
                    circle.setStroke(symbol.getStroke());
                }
                domConstruct.place(symbolDiv, this.iconNode);
                domClass.remove(parentNode, "jstoolkit-hidden");
            }
        },
        _createContextMenu: function () {
            console.log(this.declaredClass + ' _createContextMenu');
            var self = this;
            if (this._isPartOfGroupLayer()) {
                return;
            }

            var mapServiceOptionsMenu = new Menu({
                leftClickToOpen: true,
                onFocus: this.isFromWebMap ? function () { } : function () {
                    var children = this.getChildren(),
                        nodeBottom = self.toc.isBottomNode(self),
                        nodeTop = self.toc.isTopNode(self);
                    array.forEach(children, function (entry, i) {
                        if (entry.label) {
                            switch (entry.label) {
                                case "Move Up":
                                    entry.set("disabled", nodeTop); break;
                                case "Move Down":
                                    entry.set("disabled", nodeBottom); break;
                            }
                        }
                    });
                }
            });
            mapServiceOptionsMenu.bindDomNode(self.dropDownMenuIcon);

            // Transparency
            mapServiceOptionsMenu.addChild(_TocNode.ContextMenuItems.Transparency(this));


            if (!this.isFromWebMap) {
                mapServiceOptionsMenu.addChild(new MenuSeparator());

                // Remove
                mapServiceOptionsMenu.addChild(_TocNode.ContextMenuItems.Remove(this));
            }

            if (!this.isFromWebMap && this.mapService.url) {


                mapServiceOptionsMenu.addChild(new MenuItem({
                    label: "Filter",
                    iconClass: "icon-filter",
                    onClick: function () {
                        var fm = new FilterManager({
                            mapService: self.mapService
                        });
                        fm.startup();
                        fm.dialog.show();
                    }
                }));
            }

            /*
            NOTES: For the time being, Feature layers, GeoRSS layers, and KML layers will be at the top of the TOC and not movable.
            This is due to esri's convoluted way of storing and identifying layers in the map.
            
            var _moveUpMenuItem = new MenuItem({
                label: "Move Up",
                iconClass: "jstoolkit-icon jstoolkit-icon-arrow-up",
                onClick: lang.hitch(self.toc, self.toc._moveNodeUp, self)
            });

            mapServiceOptionsMenu.addChild(_moveUpMenuItem);

            var _moveDownMenuItem = new MenuItem({
                label: "Move Down",
                iconClass: "jstoolkit-icon jstoolkit-icon-arrow-down",
                onClick: lang.hitch(self.toc, self.toc._moveNodeDown, self)
            });
            mapServiceOptionsMenu.addChild(_moveDownMenuItem);
            */

            mapServiceOptionsMenu.startup();
            this.mapServiceOptionsMenu = mapServiceOptionsMenu;
        }

    });
});