﻿define([
    "dojo/_base/declare"
    , "dojo/_base/lang"
    , "dojo/_base/array"
    , "dijit/_WidgetBase"
    , "dijit/form/ComboBox"
    , "dojo/store/Memory"
    , "dojo/promise/all"
    , "esri/geometry/Extent"
    , "esri/request"
    , "dojo/Evented"
    , "dojo/on"
    // esri layers
    , "esri/layers/OpenStreetMapLayer"
    , "esri/layers/WebTiledLayer"
    , "esri/layers/ArcGISImageServiceLayer"
    , "esri/layers/ImageServiceParameters"
    , "esri/layers/ArcGISDynamicMapServiceLayer"
    , "esri/layers/ArcGISTiledMapServiceLayer"
    , "esri/virtualearth/VETiledLayer"
    , "wvs/extensions/esri/layers/GoogleMapsLayer"
    , "wvs/common/bingMapsUtil"
], function (
    declare
    , lang
    , array
    , _WidgetBase
    , ComboBox
    , Memory
    , all
    , Extent
    , esriRequest
    , Evented
    , on
    // esri layers
    , OpenStreetMapLayer
    , WebTiledLayer
    , ArcGISImageServiceLayer
    , ImageServiceParameters
    , ArcGISDynamicMapServiceLayer
    , ArcGISTiledMapServiceLayer
    , VETiledLayer
    , GoogleMapsLayer
    , bingMapsUtil
) {


    return declare([_WidgetBase, Evented], {

        declaredClass: "wvs.dijits.BasemapDropdown",

        gLayer: null,
        googleActive: false,
        globalZoomDuration: esriConfig.defaults.map.zoomDuration,
        globalZoomRate: esriConfig.defaults.map.zoomRate,
        googleZoomRate: 1,
        googleZoomDuration: 1,
        zoomStartHandler: null,
        zoomEndHandler: null,

        // bingMapsKey: String
        //      the API key for Bing maps
        bingMapsKey: null,

        // loaded: Boolean
        //      signifies whether or not the dropdown is loaded
        loaded: false,

        // _selectBasemapInProgress: Boolean
        //      signifies whether a basemap select is currently in progress
        _selectBasemapInProgress: false,

        constructor: function (params, srcNodeRef) {
            this.inherited(arguments);
            if (!params.map) {
                throw new Error("map required for " + this.declaredClass);
            }

            lang.mixin(this, params);

            // Assign our ids, if needed
            if (this.basemaps) {
                array.forEach(this.basemaps, function (basemap) {
                    if (!basemap.id || basemap.id.length === 0)
                        basemap.id = this._getUniqueId(this.basemaps);
                    array.forEach(basemap.layers, function (basemapLayer) {
                        basemapLayer.opacity = basemapLayer.opacity >= 0 ? basemap.opacity : 1;
                        basemapLayer.visibility = true;
                    }, this);
                }, this);
            }

            if (this.google) {
                //this.basemaps.add(this.google.items);
                this.basemaps.push.apply(this.basemaps, this.google.items);
                console.debug(this.basemaps);
            };


            // Declare instance variables
            // basemaps: Memory
            //      A basemap store
            this.basemaps = new Memory({ data: this.basemaps ? this.basemaps : [], idProperty: "id" });
            delete params.basemaps;

            // basemapIds: String[]
            //      List of basemap layer ids in the current map. These layers will be removed when switching to a new basemap.
            this.basemapIds = params.basemapIds ? params.basemapIds : [];
            delete params.basemapIds;

            // referenceIds: String[]
            //      List of reference layer ids in the current map. These layers will be removed when switching to a new basemap.
            this.referenceIds = params.referenceIds ? params.referenceIds : [];
            delete params.referenceIds;

            this.init();
        },
        postCreate: function(){
            // Add our select
            this.basemapSelect = new ComboBox({
                store: this.basemaps,
                autoComplete: false,
                autoWidth: true,
                searchAttr: "title",
                onChange: lang.hitch(this, this._selectOnChange),
                style: "width:200px"
            });
            this.basemapSelect.startup();
            this.basemapSelect.placeAt(this.domNode);

            //require(["dijit/form/CheckBox", "dojo/domReady!"], function (CheckBox) {
            //    this.googleTrafficCheckbox = new CheckBox({
            //        name: "chkGoogleTraffic",
            //        value: "agreed",
            //        checked: false,
            //        onChange: function (b) { alert('onChange called with parameter = ' + b + ', and widget value = ' + this.get('value')); }
            //    }, "checkBox");

            //    this.googleTrafficCheckbox.startup();
            //    this.googleTrafficCheckbox.placeAt(this.domNode, "last");
            //});


            // Signifiy we are loaded and set our selected basemap (first basemap in array)
            this.loaded = true;
            this.emit("load");



            this.basemapSelect.set("value", this.basemaps.data[0].title);
            if ((this.map.layerIds.length === 0 || this.basemaps.data[0].id.toLowerCase().indexOf("google") > -1) && this.basemaps.data.length > 0 && !this._selectBasemapInProgress) {
                var _firstInit = lang.hitch(this, function () {
                    this._select(this.basemaps.data[0].id);
                });
                if (this.map.loaded) {
                    _firstInit();
                } else {
                    on.once(this.map, "load", _firstInit);
                }
            }
        },
        init: function () {
            array.forEach(this.basemapIds, function (basemapId) {
                this.map.getLayer(basemapId)._basemapGalleryLayerType = "basemap";
            }, this);

            array.forEach(this.referenceIds, function (referenceId) {
                this.map.getLayer(referenceId)._basemapGalleryLayerType = "reference";
            }, this);
        },
        add: function (basemap) {
            // summary:
            //      Add a new basemap to the dropdown
            if (basemap) {
                if (!basemap.id) {
                    basemap.id = this._getUniqueId();
                    this.basemaps.put(basemap);
                    return true;
                }
                else if(this._isUniqueId(basemap.id)) {
                    this.basemaps.put(basemap);
                    return true;
                }
                else
                    return false;
            }
            return false;
        },
        getBasemap: function (id) {
            // summary:
            //      gets a basemap by its id
            return this.basemaps.get(id);
        },
        getSelected: function () {
            // summary:
            //      gets the currently selected basemap
            return this.basemapSelect.item;
        },
        remove: function (id) {
            // summary:
            //      removes a basemap by id
            this.basemaps.remove(id);
        },
        select: function (id) {
            // summary:
            //      selects a basemap to use on the map
            this._select(id);
        },
        _select: function(id){
            // summary:
            //      selects a basemap to use on the map
            //alert(id);

            this._selectBasemapInProgress = true;
            var basemap = this.getBasemap(id);
            // TODO: Change the way this works. Right now, it is ID-based.
            if (basemap.id.toLowerCase().indexOf("google") === -1 && basemap.title.toLowerCase().indexOf("bing") === -1){
                this._getServiceInfos(basemap);
            } else {
                //for google and bing
                this._switchBasemap(basemap);
            }
            this._selectBasemapInProgress = false;
        },

        _selectOnChange: function (value) {
            if (this.basemapSelect.item) {
                this.select(this.basemapSelect.item.id);
            }
        },
        _getServiceInfos: function(basemap){
            var serviceInfos = [];
            array.forEach(basemap.layers, function(basemapLayer){
                if(basemapLayer.url && basemapLayer.url.length > 0 && !basemapLayer.isReference){
                    basemapLayer.deferredsPos = serviceInfos.length;
                    serviceInfos.push(this._getServiceInfo(basemapLayer.url));
                }
            }, this);
            if (serviceInfos.length > 0) {
                all(serviceInfos).then(lang.hitch(this, function (results) {
                    var fullExtent = null;
                    array.forEach(basemap.layers, function (basemapLayer) {
                        if (basemapLayer.deferredsPos === 0 || basemapLayer.deferredsPos) {
                            basemapLayer.serviceInfoResponse = results[basemapLayer.deferredsPos];
                            var extent = basemapLayer.serviceInfoResponse.fullExtent;
                            if (!extent)
                                extent = basemapLayer.serviceInfoResponse.extent;
                            fullExtent = !fullExtent ? new Extent(extent) : fullExtent.union(new Extent(extent));
                        }
                    }, this);

                    if (this.map.extent && this._getIntersectionPercent(fullExtent, this.map.extent) < 5) {
                        this.map.setExtent(fullExtent, true);
                    }
                    this._switchBasemap(basemap);
                    this._updateReferenceLayer(basemap);
                }));
            }
        },
        _getIntersectionPercent: function(a, b){
            var intersectExtent = b.intersects(a);
            if(intersectExtent){
                var c = intersectExtent.getWidth() * intersectExtent.getHeight(),
                    d = b.getWidth() * b.getHeight();
                return 100 * ( c / d );
            }
            return 0;
        },
        preGoogleAnimation: function () {
            this.hiddenLayers = [];
            //esriConfig.defaults.map.panDuration = 1; // time in milliseconds, default panDuration: 250
            //esriConfig.defaults.map.panRate = 1; // default panRate: 25

            array.forEach(this.map.getLayersVisibleAtScale(), function (layer) {
                
                //TODO: fix this to turn off everything but google
                //if (layer.declaredClass !== "wvs.layers.GoogleMapsLayer" && layer.visible) {
                if (layer.declaredClass == "esri.layers.ArcGISDynamicMapServiceLayer" && layer.visible) {
                    console.debug("turning off..."+layer.declaredClass);
                    layer.hide();
                    this.hiddenLayers.push(layer);
                }
            }, this);
        },

        postGoogleAnimation: function() {
            array.forEach(this.hiddenLayers, function (layer) {
                //alert("turning on..." + layer.declaredClass);
                layer.show();
            });
        },

        //GoogleMapsLayer.MAP_TYPE_ROADMAP | MAP_TYPE_HYBRID | MAP_TYPE_SATELLITE | MAP_TYPE_TERRIAN
        _getGoogleMapType: function (param) {
            param = param.toLowerCase();

            if (param.indexOf("hybrid") > -1) {
                return GoogleMapsLayer.MAP_TYPE_HYBRID;
            }
            if (param.indexOf("terrain") > -1) {
                return GoogleMapsLayer.MAP_TYPE_TERRAIN;
            }
            if (param.indexOf("satellite") > -1 || param.indexOf("aerial") > -1 || param.indexOf("imagery") > -1) {
                return GoogleMapsLayer.MAP_TYPE_SATELLITE;
            }
            return GoogleMapsLayer.MAP_TYPE_ROADMAP;
            //GoogleMapsLayer.MAP_TYPE_ROADMAP | MAP_TYPE_HYBRID | MAP_TYPE_SATELLITE | MAP_TYPE_TERRIAN
        },

        _switchBasemap: function (basemap) {
            //alert(JSON.stringify(basemap));

            console.log("_switchBasemap()");

            if (basemap.id.toLowerCase().indexOf("google") > -1) {
                //alert("google");
                //if (this._failsTiledLayerRestriction()) {
                //    throw new Error("This new basemap is a GoogleMapsLayer tiled service and cannot be loaded as a dynamic layer");
                //}
                console.log("basemap = ", basemap);
                if (this.gLayer == null) {
                    this.gLayer = new GoogleMapsLayer({
                        id: "googleLayer",
                        apiOptions: { libraries: 'places', streetViewControl: false },
                        //disable 45deg imagery by default
                        mapOptions: { tilt: 0 },
                        googleLayerControls: basemap.googleLayerControls || null
                    });
                }
                

                if (!this.googleActive) {
                    // remove existing basemap layers
                    this._removeBasemapLayers();

                    //add the actual layer
                    this.map.addLayer(this.gLayer, 0);


                    //this.map.reorderLayer("googleLayer", 1);
                    this.googleActive = true;
                    //get rid of annoying delay in zooming b/w we're binding to zoom-end
                    esriConfig.defaults.map.zoomDuration = this.googleZoomDuration;
                    esriConfig.defaults.map.zoomRate = this.googleZoomRate;

                    var me = this;

                    require(["dojo/on"], function (on) {
                        me.zoomStartHandler = on(this.map, "zoom-start", function (evt) {
                            console.log("zoom-start");
                            me.preGoogleAnimation();
                        });
                        me.panStartHandler = on(this.map, "pan-start", function (evt) {
                            //alert("panStart");
                            me.preGoogleAnimation();
                            console.log("pan-start");
                        });

                        me.zoomEndHandler = on(this.map, "zoom-end", function (evt) {
                            console.log("zoom-end");
                            //revert these to previous settings
                            me.postGoogleAnimation();
                        });
                        me.panEndHandler = on(this.map, "pan-end", function (evt) {
                            console.log("pan-end");
                            //revert these to previous settings
                            me.postGoogleAnimation();
                        });
                    });
                }

                this.gLayer.setMapTypeId(this._getGoogleMapType(basemap.id));
                return;
            }

            if (this.googleActive) {
                map.removeLayer(this.gLayer);
                this.zoomStartHandler.remove();
                this.zoomEndHandler.remove();
                this.panStartHandler.remove();
                this.panEndHandler.remove();

                //revert these
                esriConfig.defaults.map.zoomDuration = this.globalZoomDuration;
                esriConfig.defaults.map.zoomRate = this.globalZoomRate;


                this.googleActive = false;
            }

            var basemapLayers = basemap.layers;
            if (this._failsTiledLayerRestriction() && basemapLayers[0].type && (basemapLayers[0].type === "OpenStreetMap" || basemapLayers[0].type.indexOf("BingMaps") > 0 || basemapLayers[0].type[0] === "WebTiledLayer")) {
                throw new Error("This new basemap is a tiled service and cannot be loaded as a dynamic layer");
            } else {
                // Check that we have a Bing Maps API key
                array.forEach(basemapLayers, function (basemapLayer) {
                    if (!basemapLayer.isReference && basemapLayer.type && basemapLayers[0].type.indexOf("BingMaps") > 0 && !this.bingMapsKey) {
                        throw new Error("Invalid Bing Maps key");
                    }
                }, this);

                // remove existing basemap layers
                this._removeBasemapLayers();

                // start adding at index 0
                var layerIndex = 0;

                array.forEach(basemapLayers, function (basemapLayer) {
                    // alert(basemapLayer.id);

                    if (!basemapLayer.isReference) {
                        var layer;

                        if (basemapLayer.type === "OpenStreetMap") {
                            if (this._failsTiledLayerRestriction()) {
                                throw new Error("This new basemap is a tiled service and cannot be loaded as a dynamic layer");
                            }
                            layer = new OpenStreetMapLayer({
                                id: "osm_layer",
                                opacity: basemapLayer.opacity
                            });
                        }
                        else if (basemapLayer.type === "WebTiledLayer") {
                            if (this._failsTiledLayerRestriction()) {
                                throw new Error("This new basemap is a tiled service and cannot be loaded as a dynamic layer");
                            }
                            layer = new WebTiledLayer(basemapLayer.url, {
                                visible: basemapLayer.visibility,
                                opacity: basemapLayer.opacity,
                                copyright: basemapLayer.copyright,
                                fullExtent: basemapLayer.fullExtent,
                                initialExtent: basemapLayer.initialExtent || basemapLayer.fullExtent,
                                subDomains: basemapLayer.subDomains,
                                tileInfo: basemapLayer.tileInfo,
                                tileServers: basemapLayer.tileServers
                            });
                        } else if (basemapLayer.type && basemapLayer.type.indexOf("BingMaps") > -1) {
                            if (this._failsTiledLayerRestriction()) {
                                throw new Error("This new basemap is a tiled service and cannot be loaded as a dynamic layer");
                            }
                            layer = bingMapsUtil.getBingMapFromType(basemapLayer.type);
                        } else if (basemapLayer.declaredClass && basemapLayer.declaredClass.toLowerCase().indexOf("vetiledlayer") > -1) {
                            if (this._failsTiledLayerRestriction()) {
                                throw new Error("This basemap is a tiled service and cannot be loaded as a dynamic layer");
                            }
                            layer = new VETiledLayer({
                                bingMapsKey: basemapLayer.bingMapsKey,
                                mapStyle: VETiledLayer[basemapLayer.mapStyle]
                            });
                        } else if (basemapLayer.serviceInfoResponse && basemapLayer.serviceInfoResponse.mapName) {
                            var emptyMapOrHasLevels = this.map.layerIds.length === 0 || this.map.getNumLevels() > 0;
                            if (emptyMapOrHasLevels && basemapLayer.serviceInfoResponse.singleFusedMapCache)
                                layer = this._loadAsCached(basemapLayer)
                            else
                                layer = this._loadAsDynamic(basemapLayer);

                        } else if (basemapLayer.serviceInfoResponse && basemapLayer.serviceInfoResponse.pixelSizeX) {
                            var imageServiceParameters = new ImageServiceParameters();
                            imageServiceParameters.bandIds = basemapLayer.bandIds;
                            if (!imageServiceParameters.bandIds && basemapLayer.serviceInfoResponse.bandCount && 3 < parseInt(basemapLayer.serviceInfoResponse.bandCount)) {
                                imageServiceParameters.bandIds = [0, 1, 2];
                            }
                            layer = new ArcGISImageServiceLayer(basemapLayer.url, {
                                resourceInfo: basemapLayer.serviceInfoResponse,
                                opacity: basemapLayer.opacity,
                                visible: basemapLayer.visibility,
                                imageServiceParameters: imageServiceParameters
                            });
                        }

                        // Add the basemap
                        if (layer) {
                            layer._basemapGalleryLayerType = "basemap";
                            this.map.addLayer(layer, layerIndex);
                            layerIndex++;
                        }
                    }
                }, this);
            }

        },
        _loadAsCached: function (basemapLayer) {
            var levels = [];
            if(!basemapLayer.displayLevels){
                levels = array.map(basemapLayer.serviceInfoResponse.tileInfo.lods, function(lod){ return lod.level;});
            }
            return new ArcGISTiledMapServiceLayer(basemapLayer.url, {
                resourceInfo: basemapLayer.serviceInfoResponse,
                opacity: basemapLayer.opacity,
                visible: basemapLayer.visibility,
                displayLevels: basemapLayer.displayLevels || levels,
                resampling: typeof basemapLayer.resampling === "boolean" ? basemapLayer.resampling : false
            })
        },
        _loadAsDynamic: function (basemapLayer) {
            var layer = new ArcGISDynamicMapServiceLayer(basemapLayer.url, {
                resourceInfo: basemapLayer.serviceInfoResponse,
                opacity: basemapLayer.opacity,
                visible: basemapLayer.visibility
            });
            if(basemapLayer.visibleLayers)
                layer.setVisibleLayers(basemapLayer.visibleLayers);
            return layer;
        },
        _handleReferenceServiceInfoResponse: function (referenceLayer, response, ioArgs) {
            var basemapReferenceLayer;
            referenceLayer.serviceInfoResponse = response;
            if (response && response.mapName){
                if(response.singleFusedMapCache)
                    basemapReferenceLayer = this._loadAsCached(referenceLayer);
                else
                    basemapReferenceLayer = this._loadAsDynamic(referenceLayer);
            }
                // ArcGISImageServiceLayer
            else if(response && response.pixelSizeX){
                var imageServiceParameters = new ImageServiceParameters();
                imageServiceParameters.bandIds = referenceLayer.bandIds;
                if(!imageServiceParameters.bandIds && response.bandCount && 3 < parseInt(response.bandCount)){
                    imageServiceParameters.bandIds = [0,1,2];
                }
                basemapReferenceLayer = new ArcGISImageServiceLayer(referenceLayer.url, {
                    resourceInfo: referenceLayer.serviceInfoResponse,
                    opacity: referenceLayer.opacity,
                    visible: referenceLayer.visibility,
                    imageServiceParameters: imageServiceParameters
                });
            }
            if(basemapReferenceLayer){
                basemapReferenceLayer._basemapGalleryLayerType = "reference";
                this.map.addLayer(basemapReferenceLayer);
            }
        },
        _updateReferenceLayer: function(basemap){
            this._removeReferenceLayer();
            for( var i = 0; i < basemap.layers.length; i++){
                if(basemap.layers[i].isReference)
                    this._addReferenceLayer(basemap.layers[i]);
            }
        },
        _addReferenceLayer: function(referenceLayer){
            this._getServiceInfo(referenceLayer.url, lang.hitch(this, this._handleReferenceServiceInfoResponse, referenceLayer));
        },
        _removeReferenceLayer: function () {
            for (i = this.map.layerIds.length - 1; 0 <= i; i--) {
                var layer = this.map.getLayer(this.map.layerIds[i]);
                if(layer._basemapGalleryLayerType === "reference")
                    this.map.removeLayer(layer);
            }
        },
        _getServiceInfo: function (url, callback) {
            return esriRequest({
                url: url,
                content: {
                    f: "json"
                },
                callbackParamName: "callback",
                load: function (response, ioArgs) {
                    if (callback)
                        callback(response, ioArgs);
                },
                error: lang.hitch(this, function (response, ioArgs) {
                    throw new Error(this.declaredClass + ": service not accessible.");
                })
            });
        },
        _failsTiledLayerRestriction: function(){
            return this.map.layerIds.length > 0 && this.map.getNumLevels() === 0;
        },
        _getIds: function(basemaps){
            var ids = [],
                basemaps = basemaps || this.basemaps.data;
            array.forEach(basemaps, function(basemap){
                ids.push(basemap.id);
            });
            return ids;
        },
        _getUniqueId: function (basemaps) {
            basemaps = basemaps || this.basemaps.data;
            for (var basemapIds = "," + this._getIds(basemaps).toString() + ",", i = 0; ;) {
                if (basemapIds.indexOf(",basemap_" + i + ",") > -1)
                    i++;
                else
                    return "basemap_" + i;
            }
        },
        _isUniqueId: function (id, basemaps) {
            basemaps = basemaps || this.basemaps.data;
            return ("," + this._getIds().toString() + ",").indexOf("," + id + ",") === -1 ? true : false;
        },
        _removeBasemapLayers: function () {
            // summary:
            //      removes all basemap layers from the map
            var layers = this.map.layerIds.slice();
            array.forEach(layers, function (layer) {
                layer = this.map.getLayer(layer);
                if (layer._basemapGalleryLayerType === "basemap")
                    this.map.removeLayer(layer);
            }, this);
        }
    });

});