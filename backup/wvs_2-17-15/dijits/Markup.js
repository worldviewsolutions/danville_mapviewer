console.log("./Markup.js");

define([
        "dojo/_base/declare",
        "dojo/_base/lang",
        "dojo/_base/array",
        "dijit/_WidgetBase",
        "dijit/_TemplatedMixin",
        "dojo/text!./templates/Markup.html",
        "dojo/on",
        "esri/graphic",
        "dojo/dom-construct",
        "esri/toolbars/draw",
        "dijit/form/Button",
        "dijit/form/ToggleButton",
        "dijit/form/TextBox",
        "dijit/form/CheckBox",
        "dojo/dom-style",
        "esri/geometry/Polygon",
        "esri/geometry/Polyline",
        "esri/geometry/Point",
        "esri/geometry/Multipoint",
        "./GraphicEditor",
        "../extensions/esri/layers/MarkupLayer",
        "dojo/dom-class",
        "dojo/dom",
        "esri/symbols/jsonUtils",
        "esri/symbols/PictureMarkerSymbol",
        "esri/symbols/TextSymbol",
        "require",
        "./Buffer",
        "../common/Common",
        "../common/InfoTemplateManager",
        "../_base/config",
        "dojo/_base/connect",
        "../main"
],
    function (
        declare,
        lang,
        array,
        _WidgetBase,
        _TemplatedMixin,
        template,
        on,
        Graphic,
        domConstruct,
        Draw,
        Button,
        ToggleButton,
        TextBox,
        CheckBox,
        domStyle,
        Polygon,
        Polyline,
        Point,
        Multipoint,
        GraphicEditor,
        MarkupLayer,
        domClass,
        dom,
        jsonUtils,
        PictureMarkerSymbol,
        TextSymbol,
        require,
        Buffer,
        Common,
        InfoTemplateManager,
        wvsConfig,
        connect
     ) {
        return declare([_WidgetBase, _TemplatedMixin], {

            templateString: template,

            declaredClass: "wvs.dijits.Markup",

            constructor: function (params, srcNodeRef) {
                params = params || {};

                // Required params
                if (!params.map) {
                    throw new Error("map not defined in params for " + this.declaredClass);
                }
                if (!params.markupLayer) {
                    throw new Error("markup layer not defined in params for " + this.declaredClass);
                }

                lang.mixin(this, params);

                // Constants
                this.tools = [{ type: 'POLYLINE', label: 'Line', iconClass: 'icon-arrows-h' },
                    { type: 'POLYGON', label: 'Polygon', iconClass: '' },
                    { type: 'ICON', label: 'Icon', iconClass: '' },
                    { type: 'FREEHAND', label: 'Freehand', iconClass: '' },
                    { type: 'ARROW', label: 'Arrow', iconClass: '' },
                    { type: 'RECTANGLE', label: 'Rectangle', iconClass: '' },
                    { type: 'BUFFER', label: 'Buffer', iconClass: '' },
                    { type: 'TEXT', label: 'Text', iconClass: '' }
                    //{ value: 'CIRCLE', label: 'circle', iconClass: '' },
                    //{ value: 'DOWN_ARROW', label: 'down_arrow', iconClass: '' },
                    //{ type: 'ELLIPSE', label: 'Ellipse', iconClass: '' },
                    //{ value: 'EXTENT', label: 'Extent', iconClass: '' },
                    //{ value: 'FREEHAND_POLYGON', label: 'freehand_polygon', iconClass: '' },
                    //{ value: 'FREEHAND_POLYGON', label: 'freehand_polygon', iconClass: '' },
                    //{ value: 'FREEHAND_POLYLINE', label: 'freehand_polyline', iconClass: '' },
                    //{ value: 'LEFT_ARROW', label: 'left_arrow', iconClass: '' },
                    //{ value: 'MULTI_POINT', label: 'multi_point', iconClass: '' },
                    //{ type: 'POINT', label: 'Point', iconClass: '' },
                    //{ type: 'POLYLINE', label: 'Polyline', iconClass: '' },
                    //{ value: 'RIGHT_ARROW', label: 'right_arrow', iconClass: '' },
                    //{ value: 'TRIANGLE', label: 'triangle', iconClass: '' },
                    //{ value: 'UP_ARROW', label: 'up_arrow', iconClass: '' }
                ];


                // Instance variables
                this._drawToolbar = new Draw(this.map);
                this._activeTool = null;
            },
            postCreate: function () {
                var tools = this.tools,
                    self = this,
                    tool;

                this.toolButtons = [];

                this._drawToolbar.setFillSymbol(jsonUtils.fromJson(wvsConfig.defaults.symbols.polygon));
                this._drawToolbar.setLineSymbol(jsonUtils.fromJson(wvsConfig.defaults.symbols.line));
                this._drawToolbar.setMarkerSymbol(jsonUtils.fromJson(wvsConfig.defaults.symbols.point));

                for (var i = 0; i < tools.length; i++) {
                    tool = tools[i];
                    var btn = new ToggleButton({
                        label: tool.label,
                        value: tool.type,
                        onClick: function () { self.set("_activeTool", this.get("value")); },
                        onChange: function () {
                            // Don't allow toggle if there was no change
                            if (self.get("_activeTool") === this.get("value")) {
                                this.set("checked", true);
                            }
                        }
                    });
                    domClass.add(btn.domNode, wvsConfig.defaults.buttonWidgetClass);
                    this.toolButtons.push(btn);
                    btn.placeAt(this.buttonsNode);
                }

                this.textInput = new TextBox({ value: "", placeHolder: "Enter text here" }, this.textInput);

                this.freehandPolygonMode = new CheckBox({
                    checked: false,
                    onChange: function (checked) {
                        self.set("_activeTool", checked ? "FREEHAND_POLYGON" : "FREEHAND_POLYLINE");
                    }
                }, this.freehandPolygonMode);

                this.bufferTool = new Buffer({
                    map: this.map
                });
                this.bufferTool.placeAt(this.bufferOptions);

                this._createWatchesAndEvents();

                connect.publish("map-acquire", { target: this, acquired: true });
            },
            startup: function () {
                this.inherited(arguments);
                this.set("_activeTool", "POLYGON");
            },
            destroy: function () {
                for (var i = 0; i < this.map.graphicsLayerIds.length; i++) {
                    var lyr = this.map.getLayer(this.map.graphicsLayerIds[i]);
                    lyr.enableMouseEvents();
                }
                this._drawToolbar.deactivate();
                connect.publish("map-acquire", { target: this, acquired: false });
                if (this._graphicEditor) {
                    this._graphicEditor.destroy();
                }
                this.inherited(arguments);
            },
            _createWatchesAndEvents: function () {
                // summary:
                //      Bind events and watches for the Markup widget
                // returns:
                //      undefined

                // Drawing events
                this.own(on(this._drawToolbar, "draw-end", lang.hitch(this, this.addGraphic)));

                // Watches
                this.watch("_activeTool", lang.hitch(this, function (name, oldToolType, newToolType) {
                    array.forEach(this.toolButtons, function (toolButton) {
                        if (toolButton.get("value") != newToolType) {
                            toolButton.set("checked", false);
                        }
                        else {
                            toolButton.set("checked", true);
                        }

                    }, this);
                    this._drawToolbar.deactivate();
                    this.removeGraphic();

                    if (newToolType != "TEXT") {
                        domStyle.set(this.textOptions, { display: "none" });
                    }
                    else {
                        this.textInput.set("value", "");
                        domStyle.set(this.textOptions, { display: "block" });
                    }
                    if (newToolType != "BUFFER") {
                        domStyle.set(this.bufferOptions, { display: "none" });
                    }
                    else {
                        domStyle.set(this.bufferOptions, { display: "block" });
                    }
                    if (newToolType === "FREEHAND" || newToolType === "FREEHAND_POLYGON" || newToolType === "FREEHAND_POLYLINE") {
                        domStyle.set(this.freehandOptions, { display: "block" });
                    }
                    else {
                        domStyle.set(this.freehandOptions, { display: "none" });
                    }
                    this.activateTool(newToolType);
                }));
            },
            activateTool: function (toolType) {
                // summary:
                //      Activate the given tool by type
                // returns:
                //      undefined
                if (toolType === "TEXT") {
                    this._drawToolbar.activate(Draw.POINT);
                }
                else if (toolType === "ICON") {
                    this._drawToolbar.activate(Draw.POINT);
                }
                else if (toolType === "BUFFER") {
                    this._drawToolbar.activate(Draw.POINT);
                }
                else if (toolType === "FREEHAND") {
                    toolType = this.freehandPolygonMode.get("checked") ? "FREEHAND_POLYGON" : "FREEHAND_POLYLINE";
                    this._drawToolbar.activate(Draw[toolType]);
                }
                else {
                    this._drawToolbar.activate(Draw[toolType]);
                }

                //Make sure to disable mouse events for all markup layers when the tool is active
                for (var i = 0; i < this.map.graphicsLayerIds.length; i++) {
                    var lyr = this.map.getLayer(this.map.graphicsLayerIds[i]);
                    lyr.disableMouseEvents();
                }
            },

            addGraphic: function (evt) {
                // summary:
                //      event handler for the 'draw-end' event of the draw toolbar
                // returns:
                //      undefined
                var symbol,
                    geometry = evt.geometry,
                    self = this;
                if (!array.some(this.map.layerIds, function (graphicLayerId) { return (graphicLayerId === this.markupLayer.id); }, this)) {
                    var newLayer = false;
                    for (var i = 0; i < this.markupLayer.items.data.length; i++) {
                        if (this.markupLayer.items.data[i].graphic) {
                            newLayer = true;
                            break;
                        }
                    }
                    if ((this.markupLayer.title != 'Temporary Markup' && this.markupLayer._id != null) || newLayer) {
                        this.markupLayer = new MarkupLayer({ json: [{ id: 'root', parent: null, name: 'Temporary Markup', type: 'folder' }] });
                    }
                    this.map.addLayer(this.markupLayer);
                }
                if (geometry.isInstanceOf(Point) || geometry.isInstanceOf(Multipoint)) {
                    if (this._activeTool === "TEXT") {
                        symbol = new TextSymbol(this.textInput.get("value") ? this.textInput.get("value") : "new graphic");
                    }
                    else if (this._activeTool === "ICON") {
                        symbol = new PictureMarkerSymbol(require.toUrl('../dijits/images/pushpins/blue.png'), 32, 32);
                        symbol.setAngle(0); symbol.setOffset(-0.75, 11.25);
                    }
                    else {
                        symbol = this._drawToolbar.markerSymbol;
                    }
                }
                else if (geometry.isInstanceOf(Polyline)) {
                    symbol = this._drawToolbar.lineSymbol;
                }
                else {
                    symbol = this._drawToolbar.fillSymbol;
                }

                this.removeGraphic();

                if (this._activeTool === "BUFFER") {
                    try {
                        this._graphic = this.bufferTool.createBufferFromPoint(geometry).graphic;
                    }
                    catch (e) {
                        Common.errorDialog(e.message);
                        return;
                    }
                }
                else {
                    this._graphic = new Graphic(geometry, symbol);
                }

                this._graphic._type = this._activeTool;
                this._graphic.setAttributes({ description: "" });
                this._graphic.setEditing(true);
                this._graphic.save();

                // set up the graphic infoTemplate (basically this is the "Bubble Window")
                InfoTemplateManager.setMap(this.map);
                this._graphic.setInfoTemplate(InfoTemplateManager.getTabularAttributeTemplate({ createWidgetsFromGeometry: true }));

                this.markupLayer.add(this._graphic);
                var domNode = dom.byId("wvsGraphicEditContainer");
                domStyle.set(domNode, { display: "block" });

                //Make sure to disable mouse events for all markup layers when the tool is active
                for (var i = 0; i < this.map.graphicsLayerIds.length; i++) {
                    var lyr = this.map.getLayer(this.map.graphicsLayerIds[i]);
                    lyr.disableMouseEvents();
                }

                // start up the graphics editor
                this._graphicEditor = new GraphicEditor({ map: this.map, graphic: this._graphic });
                this.own(on.once(this._graphicEditor, "close", lang.hitch(this, function () {
                    delete this._graphicEditor;
                })));
                this._graphicEditor.startup();
                this._graphicEditor.placeAt(dom.byId("wvsGraphicEditContainer"));
            },

            removeGraphic: function () {
                // summary:
                //      destroys the the internal graphic and the graphic editor
                // description:
                //      Removes the most recently created graphic. If the graphic is not saved, it is also removed from the markup layer. The graphic editor is destroyed (if needed).
                // returns:
                //      undefined

                if (this._graphic && !this._graphic.isSaved()) {
                    this.markupLayer.remove(this._graphic);
                }
                if (this._graphicEditor) {
                    this._graphicEditor.destroy();
                }
                delete this._graphic;

            }
        });
    });