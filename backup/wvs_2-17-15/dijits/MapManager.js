console.log("wvs/dijits/MapManager.js");

define([
    "dojo/_base/declare"
    , "dojo/_base/lang"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dijit/_WidgetsInTemplateMixin"
    , "dojo/text!./templates/MapManager.html"
    , "dijit/layout/BorderContainer"
    , "dijit/layout/ContentPane"
    , "dojo/store/Observable"
    , "dojo/store/Memory"
    , "dijit/tree/ObjectStoreModel"
    , "dijit/Tree"
    , "esri/request"
    , "esri/layers/ArcGISDynamicMapServiceLayer"
    , "esri/layers/ArcGISImageServiceLayer"
    , "esri/map"
    , "esri/arcgis/Portal"
    , "esri/layers/GeoRSSLayer"
    , "esri/dijit/Basemap"
    , "esri/dijit/BasemapLayer"
    , "esri/arcgis/utils"
    , "dojo/_base/array"
    , "dojo/Deferred"
    , "dojo/on"
    , "dojo/dom"
    , "dojo/dom-class"
    , "dojo/dom-construct"
    , "dojo/dom-style"
    , "dojo/request/xhr"
    , "dijit/TitlePane"
    , "dijit/layout/TabContainer"
    , "dijit/Dialog"
    , "dojo/aspect"
    , "./Toc"
    , "wvs/common/esriUtilities"
    , "wvs/common/UrlUtils"
    , "wvs/common/MarkupManager"
    , "require"
    , "dijit/Toolbar"
    , "dijit/form/Button"
    , "dijit/form/DropDownButton"
    , "dijit/DropDownMenu"
    , "dijit/Menu"
    , "dijit/MenuItem"
    , "dijit/RadioMenuItem"
    , "dijit/ToolbarSeparator"
    , "dojo/string"
    , "dijit/layout/AccordionContainer"
    , "dojo/text!./templates/MapManagerMapServiceDetails.html"
    , "dojo/text!./templates/MapManagerImageServiceDetails.html"
    , "dojo/text!./templates/MapManagerEsriServerTemplate.html"
    , "dojo/text!./templates/MapManagerEsriFolderTemplate.html"
    , "dojo/text!./templates/MapManagerWebMapDetails.html"
    , "dojo/text!./templates/MapManagerLoadingTemplate.html"
    , "dojo/text!./templates/MapManagerUserMarkupTemplate.html"
    , "dojo/text!./templates/MapManagerUserGraphicTemplate.html"
    , "dojo/text!./templates/MapManagerFeatureLayerTemplate.html"
    , "dojo/text!./templates/MapManagerGroupTemplate.html"
    , "./LayerImport"
    , "dojo/has"
    , "wvs/common/Common"
    , "wvs/common/ObjUtil"
    , "dojo/promise/all"
    , "../_base/config"
    , "wvs/extensions/esri/layers/MarkupLayer"
], function (
   declare,
   lang,
   _WidgetBase,
   _TemplatedMixin,
   _WidgetsInTemplateMixin,
   template,
   BorderContainer,
   ContentPane,
   Observable,
   Memory,
   ObjectStoreModel,
   Tree,
   esriRequest,
   ArcGISDynamicMapServiceLayer,
   ArcGISImageServiceLayer,
   Map,
   esriPortal,
   GeoRSSLayer,
   Basemap,
   BasemapLayer,
   arcgisUtils,
   array,
   Deferred,
   on,
   dom,
   domClass,
   domConstruct,
   domStyle,
   xhr,
   TitlePane,
   TabContainer,
   Dialog,
   aspect,
   Toc,
   esriUtilities,
   urlUtils,
   MarkupManager,
   require,
   Toolbar,
   Button,
   DropDownButton,
   DropDownMenu,
   Menu,
   MenuItem,
   RadioMenuItem,
   ToolbarSeparator,
   string,
   AccordionContainer,
   mapServiceTemplate,
   imageServiceTemplate,
   esriServerTemplate,
   esriFolderTemplate,
   webMapTemplate,
   loadingTemplate,
   userMarkupTemplate,
   userGraphicTemplate,
   featureLayerTemplate,
   arcGisGroupTemplate,
   LayerImport,
   has,
   Common,
   ObjUtil,
   all,
   wvsConfig,
   MarkupLayer
) {
    return declare([BorderContainer, _TemplatedMixin, _WidgetsInTemplateMixin], {

        templateString: template,

        declaredClass: "wvs.dijits.MapManager",

        // enableArcGISOnline: Boolean
        //      specifies whether or not the map manager will support ArcGIS Online
        enableArcGISOnline: false,

        //  enableMarkup: Boolean
        //      enables markup to be shown in the map manager
        enableMarkup: true,

        //  enableMapServices: Boolean
        //      enables map services to be shown in the map manager
        enableMapServices: true,

        //  enableAddLayerFromWeb: Boolean
        //      enables the add layer from web dialog for adding web resources
        enableAddLayerFromWeb: true,

        // saveToLocalStorage: Boolean
        //      Whether or not to save to local storage when this widget is closed. We only save map services as AGOL and markup are dynamic.
        saveToLocalStorage: true,

        // _ids: Integer
        //      Used to create ids for tree nodes
        _ids: 0,

        // previewMode: Boolean
        //      Indicates whether or not the MapManager is in preview mode (as opposed to service addition/selection)
        previewMode: false,

        // portalUrl: String
        //      the url for the ArcGIS Online Portal
        portalUrl: "https://www.arcgis.com",

        //  mapServicesTitle: String
        //      the title for the map services title pane
        mapServicesTitle: "Map Services",

        //  markupService: String
        //      In order to use markup, we must have a markup service and markup must be enabled
        markupService: null,

        //  hasRendered: bool
        //    Indicates if this panel has rendered the markupServices panel at least once.
        hasRendered: false,

        // _treeTypeMap: Object
        //      This is an easy to find area to set different properties used by the dojo Tree. It keeps helper methods very simple so we do not have to duplicate logic in switch statements and accidentally miss something.
        _treeTypeMap: {
            esriFolder: {
                tooltip: "ArcGIS Server Folder",
                children: true,
                iconClass: "jstoolkit-icon-folder",
                store: "mapServicesStore"
            },
            esriServer: {
                tooltip: "ArcGIS Server",
                children: true,
                iconClass: "jstoolkit-icon-server",
                store: "mapServicesStore"
            },
            esriMapService: {
                tooltip: "ArcGIS Map Server",
                children: false,
                iconClass: "jstoolkit-icon-service",
                store: "mapServicesStore"
            },
            esriImageService: {
                tootip: "ArcGIS Image Server",
                children: false,
                iconClass: "jstoolkit-icon-service",
                store: "mapServicesStore"
            },
            arcGisOnlineWebMap: {
                tooltip: "ArcGIS Online Web Map",
                children: true,
                iconClass: "jstoolkit-icon-folder",
                store: "mapServicesStore"
            },
            arcGISOnlineAccount: {
                tooltip: "ArcGIS Online",
                children: true,
                iconClass: "jstoolkit-icon-esri-medium",
                store: "arcGisOnlineStore"
            },
            root: {
                tooltip: "",
                children: true,
                iconClass: "",
                store: null // this is because the root is never visible for any of our trees. this should never be changed
            },
            "Web Map": {
                tooltip: "ArcGIS Online Web Map",
                children: true,
                iconClass: "jstoolkit-icon-map-small",
                store: "arcGisOnlineStore"
            },
            arcGISOnlineFolder: {
                tooltip: "ArcGIS Server Folder",
                children: true,
                iconClass: "jstoolkit-icon-folder",
                store: "arcGisOnlineStore"
            },
            arcGISOnlineMyFolders: {
                tooltip: "ArcGIS Server Folder",
                children: true,
                iconClass: "jstoolkit-icon-folder-user-small",
                store: "arcGisOnlineStore"
            },
            arcGISOnlineRootItems: {
                tooltip: "ArcGIS Server Folder",
                children: true,
                iconClass: "jstoolkit-icon-folder-user-small",
                store: "arcGisOnlineStore"
            },
            arcGISOnlineMyGroups: {
                tooltip: "ArcGIS Server Folder",
                children: true,
                iconClass: "jstoolkit-icon-folder-user-small",
                store: "arcGisOnlineStore"
            },
            arcGISOnlineGroup: {
                tooltip: "ArcGIS Server Folder",
                children: true,
                iconClass: "jstoolkit-icon-folder-group-small",
                store: "arcGisOnlineStore"
            },
            arcGISBasemap: {
                tooltip: "Basemap (import not currently supported)",
                children: false,
                iconClass: "",
                store: "arcGisOnlineStore"
            },
            arcGISBasemapLayer: {
                tooltip: "Basemap Layer",
                children: false,
                iconClass: "",
                store: "arcGisOnlineStore"
            },
            KML: {
                tooltip: "KML Layer",
                children: false,
                iconClass: "jstoolkit-icon-service",
                store: "mapServicesStore"
            },
            GeoRSS: {
                tooltip: "GeoRSS Layer",
                children: false,
                iconClass: "jstoolkit-icon-rss-small",
                store: "mapServicesStore"
            },
            FeatureLayer: {
                tooltip: "Feature Layer",
                children: false,
                iconClass: "jstoolkit-icon-service",
                store: "mapServicesStore"
            },
            folder: {
                tooltip: "Markup Folder",
                children: true,
                iconClass: "jstoolkit-icon-folder",
                store: "markupStore"
            },
            graphic: {
                tooltip: "Markup Graphic",
                children: false,
                iconClass: "",
                store: "markupStore"
            }
        },
        constructor: function (params, srcNodeRef) {
            this.markupService = wvsConfig.defaults.services.markupService;

            // Required params
            if (!params.map) {
                throw new Error("no map provided for wvs.dijits.MapManager");
            }
            lang.mixin(this, params);

            // variables used for preview view
            this.toc = null;
            this.extent = null;
        },

        postCreate: function () {
            //Initialize features that are enabled and remove their DOM nodes if they are not.
            if (this.enableMapServices) {
                this._initMapServicesTree();
            } else {
                this.leftPaneAccordion.removeChild(this.mapServicesContentPane);
            }

            if (this.enableMarkup) {
                this._initMarkupTree();
            } else {
                this.leftPaneAccordion.removeChild(this.markupContentPane);
            }

            if (this.enableArcGISOnline) {
                this._initArcGisOnline();
            } else {
                this.leftPaneAccordion.removeChild(this.arcGisOnlineContentPane);
            }

            //Register events with the map.
            this._registerEvents();
        },

        destroy: function () {
            if (has("local-storage") && this.saveToLocalStorage) {
                // we only save root data
                var rootData = this.mapServicesStore.query({ parent: "root" });
                if (rootData.length) {
                    var simplifiedItems = [];
                    for (var i = 0; i < rootData.length; i++) {
                        if (rootData[i].source != 'defaults') {
                            var item = lang.mixin({}, rootData[i]);
                            delete item.loaded;
                            delete item.id;
                            delete item.parent;
                            simplifiedItems.push(item);
                        }
                    }
                    localStorage.setItem("mapManager", JSON.stringify(simplifiedItems));
                }
            }
        },

        _initMapServicesTree: function () {
            var data = [{ id: 'root', name: 'Root Node', type: 'root' }];

            if (this.items || this.requiredItems || (has("local-storage") && localStorage.getItem("mapManager"))) {
                var storeData = [];
                if (this.requiredItems.length > 0) {
                    console.log("mapmanager->we have required items");

                    storeData = ObjUtil.clone(this.requiredItems);
                    for (var i = 0; i < storeData.length; i++) {
                        storeData[i].source = "defaults";
                        console.log(storeData[i]);
                    }
                } else {
                    console.log("mapmanager->no required items");
                }

                if ((has("local-storage") && localStorage.getItem("mapManager"))) {
                    var savedData = JSON.parse(localStorage.getItem("mapManager"));
                    for (var i = 0; i < savedData.length; i++) {
                        savedData[i].source = 'saved';
                        storeData.push(savedData[i]);
                    }
                } else {
                    for (var i = 0; i < this.items.length; i++) {
                        this.items[i].source = 'items';
                        storeData.push(this.items[i]);
                    }
                }
                for (var i = 0; i < storeData.length; i++) {
                    storeData[i].loaded = false;
                    storeData[i].parent = "root";
                    storeData[i].id = this._getNodeId();
                }
                data = data.concat(storeData);
            }

            // The store to be used by the model for the tree
            this.mapServicesStore = new Observable(new Memory({
                data: data,
                getChildren: function (object) {
                    return this.query({ parent: object.id });
                }
            }));

            // The tree model
            this.mapServicesModel = new ObjectStoreModel({
                store: this.mapServicesStore,
                query: { id: 'root' },
                mayHaveChildren: lang.hitch(this, this._mayHaveChildren)
            });

            // Create the Tree
            this.mapServicesTree = new Tree({
                model: this.mapServicesModel,
                showRoot: false,
                openOnClick: false,
                autoExpand: true,
                // Override icon classes based on our types
                getIconClass: lang.hitch(this, this._getIconClass),
                getTooltip: lang.hitch(this, this._getTooltip)
            });


            // Register Events
            var nodeOpen = on(this.mapServicesTree, "open", lang.hitch(this, this._nodeOpen, this.mapServicesStore));
            var nodeClick = on(this.mapServicesTree, "click", lang.hitch(this, this._nodeClick, this.mapServicesStore));
            this.own(nodeOpen, nodeClick);

            this.mapServicesContentPane.set('title', this.mapServicesTitle);
            this.mapServicesContentPane.set('content', this.mapServicesTree);
        },

        _openAddMapDialog: function () {
            var self = this;
            var dialog = Common.showWidgetInDialog(LayerImport, {
                mapManager: true
            }, { title: "Add Layer From Web", style: "width:500px;" });

            on(dialog.content, "import-error", function (error) {
                console.warn(error);
            });

            on(dialog.content, "layer-imported", function (itemNode) {
                itemNode.loaded = false;
                itemNode.parent = "root";
                itemNode.id = self._getNodeId();
                self.mapServicesStore.put(itemNode);
                dialog.hide();
            });
        },

        _removeMapFromManager: function (evt) {
            var self = this;
            if (self._activeItem) {
                if (self._activeItem.type === "arcGISOnlineAccount") {
                    self._signInOutArcGisOnline();
                    self.set("_activeItem", null);
                } else if (self._activeItem.type === "folder") {
                    var deleteMessage = "Are you sure you want to delete this item?";
                    if (self.markupService) {
                        deleteMessage += " This will also remove this markup collection from the server.";
                    }
                    // Remove the folder
                    Common.confirmDialog(deleteMessage).then(
                        function (confirmed) {
                            if (confirmed) {
                                var mm = new MarkupManager();
                                mm.deleteMarkupCollection(self._activeItem.layer._id).then(
                                   function () {
                                       alert("Markup Layer successfully deleted.");
                                   },
                                    lang.hitch(self, function (err) {
                                        Common.errorDialog(err.message);
                                    }));
                                var store = self._getStoreFromItem(self._activeItem);
                                if (store) {
                                    self._removeItem(store, self._activeItem);
                                }
                                self.set("_activeItem", null);
                            }
                        });
                } else {
                    var store = self._getStoreFromItem(self._activeItem);
                    if (store) {
                        self._removeItem(store, self._activeItem);
                    }
                    self.set("_activeItem", null);
                }
            }
        },

        _togglePreviewMode: function (evt) {
            this.set("previewMode", !this.get("previewMode"));
        },

        _togglePreviewExtent: function () {
            if (this.previewExtent == 0) {
                this.previewExtent = 1;
            } else {
                this.previewExtent = 0;
            }
            if (this.previewMode) {
                this._setPreviewMapExtent();
            }
        },

        _sendLayerToMap: function (evt) {
            if (this._activeItem) {
                this._addLayerToMapFromItem(this._activeItem);
            }
        },

        _initMarkupTree: function () {
            if (this.enableMarkup && this.markupService) {
                var mm = new MarkupManager();
                var dfd = mm.getMarkup();

                dfd.then(
                    // Success Callback
                    lang.hitch(this, function (response) {
                        if (typeof (response) == "string") {
                            this.markupContentPane.set('title', "Markup");
                            this.markupContentPane.set('content', "Error retrieving markup from " + this.markupService.toString());
                            this.markupContentPane.set('open', false);
                        } else if (response.length === 0) {
                            this.markupContentPane.set('title', "Markup");
                            this.markupContentPane.set('content', "No markup was found");
                            this.markupContentPane.set('open', false);
                        } else {
                            var data = [{ id: 'root', name: 'Root Node', type: 'root' }];
                            for (var i = 0; i < response.length; i++) {
                                // Right now, we are not saving the true structure of the object so we need to re-serialize the 'options' of the markup collection JSON.
                                response[i].options = JSON.parse(response[i].options);
                                if (response[i].options.id == "sharedMarkup") {
                                    continue;
                                }
                                delete response[i].options.id;
                                if (response[i]._id)
                                    response[i].options._id = response[i]._id;

                                var newRootId = "root_" + this._getNodeId();

                                var jsonArray = response[i].options.json;

                                //Due to inconsistent behavior and results from the server, check to see if the 'json'
                                //property exists in the first "options" property of the responseData[i] object tree.
                                if (jsonArray === undefined) {
                                    jsonArray = response[i].options.options.json;
                                }
                                for (var j = 0; j < jsonArray.length; j++) {
                                    var modifiedNode = lang.clone(jsonArray[j]);
                                    if (modifiedNode.parent === "root") {
                                        modifiedNode.parent = newRootId;
                                        modifiedNode.id = this._getNodeId();
                                    }
                                    else if (modifiedNode.id === "root") {
                                        modifiedNode.id = newRootId;
                                        modifiedNode.parent = "root";
                                        modifiedNode.layer = response[i];
                                    }
                                    data.push(modifiedNode);
                                }
                            }

                            // The store to be used by the model for the tree
                            this.markupStore = new Observable(new Memory({
                                data: data,
                                getChildren: function (object) {
                                    return this.query({ parent: object.id });
                                }
                            }));

                            // The tree model
                            this.markupModel = new ObjectStoreModel({
                                store: this.markupStore,
                                query: { id: 'root' },
                                mayHaveChildren: lang.hitch(this, this._mayHaveChildren)
                            });

                            // Create the Tree
                            this.markupTree = new Tree({
                                model: this.markupModel,
                                showRoot: false,
                                openOnClick: false,
                                // Override icon classes based on our types
                                getIconClass: lang.hitch(this, this._getIconClass),
                                getTooltip: lang.hitch(this, this._getTooltip)
                            });


                            // Register Events
                            var nodeOpen = on(this.markupTree, "open", lang.hitch(this, this._nodeOpen, this.markupStore));
                            var nodeClick = on(this.markupTree, "click", lang.hitch(this, this._nodeClick, this.markupStore));
                            this.own(nodeOpen, nodeClick);

                            this.markupContentPane.set('title', 'Markup');
                            this.markupContentPane.set('content', this.markupTree);
                            this.markupContentPane.set('open', true);
                        }
                    }),

                    lang.hitch(this, function (error) {
                        this.markupContentPane.set('title', "Markup");
                        this.markupContentPane.set('content', "Error retrieving markup from " + this.markupService.toString());
                        this.markupContentPane.set('open', false);
                    })
                );
            }
        },

        /**
        *  ArcGIS Online Functions 
        */
        _initArcGisOnline: function () {
            // arcGIS Online vars
            this.arcGISOnline = {
                active: false
            };

            // The store to be used by the model for the ArcGIS Online tree
            this.arcGisOnlineStore = new Observable(new Memory({
                data: [{ id: 'root', name: 'Root Node', type: 'root' }],
                getChildren: function (object) {
                    return this.query({ parent: object.id });
                }
            }));

            // The tree model
            this.arcGisOnlineModel = new ObjectStoreModel({
                store: this.arcGisOnlineStore,
                query: { id: 'root' },
                mayHaveChildren: lang.hitch(this, this._mayHaveChildren)
            });

            // Create the Tree
            this.arcGisOnlineTree = new Tree({
                model: this.arcGisOnlineModel,
                showRoot: false,
                openOnClick: false,
                getIconClass: lang.hitch(this, this._getIconClass),
                getTooltip: lang.hitch(this, this._getTooltip)
            });

            // Register tree events
            var nodeOpen = on(this.arcGisOnlineTree, "open", lang.hitch(this, this._nodeOpen, this.arcGisOnlineStore));
            var nodeClick = on(this.arcGisOnlineTree, "click", lang.hitch(this, this._nodeClick, this.arcGisOnlineStore));
            this.own(nodeOpen, nodeClick);


            // Add the tree to the DOM
            this.arcGisOnlineContentPane.set('title', 'ArcGIS Online');
            this.arcGisOnlineContentPane.set('content', this.arcGisOnlineTree);

            // ArcGIS Online Menu
            var arcGISMenu = new DropDownMenu({ style: "display: none;" });
            this.signInOutMenuItem = new MenuItem({
                label: "Sign In",
                onClick: lang.hitch(this, this._signInOutArcGisOnline)
            });
            arcGISMenu.addChild(this.signInOutMenuItem);

            this.arcGisOnlineButton = new DropDownButton({
                label: "ArcGIS Online",
                iconClass: "jstoolkit-icon-esri-large",
                dropDown: arcGISMenu
            });
            this.actionToolbar.addChild(this.arcGisOnlineButton);
        },

        _getItemsFromWebMap: function (item, node, store) {
            var self = this,
                deferred = new Deferred();

            var webMapId = urlUtils.getQuery(item.url).webmap;
            if (webMapId == undefined) {
                var componentArray = item.url.split('/');
                for (var i = 0; i < componentArray.length; i++) {
                    if (componentArray[i] == 'items') {
                        webMapId = componentArray[i + 1];
                        break;
                    }
                }
            }
            arcgisUtils.getItem(webMapId).then(function (result) {

                item.webmap = result.item;
                // Load operational layers
                var operationalLayers = result.itemData.operationalLayers;
                for (var i = 0; i < operationalLayers.length; i++) {
                    if (operationalLayers[i].type) {
                        // KML Layer
                        if (operationalLayers[i].type === "KML") {
                            var node = {
                                id: self._getNodeId(),
                                type: operationalLayers[i].type,
                                name: operationalLayers[i].title,
                                parent: item.id,
                                loaded: false,
                                url: operationalLayers[i].url,
                                constructorOptions: {
                                    id: operationalLayers[i].title
                                }
                            };
                            store.put(node);
                        }
                            // GeoRSS Layer
                        else if (operationalLayers[i].type === "GeoRSS") {
                            var node = {
                                id: self._getNodeId(),
                                type: operationalLayers[i].type,
                                url: operationalLayers[i].url,
                                parent: item.id,
                                name: operationalLayers[i].title,
                                loaded: false
                            };
                            store.put(node);
                        }
                    }
                        // Feature Collection
                    else if (operationalLayers[i].featureCollection) {
                        // TODO: Implement this
                    }
                        // esri Map Services
                    else {
                        var endPointUrl = operationalLayers[i].url;
                        var uri = require.toUrl(endPointUrl);
                        // Map Server
                        if (endPointUrl.match(/MapServer\/*$/g)) {
                            var node = {
                                id: self._getNodeId(),
                                name: operationalLayers[i].title,
                                type: "esriMapService",
                                parent: item.id,
                                url: endPointUrl,
                                loaded: false,
                                constructorOptions: {
                                    id: operationalLayers[i].title,
                                    visible: operationalLayers[i].visiblity
                                }
                            };
                            store.put(node);
                        }
                            // Feature Layer
                        else if (endPointUrl.match(/(MapServer|FeatureServer)\/[0-9]$/g)) {
                            var node = {
                                id: self._getNodeId(),
                                name: operationalLayers[i].title,
                                type: "FeatureLayer",
                                parent: item.id,
                                url: endPointUrl,
                                loaded: false,
                                constructorOptions: {
                                    id: operationalLayers[i].title,
                                    visible: operationalLayers[i].visiblity,
                                    opacity: operationalLayers[i].opacity,
                                    mode: operationalLayers[i].mode
                                },
                                definitionExpression: operationalLayers[i].layerDefinition ? operationalLayers[i].layerDefinition.definitionExpression : ""

                            };
                            store.put(node);
                        }
                    }
                }

                item.loaded = true;
                deferred.resolve(result);
            },
                function (error) {
                    item.loaded = true;
                    deferred.reject(error);
                    // Not implemented
                }

            );

            return deferred.promise;
        },
        _signInOutArcGisOnline: function () {
            var self = this;

            if (!this.arcGISOnline.active) {
                var login = function () {
                    self.portal.signIn().then(function (loggedInUser) {
                        self.arcGISOnline.active = true;
                        self.leftPaneAccordion.selectChild(self.arcGisOnlineContentPane);
                        self._getItemsFromArcGISOnline();
                        self.arcGisOnlineButton.set("label", loggedInUser.fullName);
                        self.signInOutMenuItem.set("label", "Sign Out");
                    });
                };
                if (!this.portal) {
                    this.portal = new esriPortal.Portal(this.portalUrl);
                    this.portal.allSSL = document.location.protocol === "https:" ? true : false;
                    on.once(this.portal, "load", function () { login(); });
                }
                else {
                    login();
                }
            }
            else {
                self.arcGISOnline.active = false;
                this.signInOutMenuItem.set("label", "Sign In");
                this.arcGisOnlineButton.set("label", "ArcGIS Online");
                // We need a lot of logic here but ignore for now
                this.portal.signOut();
                this._removeItem(this.arcGisOnlineStore, { id: "arcGISOnlineRootItems" });
                this._removeItem(this.arcGisOnlineStore, { id: "arcGISOnlineMyFolders" });
                this._removeItem(this.arcGisOnlineStore, { id: "arcGISOnlineMyGroups" });
            }
        },
        _getItemsFromArcGISOnline: function () {
            var self = this,
                user = this.portal.getPortalUser();
            // Get root web map items
            user.getItems(null).then(
                function (rootItems) {
                    var rootItemsContainer = { id: "arcGISOnlineRootItems", name: "My Maps", parent: "root", type: "arcGISOnlineRootItems" };
                    self.arcGisOnlineStore.put(rootItemsContainer);
                    array.forEach(rootItems, function (rootItem) {
                        if (rootItem.type === "Web Map") {
                            var node = { id: self._getNodeId(), name: rootItem.title, loaded: false, url: rootItem.itemDataUrl, parent: "arcGISOnlineRootItems", type: rootItem.type };
                            self.arcGisOnlineStore.put(node);
                        }
                    });
                },
                function (error) {
                    Common.errorDialog(error.message);
                }
            );

            // Get folders
            user.getFolders().then(
                function (folders) {
                    var folderNodeContainer = { id: "arcGISOnlineMyFolders", name: "My Folders", parent: "root", type: "arcGISOnlineMyFolders" };
                    self.arcGisOnlineStore.put(folderNodeContainer);
                    array.forEach(folders, function (folder) {
                        var node = { id: self._getNodeId(), name: folder.title, loaded: false, folderId: folder.id, parent: "arcGISOnlineMyFolders", type: "arcGISOnlineFolder" };
                        self.arcGisOnlineStore.put(node);
                    });
                },
                function (error) {
                    Common.errorDialog(error.message);
                }
            );



            // Get groups
            user.getGroups().then(
                function (groups) {
                    var groupNodeContainer = { id: "arcGISOnlineMyGroups", name: "My Groups", parent: "root", type: "arcGISOnlineMyGroups" };
                    self.arcGisOnlineStore.put(groupNodeContainer);
                    array.forEach(groups, function (group) {
                        var node = { id: self._getNodeId(), name: group.title, loaded: false, group: group, parent: "arcGISOnlineMyGroups", type: "arcGISOnlineGroup" };
                        self.arcGisOnlineStore.put(node);
                    });
                },
                function (error) {
                    Common.errorDialog(error.message);
                }
            );
        },
        _getItemsFromArcGISOnlineGroup: function (item, node) {
            var self = this;

            item.group.queryItems({ q: 'type:"map"' }).then(
                function (groupItems) {
                    item.loaded = true;
                    array.forEach(groupItems.results, function (groupItem) {
                        // We will support more. Later.
                        if (groupItem.type === "Web Map") {
                            var node = { id: self._getNodeId(), name: groupItem.title, loaded: false, url: groupItem.itemDataUrl, parent: item.id, type: groupItem.type };
                            self.arcGisOnlineStore.put(node);
                        }
                    });
                },
                function (error) {
                    item.loaded = true;
                    Common.errorDialog(error.message);
                }
            );
        },
        _getItemsFromArcGISOnlineFolder: function (item, node) {
            var self = this,
                user = this.portal.getPortalUser();

            user.getItems(item.folderId).then(
                function (folderItems) {
                    item.loaded = true;
                    array.forEach(folderItems, function (folderItem) {
                        // We will support more. Later.
                        if (folderItem.type === "Web Map") {
                            var node = { id: self._getNodeId(), name: folderItem.title, loaded: false, url: folderItem.itemDataUrl, parent: item.id, type: folderItem.type };
                            self.arcGisOnlineStore.put(node);
                        }
                    });
                },
                function (error) {
                    item.loaded = true;
                    Common.errorDialog(error.message);
                }
            );
        },
        /**
        *   Tree Functions
        *
        */
        _getTooltip: function (item) {
            if (item && this._treeTypeMap[item.type] && this._treeTypeMap[item.type].tooltip) {
                return this._treeTypeMap[item.type].tooltip; // String
            }
            return ""; // String
        },
        _getIconClass: function (item, opened) {
            if (item && this._treeTypeMap[item.type] && this._treeTypeMap[item.type].iconClass) {
                return this._treeTypeMap[item.type].iconClass; // String
            }
            return ""; // String
        },
        _mayHaveChildren: function (item) {
            if (item && this._treeTypeMap[item.type] && this._treeTypeMap[item.type].children) {
                return this._treeTypeMap[item.type].children; // Boolean
            }
            return false; // Boolean
        },
        /**
        *   Tree Events
        *
        */
        _nodeOpen: function (store, item, node) {

            console.log("_nodeOpen");
            console.log(item);

            var self = this;
            switch (item.type) {
                case "esriFolder":
                    if (!item.loaded) {
                        self.loadEsriResourceFromURL(item, node).then(
                            function (nodes) {
                                for (var i = 0; i < nodes.length; i++) {
                                    store.put(nodes[i]);
                                }
                            });
                    } else {
                        self._setEsriFolderDetails(item, self);
                    }
                    break;
                case "esriServer":
                    if (!item.loaded) {
                        console.log("loading esriServer->");
                        console.log(item);

                        self.loadEsriResourceFromURL(item, node).then(
                            function (nodes) {
                                for (var i = 0; i < nodes.length; i++) {
                                    store.put(nodes[i]);
                                }
                                self._setEsriServerDetails(item, self);
                            });
                    } else {
                        self._setEsriServerDetails(item, self);
                    }
                    break;
                case "arcGISOnlineAccount":
                    if (!item.loaded) {
                        if (self.arcGISOnline.active) {
                            self._getItemsFromArcGISOnline();
                        }
                        else {

                        }
                    }
                    break;
                case "arcGisOnlineWebMap":
                    if (!item.loaded) {
                        self._getItemsFromWebMap(item, node, store);
                    }
                    break;
                case "Web Map":
                    if (!item.loaded) {
                        self._getItemsFromWebMap(item, node, store);
                    }
                    break;
                case "arcGISOnlineFolder":
                    if (!item.loaded) {
                        self._getItemsFromArcGISOnlineFolder(item, node);
                    }
                    break;
                case "arcGISOnlineGroup":
                    if (!item.loaded) {
                        self._getItemsFromArcGISOnlineGroup(item, node);
                    }
                    break;
            }
        },

        _nodeClick: function (store, item, node, evt) {

            console.log("_nodeClick");
            console.log(item);

            this.detailsPane.set("content", string.substitute(loadingTemplate, {
                loadingUrl: page.root + "Content/images/loading_big.gif"
            }));
            if (item.type === "esriServer" || item.type === "esriFolder") {
                this._nodeOpen(store, item, node);
                return;
            }
            var self = this,
                setPreviewable = function (previewable) {
                    if (previewable) {
                        self.sendToMapButton.set("disabled", false);
                        self.previewButton.set("disabled", false);
                    }
                    else {
                        self.sendToMapButton.set("disabled", true);
                        self.previewButton.set("disabled", true);
                    }
                };

            this.set("_activeItem", item);

            if (item.type === "esriServer" || item.type === "esriFolder") {
                this._nodeOpen(store, item, node);
                return;
            }

            if (item.type === "esriMapService") {
                var newLayer = new ArcGISDynamicMapServiceLayer(item.url);
                setPreviewable(true);

                // populate the preview pane
                on.once(newLayer, "load", function () {
                    var layers = domConstruct.create("ul");
                    var parseLayerInfo = function (layerInfo, parentList) {
                        var subLayerInfos = layerInfo.getSubLayerInfos();
                        var li = domConstruct.create("li", null, parentList);
                        var a = domConstruct.create("a", { href: newLayer.url + "/" + layerInfo.id, innerHTML: layerInfo.name + " (" + layerInfo.id + ")", target: "_new" }, li);
                        if (subLayerInfos.length > 0) {
                            var ul = domConstruct.create("ul", null, li);
                            for (var i = 0; i < subLayerInfos.length; i++) {
                                parseLayerInfo(subLayerInfos[i], ul);
                            };
                        }
                    };
                    var arr = newLayer.getSubLayerInfos();
                    for (var i = 0; i < arr.length; i++) {
                        parseLayerInfo(arr[i], layers);
                    }

                    self.detailsPane.set("content", string.substitute(mapServiceTemplate, {
                        url: newLayer.url || "",
                        description: newLayer.description || "<em>none</em>",
                        spatialReference: newLayer.spatialReference.wkid,
                        layers: layers.innerHTML
                    }));

                }, function () {
                    setPreviewable(false);
                    this.detailsPane.set("content", "Content information is currently unavailable. Please make sure the content is publicly accessible.");
                });

            } else if (item.type === "esriImageService") {
                item.constructorOptions = {
                    opacity: 1.0
                };

                var newLayer = new ArcGISImageServiceLayer(item.url, item.constructorOptions);
                setPreviewable(true);

                on.once(newLayer, "load", function () {
                    item.constructorOptions.id = newLayer.name;
                    self.detailsPane.set("content", string.substitute(imageServiceTemplate, {
                        url: newLayer.url || "",
                        description: newLayer.description || "<em>none</em>",
                        spatialReference: newLayer.spatialReference.wkid,
                        capabilities: newLayer.capabilities,
                        allowedMosaicMethods: newLayer.allowedMosaicMethods
                    }));
                });

            } else if (item.type === "arcGisOnlineWebMap" || item.type === "Web Map") {
                self._setWebMapDetails(item, self);
                setPreviewable(false);
                if (!item.loaded) {
                    this._getItemsFromWebMap(item, node, store).then(
                        //callback
                        function (result) {
                            item.loaded = true;
                        },

                        // errback
                        function (error) {
                            item.loaded = true;
                            Common.errorDialog(error.message);
                        }
                    );
                }
            } else if (item.type === "arcGISOnlineGroup") {
                setPreviewable(false);
                self.detailsPane.set("content", string.substitute(arcGisGroupTemplate, {
                    title: item.group.title,
                    owner: item.group.owner,
                    url: item.group.url,
                    description: item.group.description,
                    access: item.group.access
                }));
            } else if (item.type === "folder" && item.parent === "root") {
                setPreviewable(true);
                this.detailsPane.set("content", string.substitute(userMarkupTemplate, {
                    name: item.name
                }));
            } else if (item.type === "graphic") {
                setPreviewable(false);
                this.detailsPane.set("content", string.substitute(userGraphicTemplate, {
                    name: item.name
                }));
            } else if (item.type === "FeatureLayer") {
                setPreviewable(true);
                this.detailsPane.set("content", string.substitute(featureLayerTemplate, {
                    name: item.name,
                    url: item.url
                }));
            } else if (item.type === "GeoRSS") {
                setPreviewable(true);
                this.detailsPane.set("content", "Details are not applicable");
            } else if (item.type === "KML") {
                setPreviewable(true);
                this.detailsPane.set("content", "Details are not applicable");
            } else {
                setPreviewable(false);
                this.detailsPane.set("content", "Details are not applicable");
            }
        },

        _setWebMapDetails: function (item, self) {
            var webMapId = urlUtils.getQuery(item.url).webmap;
            if (webMapId == undefined) {
                var componentArray = item.url.split('/');
                for (var i = 0; i < componentArray.length; i++) {
                    if (componentArray[i] == 'items') {
                        webMapId = componentArray[i + 1];
                        break;
                    }
                }
            }
            arcgisUtils.getItem(webMapId).then(function (result) {
                var opLayers = domConstruct.create('ul');
                for (var i = 0; i < result.itemData.operationalLayers.length; i++) {
                    var anchorObj = {
                        href: result.itemData.operationalLayers[i].url,
                        innerHTML: result.itemData.operationalLayers[i].title,
                        target: "_new"
                    };
                    var li = domConstruct.create("li", null, opLayers);
                    var a = domConstruct.create("a", anchorObj, li);
                }
                self.detailsPane.set("content", string.substitute(webMapTemplate, {
                    url: item.url || "",
                    description: result.item.description || "",
                    title: result.item.title,
                    owner: result.item.owner,
                    opLayers: opLayers.innerHTML
                }));
            });
        },

        _setEsriServerDetails: function (item, self) {
            var folderList = domConstruct.create("ul");
            for (var i = 0; i < item.folders.length; i++) {
                var anchorObj = {
                    href: item.url + "/" + item.folders[i],
                    innerHTML: item.folders[i],
                    target: "_new"
                };
                var li = domConstruct.create("li", null, folderList);
                var a = domConstruct.create("a", anchorObj, li);
            }
            var serviceList = domConstruct.create("ul");
            for (var i = 0; i < item.services.length; i++) {
                var anchorObj = {
                    href: item.url + "/" + item.services[i].name + "/" + item.services[i].type,
                    innerHTML: item.services[i].name,
                    target: "_new"
                };
                var li = domConstruct.create("li", null, serviceList);
                var a = domConstruct.create("a", anchorObj, li);
            }
            self.detailsPane.set("content", string.substitute(esriServerTemplate, {
                url: item.url || "",
                folders: folderList.innerHTML || "",
                services: serviceList.innerHTML || ""
            }));
        },

        _setEsriFolderDetails: function (item, self) {
            var folderList = domConstruct.create("ul");
            for (var i = 0; i < item.folders.length; i++) {
                var anchorObj = {
                    href: item.url + "/" + item.folders[i],
                    innerHTML: item.folders[i],
                    target: "_new"
                };
                var li = domConstruct.create("li", null, folderList);
                var a = domConstruct.create("a", anchorObj, li);
            }
            var serviceList = domConstruct.create("ul");
            for (var i = 0; i < item.services.length; i++) {
                //We have to remove the folder name from the url to the service resource.
                var serviceName = item.services[i].name.replace(new RegExp(item.name + "/"), "");
                var anchorObj = {
                    href: item.url + "/" + serviceName + "/" + item.services[i].type,
                    innerHTML: item.services[i].name,
                    target: "_new"
                };
                var li = domConstruct.create("li", null, serviceList);
                var a = domConstruct.create("a", anchorObj, li);
            }
            self.detailsPane.set("content", string.substitute(esriFolderTemplate, {
                url: item.url || "",
                name: item.name,
                folders: folderList.innerHTML || "",
                services: serviceList.innerHTML || ""
            }));
        },

        _registerEvents: function () {
            var self = this;

            this.watch("previewMode", function (name, oldVal, newVal) {
                // We are in preview mode
                if (newVal === true) {
                    // Preview buttons
                    this.treeViewButton.set("style", "display:inline-block;");
                    this.previewExtentButton.set("style", "display:inline-block;");

                    // Non-Preview buttons
                    this.previewButton.set("style", "display:none;");
                    if (this.enableArcGISOnline) {
                        this.arcGisOnlineButton.set("style", "display:none;");
                    }
                    this.addLayerButton.set("style", "display:none;");
                    this.removeButton.set("style", "display:none;");
                    this.nodeActionSeparator.set("style", "display:none;");


                    this.treeViewButton.set("hovering", false);

                    self.removeChild(self.centerPane);
                    self.previewContainer.set("style", "width:100%;height:100%;display:block;padding:0px;");
                    self.tocPane.set("style", "width:25%;height:100%;display:block;");
                    self.previewPane.set("style", "width:75%;height:100%;display:block;padding:0px;");
                    self.addChild(self.previewContainer);

                    // Create the map (if needed)
                    if (!(self.previewMap instanceof Map)) {
                        var mapDiv = domConstruct.create("div", { id: "map-manager-preview-map", style: 'width:100%;height:100%' });
                        self.previewPane.set("content", mapDiv);
                        self.previewMap = new Map("map-manager-preview-map", { basemap: "streets", slider: false, logo: false });
                    }


                    if (self._activeItem.type !== "esriMapService") {
                        this.previewExtent = 0;
                        this.defaultExtentFromServiceMenuItem.set("checked", false);
                        this.defaultExtentFromServiceMenuItem.set("disabled", true);
                    }
                    else {
                        this.defaultExtentFromServiceMenuItem.set("disabled", false);
                    }

                    // Create the toc
                    var tocDiv = domConstruct.create("div", { style: "width:25%;" });
                    self.tocPane.set("content", tocDiv);
                    self.toc = new Toc({ map: self.previewMap, useSpriteImage: false }, tocDiv);
                    self.toc.startup();


                    var addedLayers = self._addLayerToMapFromItem(self._activeItem, self.previewMap);
                    if (self._activeItem.type === "esriMapService") {
                        addedLayers.then(function (layer) {
                            self._activeMapServiceLayer = layer;
                            // We need to delay adding the layer and setting the extent so that the parent can first resize
                            setTimeout(function () {
                                self._setPreviewMapExtent();
                            }, 500);
                        });
                    }
                    else {
                        setTimeout(function () {
                            self._setPreviewMapExtent();
                        }, 500);
                    }
                }
                else {
                    if (self.previewMap) {
                        self.previewMap.removeAllLayers(false);
                    }

                    self.toc.destroyRecursive();
                    self.toc = null;

                    if (this.enableArcGISOnline) {
                        this.arcGisOnlineButton.set("style", "display:inline-block;");
                    }
                    this.addLayerButton.set("style", "display:inline-block;");
                    this.nodeActionSeparator.set("style", "display:inline-block;");
                    this.addLayerButton.set("style", "display:inline-block;");
                    this.removeButton.set("style", "display:inline-block;");
                    // Preview buttons
                    this.previewButton.set("style", "display:inline-block;");
                    this.treeViewButton.set("style", "display:none;");
                    this.previewExtentButton.set("style", "display:none;");

                    this.previewButton.set("hovering", false);
                    self.addChild(self.centerPane);
                    self.removeChild(self.previewContainer);
                    self.previewContainer.set("style", "display:none");

                }
            });

            this.watch("_activeItem", function (object, oldVal, newVal) {
                if (newVal) {
                    // enable deleting of only root items
                    if (newVal.source !== "defaults") {
                        self.removeButton.set("disabled", newVal.parent !== "root");
                    } else {
                        self.removeButton.set("disabled", true);
                    }
                }
                else {
                    self.removeButton.set("disabled", true);
                    self.sendToMapButton.set("disabled", true);
                    self.previewButton.set("disabled", true);
                }
            });
        },

        _setPreviewMapExtent: function () {
            var self = this,
                extent;

            switch (this.previewExtent) {
                // set to to extent of main map
                case 0:
                    extent = this.map.extent;
                    break;
                case 1:
                    extent = this._activeMapServiceLayer && this._activeMapServiceLayer.initialExtent ? this._activeMapServiceLayer.initialExtent : this.map.extent;
                    break;
                default:
                    extent = this.map.extent;
                    break;
            }
            return this.previewMap.setExtent(extent, true);
        },

        _layerExistsInMap: function (layerUrl) {
            var layerIds = this.map.layerIds.concat(this.map.graphicsLayerIds),
                layerExists = false;
            for (var i = 0; i < layerIds.length; i++) {
                var layer = this.map.getLayer(layerIds[i]);
                if (layer.url === layerUrl) {
                    layerExists = true;
                }
            }

            return layerExists;
        },

        _addLayerToMap: function (layerUrl, type) {
            if (!this._layerExistsInMap(layerUrl)) {
                this.map.addLayer(new ArcGISDynamicMapServiceLayer(layerUrl));
                return true;
            }

            return false;
        },

        _addLayerToMapFromItem: function (item, map) {
            var type = item.type,
                map = map || this.map,
                addLayer = function (layer, index) {
                    if (index) {
                        map.addLayer(layer, index);
                    } else {
                        map.addLayer(layer);
                    }
                },
                util = new esriUtilities(),
                deferred = new Deferred();

            switch (type) {
                case "FeatureLayer":
                    util.layerFactory({ url: item.url, options: item.constructorOptions, type: type }).then(
                        function (instance) {
                            if (item.definitionExpression) {
                                instance.setDefinitionExpression(item.definitionExpression);
                            }
                            addLayer(instance);
                            deferred.resolve(instance);
                        });
                    break;
                case "GeoRSS":
                    util.layerFactory({ url: item.url, type: type }).then(
                        function (instance) {
                            addLayer(instance);
                            deferred.resolve(instance);
                        });
                    break;
                case "arcGisOnlineWebMap":
                    var webmap = item.webmap;
                    util.layerFactory({ url: webmap, type: type, options: { title: item.name } }).then(
                        function (instance) {
                            addLayer(instance);
                            deferred.resolve(instance);
                        });
                    break;
                case "esriMapService":
                    util.layerFactory({ url: item.url, options: item.constructorOptions, type: "ArcGISDynamicMapServiceLayer" }).then(
                        function (instance) {
                            addLayer(instance);
                            deferred.resolve(instance);
                        });
                    break;
                case "esriImageService":
                    util.layerFactory({ url: item.url, options: item.constructorOptions, type: "ArcGISImageServiceLayer" }).then(
                       function (instance) {
                           addLayer(instance, 1);
                           deferred.resolve(instance);
                       });
                    break;
                case "KML":
                    util.layerFactory({ url: item.url, options: item.constructorOptions, type: type }).then(
                        function (instance) {
                            addLayer(instance);
                            deferred.resolve(instance);
                        });
                    break;
                case "folder":
                    var layer;
                    if (item.layer.options.options) {
                        layer = new MarkupLayer(item.layer.options.options || {});
                    } else {
                        layer = new MarkupLayer(item.layer.options || {});
                    }
                    addLayer(layer);
                    deferred.resolve(layer);
                    break;
            }

            return deferred.promise;
        },
        addEsriServer: function (endpointUrl) {
            var uri = require.toUrl(endpointUrl);
            var node = { id: this._getNodeId(), name: uri.host, type: "esriServer", parent: "root", url: endpointUrl, loaded: false };
            this.store.put(node);
        },

        loadEsriResourceFromURL: function (item, node) {
            var self = this,
                deferred = new Deferred(),
                nodes = [];

            var iconClass = item.type === "esriFolder" ? "jstoolkit-icon-folder" : "jstoolkit-icon-server";
            domClass.remove(node.iconNode, iconClass);
            domClass.add(node.iconNode, iconClass);
            var serverRequest = esriRequest({
                url: item.url,
                content: { f: "json" },
                handleAs: "json",
                callbackParamName: "callback"
            });

            item.loaded = true;
            serverRequest.then(
                // Callback
                function (response) {
                    lang.mixin(item, response);
                    domClass.add(node.iconNode, iconClass);
                    domClass.remove(node.iconNode, iconClass);

                    var folders = response.folders,
                        services = response.services;

                    // Create folder nodes
                    for (var i = 0; i < folders.length; i++) {
                        var newFolderNode = { id: self._getNodeId(), name: folders[i], type: "esriFolder", parent: item.id, url: item.url + "/" + folders[i], loaded: false };
                        nodes.push(newFolderNode);
                    }


                    // Create service nodes
                    for (var i = 0; i < services.length; i++) {
                        // We currently only support MapServer
                        if (services[i].type === "MapServer") {
                            var name;
                            if (item.type === "esriServer") {
                                name = services[i].name.replace(new RegExp(item.name + "/", "g"), "");
                            } else {
                                name = services[i].name.split("/")[1];
                            }
                            var serviceNode = { id: self._getNodeId(), name: name, type: "esriMapService", url: item.url + "/" + name + "/MapServer", parent: item.id, loaded: false };
                            nodes.push(serviceNode);
                        }
                    }

                    //self._setEsriServerDetails(item, self);

                    deferred.resolve(nodes);

                },
                // Errback
                function (error) {
                    domClass.add(node.iconNode, iconClass);
                    domClass.remove(node.iconNode, "jstoolkit-icon-loading-small");
                    item.loaded = true;
                    // Not implemented....Yet.
                }
            );

            return deferred.promise;
        },

        _getStoreFromItem: function (item) {
            var store = null;
            if (item && this._treeTypeMap[item.type] && this._treeTypeMap[item.type].store && this[this._treeTypeMap[item.type].store]) {
                store = this[this._treeTypeMap[item.type].store];
            }

            return store;
        },

        _removeItem: function (store, item) {
            var children = store.query({ parent: item.id });
            for (var i = 0; i < children.length; i++) {
                this._removeItem(store, children[i]);
            }
            store.remove(item.id);
        },
        _getNodeId: function () {
            return this._ids++;
        }

    });
});