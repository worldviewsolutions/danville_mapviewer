﻿define([
    "dojo/_base/declare"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dojo/text!./templates/ViewShed.html"
    , "dojo/_base/lang"
    , "dojo/on"
    , "dojo/_base/array"
    , "dijit/form/TextBox"
    , "dijit/form/Select"
    , "dijit/form/Button"
    , "dojo/dom-style"
    , "esri/units"
    , "../common/Common"
    , "../common/Converter"
    , "../common/InfoTemplateManager"
    , "esri/symbols/jsonUtils"
    , "esri/geometry/Polygon"
    , "esri/geometry/Polyline"
    , "esri/geometry/Point"
    , "esri/graphic"
    , "esri/tasks/LinearUnit"
    , "esri/tasks/Geoprocessor"
    , "esri/tasks/FeatureSet"
    , "esri/graphicsUtils"
    , "dojo/_base/connect"
    , "esri/SpatialReference"
    , "require"
    , "../_base/config"
    , "esri/geometry/geodesicUtils"
    , "esri/geometry/webMercatorUtils"
    , "dojo/string"
], function (declare, _WidgetBase, _TemplatedMixin, template, lang, on, array, TextBox, Select, Button, domStyle, Units, Common, Converter, InfoTemplateManager, jsonUtils, Polygon, Polyline, Point, Graphic, LinearUnit, Geoprocessor, FeatureSet, graphicsUtils, connect, SpatialReference, require, wvsConfig, geodesicUtils, webMercatorUtils, string) {
    return ViewShed = declare([_WidgetBase, _TemplatedMixin], {

        templateString: template,

        declaredClass: "wvs.dijits.ViewShed",

        polygonSymbol: { "color": [244, 164, 96, 117], "outline": { "color": [0, 0, 0, 255], "width": 1.5, "type": "esriSLS", "style": "esriSLSDash" }, "type": "esriSFS", "style": "esriSFSSolid" },

        bufferSymbol: { "color": [127, 255, 0, 74], "outline": { "color": [255, 255, 255, 255], "width": 1.5, "type": "esriSLS", "style": "esriSLSDash" }, "type": "esriSFS", "style": "esriSFSSolid" },

        // maxDistance: Number
        //      the maximum allowable distance (in meters) for which the viewpoint should be calculated
        maxDistance: "20000",

        inputPointsParamName: "Input_Observation_Point",

        inputDistanceParamName: "Viewshed_Distance",

        title: "Viewshed",

        constructor: function (params, srcNodeRef) {
            if (!params.map) {
                throw new Error("map required for " +  this.declaredClass);
            }
            if (!params.graphic) {
                throw new Error("graphic required for " + this.declaredClass);
            }
            if (!params.gpService && !wvsConfig.defaults.services.viewShed) {
                throw new Error("gpService required for " + this.declaredClass);
            }
            lang.mixin(this, params);

            this.gpService = this.gpService || wvsConfig.defaults.services.viewShed;

            // instance vars
            this.linearUnit = new LinearUnit();
            this.linearUnit.distance = 5;
            this.linearUnit.units = "esriMiles";

            this.gp = new Geoprocessor(this.gpService);
            this.gp.outSpatialReference = this.map.spatialReference;
            this.gp.processSpatialReference = new SpatialReference(4326);

            this.title = this.graphic.geometry.isInstanceOf(Polyline) ? "Line of Sight" : "Viewshed";

            this.graphics = [];
            this.polygonSymbol = jsonUtils.fromJson(this.polygonSymbol);
            this.bufferSymbol = jsonUtils.fromJson(this.bufferSymbol);
            this.markupLayer = this.markupLayer ? this.markupLayer : wvsConfig.defaults.markupLayer ? wvsConfig.defaults.markupLayer : null;
            this.graphicsLayer = this.markupLayer || this.map.graphics;
        },
        postCreate: function () {
            var self = this;
            this.inherited(arguments);

            if (this.graphic.geometry.isInstanceOf(Point)) {
                domStyle.set(this.pointOptions, { display: "block" });
                this.distanceTextBox = new TextBox({
                    value: this.linearUnit.distance,
                    onChange: lang.hitch(this, function(value){ 
                        this.linearUnit.distance = value;
                    }),
                    style: "width:100px;"
                }, this.distanceTextBox);


                this.unitSelect = new Select({
                    options: [{ label: "Kilometers", value: Units.KILOMETERS },
                        { label: "Meters", value: Units.METERS },
                        { label: "Miles", value: Units.MILES }
                    ],
                    onChange: lang.hitch(this, function (value) {
                        this.linearUnit.units = value;
                    })
                }, this.unitSelect);
            }

            this.getViewshedButton = new Button({
                label: this.graphic.geometry.isInstanceOf(Point) ?  "Get Viewshed" : "Get Line of Sight",
                onClick: lang.hitch(this, this._getViewshed)
            }, this.getViewshedButton);

            //connect.publish("map-acquire", { target: this, acquired: true });

            //this.own(on(this.map, "click", lang.hitch(this, this._getViewshed)));
        },
        destroy: function () {
            if(!this.markupLayer)
                this._removeGraphics();
            this.inherited(arguments);
        },
        _removeGraphics: function () {
            array.forEach(this.graphics, function (g) {
                this.graphicsLayer.remove(g);
            }, this);
            this.graphics = [];
        },
        _getViewshed: function (evt) {
            var geometry = this.graphic.geometry,
                pt = geometry.isInstanceOf(Polyline) ? geometry.getExtent().getCenter() : geometry;
            if (this.graphic.geometry.isInstanceOf(Polyline)) {
                var startPointLine = new Polyline(this.map.spatialReference);
                startPointLine.addPath([geometry.getPoint(0, 0), pt]);
                var startLength = startPointLine.getLength(Units.METERS);
                this.linearUnit.distance = startLength;
                this.linearUnit.units = Units.METERS;
            }
            if (this._isViewShedValid()) {
                this._removeGraphics();
                // Draw viewshed buffer
                var bufferPoly = new Polygon(this.map.spatialReference);
                bufferPoly.addRing(Common.buildBufferPointArray(this.map, pt, this.linearUnit.distance, this.linearUnit.units, 60));
                var bufferGraphic = new Graphic(bufferPoly
                    , this.bufferSymbol
                    , { description: "" }
                    , null);
                this.graphics.push(bufferGraphic);
                bufferGraphic.setInfoTemplate(this._parent.getTabularAttributeTemplate({ title: string.substitute("${toolTitle} Buffer - ${radius} ${units}", { toolTitle: this.title, radius: this.linearUnit.distance, units: this.linearUnit.units.replace(/esri/, "") }) }));

                this.graphicsLayer.add(bufferGraphic);
                var moveToBack = setInterval(function () {
                    if (typeof bufferGraphic.getShape === "function") {
                        bufferGraphic.getShape().moveToBack();
                        clearInterval(moveToBack);
                    }
                }, 20);
                
                


                var graphic = new Graphic(pt, this.pointSymbol);
                //this.graphics.push(graphic);
                //map.graphics.add(graphic);

                var features = [];
                features.push(graphic);
                var featureSet = new FeatureSet();
                featureSet.features = features;
                var params = {};
                params[this.inputPointsParamName] = featureSet;
                params[this.inputDistanceParamName] = this.linearUnit;
                this.getViewshedButton.set("iconClass", "icon-spinner icon-spin");
                this.getViewshedButton.set("disabled", true);
                this.gp.execute(params,
                    lang.hitch(this, this._drawViewShed),
                    lang.hitch(this, this._gpError));
            }
        },
        _gpError: function(message){
            this.getViewshedButton.set("iconClass", "");
            this.getViewshedButton.set("disabled", false);
            Common.errorDialog(message || "There was an error processing this view shed");
        },
        _drawViewShed: function (results, messages) {
            this.getViewshedButton.set("iconClass", "");
            this.getViewshedButton.set("disabled", false);
            var features = results[0].value.features,
                map = this.map;
            for (var f = 0, fl = features.length; f < fl; f++) {
                var feature = features[f];
                feature.setSymbol(this.polygonSymbol);
                feature.setInfoTemplate(this._parent.getTabularAttributeTemplate({ title: string.substitute("${toolTitle} - ${radius} ${units}", { toolTitle: this.title, radius: this.linearUnit.distance, units: this.linearUnit.units.replace(/esri/, "") }) }));
                this.graphicsLayer.add(feature);
                this.graphics.push(feature);
            }
            map.setExtent(graphicsUtils.graphicsExtent(this.graphics), true);
        },
        _isViewShedValid: function () {
            var distance = parseFloat(this.linearUnit.distance);

            if (isNaN(distance) ||
                distance <= 0 ||
                (this.graphic.geometry.isInstanceOf(Point) && Converter.convertUnits(distance, this.unitSelect.value, Units.METERS) > this.maxDistance) ||
                (this.graphic.geometry.isInstanceOf(Polyline) && distance > this.maxDistance)
                ) {
                Common.errorDialog("Invalid distance: The distance must be between 0 and " + this.maxDistance + " meters");
                return false;
            }
            
            return true;
        }
    });
});