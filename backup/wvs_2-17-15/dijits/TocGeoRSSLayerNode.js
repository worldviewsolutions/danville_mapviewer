console.log("wvs/dijits/TocMapServiceNode.js");

define([
    "dojo/_base/declare"
    , "dojo/text!./templates/TocGeoRSSLayerNode.html"
    , "dojo/_base/lang"
    , "./_TocNode"
    , "dijit/form/HorizontalSlider"
    , "dojo/dom-style"
    , "dojo/ready"
    , "dojo/dom-construct"
    , "dojo/dom-attr"
    , "dojo/on"
    , "dijit/Menu"
    , "dijit/MenuItem"
    , "dijit/MenuSeparator"
    , "dijit/PopupMenuItem"
    , "dijit/TooltipDialog"
    , "dojo/_base/array"
    , "./TocFeatureLayerNode"
    , "dojo/dom-class"
    , "dojo/has"
    , "dojo/dom"
    , "dojo/_base/sniff"
], function (declare, template, lang, _TocNode, HorizontalSlider, domStyle, ready, domConstruct, domAttr, on, Menu, MenuItem, MenuSeparator, PopupMenuItem, TooltipDialog, array, TocFeatureLayerNode, domClass, has, dom) {
    return declare('wvs.dijits.TocGeoRSSLayerNode', [_TocNode], {

        templateString: template,

        // refreshDelay: Integer
        //      Number of milliseconds to delay between clicking a checkbox and refreshing the map service. A longer delay allow the user to check/uncheck multiple
        //      layers before refreshing the map service."dojo/ready",
        refreshDelay: 500,

        // useSpriteImage: Boolean
        //      Use the custom sprite image for the legend icons. When false, the "Legend" REST service will be used for 10.01 and newer services.
        useSpriteImage: false,

        constructor: function (params, srcNodeRef) {
            console.log(this.declaredClass + " constructor");
            params = params || {};
            if (!params.mapService) {
                throw new Error('mapService not defined in params for TocMapServiceNode');
            }
            if (!params.toc) {
                throw new Error('no reference to the toc found for jTocMapServiceNode');
            }

            lang.mixin(this, params);

            if (this.title === null) {
                // call the extension method in jstoolkit/layers/Layer.js to get the service name
                this.title = this.mapService.getServiceName();
            }

            
        },
        // extension point called by framework
        postCreate: function () {
            console.log(this.declaredClass + " postCreate");
            
            domClass.remove(this.expandoNode, "jstoolkit-hidden");
            domClass.add(this.labelNode, "group-title");
            this._createChildNodes();

            // TODO: Enable Legened for GeoRSS feeds
            //this.mapService.getLegend(lang.hitch(this, this._createChildNodes), this.useSpriteImage);

            this.set('transparency',0);
        },
        startup: function(){
            console.log(this.declaredClass + ' startup');
            this.inherited(arguments);
            this._createContextMenu();
        },
        setVisible: function (visible) {
            // return if the visibility has not changed
            if (this._state.visible == visible) return;

            this._state.visible = visible;

            // update the map service
            if (visible) {
                this.mapService.show();
                this._showSwatch();
                this._enableChildNodes();
            } else {
                this.mapService.hide();
                this._hideSwatch();
                this._disableChildNodes();
            }
        },
        _checkboxOnClick: function (e) {
            this.setVisible(this._checkbox && this._checkbox.checked);
        },

        _refreshLayer: function () {
            if (this._refreshTimer) {
                window.clearTimeout(this._refreshTimer);
                this._refreshTimer = null;
            }

            var mapService = this.mapService;

            this._refreshTimer = window.setTimeout(function () {
                mapService.refresh();
            }, this.refreshDelay);
        },
        _createContextMenu: function () {
            console.log(this.declaredClass + ' _createContextMenu');
            var self = this;

            var mapServiceOptionsMenu = new Menu({
                leftClickToOpen: true,
                onFocus: function () {
                    var children = this.getChildren(),
                        nodeBottom = self.toc.isBottomNode(self),
                        nodeTop = self.toc.isTopNode(self);

                    array.forEach(children, function (entry, i) {
                        if (entry.label) {
                            switch (entry.label) {
                                case "Move Up":
                                    entry.set("disabled", nodeTop); break;
                                case "Move Down":
                                    entry.set("disabled", nodeBottom); break;
                            }
                        }
                    });
                }
            });
            mapServiceOptionsMenu.bindDomNode(this.dropDownMenuIcon);
            // NOTE: esri doesn't support changing the opacity of feature layers in IE8 and below. Stoopid.
            if (!has("ie") ||  has("ie") > 8) {
                var transparencyLabel = domConstruct.create("label", { innerHTML: "<span id='GeoRSSLayerNode_transparencyLabel_" + this.dijitIndex + "'>" + this.transparency + "</span>% transparent" });
                var transparencyDialog = new TooltipDialog({
                    content: "<label><span id='GeoRSSLayerNode_transparencyLabel_" + this.dijitIndex + "'>" + this.transparency + "</span>% transparent</label>"
                });
                var transparencySlider = new HorizontalSlider({
                    name: "transparencySlider",
                    value: self.transparency,
                    minimum: 0,
                    maximum: 1,
                    showButtons: true,
                    style: "width:300px",
                    onChange: lang.hitch(self, self._onTransparencySliderChange)
                });
                var transparencyMenuItem = new PopupMenuItem({
                    label: "Transparency",
                    popup: transparencyDialog,
                    iconClass: "jstoolkit-icon jstoolkit-icon-transparency"
                });
                transparencyDialog.addChild(transparencySlider);
                mapServiceOptionsMenu.addChild(transparencyMenuItem);
                mapServiceOptionsMenu.addChild(new MenuSeparator());
            }

            mapServiceOptionsMenu.addChild(new MenuItem({
                label: "Remove",
                iconClass: "jstoolkit-icon jstoolkit-icon-delete",
                onClick: lang.hitch(self.toc, self.toc._removeServiceLayer, self.mapService)
            }));


            /*
            NOTES: For the time being, Feature layers, GeoRSS layers, and KML layers will be at the top of the TOC and not movable.
            This is due to esri's convoluted way of storing and identifying layers in the map.
            
            var _moveUpMenuItem = new MenuItem({
                label: "Move Up",
                iconClass: "jstoolkit-icon jstoolkit-icon-arrow-up",
                onClick: lang.hitch(self.toc, self.toc._moveNodeUp, self)
            });

            mapServiceOptionsMenu.addChild(_moveUpMenuItem);

            var _moveDownMenuItem = new MenuItem({
                label: "Move Down",
                iconClass: "jstoolkit-icon jstoolkit-icon-arrow-down",
                onClick: lang.hitch(self.toc, self.toc._moveNodeDown, self)
            });
            mapServiceOptionsMenu.addChild(_moveDownMenuItem);
            */

            mapServiceOptionsMenu.startup();
            this.mapServiceOptionsMenu = mapServiceOptionsMenu;
        },
        _createChildNodes: function () {
            console.debug(this.declaredClass + ": _createChildNodes()");
            var featureLayers = this.mapService.getFeatureLayers();
            array.forEach(featureLayers, function (featureLayer) {
                var node = new TocFeatureLayerNode({
                    _parent: this,
                    mapService: featureLayer,
                    featureLayerType: 'georss',
                    refreshDelay: this.refreshDelay,
                    useSpriteImage: this.useSpriteImage
                });
                node.placeAt(this.childrenNode);
                node.startup();
                this.nodes.push(node);
            }, this);

            this._updateVerticalAlignment();
        },
        // Event Handlers
        /*
        *  Changes the transparency of the associated map layer
        *  @author: Eddie Stathopoulos
        */
        _onTransparencySliderChange: function (value) {
            console.log(this.declaredClass + " _onTransparencySliderChange");
            var opacity = 1 - value,
                layer = this.mapService;
            array.forEach(layer.getFeatureLayers(), function (featureLayer) {
                featureLayer.setOpacity(opacity);
            });
            dom.byId("GeoRSSLayerNode_transparencyLabel_" + this.dijitIndex).innerHTML = Math.round(value * 10000) / 100;
        }

    });
});