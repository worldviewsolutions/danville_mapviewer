﻿define(["dojo/_base/declare", "dojo/_base/lang", "esri/request"], function (declare, lang, esriRequest) {
    // module:
    //      wvs/common/UrlShortener
    // description:
    //      a module to provide minified URLs. Minify services can be added to the serviceSettings object. The key will be the name of the service as well as the name of the function that will perform the minification logic
    return declare(null, {
        settings: {
            defaultService: "snipUrl",
            serviceSettings: {
                snipUrl: {
                    apiEndpoint: "http://snipr.com/site/getsnip",
                    restSettings: {
                        snipuser: "eddiestat",
                        snipapi: "565e94c4212110c10c1cb35cdc0389dc",
                        snipformat: "simple",
                        sniplink: ""
                    },
                    responseType: "text"
                }
            }
        },
        declaredClass: "wvs.common.UrlShortener",
        constructor: function(url){
            if (!url) {
                throw new Error("url needed for " + this.declaredClass);
            }
            this.url = url;
        },
        shorten: function (serviceName) {
            // summary:
            //      shortens the url
            // serviceName: String
            //      the name of the service to perform URL minification
            // returns:
            //      minified url
            var serviceName = serviceName || this.settings.defaultService;
            return this[serviceName](this.url); // String
        },
        setUrl: function (url) {
            // summary:
            //      sets the URL to be minified
            // url: String
            //      the url to be minified
            // returns:
            //      whether setting the url was successful
            if (url && typeof url === "string") {
                this.url = url;
                return true; // Boolean
            }

            return false; // Boolean
        },
        snipUrl: function (url) {
            // summary:
            //      the utility method used to use snipr.com URL-minification
            // url: String
            //      the url to minify
            // returns:
            //      a deferred object
            var postData = lang.mixin({}, this.settings.serviceSettings.snipUrl.restSettings);
            postData.sniplink = url;
            var request = esriRequest({
                url: this.settings.serviceSettings.snipUrl.apiEndpoint,  
                content: postData,
                handleAs: 'text'
            }, {
                usePost: true
            });

            return request; // Deferred
        }
    });
});