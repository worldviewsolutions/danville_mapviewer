﻿// Utility class for handling Javascript objects
define([
    "dojo/_base/lang"
], function (lang) {
    //Singleton class defining utility functions for cloning and comparing javascript Objects fully and recursively.
    return {

        /*@function clone
        *       This function will iterate through the entire object tree for the given object and recursively copy every
        *       value for every property into a completely new object, distinct from the original. The only things that won't
        *       get copied over will be functions.
        * @param obj: Object
        *       The object to be cloned, recursively.
        * @return temp: Object
        *       The new object, distinct in memory from the original, that is a clone of the complete object tree passed in.
        */
        clone: function (obj) {
            var temp = new obj.constructor();
            for (var prop in obj) {
                if (obj[prop] == null || typeof (obj[prop]) != 'object') {
                    temp[prop] = obj[prop];
                } else if (obj.hasOwnProperty(prop)) {
                    temp[prop] = this.clone(obj[prop]);
                }
            }
            return temp;
        },

        /*@function compare
        *       This function will recursively compare every property in the object trees of two objects until either
        *       both trees are exhausted, or a discrepancy is found between the two objects.  This does a soft compare
        *       indicating if all properties of the same name at the same level of the object tree match, instead of
        *       comparing pointers to see if variables point to the same objects in memory.
        * @param obj1: Object
        *       The first object in the comparison
        * @param obj2: Object
        *       The object being compared to obj1
        * @return bool
        *       Indicates the result of the comparison.
        */
        compare: function (obj1, obj2) {
            if ((obj1 == null || typeof (obj1) != 'object') && (obj2 == null || typeof (obj2) != 'object')) {
                return obj1 === obj2;
            }
            var match = false;
            for (var prop in obj1) {
                if (obj2[prop]) {
                    match = this.compare(obj1[prop], obj2[prop]);
                    if (!match) { return match; }
                }
                else {
                    return false;
                }
            }
            for (var prop in obj2) {
                if (obj1[prop]) {
                    match = this.compare(obj1[prop], obj2[prop]);
                    if (!match) { return match; }
                }
                else {
                    return false;
                }
            }
            return true;
        }

    };
}
);