﻿define([
    "dojo/_base/declare"
    , "dojo/_base/lang"
    , "dojo/_base/array"
    , "esri/layers/layer"
    , "dojo/dom-style"
    , "dojo/Evented"
    , "dojo/dom-construct"
], function (
    declare
    , lang
    , array
    , Layer
    , domStyle
    , Evented
    , domConstruct
) {
    return declare([Layer, Evented], {
        // NOTES: 4-9-14 EIS
        // In some cases, feature layers or other layers are 'visually grouped together' to the user. I.E. ArcGIS.com Feature Collection layers (feature layers) which could be their Map Markup or imported shape data
        // This is a container format ONLY, similar to ArcGISWebMapLayer. In fact, it's essentially the same overriden layer methods from ArcGISWebMapLayer.
        // For the time being, ONLY group FEATURE COLLECTIONS (feature layers) as there is no handling for deferred load events.
        declaredClass: "wvs.layers.GroupLayer", 

        title: "",

        constructor: function(options){
            this.inherited(arguments);
            this.layers = [];
            lang.mixin(this, options || {});
            array.forEach(this.layers, function (l) {
                l._groupLayer = true;
                if (options._webmap)
                    l._webmap = true;
            });
            if (this.layers.length) {
                this.loaded = true;
                this.emit("load", { layer: this });
            }
            else {
                this.emit("error", { layer: this });
            }
        },
        getLayers: function(){
            return this.layers;
        },
        /* Start Overriden Layer functions */
        _setMap: function (map, container) {
            this._map = map;
            var style = {
                position: 'absolute',
                top: '0px',
                left: '0px',
                width: '0px',
                height: '0px'
            };
            var element = domConstruct.create('div', {}, container);
            if (this.id) {
                element.id = this.id;
            }
            domStyle.set(element, style);
            this._element = element;
            array.forEach(this.layers, function (layer) {
                map.addLayer(layer);
            });

            return element;
        },
        _unsetMap: function (map, layersDiv) {
            array.forEach(this.layers, function (layer) {
                map.removeLayer(layer);
            });

            this._map = null;
        },
        setVisibility: function (isVisible) {
            domStyle.set(this._element, { display: isVisible ? "block" : "none" });
            array.forEach(this.layers, function (layer) {
                layer.setVisibility(isVisible);
            });
            this.emit("visiblity-change", { target: this, visible: isVisible });
        },
        setOpacity: function (opacity) {
            array.forEach(this.layers, function (layer) {
                layer.setOpacity(opacity);
            });
            this.opacity = opacity;
            this.emit("opacity-change", { target: this, opacity: opacity });
        }
        /* End Overriden Layer functions */
    });
});