define([
    "esri/geometry",
    "esri/geometry/mathUtils",
    "esri/geometry/Point"
], function(esriGeometry, mathUtils, Point){
        return {

            getPointFromM: function(polyline, m){
                // This function assumes the M values are in assending order for each point

                // loop through each path
                for(var i = 0; i < polyline.paths.length; i++){
                    if(m < polyline.paths[i][0][0]){
                        // m value is too small and does not exist on the path
                        continue;
                    }
                    // compare the M values, skipping the first point
                    for(var p = 1; p < polyline.paths[i].length; p++){
                        var p2 = polyline.paths[i][p];
                        if(p2[2] < m){
                            continue;
                        }                        
                        var p1 = polyline.paths[i][p-1];
                        // if we got here then the m value is between point 1 and 2;
                        if(p1[2] == m){
                            return p1;
                        }

                        if(p2[2] == m){
                            return p2;
                        }

                        // convert to actual points for easier reading
                        var point1 = new Point(p1), 
                            point2 = new Point(p2);

                        point1.m = p1[2];
                        point2.m = p2[2];
                                                

                        var totalMDiff = point2.m - point1.m;
                        var partialMDiff = m - point1.m;

                        // use a ratio to calculate the point on the line based on the M value;
                        var totalLengthMapUnits =  mathUtils.getLength(point1, point2);

                        var sectionLengthMapUnits = 0;

                        if (totalMDiff * totalLengthMapUnits > 0) {
                            sectionLengthMapUnits = partialMDiff / totalMDiff * totalLengthMapUnits;
                        }
                        var ratio = sectionLengthMapUnits / totalLengthMapUnits;

                        if (!ratio || ratio < 0 || ratio > 1){
                            console.warn('getPointFromM(): something went wrong');
                            return null;
                        }
                        
                        var returnX,
                            returnY;

                        //get X distance
                        var xDiff = point2.x - point1.x;
                        xDiff = xDiff * ratio;

                        if (xDiff == 0) {
                            returnX = startPoint.x;
                        } else {
                            if (point1.x > point2.x) {
                                returnX = point1.x - xDiff;
                            } else {
                                returnX = point1.x + xDiff;
                            }
                        }

                        //get Y distance
                        var yDiff = point2.y - point1.y;
                        yDiff = yDiff * ratio;

                        if (yDiff == 0) {
                            returnY = startPoint.y;
                        } else {
                            if (point1.y > point2.y) {
                                returnY = point1.y - yDiff;
                            } else {
                                returnY = point1.y + yDiff;
                            }
                        }

                        var returnPoint = new Point(returnX, returnY);
                        returnPoint.spatialReference = polyline.spatialReference;
                        return returnPoint;
                    }
                }
            },

            // summary:
            //      Computes the distance from a point to a line defined by a fromPoint and toPoint.
            //      Note: NON-ROBUST!
            // point: esri.geometry.Point
            //      The point to compute the distance for.
            // fromPoint: esri.geometry.Point
            //      The starting point of the line.
            // toPoint: esri.geometry.Point
            //      The ending point of the line.
            // returns: Number
            //      The distance from 'point' to line defined by the fromPoint and toPoint.
            getDistancePointFromLine: function(point, fromPoint, toPoint){
                var p = point, 
                    A = fromPoint, 
                    B = toPoint;

                // if start == end, then use length method
                if (A.x == B.x && A.y == B.y)
                    return esriGeometry.getLength(p, A);

                // otherwise use comp.graphics.algorithms Frequently Asked Questions method
                /*(1)     	      AC dot AB
                            r =   ---------
                                    ||AB||^2
     
                            r has the following meaning:
                            r=0 Point = A
                            r=1 Point = B
                            r<0 Point is on the backward extension of AB
                            r>1 Point is on the forward extension of AB
                            0<r<1 Point is interior to AB
                */
                var r = ((p.x - A.x) * (B.x - A.x) + (p.y - A.y) * (B.y - A.y)) / ((B.x - A.x) * (B.x - A.x) + (B.y - A.y) * (B.y - A.y));
                if (r <= 0.0) return esriGeometry.getLength(p, A);
                if (r >= 1.0) return esriGeometry.getLength(p, B);

                /*(2)
                                (Ay-Cy)(Bx-Ax)-(Ax-Cx)(By-Ay)
                            s = -----------------------------
             	                            Curve^2

                            Then the distance from C to Point = |s|*Curve.
                */
                var s = ((A.y - p.y) * (B.x - A.x) - (A.x - p.x) * (B.y - A.y)) / ((B.x - A.x) * (B.x - A.x) + (B.y - A.y) * (B.y - A.y));
                var result = Math.abs(s) * Math.sqrt(((B.x - A.x) * (B.x - A.x) + (B.y - A.y) * (B.y - A.y)));
                return result;
            },
         
            // summary:
            //      Determines the point along the paths/rings that is closet to the input point
            // point: esri.geometry.Point
            //      The point to compute the distance for.
            // geometry: Number[][][]
            //      When working with a Polyline this would be "paths" array. When working with a Polygon this would be the "rings" array.
            // returns: esri.geometry.Point
            //      Point
            getSnapPoint: function(point, geometry) {
                var closestDistance = Number.MAX_VALUE,
                    currentDistance,
                    currentPath,
                    currentSegment,
                    fromPoint,
                    toPoint,
                    closestSegment = { fromPoint: null, toPoint: null };

                for (var p = 0; p < geometry.length; p++) {
                    currentPath = geometry[p];
                    for (var s = 1; s < currentPath.length; s++) {
                        currentSegment = currentPath[s];
                        fromPoint = { x: currentPath[s-1][0], y: currentPath[s-1][1] };
                        toPoint = { x: currentPath[s][0], y: currentPath[s][1] };
                        var currentDistance = this.getDistancePointFromLine(point, fromPoint, toPoint);

                        if (currentDistance < closestDistance) {
                            closestDistance = currentDistance;
                            closestSegment.fromPoint = fromPoint;
                            closestSegment.toPoint = toPoint;
                        }
                    }
                }
                return this._getSnappedPoint(point, closestSegment.fromPoint, closestSegment.toPoint);
            },
                
            _getSnappedPoint: function(point, fromPoint, toPoint){
                var c = point, 
                    a = fromPoint, 
                    b = toPoint,
	                dot_ta,
                    dot_tb,
                    nearest = { x: null, y: null };

                // SEE IF a IS THE NEAREST POINT - ANGLE IS OBTUSE
                dot_ta = (c.x - a.x) * (b.x - a.x) + (c.y - a.y) * (b.y - a.y);
    
                // IT IS OFF THE AVERTEX
                if (dot_ta <= 0){
                    nearest.x = a.x;
                    nearest.y = a.y;
                    return nearest;
                }
    
                dot_tb = (c.x - b.x) * (a.x - b.x) + (c.y - b.y) * (a.y - b.y);
                // SEE IF b IS THE NEAREST POINT - ANGLE IS OBTUSE
                if (dot_tb <= 0){
                    nearest.x = b.x;
                    nearest.y = b.y;
                    return nearest;
	            }
                // FIND THE REAL NEAREST POINT ON THE LINE SEGMENT - BASED ON RATIO
                nearest.x = a.x + ((b.x - a.x) * dot_ta)/(dot_ta + dot_tb);
                nearest.y = a.y + ((b.y - a.y) * dot_ta)/(dot_ta + dot_tb);
                return nearest;
            }
        };
    }
);