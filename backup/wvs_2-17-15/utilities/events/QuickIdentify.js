﻿define([
    "dojo/_base/declare"
    , "dojo/_base/lang"
    , "./_MapEventMixIn"
    , "esri/tasks/IdentifyParameters"
    , "esri/tasks/IdentifyTask"
    , "dojo/on"
    , "dojo/_base/array"
    , "esri/graphic"
    , "../../common/InfoTemplateManager"
    , "../../common/SearchResultsAdapter"
    , "esri/symbols/jsonUtils"
    , "esri/layers/ArcGISDynamicMapServiceLayer"
    , "dojo/promise/all"
    , "dojo/_base/connect"
    , "dojo/Deferred"
    , "esri/lang"
    , "wvs/common/Common"
], function (
    declare
    , lang
    , _MapEventMixIn
    , IdentifyParameters
    , IdentifyTask
    , on
    , array
    , Graphic
    , InfoTemplateManager
    , SearchResultsAdapter
    , symbolJsonUtils
    , ArcGISDynamicMapServiceLayer
    , all
    , connect
    , Deferred
    , esriLang
    , Common
) {
    var QuickIdentifyMapEvent = declare([_MapEventMixIn], {

        // tolerance: Integer
        //      the tolerance used for identify
        tolerance: 5,

        // bindToResultsStore: Boolean
        //      the identify should send its results to the searchResults Store
        bindToResultsStore: false,

        // showSingleResult: Boolean
        //      show a single result based on closest feature to the click
        showSingleResult: true,

        // _store: SearchResultStore
        //      the search result store that the identify operation may bind to
        store: null,

        disableOnMapAcquire: true, // Overriden property from parent; should not be touched as this behavior is desired.
        constructor: function(map, options){

            // _subLayerPopupMap: Object
            //      a map that holds popups for sublayer of dynamic layers. This is mainly used for ArcGIS Online integration
            this._subLayerPopupMap = {};

            this._subscriptions.push(connect.subscribe("sublayer-popup", lang.hitch(this, this._addSubLayerPopup)));
        },
        initialize: function () {
            // summary:
            //      binds the event to the map
            // returns:
            //      the event handler from dojo/on
            var self = this,
                map = this._map;
            
            var click = on(map, "click", function (evt) {

                // EIS - 1/19/15
                // NOTE: Starting at 3.10 or later, esri changed the way they handle click events on the map
                // Now a map click happens regardless if a client-side feature was clicked or not. We have to inspect the MouseEvent to ensure this is not the case.
                if (evt.graphic) {
                    return;
                }

                if (self.enabled) {
                    var p = evt.mapPoint;
                    var deferreds = [];
                    var features = [];

                    //var placeGraphic = new Graphic(p, symbolJsonUtils.fromJson(wvsConfig.defaults.pointSymbol), {}, InfoTemplateManager.getTabularAttributeTemplate({ title: "Quick Identify", createWidgetsFromGeometry: true, useGlobalSearchStore: bindToResultsStore }));
                    //if (wvsConfig.defaults.markupLayer)
                    //    wvsConfig.defaults.markupLayer.add(placeGraphic);
                    //else
                    //    map.graphics.add(placeGraphic);
                    //map.infoWindow.hide();
                    //map.infoWindow.setContent(placeGraphic.getContent());
                    //map.infoWindow.setTitle(placeGraphic.getTitle());
                    //map.infoWindow.show(p);


                    array.forEach(map.layerIds, function (layerId) {
                        var layer = map.getLayer(layerId);
                        if (layer.isInstanceOf(ArcGISDynamicMapServiceLayer) && !layer._hidden) {
                            var dfd = self._processIdentify(layer, p);
                            if(dfd)
                                deferreds.push(dfd);
                        }
                    });

                    if (deferreds.length === 0)
                        return;

                    map.infoWindow.hide();
                    map.setMapCursor("wait");

                    all(deferreds).then(
                        function (results) {
                            map.setMapCursor("default");
                            array.forEach(results, function (identifyResults) {
                                array.forEach(identifyResults.results, function (result) {
                                    var popupInfo = self._subLayerPopupMap[identifyResults.layer.id] && self._subLayerPopupMap[identifyResults.layer.id][result.layerId] ? self._subLayerPopupMap[identifyResults.layer.id][result.layerId] : null;
                                    result.feature.setInfoTemplate(InfoTemplateManager.getTabularAttributeTemplate({ title: result.layerName + " - " + result.value, createWidgetsFromGeometry: true, useGlobalSearchStore: self.bindToResultsStore, popupInfo: popupInfo, featureLayer: identifyResults.layer.url + "/" + result.layerId }));
                                    result.feature.geometry.setSpatialReference(p.spatialReference);
                                    features.push(result.feature);
                                });
                            });
                            if (features.length) {
                                console.log("quick identify features", features);
                                if (self.showSingleResult)
                                    map.infoWindow.setFeatures([features[0]]);
                                else
                                    map.infoWindow.setFeatures(features);
                                map.infoWindow.show(p, { closestFirst: true });
                            }
                        }
                    );
                }
            });

            this._event = click;
            this.enable();
            return click; // EventHandler
        },
        _addSubLayerPopup: function(layerPopupInfo){
            // summary: 
            //      add a dynamic service sublayer popup to be processed after an identify
            // sublayerPopup: Object
            //      an object that contains the dynamic layer, the sublayer id, and the popup to process. { layer: layerObject, sublayerId: id_of_sublayer, popup: popup_info_for_template}
            if (!esriLang.isDefined(this._subLayerPopupMap[layerPopupInfo.layer.id]))
                this._subLayerPopupMap[layerPopupInfo.layer.id] = {};
            if (!esriLang.isDefined(this._subLayerPopupMap[layerPopupInfo.layer.id][layerPopupInfo.sublayerId]))
                this._subLayerPopupMap[layerPopupInfo.layer.id][layerPopupInfo.sublayerId] = layerPopupInfo.popup;
        },
        _processIdentify: function (layer, geometry) {
            // summary:
            //      given a layer and a geometry, perform an identify task on all visible sub-layers, and optionally send to a search results store
            // layer: Layer
            //      the layer to identify on
            // geometry: Geometry
            //      the geometry to use for the identify
            // returns:
            //      a deferred object
            var layerIds = array.map(
                array.filter(layer.layerInfos, function (layerInfo) {
                    return !layerInfo.subLayerIds && layerInfo.getVisible() ;
                }),
                function (visibleLayerInfo) {
                    return visibleLayerInfo.id;
                });

            if (layerIds.length === 0)
                return null;

            var identifyParameters = new IdentifyParameters(),
                store = self.store,
                bindToResultsStore = self.bindToResultsStore,
                dfd = new Deferred();

            identifyParameters.mapExtent = map.extent;
            identifyParameters.height = map.height;
            identifyParameters.width = map.width;
            identifyParameters.geometry = geometry;
            identifyParameters.tolerance = this.tolerance;
            identifyParameters.returnGeometry = true;
            identifyParameters.layerOption = IdentifyParameters.LAYER_OPTION_ALL;

            var identifyUrl = layer.url;
            var identifyTask;
            identifyParameters.layerIds = layerIds;
            identifyTask = new IdentifyTask(identifyUrl);


            if (bindToResultsStore) {
                var searchResultsAdapter = new SearchResultsAdapter();
                store.clear();
                store.setLoadingData(true);
            }

            identifyTask.execute(identifyParameters,
                function (identifyResults) {
                    dfd.resolve({ layer: layer, results: identifyResults });
                    if (bindToResultsStore) {
                        var transformedResults = searchResultsAdapter.identifyToResultSet(identifyResults, identifyUrl);
                        transformedResults.then(
                            function (searchResult) {
                                store.put(searchResult);
                            }
                        );
                    }
                },
                function (error) {
                    if (bindToResultsStore)
                        store.setLoadingData(false);
                    dfd.reject(error);
                }
            );

            return dfd; // Deferred
        }
    });

    return QuickIdentifyMapEvent;
});