﻿console.log("wvs/common/Bookmark.js");

define(["dojo/_base/declare"
    , "dojo/_base/lang"
], function (declare, lang) {

    return declare([], {
        name: null,
        extent: null,
        mapState: null,
        children: null,

        constructor: function (params) {
           lang.mixin(this, params);
        },

        hasChildren: function () {
           return (this.children != null);
        },

        hasExtent: function () {
           return (this.extent == null);
        },

        toJson: function(){
            var returnObj = {
                name: this.name,
                extent: this.extent,
                children: this.children
            };
            if (this.mapState) {
                returnObj.mapState = this.mapState;
            }
        }
    });

});