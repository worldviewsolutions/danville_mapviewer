﻿// Utility class for handling Javascript objects
define([
    "dojo/_base/lang"
], function (lang) {
   return {

      clone: function (obj) {
         var temp = new obj.constructor();
         for (var prop in obj) {
            if (obj[prop] == null || typeof (obj[prop]) != 'object') {
               temp[prop] = obj[prop];
            } else if (obj.hasOwnProperty(prop)) {
               temp[prop] = this.clone(obj[prop]);
            }
         }
         return temp;
      },

      compare: function (obj1, obj2) {
         if ((obj1 == null || typeof (obj1) != 'object') && (obj2 == null || typeof (obj2) != 'object')) {
            return obj1 === obj2;
         }
         var match = false;
         for (var prop in obj1) {
            if (obj2[prop]) {
               match = this.compare(obj1[prop], obj2[prop]);
               if (!match) { return match; }
            }
            else {
               return false;
            }
         }
         for (var prop in obj2) {
            if (obj1[prop]) {
               match = this.compare(obj1[prop], obj2[prop]);
               if (!match) { return match; }
            }
            else {
               return false;
            }
         }
         return true;
      }

   };
}
);