﻿define(["dojo/_base/array"
        , "dojo/_base/lang"
        , "dojo/has"
], function (array, lang, has) {
    var SaveManager = {
        saveToLocalStorage: function(key, json){
            if (has("local-storage")) {
                localStorage.setItem(key, JSON.stringify(json));
            }
            else {
                throw new Error("attempting to save to local storage but this browser does not support it");
            }
        }
        // Markup
        // Basemap
        // Extent
        // Layers
        // Bookmarks
        // 
    };


});