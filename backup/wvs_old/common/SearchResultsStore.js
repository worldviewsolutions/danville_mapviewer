﻿console.log("wvs/common/SearchResultsStore.js");

define(["dojo/_base/declare", "dojo/_base/lang", "dojo/_base/array", "dojo/store/Observable", "dojo/store/Memory", "dojo/Evented"],
    function (declare, lang, array, Observable, Memory, Evented) {
        // module:
        //      wvs/common/SearchResultsStore
        // description:
        //      an encapsulated store to manage Search Results
        return declare("wvs.common.SearchResultsStore", [Evented], {
            SEARCH_RESULT_LIMIT: 20,
            constructor: function() {
                this._searchNumber = 0;
                this._store = new Observable(Memory({ data: [], idProperty: "searchId" }));
            },
            put: function (SearchResult) {
                var self = this;
                if (this._store.data.length === this.SEARCH_RESULT_LIMIT) {
                    this._store.remove(0);
                }
                this._store.put(lang.mixin({ searchId: this._searchNumber }, { searchResult: SearchResult }));
                
                this._searchNumber++;
            },
            get: function (searchId) {
                return this._store.get(searchId);
            },
            remove: function (searchId) { 
                return this._store.remove(searchId);
            },
            query: function (query, options) {
                return this._store.query(query, options);
            },
            clear: function () {
                var indices = lang.clone(this._store.index);
                for (var key in indices) {
                    this._store.remove(key);
                }
            },
            setLoadingData: function (loading) {
                if (loading) {
                    this.emit("data-loading");
                }
                else{
                    this.emit("data-loaded");
                }
            }
        });
    });