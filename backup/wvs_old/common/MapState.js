﻿console.log("wvs/common/MapState.js");

define([
    "dojo/_base/declare"
    , "dojo/_base/lang"
    , "dojo/_base/array"
    , "dojo/on"
    , "dojo/has"
    , "esri/layers/ArcGISDynamicMapServiceLayer"
    , "esri/layers/ArcGISImageServiceLayer"
    , "esri/layers/FeatureLayer"
    , "esri/layers/KMLLayer"
    , "../extensions/esri/layers/MarkupLayer"
    , "../extensions/esri/layers/ArcGISWebMapLayer"
    , "../extensions/esri/layers/ArcGISImageServiceLayer"
    , "../extensions/esri/layers/KMLLayer"
], function (declare, lang, array, on, has, ArcGISDynamicMapServiceLayer, ArcGISImageServiceLayer, FeatureLayer, KMLLayer, MarkupLayer, ArcGISWebMapLayer) {
    // module: 
    //      wvs/common/MapState
    // description:
    //      the map state object is used to capture the state of the map and serialize it into a JSON structure
    var MapState = declare(null,{

        declaredClass: 'wvs.common.MapState',

        supportedLayerTypes: [ArcGISDynamicMapServiceLayer, ArcGISImageServiceLayer, KMLLayer, MarkupLayer, ArcGISWebMapLayer],

        supportedGraphicsLayerTypes: [FeatureLayer],

        constructor: function (map) {
            this.map = map;
            this.exclusionIds = [];
            
            this.extent = null;
            this.basemap = null;
            this.layers = [];
        },
        update: function () {
            // summary:
            //      updates the map state based on the map
            this.extent = this.map.extent.toJson();
            this.basemap = this.map.getBasemap();
            this.layers = [];

            var centerPoint = this.map.extent.getCenter();
            this.lat = centerPoint.getLatitude();
            this.lon = centerPoint.getLongitude();
            this.scale = this.map.getScale();

            // save layers
            var layerIds = this.map.layerIds.concat(this.map.graphicsLayerIds);
            array.forEach(layerIds, function (layerId) {
                var layer = this.map.getLayer(layerId);
                if (this.isSerialized(layer)) {
                    if (layer.isInstanceOf(KMLLayer)) {
                        this.exclusionIds = this.exclusionIds.concat(array.map(layer.getLayers(), function (kmlLayers) {
                            return kmlLayers.id;
                        }));
                    }
                    this.layers.push(layer.toJson());
                }
            }, this);

            //var graphicsLayerIds = this.map.graphicsLayerIds;
            //array.forEach(graphicsLayerIds, function (graphicsLayerId) {
            //    var layer = this.map.getLayer(graphicsLayerId);
            //    if (this.isSerialized(layer, true)) {
            //        this.layers.push(layer.toJson());
            //    }
            //}, this);


        },
        addExclusion: function (layerId) {
            // summary:
            //      adds a layer to the exclusion list by the layer's id
            // layerId: String
            //      the id of the layer to exclude
            var i = array.indexOf(this.exclusionIds, layerId);
            if (i === -1)
                this.exclusionIds.push(layerId);
        },
        removeExclusion: function (layerId) {
            // summary:
            //      removes a layer from the exclusion list by the layer's id
            // layerId: String
            //      the id of the layer to remove from the exclusion list
            var i = array.indexOf(this.exclusionIds, layerId);
            if (i !== -1)
                this.exclusionIds.splice(i, 1);
        },
        toJson: function () {
            // summary:
            //      creates a JSON object of the map state
            this.update();
            var mapExport = {
                map: {
                    extent: this.extent,
                    basemap: this.basemap,
                    center: [this.lon, this.lat],
                    scale: this.scale
                },
                layers: this.layers
            };
            return mapExport; // Object
        },
        isSerialized: function (layer, isGraphicsLayer) {
            // summary:
            //      determines if the given layer is to be serialized (aka included in the map state)
            // layer: Layer
            //      the layer to check
            // isGraphicsLayer: Boolean
            //      whether or not the given layer is a graphics layer
            var isGraphicsLayer = typeof isGraphicsLayer === "boolean" ? isGraphicsLayer : false;
            if (isGraphicsLayer) {
                return !layer._visibleInToc && !layer._webmap && !layer._markupLayer &&
                    array.some(this.supportedGraphicsLayerTypes, function (supportedGraphicsLayerType) { return layer.isInstanceOf(supportedGraphicsLayerType); }) &&
                    !array.some(this.exclusionIds, function (exclusionId) { return layer.id === exclusionId; });
            }
            else{
                return  !layer._webmap && array.some(this.supportedLayerTypes, function (supportedLayerType) { return layer.isInstanceOf(supportedLayerType); }) &&
                    !array.some(this.exclusionIds, function (exclusionId) { return layer.id === exclusionId; });
            }
        }


    });

    return MapState;

});