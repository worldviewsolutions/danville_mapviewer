﻿console.log("wvs/common/SearchResultsAdapter.js");

define(["dojo/_base/declare"
    , "dojo/_base/lang"
    , "dojo/_base/array"
    , "dojo/Deferred"
    , "./SearchResult"
    , "./esriUtilities"
    , "dojo/promise/all"],
    function (declare, lang, array, Deferred, SearchResult, esriUtilities, all) {
        // module: 
        //      wvs/common/SearchResultsAdapter
        // description:
        //      converts an identify or query task result to a SearchResult
        return declare([], {
            declaredClass: "wvs.common.SearchResultsAdapter",
            featureSetToResultSet: function (featureSet, url, hiddenFields) {
                // summary:
                //      converts a feature set (result form QueryTask) to a SearchResult
                // featureSet: Object
                //      the feature set to convert
                // url: String
                //      the endpoint where the QueryTask was performed
                var self = this,
                    deferred = new Deferred(),
                    layerName = null,
                    utilities = new esriUtilities(),
                    hasGeometry = featureSet.geometryType ? true : false;

                if (featureSet.features.length) {
                    // Start FeatureSet->GridData Conversion
                    //var colModel = lang.mixin({ selectColumn: selector({ label: "", selectorType: "checkbox" }) }, featureSet.fieldAliases);
                    //if (hasGeometry) {
                    //    colModel.resultGraphic = { label: "resultGraphic", hidden: true, unhideable: true };
                    //}
                    var data = array.map(featureSet.features, function (feature) {
                        if (!hasGeometry) {
                            return feature.attributes;
                        }
                        else {
                            return lang.mixin(feature.attributes, { resultGraphic: feature });
                        }
                    });
                    if (hiddenFields) {
                        for (var i = 0; i < hiddenFields.length; i++) {
                            for (var j = 0; j < data.length; j++) {
                                for (var prop in data[j]) {
                                    if (hiddenFields[i].toLowerCase() === prop.toLowerCase()) {
                                        delete data[j][prop];
                                    }
                                }
                            }
                        }
                    }
                    // End FeatureSet->GridData Conversion


                    // We need to return a deferred since we have to make an AJAX call to get the layer name
                    // We resolve the outermost deferred once we have retrieved the layer name
                    utilities.getLayerInfoFromUrl(url).then(
                        function (layerInfo) {
                            var fields = featureSet.fields || layerInfo.fields;
                            if (hiddenFields) {
                                for (var i = 0; i < hiddenFields.length; i++) {
                                    for (var j = 0; j < fields.length; j++) {
                                        if (fields[j].name.toLowerCase() === hiddenFields[i].toLowerCase()) {
                                            fields.splice(j, 1);
                                            j--;
                                        }
                                    }
                                }
                            }
                            var searchResult = new SearchResult(url, [{ hasGeometry: hasGeometry, name: layerInfo.name, id: layerInfo.id, fields: fields, data: data, displayField: layerInfo.displayField, url: url + "/" + layerInfo.id }]);
                            deferred.resolve(searchResult);
                        },
                        function (error) {
                            throw new Error("error retrieving layer info from " + url + " in " + self.declaredClass);
                        }
                    );
                }
                setTimeout(function () {
                    deferred.resolve(new SearchResult(url, []));
                }, 3000);
                return deferred;
            },
            identifyToResultSet: function (identifyResults, url, hiddenFields) {
                // summary:
                //      converts an array of IdentifyResult (result form IdentifyTask) to a SearchResult
                // identifyResults: IdentifyResult[]
                //      the identify results to convert
                // url: String
                //      the endpoint where the IdentifyTask was performed
                var deferred = new Deferred(),
                    deferreds = [],
                    searchResults = [],
                    utilities = new esriUtilities();
                if (identifyResults.length) {
                    // create grid-compatible object

                    array.forEach(identifyResults, function (identifyResult) {
                        var layerId = identifyResult.layerId,
                            layerIdIndex = -1,
                            feature = identifyResult.feature;
                        for (var i = 0, il = searchResults.length; i < il; i++) {
                            if (searchResults[i].name === identifyResult.layerName) {
                                layerIdIndex = i;
                            }
                        }

                        // TODO: Only return fields that are specified by the optional definition expressions
                        var hasGeometry = identifyResult.geometryType ? true : false
                        var dataRow = hasGeometry ? lang.mixin(feature.attributes, { resultGraphic: feature }) : feature.attributes;

                        if (layerIdIndex === -1) {
                            searchResults.push({ hasGeometry: hasGeometry ? true : false, name: identifyResult.layerName, id: identifyResult.layerId, fields: null, data: [dataRow] });
                            newLayerIdIndex = searchResults.length - 1;
                            // We need to return a deferred since we have to make an AJAX call to get the layer name
                            // We resolve the outermost deferred once we have retrieved the layer name
                            var callback = function (layerIndex, layerInfo) {
                                searchResults[layerIndex].displayField = layerInfo.displayField;
                                searchResults[layerIndex].fields = layerInfo.fields;
                                searchResults[layerIndex].relationships = layerInfo.relationships;
                                searchResults[layerIndex].url = url + "/" + layerInfo.id;
                            };
                            var layerFieldsDeferred = utilities.getLayerInfoFromUrl(url + "/" + layerId).then(
                                lang.partial(callback, newLayerIdIndex),
                                function (error) {
                                    throw new Error("error retrieving layer info from " + url + " in " + self.declaredClass);
                                }
                            );

                            deferreds.push(layerFieldsDeferred);
                        }
                        else {
                            searchResults[layerIdIndex].data.push(dataRow);
                        }


                    });

                    all(deferreds).then(function (results) {
                        console.log("search results = ", searchResults);
                        var searchResult = new SearchResult(url, searchResults);
                        deferred.resolve(searchResult);
                    });

                    return deferred;
                }
                else {
                    setTimeout(function () {
                        deferred.resolve(new SearchResult(url, []));
                    }, 1000);
                    return deferred;
                }
            }
        });
    });