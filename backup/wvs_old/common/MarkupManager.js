﻿define([
    "dojo/_base/lang"
    , "dojo/_base/declare"
    , "../_base/config"
    , "esri/request"
    , "./Common"
    , "dojo/Deferred"
], function (
    lang
    , declare
    , wvsConfig
    , esriRequest
    , Common
    , Deferred
) {

    var MarkupManager = declare([], {

        markupService: null,

        constructor: function (markupService) {
            this.markupService = markupService ? markupService : wvsConfig.defaults.services.markup ? wvsConfig.defaults.services.markup : null;

            if (!this.markupService) {
                throw new Error("Markup Manager cannot be created without a markup service");
            }
        },
        _getUniqueParam: function () {
            var d = (new Date()).getTime();
            return d + "=" + d;
        },
        getMarkup: function () {
            // summary:
            //      gets markup from the markup service/
            // returns:
            //      a deferred object for the XHR request that will resolve for the data
            var deferred = new Deferred();
            var dfd = esriRequest({
                url: this.markupService + "/Get?" + this._getUniqueParam()
            });

            dfd.then(
                function (response) {
                    if (response.status === "SUCCESS") {
                        deferred.resolve(response.data);
                    }
                    else {
                        deferred.reject(response.message);
                    }
                },

                function (error) {
                    deferred.reject(error.message);
                }
            );

            return deferred; // Deferred
        },

        getMarkupCollection: function (id) {
            // summary:
            //      gets markup from the markup service/
            // returns:
            //      a deferred object for the XHR request that will resolve for the data
            var deferred = new Deferred();
            var dfd = esriRequest({
                url: this.markupService + "/GetCollection?id=" + id
            });

            dfd.then(
                function (response) {
                    if (response.status === "SUCCESS") {
                        deferred.resolve(response.data);
                    }
                    else {
                        deferred.reject(response.message);
                    }
                },

                function (error) {
                    deferred.reject(error.message);
                }
            );

            return deferred; // Deferred
        },

        saveMarkupCollection: function (markupLayer) {
            console.log("markupManager.saveMarkupCollection");
            // summary:
            //      a facade method that chooses whether or not to add or edit a markup collection
            if (typeof markupLayer._id === "number") {
                return this._editMarkupCollection(markupLayer); // Deferred
            }
            else {
                return this._addMarkupCollection(markupLayer); // Deferred
            }
        },
        _addMarkupCollection: function (markupLayer) {
            console.log("markupManager._addMarkupCollection");

            // summary:
            //      gets markup from the markup service/
            // returns:
            //      a deferred object for the XHR request that will resolve for the data
            var deferred = new Deferred(),
                markupLayerJson = markupLayer.toJson();


            markupLayerJson.options = JSON.stringify(markupLayerJson.options);
            var dfd = esriRequest({
                url: this.markupService + "/Add?" + this._getUniqueParam(),
                content: markupLayerJson
            }, { usePost: true });


            dfd.then(
                function (response) {

                    if (response.status === "SUCCESS") {
                        console.log("markupManager._addMarkupCollection was a success");
                        markupLayer._id = response.data;
                        deferred.resolve(response.message);
                    }
                    else {
                        console.log("markupManager._addMarkupCollection FAILED.  response status was->" + response.status);
                        deferred.reject(response.message);
                    }
                },

                function (error) {
                    console.log("markupManager._addMarkupCollection FAILED");
                    deferred.reject(error.message);
                }
            );

            return deferred; // Deferred
        },

        storeSessionMarkup: function (markupLayer) {

            // summary:
            //      gets markup from the markup service/
            // returns:
            //      a deferred object for the XHR request that will resolve for the data
            var deferred = new Deferred(),
                markupLayerJson = markupLayer.toJson();


            markupLayerJson.options = JSON.stringify(markupLayerJson.options);
            var dfd = esriRequest({
                url: this.markupService + "/AddSessionCollection?" + this._getUniqueParam(),
                content: markupLayerJson
            }, { usePost: true });


            dfd.then(
                function (response) {

                    if (response.status === "SUCCESS") {
                        console.log("markupManager._addMarkupCollection was a success");
                        markupLayer._id = response.data;
                        deferred.resolve(response.message);
                    }
                    else {
                        console.log("markupManager._addMarkupCollection FAILED.  response status was->" + response.status);
                        deferred.reject(response.message);
                    }
                },

                function (error) {
                    console.log("markupManager._addMarkupCollection FAILED");
                    deferred.reject(error.message);
                }
            );

            return deferred; // Deferred
        },

        deleteMarkupCollection: function (id) {
            // summary:
            //      gets markup from the markup service/
            // returns:
            //      a deferred object for the XHR request that will resolve for the data
            var deferred = new Deferred(),
                dfd = esriRequest({
                    url: this.markupService + "/Delete?id=" + id + "&" + this._getUniqueParam()
                });


            dfd.then(
                function (response) {
                    if (response.status === "SUCCESS") {
                        deferred.resolve(response);
                    }
                    else {
                        deferred.reject(response);
                    }
                },

                function (error) {
                    deferred.reject(error);
                }
            );

            return deferred; // Deferred
        },
        _editMarkupCollection: function (markupLayer) {
            // summary:
            //      gets markup from the markup service/
            // returns:
            //      a deferred object for the XHR request that will resolve for the data
            var deferred = new Deferred(),
                markupLayerJson = markupLayer.toJson();

            markupLayerJson.options = JSON.stringify(markupLayerJson.options);

            var dfd = esriRequest({
                url: this.markupService + "/Edit?" + this._getUniqueParam(),
                content: markupLayerJson
            }, { usePost: true });


            dfd.then(
                function (response) {
                    if (response.status === "SUCCESS") {
                        deferred.resolve(response.message);
                    }
                    else {
                        deferred.reject(response.message);
                    }
                },

                function (error) {
                    deferred.reject(error.message);
                }
            );

            return deferred; // Deferred
        }


    });

    return MarkupManager;

});