﻿define([
    "dojo/_base/declare"
    , "dojo/_base/lang"
], function(declare, lang){
    return declare('wvs.toolbars.shapes.Arrow', null, {
    
            arrowHeadWidthPixels: 30,
            barWidthPixels: 10,
            arrowLengthPixels: 100,
            theta: 0.7,

            constructor: function(options){
                lang.mixin(this, options || {});
                
                this._baseCenterPoint = {x: 0, y: 0};
                this._arrowHeadRightPoint = {x: 0, y: 0};
                this._arrowHeadLeftPoint = {x: 0, y: 0};
                this._arrowHeadRightAnchorPoint = {x: 0, y: 0};
                this._arrowHeadLeftAnchorPoint = {x: 0, y: 0};
                this._barRightPoint = {x: 0, y: 0};
                this._barLeftPoint = {x: 0, y: 0};
            },

            setFromPoint: function(point){
                this._fromPoint = point;
            },

            setToPoint: function(point){
                this._toPoint = point;
            },

            create: function(map){
        
               var tipPoint = map.toScreen(this._fromPoint);
               var endPoint = map.toScreen(this._toPoint);

                //console.debug(dojo.toJson(endPoint));
                //console.debug(dojo.toJson(tipPoint));

                var _lineHeight,
                    _lineWidth,
                    _lineLength,
                    _vecLeft = [],
                    _th,
                    _ta,
                    _thBar;     

        //        if (!this._mapUnitsPerPixel || !endPoint || !tipPoint) {
        //            //this is an error condition
        //            alert("arrow is invalid!  Missing parameters");
        //            this.isValid = false;
        //            return;
        //        }

        //        if (distance(endPoint, tipPoint) < this._mapUnitsPerPixel) {
        //            this.isValid = false;
        //            //alert("arrow is invalid!  too short");
        //            return;
        //        }

        
                this._arrowHeadMapWidth = this.arrowHeadWidthPixels; // * this._mapUnitsPerPixel;
                this._barMapWidth = this.barWidthPixels; // * this._mapUnitsPerPixel;

                ////console.debug("this._arrowHeadMapWidth = " + this._arrowHeadMapWidth);
                //console.debug("this._barMapWidth = " + this._barMapWidth);

                // build the line vector
                _lineWidth = tipPoint.x - endPoint.x;
                _lineHeight = tipPoint.y - endPoint.y;
                //console.debug("_lineWidth = " + _lineWidth);
                //console.debug("_lineHeight = " + _lineHeight);

                // build the arrow base vector - normal to the line
                _vecLeft[0] = -_lineHeight;
                _vecLeft[1] = _lineWidth;

                // setup length parameters
                _lineLength = Math.sqrt((_lineWidth * _lineWidth) + (_lineHeight * _lineHeight));
                //console.debug("_lineLength = " + _lineLength);

                // _arrowHead parameters
                _th = this._arrowHeadMapWidth / (2 * _lineLength);
                _ta = this._arrowHeadMapWidth / (2 * (Math.tan(this.theta) / 2) * _lineLength);
                //console.debug("_th = " + _th);
                //console.debug("_ta = " + _ta);

                // bar parameters
                _thBar = this._barMapWidth / (2 * _lineLength);
                //console.debug("_thBar = " + _thBar);

                // find the base of the arrow
                this._baseCenterPoint.x = tipPoint.x - (_ta * _lineWidth);
                this._baseCenterPoint.y = tipPoint.y - (_ta * _lineHeight);

                // build the outer points on the sides of the _arrowHead
                this._arrowHeadRightPoint.x = this._baseCenterPoint.x + _th * _vecLeft[0];
                this._arrowHeadRightPoint.y = this._baseCenterPoint.y + _th * _vecLeft[1];
                this._arrowHeadLeftPoint.x = this._baseCenterPoint.x - _th * _vecLeft[0];
                this._arrowHeadLeftPoint.y = this._baseCenterPoint.y - _th * _vecLeft[1];

                // build the inner anchor points of the _arrowHead
                this._arrowHeadRightAnchorPoint.x = this._baseCenterPoint.x + _thBar * _vecLeft[0];
                this._arrowHeadRightAnchorPoint.y = this._baseCenterPoint.y + _thBar * _vecLeft[1];
                this._arrowHeadLeftAnchorPoint.x = this._baseCenterPoint.x - _thBar * _vecLeft[0];
                this._arrowHeadLeftAnchorPoint.y = this._baseCenterPoint.y - _thBar * _vecLeft[1];

                //build the points on the base of the bar
                this._barRightPoint.x = (0 + endPoint.x) + (_thBar * _vecLeft[0]);
                this._barRightPoint.y = endPoint.y + _thBar * _vecLeft[1];
                this._barLeftPoint.x = endPoint.x - _thBar * _vecLeft[0];
                this._barLeftPoint.y = endPoint.y - _thBar * _vecLeft[1];
              

                // convert the screen points to map points
                var _toScreenPoint = map.toMap(tipPoint),
                    _arrowHeadLeftPoint = map.toMap(this._arrowHeadLeftPoint),
                    _arrowHeadLeftAnchorPoint = map.toMap(this._arrowHeadLeftAnchorPoint),
                    _barLeftPoint = map.toMap(this._barLeftPoint),
                    _barRightPoint = map.toMap(this._barRightPoint),
                    _arrowHeadRightAnchorPoint = map.toMap(this._arrowHeadRightAnchorPoint),
                    _arrowHeadRightPoint = map.toMap(this._arrowHeadRightPoint);

       
                // build the point array
                return [
                    [
                        [_toScreenPoint.x, _toScreenPoint.y],
                        [_arrowHeadLeftPoint.x, _arrowHeadLeftPoint.y],
                        [_arrowHeadLeftAnchorPoint.x, _arrowHeadLeftAnchorPoint.y],
                        [_barLeftPoint.x, _barLeftPoint.y],
                        [_barRightPoint.x, _barRightPoint.y],
                        [_arrowHeadRightAnchorPoint.x, _arrowHeadRightAnchorPoint.y],
                        [_arrowHeadRightPoint.x, _arrowHeadRightPoint.y],
                        [_toScreenPoint.x, _toScreenPoint.y] // don't forget to close the path with the starting point
                    ]
                ];
            }
        });
});