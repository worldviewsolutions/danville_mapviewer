﻿define([
    "dojo/_base/declare"
    , "dojo/_base/lang"
], function(declare, lang){
    return declare('wvs.toolbars.shapes.Triangle', null, {
            
            constructor: function(options){
                lang.mixin(this, options || {});
            },

            setFromPoint: function(point){
                this._fromPoint = point;
            },

            setToPoint: function(point){
                this._toPoint = point;
            },

            create: function(map){       
       
                var p1 = map.toScreen(this._fromPoint),
                    p2 = map.toScreen(this._toPoint),              
                    w = Math.max(p1.x, p2.x) - Math.min(p1.x, p2.x),       
                    x1 = p1.x,
                    x2 = p1.x - w,
                    x3 = p1.x + w,
                    y2 = p1.y + (p2.y - p1.y),
                    points = [];

                // top
                points.push([x1, p1.y]);
                points.push([x2, y2]);
                points.push([x3, y2]);
                points.push([x1, p1.y]);

                var mapPoints = [];
                for(var i = 0; i < points.length; i++){
                    var p = map.toMap({ x: points[i][0], y: points[i][1] })
                    mapPoints.push([p.x, p.y] );
                }

                // build the point array
                return [ mapPoints ];
            }
        });
});