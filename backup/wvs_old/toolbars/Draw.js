﻿define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "esri/toolbars/draw",
    "esri",
    "esri/geometry/Polygon",
    "esri/graphic",
    "dojo/on",
    "../main"
], function(declare, lang, Draw, esri, Polygon, Graphic, on){
    return declare('wvs.toolbars.Draw', [Draw], {
    
            constructor: function(map, options){
                // we will use our own handles instead of ESRI's so we don't have to worry about breaking things.
                this._onMouseDownHandler2 = lang.hitch(this, this._onMouseDownHandler2);
                this._onMouseDragHandler2 = lang.hitch(this, this._onMouseDragHandler2);
                this._onMouseUpHandler2 = lang.hitch(this, this._onMouseUpHandler2);
            },

            activate: function(geometryType, options, shape){

                if(geometryType !== "shape" && geometryType !== "text"){            
                    return this.inherited(arguments);
                }
        
                if(this._geometryType){
                    this.deactivate();
                }        

                this._shape = shape || null;
        
                var map = this.map;
        
                // mix in the options
                this._options = lang.mixin(lang.mixin({}, this._options), options || {});
               
                this._deactivateMapTools(true, false, false, true);
                this._onMouseDownHandler2_connect = on(map, esri.isTouchEnabled ? "onTouchStart":"onMouseDown", this._onMouseDownHandler2);
                this._onMouseDownHandler_connect = on(map, esri.isTouchEnabled ? "onTouchStart" : "onMouseDown", this._onMouseDownHandler);
                this._onMouseDragHandler2_connect = on(map, esri.isTouchEnabled ? "onTouchMove" : "onMouseDrag", this._onMouseDragHandler2);
                this._onMouseUpHandler2_connect = on(map, esri.isTouchEnabled ? "onTouchEnd" : "onMouseUp", this._onMouseUpHandler2);
        
                this._onKeyDown_connect = on(map, "onKeyDown", this._onKeyDownHandler);
                this._redrawConnect = on(map, "onExtentChange", this._redrawGraphic);
                this._geometryType = geometryType;

                this.onActivate(this._geometryType);
            },

            deactivate: function(){
                this._onMouseDownHandler2_connect.remove();
                this._onMouseDragHandler2_connect.remove();
                this._onMouseUpHandler2_connect.remove();

                if(this._geometryType === "shape"){
                    this._activateMapTools(true,false,false,true);
                }
        
                this.inherited(arguments);
            },

            _onMouseDownHandler2:function(evt){
                this.onDrawBegin();
                this.inherited(arguments);
            },

            _onMouseDownHandler2:function(evt){
                this.onDrawBegin();
                this._dragged = false;
        
                var point = evt.mapPoint

                this._shape.setFromPoint(point);
        
                var polygon = new Polygon(map.spatialReference);
                polygon.addRing([point]);
                this._graphic = this.map.graphics.add(new Graphic(polygon, this.fillSymbol), true);
        
                if(esri.isTouchEnabled){
                    evt.preventDefault();
                }
            },

            _onMouseDragHandler2:function(evt){

                this._dragged = true;

                this._shape.setToPoint(evt.mapPoint);
                this._createShape();
       
                if(esri.isTouchEnabled){
                    evt.preventDefault();
                }
            },

            _onMouseUpHandler2:function(evt){
                if(!this._dragged){
                    this._clear();
                    return;
                }

                this._createShape(evt);

                if(esri.isTouchEnabled){
                    evt.preventDefault();
                }

                var geometry = new Polygon();
                geometry.addRing(this._graphic.geometry.rings[0]);

                this._clear();

                this.onDrawEnd(geometry);
            },
    
            _createShape: function(){
                var rings = this._shape.create(this.map);
                this._graphic.setGeometry(lang.mixin(this._graphic.geometry, { rings: rings }));
            },

            onDrawBegin: function(){}
        });
});