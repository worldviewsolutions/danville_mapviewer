﻿define([
    "dojo/_base/declare"
    , "dojo/_base/lang"
    , "dojo/_base/array"
    , "dojo/_base/connect"
], function (
    declare
    , lang
    , array
    , connect
) {
    var _MapEventMixIn = declare([], {

        // enabled: Boolean
        //      flag to determine whether the given event is enabled. it may be "active" but not enabled.
        //      For example, we may want temporarily disable an event (i.e. "one-click identify") when we are drawing markup and re-enable it when we are done
        enabled: false,

        // disableOnMapAcquire: Boolean
        //      disable the event when another object 'acquires' the map (topic: map-acquire with acquired flag) and enable it when it is released (topic: map-acquire w/o acquired flag). objects like markup, identify and so on need some events to be disabled.
        disableOnMapAcquire: false,

        // _event: Object
        //      a reference to the dojo/on event.
        _event: null,

        // _map: Map
        //      a reference to the map
        _map: null,

        constructor: function (map, options) {
            var self = this;

            if (!map) {
                throw new Error("no map defined for " + this.declaredClass);
            }

            this._map = map;
            this._subscriptions =  [];
            lang.mixin(this, options || {});

            if (this.disableOnMapAcquire) {
                this._subscriptions.push(
                    connect.subscribe("map-acquire",
                        function (topic) {
                        if (topic.target != self && topic.acquired)
                            self.disable();
                        else if(topic.target != self && !topic.acquired)
                            self.enable();
                        }
                    )
                );
            }
        },


        initialize: function () { throw new Error("this function must be implemented by sub-classes"); },
        
        enable: function () { this.enabled = true;},

        disable: function () { this.enabled = false; },

        remove: function () {
            if (this._event)
                this._event.remove();
            array.forEach(this._subscriptions, function (sub) {
                sub.remove();
            });
        }


    });

    return _MapEventMixIn;
});