﻿define([
    "dojo/_base/declare"
    , "dojo/_base/lang"
    , "./_MapEventMixIn"
    , "esri/graphic"
    , "dijit/Menu"
    , "dijit/MenuItem"
    , "esri/symbols/jsonUtils"
    , "../../common/InfoTemplateManager"
    , "../../_base/config"
    , "dojo/query"
    , "dojo/_base/array"
    , "dojo/on"
    , "wvs/extensions/esri/layers/MarkupLayer"
], function (
    declare
    , lang
    , _MapEventMixIn
    , Graphic
    , Menu
    , MenuItem
    , symbolJsonUtils
    , InfoTemplateManager
    , wvsConfig
    , query
    , array
    , on
    , MarkupLayer
) {
    var ContextMenu = declare([_MapEventMixIn], {
        // _lastMapPoint: Point
        //      the last map point from a right click
        _lastMapPoint: null,

        _menu: null,

        menuItems: [],

        initialize: function () {
            var self = this;

            // Pushpin marker on right-click
            var event = on(this._map, "mouse-down", function (evt) {
                self._lastMapPoint = evt.mapPoint;
            });

            var menu = new Menu({
                targetNodeIds: [this._map.id],
                onOpen: function () {
                    this._lastMapPoint = self._lastMapPoint;
                    this._map = self._map;
                }
            });

            array.forEach(this.menuItems, function (menuItem) {
                if (typeof menuItem === "function")
                    menu.addChild(menuItem());
                else
                    menu.addChild(menuItem);
            }, this);

            this._menu = menu;
            this._event = event;
            this.enable();
            return event;
        },
        addItem: function (menuItem) {
            // TODO: Implement
        },
        removeItem: function (menuItem) {
            // TODO: Implement
        }
    });

    ContextMenu.MenuItems = {};

    ContextMenu.MenuItems.PushPin = function () {
        var self = this,
            markupLayer = wvsConfig.defaults.markupLayer;

        return new MenuItem({
            label: "Add Pushpin Marker Here",
            onClick: function (evt) {
                var mapPoint = this.getParent()._lastMapPoint,
                    map = this.getParent()._map,
                    marker = new Graphic(mapPoint, symbolJsonUtils.fromJson(wvsConfig.defaults.symbols.point), {}, InfoTemplateManager.getTabularAttributeTemplate({ title: "XY Location", createWidgetsFromGeometry: true }));
                marker.setEditing(true);
                var infoWindow = map.infoWindow;
                if (!array.some(map.layerIds, function (graphicLayerId) { return (graphicLayerId === markupLayer.id); }, this)) {
                    var newLayer = false;
                    for (var i = 0; i < markupLayer.items.data.length; i++) {
                        if (markupLayer.items.data[i].graphic) {
                            newLayer = true;
                            break;
                        }
                    }
                    if ((markupLayer.title != 'Temporary Markup' && markupLayer._id != null) || newLayer) {
                        markupLayer = new MarkupLayer({ json: [{ id: 'root', parent: null, name: 'Temporary Markup', type: 'folder' }] });
                    }
                    map.addLayer(markupLayer);
                }
                markupLayer.add(marker);

                infoWindow.hide();
                infoWindow.setContent(marker.getContent());
                infoWindow.setTitle(marker.getTitle());
                infoWindow.show(marker.geometry);
                // TODO: Find a more elegant way to do this as others may want to select a sub-widget from the bubble
                //var nl = query("a", infoWindow.domNode);
                //array.forEach(nl, function (node) {
                //   if (node.innerHTML === "Coordinates") {
                //      on.emit(node, "click", { bubbles: true, cancelable: true });
                //   }
                //});
            }
        });
    };

    return ContextMenu;
});