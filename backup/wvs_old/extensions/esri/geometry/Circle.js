﻿define([
        "dojo/_base/lang",
        "esri/geometry/Circle",
        "esri/geometry/Polyline",
        "esri/geometry/Point",
        "esri/geometry/geodesicUtils",
        "esri/geometry/webMercatorUtils"
],
    function (lang, Circle, Polyline, Point, geodesicUtils, webMercatorUtils) {
        lang.extend(Circle, {

            getArea: function (areaUnit, places) {
                var newGeometry = webMercatorUtils.webMercatorToGeographic(this),
                    places = places || 2,
                    area = geodesicUtils.geodesicAreas([this], areaUnit);

                area = area[0] = area[0] < 0 ? area[0] * -1 : area[0];
                return area.toFixed(places);
            },
            getPerimeter: function (lengthUnit, places) {
                var newGeometry = webMercatorUtils.webMercatorToGeographic(this),
                    places = places || 2,
                    perimeter = geodesicUtils.geodesicLengths([new Polyline(this.rings[0])], lengthUnit)[0];

                // Perimeter
                return perimeter.toFixed(places);
            }
        });
    });