﻿console.log("wvs/dijits/InfoWindow.js");

define([
    "dojo/_base/lang",
    "dojo/_base/declare",
    "dojo/on",
    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/dom-prop",
    "dojo/dom-class",
    "dijit/registry",
    "dojo/Evented",
    "esri/InfoWindowBase",
    "esri/domUtils",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "../_base/debug"
],
    function(
        lang,
        declare,
        on,
        dom,
        domConstruct,
        domStyle,
        domAttr,
        domProp,
        domClass,
        registry,
        Evented,
        InfoWindowBase,
        domUtils,
        _TemplatedMixin,
        _WidgetsInTemplateMixin,
        console
) {
        return declare("wvs.dijits.InfoWindow", [InfoWindowBase, _TemplatedMixin, _WidgetsInTemplateMixin, Evented], {

            domNode: null,

            actionListDivider: '&nbsp;|&nbsp;',

            isShowing: false,

            isMaximized: false,

            _contentString: null,

            _windowHeight: null,

            _windowWidth: null,

            _wrapperHeight: null,

            _wrapperWidth: null,

            _contentHeight: null,

            _contentWidth: null,

            _domTop: null,

            _domLeft: null,

            _wrapperNode: null,

            _map: null,

            _popup: null,

            _pointer: null,

            _closeButton: null,

            _maxButton: null,

            _minButton: null,

            _prevButton: null,

            _nextButton: null,

            _title: null,

            _content: null,

            _actions: null,

            _location: null,

            _actionPanes: null,

            constructor: function (params, srcNodeRef) {
                lang.mixin(this, params);

                if (params && params.map) {
                    if (srcNodeRef) {
                        this.domNode = srcNodeRef;
                    }
                    else {
                        this.domNode = domConstruct.create("div", null, dom.byId(params.map));
                    }
                    this._map = params.map;
                }
                else {
                    if (srcNodeRef) {
                        this.domNode = srcNodeRef;
                    }
                    else {
                        this.domNode = domConstruct.create("div");
                    }
                }
                this._actionPanes = [];
                domClass.add(this.domNode, "wvsInfoWindow");
                this._wrapperNode = domConstruct.create("div", null, this.domNode);
                domClass.add(this._wrapperNode, "infoWindowWrapper");
                this._closeButton = domConstruct.create("div", null, this._wrapperNode);
                this._maxButton = domConstruct.create("div", null, this._wrapperNode);
                //this._nextButton = domConstruct.create("div", null, this._wrapperNode);
                //this._prevButton = domConstruct.create("div", null, this._wrapperNode);
                //this._minButton = domConstruct.create("div", null, this._wrapperNode);
                this._title = domConstruct.create("div", { "class": "titlePane" }, this._wrapperNode);
                this._content = domConstruct.create("div", { "class": "contentPane" }, this._wrapperNode);
                this._actions = domConstruct.create("div", { "class": "actionsPane" }, this._wrapperNode);

                domClass.add(this._maxButton, 'titleButton');
                /*domClass.add(this._nextButton, 'titleButton');
                domClass.add(this._prevButton, 'titleButton');
                domClass.add(this._minButton, 'titleButton');*/
                domClass.add(this._maxButton, 'maximize');
                /*domClass.add(this._nextButton, 'next');
                domClass.add(this._prevButton, 'prev');
                domClass.add(this._minButton, 'minimize');*/
                domClass.add(this._closeButton, 'titleButton');
                domClass.add(this._closeButton, 'close');

                on(this._closeButton, "click", lang.hitch(this, function () {
                    this.hide();
                    if (this.isShowing) {
                        this.isShowing = false;
                    }
                }));

                var action = domConstruct.create("a", { "class": "selectedAction", innerHTML: "Summary", href: "javascript:void(0);" }, this._actions);
                //domProp.set(action, 'innerHTML', 'Summary');
                //domProp.set(action, 'href', "javascript:void(0);");
                //domClass.add(action, 'selectedAction');
                //domConstruct.place(action, this._actions, "last");
                on(action, 'click', lang.hitch(this, function () {
                    for (var i = 0; i < this._actions.childNodes.length; i++) {
                        domClass.remove(this._actions.childNodes[i], 'selectedAction');
                    }
                    domClass.add(action, 'selectedAction');
                    this.place(this._contentString, this._content);
                }));

                on(this._maxButton, 'click', lang.hitch(this, this._maxOrRestoreWindow));

                domUtils.hide(this.domNode);
                this.isShowing = false;
            },

            destroyDijits: function () {
                this.inherited(arguments);
            },

            hide: function () {
                domUtils.hide(this.domNode);
                this.isShowing = false;
                this.emit('hide', {});
            },

            place: function (value, parentNode) {
                this.inherited(arguments);
            },

            _removePageName: function (value, parentNode) {
                for(var i = 0; i < parentNode.childNodes.length; i ++) {
                    if (domProp.get(parentNode.childNodes[i], 'innerHTML') == value) {
                        domConstruct.destroy(parentNode.childNodes[i]);
                        if (parentNode.childNodes[i - 1] && parentNode.childNodes[i - 1].innerHTML == this.actionListDivider) {
                            domConstruct.destroy(parentNode.childNodes[i - 1]);
                        }
                    }
                }

                if (parentNode.childNodes[0].innerHTML == this.actionListDivider) {
                    domConstruct.destroy(parentNode.childNodes[0]);
                }
                parentNode.childNodes[0].click();
            },

            resize: function (width, height) {
                domStyle.set(this.domNode, {
                    "width": width + "px",
                    "height": height + "px"
                });
                domStyle.set(this._wrapperNode, {
                    "width": width + "px",
                    "height": height + "px"
                });
                console.log(domStyle.get(this._wrapperNode, 'height'));
                domStyle.set(this._content, {
                    "height": domStyle.get(this._wrapperNode, 'height') + "px",
                    "width": domStyle.get(this._wrapperNode, 'width') + "px"
                });
                console.log(domStyle.get(this._content, 'height'));
                domStyle.set(
                    this._content,
                    'width',
                    domStyle.get(this._wrapperNode, 'width')
                );

                //domStyle.set(this._content, 'height', height - (domStyle.get(this._title, 'height') + domStyle.get(this._actions, 'height')) + "px");

            },

            _maximize: function () {
                /*this._windowHeight = domStyle.get(this.domNode, 'height');
                this._windowWidth = domStyle.get(this.domNode, 'width');

                this._wrapperHeight = domStyle.get(this._wrapperNode, 'height');
                this._wrapperWidth = domStyle.get(this._wrapperNode, 'width');

                this._contentHeight = domStyle.get(this._content, 'height');
                this._contentWidth = domStyle.get(this._content, 'width');

                this._domTop = domStyle.getComputedStyle(this.domNode, 'top');
                this._domLeft = domStyle.getComputedStyle(this.domNode, 'left');

                
                domStyle.set(this.domNode, {
                    "width": (domStyle.get(dom.byId(this._map), 'width') - 50) + "px",
                    "height": (domStyle.get(dom.byId(this._map), 'height') - 50) + "px"
                });
                domStyle.set(this._wrapperNode, {
                    "width": (domStyle.get(dom.byId(this._map), 'width') - 50) + "px",
                    "height": (domStyle.get(dom.byId(this._map), 'height') - 50) + "px"
                });
                /*domStyle.set(this._content, {
                    "height": domStyle.get(this.domNode, 'height') + "px",
                });

                domStyle.set(this.domNode, {
                    "top": "25px",
                    "left": "25px"
                });*/

            },

            _restore: function () {
                /*domStyle.set(this.domNode, {
                    "width": this._windowWidth + "px",
                    "height": this._windowHeight + "px"
                });
                domStyle.set(this._wrapperNode, {
                    "width": this._wrapperWidth + "px",
                    "height": this._wrapperHeight + "px"
                });
                domStyle.set(this._content, {
                    "width": this._contentWidth + "px",
                    "height": this._contentHeight + "px"
                });
                this.show(this._location);*/
            },

            _maxOrRestoreWindow: function () {
                if (!this.isMaximized) {
                    domClass.remove(this._maxButton, 'maximize');
                    domClass.add(this._maxButton, 'restore');
                    this._maximize();
                    this.isMaximized = !this.isMaximized;
                }

                else {
                    this.resize(this._windowWidth, this._windowHeight);
                    domClass.remove(this._maxButton, 'restore');
                    domClass.add(this._maxButton, 'maximize');
                    this._restore();
                    this.isMaximized = !this.isMaximized;
                }
            },

            addPage: function (newPage) {
                for (var i = 0; i < this._actions.childNodes.length; i++) {
                    if (domProp.get(this._actions.childNodes[i], 'innerHTML') == newPage.name) {
                        console.warn('InfoWindow page cannot be created twice');
                        return;
                    }
                }
                if (this._actions.childNodes.length > 0) {
                    var divider = domConstruct.create("TextNode");
                    domProp.set(divider, 'innerHTML', this.actionListDivider);
                    domConstruct.place(divider, this._actions, "last");
                }
                var action = domConstruct.create("a");
                domProp.set(action, 'innerHTML', newPage.name);
                domProp.set(action, 'href', "javascript:void(0);");
                domConstruct.place(action, this._actions, "last");
                on(action, 'click', lang.hitch(this, function () {
                    for (var i = 0; i < this._actions.childNodes.length; i++) {
                        domClass.remove(this._actions.childNodes[i], 'selectedAction');
                    }
                    domClass.add(action, 'selectedAction');
                    this.place(newPage.content, this._content);
                }));
            },

            removePage: function (pageName) {
                if (pageName == 'Summary') {
                    console.warn("Cannot remove the 'Summary' panel from the Info Window");
                }
                else {
                    this._removePageName(pageName, this._actions);
                }
            },

            setContent: function (content) {
                //this.place(content, this._content);
                this._contentString = content;
                this._actions.childNodes[0].click();
            },

            setMap: function (map) {
                this.inherited(arguments);
                if (map && map.id) {
                    this._map = map;
                    domConstruct.place(this.domNode, dom.byId(this._map.id));
                }
            },

            setTitle: function (title) {
                this.place(title, this._title);
            },

            show: function (location) {
                var loc;
                if (location.spatialReference) {
                    this._location = location;
                    loc = this._map.toScreen(location);
                }

                domStyle.set(this.domNode, {
                    "left": (loc.x) + "px",
                    "bottom": (loc.y) + "px"
                });
                domUtils.show(this.domNode);
                this.isShowing = true;
                this.emit('show', {});
            },

            startupDijits: function () {
                this.inherited(arguments);
            },

            unsetMap: function (map) {
                this.inherited(arguments);
            }

        });
    })