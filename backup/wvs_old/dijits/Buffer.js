﻿define([
    "dojo/_base/declare"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dojo/text!./templates/Buffer.html"
    , "dojo/_base/lang"
    , "dojo/on"
    , "dojo/_base/array"
    , "dijit/form/TextBox"
    , "dijit/form/Select"
    , "esri/units"
    , "dojo/Evented"
    , "../common/Common"
    , "esri/symbols/jsonUtils"
    , "esri/geometry/Polygon"
    , "esri/graphic"
    , "dojo/dom-class"
    , "dojo/dom"
    , "esri/symbols/SimpleFillSymbol"
    , "esri/symbols/SimpleMarkerSymbol"
    , "esri/symbols/SimpleLineSymbol"
], function (declare, _WidgetBase, _TemplatedMixin, template, lang, on, array, TextBox, Select, Units, Evented, Common, jsonUtils, Polygon, Graphic, domClass, dom, SimpleFillSymbol, SimpleMarkerSymbol, SimpleLineSymbol) {
    return declare([_WidgetBase, _TemplatedMixin, Evented], {

        templateString: template,

        declaredClass: "wvs.dijits.Buffer",

        // numberOfPoints: Integer
        //      number of points that will define the circle geometry
        numberOfPoints: 36,

        // radiusLowerBound: Integer
        //      this is an exclusive lower bounds, so anything greater than the number supplied
        radiusLowerBound: 0,

        // radius: Number
        //      the radius of the circle
        radius: 0,

        // bufferSymbol: Symbol
        //      the symbol used for the buffer graphic
        bufferSymbol: jsonUtils.fromJson({ "color": [0, 0, 255, 100], "outline": { "color": [0, 0, 255, 255], "width": 2, "type": "esriSLS", "style": "esriSLSSolid" }, "type": "esriSFS", "style": "esriSFSSolid" }),

        postCreate: function () {
            var self = this;

            this.inherited(arguments);

            this.radiusTextBox = new TextBox({
                value: this.radius,
                onKeyUp: function (evt) { self.radius = this.get("value"); }
            }, this.radiusTextBox);

            //Apply uniform styling classes
            domClass.add(this.radiusTextBox.domNode, "span1");

            this.unitSelect = new Select({
                options: [{ label: "Feet", value: Units.FEET },
                    { label: "Kilometers", value: Units.KILOMETERS },
                    { label: "Meters", value: Units.METERS },
                    { label: "Miles", value: Units.MILES }
                ]
            }, this.unitSelect);
        },
        createBufferFromPoint: function (point) {
            // summary:
            //      creates a buffer (circle) from a point using a polygon geometry
            var bufferUnits = this.unitSelect.get("value"),
                radius = parseFloat(this.radius);
            if (isNaN(radius) || radius <= this.radiusLowerBound) {
                throw new Error("Invalid radius: the radius must be greater than " + this.radiusLowerBound);
                return;
            }

            //var buffer = new Circle(point, { numberOfPoints: this.numberOfPoints, radius: radius, radiusUnit: bufferUnits });
            var polygon = new Polygon(point.spatialReference);
            polygon.addRing(Common.buildBufferPointArray(this.map, point, radius, bufferUnits, this.numberOfPoints));
            var graphic = new Graphic(polygon, this.bufferSymbol);

            return { geometry: polygon, graphic: graphic };
        }
    });
});