﻿console.log("wvs/dijits/SymbolCreator.js");

define([
    "dojo/_base/declare"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dojo/text!./templates/SymbolCreator.html"
    , "dojo/_base/lang"
    , "dojo/_base/array"
    , "dojo/query"
    , "dojo/dom-style"
    , "dojo/dom-construct"
    , "dijit/Dialog"
    , "esri/symbols/SimpleFillSymbol"
    , "esri/symbols/SimpleLineSymbol"
    , "esri/symbols/jsonUtils"
    , "esri/graphic"
    , "dijit/ColorPalette"
    , "dojo/_base/Color"
    , "dijit/form/HorizontalSlider"
    , "dijit/form/DropDownButton"
    , "dijit/form/TextBox"
    , "dijit/DropDownMenu"
    , "dijit/MenuItem"
    , "dojox/gfx"
    , "dojo/on"
    , "esri/symbols/PictureMarkerSymbol"
    , "./images/pushpins/markers"
    , "dojo/has"
    , "esri/kernel"
    , "dojox/gfx/_base"
    , "../extensions/esri/graphic"
], function (declare, _WidgetBase, _TemplatedMixin, template, lang, array, query, domStyle, domConstruct, Dialog, SimpleFillSymbol, SimpleLineSymbol, jsonUtils, Graphic, ColorPalette, Color, HorizontalSlider, DropDownButton, TextBox, DropDownMenu, MenuItem, gfx, on, PictureMarkerSymbol, markers, has, esriKernel, gfxBase) {
    // module:
    //      wvs/dijits/SymbolCreator
    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,

        declaredClass: "wvs.dijits.SymbolCreator",

        showCustomMapIcons: true,

        constructor: function (params, srcNodeRef) {
            // graphic: Graphic
            this.graphic = null;
            lang.mixin(this, params || {});
            // this.graphic = new Graphic(params.graphic.toJson());

            if (!this.graphic && !this.symbolClass) {
                throw new Error("either a graphic or a symbol type must be defined in " + this.declaredClass);
            }

            if (this.graphic) {
                this.symbol = jsonUtils.fromJson(this.graphic.symbol.toJson());
                this.symbolClass = this.symbol.declaredClass;
                if (!this.symbol._fill) {
                    this.symbol._fill = new Color();
                }
                if (!this.symbol._stroke) {
                    this.symbol._stroke = gfxBase.defaultStroke;
                }
            }


            this.lineTitle = this.symbolClass === "esri.symbol.SimpleFillSymbol" ? "Border" : "Line";
            var internalVariables = {
                fillTransparency: 100,
                lineTransparency: 100,
                lineWidth: 1,
                fontText: "",
                fontSize: 12,
                loaded: false
            };
            lang.mixin(this, internalVariables);
        },
        postCreate: function () {
            console.log(this.declaredClass + " postCreate");
            this.inherited(arguments);
            this._createInternalWidgets();

            array.forEach(markers, function (marker, i) {
                var img = domConstruct.create("img", { src: marker.url, style: "cursor:pointer; height: 32; width: 32;" }, this.teardropIcons);
                on(img, "click", lang.hitch(this, function () {
                    this.setPictureMarkerSymbolFromJson(marker);
                }));
            }, this);

            this._setMode();

            if (this.graphic) {
                this._setOptionsFromGraphic();
            }

            if (!this.showCustomMapIcons) {
                domStyle.set(this.customMapIconsContainer, { display: "none" });
            }
        },
        startup: function () {
            setTimeout(lang.hitch(this, function () {
                this.loaded = true;
            }, 100));
        },

        updateGraphic: function () {
            if (this.graphic) {
                if (this.loaded) {
                    this.graphic.setModified();
                }
                this.graphic.symbol = jsonUtils.fromJson(this.symbol.toJson());
                this.graphic.draw();
            }
        },

        destroy: function () {
            this.graphic = null;
            this.symbol = null;
            this.inherited(arguments);
        },

        getSymbol: function () {
            return this.symbol;
        },

        setGraphic: function (graphic) {
            this.graphic = graphic;
            this.symbol = graphic.symbol;
            if (!this.symbol._fill)
                this.symbol._fill = new Color();
            if (!this.symbol._stroke)
                this.symbol._stroke = gfxBase.defaultStroke;
            this.symbolClass = this.graphic.symbol.declaredClass;
            this.lineTitle = this.symbolClass === "esri.symbol.SimpleFillSymbol" ? "Border" : "Line";
            this._setMode();
        },

        _createInternalWidgets: function () {
            var self = this;

            // Create internal widgets
            this.fillColorPicker = new ColorPalette({
                palette: "7x10",
                onChange: lang.hitch(this, this._fillColorChange)
            }, this.fillColorPicker);

            this.lineColorPicker = new ColorPalette({
                palette: "7x10",
                onChange: lang.hitch(this, this._lineColorChange)
            }, this.lineColorPicker);

            this.fontColorPicker = new ColorPalette({
                palette: "7x10",
                onChange: lang.hitch(this, this._fontColorChange)
            }, this.fontColorPicker);

            this.fillTransparencySlider = new HorizontalSlider({
                value: this.fillTransparency,
                minimum: 0,
                maximum: 100,
                discreteValues: 101,
                showButtons: true,
                style: "width:80%; display:inline-block;",
                onChange: lang.hitch(this, this._fillTransparencyChange)
            }, this.fillTransparencySlider);

            this.lineTransparencySlider = new HorizontalSlider({
                value: this.lineTransparency,
                minimum: 0,
                maximum: 100,
                discreteValues: 101,
                showButtons: true,
                style: "width:80%; display:inline-block;",
                onChange: lang.hitch(this, this._lineTransparencyChange)
            }, this.lineTransparencySlider);

            this.lineWidthSlider = new HorizontalSlider({
                value: this.lineWidth,
                minimum: 1,
                maximum: 50,
                discreteValues: 50,
                showButtons: true,
                style: "width:80%; display:inline-block;",
                onChange: lang.hitch(this, this._lineWidthChange)
            }, this.lineWidthSlider);

            this.fontSizeSlider = new HorizontalSlider({
                value: this.fontSize,
                minimum: 10,
                maximum: 100,
                discreteValues: 91,
                showButtons: true,
                intermediateChanges: true,
                style: "width:80%; display:inline-block;",
                onChange: lang.hitch(this, this._fontSizeChange)
            }, this.fontSizeSlider);

            this.fontTextBox = new TextBox({
                value: this.fontText,
                onKeyUp: lang.hitch(this, this._fontTextChange)
            }, this.fontTextBox);

            // TODO: Create fill style drop down button
            var supportedFillStyles = ['STYLE_BACKWARD_DIAGONAL', 'STYLE_CROSS', 'STYLE_DIAGONAL_CROSS', 'STYLE_FORWARD_DIAGONAL', 'STYLE_HORIZONTAL', 'STYLE_SOLID', 'STYLE_VERTICAL'];

            // Create line style drop down button
            var supportedLineStyles = [
                { lineType: 'STYLE_SOLID', label: "Solid" },
                { lineType: 'STYLE_DASH', label: "Dash" },
                { lineType: 'STYLE_DASHDOT', label: "Dash-Dot" },
                { lineType: 'STYLE_DASHDOTDOT', label: "Dash-Dot-Dot" },
                { lineType: 'STYLE_DOT', label: "Dot" }
                //ESRI does not support these simple line styles in their print utility. Disabling to prevent issues.
                //{ lineType: 'STYLE_LONGDASH', label: "Long Dash" },
                //{ lineType: 'STYLE_LONGDASHDOT', label: "Long Dash-Dot" },
                //{ lineType: 'STYLE_SHORTDASH', label: "Short Dash" },
                //{ lineType: 'STYLE_SHORTDASHDOT', label: "Short Dash-Dot" },
                //{ lineType: 'STYLE_SHORTDASHDOTDOT', label: "Short Dash-Dot-Dot" },
                //{ lineType: 'STYLE_SHORTDOT', label: "Short Dot" }
            ];


            var lineTypeMenu = new DropDownMenu({ style: "display:none;" });
            array.forEach(supportedLineStyles, function (supportedLineStyle) {
                if (!has("ie") || has("ie") > 8) {
                    var lineTypeDiv = domConstruct.create("div");
                    var surface = gfx.createSurface(lineTypeDiv, 25, 10);
                    surface.createLine({ x1: 5, y1: 5, x2: 30, y2: 5 }).setStroke({ color: "black", style: SimpleLineSymbol[supportedLineStyle.lineType], width: 1 });
                }
                var menuItem = new MenuItem({
                    label: !has("ie") || has("ie") > 8 ? lineTypeDiv.innerHTML : supportedLineStyle.label,
                    title: supportedLineStyle.label,
                    _lineStyle: supportedLineStyle.lineType,
                    onClick: lang.hitch(self, self._lineTypeButtonChange, supportedLineStyle.lineType)
                });
                lineTypeMenu.addChild(menuItem);
            });

            this.lineTypeDropDownButton = new DropDownButton({
                label: "Border Style",
                dropDown: lineTypeMenu
            }, this.lineTypeDropDownButton);

        },

        _setMode: function () {
            switch (this.symbolClass) {
                case "esri.symbol.SimpleFillSymbol":
                    domConstruct.place(this.commonOptions, this.fillSymbolOptions);
                    domStyle.set(this.colorPickers, { display: "block" });
                    domStyle.set(this.lineColorPicker.domNode, { display: "none" });
                    domStyle.set(this.markerOptions, { display: "none" });
                    domStyle.set(this.lineOptions, { display: "none" });
                    domStyle.set(this.fillColorPicker.domNode, { display: "block" });
                    domStyle.set(this.commonOptions, { display: "block" });
                    domStyle.set(this.fillSymbolOptions, { display: "block" });
                    break;
                case "esri.symbol.SimpleLineSymbol":
                    domConstruct.place(this.commonOptions, this.lineOptions);
                    domStyle.set(this.lineColorPicker.domNode, { display: "block" });
                    domStyle.set(this.fillColorPicker.domNode, { display: "none" });
                    domStyle.set(this.colorPickers, { display: "block" });
                    domStyle.set(this.fillSymbolOptions, { display: "none" });
                    domStyle.set(this.markerOptions, { display: "none" });
                    domStyle.set(this.commonOptions, { display: "block" });
                    domStyle.set(this.lineOptions, { display: "block" });
                    break;
                case "esri.symbol.PictureMarkerSymbol":
                    domStyle.set(this.fillSymbolOptions, { display: "none" });
                    domStyle.set(this.commonOptions, { display: "none" });
                    domStyle.set(this.lineOptions, { display: "none" });
                    domStyle.set(this.markerOptions, { display: "block" });
                    break;
                case "esri.symbol.TextSymbol":
                    domStyle.set(this.fillSymbolOptions, { display: "none" });
                    domStyle.set(this.commonOptions, { display: "none" });
                    domStyle.set(this.lineOptions, { display: "none" });
                    domStyle.set(this.fontOptions, { display: "block" });
                    break;
            }

            var nl = query(".jstoolkit-symbol-creator-line-title", this.commonOptions);
            array.forEach(nl, function (node) {
                node.innerHTML = this.lineTitle;
            }, this);
        },

        _setOptionsFromGraphic: function () {
            if (this.symbolClass === "esri.symbol.SimpleFillSymbol") {
                this.fillColorPicker.set("value", this.symbol.getFill().toHex());
                this.fillTransparencySlider.set("value", Math.floor(this.symbol.getFill().a * 100));
                this.fillTransparencyValue.innerHTML = Math.floor(this.symbol.getFill().a * 100);
                this.lineColorPicker.set("value", this.symbol.getStroke().color.toHex());
                this._lineTypeButtonChange("STYLE_" + this.symbol.getStroke().style.toUpperCase());
                this.lineTransparencySlider.set("value", Math.floor(this.symbol.getStroke().color.a * 100));
                this.lineTransparencyValue.innerHTML = Math.floor(this.symbol.getStroke().color.a * 100);
                this.lineWidthSlider.set("value", Math.floor(this.symbol.getStroke().width));
                this.lineWidthValue.innerHTML = Math.floor(this.symbol.getStroke().width);
            }
            else if (this.symbolClass === "esri.symbol.SimpleLineSymbol") {
                this.lineColorPicker.set("value", this.symbol.color.toHex());
                this._lineTypeButtonChange("STYLE_" + this.symbol.getStroke().style.toUpperCase());
                this.lineTransparencyValue.innerHTML = Math.floor(this.symbol.getStroke().color.a * 100);
                this.lineWidthSlider.set("value", Math.floor(this.symbol.getStroke().width));
                this.lineTransparencySlider.set("value", Math.floor(this.symbol.getStroke().color.a * 100));
                this.lineWidthValue.innerHTML = Math.floor(this.symbol.getStroke().width);
            }
            else if (this.symbolClass === "esri.symbol.TextSymbol") {
                this.fontSizeSlider.set("value", parseInt(this.symbol.font.size));
                domStyle.set(this.fontColorButton, { background: this.symbol.color.toHex() });
                this.fontTextBox.set("value", this.symbol.text);
            }
        },
        // Event handlers
        _fillColorChange: function (val) {
            var newColor = new Color(val);

            domStyle.set(this.fillColorButton, { background: newColor.toHex() });
            domStyle.set(this.colorPickers, { display: "none" });


            this.symbol.color.r = newColor.r;
            this.symbol.color.g = newColor.g;
            this.symbol.color.b = newColor.b;
            // TODO: Editing the symbol was pretty tricky in 3.8 and under. It changed in 3.9 and the below private properties are gone but they were needed in 3.8 and below
            if (esriKernel.version <= 3.8) {
                this.symbol._fill.r = newColor.r;
                this.symbol._fill.g = newColor.g;
                this.symbol._fill.b = newColor.b;
            }
            this.updateGraphic();
        },

        _lineColorChange: function (val) {
            var newColor = new Color(val);
            domStyle.set(this.lineColorButton, { background: newColor.toHex() });
            domStyle.set(this.colorPickers, { display: "none" });
            if (this.symbolClass === "esri.symbol.SimpleFillSymbol") {
                this.symbol.outline.color.r = newColor.r;
                this.symbol.outline.color.g = newColor.g;
                this.symbol.outline.color.b = newColor.b;
            }
            // This was needed in 3.8 and below. This changed in 3.9
            var strokeColor = esriKernel.version <= 3.8 ? this.symbol._stroke && this.symbol._stroke.color : this.symbol.getStroke().color;
            strokeColor.r = newColor.r;
            strokeColor.g = newColor.g;
            strokeColor.b = newColor.b;

            this.updateGraphic();
        },

        _fontColorChange: function (val) {
            var newColor = new Color(val);

            domStyle.set(this.fontColorButton, { background: newColor.toHex() });
            domStyle.set(this.colorPickers, { display: "none" });

            this.symbol.setColor(newColor);
            this.updateGraphic();
        },

        _fillTransparencyChange: function (value) {
            var alpha = value / 100;
            this.fillTransparencyValue.innerHTML = value;

            // Needed for 3.8 and below. Breaks 3.9
            if (esriKernel.version <= 3.8) {
                this.symbol._fill.a = alpha;
            }
            this.symbol.color.a = alpha;
            this.updateGraphic();
        },

        _lineTypeChange: function (lineStyle) {
            var style = SimpleLineSymbol[lineStyle];
            if (this.symbolClass === "esri.symbol.SimpleFillSymbol") {
                // Again, 3.9 changed the way things worked (actually works correctly)
                if (esriKernel.version <= 3.8) {
                    this.symbol._stroke.style = style;
                }
                else {
                    this.symbol.getStroke().style = style;
                }
                this.symbol.getStroke().style = style;
                this.symbol.outline.style = style;
            }
            else {
                this.symbol.setStyle(style);
            }

            this.updateGraphic();
        },

        _fontSizeChange: function (value) {
            this.symbol.font.setSize(value);
            this.fontSizeValue.innerHTML = value + "pt";
            this.updateGraphic();
        },

        _lineWidthChange: function (value) {
            if (this.symbolClass === "esri.symbol.SimpleFillSymbol") {
                //var stroke = esriKernel.version <= 3.8 ? this.symbol._stroke: this.symbol.getStroke();
                this.symbol.getStroke().width = value;
                this.symbol.outline.width = value;
            }
            else {
                this.symbol.setWidth(value);
            }
            this.lineWidthValue.innerHTML = value;
            this.updateGraphic();
        },

        _lineTransparencyChange: function (value) {
            var alpha = value / 100;
            this.lineTransparencyValue.innerHTML = value;
            this.symbol.getStroke().color.a = alpha;
            this.updateGraphic();
        },

        _lineTypeButtonChange: function (lineStyle, evt) {
            var menuItems = this.lineTypeDropDownButton.dropDown.getChildren();
            array.forEach(menuItems, function (menuItem) {
                if (menuItem._lineStyle === lineStyle) {
                    this.lineTypeDropDownButton.set("label", menuItem.label);
                    this._lineTypeChange(lineStyle);
                }
            }, this);
        },

        _showFillColorPicker: function () {
            if (domStyle.get(this.colorPickers, "display") === "block") {
                domStyle.set(this.colorPickers, { display: "none" });
            }
            else {
                domStyle.set(this.colorPickers, { display: "block" });
                domStyle.set(this.lineColorPicker.domNode, { display: "none" });
                domStyle.set(this.fontColorPicker.domNode, { display: "none" });
                domStyle.set(this.fillColorPicker.domNode, { display: "block" });
            }
        },

        _showLineColorPicker: function () {
            if (domStyle.get(this.colorPickers, "display") === "block") {
                domStyle.set(this.colorPickers, { display: "none" });
            }
            else {
                domStyle.set(this.colorPickers, { display: "block" });
                domStyle.set(this.lineColorPicker.domNode, { display: "block" });
                domStyle.set(this.fillColorPicker.domNode, { display: "none" });
                domStyle.set(this.fontColorPicker.domNode, { display: "none" });
            }
        },

        _showFontColorPicker: function () {
            if (domStyle.get(this.colorPickers, "display") === "block") {
                domStyle.set(this.colorPickers, { display: "none" });
            }
            else {
                domStyle.set(this.colorPickers, { display: "block" });
                domStyle.set(this.fontColorPicker.domNode, { display: "block" });
                domStyle.set(this.lineColorPicker.domNode, { display: "none" });
                domStyle.set(this.fillColorPicker.domNode, { display: "none" });
            }
        },

        _fontTextChange: function () {
            this.symbol.setText(this.fontTextBox.get("value"));
            this.updateGraphic();
        },

        setPictureMarkerSymbolFromJson: function (json) {
            var markerSymbol = new PictureMarkerSymbol(json.url, json.width, json.height);
            markerSymbol.setOffset(json.xoffset, json.yoffset); markerSymbol.setAngle(json.angle);
            this.setPictureMarkerSymbol(markerSymbol);
        },

        setPictureMarkerSymbol: function (markerSymbol) {
            this.graphic.setSymbol(markerSymbol);
            this.symbol = markerSymbol;
            this.updateGraphic();
        }
    });

});