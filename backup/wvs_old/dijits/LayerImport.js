console.log("wvs/dijits/LayerImport.js");

define([
    "dojo/_base/declare"
    , "dojo/_base/lang"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dojo/text!./templates/LayerImport.html"
    , "dojo/on"
    , "dijit/form/Select"
    , "dijit/form/TextBox"
    , "dijit/form/Button"
    , "dijit/form/CheckBox"
    , "dojo/dom-construct"
    , "dojo/dom-style"
    , "esri/layers/ArcGISTiledMapServiceLayer"
    , "esri/layers/ArcGISImageServiceLayer"
    , "esri/layers/ArcGISDynamicMapServiceLayer"
    , "esri/layers/FeatureLayer"
    , "esri/layers/GeoRSSLayer"
    , "esri/layers/KMLLayer"
    , "dojo/Deferred"
    , "dojo/_base/url"
    , "dojo/Evented"
    , "esri/request"
], function (declare, lang, _WidgetBase, _TemplatedMixin, template, on, Select, TextBox, Button, CheckBox, domConstruct, domStyle, ArcGISTiledMapServiceLayer, ArcGISImageServiceLayer, ArcGISDynamicMapServiceLayer, FeatureLayer, GeoRSSLayer, KMLLayer, Deferred, dojoUrl, Evented, esriRequest) {
    return declare([_WidgetBase, _TemplatedMixin, Evented], {
        templateString: template,

        declaredClass: "wvs.dijits.LayerImport",

        // mapManager: Boolean
        //      If the mapManager flag is set, we get items for the markup manager instead of layers
        mapManager: false,

        constructor: function (params, srcNodeRef) {
            // Defaults
            // If the mapManager flag is set, we get nodes instead of layers
            this.mapManager = false;

            lang.mixin(this, params);

            this.selectedOption = "autodetect";
            this.optionMapping = {
                autodetect: {
                    loader: "autoDetectResource",
                    regexes: [
                        { type: "arcgis", regex: 
                            this.mapManager ? [/MapServer\/*$/ig, /(MapServer|FeatureServer)\/[0-9]$/ig, /arcgis\/rest\/*(services\/*)?$/ig] : [/MapServer\/*$/ig, /(MapServer|FeatureServer)\/[0-9]$/ig]
                        },
                        { type: "kml", regex: [/\.(kmz|kml)$/ig] },
                        { type: "georss", regex: [/\.(rss|xml)$/ig, /.*georss.*/ig] }
                    ]
                },
                arcgis: {
                    loader: "loadArcGisResource"
                },
                kml: {
                    loader: "loadKMLFromWeb"
                },
                georss: {
                    loader: "loadGeoRssFromWeb"
                }
            };
            this.useAsBasemap = false;
        },
        postCreate: function () {
            var self = this;

            var options = [
                    { label: "Auto-Detect", value:"autodetect", selected: true },
                    { label: this.mapManager ? "ArcGIS Server/Web Service" : "ArcGIS Web Service", value:"arcgis" },
                    { label: "KML File", value:"kml" },
                    { label: "GeoRSS File", value:"georss" }
            ];

            // Create widgets

            // Our main select for our options
            this.layerSelect = new Select({
                options: options
            }, this.layerSelect);

            // The user input for the URL
            this.urlTextBox = new TextBox({
                trim: true
            }, this.urlTextBox);


            // Use as basemap for map servers
            this.useAsBasemapButton = new CheckBox({
                value: false,
                checked: false,
                onChange: function () {
                    self.useAsBasemap = this.get("checked");
                }
            }, this.useAsBasemapButton);

            // Submit button
            this.submitButton = new Button({
                label: "Add",
                onClick: lang.hitch(this, this.submit)
            }, this.submitButton);

            this.own(
                on(this.layerSelect, "change", function () {
                    self.selectedOption = this.get("value");
                    if (self.selectedOption === "arcgis") {
                        domStyle.set(self.useAsBasemapDiv, { display: "block" });
                    }
                    else {
                        domStyle.set(self.useAsBasemapDiv, { display: "none" });
                    }
                })
            );
        },
        clearForm: function(){
            this.urlTextBox.set("value", "");
            this.layerSelect.set("value", "autodetect");
            this.useAsBasemapButton.set("checked", false);
        },
        submit: function(){
            var url = this.urlTextBox.get("value");
            
            if (url) {
                this[this.optionMapping[this.selectedOption]["loader"]](url);
            }
            else {
                throw new Error("No URL provided");
            }
        },
        _setLoading: function (loading) {
            var loading = loading === true || false;
            if (loading) {
                this.submitButton.set("disabled", true);
                this.submitButton.set("iconClass", "icon-spinner icon-spin");
            }
            else {
                this.submitButton.set("disabled", false);
                this.submitButton.set("iconClass", "");
            }
            
        },
        autoDetectResource: function (url) {
            this._setLoading(true);
            var regexes = this.optionMapping.autodetect.regexes;
            for (var i = 0, il = regexes.length; i < il; i++) {
                var entry = regexes[i];
                for (var j = 0, jl = entry.regex.length; j < jl; j++){
                    var regex = entry.regex[j];
                    if (url.match(regex)) {
                        return this[this.optionMapping[entry.type]["loader"]](url);
                    }
                }
            }
            alert("Could not auto-detect your given resource");
            this._setLoading(false);
        },
        loadArcGisResource: function (url) {
            var self = this,
                deferred = new Deferred(),
                layer = null,
                errorMessage = "There was a problem loading your ArcGIS Service";

            this._setLoading(true);

            // FeatureLayer
            if(url.match(/(MapServer|FeatureServer)\/[0-9]$/g)){
                layer = new FeatureLayer(url);
                on(layer, "error", function (error) {
                    self.emit("import-error", error);
                    self._setLoading(false);
                    deferred.reject(errorMessage);
                });
                on(layer, "load", function (evt) {
                    var response = self.mapManager ? { type: "FeatureLayer", url: url, name: evt.layer.name } : evt.layer;
                    self.emit("layer-imported", response);
                    self._setLoading(false);
                    deferred.resolve(response);
                });

            }
            //ArcGISDynamicMapServiceLayer (or use as basemap)
            else if (url.match(/MapServer\/*$/g)) {
                if (this.useAsBasemap) {
                    layer = new ArcGISTiledMapServiceLayer(url);
                    on(layer, "error", function (error) {
                        self.emit("import-error", error);
                        deferred.reject(errorMessage);
                        self._setLoading(false);
                    });
                    on(layer, "load", function (evt) {
                        var response = self.mapManager ? { type: "esriBasemap", url: url, name: evt.layer.getServiceName() } : evt.layer;
                        self.emit("layer-imported", response);
                        deferred.resolve(response);
                        self._setLoading(false);
                    });
                }
                else {
                    layer = new ArcGISDynamicMapServiceLayer(url);
                    on(layer, "error", function (error) {
                        self.emit("import-error", error);
                        deferred.reject(errorMessage);
                        self._setLoading(false);
                    });
                    on(layer, "load", function (evt) {
                        var response = self.mapManager ? { type: "esriMapService", url: url, name: evt.layer.getServiceName() } : evt.layer;
                        self.emit("layer-imported", response);
                        deferred.resolve(response);
                        self._setLoading(false);
                    });
                }
            }
            //ArcGISImageServiceLayer
            else if (url.match(/ImageServer\/*$/g)) {
                layer = new ArcGISImageServiceLayer(url);
                on(layer, "error", function (error) {
                    self.emit("import-error", error);
                    deferred.reject(errorMessage);
                    self._setLoading(false);
                });
                on(layer, "load", function (evt) {
                    var response = self.mapManager ? { type: "esriImageService", url: url, name: evt.layer.getServiceName() } : evt.layer;
                    self.emit("layer-imported", response);
                    deferred.resolve(response);
                    self._setLoading(false);
                });
            }
            else if (this.mapManager && url.match(/arcgis\/rest\/*(services\/*)?$/ig)) {
                var uri = new dojoUrl(url),
                    response = { type: "esriServer", url: url, name: uri.host };

                var request = esriRequest({
                    url: url,
                    content: { f: "json" },
                    handleAs: "json",
                    callbackParamName: "callback"
                });
                request.then(
                    function (r) {
                        self.emit("layer-imported", response);
                        deferred.resolve(response);
                        self._setLoading(false);
                    },
                    function (error) {
                        self.emit("import-error", error);
                        deferred.reject(errorMessage);
                        self._setLoading(false);
                    });

            }
            else {
                setTimeout(function () {
                    self.emit("import-error", errorMessage);
                    deferred.reject(errorMessage);
                    self._setLoading(false);
                }, 500);
            }

            return deferred.promise;
        },
        loadKMLFromWeb: function (url) {
            var deferred = new Deferred(),
                layer = new KMLLayer(url),
                self = this;

            this._setLoading(true);
            on(layer, "error", function (error) {
                    self.emit("import-error", error);
                    deferred.reject("There was a problem loading your KML file.");
                    self._setLoading(false);
                });
                on(layer, "load", function (evt) {
                    var name = evt.layer.folders.length > 0 ? evt.layer.folders[0].name : evt.layer.name;
                    // if we are feeding the map manager, we want to give it a node
                    var response = self.mapManager ? { type: "KML", url: url, name: name } : evt.layer;
                    self.emit("layer-imported", response);
                    deferred.resolve(response);
                    self._setLoading(false);
                });

            return deferred.promise;
        },
        loadGeoRssFromWeb: function (url) {
            var deferred = new Deferred(),
                layer = new GeoRSSLayer(url),
                self = this;

            this._setLoading(true);

            on(layer, "error", function (error) {
                self.emit("import-error", error);
                deferred.reject("There was a problem loading your GeoRSS feed.");
                self._setLoading(false);
            });
            on(layer, "load", function (evt) {
                // if we are feeding the map manager, we want to give it a node
                var response = self.mapManager ? { type: "GeoRSS", url: url, name: evt.layer.name } : evt.layer;
                self.emit("layer-imported", response);
                deferred.resolve(response);
                self._setLoading(false);
            });

            return deferred.promise;
        }

    });
});