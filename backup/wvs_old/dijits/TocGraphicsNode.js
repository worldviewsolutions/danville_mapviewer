console.log("wvs/dijits/TocGraphicsNode.js");

define([
    "dojo/_base/declare"
    , "dojo/text!./templates/_TocNode.html"
    , "dojo/_base/lang"
    , "./_TocNode"
    , "dojox/gfx"
    , "esri/geometry/Polygon"
    , "esri/geometry/Point"
    , "esri/geometry/Polyline"
    , "esri/symbols/PictureMarkerSymbol"
    , "dojo/dom-construct"
    , "dojo/dom-class"
    , "dijit/Menu"
    , "dijit/MenuItem"
    , "dojo/dom-attr"
    , "./GraphicEditor"
    , "dojo/dom"
    , "dojo/dom-style"
    , "dojo/on"
    , "dojo/aspect"
    , "dojo/has"
], function (declare, template, lang, _TocNode, gfx, Polygon, Point, Polyline, PictureMarkerSymbol, domConstruct, domClass, Menu, MenuItem, domAttr, GraphicEditor, dom, domStyle, on, aspect, has) {
    return declare([_TocNode], {

        templateString: template,

        declaredClass: "wvs.dijits.TocGraphicsNode",

        // refreshDelay: Integer
        //      Number of milliseconds to delay between clicking a checkbox and refreshing the map service. A longer delay allow the user to check/uncheck multiple
        //      layers before refreshing the map service."dojo/ready",
        refreshDelay: 500,

        // useSpriteImage: Boolean
        //      Use the custom sprite image for the legend icons. When false, the "Legend" REST service will be used for 10.01 and newer services.
        useSpriteImage: true,

        hasDropDown: true,

        constructor: function (params, srcNodeRef) {
            console.log(this.declaredClass + " constructor");
            params = params || {};
            if (!params.graphic) {
                throw new Error('graphic not defined in params for TocGraphicsNode');
            }

            lang.mixin(this, params);

            this.title = this.graphic.getTitle() ? this.graphic.getTitle() : "Unnamed graphic";
            
        },
        // extension point called by framework
        postCreate: function () {
            this.inherited(arguments);
            this._createIcon();
            this.labelNode.innerHTML = this.title;
            aspect.after(this.graphic, "setSymbol", lang.hitch(this, function () {
                domConstruct.empty(this.iconNode);
                this._createIcon();
            }));

            this._createContextMenu();
        },
        setVisible: function (visible) {
            // return if the visibility has not changed
            if (this._state.visible == visible) return;

            this._state.visible = this._state.userEnabled = visible;

            // update the map service
            if (visible) {
                this.graphic.show();
                this._showSwatch();
            } else {
                this.graphic.hide();
                this._hideSwatch();
            }
        },
        _createIcon: function(){
            var geometry = this.graphic.geometry,
                symbol = this.graphic.symbol;

            // If we have an image, just make the image from the source URL
            if (symbol.isInstanceOf(PictureMarkerSymbol)) {
                domConstruct.create("img", { src: symbol.url }, this.iconNode);
                domClass.remove(this.iconNode, "jstoolkit-hidden");
                return;
            }
            else{
                var w = 20,
                    h = 20,
                    symbolDiv = domConstruct.create("div", { display: "inline" }),
                    surface = gfx.createSurface(symbolDiv, w, h);

                if (geometry.isInstanceOf(Polygon)) {
                    var rect = surface.createRect({ x: 0, y: 0, width: w, height: h });
                    rect.setFill(symbol.getFill());
                    rect.setStroke(symbol.getStroke());
                }
                else if (geometry.isInstanceOf(Polyline) && !has("ie") || has("ie") > 8) {
                    var line = surface.createLine({ x1: 0, y1: h / 2, x2: w, y2: h / 2 });
                    line.setStroke(symbol.getStroke());
                }
                else if (geometry.isInstanceOf(Point)) {
                    // TODO: Finish implemnting point
                    var circle = surface.createCircle({cx: w / 2, cy: h / 2, r: w / 2});
                    circle.setFill(symbol.getFill());
                    circle.setStroke(symbol.getStroke());
                }
                domConstruct.place(symbolDiv, this.iconNode);
                domClass.remove(this.iconNode, "jstoolkit-hidden");
            }


            

            //else if (geometry.isInstanceOf(
            //surface.createLine({ x1: 5, y1: 5, x2: 30, y2: 5 }).setStroke({ color: "black", style: SimpleLineSymbol[supportedLineStyle], width: 2 });
        },
        _checkboxOnClick: function (e) {
            this.setVisible(this._checkbox && this._checkbox.checked);
        },
        _createContextMenu: function () {
            console.log(this.declaredClass + ' _createContextMenu');
            var self = this;

            var mapServiceOptionsMenu = new Menu({
                leftClickToOpen: true
            });

            mapServiceOptionsMenu.addChild(new MenuItem({
                label: "Zoom To",
                iconClass: "jstoolkit-icon jstoolkit-icon-zoom",
                title: "Show in Map",
                onClick: lang.hitch(self._parent, self._parent.zoomToNode, self)
            }));
            mapServiceOptionsMenu.addChild(new MenuItem({
                label: "Remove",
                iconClass: "jstoolkit-icon jstoolkit-icon-delete",
                title: "Remove selected graphic",
                onClick: lang.hitch(this, function () {
                    if (this.graphicEditor) {
                        this.graphicEditor.destroy();
                    }
                    this.graphic._graphicsLayer.remove(this.graphic);
                })
            }));
            
            mapServiceOptionsMenu.bindDomNode(this.dropDownMenuIcon);
            mapServiceOptionsMenu.startup();
            this.mapServiceOptionsMenu = mapServiceOptionsMenu;
        }

    });
});