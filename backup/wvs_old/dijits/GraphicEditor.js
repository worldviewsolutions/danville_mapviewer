﻿define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dojo/text!./templates/GraphicEditor.html",
    "dojo/on",
    "dojo/Evented",
    "dijit/form/TextBox",
    "dijit/form/Textarea",
    "./SymbolCreator",
    "dijit/form/Button",
    "esri/graphic",
    "dojo/dom-style",
    "esri/toolbars/edit",
    "esri/geometry/Point",
    "esri/geometry/Polygon",
    "esri/geometry/Polyline",
    "dojo/aspect",
    "./SimpleButton",
    "../_base/config",
    "../common/ObjUtil",
    "../extensions/esri/graphic"
], function (
    declare,
    lang,
    array,
    _WidgetBase,
    _TemplatedMixin,
    template,
    on,
    Evented,
    TextBox,
    Textarea,
    SymbolCreator,
    Button,
    Graphic,
    domStyle,
    Edit,
    Point,
    Polygon,
    Polyline,
    aspect,
    SimpleButton,
    wvsConfig,
    ObjUtil
) {
    return declare([_WidgetBase, _TemplatedMixin, Evented], {
        templateString: template,
        declaredClass: "wvs.dijits.GraphicEditor",

        constructor: function (params, srcNodeRef) {
            var self = this;
            this.inherited(arguments);
            params = params || {};

            // graphic: Graphic
            //      the graphic to edit
            this.graphic = null;

            this.infoTemplateManager;

            require(["wvs/common/InfoTemplateManager"], function (InfoTemplateManager) {
                self.infoTemplateManager = InfoTemplateManager;
            });

            // map: Map
            //      the map which the graphic is attached to
            this.map = null;

            if (!params.graphic) {
                throw new Error("no graphic defined for " + this.declaredClass);
            }
            if (!params.map) {
                throw new Error("no map defined for " + this.declaredClass);
            }
            lang.mixin(this, params);

            // backup: Object
            //      an object representing the initial state of the graphic before editing
            this.backup = {
                graphic: this.graphic.toJson(),
                title: this.graphic.getTitle(),
                description: this.graphic.description
            };

            if (!this.graphic.isEditable()) {
                this.graphic.setEditing(true);
            }

            this._editToolbar = new Edit(this.map);
        },
        postCreate: function () {
            var self = this;
            this.inherited(arguments);

            this.titleTextBox = new TextBox({
                value: this.graphic.getTitle(),
                onKeyUp: function (evt) {
                    self.graphic.infoTemplate.setTitle(this.get("value"));
                    self.graphic.setModified();
                }
            }, this.titleTextBox);

            this.descriptionTextarea = new Textarea({
                value: this.graphic.attributes.description,
                style: "width:200px;",
                onKeyUp: function (evt) {
                    self.graphic.attributes.description = this.get("value");
                    self.graphic.setModified();
                }
            }, this.descriptionTextarea);

            this.symbolCreator = new SymbolCreator({ graphic: this.graphic, showCustomMapIcons: wvsConfig.defaults.showCustomMapIcons }, this.symbolCreator);
            this.symbolCreator.startup();

            //label: "Yay", iconClass: "icon-trash";
            this.saveButton = new SimpleButton({
                label: "",
                tooltip: "Save this graphic",
                leftIconClass: "icon-save",
                onClick: lang.hitch(this, function () { this.save(); })
            }, this.saveButton);

            this.cancelButton = new SimpleButton({
                label: "",
                tooltip: "Cancel changes made to this graphic",
                leftIconClass: "icon-ban-circle",
                onClick: lang.hitch(this, function () { this.cancel(); })
            }, this.cancelButton);

            this.deleteButton = new SimpleButton({
                label: "",
                leftIconClass: "icon-trash",
                tooltip: "Delete this graphic",
                onClick: lang.hitch(this, function () { this.deleteGraphic(); })
            }, this.deleteButton);

            this.modifiedEvent = aspect.after(this.graphic, "setModified", lang.hitch(this, function () {
                this.saveButton.set("disabled", false);
                this.cancelButton.set("disabled", false);
            }));
        },
        startup: function () {
            // Hide the map info (bubble) window
            this.map.infoWindow.hide();

            // Decide what kind of editing we will do on either geometry type or custom graphic type
            var geometry = this.graphic.geometry;
            if (geometry.isInstanceOf(Point)) {
                this._editToolbar.activate(Edit.MOVE, this.graphic);
                this.own(on(this._editToolbar, "graphic-move-stop", lang.hitch(this, this.onChange)));
            } else if (geometry.isInstanceOf(Polygon) || geometry.isInstanceOf(Polyline)) {
                if (this.graphic._type && (this.graphic._type === "ARROW" || this.graphic._type === "RECTANGLE")) {
                    this._editToolbar.activate(Edit.SCALE, this.graphic, { uniformScaling: true });
                    this.own(on(this._editToolbar, "scale-stop", lang.hitch(this, this.onChange)));
                } else if (this.graphic._type !== "BUFFER") {
                    this._editToolbar.activate(Edit.EDIT_VERTICES, this.graphic);
                    this.own(on(this._editToolbar, "vertex-move-stop", lang.hitch(this, this.onChange)),
                        on(this._editToolbar, "vertex-add", lang.hitch(this, this.onChange)));
                }
            } else {
                throw new Error("Unsupported geometry type for " + this.declaredClass);
            }

            // Enable/Disable Cancel/Save buttons based on whether or not this graphic is saved
            console.log("graphicEditor.startup->save/cancel button status being based on this.graphic.isSaved()->" + this.graphic.isSaved());
            this.saveButton.set("disabled", this.graphic.isSaved());
            this.cancelButton.set("disabled", this.graphic.isSaved());
        },

        destroy: function () {
            this._editToolbar.deactivate();
            this.modifiedEvent.remove();
            var parentNode = this.domNode && this.domNode.parentNode ? this.domNode.parentNode : null;
            if (parentNode) {
                domStyle.set(parentNode, { display: "none" });
            }
            this.symbolCreator.destroy();
            this.emit("close");
            this.inherited(arguments);
        },
        save: function () {
            this.graphic.save();
            this.destroy();
        },
        cancel: function () {
            // Fix 4/21: We now update the graphic instead of making a copy which can break things

            var graphic = new Graphic(this.backup.graphic);
            this.graphic.setGeometry(graphic.geometry);
            this.graphic.setSymbol(graphic.symbol);
            this.graphic.description = this.backup.description;
            this.graphic.setInfoTemplate(this.infoTemplateManager.getTabularAttributeTemplate({ title: this.backup.title }));
            this.destroy();
        },
        deleteGraphic: function () {
            var gLayer = this.graphic._markupLayer;
            gLayer.remove(this.graphic);
            this.destroy();
        },
        hide: function () {
            if (!this.cancelButton.get("disabled")) {
                this.cancel();
            }
            else {
                this.destroy();
            }
        },
        onChange: function (evt) {
            var graphicState = this._editToolbar.getCurrentState();
            if (graphicState.isModified) {
                this.saveButton.set("disabled", false);
                this.cancelButton.set("disabled", false);
            }
        }
    });
});