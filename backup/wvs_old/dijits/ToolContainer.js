﻿console.log("wvs/dijits/ToolContainer.js");

define([
    "dojo/_base/declare"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dojo/text!./templates/ToolContainer.html"
    , "dojo/_base/lang"
    , "./Identify"
    , "./Measure"
    , "./Swipe"
    , "./Markup"
    , "dojo/dom-style"
    , "dojo/dom-construct"
    , "dojo/Evented"
], function (declare, _WidgetBase, _TemplatedMixin, template, lang, Identify, Measure, Swipe, Markup, domStyle, domConstruct, Evented) {
    // module:
    //      wvs / dijits / ToolContainer
    return declare([_WidgetBase, _TemplatedMixin, Evented], {
        templateString: template,

        declaredClass: "wvs.dijits.ToolContainer",

        toolConstructors: {
            Identify: Identify,
            Measure: Measure,
            Swipe: Swipe,
            Markup: Markup
        },

        constructor: function (params, srcNodeRef) {

            // map: Map
            //      an instance of the map for the tool container to connect to
            this.map = null;

            // mapTool
            
            if (!params.map) {
                throw new Error("no map defined");
            }

            // enclosedWidget: Object
            //      an instace of the currently active widget/tool
            this.enclosedWidget = null;

            lang.mixin(this, params);
        },
        activateTool: function (toolName, params, toolTitle) {
            // summary:
            //      activates a widget and displays it within the tool container
            // toolName: String
            //      a string index that represents the constructor for a given tool
            // params: Object
            //      a key-value parameter hash for the given tool
            // toolTitle: String
            //      an optional title string to override the default title
            domStyle.set(this.domNode, "display", "block");
            params = params || {};
            if (this.enclosedWidget) {
                this.enclosedWidget.destroy();
                this.enclosedWidget = null;
            }
            this.enclosedWidget = new this.toolConstructors[toolName](lang.mixin({ map: this.map }, params), domConstruct.create("div", null, this.activeTool));
            this.enclosedWidget.startup();
            this.toolNameSpan.innerHTML = toolTitle || toolName;
            this.emit("tool-activated");
        },
        hide: function () {
            // summary:
            //      hides the tool container
            domStyle.set(this.domNode, "display", "none");
            if (this.enclosedWidget) {
                this.enclosedWidget.destroy();
                this.enclosedWidget = null;
            }
            
            this.emit("tool-deactivated");
        },
        show: function () {
            // summmary:
            //      shows the tool container
            domStyle.set(this.domNode, "display", "block");
        }

    });

});