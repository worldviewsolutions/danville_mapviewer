﻿define([
	"dojo/_base/lang",
    "dojo/_base/declare",
    "dojo/_base/array",
    "dojo/on",
    "dojo/Deferred",
    "dojo/when",
	"dojo/dom",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dijit/layout/BorderContainer",
    "dojox/layout/ToggleSplitter",
    "dijit/_TemplatedMixin",
    "dojo/text!./templates/MapViewer.html",
    "esri/map",
    "esri/geometry/Extent",
    "esri/dijit/Scalebar",
    "esri/dijit/OverviewMap",
    "esri/dijit/HomeButton",
    "./Swipe",
    "esri/dijit/BasemapGallery",
	"esri/layers/ArcGISTiledMapServiceLayer",
	"esri/layers/ArcGISDynamicMapServiceLayer",
	"esri/layers/FeatureLayer",
    "esri/layers/GraphicsLayer",
    "esri/layers/GeoRSSLayer",
    "esri/layers/ArcGISImageServiceLayer",
    "./ResultsConsole",
    "./CoordinateDisplay",
    "./Toc",
    "./Filter/AttributeSearch",
    "./ZoomToXY",
    "./Identify",
    "./Markup",
    "./MapLink",
    "../extensions/esri/layers/MarkupLayer",
    "./ToolContainer",
    "../common/SearchResultsStore",
    "dijit/layout/TabContainer",
    "dijit/layout/ContentPane",
    "dijit/Menu",
    "dijit/DropDownMenu",
	"dijit/MenuBar",
	"dijit/MenuItem",
    "dijit/PopupMenuBarItem",
	"dijit/PopupMenuItem",
    "dijit/MenuBarItem",
    "dijit/MenuSeparator",
	"dijit/TitlePane",
    "esri/dijit/Basemap",
    "esri/dijit/BasemapLayer",
    "require",
    "esri/dijit/Popup",
    "dojo/aspect",
    "dojo/dom-style",
    "./MapManager",
    "dijit/Dialog",
    "dojo/window",
    "../_base/config",
    "esri/config",
    "dojo/string",
    "./Print",
    "../common/Common",
    "../common/Bookmark",
    "dojo/query",
    "dojo/io-query",
    "dojo/has",
    "esri/graphic",
    "dojo/_base/unload",
    "../common/MarkupManager",
    "../common/InfoTemplateManager",
    "../common/MapState",
    "./BookmarkManager",
    "esri/symbols/jsonUtils",
    "./SimpleSearch",
    "dojo/_base/url",
    "./Azimuth",
    "./Accordion",
    "esri/IdentityManager",
    "esri/urlUtils",
    "../extensions/esri/layers/ArcGISWebMapLayer",
    "esri/request",
    "dojo/_base/connect",
    "wvs/dijits/SimpleButton",
    "./BasemapDropdown",
    "wvs/extensions/esri/layers/GoogleMapsLayer",
    "esri/tasks/GeometryService",
    "dojo/fx",
    "dojo/store/Memory",
    // Extensions/Plug-ins (these are behavioral; they do not import into anything)
    "../extensions/esri/Map",
    "../extensions/esri/layers/ArcGISDynamicMapServiceLayer",
    "../extensions/esri/layers/LayerInfo",
    "../extensions/dojo/_base/lang",
    'dojo/domReady!'
], function (
	lang,
    declare,
    array,
	on,
   Deferred,
   when,
    dom,
    domClass,
    domConstruct,
    BorderContainer,
    ToggleSplitter,
    _TemplatedMixin,
    template,
    Map,
    Extent,
    Scalebar,
    OverviewMap,
    HomeButton,
    Swipe,
    BasemapGallery,
	ArcGISTiledMapServiceLayer,
	ArcGISDynamicMapServiceLayer,
    FeatureLayer,
    GraphicsLayer,
    GeoRSSLayer,
    ArcGISImageServiceLayer,
    ResultsConsole,
	CoordinateDisplay,
    Toc,
    AttributeSearch,
    ZoomToXY,
    Identify,
    Markup,
    MapLink,
    MarkupLayer,
    ToolContainer,
    SearchResultsStore,
    TabContainer,
    ContentPane,
    Menu,
    DropDownMenu,
    MenuBar,
    MenuItem,
    PopupMenuBarItem,
    PopupMenuItem,
    MenuBarItem,
    MenuSeparator,
    TitlePane,
    Basemap,
    BasemapLayer,
    require,
    Popup,
    aspect,
    domStyle,
    MapManager,
    Dialog,
    win,
    wvsConfig,
    esriConfig,
    string,
    Print,
    Common,
    Bookmark,
    query,
    ioQuery,
    has,
    Graphic,
    baseUnload,
    MarkupManager,
    InfoTemplateManager,
    MapState,
    BookmarkManager,
    symbolJsonUtils,
    SimpleSearch,
    url,
    Azimuth,
    Accordion,
    IdentityManager,
    urlUtils,
    ArcGISWebMapLayer,
    esriRequest,
    connect,
    SimpleButton,
    BasemapDropdown,
    GoogleMapsLayer,
    GeometryService
) {
    var MapViewer = declare([BorderContainer, _TemplatedMixin], {

        templateString: template,

        constructor: function (params, srcNodeRef) {

            has.add("local-storage", function (global, document, anElement) {
                if (global.localStorage) {
                    return true;
                }
                else {
                    return false;
                }
            });

            this.settings = {};
            this.design = 'headline';
            this._splitterClass = ToggleSplitter;
            this.fullscreen = false;
            this._mapEvents = [];
            this._subscriptions = [];

            this.domNodeToComponentMappings = {
                bottomMapRegion: ["coordinateDisplay", "scaleBar"],
                basemapGalleryContainer: ["basemapGallery"],
                bottomPane: ["resultsConsole"],
                leftRegion: ["toc"],
                mapRegionMenuBar: ["mapToolbar"]
            };

            // setting should be passed in, do a null check
            var defaultSettings = {
                proxyUrl: "",
                alwaysUseProxy: false,
                // TODO: potentially rename this property from global
                // some options may affect more than one widget (like basemaps affecting the gallery and the map)
                globalOptions: {
                    basemaps: [],
                    layers: [],
                    layout: {
                        embedded: false,
                        header: {
                            visible: false,
                            content: []
                        },
                        minimized: {
                            headerRegion: true,
                            leftRegion: true,
                            bottomPane: true,
                            mapToolbar: true
                        },
                        maximized: {
                            headerRegion: false,
                            leftRegion: false,
                            bottomPane: false,
                            mapToolbar: false
                        }
                    },
                    proxyReplacements: [],
                    proxyRules: [
                        //{ proxyUrl: "", urlPrefix: "" }
                    ],
                    mapEvents: [
                        // Example: 
                        //{
                        //    ctr: eventConstructor,
                        //    options: {

                        //    }
                        //}
                    ],

                    autoSave: {
                        // if we have a save endpoint available (currently, only local storage if browser supports it,) then auto-save
                        enabled: false,
                        // auto-save when one of these topics is published; an empty array is equivalent to disabling auto-save
                        topics: ["map/extent/change", "map/basemap/change", "map/layer/add", "map/layer/remove", "markup/item/modify", "markup/item/add", "markup/item/remove", "markup/item/move", "markup/item/copy"]
                    },
                    showLoadingOverlay: true,
                    loadingOverlayImage: require.toUrl("./images/WVSLogo-Load.gif")
                },
                map: {
                    widgetOptions: {
                        logo: false,
                        sliderStyle: 'large',
                        slider: true,
                        basemap: "topo",
                        sliderPosition: "top-right"
                    }
                },
                mapToolbar: {
                    visible: true
                },
                bookmarks: {
                    visible: true,
                    menuTitle: "Bookmarks",
                    widgetTitle: "Bookmark Manager",
                    dependencies: ["mapToolbar"]
                },
                viewIn: {
                    visible: true,
                    dependencies: ["mapToolbar"]
                },
                profile: {
                    visible: false,
                    menuTitle: "Profile",
                    widgetTitle: "Profile",
                    widgetOptions: {}
                },
                exportTool: {
                    visible: true,
                    widgetOptions: {},
                    menuTitle: "Print",
                    widgetTitle: "Export Map & Print",
                    dependencies: ["mapToolbar"]
                },
                basemapGallery: {
                    visible: true,
                    widgetOptions: {
                        // If this is set to true, the basemap gallery is automatically loaded by esri (recommended to avoid for now for more control)
                        showArcGISBasemaps: false
                    },
                    esriBasemaps: [
                        {
                            options: {
                                // Following properties for basemap gallery widget and setBasemap method of the map
                                title: "Street Map"
                            },
                            layers: [
                                { url: "https://services.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer" }
                            ]
                        },
                        {
                            options: {
                                title: "Aerials"
                            },
                            layers: [
                                { url: "http://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer" }
                            ]
                        },
                        {
                            options: {
                                title: "Topographic"
                            },
                            layers: [
                                { url: "http://services.arcgisonline.com/ArcGIS/rest/services/USA_Topo_Maps/MapServer" }
                            ]
                        },
                        {
                            options: {
                                title: "National Geographic"
                            },
                            layers: [
                                { url: "http://services.arcgisonline.com/arcgis/rest/services/NatGeo_World_Map/MapServer" }
                            ]
                        },
                        {
                            options: {
                                title: "Light Gray"
                            },
                            layers: [
                                { url: "https://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Canvas_Light_Gray/MapServer" }
                            ]
                        },
                        {
                            options: {
                                title: "Shaded Relief"
                            },
                            layers: [
                                { url: "http://services.arcgisonline.com/ArcGIS/rest/services/World_Shaded_Relief/MapServer" }
                            ]
                        }
                    ]
                },
                identify: {
                    visible: true,
                    widgetOptions: {
                        mapServiceLabelText: "Map Services",
                        subLayerLabelText: "Sub-Layer"
                    },
                    menuTitle: "Identify",
                    widgetTitle: "Identify",
                    dependencies: ["mapToolbar"]
                },
                toc: {
                    visible: true,
                    widgetOptions: {
                        useSpriteImage: false,
                        // Support Feature Layers
                        enableFeatureLayers: true,
                        // Support ImageService Layers
                        enableImageServiceLayers: true,
                        // Support GeoRSS Layers
                        enableGeoRSSLayers: true,
                        showTiledServices: false
                    },
                    menuTitle: "Map Layers",
                    widgetTitle: "Map Layers"
                },
                mapManager: {
                    visible: true,
                    widgetOptions: {

                    },
                    menuTitle: "Map Manager",
                    widgetTitle: "Map Manager"
                },
                azimuth: {
                    visible: true,
                    widgetOptions: {},
                    menuTitle: "Azimuth",
                    widgetTitle: "Azimuth Tool",
                    dependencies: ["mapToolbar"]
                },
                markup: {
                    visible: true,
                    widgetOptions: {

                    },
                    menuTitle: "Markup",
                    widgetTitle: "Markup",
                    dependencies: ["mapToolbar"]
                },
                attributeSearch: {
                    visible: true,
                    widgetOptions: {
                        enableSaveAsQuickSearch: true
                    },
                    menuTitle: "Attribute Search",
                    widgetTitle: "Attribute Search",
                    dependencies: ["mapToolbar"]
                },
                measure: {
                    visible: true,
                    widgetOptions: {

                    },
                    menuTitle: "Measure",
                    widgetTitle: "Measure",
                    dependencies: ["mapToolbar"]
                },
                swipe: {
                    visible: true,
                    widgetOptions: {},
                    menuTitle: "Swipe",
                    widgetTitle: "Swipe",
                    dependencies: ["mapToolbar"]
                },
                zoomToXY: {
                    visible: true,
                    widgetOptions: {},
                    menuTitle: "Zoom To XY",
                    widgetTitle: "Zoom To XY",
                    dependencies: ["mapToolbar"]
                },
                coordinateDisplay: {
                    visible: true,
                    widgetOptions: {

                    }
                },
                simpleSearch: {
                    visible: true,
                    widgetOptions: {

                    }
                },
                mapLink: {
                    visible: true,
                    widgetOptions: {
                        enableShortUrls: true,
                        autoUpdateLink: true
                    },
                    menuTitle: "Share",
                    widgetTitle: "Create Map Link",
                    dependencies: ["mapToolbar"]
                },
                scaleBar: {
                    visible: true,
                    widgetOptions: {
                        // "dual" displays both miles and kilmometers
                        // "english" is the default, which displays miles
                        // "metric" for kilometers
                        scalebarUnit: "english"
                    }
                },
                resultsConsole: {
                    visible: false
                },
                accordion: {
                    visible: true,
                    widgetOptions: {}
                },
                overviewMap: {
                    visible: true,
                    widgetOptions: {
                        attachTo: "bottom-right",
                        expandFactor: 3
                    }
                },
                clientWidgets: []
            };

            lang.mixin(this.settings, defaultSettings);

            // mixin in user settings
            if (params.userSettings) {
                lang.mixinMissing(this.settings, params.userSettings);
            }

            if (this.settings.defaults) {
                lang.mixin(wvsConfig.defaults, this.settings.defaults);
            }


            this.controller = new MapViewerController(this);
            wvsConfig.defaults.mapViewerController = this.controller;

            // Enable Map Overlay (if set)
            if (this.settings.globalOptions.showLoadingOverlay) {
                var mapLoadOverlay = domConstruct.create("div", { id: "mapLoadOverlay" });
                var loadingImage = domConstruct.create("img", { src: this.settings.globalOptions.loadingOverlayImage }, mapLoadOverlay);
                domConstruct.create("br", {}, mapLoadOverlay);
                var loadingThrobber = domConstruct.create("img", { src: require.toUrl("./images/mapLoadOverlay-throb.GIF") }, mapLoadOverlay);
                this.mapLoadOverlay = mapLoadOverlay;
                domConstruct.place(mapLoadOverlay, window.document.body);
            }

            if (this.settings.proxyUrl) {
                esriConfig.defaults.io.proxyUrl = this.settings.proxyUrl;// DEPRECTATED: Not supported in IE10 this.settings.proxyUrl.match(/^\//) ? window.location.origin + this.settings.proxyUrl : this.settings.proxyUrl;
            }
            esriConfig.defaults.io.alwaysUseProxy = this.settings.alwaysUseProxy;
            esriConfig.defaults.geometryService = wvsConfig.defaults.services.geometry ? new GeometryService(wvsConfig.defaults.services.geometry) : null;

            this._processSettings();
            this._setUpProxy();

        },

        onGooglePlaceChangeHandler: function (geom) {

            if (geom.xmin != null) {
                //extent
                alert("extent");
                map.setExtent(geom);
            } else {
                //point
                alert("point");
                //create csc pushpin
                map.centerAndZoom(geom, 16);
            }
            map.centerAndZoom(point, 17);
        },

        startup: function () {
            if (this.loaded)
                return;

            var self = this;
            self.store = new SearchResultsStore();
            wvsConfig.defaults.searchResultStore = self.store;

            // Attach child widgets to the layout

            if (self.settings.globalOptions.layout.header.visible) {
                var headerSettings = self.settings.globalOptions.layout.header;
                if (headerSettings.template) {
                    domConstruct.destroy(self.headerRegion);
                    self.headerRegion = domConstruct.toDom(headerSettings.template);
                    self.simpleSearch = domConstruct.create("div", {}, self.headerRegion);
                    domConstruct.place(self.headerRegion, self.domNode, "first");
                }
                self.headerRegion = new ContentPane({
                    title: 'header',
                    splitter: false,
                    region: 'top',
                    style: headerSettings.style ? headerSettings.style : ""
                }, self.headerRegion);

                if (headerSettings.content && headerSettings.content.length) {
                    for (var i = 0; i < headerSettings.content.length; i++) {
                        domConstruct.place(headerSettings.content[i], self.headerRegion.domNode);
                    }
                }
            }

            if (self._componentsVisible(["toc", "accordion"])) {
                self.leftRegion = new TabContainer({
                    region: 'left',
                    splitter: true,
                    style: "width: 325px; font-size: small;"
                }, self.leftRegion);
                self.leftRegion.startup();
            } else if (self._componentsVisible(["toc"]) && !self._componentsVisible(["accordion"])) {
                self.leftRegion = new ContentPane({
                    region: 'left',
                    tabStrip: false,
                    splitter: true,
                    style: "width: 325px; font-size: small;"
                }, self.leftRegion);
                self.leftRegion.startup();
            }

            if (self._componentsVisible(["mapToolbar"])) {
                self.topMapRegion = new ContentPane({
                    title: 'Results',
                    splitter: false,
                    region: 'top',
                    style: "padding:0px"
                }, self.topMapRegion);

                self._createMainMenuBar();

                self.topMapRegion.startup();
            }

            self.mapRegion = new BorderContainer({
                design: 'headline',
                region: 'center',
                gutters: false
            }, self.mapRegion);
            self.mapRegion.startup();

            // The map should always exist
            var mapContentPane = new ContentPane({
                region: 'center',
                gutters: false,
                style: "padding: 0px"
            }, self.mapNode);
            mapContentPane.startup();

            if (self._componentsVisible(["coordinateDisplay", "scaleBar"], true)) {
                self.bottomMapRegion = new ContentPane({
                    region: 'bottom'
                }, self.bottomMapRegion);
                self.bottomMapRegion.startup();
            }

            //if (this._componentsVisible(["resultsConsole"])) {
            self.bottomPane = new ContentPane({
                title: 'Results',
                splitter: true,
                region: 'bottom'
            }, self.bottomPane);

            self.bottomPane.startup();
            //}

            // Remove dom nodes whose presence is dependent on its child widget(s) being visible (at least one must be visible)
            //this._parseDomNodeMappings();

            self.inherited(arguments);

            // For the time being, the infowindow highlight will be turned off. Ask Eddie for further info
            var popup = new Popup({}, domConstruct.create("div"));


            //if (this.settings.map.widgetOptions.basemap) {
            //   delete this.settings.map.widgetOptions.basemap;
            //}

            if (self.fullscreen) {
                self.maximize();
            }

            // create the map
            self.map = new Map(self.mapNode.id, lang.mixin({ infoWindow: popup }, self.settings.map.widgetOptions));

            // make the map public
            lang.setObject("window.map", self.map);

            InfoTemplateManager.setMap(self.map);

            self.homeButton = new HomeButton({
                map: self.map
            }, self.mapHomeButton);
            self.homeButton.startup();

            // Create the map widgets
            if (self._componentsVisible(["simpleSearch"])) {
                self.simpleSearch = new SimpleSearch(lang.mixin({ map: self.map }, self.settings.simpleSearch.widgetOptions), self.simpleSearch);
                self.simpleSearch.startup();
            }

            if (self._componentsVisible(["overviewMap"]) && self.map.getBasemap()) {
                var overviewMapDijit = new OverviewMap(lang.mixin({ map: self.map }, self.settings.overviewMap.widgetOptions));
                overviewMapDijit.startup();
            }
            if (this._componentsVisible(["basemapGallery"])) {
                var basemapGallery = new BasemapDropdown(lang.mixin({ map: this.map, basemapIds: this.map.basemapLayerIds }, this.settings.basemapGallery.widgetOptions), this.basemapGalleryNode);
                basemapGallery.startup();
                this.basemapGallery = basemapGallery;
            }

            if (self._componentsVisible(["scaleBar"])) {
                var scalebar = new Scalebar(lang.mixin({ map: self.map }, self.settings.scaleBar.widgetOptions), self.scalebarNode);
            }

            var layerPromises = self._loadLayers();
            self._setupMapEvents();
            for (var i = 0; i < layerPromises.length; i++) {
                when(layerPromises[i], function (layer) {
                    self.map.addLayer(layer);
                });
            }


            self.toolContainer = new ToolContainer({
                map: self.map,
                mapTool: "Identify"
            }, self.mapToolContainerNode);
            self.toolContainer.startup();

            on(self.map, "load", function (evt) {

                if (self._componentsVisible(["resultsConsole"])) {
                    self.results = new ResultsConsole(lang.mixin({ map: self.map, store: self.store }, self.settings.resultsConsole.widgetOptions), self.resultsConsole);

                    // Open the results pane when the results console has a search result
                    on(self.results, "result-found", function () {
                        var bottomSplitter = self.getSplitter("bottom");
                        if (bottomSplitter.get("state") !== "full") {
                            bottomSplitter._toggle();
                        }
                        if (self.bottomPane.h < 200) {
                            self.bottomPane.resize({ w: self.bottomPane.w, h: 200 });
                            self.resize();
                        }
                    });
                    self.results.startup();
                }

                // create the CoordinateDisplay
                if (self._componentsVisible(["coordinateDisplay"])) {
                    self.coordinateDisplay = new CoordinateDisplay(lang.mixin({ map: self.map }, self.settings.coordinateDisplay.widgetOptions), self.coordinateDisplayNode);
                    self.coordinateDisplay.startup();
                }

                if (self._componentsVisible(["toc", "accordion"])) {
                    var cp = new ContentPane({
                        title: "Map Layers",
                        style: "overflow:auto;"
                    });
                    self.toc = new Toc(lang.mixin({ map: self.map }, self.settings.toc.widgetOptions));
                    cp.addChild(self.toc);
                    self.leftRegion.addChild(cp);
                    self.toc.startup();
                } else if (self._componentsVisible(["toc"]) && !self._componentsVisible(["accordion"])) {
                    self.toc = new Toc(lang.mixin({ map: self.map }, self.settings.toc.widgetOptions), self.tocNode);
                    self.toc.startup();
                }

                if (self._componentsVisible(["accordion"])) {
                    var accordionCp = new ContentPane({
                        title: "Results",
                        style: "height:100%;width:100%;"
                    });
                    self.accordion = new Accordion(lang.mixin({ map: self.map, store: self.store }, self.settings.accordion.widgetOptions));
                    accordionCp.addChild(self.accordion);
                    self.leftRegion.addChild(accordionCp);
                    // Open the accordion tab when the accordion has a search result
                    on(self.accordion, "result-found", function () {
                        var leftSplitter = self.getSplitter("left");
                        if (leftSplitter.get("state") !== "full") {
                            leftSplitter._toggle();
                        }
                        self.leftRegion.selectChild(accordionCp);
                    });
                }
                // Enable client widgets
                var cWidgets = self.settings.clientWidgets;
                for (var i = 0; i < cWidgets.length; i++) {
                    if (cWidgets[i].visible) {
                        if (cWidgets[i].region === "left") {
                            var cp = new ContentPane({
                                title: cWidgets[i].displayName
                            }),
                            widget = new cWidgets[i].ctr(lang.mixin({ map: self.map }, cWidgets[i].widgetOption));
                            self[cWidgets[i].name] = widget;
                            cp.addChild(widget);
                            self.leftRegion.addChild(cp);
                        }
                    }
                }

                if (self.settings.globalOptions.showLoadingOverlay) {
                    domConstruct.destroy(self.mapLoadOverlay);
                }

                self._setupAutoSave();
                self.loaded = true;
                self.emit("mapviewer-loaded", self);
            });
            self._setDefaultEvents();
        },


        destroy: function () {
            var allEvents = this._mapEvents.concat(this._subscriptions);
            for (var i = 0; i < allEvents.length; i++) {
                allEvents[i].remove();
            }
            this.inherited(arguments);
        },

        resize: function (newSize, currentSize) {
            var self = this;
            if (this.settings.globalOptions.layout.embedded && !this.loaded) {
                on.once(this, "mapviewer-loaded", function () {
                    self.resize(newSize, currentSize);
                });
                return;
            } else {
                this.inherited(arguments);
            }
        },
        _setDefaultEvents: function () {
            var self = this;
            this.own(on(self.fullScreenButton, "click", function (evt) {
                if (self.fullscreen) {
                    self.minimize();
                } else {
                    self.maximize();
                }
            }));
        },

        _processSettings: function () {
            var self = this,
             settings = self.settings,
             // Check 1: Is there a map link?
             hasGetParams = self._processGetParams();
            // Check 2: If there is no map link, do we have local storage items? 
            if (!hasGetParams) {
                var hasLocalStorage = self._loadFromLocalStorage();
            }

            // We will save credentials to local storage on every successful login attempt
            // This is hacky, but there is no 'complete' event for the identity manager and it's silly to add in the same logic in x modules
            aspect.after(IdentityManager, "signIn", function (deferred) {
                var initCredentials = IdentityManager.credentials ? IdentityManager.credentials.length : 0;
                deferred.then(function () {
                    var resolveTime = Date.now();
                    var saveCredential = setInterval(function () {
                        if (IdentityManager.credentials.length > initCredentials) {
                            // Save credentials to local storage
                            if (has("local-storage"))
                                localStorage.setItem("credentials", JSON.stringify(IdentityManager.toJson()));
                        }
                        if ((Date.now() - resolveTime > 10000) || IdentityManager.credentials.length > initCredentials) {
                            clearInterval(saveCredential);
                        }
                    }, 2000);
                });

                return deferred;
            });

            // Set up map, layers, filters, etc.
            if (!settings.map.widgetOptions.extent && !settings.map.widgetOptions.center) {
                throw new Error("No extent or center point provided for map");
            }

            if (!hasGetParams && !hasLocalStorage) {
                settings.map.widgetOptions.extent = new Extent(settings.map.widgetOptions.extent);
            }


            // prepare basemaps and basemap gallery widget
            if (settings.basemapGallery.visible) {
                var basemaps = [],
                    userBasemaps = [];

                if (settings.globalOptions.basemaps.length > 0) {
                    var userBasemaps = self._processBasemapForGallery(settings.globalOptions.basemaps);
                    basemaps = basemaps.concat(userBasemaps);
                }
                if (!settings.basemapGallery.widgetOptions.showArcGISBasemaps && settings.basemapGallery.esriBasemaps.length > 0) {
                    basemaps = basemaps.concat(self._processBasemapForGallery(settings.basemapGallery.esriBasemaps));
                }
                if (basemaps.length == 0) {
                    settings.basemapGallery.widgetOptions.showArcGISBasemaps = true;
                }
                settings.basemapGallery.widgetOptions.basemaps = basemaps;
            }

            var setBasemapFromString = function (string) {
                for (var i = 0; i < settings.basemapGallery.widgetOptions.basemaps.length; i++) {
                    var basemap = settings.basemapGallery.widgetOptions.basemaps[i];
                    if (basemap.title === string) {
                        settings.map.widgetOptions.basemap = basemap;
                    }
                }
            };

            // Map Link basemap -> First user-defined basemap -> default basemap
            if (userBasemaps.length > 0) {
                settings.map.widgetOptions.basemap = userBasemaps[0];
            } else {
                setBasemapFromString(settings.map.widgetOptions.basemap);
            }
        },

        _processGetParams: function () {
            var self = this;
            var uri = window.document.location.search;
            var queryObject = ioQuery.queryToObject(uri.substring(uri.indexOf("?") + 1, uri.length));
            if (queryObject.sv) {
                queryObject.sv = JSON.parse(queryObject.sv);
                var settings = this.settings;
                // Set up map params
                if (queryObject.lat && queryObject.lon && queryObject.s && settings.map.widgetOptions.extent) {
                    delete settings.map.widgetOptions.extent;
                }


                if (queryObject.lat && queryObject.lon) {
                    settings.map.widgetOptions.center = [parseFloat(queryObject.lon), parseFloat(queryObject.lat)];
                }
                if (queryObject.s) {
                    settings.map.widgetOptions.scale = parseFloat(queryObject.s);
                }
                if (queryObject.b) {
                    settings.map.widgetOptions.basemap = queryObject.b;
                }
                // We do not use the defaults if we have layer info in the map link
                if (queryObject.sv.length > 0) {
                    settings.globalOptions.layers = [];
                }

                if (queryObject.mcId && queryObject.mcId != -1) {
                    var mm = new MarkupManager();
                    mm.getMarkupCollection(queryObject.mcId)
                        .then(
                            function (resp) {
                                var markup;
                                if (resp.options.options) {
                                    markup = JSON.parse(resp.options.options);
                                } else {
                                    markup = JSON.parse(resp.options);
                                }
                                settings.globalOptions.layers.push(markup);
                                on(self.map, 'load', function () {
                                    var lyr = new MarkupLayer(markup);
                                    self.map.addLayer(lyr);
                                });
                            }, function (err) {
                                console.warn(err);
                            }
                        );
                }


                for (var i = 0; i < queryObject.sv.length; i++) {
                    var service = queryObject.sv[i];
                    settings.globalOptions.layers.push(service);
                    if (service.qw && typeof service.qid === "number") {
                        on(self, "mapviewer-loaded", lang.hitch(self, function () {
                            var store = wvsConfig.defaults.searchResultStore,
                                queryUrl = service.url + "/" + service.qid,
                                queryTask = new QueryTask(queryUrl),
                                curQuery = new Query();

                            // clear the store and set it loading
                            store.clear();
                            store.setLoadingData(true);

                            // set up query
                            curQuery.where = service.qw.replace(/_eq_/g, '=');
                            curQuery.outFields = ["*"];
                            curQuery.returnGeometry = true;
                            //execute querytask
                            queryTask.execute(curQuery,
                                function (featureSet) {
                                    // zoom to the extent of the returned features
                                    if (featureSet.features && featureSet.features.length) {
                                        var extent = graphicsUtils.graphicsExtent(featureSet.features);
                                        if (extent) {
                                            self.map.setExtent(extent);
                                        }
                                    }

                                    var adapter = new SearchResultsAdapter();
                                    adapter.featureSetToResultSet(featureSet, queryUrl).then(function (searchResult) {
                                        store.put(searchResult);

                                        setTimeout(function () {
                                            if (self.accordion && self.accordion.gridMap[0]) {
                                                self.accordion.gridMap[0].grid.selectAll();
                                            }
                                        }, 2000);
                                    });
                                },
                                function (error) {
                                    store.setLoadingData(false);
                                }
                            );
                        }, service)
                        );
                    }
                }

                return queryObject.s && queryObject.lat && queryObject.lon; // Boolean
            }
            return false; // Boolean
        },

        _loadFromLocalStorage: function () {
            // summary:
            //      loads saved data from local storage
            // returns:
            //      whether or not there is saved map data
            var self = this,
             settings = self.settings;

            //Creates an internal 'sessionBookmarks' property that will be used to generate the complete list of items
            self.settings.bookmarks.sessionBookmarks = [];

            var pushBookmarks = function (bookmarkArray, bookmarkList) {
                for (var i in bookmarkList) {
                    var bookmark = new Bookmark(bookmarkList[i]);
                    if (bookmark.hasChildren()) {
                        var newChildren = [];
                        pushBookmarks(newChildren, bookmarkList[i].children);
                        bookmark.children = newChildren;
                    }
                    bookmarkArray.push(bookmark);
                }
            };

            if (has("local-storage")) {
                var savedMap = localStorage.getItem("m4") ? JSON.parse(localStorage.getItem("m4")) : null;

                if (savedMap) {
                    settings.map.widgetOptions.extent = new Extent(savedMap.map.extent);
                    settings.map.widgetOptions.basemap = savedMap.map.basemap;
                    // We do not use the defaults if we have layer info in local storage
                    if (savedMap.layers.length > 0) {
                        settings.globalOptions.layers = [];
                    }
                    for (var i = 0; i < savedMap.layers.length; i++) {
                        settings.globalOptions.layers.push(savedMap.layers[i]);
                    }
                    self.savedMap = savedMap;
                }
                if (self._componentsVisible(["bookmarks"]) && localStorage.getItem("bookmarks")) {
                    var bookmarkList = JSON.parse(localStorage.getItem("bookmarks"));
                    //Adding in a property to store the concatenated list of saved bookmarks from mapSettings.js and
                    //the user-created bookmarks stored in local storage.
                    pushBookmarks(self.settings.bookmarks.sessionBookmarks, bookmarkList);
                } else {
                    pushBookmarks(self.settings.bookmarks.sessionBookmarks, self.settings.bookmarks.savedBookmarks);
                }
                if (localStorage.getItem("credentials")) {
                    IdentityManager.initialize(JSON.parse(localStorage.getItem("credentials")));
                }
                return savedMap ? true : false; // Boolean
            } else {
                pushBookmarks(self.settings.bookmarks.sessionBookmarks, self.settings.bookmarks.savedBookmarks);
            }
            return false; // Boolean
        },

        _loadLayers: function () {
            // summary:
            //      create the array of layers to be added to the map
            var self = this,
            returnDeferreds = [];

            self.settings.map.initialLayers = self.settings.map.initialLayers || [];

            var hasMarkup = false;
            // We will always want our default layers

            var lyrs = self.settings.globalOptions.layers;
            for (var i = 0; i < lyrs.length; i++) {
                var instance = self._createLayerFromSettingsObject(lyrs[i]),
                newDeferred = new Deferred();
                if (lyrs[i].type === "MarkupLayer") {
                    this.markupLayer = wvsConfig.defaults.markupLayer = instance;
                    hasMarkup = true;
                }
                on.once(instance, "load", function () {
                    self.settings.map.initialLayers.push(instance);
                    newDeferred.resolve(instance);
                });
                returnDeferreds.push(newDeferred);
            }

            if (!hasMarkup && self._componentsVisible(["markup"])) {
                self.markupLayer = new MarkupLayer({ title: "Temporary Markup", id: "markupLayer" });
                wvsConfig.defaults.markupLayer = self.markupLayer;
                self.settings.map.initialLayers.push(self.markupLayer);
            }

            return returnDeferreds;
        },

        _processBasemapForGallery: function (basemapArray) {
            var basemaps = [];
            for (var i = 0; i < basemapArray.length; i++) {
                var basemapLayers = [];
                for (var j = 0; j < basemapArray[i].layers.length; j++) {
                    basemapLayers.push(new BasemapLayer(basemapArray[i].layers[j]));
                }
                var basemapOptions = lang.mixin({ layers: basemapLayers }, basemapArray[i].options);
                basemaps.push(new Basemap(basemapOptions));
            }
            return basemaps;
        },

        _setupMapEvents: function () {
            // summary:
            //      binds the passed in map events.
            // description:
            //      'Map events' are objects that encapsulate a map event and add a few common behaviors if necessary.
            //      Instead of the mapviewer itself having direct knowledge of these events, we can extend the _MapEventMixIn module
            //      and add in our own custom events as we like. This results in cleaner, more modular code.
            var self = this,
               evts = this.settings.globalOptions.mapEvents;
            for (var i = 0; i < evts.length; i++) {
                var mapEvent = new evts[i].ctr(self.map, evts[i].options || {});
                self._mapEvents.push(mapEvent);
                self.own(mapEvent.initialize());
            }
        },
        _createLayerFromSettingsObject: function (layerObject) {
            var layer = null;

            switch (layerObject.type) {
                case "ArcGISDynamicMapServiceLayer":
                    layer = new ArcGISDynamicMapServiceLayer(layerObject.url, layerObject.options || {});
                    break;
                case "FeatureLayer":
                    var options = layerObject.options || {};
                    layer = new FeatureLayer(layerObject.url, layerObject.options || {});
                    on.once(layer, "load", function () {
                        layer.setInfoTemplate(InfoTemplateManager.getTabularAttributeTemplate({ featureLayer: layer, createWidgetsFromGeometry: true }));
                    });
                    break;
                case "ArcGISWebMapLayer":
                    layer = new ArcGISWebMapLayer(layerObject.url, layerObject.options || {});
                    break;
                case "GraphicsLayer":
                    layer = new GraphicsLayer(layerObject.options || {});
                    break;
                case "GeoRSSLayer":
                    layer = new GeoRSSLayer(layerObject.url, layerObject.options || {});
                    break;
                case "ArcGISImageServiceLayer":
                    layer = new ArcGISImageServiceLayer(layerObject.url, layerObject.options || {});
                    break;
                case "MarkupLayer":
                    layer = new MarkupLayer(layerObject.options || {});
                    break;
                case "GoogleMapsLayer":
                    layer = new GoogleMapsLayer(layerObject.options || {});
                    break;
                default:
                    //throw new Error("an unknown or unsupported type was added to the map");
                    break;
            }

            return layer;
        },
        _setUpProxy: function () {
            // summary:
            //      sets up proxy rules by looking at layer endpoints and proxy rules defined in the configuration. If there are any proxy replacements (i.e. mapping a server to an "accessible" one for the proxy), 
            var proxyUrl = esriConfig.defaults.io.proxyUrl,
               proxyRules = this.settings.globalOptions.proxyRules,
               lyrs = this.settings.globalOptions.layers,
               basemaps = this.settings.basemapGallery.widgetOptions.basemaps,
               proxyReplacements = this.settings.globalOptions.proxyReplacements;
            // Look through proxy rules
            for (var i = 0; i < proxyRules.length; i++) {
                urlUtils.addProxyRule({ proxyUrl: proxyRules[i].proxyUrl || proxyUrl, urlPrefix: proxyRule.urlPrefix });
            }

            var parseLayerForProxy = function (layer) {
                var layerUrl = new url(layer.url);
                if (layerUrl.scheme !== "https" && layerUrl.scheme !== location.protocol.replace(/:/, "")) {
                    urlUtils.addProxyRule({ proxyUrl: proxyUrl, urlPrefix: layerUrl.scheme + "://" + layerUrl.authority });
                }
            };

            // Look through layers
            for (var i = 0; i < lyrs.length; i++) {
                parseLayerForProxy(lyrs[i]);
            }

            // Look through basemaps
            for (var i = 0 ; i < basemaps.length; i++) {
                for (var j = 0; j < basemaps[i].layers.length; j++) {
                    parseLayerForProxy(basemaps[i].layers[j]);
                }
            }

            if (proxyReplacements.length > 0) {
                var replaceUrl = function (ioArgs) {
                    if (ioArgs.content) {
                        var content = ioArgs.content;
                        for (var key in content) {
                            var item = content[key];
                            if (typeof item === "string") {
                                for (var i = 0; i < proxyReplaceents.length; i++) {
                                    item = item.replace(new RegExp(r.oldText.replace(/[-\/\\^$+?.()|[\]{}]/g, '\\$&'), "g"), r.newText);
                                }
                                ioArgs.content[key] = item;
                            }
                        }
                    }
                    return ioArgs;
                };
                esriRequest.setRequestPreCallback(replaceUrl);
            }
        },
        _setupAutoSave: function () {
            // summary:
            //      sets up auto save based on map settings
            var autoSaveSettings = this.settings.globalOptions.autoSave;
            if (autoSaveSettings.enabled && autoSaveSettings.topics.length) {
                for (var i = 0; i < autoSaveSettings.topics.length; i++) {
                    switch (autoSaveSettings.topics[i]) {
                        case "map/extent/change":
                            this.own(on(this.map, "extent-change", this.save));
                            break;
                        case "map/basemap/change":
                            this.own(on(this.map, "basemap-change", this.save));
                            break;
                        case "map/layer/add":
                            this.own(on(this.map, "layer-add", this.save));
                            break;
                        case "map/layer/remove":
                            this.own(on(this.map, "layer-remove", this.save));
                            break;
                        default:
                            this._subscriptions.push(connect.subscribe(topic, this.save));
                            break;
                    }
                }
            }
        },

        _createMainMenuBar: function () {
            var self = this;

            self.mapRegionMenuBar = new MenuBar({
                region: 'top'
            }, self.mapRegionMenuBar);

            var mapRegionToolsMenu = new DropDownMenu({});

            if (self._componentsVisible(["attributeSearch"])) {
                // Attribute Search Menu Item
                mapRegionToolsMenu.addChild(new MenuItem({
                    label: self.settings.attributeSearch.menuTitle,
                    iconClass: 'icon-search',
                    onClick: function () {
                        if (!self._componentsVisible(["simpleSearch"])) {
                            self.settings.attributeSearch.widgetOptions.enableSaveAsQuickSearch = false;
                        }
                        Common.showWidgetInDialog(AttributeSearch, lang.mixin({ map: self.map, store: self.store }, self.settings.attributeSearch.widgetOptions), { title: self.settings.attributeSearch.widgetTitle, "class": "nonModal", style: "width:850px;" });
                    }
                }));
            }

            if (self._componentsVisible(["identify"])) {
                // Identify Menu Item
                mapRegionToolsMenu.addChild(new MenuItem({
                    label: self.settings.identify.menuTitle,
                    iconClass: 'icon-info-sign',
                    onClick: lang.hitch(self, self._identifyTool)
                }));
            }

            if (self._componentsVisible(["markup"])) {
                // Markup Menu Item
                mapRegionToolsMenu.addChild(new MenuItem({
                    label: self.settings.markup.menuTitle,
                    iconClass: 'icon-pencil',
                    onClick: lang.hitch(self, self._showMarkupTool)
                }));
            }

            if (self._componentsVisible(["measure"])) {
                // Measure Menu Item
                mapRegionToolsMenu.addChild(new MenuItem({
                    label: self.settings.measure.menuTitle,
                    iconClass: 'icon-resize-full',
                    onClick: lang.hitch(self, self._measureTool)
                }));
            }

            if (self._componentsVisible(["azimuth"])) {
                // Measure Menu Item
                mapRegionToolsMenu.addChild(new MenuItem({
                    label: self.settings.azimuth.menuTitle,
                    iconClass: 'icon-resize-full',
                    onClick: function () {
                        Common.showWidgetInDialog(Azimuth, lang.mixin({ map: self.map }, self.settings.azimuth.widgetOptions), { title: self.settings.azimuth.widgetTitle, "class": "nonModal", style: "width:600px;" });
                    }
                }));
            }

            if (self._componentsVisible(["swipe"])) {
                // Swipe Menu Item
                mapRegionToolsMenu.addChild(new MenuItem({
                    label: self.settings.swipe.menuTitle,
                    iconClass: 'icon-move',
                    onClick: lang.hitch(self, self._swipeTool)
                }));
            }


            if (self._componentsVisible(["zoomToXY"])) {
                // Zoom To XY Menu Item
                mapRegionToolsMenu.addChild(new MenuItem({
                    label: self.settings.zoomToXY.menuTitle,
                    iconClass: 'icon-screenshot',
                    onClick: function () {
                        var dialog = Common.showWidgetInDialog(ZoomToXY, lang.mixin({ map: self.map }, self.settings.zoomToXY.widgetOptions), { title: self.settings.zoomToXY.widgetTitle, "class": "nonModal" });

                        on(dialog.content, "point-created", function (pt) {
                            var graphic = new Graphic(pt, symbolJsonUtils.fromJson(wvsConfig.defaults.symbols.point), {}, InfoTemplateManager.getTabularAttributeTemplate({ title: "XY Location", createWidgetsFromGeometry: true }));
                            if (self.markupLayer) {
                                graphic.setEditing(true);
                                graphic.save();
                                self.markupLayer.add(graphic);
                            } else {
                                self.map.graphics.add(graphic);
                            }

                            self.map.infoWindow.setContent(graphic.getContent());
                            self.map.infoWindow.setTitle(graphic.getTitle());
                            self.map.centerAndZoom(pt, self.map.getNumLevels()).then(function () {
                                self.map.infoWindow.show(pt);
                            });
                        });
                    }
                }));
            }


            if (self._componentsVisible(["viewIn"])) {

                var getMapInfo = function () {
                    var centerPoint = self.map.extent.getCenter();
                    return { level: self.map.getLevel(), latitude: centerPoint.getLatitude(), longitude: centerPoint.getLongitude() };
                },

                viewInMenu = new DropDownMenu({}),

                // Create bing menu
                bingMenu = new DropDownMenu({});

                bingMenu.addChild(new MenuItem({
                    label: "Roads",
                    onClick: function () {
                        var mapUrl = string.substitute("https://www.bing.com/maps/?lvl=${level}&cp=${latitude}~${longitude}&style=r", getMapInfo());
                        window.open(mapUrl);
                    }
                }));

                bingMenu.addChild(new MenuItem({
                    label: "Aerials",
                    onClick: function () {
                        var mapUrl = string.substitute("https://www.bing.com/maps/?lvl=${level}&cp=${latitude}~${longitude}&style=a", getMapInfo());
                        window.open(mapUrl);
                    }
                }));

                bingMenu.addChild(new MenuItem({
                    label: "Hybrid",
                    onClick: function () {
                        var mapUrl = string.substitute("https://www.bing.com/maps/?lvl=${level}&cp=${latitude}~${longitude}&style=h", getMapInfo());
                        window.open(mapUrl);
                    }
                }));

                bingMenu.addChild(new MenuItem({
                    label: "Bird's Eye",
                    onClick: function () {
                        var mapUrl = string.substitute("https://www.bing.com/maps/?v=2&cp=${latitude}~${longitude}&lvl=${level}&dir=3.025637994274562&sty=u&style=o", getMapInfo());
                        window.open(mapUrl);
                    }
                }));
                var bingMenuPopupMenuItem = new PopupMenuItem({ label: "Bing", iconClass: "icon-windows", popup: bingMenu });

                // Create Google Menu
                var googleMenu = new DropDownMenu({});
                googleMenu.addChild(new MenuItem({
                    label: "Roads",
                    onClick: function () {
                        var mapUrl = string.substitute("https://maps.google.com/?z=${level}&ll=${latitude},${longitude}&t=m", getMapInfo());
                        window.open(mapUrl);
                    }
                }));

                googleMenu.addChild(new MenuItem({
                    label: "Aerials",
                    onClick: function () {
                        var mapUrl = string.substitute("https://maps.google.com/?z=${level}&ll=${latitude},${longitude}&t=k", getMapInfo());
                        window.open(mapUrl);
                    }
                }));

                googleMenu.addChild(new MenuItem({
                    label: "Hybrid",
                    onClick: function () {
                        var mapUrl = string.substitute("https://maps.google.com/?z=${level}&ll=${latitude},${longitude}&t=h", getMapInfo());
                        window.open(mapUrl);
                    }
                }));

                googleMenu.addChild(new MenuItem({
                    label: "Earth",
                    onClick: function () {
                        var mapUrl = string.substitute("https://maps.google.com/?z=${level}&ll=${latitude},${longitude}&t=f", getMapInfo());
                        window.open(mapUrl);
                    }
                }));
                var googlePopupMenuItem = new PopupMenuItem({ label: "Google", iconClass: "icon-google-plus-sign", popup: googleMenu });

                viewInMenu.addChild(bingMenuPopupMenuItem);
                viewInMenu.addChild(googlePopupMenuItem);

                var viewInPopupMenuItem = new PopupMenuItem({ label: "View In", iconClass: 'icon-eye-open', popup: viewInMenu });
                mapRegionToolsMenu.addChild(viewInPopupMenuItem);
            }

            var mapRegionToolsPopupMenuBarItem = new PopupMenuBarItem({ label: "Tools", popup: mapRegionToolsMenu });

            self.mapRegionMenuBar.addChild(mapRegionToolsPopupMenuBarItem);

            if (self._componentsVisible(["bookmarks"])) {
                var bookmarks = self.settings.bookmarks.sessionBookmarks,
                    bookmarksMenu = new DropDownMenu({ _bIndex: 0, _fIndex: 0 });

                for (var i = 0; i < bookmarks.length; i++) {
                    bookmarks[i] = new Bookmark(bookmarks[i]);
                }
                //Maintain a _bIndex to keep track of the menu item index for bookmarks and a _fIndex for
                //the current index to insert folders into. This maintains a Folders-on-top/Bookmarks-on-bottom
                //paradigm for the menu.

                var addBookmarkMenuItemsFromArray = function (bookmarkArray, menu) {
                    if (bookmarkArray.length == 0) {
                        //If the current array is empty, indicate as such with a blank menu item.
                        menu.addChild(new MenuItem({
                            label: "(Empty)"
                        }), menu._bIndex);
                        menu._bIndex++;
                    } else {
                        for (var i = 0; i < bookmarkArray.length; i++) {
                            var bMark = bookmarkArray[i];
                            if (bMark.hasChildren()) {
                                //If the bookmark has children, it is a folder. Create a popout menu for all of it's child nodes and
                                //recursively run this method on that new menu.
                                var newMenu = new DropDownMenu({
                                    _bIndex: 0,
                                    _fIndex: 0
                                });
                                menu.addChild(new PopupMenuItem({
                                    label: bMark.name,
                                    iconClass: "icon-folder-close",
                                    popup: newMenu,
                                    _bookmark: bMark
                                }), menu._fIndex);
                                addBookmarkMenuItemsFromArray(bMark.children, newMenu);
                                //Increment the _fIndex and the _bIndex because folders can and will be added to the middle of the menu
                                //whereas bookmarks should only always be added to the bottom.
                                menu._fIndex++;
                                menu._bIndex++;
                            } else {
                                //If the bookmark has no children, it is a leaf in the bookmarks tree and can be added to the bottom
                                //of the current menu with an onClick event that will zoom to the bookmarked extent.
                                var menuItem = new MenuItem({
                                    label: bMark.name,
                                    iconClass: "icon-bookmark",
                                    _bookmark: bMark
                                });
                                //We have to explicitly add the 'mousedown' handler and hitch it to the context of the MenuItem
                                //so that we can access the instance property 'this._bookmark'. Otherwise the menu items will all
                                //set the map to the extent of the last bookmark added because the value in their onMouseDown functions
                                //will change with each iteration through the loop.
                                on(menuItem, "mouseDown", lang.hitch(menuItem, function () {
                                    self.map.setExtent(new Extent(this._bookmark.extent));
                                }));
                                menu.addChild(menuItem, menu._bIndex);
                                //Only increment the _bIndex since the _fIndex will stay the same so folders will always be added
                                //to the end of the folder list.
                                menu._bIndex++;
                            }
                        }
                    }
                };

                // Create Bookmarks Menu
                addBookmarkMenuItemsFromArray(bookmarks, bookmarksMenu);

                //Function to write a new bookmark tree to the menu dropdown
                var updateBookmarksMenu = function (bookmarks) {
                    self.settings.bookmarks.sessionBookmarks = bookmarks;
                    bookmarksMenu.destroyDescendants(false);
                    bookmarksMenu._bIndex = 0;
                    bookmarksMenu._fIndex = 0;
                    addBookmarkMenuItemsFromArray(bookmarks, bookmarksMenu);
                    addBookmarkManagerItem();
                    self.saveBookmarks();
                };

                self.bookmarksMenu = bookmarksMenu;

                //Function to add the bookmark manager node to the bottom of the menu dropdown
                var addBookmarkManagerItem = function () {
                    bookmarksMenu.addChild(new MenuSeparator({}));
                    bookmarksMenu.addChild(new MenuItem({
                        label: "Manage Bookmarks",
                        onClick: function () {
                            var dialog = Common.showWidgetInDialog(BookmarkManager, { map: map, bookmarks: bookmarks }, { title: "Bookmark Manager", "class": "nonModal" });
                            on(dialog.content, "bookmarks-updated", updateBookmarksMenu);
                        }
                    }));
                };

                addBookmarkManagerItem();

                var bookmarksPopupMenuItem = new PopupMenuBarItem({ label: "Bookmarks", popup: bookmarksMenu });

                self.mapRegionMenuBar.addChild(bookmarksPopupMenuItem);
            }

            // MapManager button
            if (self._componentsVisible(["mapManager"])) {
                self.mapRegionMenuBar.addChild(new MenuBarItem({
                    label: self.settings.mapManager.menuTitle,
                    onClick: lang.hitch(self, self._showMapManager)
                }));
            }

            // Print button
            if (self._componentsVisible(["exportTool"])) {
                self.mapRegionMenuBar.addChild(new MenuBarItem({
                    label: self.settings.exportTool.menuTitle,
                    iconClass: "icon-print",
                    onClick: function () {
                        Common.showWidgetInDialog(Print, lang.mixin({ map: self.map }, self.settings.exportTool.widgetOptions), { title: self.settings.exportTool.widgetTitle, "class": "nonModal" });
                    }
                }));
            }

            if (self._componentsVisible(["mapLink"])) {
                self.mapRegionMenuBar.addChild(new MenuBarItem({
                    label: self.settings.mapLink.menuTitle,
                    iconClass: "icon-share",
                    onClick: function () {
                        Common.showWidgetInDialog(MapLink, lang.mixin({ map: self.map }, self.settings.mapLink.widgetOptions), { title: self.settings.mapLink.widgetTitle, "class": "nonModal" });
                    }
                }));
            }

            self.mapRegionMenuBar.startup();
        },

        maximize: function () {
            var layout = this.settings.globalOptions.layout.maximized;
            if (this.settings.globalOptions.layout.header.visible) {
                var headerProp = layout.headerRegion ? "block" : "none";
                domStyle.set(this.headerRegion.domNode, { display: headerProp });
            }
            if (this.settings.toc.visible) {
                var displayProp = layout.leftRegion ? "block" : "none";
                var leftSplitter = this.getSplitter("left");
                domStyle.set(leftSplitter.child.domNode, { display: displayProp });
                domStyle.set(leftSplitter.domNode, { display: displayProp });
            }
            if (this.settings.resultsConsole.visible) {
                var displayProp = layout.bottomPane ? "block" : "none";
                var bottomSplitter = this.getSplitter("bottom");
                domStyle.set(bottomSplitter.child.domNode, { display: displayProp });
                domStyle.set(bottomSplitter.domNode, { display: displayProp });
            }
            if (this.settings.mapToolbar.visible) {
                var displayProp = layout.mapToolbar ? "block" : "none";
                domStyle.set(this.topMapRegion.domNode, { display: displayProp });
            }
            this.resize();

            this.fullscreen = true;
            domClass.add(this.fullScreenButton, "maximized");
            this.emit("maximized", this);
        },

        minimize: function () {
            var layout = this.settings.globalOptions.layout.minimized;
            if (this.settings.globalOptions.layout.header.visible) {
                var headerProp = layout.headerRegion ? "block" : "none";
                domStyle.set(this.headerRegion.domNode, { display: headerProp });
            }
            if (this.settings.toc.visible) {
                var displayProp = layout.leftRegion ? "block" : "none";
                var leftSplitter = this.getSplitter("left");
                domStyle.set(leftSplitter.child.domNode, { display: displayProp });
                domStyle.set(leftSplitter.domNode, { display: displayProp });
            }
            if (this.settings.resultsConsole.visible) {
                var displayProp = layout.bottomPane ? "block" : "none";
                var bottomSplitter = this.getSplitter("bottom");
                domStyle.set(bottomSplitter.child.domNode, { display: displayProp });
                domStyle.set(bottomSplitter.domNode, { display: displayProp });
            }
            if (this.settings.mapToolbar.visible) {
                var displayProp = layout.mapToolbar ? "block" : "none";
                domStyle.set(this.topMapRegion.domNode, { display: displayProp });
            }
            this.resize();

            this.fullscreen = false;
            domClass.remove(this.fullScreenButton, "maximized");
            this.emit("minimized", this);
        },

        _componentsVisible: function (componentNames, any) {
            var settings = this.settings,
                any = any ? true : false,
                visible = any ? false : true;

            for (var i = 0; i < componentNames.length; i++) {
                var componentSetting = settings[componentNames[i]];
                visible = any ? componentSetting.visible || visible : componentSetting.visible && visible;

                if (componentSetting.dependencies) {
                    for (var j = 0; j < componentSetting.dependencies.length; j++) {
                        var dependentVisible = settings[componentSetting.dependencies[j]].visible;
                        if (!dependentVisible) {
                            console.warn("The component, " + componentNames[i] + ", was not loaded because its dependency, " + componentSetting.dependencies[j] + ", is set not to load.");
                        }
                        visible = any ? dependentVisible || visible : dependentVisible && visible;
                    }
                }
            }
            return visible;
        },

        save: function () {
            if (!has("local-storage")) {
                return;
            }

            var mapState = new MapState(this.map);
            var mapExport = mapState.toJson();
            localStorage.setItem("m4", JSON.stringify(mapExport));

            this.saveBookmarks();
        },

        saveBookmarks: function () {
            if (has("local-storage") && this._componentsVisible(["bookmarks"])) {
                var bookmarks = this.settings.bookmarks.sessionBookmarks.slice();
                localStorage.setItem("bookmarks", JSON.stringify(bookmarks));
            }
        },

        _parseDomNodeMappings: function () {
            var domNodeToComponentMappings = this.domNodeToComponentMappings;

            for (var domNode in domNodeToComponentMappings) {
                var domNodeConstructed = false,
                   mappings = domNodeToComponentMappings[domNode];
                for (var i = 0; i < mappings.length; i++) {
                    domNodeConstructed = domNodeConstructed || this._componentsVisible([mappings[i]]);
                }

                if (!domNodeConstructed) {
                    domConstruct.destroy(this[domNode]);
                }
            }
        },

        _identifyTool: function () {
            this.toolContainer.activateTool('Identify', lang.mixin({ store: this.store }, this.settings.identify.widgetOptions), this.settings.identify.widgetTitle);
        },

        _measureTool: function () {
            this.toolContainer.activateTool('Measure', this.settings.measure.widgetOptions, this.settings.measure.widgetTitle);
        },

        _showMapManager: function () {
            var vs = win.getBox(),
            w = vs.w * .9,
            h = vs.h * .75,
            dialog = Common.showWidgetInDialog(MapManager, lang.mixin({
                map: this.map,
                design: "headline",
                gutters: false,
                style: "width:" + w + "px;height:" + h + "px;"
            }, this.settings.mapManager.widgetOptions), { title: this.settings.mapManager.widgetTitle, "class": "nonModal" }, true);
        },

        _swipeTool: function () {
            this.toolContainer.activateTool('Swipe', lang.mixin({ basemapLayers: this.settings.basemapGallery.widgetOptions.basemaps }, this.settings.swipe.widgetOptions), this.settings.swipe.widgetTitle);
        },

        _showMarkupTool: function () {
            this.toolContainer.activateTool('Markup', lang.mixin({ markupLayer: this.markupLayer }, this.settings.markup.widgetOptions), this.settings.markup.widgetTitle);
        }
    });

    var MapViewerController = new declare([], {

        constructor: function (mapViewer) {
            this.mapViewer = mapViewer;
        },

        getWidget: function (ctr, region, options) {

            if (!ctr || !region)
                throw new Error("A constructor and a region are required");

            var mv = this.mapViewer,
                instance = null;
            for (var key in mv) {
                var prop = mv[key];
                if (prop && typeof prop === "object" && typeof prop.isInstanceOf === "function" && prop.isInstanceOf(ctr)) {
                    instance = prop;
                }
            }
            if (!instance) {
                instance = new ctr(lang.mixin({ map: mv.map, store: mv.store }, options || {}));
                this.sendToRegion(instance, region);
            }
            return instance;
        },

        sendToRegion: function (widgetOrHtml, region) {
            // summary:
            //      send data to widget
            // widgetOrHtml: Object | DomNode | String
            //      the content to send to the specified section
            // sectionName: String
            //      the name of the section where the content should appear

            var cp;
            switch (region) {
                case "bottom":
                    cp = this.mapViewer.bottomPane;
                    break;
                case "header":
                    //cp = this.mapViewer.bottomPane;
                    break;

                case "left":
                    //cp = this.mapViewer.bottomPane;
                    break;
            }

            //var currentContent = cp.get("content");

            //var backButton = new SimpleButton({
            //    label: "",
            //    tooltip: "Return to Content",
            //    leftIconClass: "icon-circle-arrow-left",
            //    style: "font-size:24px;",
            //    onClick: lang.hitch(this, function () { cp.set("content", currentContent); })
            //});

            cp.set("content", widgetOrHtml);
            //backButton.placeAt(cp.domNode, 'first');
        }
    });


    return MapViewer;
});