console.log("wvs/dijits/TocLegendNode.js");

define([
    "dojo/_base/declare",
    "dojo/text!./templates/TocLegendNode.html",
    "dojo/_base/lang",
    "./_TocNode"
], function(declare, template, lang, _TocNode){
    return declare([_TocNode], {

            declaredClass: 'wvs.dijits.TocLegendNode',
    
            templateString: template,
                        
            constructor: function(params, srcNodeRef) {
       
                params = params || {};
                if (!params.mapService) {
                  throw new Error('mapService not defined in params for ' + this.declaredClass);
                }

                if (!params.layerInfo) {
                  throw new Error('layerInfo not defined in params for ' + this.declaredClass);
                }

                if (!params.legend) {
                  throw new Error('legend not defined in params for ' + this.declaredClass);
                }

                lang.mixin(this, params);
        
                // set common properties based on the layerInfo
                this.allowVisibilityChange = false; // for now we are not going to allow the user to check the visibility   
                this.title = this.legend.label;
                this.maxScale = this.layerInfo.maxScale;
                this.minScale = this.layerInfo.minScale;        
                this._state.visible = this._state.enabled = this.layerInfo.defaultVisibility;
            },

            postCreate: function() {
                this.inherited(arguments);
                this._swatchNode = this._createIcon(this.legend, this.iconNode);        

                // call base method to finish setup
                this.inherited(arguments);
            }, 

            _checkboxOnClick: function(e){
                this.setVisible(this._checkbox && this._checkbox.checked);
            },

            _refreshLayer: function() {
                var serviceLayer = this.mapService;
                if (this._refreshTimer) {
                    window.clearTimeout(this._refreshTimer);
                    this._refreshTimer = null;
                }
                this._refreshTimer = window.setTimeout(function() {
                    serviceLayer.setVisibleLayers(serviceLayer.visibleLayers);
                }, 1000);
            },

            _setSwatchWidth: function(width){
                if(width <= 0) return;
                this.iconNode.style.width = width + "px";
            },

            _getSwatchWidth: function(width){
                if(this._swatchNode){
                    return  parseInt(this._swatchNode.style.width);
                }
                return 0;
            }
        });
});