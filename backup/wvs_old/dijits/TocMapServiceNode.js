console.log("wvs/dijits/TocMapServiceNode.js");

define([
    "dojo/_base/declare"
    , "dojo/text!./templates/_TocNode.html"
    , "dojo/_base/lang"
    , "./_TocNode"
    , "dijit/form/HorizontalSlider"
    , "dojo/dom-style"
    , "dojo/ready"
    , "dojo/dom-construct"
    , "dojo/dom-attr"
    , "dojo/on"
    , "dijit/Menu"
    , "dijit/MenuItem"
    , "dijit/MenuSeparator"
    , "dijit/PopupMenuItem"
    , "dijit/TooltipDialog"
    , "dojo/_base/array"
    , "./TocMapServiceLayerNode"
    , "dojo/dnd/Source"
    , "dojo/query"
    , "dojo/dom-class"
    , "dojo/has"
    , "dojo/dom"
    , "dojo/_base/sniff"
], function (declare, template, lang, _TocNode, HorizontalSlider, domStyle, ready, domConstruct, domAttr, on, Menu, MenuItem, MenuSeparator, PopupMenuItem, TooltipDialog, array, TocMapServiceLayerNode, dndSource, query, domClass, has, dom) {
    return declare([_TocNode], {

        templateString: template,

        // refreshDelay: Integer
        //      Number of milliseconds to delay between clicking a checkbox and refreshing the map service. A longer delay allow the user to check/uncheck multiple
        //      layers before refreshing the map service."dojo/ready",
        refreshDelay: 500,

        // useSpriteImage: Boolean
        //      Use the custom sprite image for the legend icons. When false, the "Legend" REST service will be used for 10.01 and newer services.
        useSpriteImage: true,

        declaredClass: "wvs.dijits.TocMapServiceNode",

        hasDropDown: true,

        isGroupNode: true,

        constructor: function (params, srcNodeRef) {
            params = params || {};
            if (!params.mapService) {
                throw new Error('mapService not defined in params for TocMapServiceNode');
            }
            if (!params._parent) {
                throw new Error('no reference to the toc found for TocMapServiceNode');
            }

            lang.mixin(this, params);

            if (this.title === null) {
                // call the extension method in jstoolkit/layers/Layer.js to get the service name
                this.title = this.mapService.getServiceName();
            }

            this.maxScale = this.mapService.maxScale;
            this.minScale = this.mapService.minScale;
        },
        postCreate: function () {

            this.mapService.getLegend(lang.hitch(this, this._createChildNodes), this.useSpriteImage);


            // Bind Transparency
            var transparency = (1 - this.mapService.opacity).toPrecision(1);
            this.set("transparency", transparency);

            // Create Context Menu
            this._createContextMenu();

            // Call super
            this.inherited(arguments);
        },
        /* Start Interface functions */
        setVisible: function (visible) {
            // return if the visibility has not changed
            if (this._state.visible == visible) return;

            this._state.visible = visible;
            
            

            // update the map service
            if (visible) {
                this.mapService.show();
                this._showSwatch();
                this._enableChildNodes();
            } else {
                this.mapService.hide();
                this._hideSwatch();
                this._disableChildNodes();
            }
        },
        _checkboxOnClick: function (e) {
            this.setVisible(this._checkbox && this._checkbox.checked);
        },
        /* End Interface functions */
        _refreshLayer: function () {
            var visibleLayers = this._getVisibleLayers();
            console.log("visible layers", visibleLayers);
            if (visibleLayers.length === 0) {
                this.mapService.hide();
                return;
            }
            else
                this.mapService.show();

            this.mapService.setVisibleLayers(visibleLayers, true);

            if (this._refreshTimer) {
                window.clearTimeout(this._refreshTimer);
                this._refreshTimer = null;
            }

            var mapService = this.mapService;

            this._refreshTimer = window.setTimeout(function () {
                mapService.refresh();
            }, this.refreshDelay);
        },

        _getVisibleLayers: function () {
            return array.map(
                array.filter(this.mapService.layerInfos, function (layerInfo) {
                    return layerInfo.subLayerIds === null && layerInfo.visible;
                }), function (filteredLayerInfos) {
                    return filteredLayerInfos.id;
                });

            // DEPRECATED CODE
            // EIS - 8-11-2014
            //var vis = [],
            //    subLayerInfos = this.mapService.layerInfos();

            //subLayerInfos = array.filter(subLayerInfos, function (subLayerInfo) {
            //    // only return non-group layers that are visible and whose parents are also visible
            //    return subLayerInfo.getParentLayerInfo().visible && subLayerInfo.subLayerIds === null && subLayerInfo.visible;
            //});
            //array.forEach(subLayerInfos, function (subLayerInfo) {
            //    vis.push(subLayerInfo.id);
            //});

            //return vis;
        },
        _createChildNodes: function () {
            var self = this,
                subLayers = this.mapService.getSubLayerInfos();
            array.forEach(subLayers, function (layerInfo) {
                var node = new TocMapServiceLayerNode({
                    _parent: self,
                    layerInfo: layerInfo,
                    mapService: self.mapService,
                    refreshDelay: self.refreshDelay,
                    useSpriteImage: self.useSpriteImage,
                    isFromWebMap: self.isFromWebMap
                });
                node.placeAt(self.childrenNode);
                node.startup();
                self.nodes.push(node);
            });

            this._updateVerticalAlignment();
        },
        _createContextMenu: function () {
            var self = this;

            var mapServiceOptionsMenu = new Menu({
                leftClickToOpen: true,
                onFocus: this.isFromWebMap ? function() {} : function () {
                    var children = this.getChildren(),
                        nodeBottom = self._parent.isBottomNode(self),
                        nodeTop = self._parent.isTopNode(self);

                    array.forEach(children, function (entry, i) {
                        if (entry.label) {
                            switch (entry.label) {
                                case "Move Up":
                                    entry.set("disabled", nodeTop); break;
                                case "Move Down":
                                    entry.set("disabled", nodeBottom); break;
                            }
                        }
                    });
                }
            });

            // Transparency
            mapServiceOptionsMenu.addChild(_TocNode.ContextMenuItems.Transparency(this));

            if(!this.isFromWebMap)
                mapServiceOptionsMenu.addChild(new MenuSeparator());


            if (!this.isFromWebMap) {
                mapServiceOptionsMenu.addChild(_TocNode.ContextMenuItems.Remove(this));

                var _moveUpMenuItem = new MenuItem({
                    label: "Move Up",
                    iconClass: "jstoolkit-icon jstoolkit-icon-arrow-up",
                    onClick: lang.hitch(self._parent, self._parent._moveNodeUp, self)
                });

                mapServiceOptionsMenu.addChild(_moveUpMenuItem);

                var _moveDownMenuItem = new MenuItem({
                    label: "Move Down",
                    iconClass: "jstoolkit-icon jstoolkit-icon-arrow-down",
                    onClick: lang.hitch(self._parent, self._parent._moveNodeDown, self)
                });
                mapServiceOptionsMenu.addChild(_moveDownMenuItem);
            }
            mapServiceOptionsMenu.bindDomNode(this.dropDownMenuIcon);
            mapServiceOptionsMenu.startup();
            this.mapServiceOptionsMenu = mapServiceOptionsMenu;
        }

    });
});