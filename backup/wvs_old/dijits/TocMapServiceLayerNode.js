console.log("wvs/dijits/TocMapServiceLayerNode.js");

 //  This dijit is used to create one of the following node types in the Toc
//      Group Node: A group node is like a folder. It does not contain an actual data layer that can be displayed on the map. Group 
//                  nodes may or may not have child nodes.
//      Layer Node: A layer node is a layer that can be displayed on the map.    
define([
    "dojo/_base/declare"
    , "dojo/_base/lang"
    , "./_TocNode"
    , "dojo/text!./templates/_TocNode.html"
    , "./TocLegendNode"
    , "dojo/dom-class"
    , "dojo/dom-construct"
    , "dojo/_base/array"
    , "dijit/Menu"
    , "dijit/MenuItem"
    , "./Filter/FilterManager"
    , "dojo/aspect"
], function (declare, lang, _TocNode, template, TocLegendNode, domClass, domConstruct, array, Menu, MenuItem, FilterManager, aspect) {
    var TocMapServiceLayerNode = declare([_TocNode], {
            
            templateString: template,
            
            declaredClass: "wvs.dijits.TocMapServiceLayerNode",

            // refreshDelay: Integer
            //      Number of milliseconds to delay between clicking a checkbox and refreshing the map service. A longer delay allow the user to check/uncheck multiple
            //      layers before refreshing the map service.
            refreshDelay: 500,

            // useSpriteImage: Boolean
            //      Use the custom sprite image for the legend icons. When false, the "Legend" REST service will be used for 10.01 and newer services.
            useSpriteImage: true,
                
            constructor: function(params, srcNodeRef) {
                params = params || {};
                if (!params.mapService) {
                  throw new Error('mapService not defined in params for ' + this.declaredClass);
                }

                if (!params.layerInfo) {
                  throw new Error('layerInfo not defined in params for ' + this.declaredClass);
                }
        
                lang.mixin(this, params);

                if (this.layerInfo.visible === undefined) {
                    this.layerInfo.visible = this.layerInfo.defaultVisibility;
                }

                // set common properties based on the layerInfo        
                this.title = this.layerInfo.name;
                this.maxScale = this.layerInfo.maxScale;
                this.minScale = this.layerInfo.minScale;        
                this._state.enabled = !this._parent._state.enabled || !this._parent._state.visible ? false : true;
                this._state.visible = this.layerInfo.visible;
                this._subLayers = this.layerInfo.getSubLayerInfos();
                this._legend = this.mapService.getLegendInfo(this.layerInfo.id);
                this.hasDropDown = !this.isFromWebMap && this.mapService.version >= 10 && this._subLayers.length === 0;

            },
            postCreate: function () {
                // Create child nodes
                if(this._subLayers.length > 0){
                    this.isGroupNode = true;
                    this._createChildNodes();
                }
                else if (this._legend.length > 1) {
                    this.isGroupNode = true;
                    this._createChildLegend();
                }
                else if (this._legend.length == 1) {
                    this._swatchNode = this._createIcon(this._legend[0], this.iconNode);
                    domClass.remove(this.iconNode, "jstoolkit-hidden");
                    this.childrenNode = null;
                    domConstruct.destroy(this.childrenNode);
                }

                if (!this._state.enabled) {
                    this.disable();
                }
                this._expandCollapseBasedOnCheckStatus(true);

                if (this.hasDropDown) {
                    aspect.after(this.layerInfo, "setFilters", function () {
                        if (self.layerInfo._filters && !self.filteredIcon) {
                            var filteredIcon = domConstruct.create("span", { innerHTML: '<i class="icon-filter"></i>' });
                            domConstruct.place(filteredIcon, self.dropDownMenuIcon, "before");
                            self.filteredIcon = filteredIcon;
                        }
                        else if (!self.layerInfo._filters) {
                            domConstruct.destroy(self.filteredIcon);
                        }
                    });
                    this._createContextMenu();
                }
                this.inherited(arguments);
            },
            /* Start Overrides */
            disable: function() {
                // summary:
                //      call base method which will set the visual state of the node
                this.inherited(arguments);
                this.layerInfo.visible = this._state.enabled && this._state.visible;
            },

            enable: function() {
                // summary:
                //      call base method which will set the visual state of the node
                this.inherited(arguments);
                this.layerInfo.visible = this._state.enabled && this._state.visible;
            },
            /* End Overrides */

            _refreshLayer: function () {
                // summary:
                //      call refresh on the parent until it propagates up to the map service node
                this._parent._refreshLayer();
            },
            /* Start Interface functions */
            setVisible: function (visible) {
                console.log("set Visible = " + visible);
                // return if the visibility has not changed
                if (this._state.visible == visible) return;

                this._state.visible = this.layerInfo.visible = visible;

                if (this._state.visible) {
                    this._showSwatch();
                    this._enableChildNodes();
                } else {
                    this._hideSwatch();
                    this._disableChildNodes();
                }
                this._toggleCheckbox();
                this._refreshLayer();
            },
    
            _checkboxOnClick: function (e) {
                this.setVisible(this._checkbox && this._checkbox.checked);
            },
            /* End Interface functions */
            _createChildNodes: function () {
                var subLayers = this._subLayers;
                array.forEach(subLayers, function(layerInfo){
                    var node = new TocMapServiceLayerNode({
                            _parent: this,
                            layerInfo: layerInfo,
                            mapService: this.mapService,
                            isFromWebMap: this.isFromWebMap
                        });
                    node.placeAt(this.childrenNode);
                    this.nodes.push(node);
                    node.startup();
                }, this);

                this._updateVerticalAlignment();
            },
            _createChildLegend: function() {
                array.forEach(this._legend, function(legend){
                    var node = new TocLegendNode({
                            _parent: this,
                            layerInfo: this.layerInfo,
                            mapService:  this.mapService,
                            legend: legend
                        });
                    node.placeAt(this.childrenNode);
                    this.nodes.push(node);
                }, this);

                this._updateVerticalAlignment();
            },
            _createContextMenu: function () {
                var self = this;

                var mapServiceOptionsMenu = new Menu({
                    leftClickToOpen: true
                });

                mapServiceOptionsMenu.addChild(new MenuItem({
                    label: "Filter",
                    iconClass: "icon-filter",
                    onClick: function () {
                        var fm = new FilterManager({
                            mapService: self.mapService,
                            subLayerInfo: self.layerInfo
                        });
                        fm.startup();
                        fm.dialog.show();
                    }
                }));
                mapServiceOptionsMenu.bindDomNode(this.dropDownMenuIcon);
                mapServiceOptionsMenu.startup();
                this.mapServiceOptionsMenu = mapServiceOptionsMenu;
            },
            _setSwatchWidth: function (width) {
                if(width <= 0 || this._legend.length > 1) return;

                this.iconNode.style.width = width + "px";
            },

            _getSwatchWidth: function (width) {
                if(this._swatchNode){
                    return parseInt(this._swatchNode.style.width);
                }
                return 0;
            }
        });

    return TocMapServiceLayerNode;
});