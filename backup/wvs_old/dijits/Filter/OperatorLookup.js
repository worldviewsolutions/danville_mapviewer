﻿define([
    "dojo/_base/declare",
    "dojo/store/Memory",
    "dojo/_base/array"
], function (
    declare,
    Memory,
    array
) {
    return declare(null, {
        constructor: function(caseSensitive){
            this.caseSensitive = caseSensitive ? caseSensitive : false;
        },
        operators: [
            {
                id: 0,
                name: 'Less than',
                operatorRegex: '(FIELD_NAME < VALUE)',
                hasValue: true
            },
            {
                id: 1,
                name: 'Less than or equal to',
                operatorRegex: '(FIELD_NAME <= VALUE)',
                hasValue: true
            },
            {
                id: 2,
                name: 'Greater than',
                operatorRegex: '(FIELD_NAME > VALUE)',
                hasValue: true
            },
            {
                id: 3,
                name: 'Greater than or equal to',
                operatorRegex: '(FIELD_NAME >= VALUE)',
                hasValue: true
            },
            {
                id: 4,
                name: 'Equals',
                operatorRegex: this.caseSensitive ? "(FIELD_NAME = 'VALUE')" : "(UPPER(FIELD_NAME) = UPPER('VALUE'))",
                hasValue: true
            },
            {
                id: 5,
                name: 'Not equal to',
                operatorRegex: this.caseSensitive ? "(FIELD_NAME <> 'VALUE')" : "(UPPER(FIELD_NAME) <> UPPER('VALUE'))",
                hasValue: true
            },
            {
                id: 6,
                name: 'Contains',
                operatorRegex: this.caseSensitive ? "(FIELD_NAME LIKE '%VALUE%')" : "(UPPER(FIELD_NAME) LIKE UPPER('%VALUE%'))",
                hasValue: true
            },
            {
                id: 7,
                name: 'Begins with',
                operatorRegex: this.caseSensitive ? "(FIELD_NAME LIKE 'VALUE%')" : "(UPPER(FIELD_NAME) LIKE UPPER('VALUE%'))",
                hasValue: true
            },
            {
                id: 8,
                name: 'Ends with',
                operatorRegex: this.caseSensitive ? "(FIELD_NAME LIKE '%VALUE')" : "(UPPER(FIELD_NAME) LIKE UPPER('%VALUE'))",
                hasValue: true
            },
            {
                id: 9,
                name: 'Before',
                operatorRegex: "(FIELD_NAME < 'DATE')",
                hasValue: true
            },
            {
                id: 10,
                name: 'After',
                operatorRegex: "(FIELD_NAME > 'DATE')",
                hasValue: true
            },
            {
                id: 11,
                name: 'Between',
                operatorRegex: "(FIELD_NAME > 'BEGIN_DATE' AND FIELD_NAME < 'END_DATE')",
                hasValue: true
            },
            {
                id: 12,
                name: 'Equals',
                operatorRegex: "(FIELD_NAME = 'DATE')",
                hasValue: true
            },
            {
                id: 13,
                name: 'Is Blank',
                operatorRegex: "(FIELD_NAME IS NULL)",
                hasValue: false
            },
            {
                id: 14,
                name: 'Is Not Blank',
                operatorRegex: "(FIELD_NAME IS NOT NULL)",
                hasValue: false
            }
        ],
        opertaorLookupStore: new Memory({
            data: [
                {
                    type: 'esriFieldTypeDate',
                    operators: [9, 10, 11, 12]
                },
                {
                    type: 'esriFieldTypeDouble',
                    operators: [0, 1, 2, 3, 4, 5]
                },
                {
                    type: 'esriFieldTypeGlobalID',
                    operators: [0, 1, 2, 3, 4, 5]
                },
                {
                    type: 'esriFieldTypeGUID',
                    operators: [0, 1, 2, 3, 4, 5]
                },
                {
                    type: 'esriFieldTypeInteger',
                    operators: [0, 1, 2, 3, 4, 5]
                },
                {
                    type: 'esriFieldTypeOID',
                    operators: [0, 1, 2, 3, 4, 5]
                },
                {
                    type: 'esriFieldTypeSingle',
                    operators: [0, 1, 2, 3, 4, 5]
                },
                {
                    type: 'esriFieldTypeSmallInteger',
                    operators: [0, 1, 2, 3, 4, 5]
                },
                {
                    type: 'esriFieldTypeString',
                    operators: [4, 5, 6, 7, 8, 13, 14]
                },
                {
                    type: 'codedValue',
                    operators: [4, 5]
                }
            ]
        }),

        getOperatorStoreForType: function (type) {
            var _this = this;

            var tmp = [];

            // Get the operator indexes for type
            var operatorIndexes = this.opertaorLookupStore.query({
                type: type
            })[0].operators;

            // Prepare to return them
            array.forEach(operatorIndexes, function (entry, i) {
                tmp.push(_this.operators[entry]);
            });

            return new Memory({ data: tmp });
        }
    });
});