﻿define([
    "dojo/_base/declare",
    "dojo/parser",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dojo/text!../templates/Filter.html",
    "dijit/form/FilteringSelect",
    "dijit/form/ValidationTextBox",
    "dijit/registry",
    "dojo/store/Memory",
    "dojo/_base/lang",
    "dojo/_base/array",
    "./OperatorLookup",
    "dijit/form/DateTextBox",
    "dijit/form/ValidationTextBox",
    "dijit/form/Form",
    "dojo/on",
    "dojo/ready"
], function (
    declare,
    parser,
    _WidgetBase,
    _TemplatedMixin,
    template,
    FilteringSelect,
    TextBox,
    registry,
    Memory,
    lang,
    array,
    OperatorLookup,
    DateTextBox,
    ValidationTextBox,
    Form,
    on,
    ready
) {
    var Filter =  declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,

        constructor: function (args) {
            var instanceDefaults = {
                codedValueStore: null,
                fieldSelect: null,
                codedValueSelect: null,
                operatorSelect: null,
                beginDateSelect: null,
                endDateSelect: null,
                filterTextBox: null,
                activeValueElement: null,
                filterForm: null
            };

            lang.mixin(this, instanceDefaults);
            
            this.caseSensitive = args.caseSensitive ? params.caseSensitive : false;
            this.operatorLookup = new OperatorLookup(this.caseSensitive);


            // Create the stores based on the options passed in
            this.fieldStore = new Memory({
                idProperty: "name",
                data: args.fieldSelectOptions
            });
        },

        postCreate: function () {

            this.fieldSelect = new FilteringSelect({
                store: this.fieldStore,
                onChange: lang.hitch(this, this.fieldSelectOnChange)
            }, this.fieldSelect);

            this.operatorSelect = new FilteringSelect({
                onChange: lang.hitch(this, this.operatorSelectOnChange)
            }, this.operatorSelect);

            this.codedValueSelect = new FilteringSelect({
            }, this.codedValueSelect);

            this.beginDateSelect = new DateTextBox({
                onChange: lang.hitch(this, this.updateEndDateMinDate)
            }, this.beginDateSelect);

            this.endDateSelect = new DateTextBox({
                onChange: lang.hitch(this, this.updateEndDateMinDate)
            }, this.endDateSelect);

            this.filterTextBox = new ValidationTextBox({

            }, this.filterTextBox);

            this.filterForm = new Form({}, this.filterForm);

            // Hide elements until a field is selected
            this.operatorSelect.set('disabled', true);
            this.filterTextBox.set('disabled', true);
            this.codedValueSelect.set('style', 'display: none;');
            this.beginDateSelect.set('style', 'display: none;');
            this.endDateSelect.set('style', 'display: none;');
            this.operatorSelect.set('required', false);
            this.filterTextBox.set('required', false);
            this.codedValueSelect.set('required', false);
            this.beginDateSelect.set('required', false);
            this.endDateSelect.set('required', false);


        },

        fieldSelectOnChange: function () {
            var selectedField = this.fieldSelect.item;

            if (selectedField) {

                this.operatorSelect.set('disabled', false);
                this.filterTextBox.set('disabled', false);
                this.operatorSelect.set('required', true);
                this.filterTextBox.set('required', true);

                if (selectedField.domain != null) {
                    // Populate the coded values that are in this field's domain
                    this.codedValueSelect.set({
                        store: new Memory({ data: selectedField.domain.codedValues })
                    });

                    // Populate the operators that can be applied to coded values
                    this.operatorSelect.set({
                        store: this.operatorLookup.getOperatorStoreForType('codedValue')
                    });


                    // Show/hide the appropriate dijits
                    this.operatorSelect.set('style', 'display: inline-block;');
                    this.codedValueSelect.set('style', 'display: inline-block;');
                    this.operatorSelect.set('required', true);
                    this.codedValueSelect.set('required', true);
                    this.filterTextBox.set('style', 'display: none;');
                    this.beginDateSelect.set('style', 'display: none;');
                    this.endDateSelect.set('style', 'display: none;');
                    this.filterTextBox.set('required', false);
                    this.beginDateSelect.set('required', false);
                    this.endDateSelect.set('required', false);


                    this.activeValueElement = this.codedValueSelect;
                } else {
                    // Populate the operators for this type
                    this.operatorSelect.set({
                        store: this.operatorLookup.getOperatorStoreForType(selectedField.type)
                    });

                    // Show/hide the appropriate dijits
                    if (selectedField.type === 'esriFieldTypeDate') {
                        this.beginDateSelect.set('style', 'display: inline-block;');
                        this.endDateSelect.set('style', 'display: inline-block;');
                        this.beginDateSelect.set('required', true);
                        this.endDateSelect.set('required', true);
                        this.filterTextBox.set('style', 'display: none;');
                        this.codedValueSelect.set('style', 'display: none;');
                        this.filterTextBox.set('required', false);
                        this.codedValueSelect.set('required', false);

                    } else {
                        this.beginDateSelect.set('style', 'display: none;');
                        this.endDateSelect.set('style', 'display: none;');
                        this.codedValueSelect.set('style', 'display: none;');
                        this.beginDateSelect.set('required', false);
                        this.endDateSelect.set('required', false);
                        this.codedValueSelect.set('required', false);

                        this.operatorSelect.set('style', 'display: inline-block;');
                        this.operatorSelect.set('required', true);
                        this.filterTextBox.set('disabled', false);
                        this.filterTextBox.set('style', 'display: inline-block;');
                        this.filterTextBox.set('required', true);
                        this.activeValueElement = this.filterTextBox;
                    }
                }
            }
        },

        // Only used to handle the disabling of fields on the date filter
        //
        operatorSelectOnChange: function () {
            var _this = this;

            if (this.fieldSelect.item.type === 'esriFieldTypeDate') {
                if (this.operatorSelect.item.name === 'Between') {
                    this.beginDateSelect.set('disabled', false);
                    this.endDateSelect.set('disabled', false);
                    this.beginDateSelect.set('required', true);
                    this.endDateSelect.set('required', true);
                } else {
                    this.beginDateSelect.set('disabled', false);
                    this.beginDateSelect.set('required', true);
                    this.endDateSelect.set('disabled', true);
                    this.endDateSelect.set('required', false);
                }
            }

            this.filterTextBox.set("required", this.operatorSelect.item.hasValue);
            this.filterTextBox.set("disabled", !this.operatorSelect.item.hasValue);
        },

        updateEndDateMinDate: function () {
            this.endDateSelect.constraints.min = this.beginDateSelect.value;
        },

        getQuery: function (isWildCarded) {
            var field;
            var operator;
            var value;
            var beginDate;
            var endDate;

            if (this.fieldSelect.item != null) {

                // Get the field
                field = this.fieldSelect.item;

                // Get the operator
                operator = this.operatorSelect.item;

                // Handle the special case for dates
                if (this.fieldSelect.item.type === 'esriFieldTypeDate') {
                    return (this.operatorSelect.item.name === 'Between') ?
                        operator.operatorRegex.replace(/FIELD_NAME/g, field.name).replace('BEGIN_DATE', this.beginDateSelect).replace('END_DATE', this.endDateSelect) :
                        operator.operatorRegex.replace(/FIELD_NAME/g, field.name).replace('DATE', this.beginDateSelect);
                }

                // Get the applied value
                value = this.activeValueElement.value;

                return operator.operatorRegex.replace('FIELD_NAME', field.name).replace("VALUE", isWildCarded ? "%s" : value);
            }

            return null;
        },
        equals: function(filter){
            return filter.fieldSelect.value === this.fieldSelect.value
                && filter.operatorSelect.value === this.operatorSelect.value
                && filter.filterTextBox.value === this.filterTextBox.value;
        },
        toJSON: function() {
            var obj = {
                field: this.fieldSelect.get("value"),
                operator: this.operatorSelect.get("value"),
                filterText: this.filterTextBox.get("value")
            };

            return obj;
        },
        validate: function () {
            if (this.filterForm.validate())
                return true;
            return false;
        }
    });
    
    Filter.fromJSON = function (JSON) {
        
    };

    return Filter;
});