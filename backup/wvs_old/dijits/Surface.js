﻿define([
    // common widget imports
    "dojo/_base/lang"
    , "dojo/_base/declare"
    , "dojo/_base/array"
    , "dijit/_WidgetBase"
    // dojo imports
    , "dojo/_base/Color"
    , "dojo/number"
    , "dojo/dom-construct"
    , "dojo/on"
    , "dojo/_base/connect"
    , "dojo/dom-class"
    // dijit imports
    , "dijit/form/DropDownButton"
    , "dijit/DropDownMenu"
    , "dijit/MenuItem"
    , "dijit/form/Button"
    // esri imports
    , "esri/map"
    , "esri/toolbars/draw"
    , "esri/tasks/ProjectParameters"
    , "esri/SpatialReference"
    , "esri/config"
    , "esri/symbols/SimpleMarkerSymbol"
    , "esri/symbols/SimpleLineSymbol"
    , "esri/symbols/TextSymbol"
    , "esri/graphic"
    , "esri/symbols/Font"
    , "esri/tasks/GeometryService"
    , "esri/tasks/BufferParameters"
    , "esri/geometry/Polyline"
    // wvs imports
    , "wvs/libs/SurfaceSOE"
    , "wvs/common/Common"
    // extensions
    , "dojo/colors"
], function (lang
    , declare
    , array
    , _WidgetBase
    // dojo imports
    , Color
    , number
    , domConstruct
    , on
    , connect
    , domClass
    // dijit imports
    , DropDownButton
    , DropDownMenu
    , MenuItem
    , Button
    // esri imports
    , Map
    , Draw
    , ProjectParameters
    , SpatialReference
    , esriConfig
    , SimpleMarkerSymbol
    , SimpleLineSymbol
    , TextSymbol
    , Graphic
    , Font
    , GeometryService
    , BufferParameters
    , Polyline
    // wvs imports
    , SurfaceSOE
    , Common
    // extensions
) {

    var Surface = declare([_WidgetBase], {

        // Prototype members     

        declaredClass: "wvs.dijits.Surface",

        offsetObserverLOS: 10,

        offsetTargetLOS: 10,

        horizonDistances: [7.0],

        horizonUnit: null,

        horizonGeodesic: false,

        stepSizeSurfaceLength: 20,

        currentTool: null,

        _toolsMap: {
            // Surface.SURFACE_PROFILE
            "0": {
                label: "Profile",
                iconClass: "",
                description: "Get an elevation profile for a polyline",
                mapCursor: "crosshair",
                drawAction: Draw.POLYLINE,
                drawEnd: "getProfileAlongGeom"
            },
            // Surface.SURFACE_POINT
            "1": {
                label: "Point",
                iconClass: "",
                description: "Get elevation (meters) at a given point",
                mapCursor: "crosshair",
                drawAction: Draw.POINT,
                drawEnd: "getElevationAlongGeom"
            },
            // Surface.SURFACE_MULTIPOINT
            "2": {
                label: "Multipoint",
                iconClass: "",
                description: "Get elevation (meters) at multiple points",
                mapCursor: "crosshair",
                drawAction: Draw.MULTI_POINT,
                drawEnd: "getElevationAlongGeom"
            },
            // Surface.SURFACE_POLYLINE
            "3": {
                label: "Polyline",
                iconClass: "",
                description: "Get elevation points (meters) for a polyline",
                mapCursor: "crosshair",
                drawAction: Draw.POLYLINE,
                drawEnd: "getElevationAlongGeom"
            },
            // Surface.SURFACE_POLYGON
            "4": {
                label: "Polygon",
                iconClass: "",
                description: "Get elevation points (meters) inside of a polygon",
                mapCursor: "crosshair",
                drawAction: Draw.POLYGON,
                drawEnd: "getElevationAlongGeom"
            },
            // Surface.SURFACE_HORIZON
            "5": {
                label: "Horizon",
                iconClass: "",
                description: "",
                mapCursor: "crosshair",
                drawAction: Draw.POINT,
                drawEnd: "getHorizon"
            },
            // Surface.SURFACE_LINEOFSIGHT
            "6": {
                label: "Line of Sight",
                iconClass: "",
                description: "A line drawn between two points, an origin and a target, that is compared against a surface to show whether the target is visible from the origin and, if it is not visible, where the view is obstructed",
                mapCursor: "crosshair",
                drawAction: Draw.LINE,
                drawEnd: "getLineOfSight"
            },
            // Surface.SURFACE_STEEPESTPATH
            "7": {
                label: "Steepest Path",
                iconClass: "",
                description: "A line that follows the steepest downhill direction on a surface. Paths terminate at the surface perimeter or in surface concavities or pits.",
                mapCursor: "crosshair",
                drawAction: Draw.POINT,
                drawEnd: "getSteepestPath"
            },
            // Surface.SURFACE_CONTOUR
            "8": {
                label: "Contour",
                iconClass: "",
                description: "A line on a map that connects points of equal elevation based on a vertical datum",
                mapCursor: "crosshair",
                drawAction: Draw.POINT,
                drawEnd: "getContour"
            },
            // Surface.SURFACE_SLOPE
            "9": {
                label: "Slope",
                iconClass: "",
                description: "The maximum rate of change between each cell and its neighbors, for example, the steepest downhill descent for the cell (the maximum change in elevation over the distance between the cell and its eight neighbors).",
                mapCursor: "crosshair",
                drawAction: Draw.POINT,
                drawEnd: "getSlope"
            },
            // Surface.SURFACE_ASPECT
            "10": {
                label: "Aspect",
                iconClass: "",
                description: "Aspect identifies the steepest downslope direction from each cell to its neighbors. It can be thought of as slope direction or the compass direction a hill faces.",
                mapCursor: "crosshair",
                drawAction: Draw.POINT,
                drawEnd: "getAspect"
            },
            // Surface.SURFACE_SURFACELENGTH
            "11": {
                label: "Surface Length",
                iconClass: "",
                description: "",
                mapCursor: "crosshair",
                drawAction: Draw.POLYLINE,
                drawEnd: "getSurfaceLength"
            },
            // Surface.SURFACE_NORMAL
            "12": {
                label: "Normal",
                iconClass: "",
                description: "",
                mapCursor: "crosshair",
                drawAction: Draw.POINT,
                drawEnd: "getNormal"
            }
            // TODO: Should we implement?
            // Surface.SURFACE_LOCATE
            //"13": {
            //    label: "Locate",
            //    iconClass: "",
            //    mapCursor: "crosshair",
            //    drawAction: Draw.POINT,
            //    drawEnd: "getNormal"
            //},
            //// Surface.SURFACE_LOCATEALL
            //"14": {
            //    label: "Locate All",
            //    iconClass: "",
            //    mapCursor: "crosshair",
            //    drawAction: Draw.POLYLINE,
            //    drawEnd: this.latlon
            //},
            //// Surface.SURFACE_GETDATA
            //"15": {
            //    label: "Get Data",
            //    iconClass: "",
            //    mapCursor: "crosshair",
            //    drawAction: Draw.POLYLINE,
            //    drawEnd: this.latlon
            //}
        },

        // Overrides
        constructor: function (params, srcNodeRef) {
            // Set object-based properties
            this.map = null;
            this._graphicsLayer = null;
            this.surfaceSOEUtils = null;
            this.enabledTools = [];
            this.horizonUnit = GeometryService.UNIT_KILOMETER;


            // Object state properties
            this.currentTool = null;;
            this._postDrawEnd = null;//this._toolsMap[this.currentTool].drawEnd;
            this._mapCursor = null;//this._toolsMap[this.currentTool].mapCursor;
            this._drawAction = null;//this._toolsMap[this.currentTool].drawAction;

            // Mixin params
            lang.mixin(this, params || {});
            
            this.geometryService = this.geometryService ? this.geometryService : esriConfig.defaults.geometryService ? esriConfig.defaults.geometryService : null;

            if (!this.geometryService)
                throw new Error("no geometry service for " + this.declaredClass);
            if (!this.map)
                throw new Error("no map for " + this.declaredClass);
            if (!this.soeInfo)
                throw new Error("server object extension info required for " + this.declaredClass);
            if (!this.profileChartParams)
                throw new Error("profile chart params required for " + this.declaredClass);

            this._drawToolbar = new Draw(this.map);
            this._drawEnd = on(this._drawToolbar, "draw-end", lang.hitch(this, this._onDrawEnd));
            this.own(this._drawEnd);

            
            this.surfaceSOEUtils = new SurfaceSOE();
            this.surfaceSOEUtils.init(this.soeInfo, lang.mixin({ map: this.map }, this.profileChartParams));

            // Use passed in graphics layer or map's graphics layer
            this._graphicsLayer = this.graphicsLayer || this.map.graphics;
        },
        postCreate: function () {

            domClass.add(this.domNode, "jstoolkit-dijits-surface");

            // Add container node for content
            var containerNode = domConstruct.create("div", { "class": "content" }, this.domNode);

            this.descNode;
            

            // Acquire the map
            connect.publish("map-acquire", { target: this, acquired: true });

            // We'll have a drop-down menu if we have multiple tools
            if (this.enabledTools.length > 1 || this.enabledTools.length === 0) {

                // Create label
                domConstruct.create("label", { innerHTML: "Surface Tool: " }, containerNode);



                var _tools = [];
                if (this.enabledTools.length === 0) {
                    for (var key in this._toolsMap) {
                        this.enabledTools.push(key);
                    }
                }

                var defaultTool = this._toolsMap[this.enabledTools[0]];

                var menu = new DropDownMenu({ style: "display: none;" });

                array.forEach(this.enabledTools, function (enabledTool) {
                    var tool = this._toolsMap[enabledTool],
                        menuItem = new MenuItem({
                            label: tool.label,
                            iconClass: tool.iconClass,
                            onClick: lang.hitch(this, function (t, evt) {
                                this.currentTool = t;
                                dropdownButton.set("label", tool.label);
                                descNode.innerHTML = tool.description;
                            }, enabledTool)
                        });
                    menu.addChild(menuItem);
                }, this);

                var dropdownButton = new DropDownButton({
                    label: defaultTool.label,
                    dropDown: menu
                }).placeAt(containerNode);

            }

            descNode = domConstruct.create("div", { innerHTML: this._toolsMap[this.enabledTools[0]].description, "class": "description" }, containerNode);

            // create our results panel
            this.resultsContainer = domConstruct.create("h1", { "class": "results-title", innerHTML: "Results" }, containerNode);
            this.resultsPanel = domConstruct.create("div", { innerHTML: "No Results", "class": "results" }, containerNode);


            var actionBar = domConstruct.create("div", { "class": "dijitDialogPaneActionBar" }, this.domNode);

            this.executeButton = new Button({
                label: this.enabledTools.length === 1 ? "Execute " + this._toolsMap[this.enabledTools[0]].label : "Execute",
                onClick: lang.hitch(this, function(){
                    if (this.currentTool != null)
                        this.enableTool(this.currentTool);
                })
            }).placeAt(actionBar);


            this.currentTool = this.enabledTools[0];
        },
        destroy: function(){
            this.clear();
            // Release the map
            connect.publish("map-acquire", { target: this, acquired: false });
            this._drawToolbar.deactivate();
            this.inherited(arguments);
        },

        // Public functions
        clear: function(){
            this.displayMessage("");
            this.map.setMapCursor("default");
            this._graphicsLayer.clear();
        },
        enableTool: function (tool) {
            if (this._toolsMap[tool]) {
                this.clear();
                var _curTool = this._toolsMap[tool];
                this._postDrawEnd = this[_curTool.drawEnd];
                this._mapCursor = _curTool.mapCursor;
                this._drawAction = _curTool.drawAction;
                this._startDrawing();
            }
            else
                throw new Error("invalid tool for ");
        },
        getLineOfSight: function(geometry) {
            this._setLoading(true);

            this.surfaceSOEUtils.getLineOfSight(geometry, this.offsetObserverLOS, this.offsetTargetLOS, false, false, null).then(
                lang.hitch(this, function (lineOfSight) {

                    if (lineOfSight.pointObstruction != null) {
                        var markerSym = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 17, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 0, 0]), 1), new Color([0, 255, 0, 0.25]));
                        var userGraphic = new Graphic(lineOfSight.pointObstruction, markerSym);
                        this._graphicsLayer.add(userGraphic);
                    }

                    if (lineOfSight.visibleLines != null) {
                        var polylineSymbol = new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0, 255, 0]), 5)
                        userGraphic = new Graphic(lineOfSight.visibleLines, polylineSymbol);
                        this._graphicsLayer.add(userGraphic);
                    }

                    if (lineOfSight.invisibleLines != null) {
                        polylineSymbol = new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 0, 0]), 5)
                        userGraphic = new Graphic(lineOfSight.invisibleLines, polylineSymbol);
                        this._graphicsLayer.add(userGraphic);
                    }

                    this.map.setMapCursor('default');
                    this._setLoading(false);
                })
            , lang.hitch(this, this._onError));
        },
        getContour: function(geometry) {

            this._setLoading(true);
            this.surfaceSOEUtils.getContour(geometry).then(
                lang.hitch(this, function (contour) {
                if (contour.geometry != null) {
                    var polylineSymbol = new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0, 255, 0]), 4)
                    userGraphic = new Graphic(contour.geometry, polylineSymbol);
                    this._graphicsLayer.add(userGraphic);
                    var textSymbol = new TextSymbol(number.format(contour.elevation, {places:2})).setColor(new Color(Color.named.red)).setAlign(TextSymbol.ALIGN_MIDDLE).setOffset(0, -5).setFont(new Font("10pt").setWeight(Font.WEIGHT_BOLD));
                    var userTxtGraphic = new Graphic(geometry, textSymbol);
                    this._graphicsLayer.add(userTxtGraphic);
                }

                map.setMapCursor('default');
                this._setLoading(false);

                }), lang.hitch(this, this._onError));
        },
        getSlope: function (geometry) {
            this._setLoading(true);
            this.surfaceSOEUtils.getSlope(geometry, this.surfaceSOEUtils.unitsSlope.PERCENT).then(
            lang.hitch(this, function (slope) {
                if (slope.slope != null) {
                    var pointSymbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 15, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 0, 0]), 1), new Color([0, 255, 0, 0.25]));
                    var userGraphic = new Graphic(geometry, pointSymbol);
                    this._graphicsLayer.add(userGraphic);
                    var textSymbol = new TextSymbol(number.format(slope.slope, {places:2}) + ' %').setColor(new Color(Color.named.red)).setAlign(TextSymbol.ALIGN_MIDDLE).setOffset(-20, -20).setFont(new Font("10pt").setWeight(Font.WEIGHT_BOLD));
                    var userTxtGraphic = new Graphic(geometry, textSymbol);
                    this._graphicsLayer.add(userTxtGraphic);
                }

                this.map.setMapCursor('default');
                this._setLoading(false);

            }), lang.hitch(this, this._onError));
        },
        getAspect: function(geometry) {
            this._setLoading(true);
            this.surfaceSOEUtils.getAspect(geometry, this.surfaceSOEUtils.unitsAspect.DEGREES).then(lang.hitch(this, function (aspect) {
                if (aspect.aspect != null) {
                    var pointSymbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 15, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 0, 0]), 1), new Color([0, 255, 0, 0.25]));
                    var userGraphic = new Graphic(geometry, pointSymbol);
                    this._graphicsLayer.add(userGraphic);
                    var textSymbol = new TextSymbol(number.format(aspect.aspect, {places:0}) + '°').setColor(new Color(Color.named.red)).setAlign(TextSymbol.ALIGN_MIDDLE).setOffset(-20, -20).setFont(new Font("10pt").setWeight(Font.WEIGHT_BOLD));
                    var userTxtGraphic = new Graphic(geometry, textSymbol);
                    this._graphicsLayer.add(userTxtGraphic);
                }

                this.map.setMapCursor('default');
                this._setLoading(false);

            }), lang.hitch(this, this._onError));
        },
        getSteepestPath: function (geometry) {
            this._setLoading(true);
            this.surfaceSOEUtils.getSteepestPath(geometry).then(
                lang.hitch(this, function (steepestPath) {
                    if (steepestPath.geometry != null) {
                        var polylineSymbol = new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0, 255, 0]), 4)
                        userGraphic = new Graphic(steepestPath.geometry, polylineSymbol);
                        this._graphicsLayer.add(userGraphic);
                    }
                    else {
                        this.displayMessage("No path found");
                    }

                    this.map.setMapCursor('default');
                    this._setLoading(false);

                }), lang.hitch(this, this._onError));
        },
        getSurfaceLength: function(geometry) {

            this._setLoading(true);

            this.surfaceSOEUtils.getSurfaceLength(geometry, this.stepSizeSurfaceLength).then(lang.hitch(this, function (surfaceLength) {
                if (surfaceLength.surfaceLength != null) {
                    this.displayMessage(number.format(surfaceLength.surfaceLength, {places:2}));
                }

                this.map.setMapCursor('default');
                this._setLoading(false);

            }), lang.hitch(this, this._onError));
        },
        getElevation: function(point) {
            var self = this;
            var params = new ProjectParameters();
            params.geometries = [point];
            params.outSR = new SpatialReference({ wkid: 4326 });

            this._setLoading(true);
            this.geometryService.project(params).then(
                function (geoPoint) {
                    self.surfaceSOEUtils.getElevationAtLonLat(geoPoint[0]).then(function (response) {

                        var elevation = number.format(response.elevation, {
                            'places':2
                        });
                        self._addElevationPointGraphic(point, elevation);

                        //displayMessage(lang.replace("Elevation = {0} meters", [elevation]));
                        self.map.setMapCursor('default');
                        self._setLoading(false);
                    }, self._onError);
                }
                , lang.hitch(self, self._onError)
            );
        },
        getElevationAlongGeom: function (geometry) {
            this._setLoading(true);
            this.surfaceSOEUtils.getElevations(geometry).then(
                
                lang.hitch(this, function (elevationInfo) {

                var elevationData = elevationInfo.data;

                var elevationPoints = this.surfaceSOEUtils.getPointsFromGeometry(elevationInfo.geometry);
                array.forEach(elevationPoints, function (elevPoint) {
                    this._addElevationPointGraphic(elevPoint);
                }, this);

                this.map.setMapCursor('default');
                this._setLoading(false);

            }), lang.hitch(this, this._onError));
        },
        getNormal: function(geometry) {
            this._setLoading(true);
            this.surfaceSOEUtils.getNormal(geometry).then(lang.hitch(this, function (normal) {
                if ((normal.geometry != null) && (normal.vector3D != null)) {
                    this.displayMessage('Origin x: ' + number.format(normal.geometry.x, {places:2}) + ' y: ' + number.format(normal.geometry.y, {places:2}) + '<br/>Components versor x: ' + number.format(normal.vector3D[0], {places:5}) + ' y: ' + number.format(normal.vector3D[1], {places:5}) + ' z: ' + number.format(normal.vector3D[2], {places:5}));
                }

                this.map.setMapCursor('default');
                this._setLoading(false);

            }), lang.hitch(this, this._onError));
        },
        getHorizon: function(point) {
            var bufferParams = new BufferParameters();
            bufferParams.geometries = [point];  // [esri.geometry.webMercatorToGeographic(point)];
            bufferParams.unionResults = false;
            bufferParams.distances = this.horizonDistances;
            bufferParams.unit = this.horizonUnit;
            bufferParams.geodesic = this.horizonGeodesic; //  true;
            bufferParams.outSpatialReference = this.map.spatialReference;

            this._setLoading(true);

            this.geometryService.buffer(bufferParams).then(
                lang.hitch(this, function (bufferGeoms) {
                    var buffer = bufferGeoms[0];
                    var bufferLine = new Polyline(this.map.spatialReference);
                    bufferLine.addPath(buffer.rings[0]);
                    this._graphicsLayer.add(new Graphic(point, this._drawToolbar.markerSymbol));
                    this._graphicsLayer.add(new Graphic(bufferLine, this._drawToolbar.lineSymbol));
                    this.getProfileAlongGeom(bufferLine);
                }), lang.hitch(this, this._onError));
        },
        // Start Private functions
        displayMessage: function(msg){
            this.resultsPanel.innerHTML = msg || "No Results";
        },
        _startDrawing: function () {
            this.map.setMapCursor(this._mapCursor);
            this._drawToolbar.activate(this._drawAction);
        },
        _setLoading: function(loading){
            this.executeButton.set("disabled", loading ? true : false);
            this.executeButton.set("iconClass", loading ? "icon-spinner icon-spin" : "");
        },
        _addElevationPointGraphic: function(point, elev, clr) {
            var elevValue = (elev || point.z);
            var elevStr = number.format(elevValue, {places:2});
            var color = clr || ((elevValue < 0) ? Color.named.blue : Color.named.brown);

            var markerOutline = new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0, 0, 0, 0]), 1.0);
            var markerSize = 25.0;

            var markerSym = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, markerSize, markerOutline, new Color(color));
            var userGraphic = new Graphic(point, markerSym);
            this._graphicsLayer.add(userGraphic);

            var textSymbol = new TextSymbol(elevStr).setColor(new Color(Color.named.green)).setAlign(TextSymbol.ALIGN_MIDDLE).setOffset(0, -5).setFont(new Font("10pt").setWeight(Font.WEIGHT_BOLD));
            var userTxtGraphic = new esri.Graphic(point, textSymbol);
            this._graphicsLayer.add(userTxtGraphic);
        },
        getProfileAlongGeom: function (geometry) {

            this._setLoading(true);

            this.resultsPanel.innerHTML = "";

            var chartNode = domConstruct.create("div", {
                style:"width:100%;height:250px;"
            }, this.resultsPanel, 'first');

            this.surfaceSOEUtils.createProfileChart(geometry, chartNode).then(
                lang.hitch(this, function () {
                    //displayMessage("Elevation Profile done...");
                    this.map.setMapCursor('default');
                    this._setLoading(false);
                })
                , this._onError
            );
        },
        // Event Handlers
        _onError: function(error){
            // alert error message
            Common.errorDialog(error.message);

            // Reset our map cursor just in case
            this.map.setMapCursor('default');
            this._setLoading(false);
        },
        _onDrawEnd: function (evt) {
            var geometry = evt.geometry;
            this._drawToolbar.deactivate();
            this.map.setMapCursor('wait');

            // Add our geometry

            switch (geometry.type) {
                case 'multipoint':
                    this._graphicsLayer.add(new Graphic(geometry, this._drawToolbar.markerSymbol));
                    break;
                case 'extent':
                    this._graphicsLayer.add(new Graphic(geometry, this._drawToolbar.fillSymbol));
                    break;
                case 'polyline':
                    this._graphicsLayer.add(new Graphic(geometry, this._drawToolbar.lineSymbol));
                    break;
                case 'polygon':
                    this._graphicsLayer.add(new Graphic(geometry, this._drawToolbar.fillSymbol));
                    break;
            }


            this._postDrawEnd(geometry);

        }

    });

    Surface.SURFACE_PROFILE = 0;
    Surface.SURFACE_POINT = 1;
    Surface.SURFACE_MULTIPOINT = 2;
    Surface.SURFACE_POLYLINE = 3;
    Surface.SURFACE_POLYGON = 4;
    Surface.SURFACE_HORIZON = 5;
    Surface.SURFACE_LINEOFSIGHT = 6;
    Surface.SURFACE_STEEPESTPATH = 7;
    Surface.SURFACE_CONTOUR = 8;
    Surface.SURFACE_SLOPE = 9;
    Surface.SURFACE_ASPECT = 10;
    Surface.SURFACE_SURFACELENGTH = 11;
    Surface.SURFACE_NORMAL = 12;
    // Should we implement?
    //Surface.SURFACE_LOCATE = 13;
    //Surface.SURFACE_LOCATEALL = 14;
    //Surface.SURFACE_GETDATA = 15;

    return Surface;
});