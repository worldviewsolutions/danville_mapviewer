﻿//ViewIn Module

// EIS
// 1-15-2015

// This module is not an actual widget.
// It creates a MenuItem for itself to attach to another menu, whether that be a MenuBar or a DropDownMenu
// It will automatically bind itself to parentMenu if that is passed in. Otherwise, pass in a menu to the attachToMenu function to create the menu

// Example - Custom "View In" items
// Pass in viewInMenuItems as an array of menu entries. Each object represent a drop-down menu, with its label being the label used on the menu.
// The array of links within that drop-down menu represent menu items that will be 'View In Links'
// As of this writing, the url accepts 4 substitution parameters that will be calculated/replaced by the current value in the map
// ${longitude} - longitude of the center point of the map
// ${latitude} - latitude of the center point of the map
// ${level} - the current level of the map
// ${scale} - the current scale of the map

//viewInMenuItems: [
//    {
//        label: "Pictometry",
//        links: [
//            { label: "Face North", url: "http://myUrl/?lat=${latitude}&lon=${longitude}&level=${level}&scale=${scale}" }
//        ]
//    }
//]

define(["dojo/_base/lang"
	, "dojo/_base/declare"
	, "dijit/DropDownMenu"
	, "dijit/MenuItem"
	, "dijit/PopupMenuItem"
	, "dijit/MenuBar"
	, "dijit/PopupMenuBarItem"
	, "dojo/string"],
	function (lang
		, declare
		, DropDownMenu
		, MenuItem
		, PopupMenuItem
		, MenuBar
		, PopupMenuBarItem
		, string) {

	    var ViewIn = declare([], {

	        // _defaults: Object[]
	        //      default entries for the View In menu
	        _defaults: [
                {
                    label: "Google Maps",
                    iconClass: "icon-google-plus-sign",
                    links: [
                        { label: "Roads", url: "https://maps.google.com/?z=${level}&ll=${latitude},${longitude}&t=m" },
                        { label: "Aerials", url: "https://maps.google.com/?z=${level}&ll=${latitude},${longitude}&t=k" },
                        { label: "Hybrid", url: "https://maps.google.com/?z=${level}&ll=${latitude},${longitude}&t=h" },
                        { label: "Earth", url: "https://maps.google.com/?z=${level}&ll=${latitude},${longitude}&t=f" }]
                },
                {
                    label: "Bings / Virtual Earth",
                    iconClass: "icon-windows",
                    links: [
                        { label: "Roads", url: "https://www.bing.com/maps/?lvl=${level}&cp=${latitude}~${longitude}&style=r" },
                        { label: "Aerials", url: "https://www.bing.com/maps/?lvl=${level}&cp=${latitude}~${longitude}&style=a" },
                        { label: "Hybrid", url: "https://www.bing.com/maps/?lvl=${level}&cp=${latitude}~${longitude}&style=h" },
                        { label: "Bird's Eye", url: "https://www.bing.com/maps/?v=2&cp=${latitude}~${longitude}&lvl=${level}&dir=3.025637994274562&sty=u&style=o" }
                    ]
                }
	        ],

	        //  title: String
	        //      the title of the View In menu. Defaults to "View In"
	        title: "View In",

	        //  iconClass: String
	        //      the class to use for the View In MenuItem
	        iconClass: "icon-eye-open",

	        //  includeDefaults: Boolean
	        //      determines whether or not the default entries (Google and Bing View-In Menus) are included
	        includeDefaults: true,

	        constructor: function (params) {
	            console.log(params);
	            lang.mixin(this, params || {});

	            if (!this.map) {
	                throw new Error("a map is required");
	            }

	            if (this.parentMenu) {
	                this.attachToMenu(this.parentMenu);
	            }
	        },
	        //  attachToMenu
	        //      creates the "View In Menu" and attachs itself to the passed in Menu
	        attachToMenu: function (menu) {
	            if (typeof menu.isInstanceOf !== "function")
	                throw new Error("Invalid menu type");

	            var viewInMenu = null,
                    menuItems = [],
                    mainDropDown = new DropDownMenu({}),
                    i;

	            // Add in any custom links
	            if (this.viewInMenuItems && this.viewInMenuItems.length) {
	                for (i = 0; i < this.viewInMenuItems.length; i++) {
	                    menuItems.push(this._createDropDownFromMenuDef(this.viewInMenuItems[i]));
	                }

	            }

	            // Add in our defaults, if they are enabled
	            if (this.includeDefaults) {
	                for (i = 0; i < this._defaults.length; i++) {
	                    menuItems.push(this._createDropDownFromMenuDef(this._defaults[i]));
	                }
	            }

	            for (i = 0; i < menuItems.length; i++) {
	                mainDropDown.addChild(menuItems[i]);
	            }


	            if (menu.isInstanceOf(MenuBar)) {
	                // we create a PopupMenuBarItem
	                viewInMenu = new PopupMenuBarItem({ label: this.title, popup: mainDropDown, iconClass: this.iconClass || "" });
	            }
	            else {
	                // we create a PopupMenuItem
	                viewInMenu = new PopupMenuItem({ label: this.title, popup: mainDropDown, iconClass: this.iconClass || "" });
	            }

	            menu.addChild(viewInMenu);
	        },
	        //  _createDropDownFromMenuDef
	        //      Creates a drop-down menu for a "menu definition' which represents a collection of links to perform a "view in" 
	        _createDropDownFromMenuDef: function (menuDef) {
	            var dropDown = new DropDownMenu({}),
                    links = menuDef.links;

	            for (var i = 0; i < links.length; i++) {
	                dropDown.addChild(
                        new MenuItem(
                            {
                                label: links[i].label
                            , onClick: lang.hitch(this, this._linkOnClick, links[i].url)
                            })
                    );
	            }

	            return PopupMenuItem({ label: menuDef.label, iconClass: menuDef.iconClass || "", popup: dropDown });
	        },
	        //  _getMapInfo
	        //      returns all supported properties that will be used in URL string substition for links
	        _getMapInfo: function () {
	            var centerPt = this.map.extent.getCenter();
	            return { level: this.map.getLevel(), latitude: centerPt.getLatitude(), longitude: centerPt.getLongitude(), scale: this.map.getScale() };
	        },
	        //  _linkOnClick
	        //      click handler for link MenuItems. Performs string substituion on the link (url) and opens that link in a new window
	        _linkOnClick: function (link) {
	            var newUrl = string.substitute(link, this._getMapInfo());
	            window.open(newUrl);
	        }
	    });

	    return ViewIn;
	});