﻿// TODO: Ignore all things that come from ArcGISOnline

define([
    "dojo/_base/declare"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dojo/text!./templates/MapLink.html"
    , "dojo/_base/lang"
    , "dojo/_base/array"
    , "dojo/on"
    , "dijit/form/Button"
    , "dijit/form/SimpleTextarea"
    , "dijit/form/TextBox"
    , "dojo/dom-style"
    , "../common/UrlShortener"
    , "dojo/_base/window"
    , "dojo/string"
    , "dojo/dom-attr"
    , "esri/layers/ArcGISDynamicMapServiceLayer"
    , "../common/Common"
    , "../common/MapState"
    , "../common/MarkupManager"
    , "../extensions/esri/layers/MarkupLayer"
    , "dijit/form/CheckBox"
    , "dojo/fx/Toggler"
    , "dojo/fx"
    , "../_base/config"
], function (declare, _WidgetBase, _TemplatedMixin, template, lang, array, on, Button, SimpleTextarea, TextBox, domStyle, UrlShortener, win, string, domAttr, ArcGISDynamicMapServiceLayer, Common, MapState, MarkupManager, MarkupLayer, CheckBox, Toggler, coreFx, wvsConfig) {
    return declare([_WidgetBase, _TemplatedMixin], {

        templateString: template,

        declaredClass: "wvs.dijits.MapLink",

        // linkTemplate: String
        //      the template used to create the map link from the map state
        linkTemplate: "?sv=${services}&b=${basemap}&lon=${lon}&lat=${lat}&s=${scale}&mcId=${markupCollectionId}",

        _mapLink: null,

        markupService: null,

        markupLayer: null,

        /* Start Overrides */
        constructor: function (params, srcNodeRef) {
            // required params
            if (!params.map) {
                throw new Error("map required for " + this.declaredClass);
            }
            this.markupService = wvsConfig.defaults.services.markup;

            // optional parameters
            this.enableShortUrls = true;
            this.autoUpdateLink = true;

            lang.mixin(this, params);


            // instance vars
            this._showOptions = false;
            this.mapState = new MapState(this.map);
        },

        postCreate: function () {
            this.inherited(arguments);

            this.linkTextarea = new SimpleTextarea({
                style: "width:100%",
                rows: 10,
                selectOnClick: true
                //style: "width:350px;display:block;font-size:12px;"
            }, this.linkTextarea);

            this.updateLinkButton = new Button({
                label: "Update Link",
                onClick: lang.hitch(this, this.updateLink)
            }, this.updateLinkButton);

            this.shortUrlTextBox = new SimpleTextarea({
                //style:"width:200px;"
            }, this.shortUrlTextBox);

            this.shortenUrlButton = new Button({
                label: "Get short URL",
                onClick: lang.hitch(this, this._getShortUrl)
            }, this.shortenUrlButton);

            if (wvsConfig.defaults.markupLayer || this.markupLayer) {
                this.serializeMarkup = new CheckBox({
                    checked: false,
                    onChange: lang.hitch(this, this._onMarkupCheckboxChange)
                }, this.serializeMarkup);

                this.optionsToggler = new Toggler({
                    node: this.options,
                    showFunc: coreFx.wipeIn,
                    hideFunc: coreFx.wipeOut
                });
            }

        },

        startup: function () {
            var self = this;
            if (this.autoUpdateLink) {
                this.own(on(this.map, "extent-change", function () { self.updateLink(); }));
                this.own(on(this.map, "basemap-change", function () { self.updateLink(); }));
                domStyle.set(this.updateLinkButton.domNode, { display: "none" });
            }
            if (!this.enableShortUrls) {
                domStyle.set(this.shortenUrlButton.domNode, { display: "none" });
            }

            if (wvsConfig.defaults.markupLayer || this.markupLayer) {
                this.own(on(this.optionsLink, "click", lang.hitch(this, function () {
                    this._showOptions = !this._showOptions;
                    this.optionsLink.innerHTML = this._showOptions ? "Hide Options" : "Show Options";
                    if (this._showOptions)
                        this.optionsToggler.show();
                    else
                        this.optionsToggler.hide();
                })
                ));
            }
            else {
                domStyle.set(this.optionsLink, { display: "none" });
            }

            this._onMarkupCheckboxChange();

            this.own(on(this.openLink, "click", function () {
                window.open(self._mapLink, "_blank");
            }));
        },
        /* End Overrides */
        updateLink: function () {
            // summary:
            //      updates the shared link for the current state of the map

            var currentHref = window.document.location.protocol + "//" + window.document.location.host + window.document.location.pathname,
                mapState = this.mapState.toJson(),
                mapLinkTemplate = currentHref + this.linkTemplate;

            var mapLink = string.substitute(mapLinkTemplate, { scale: mapState.map.scale, lat: mapState.map.center[1], lon: mapState.map.center[0], basemap: mapState.map.basemap, services: JSON.stringify(mapState.layers), markupCollectionId: this.markupLayer._id || '-1' });
            this.linkTextarea.set("value", mapLink);

            this._mapLink = mapLink;
        },
        _getShortUrl: function () {
            // summary:
            //      produces a short url for the given map link
            var self = this,
                url = this.linkTextarea.get("value");

            if (url) {
                var shortener = new UrlShortener(url);
                this.shortenUrlButton.set("disabled", true);
                this.shortenUrlButton.set("iconClass", "icon-spinner icon-spin");
                var deferred = shortener.shorten();
                deferred.then(function (tinyUrl) {
                    self.shortenUrlButton.set("disabled", false);
                    self.shortenUrlButton.set("iconClass", "");
                    domStyle.set(self.shortUrlArea, { display: "block" });
                    self.shortUrlTextBox.set("value", tinyUrl);
                },
                function (error) {
                    self.shortenUrlButton.set("disabled", false);
                    self.shortenUrlButton.set("iconClass", "");
                    console.warn(error);
                });
            }
        },
        _onMarkupCheckboxChange: function () {
            // summary:
            //      handler for the serialize markup checkbox
            //this.showLoadingIcon();
            var self = this;
            var checked = this.serializeMarkup.checked;
            this.markupLayer = new MarkupLayer({ title: "Shared Markup", id: "sharedMarkup" });
            array.forEach(this.map.layerIds, function (layerId) {
                var layer = this.map.getLayer(layerId);
                if (layer.declaredClass === "wvs.layers.MarkupLayer") {
                    if (checked) {
                        for (var j = 0; j < layer.items.data.length; j++) {
                            if (layer.items.data[j].type != "folder") {
                                this.markupLayer.addGraphic(layer.items.data[j].graphic, "root");
                            }
                        }
                    } else {
                        this.mapState.addExclusion(layerId);
                    }
                }
            }, this);
            if (checked) {
                var mm = new MarkupManager();
                mm.storeSessionMarkup(this.markupLayer).then(
                    function (message) {
                        self.updateLink();
                    },
                    function (message) {
                        Common.errorDialog(message, "Markup Layer Action");
                        console.warn('failed');
                        self.updateLink();
                    }
                );
            } else {
                this.updateLink();
            }
        }
    });
});