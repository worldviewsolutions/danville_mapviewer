﻿define([
    "dojo/_base/declare"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dijit/_WidgetsInTemplateMixin"
    , "dojo/text!./templates/PrintWVS.html"
    , "dojo/_base/lang"
    , "dojo/_base/array"
    , "dojo/on"
    , "esri/tasks/PrintParameters"
    , "esri/tasks/PrintTemplate"
    , "esri/tasks/PrintTask"
    , "dijit/form/Select"
    , "dijit/form/RadioButton"
    , "dijit/form/CheckBox"
    , "dijit/form/TextBox"
    , "dijit/form/Button"
    , "../_base/config"
    , "dojo/dom-style"
    , "dojo/dom-class"
    , "dojo/dom"
    , "dojo/dom-attr"
    , "esri/request"
    , "dojo/dom-construct"
    , "../common/Common"
    , "../common/ObjUtil"
    , "esri/lang"
    , "esri/graphic"
    , "esri/symbols/jsonUtils"
    , "esri/geometry/Point"
    , "esri/geometry/Extent"
    , "esri/geometry/ScreenPoint"
    , "dijit/Fieldset"
    , "esri/SpatialReference"
], function (declare, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, template, lang, array, on, PrintParameters, PrintTemplate, PrintTask, Select, RadioButton, CheckBox, TextBox, Button, wvsConfig, domStyle, domClass, dom, domAttr, esriRequest, domConstruct, Common, ObjUtil, esriLang, Graphic, symbolJsonUtils, Point, Extent, ScreenPoint, Fieldset, SpatialReference) {
    // module:
    //      wvs/dijits/Print

    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {

        templateString: template,

        declaredClass: "wvs.dijits.Print",

        // GPServer: String
        //      points the geoprocessing print service that this widget will use (either custom or out-of-box)
        GPServer: null,

        // replaceMap: Array
        //      the print service must be able to access the urls for the webmap so sometimes we need to manipulate the request before it gets executed
        //      Example: [ { oldText: 'http://www.myservice.com:6080', newText: 'http://localhost'
        replaceMap: [],

        // qualityDef: Object(Enum)
        //      The quality specifications for printing maps.
        qualityDef: {
            Standard: 'standard',
            Medium: 'better',
            Best: 'best'
        },

        // textElements: Object(Enum)
        //      The enumerated type of text elements that may be used with templates.
        textElementsDef: {
            NAME: "Name",
            TITLE: "PageTitle"
        },

        //templates: Array
        //      The array of template objects returned from server.
        templates: null,

        // width: Number
        //      the width (inches) of the exported map
        width: 4,

        // height: Number
        //      the height (inches) of the exported map
        height: 4,

        // dpi: Number
        //      the dpi of the exported map
        dpi: 96,

        // useCustomMapScale: Boolean
        //      determines whether or not this map is to use a custom map scale
        useCustomMapScale: false,

        // isExtendedService: Boolean
        //      is this GP service extended to provide size information for templates?
        isExtendedService: false,

        // _exportLink: String
        //      the link to the exported image
        _exportLink: null,

        _serviceErrorMessage: "<h3>Could not resolve the geoprocessing service</h3><p>This widget will now close. Please try again later or check the service and/or your settings</p>",

        constructor: function (params, srcNodeRef) {
            // required params
            if (!params.map) {
                throw new Error("map required for " + this.declaredClass);
            }

            this.GPServer = params.GPServer ? params.GPServer : wvsConfig.defaults.services.print ? wvsConfig.defaults.services.print : null;

            if (!this.GPServer) {
                throw new Error("GPServer required for " + this.declaredClass);
            }

            this._onExtentChangeHandler = null;
        },
        postCreate: function () {
            //Apply uniform styling classes
            domClass.add(this.widthTextbox.domNode, "span1");
            domClass.add(this.heightTextbox.domNode, "span1");
            domClass.add(this.titleTextbox.domNode, "span2");

            for (var prop in this.qualityDef) {
                this.basemapQualitySelect.addOption({ label: prop, value: this.qualityDef[prop] });
            }

            //Initialize values
            this.widthTextbox.set("value", this.width);
            this.heightTextbox.set("value", this.height);
            this.enableCustomScaleCheckbox.set("checked", this.useCustomMapScale);
            this.customScaleTextBox.set("disabled", !this.useCustomMapScale);
            this.customScaleTextBox.set("value", Math.ceil(this.map.getScale()));

            this.own(on(this.exportLink, "click", lang.hitch(this, function () {
                this.exportButton.set("iconClass", "");
                this.exportButton.set("disabled", false);
                domStyle.set(this.exportButton.domNode, { display: "inline-block" });
                domStyle.set(this.exportLink, { display: "none" });
                window.open(this._exportLink, '_blank');
            })));

            this._customScaleCheckboxChange(this.useCustomMapScale);
        },

        _formatChanged: function () {
            if (this.exportFormatSelect.get("value").toLowerCase() === "pdf") {
                this.specifyTemplateRadio.set("checked", true);
            }
            else {
                this.specifySizeRadio.set("checked", true);
            }
        },

        _customScaleCheckboxChange: function (checked) {
            this.customScaleTextBox.set("disabled", !checked);
            this.useCustomMapScale = checked;
            if (!checked) {
                if (!this._onExtentChangeHandler) {
                    this._onExtentChangeHandler = on(this.map, "extent-change", lang.hitch(this, this._onExtentChange));
                    this.own(this._onExtentChangeHandler);
                }
                if (this.width && this.height) {
                    this._placeSelectionExtent(this.width, this.height);
                }
            }
            else {
                if (this._onExtentChangeHandler) {
                    this._onExtentChangeHandler.remove();
                    this._onExtentChangeHandler = null;
                }
                this._removeSelectionExtent();
            }
        },

        _setDimensionSelection: function () {
            // summary:
            //      toggles between template and size mode based on form values
            if (this.specifySizeRadio.get("checked")) {
                this._onWidthHeightChange();
                domStyle.set(this.sizeOptions, { display: "block" });
                domStyle.set(this.templateOptions, { display: "none" });
            }
            else {
                this._onTemplateChange();
                domStyle.set(this.sizeOptions, { display: "none" });
                domStyle.set(this.templateOptions, { display: "block" });
            }
        },
        _onExtentChange: function (evt) {
            if (evt.levelChange) {
                this.customScaleTextBox.set("value", Math.ceil(this.map.getScale()));
            }
        },
        startup: function () {
            var self = this;
            self.templates = [];

            // set a loading icon just in case
            var loadingSpan = domConstruct.create("span", { "class": "icon-spinner icon-spin" }, this.templateSelect, 'after');
            // Get our templates from the REST endpoint
            var request = new esriRequest({
                url: this.GPServer,
                content: { f: "json" },
                handleAs: "json"
            });

            request.then(
                function (response) {
                    if (response.templates) {
                        var options = [];
                        for (var i = 0; i < response.templates.length; i++) {
                            self.templates.push(response.templates[i]);
                            options.push({ label: response.templates[i].name, value: response.templates[i].name });
                        }
                    }
                    domConstruct.destroy(loadingSpan);
                    self.templateSelect.set("options", options);
                    self.templateSelect.set("value", options[0]);
                },
                function (error) {
                    domConstruct.destroy(loadingSpan);
                    Common.errorDialog(self._serviceErrorMessage)
                        .then(function () { self.destroy(); });

                    throw new Error("Could not resolve the geoprocessing service for " + self.declaredClass);
                }
            );

            this.own(on(this.map, "extent-change", lang.hitch(this, function () {
                if (!this.useCustomMapScale && this._selectionExtent && this.width && this.height) {
                    this._placeSelectionExtent(this.width, this.height);
                }
            })));

            this._placeSelectionExtent(this.width, this.height);
        },

        destroy: function () {
            this.map.graphics.remove(this._selectionExtent);
            this.inherited(arguments);
        },

        _onTemplateChange: function () {
            if (!this.specifySizeRadio.get("checked")) {
                var curValue = this.templateSelect.get("value"),
                    rowString = "<div class=\"row-fluid\"></div>",
                    labelDivString = "<div class=\"colspan3\"></div>",
                    textBoxDivString = "<div class=\"colspan9\"></div>";
                domConstruct.empty(this.textElementsList);
                for (var i = 0; i < this.templates.length; i++) {
                    if (curValue === this.templates[i].name) {
                        this._placeSelectionExtent(this.templates[i].mapWidth, this.templates[i].mapHeight);
                        for (var j = 0; j < this.templates[i].textElements.length; j++) {
                            var element = this.templates[i].textElements[j];
                            var rowNode = domConstruct.place(rowString, this.textElementsList, "last");
                            var labelNode = domConstruct.place(labelDivString, rowNode, "first");
                            var textBoxNode = domConstruct.place(textBoxDivString, rowNode, "last");

                            var textElem = element;
                            for (var k = 1; k < textElem.length; k++) {
                                if ((textElem.charAt(k) != ' ' && textElem.charAt(k - 1) != ' ')
                                    && (textElem.charAt(k) == textElem.charAt(k).toUpperCase())
                                    && (textElem.charAt(k - 1) == textElem.charAt(k - 1).toLowerCase())) {
                                    textElem = textElem.substring(0, k) + " " + textElem.substring(k);
                                    k--;
                                }
                            }
                            domConstruct.place("<label>" + textElem + ": </label>", labelNode, 'first');
                            this[this.templates[i].textElements[j]] = new TextBox({}, textBoxNode);
                        }
                        this.mapNameTextbox.set({ disabled: true, value: "" });//domStyle.set(this.mapNameDiv, { display: 'none' });
                        this.titleTextbox.set({ disabled: true, value: "" });//domStyle.set(this.mapTitleDiv, { display: 'none' });
                        for (var j = 0; j < this.templates[i].textElements.length; j++) {
                            if (this.templates[i].textElements[j] === this.textElementsDef.NAME) {
                                this.mapNameTextbox.set("disabled", false);//domStyle.set(this.mapNameDiv, { display: 'block' });
                            } else if (this.templates[i].textElements[j] === this.textElementsDef.TITLE) {
                                this.titleTextbox.set("disabled", false);//domStyle.set(this.mapTitleDiv, { display: 'block' });
                            }
                        }
                    }
                }
            }
        },

        _onWidthHeightChange: function () {
            this._placeSelectionExtent(parseFloat(this.widthTextbox._getDisplayedValueAttr()), parseFloat(this.heightTextbox._getDisplayedValueAttr()));
        },

        _removeSelectionExtent: function () {
            if (this._selectionExtent) {
                this.map.graphics.remove(this._selectionExtent);
                this._selectionExtent = null;
            }
        },

        _placeSelectionExtent: function (w, h) {
            var dpi = this.dpi;

            if (isNaN(w) || isNaN(h) || w < 0 || h < 0) {
                return;
            }

            this.width = w;
            this.height = h;


            w = w * dpi;
            h = h * dpi;

            var centerPt = this.map.toScreen(this.map.extent.getCenter()),
                blExtentPt = this.map.toMap(new ScreenPoint(centerPt.x - (w / 2), centerPt.y + (h / 2))),
                trExtentPt = this.map.toMap(new ScreenPoint(centerPt.x + (w / 2), centerPt.y - (h / 2)));

            if (!this._selectionExtent) {
                this._selectionExtent = new Graphic();
                this._selectionExtent.setSymbol(symbolJsonUtils.fromJson(wvsConfig.defaults.symbols.selectionPolygon));
                this._selectionExtent.setGeometry(new Extent(blExtentPt.x, blExtentPt.y, trExtentPt.x, trExtentPt.y, blExtentPt.spatialReference));
                this.map.graphics.add(this._selectionExtent);
            }
            else {
                this._selectionExtent.setGeometry(new Extent(blExtentPt.x, blExtentPt.y, trExtentPt.x, trExtentPt.y, blExtentPt.spatialReference));
            }
        },

        _startExport: function () {
            var self = this,
                params = { f: 'json' },
                targetUrl = this.GPServer,
                mapLayers = this.map.getLayersVisibleAtScale(this.map.getScale());

            this.exportButton.set("iconClass", "icon-spinner icon-spin");
            this.exportButton.set("disabled", true);

            if (this.specifySizeRadio.get("checked")) {
                targetUrl += "/ExportImage";
                params.size = [(this.width * this.dpi), (this.height * this.dpi)];
                params.format = this.exportFormatSelect.get("value");
            } else {
                var templateId;

                for (var i = 0; i < this.templates.length; i++) {
                    if (this.templates[i].name === this.templateSelect.get("value")) {
                        templateId = this.templates[i].id;
                    }
                }
                params.textElements = {};
                for (var i = 0; i < this.templates.length; i++) {
                    if (this.templates[i].name === this.templateSelect.get("value")) {
                        for (var j = 0; j < this.templates[i].textElements.length; j++) {
                            params.textElements[this.templates[i].textElements[j]] = this[this.templates[i].textElements[j]].get("value");
                        }
                    }
                }
                targetUrl += "/Templates/" + templateId + "/Export";
            }
            params.bbox = this._selectionExtent.geometry.xmin + ", ";
            params.bbox += this._selectionExtent.geometry.ymin + ", ";
            params.bbox += this._selectionExtent.geometry.xmax + ", ";
            params.bbox += this._selectionExtent.geometry.ymax;
            params.bboxSR = this._selectionExtent.geometry.spatialReference.toJson();
            params.scale = this.map.getScale();
            //params.format = this.exportFormatSelect.get("value");
            params.resources = [];
            params.graphics = [];

            for (var i = 0; i < mapLayers.length; i++) {
                if (mapLayers[i].url) {
                    var newResource = {
                        name: mapLayers[i].getServiceName().trim(),
                        restUrl: mapLayers[i].url,
                        opacity: mapLayers[i].opacity
                    };
                    if (mapLayers[i].visibleLayers) {
                        for (var j = 0; j < mapLayers[i].layerInfos.length; j++) {
                            if (mapLayers[i].layerInfos[j].visible) {
                                if (!newResource.layers) {
                                    newResource.layers = [];
                                }
                                newResource.layers.push({ id: mapLayers[i].layerInfos[j].id, visible: true });
                            }
                        }
                    }
                    if (mapLayers[i].declaredClass) {
                        if (mapLayers[i].declaredClass.indexOf("ArcGISDynamicMapServiceLayer") >= 0) {
                            newResource.type = "ArcGISDynamicMapService";
                            newResource.token = null;
                        } else if (mapLayers[i].declaredClass.indexOf("ArcGISTiledMapServiceLayer") >= 0) {
                            newResource.type = "ArcGISTiledMapService";
                        }
                    }
                    params.resources.push(newResource);
                } else if (mapLayers[i].declaredClass && mapLayers[i].declaredClass.indexOf("GraphicsLayer") >= 0) {
                    if (!mapLayers[i].graphics) {
                        continue;
                    }
                    for (var j = 0; j < mapLayers[i].graphics.length; j++) {
                        params.graphics.push(mapLayers[i].graphics[j].toJson());
                    }
                }
            }

            for (var prop in params) {
                if (typeof (params[prop]) == "object") {
                    params[prop] = JSON.stringify(params[prop]);
                }
            }
            esriRequest({
                content: params,
                url: targetUrl
            }, {
                usePost: true
            }).then(function (response) {
                domStyle.set(self.exportButton.domNode, { display: "none" });
                var urlRoot = self.GPServer;
                if (self.GPServer.indexOf('/rest') >= 0) {
                    urlRoot = self.GPServer.substring(0, self.GPServer.indexOf('/rest'));
                }
                self.GPServer.substring
                self._exportLink = urlRoot + response.url;
                domStyle.set(self.exportLink, { display: "inline" });
                // TODO: Uncomment this line when the esri bug is gone
                //if (self.replaceMap.length || typeof customScale === "number")
            });
        }
    });
});