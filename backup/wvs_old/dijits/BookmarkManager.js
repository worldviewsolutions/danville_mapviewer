﻿console.log("wvs/dijits/BookmarkManager.js");

define([
    "dojo/_base/declare"
    , "dojo/_base/lang"
    , "dojo/on"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dijit/_WidgetsInTemplateMixin"
    , "dojo/text!./templates/BookmarkManager.html"
    , "../_base/config"
    , "../common/Bookmark"
    , "esri/geometry/Extent"
    , "dojo/_base/array"
    , "dojo/dom"
    , "dojo/dom-construct"
    , "dojo/dom-class"
    , "dojo/dom-attr"
    , "dojo/query"
    , "dijit/form/CheckBox"
    , "dijit/form/TextBox"
    , "dojo/dom-style"
    , "dijit/form/Button"
    , "dojo/Evented"
    , "../common/Common"
    , "./SimpleButton"
], function (declare, lang, on, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, template, config, Bookmark, Extent, array, dom, domConstruct, domClass, domAttr, query, CheckBox, TextBox, domStyle, Button, Evented, Common, SimpleButton) {
   // module:
   //      wvs/dijits/BookmarkManager
   var BookmarkManager = declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, Evented], {
      templateString: template,

      declaredClass: 'wvs.dijits.BookmarkManager',

      /*@function Constructor
      *     create a BookmarkManager singleton instance
      *@param params: Object
      *     The object hash for pre-set instance properties.
      *@param srcNodeRef: DomNode
      *     The DOM node to which the BookmarkManager will be attached.
      */
      constructor: function (params, srcNodeRef) {
         this.root = new Bookmark({
            name: "Bookmarks",
            children: null
         });

         if (!params.map) {
            throw new Error('map not defined in params for ' + this.declaredClass);
         }

         lang.mixin(this, params);
         this.root.children = this.bookmarks || [];

         // instance vars
         this.activeBookmark = null;
         this.selectedFolder = this.root;
      },


      /*@function postCreate
      *     Handles all of the DOM setup and manipulation once the BookmarkManager exists as an element of the DOM
      */
      postCreate: function () {
         var self = this;

         //Set the selectedFolderNode to the root node. If a user doesn't select an existing folder, the bookmark or folder should
         //be added to the root Bookmarks directory. Do this in postCreate to be certain the node exists in the DOM tree.
         this.selectedFolderNode = this.rootBookmarkNode;

         if (this.root.children.length) {
            for (var i = 0; i < this.root.children.length; i++) {
               this.add(this.root.children[i], false, this.root, this.rootBookmarkNode);
            }
         }

         this.nameTextbox = new TextBox({ value: "", selectOnClick: true }, this.nameTextbox);
         this.updateExtentCheckbox = new CheckBox({ checked: false }, this.updateExtentCheckbox);
      },

      /*@function destroy
      *     The handler for destroying the widget.
      */
      destroy: function () {
         this.emit("close");
         this.inherited(arguments);
      },

      /*@function add
      *     Adds a new or existing bookmark to the widget display.
      *@param bookmark: Bookmark
      *     The Bookmark instance to be added to the widget
      *@param isNew: bool
      *     Boolean value to indicate whether the bookmark is a new bookmark or existing bookmark.
      *@param parentFolder: Bookmark
      *     The bookmark folder to which the new bookmark is to be added.
      *@param parentNode: DomNode
      *     The domNode to which the new DOM representations of the bookmarks should be added as siblings
      */
      add: function (bookmark, isNew, parentFolder, parentNode) {

         //Create two nodes to put folders and bookmarks into, if they do not already exist, as siblings of the
         //designated parent node.
         if (!parentNode.folderNode) {
            parentNode.folderNode = domConstruct.create("div", { "class": "row-fluid", "style": "margin-left: 10px;" }, parentNode, "after");
         }
         if (!parentNode.bookmarkNode) {
            parentNode.bookmarkNode = domConstruct.create("div", { "class": "row-fluid", "style": "margin-left: 15px;" }, parentNode.folderNode, "after");
         }

         //Create the container div for the bookmark
         var newBookmarkDiv = domConstruct.create("div", { "class": "row-fluid bookmarkList" });

         //If the bookmark has children, it is a folder and should be given a folder icon and the folder select functionality should be hooked to its click event
         if (bookmark.hasChildren()) {
            var folderIcon = domConstruct.create("i", { "class": "colspan1 icon-folder-close", "style": "padding-left: 10px;" }, newBookmarkDiv);
            domStyle.set(newBookmarkDiv, "cursor", "pointer");
            //Connect to the click event to toggle the icon and open the sub-directory for the folder
            on(newBookmarkDiv, "click", lang.hitch(this, this._onFolderSelect, newBookmarkDiv, bookmark, folderIcon));
         } else {
            domConstruct.create("i", { "class": "colspan1 icon-bookmark-empty", "style": "padding-left: 15px;" }, newBookmarkDiv);
         }

         var bookmarkLabel = domConstruct.create("div", { "class": "colspan9 bookmarkLabel", innerHTML: bookmark.name }, newBookmarkDiv);

         //Make sure to use absolute positioning for the edit and delete icons. This avoids losing them off the side of the widget for deeply nested folder structures.
         var editIcon = domConstruct.create("span", { "class": "colspan1 icon-pencil", title: "Edit this bookmark", style: "cursor:pointer; position: absolute; right: 35px;" }, newBookmarkDiv);
         on(editIcon, "click", lang.hitch(this, this._onEditBookmark, bookmark, bookmarkLabel));

         var deleteIcon = domConstruct.create("span", { "class": "colspan1 icon-trash", title: "Delete this bookmark", style: "cursor:pointer; position: absolute; right: 0px;" }, newBookmarkDiv);
         on(deleteIcon, "click", lang.hitch(this, this._onRemoveBookmarkClick, bookmark, newBookmarkDiv));

         if (bookmark.hasChildren()) {
            domConstruct.place(newBookmarkDiv, parentNode.folderNode, "last");
         } else {
            domConstruct.place(newBookmarkDiv, parentNode.bookmarkNode, "last");
         }

         //If the bookmark is new, add it to the appropriate folder's children and open the update components.
         if (isNew) {
            parentFolder.children.push(bookmark);
            domStyle.set(this.bookmarkOptions, { display: "block" });
            this._onEditBookmark(bookmark, bookmarkLabel);
            this.updateExtentCheckbox.set("checked", true);
            this.updateExtentCheckbox.set("disabled", true);
            this.emit("bookmarks-updated", this.root.children);
         }
      },

      /*@function edit
      *     edit the title of an existing bookmark. Then emit the "bookmark-updated" event to update the Bookmarks menu.
      *@param bookmark: Bookmark
      *     The bookmark whose 'name' property is being edited.
      *@param labelNode: DomNode
      *     The HTML element currently displaying the name of the bookmark to be updated by the edit operation.
      */
      _onEditBookmark: function (bookmark, labelNode) {

         // Set bookmark as active
         this.activeBookmark = { bookmark: bookmark, labelNode: labelNode };

         // Set form values
         this.nameTextbox.set("value", bookmark.name);
         this.updateExtentCheckbox.set("checked", false);
         this.updateExtentCheckbox.set("disabled", false);

         // Show form
         domStyle.set(this.bookmarkOptions, { display: "block" });
         if (bookmark.hasChildren()) {
            domStyle.set(this.updateExtentContainer, { display: "none" });
         } else {
            domStyle.set(this.updateExtentContainer, { display: "block" });
         }
      },

      /*@function _onSaveButtonClick
      *     Click handler for save button. Initializes saving edits to a bookmark.
      */
      _onSaveButtonClick: function () {
         if (this.activeBookmark) {
            this._saveBookmark(this.activeBookmark.bookmark, this.activeBookmark.labelNode);
         }
      },

      /*@function save
      *     Saves the changes made to a bookmark's name/extent properties.
      *@param bookmark: Bookmark
      *     The bookmark whose properties have been edited and are being saved
      *@param labelNode: DomNode
      *     The HTML element displaying the name of the edited bookmark, updated by the save operation.
      */
      _saveBookmark: function (bookmark, labelNode) {
         if (bookmark) {
            bookmark.name = this.nameTextbox.get("value");
            if (this.updateExtentCheckbox.get("checked")) {
               bookmark.extent = this.map.extent.toJson();
            }

            labelNode.innerHTML = this.nameTextbox.get("value");

            // Show form
            domStyle.set(this.bookmarkOptions, { display: "none" });

            this.emit("bookmarks-updated", this.root.children);
         }
      },

      /*@function _onCancelButtonClick
      *     Click handler for cancel button. Cancels edits a user would make.
      */
      _onCancelButtonClick: function () {
         domStyle.set(this.bookmarkOptions, { display: "none" });
      },

      /*@function _onRemoveBookmarkClick
      *     Click handler for the trash icon for bookmarks. Fires the confirmation dialog with a callback
      *     to the _removeBookmark function.
      *@param bookmark: Bookmark
      *     The bookmark to be trimmed from the tree
      *@param containingDiv: DomNode
      *     The HtmlElement for the bookmark in the UI.
      */
      _onRemoveBookmarkClick: function (bookmark, containingDiv) {
         var dialog = Common.confirmDialog("Are you sure you want to delete this bookmark?");
         dialog.then(lang.hitch(this, this._removeBookmark, bookmark, containingDiv));
      },

      /*@fuction _onRemoveBookmarkClick
      *     Click handler for remove bookmark button. Presents user with confirmation dialog confirming the removal of the bookmark.
      *@param bookmark: Bookmark
      *     The bookmark to be trimmed from the tree
      *@param containingDiv: DomNode
      *     The HTMLElement displaying the bookmark.
      *@param confirmDelete: bool
      *     The users response to the confirmation dialog for deleting a bookmark.
      */
      _removeBookmark: function (bookmark, containingDiv, confirmDelete) {
         if (confirmDelete) {
            domStyle.set(this.bookmarkOptions, { display: "none" });

            //Because of the tree structure, we need to be able to recursively traverse nodes and remove the bookmark
            //when we find it; so the traverse bookmarks internal function serves that purpose for us.
            var traverseBookmarks = function (bookmarkArray) {
               for (var i = 0; i < bookmarkArray.length; i++) {
                  if (bookmarkArray[i] === bookmark) {
                     bookmarkArray.splice(i, 1);
                     break;
                  } else {
                     if (bookmarkArray[i].hasChildren()) {
                        traverseBookmarks(bookmarkArray[i].children);
                     }
                  }
               }
            }

            //Recursively search through and then emit necessary events and perform necessary DOM cleanup.
            traverseBookmarks(this.root.children);
            this.emit("bookmarks-updated", this.root.children);
            domConstruct.destroy(containingDiv.folderNode);
            domConstruct.destroy(containingDiv.bookmarkNode)
            domConstruct.destroy(containingDiv);
         }
      },

      /*@function addFolder
      *     onclick event handler for the 'Add Folder' button. Creates a bookmark with an empty 'children' array and adds it to the manager.
      */
      _onAddFolderClick: function () {
         var bookmark = new Bookmark({
            name: "Unnamed Folder",
            extent: this.map.extent.toJson(),
            children: []
         });
         this.add(bookmark, true, this.selectedFolder, this.selectedFolderNode);
      },

      /*@function addBookmark
      *     onclick event handler for the 'Add Bookmark' button. Creates a bookmark with no children array and adds it to the manager.
      */
      _onAddBookmarkClick: function () {
         var bookmark = new Bookmark({
            name: "Unnamed Bookmark",
            extent: this.map.extent.toJson(),
            children: null
         });
         this.add(bookmark, true, this.selectedFolder, this.selectedFolderNode);
      },

      /*@function folderSelect
      *     onclick event handler for DomNodes of bookmark folders. Calls the necessary toggle functions for changing the folder icon, opening the sub-directory
      *     and setting the active folder for adding bookmarks to. These functions were rolled into a single event handler to be handled synchronously.
      *@param node: DomNode
      *     The node that was clicked whose sub-directory and icon should be toggled.
      *@param bookmark: Bookmark
      *     The bookmark object in internal memory that is represented by the 'node' parameter.
      *@param icon: DomNode
      *     A reference to the HTML element holding the folder icon to be toggled.
      */
      _onFolderSelect: function (node, bookmark, icon) {
         this._toggleFolder(icon);
         lang.hitch(this, this._toggleSubDirectory(node, bookmark));
         lang.hitch(this, this._setSelectedFolder(node, bookmark));
      },

      /*@function _toggleFolder
      *     Toggles the open/close image for the folder icon when a folder is selected.
      *@param icon: DomNode
      *     The HTML element whose class is to be changed to change the displayed icon.
      */
      _toggleFolder: function (icon) {
         domClass.toggle(icon, "icon-folder-close");
         domClass.toggle(icon, "icon-folder-open");
      },

      /*@function _toggleSubDirectory
      *     Toggles the display of the sub-directory of a clicked folder so that an open sub-directory is closed and vice versa.
      *@param node: DomNode
      *     The HTML Element representing the folder to be 'opened', next to which siblings will be added to display the sub directory.
      *@param bookmark: Bookmark
      *     The Bookmark object representing the folder in internal memory whose children will be added when the sub-directory is opened.
      */
      _toggleSubDirectory: function (node, bookmark) {
         var self = this;
         //If the node has a "folderNode" property, then it is 'opened'. Destroy the sub-nodes to clear the UI and delete
         //the properties to maintain an internally consistent representation of what is currently displayed in the UI
         if (node.folderNode) {
            domConstruct.destroy(node.folderNode);
            domConstruct.destroy(node.bookmarkNode);
            delete node.folderNode;
            delete node.bookmarkNode;
         } else {
            for (var i = 0; i < bookmark.children.length; i++) {
               self.add(bookmark.children[i], false, bookmark, node);
            };
         }
      },

      /*@function _setSelectedFolder
      *     Sets the currently selected folder to which bookmarks and folders will be added as children.
      *@param node: DomNode
      *     The HTML element representing the folder which is either being selected or deselected.
      *@param bookmark: Bookmark
      *     The Bookmark object which represents the selected/deselected folder in internal memory.
      */
      _setSelectedFolder: function (node, bookmark) {

         //remove the .selected-bookmark class from the currently selected folder (there can only be on folder selected at a time).
         query(".selected-bookmark").forEach(this._deselectBookmark);

         //Add the class to the clicked node if it is not already opened (as indicated by the existence of a 'folderNode' property) or if
         //it is an empty folder that is not currently selected. Then set the clicked node as the currently selected node/bookmark.
         if (node.folderNode || (bookmark.children.length == 0 && node != this.selectedFolderNode)) {
            domClass.add(node, 'selected-bookmark');
            this.selectedFolder = bookmark;
            this.selectedFolderNode = node;
         } else {
            //If the bookmark has been de-selected, then set the currently selected node/bookmark to the root bookmark object.
            this.selectedFolder = this.root;
            this.selectedFolderNode = this.rootBookmarkNode;
         }
      },

      /*@function _deselectBookmark
      *     The event handler for querying up any HtmlElements with the 'selected-bookmark' class.
      *@param element: HtmlElement
      *     The element to remove the 'selected-bookmark' class from.
      */
      _deselectBookmark: function (element) {
         domClass.remove(element, 'selected-bookmark');
      },

      share: function (bookmark) {
         // TODO: Implement
      }
   });

   return BookmarkManager;
});