﻿define([
	"dojo/_base/lang",
    "dojo/_base/declare",
    "dojo/_base/array",
    "dojo/on",
	"dojo/dom",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dijit/layout/BorderContainer",
    "dojox/layout/ToggleSplitter",
    "dijit/_TemplatedMixin",
    "dojo/text!./templates/MapViewer.html",
    "esri/map",
    "esri/geometry/Extent",
    "esri/dijit/Scalebar",
    "esri/dijit/OverviewMap",
    "esri/dijit/HomeButton",
    "./Swipe",
    "esri/dijit/BasemapGallery",
	"esri/layers/ArcGISTiledMapServiceLayer",
	"esri/layers/ArcGISDynamicMapServiceLayer",
	"esri/layers/FeatureLayer",
    "esri/layers/GraphicsLayer",
    "esri/layers/GeoRSSLayer",
    "esri/layers/ArcGISImageServiceLayer",
    "./ResultsConsole",
    "./CoordinateDisplay",
    "./Toc",
    "./Filter/AttributeSearch",
    "./ZoomToXY",
    "./Identify",
    "./Markup",
    "./MapLink",
    "./Measure",
    "../extensions/esri/layers/MarkupLayer",
    "./ToolContainer",
    "../common/SearchResultsStore",
    "dijit/layout/TabContainer",
    "dijit/layout/ContentPane",
    "dijit/DropDownMenu",
	"dijit/MenuBar",
	"dijit/MenuItem",
    "dijit/PopupMenuBarItem",
	"dijit/PopupMenuItem",
    "dijit/MenuBarItem",
    "dijit/MenuSeparator",
	"dijit/TitlePane",
    "esri/dijit/Basemap",
    "esri/dijit/BasemapLayer",
    "require",
    "dojo/aspect",
    "dojo/dom-style",
    "./MapManager",
    "dijit/Dialog",
    "dojo/window",
    "../_base/config",
    "esri/config",
    "dojo/string",
    "./Print",
    "../common/Common",
    "dojo/io-query",
    "dojo/has",
    "esri/graphic",
    "dojo/_base/unload",
    "../common/InfoTemplateManager",
    "../common/MapState",
    "./BookmarkManager",
    "./ViewIn",
    "dojo/store/Memory",
    "../extensions/esri/Map",
    "../extensions/esri/layers/ArcGISDynamicMapServiceLayer",
    "../extensions/esri/layers/LayerInfo",
    "../extensions/dojo/_base/lang"
], function (
	lang,
    declare,
    array,
	on,
    dom,
    domClass,
    domConstruct,
    BorderContainer,
    ToggleSplitter,
    _TemplatedMixin,
    template,
    Map,
    Extent,
    Scalebar,
    OverviewMap,
    HomeButton,
    Swipe,
    BasemapGallery,
	ArcGISTiledMapServiceLayer,
	ArcGISDynamicMapServiceLayer,
    FeatureLayer,
    GraphicsLayer,
    GeoRSSLayer,
    ArcGISImageServiceLayer,
    ResultsConsole,
	CoordinateDisplay,
    Toc,
    AttributeSearch,
    ZoomToXY,
    Identify,
    Markup,
    MapLink,
    Measure,
    MarkupLayer,
    ToolContainer,
    SearchResultsStore,
    TabContainer,
    ContentPane,
    DropDownMenu,
    MenuBar,
    MenuItem,
    PopupMenuBarItem,
    PopupMenuItem,
    MenuBarItem,
    MenuSeparator,
    TitlePane,
    Basemap,
    BasemapLayer,
    require,
    aspect,
    domStyle,
    MapManager,
    Dialog,
    win,
    config,
    esriConfig,
    string,
    Print,
    Common,
    ioQuery,
    has,
    Graphic,
    baseUnload,
    InfoTemplateManager,
    MapState,
    BookmarkManager,
    ViewIn,
    Memory
) {
    return declare([BorderContainer, _TemplatedMixin], {

        templateString: template,

        constructor: function (params, srcNodeRef) {

            has.add("local-storage", function (global, document, anElement) {
                if (global.localStorage) {
                    return true;
                }
                else {
                    return false;
                }
            });

            this.settings = {};
            this.design = 'headline';
            this._splitterClass = ToggleSplitter;
            this.fullscreen = false;

            // setting should be passed in, do a null check
            var defaultSettings = {
                // TODO: potentially rename this property from global
                // some options may affect more than one widget (like basemaps affecting the gallery and the map)
                globalOptions: {
                    basemaps: [],
                    layers: [],
                    layout: {
                        minimized: {
                            leftPane: true,
                            resultsPane: true,
                            mapToolbar: true
                        },
                        maximized: {
                            leftPane: false,
                            resultsPane: false,
                            mapToolbar: false
                        }
                    }
                },
                map: {
                    area: "mapNodeRegion",
                    widgetOptions: {
                        logo: false,
                        sliderStyle: 'large',
                        slider: true,
                        basemap: "topo",
                        sliderPosition: "top-right"
                    }
                },
                areas: [
                    { name: "mapRegion", visible: true, parent: "", load: this._renderMapRegion },
                    { name: "leftRegion", visible: true, parent: "", load: this._renderLeftRegion },
                    { name: "bottomRegion", visible: true, parent: "", load: this._renderBottomRegion },
                    { name: "mapNodeRegion", visible: true, parent: "mapRegion", load: this._mapNodeRegion },
                    { name: "bottomMapRegion", visible: true, parent: "mapRegion", load: this._renderBottomMapRegion },
                    { name: "mainMenu", visible: true, parent: "mapRegion", load: this._renderMainMenu },
                    { name: "toolsMenu", visible: true, parent: "mainMenu", load: this._renderToolsMenu }
                ],
                widgets:[
                    {
                        visible: true,
                        displayName: "Export Map & Print",
                        id: "exportTool",
                        ctr: Print,
                        area: {
                            name: "mainMenu",
                            options: { iconClass: "icon-screenshot" }
                        },
                        target: {
                            type: "dialog",
                            options: { "class": "nonModal" }
                        },
                        widgetOptions: { }
                    },
                    {
                        visible: true,
                        displayName: "Bookmarks",
                        id: "bookmarks",
                        ctr: BookmarkManager,
                        area: {
                            name: "mainMenu",
                            options: { iconClass: "icon-screenshot" }
                        },
                        target: {
                            type: "dialog",
                            options: { "class": "nonModal" }
                        },
                        widgetOptions: {}
                    },
                    {
                        visible: true,
                        displayName: "Basemap Gallery",
                        id: "basemapGallery",
                        widgetOptions: {
                            // If this is set to true, the basemap gallery is automatically loaded by esri (recommended to avoid for now for more control)
                            showArcGISBasemaps: false
                        },
                        targetNode: "basemapGalleryNode",
                        esriBasemaps: [
                                {
                                    options: {
                                        thumbnailUrl: require.toUrl("./images/imagery_thumbnail.jpg"),
                                        title: "Imagery"
                                    },
                                    layers: [
                                        {
                                            url: "http://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer"
                                        }
                                    ]
                                },
                                {
                                    options: {
                                        thumbnailUrl: require.toUrl("./images/imagery_labels_thumbnail.png"),
                                        title: "Imagery with Labels"
                                    },
                                    layers: [
                                        {
                                            url: "http://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer"
                                        },
                                        {
                                            url: "http://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places/MapServer"
                                        }
                                    ]
                                },
                                {
                                    options: {
                                        thumbnailUrl: require.toUrl("./images/steets_thumbnail.jpg"),
                                        title: "Streets"
                                    },
                                    layers: [
                                        {
                                            url: "http://services.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer"
                                        }
                                    ]
                                },
                                {
                                    options: {
                                        thumbnailUrl: require.toUrl("./images/topo_thumbnail.jpg"),
                                        title: "Topographic"
                                    },
                                    layers: [
                                        {
                                            url: "http://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer"
                                        }
                                    ]
                                },
                                {
                                    options: {
                                        thumbnailUrl: require.toUrl("./images/terrain_labels.png"),
                                        title: "Terrain with Labels"
                                    },
                                    layers: [
                                        {
                                            url: "http://services.arcgisonline.com/ArcGIS/rest/services/World_Terrain_Base/MapServer"
                                        },
                                        {
                                            url: "http://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Reference_Overlay/MapServer"
                                        }
                                    ]
                                },
                                {
                                    options: {
                                        thumbnailUrl: require.toUrl("./images/light_gray_canvas_thumbnail.png"),
                                        title: "Light Gray Canvas"
                                    },
                                    layers: [
                                        {
                                            url: "http://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer"
                                        },
                                        {
                                            url: "http://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Reference/MapServer"
                                        }
                                    ]
                                },
                                {
                                    options: {
                                        thumbnailUrl: require.toUrl("./images/natgeo_thumbnail.jpg"),
                                        title: "National Geographic"
                                    },
                                    layers: [
                                        {
                                            url: "http://services.arcgisonline.com/ArcGIS/rest/services/NatGeo_World_Map/MapServer"
                                        }
                                    ]
                                },
                                {
                                    options: {
                                        thumbnailUrl: require.toUrl("./images/oceans_thumbnail.jpg"),
                                        title: "Oceans"
                                    },
                                    layers: [
                                        {
                                            url: "http://services.arcgisonline.com/ArcGIS/rest/services/Ocean_Basemap/MapServer"
                                        }
                                    ]
                                }
                                // TODO: Add in OpenStreetMaps
                        ]
                    },
                    {
                        visible: true,
                        displayName: "Identify",
                        id: "identify",
                        area: "toolsMenu",
                        target: "toolContainer",
                        widgetOptions: {
                            mapServiceLabelText: "Map Services",
                            subLayerLabelText: "Sub-Layer",
                            polygonSymbolJson: { "color": [255, 0, 0, 100], "outline": { "color": [0, 0, 0, 255], "width": 1.5, "type": "esriSLS", "style": "esriSLSSolid" }, "type": "esriSFS", "style": "esriSFSSolid" },
                            bufferSymbolJson: { "color": [0, 0, 255, 100], "outline": { "color": [0, 0, 255, 255], "width": 2, "type": "esriSLS", "style": "esriSLSSolid" }, "type": "esriSFS", "style": "esriSFSSolid" }
                        }
                    },
                    {
                        visible: true,
                        displayName: "Map Manager",
                        id: "mapManager",
                        area: {
                            name: "mainMenu",
                            options: { iconClass: "icon-screenshot"}
                        },
                        target:{
                            type: "dialog",
                            options: { "class": "nonModal" }
                        },
                        widgetOptions: { showArcGISOnline: false }
                    },
                    {
                        visible: true,
                        displayName: "Map Layers",
                        id: "toc",
                        ctr: Toc,
                        widgetOptions: {
                            useSpriteImage: false,
                            // Support Feature Layers
                            enableFeatureLayers: true,
                            // Support ImageService Layers
                            enableImageServiceLayers: true,
                            // Support GeoRSS Layers
                            enableGeoRSSLayers: true,
                            showTiledServices: false
                        },
                        area: {
                            name: "leftRegion",
                            options: { iconClass: "icon-screenshot" }
                        },
                        loadPoint: "mapOnLoad"
                    },
                    {
                        displayName: "Markup",
                        id: "markup",
                        visible: true,
                        ctr: Markup,
                        area: { 
                            name: "toolsMenu",
                            options: { iconClass: "icon-screenshot" }
                        },
                        target: "toolContainer",
                        widgetOptions: { }
                    },
                    {
                        displayName: "Attribute Search",
                        id: "attributeSearch",
                        visible: true,
                        ctr: AttributeSearch,
                        widgetOptions: {},
                        area: { 
                            name: "toolsMenu",
                            options: "icon-screenshot"
                        },
                        target: {
                            type: "dialog",
                            options: { "class": "nonModal", style: "width:850px;" }
                        }
                    },
                    {
                        displayName: "Swipe",
                        id: "swipe",
                        visible: true,
                        ctr: Swipe,
                        widgetOptions: {},
                        area: { 
                            name: "toolsMenu",
                            options: { iconClass: "icon-screenshot" }
                        },
                        target: "toolContainer"
                    },
                    {
                        displayName: "Measure",
                        id: "measure",
                        visible: true,
                        ctr: Measure,
                        widgetOptions: {},
                        area: {
                            name: "toolsMenu",
                            options: { iconClass: "icon-screenshot" }
                        },
                        target: "toolContainer"
                    },
                    {
                        displayName: "Zoom to XY",
                        id: "zoomToXY",
                        visible: true,
                        widgetOptions: {},
                        area: {
                            name: "toolsMenu",
                            options: {
                                iconClass: "icon-screenshot"
                            }
                        },
                        target: {
                            type: "dialog",
                            options: { "class": "nonModal"}
                        }
                    },
                    {
                        visible: true,
                        displayName: "Coordinate Display",
                        id: "coordinateDisplay",
                        widgetOptions: {},
                        area: "bottomMapRegion",
                        targetNode: "coordinateDisplayNode"
                    },
                    {
                        visible: true,
                        displayName: "Share",
                        id: "mapLink",
                        widgetOptions: {
                            enableShortUrls: true,
                            autoUpdateLink: true
                        },
                        area: {
                            name: "mainMenu"
                        },
                        target: {
                            type: "dialog",
                            options: {
                                title: "Create Map Link",
                                "class": "nonModal"
                            }
                        }
                    },
                    {
                        visible: true,
                        displayName: "Scale Bar",
                        id: "scaleBar",
                        widgetOptions: {
                            // "dual" displays both miles and kilmometers
                            // "english" is the default, which displays miles
                            // "metric" for kilometers
                            scalebarUnit: "english"
                        },
                        area: "bottomMapRegion",
                        ctr: Scalebar,
                        targetNode: "scalebarNode"
                    },
                    {
                        displayName: "Results Console",
                        id: "results",
                        area: "bottomRegion",
                        visible: true,
                        ctr: ResultsConsole,
                        widgetOptions: {
                            polygonHighlightSymbolJson: { "color": [255, 255, 0, 100], "outline": { "color": [0, 0, 255, 100], "width": 2, "type": "esriSLS", "style": "esriSLSSolid" }, "type": "esriSFS", "style": "esriSFSSolid" },
                            pointHighlightSymbolJson: { "angle": 0, "xoffset": -0.75, "yoffset": 11.25, "type": "esriPMS", "url": require.toUrl("./images/green.png"), "width": 14.25, "height": 24 },
                            lineHighlightSymbolJson: { "color": [0, 0, 255, 255], "width": 2.25, "type": "esriSLS", "style": "esriSLSSolid" }
                        }
                    },
                    {
                        visible: true,
                        displayName: "View In",
                        id: "viewIn",
                        widgetOptions: {},
                        area: {
                            name: "toolsMenu",
                            options: "icon-eye-open"
                        },
                        target: {
                            type: "subMenu",
                            options: {
                                iconClass: "icon-eye-open"
                            }
                        }
                    }
                ],
                clientWidgets: []
            };

            lang.mixin(this.settings, defaultSettings);

            // mixin in user settings
            if (params.userSettings) {
                lang.mixinMissing(this.settings, params.userSettings);
            }

            esriConfig.defaults.io.proxyUrl = "http://localhost:51431/Source/proxy.ashx";
            this._processGetParams();
            this._loadFromLocalStorage();
            this._processSettings();
        },
        startup: function () {
            var self = this;

            // TODO: THIS IS A TEMPORARY STATEMENT
            config.defaults.services.geometry = "http://vm101.worldviewsolutions.net:6080/arcgis/rest/services/Utilities/Geometry/GeometryServer";

            this.store = new SearchResultsStore();
            config.defaults.searchResultStore = this.store;

            this.widgets = new Memory({ data: this.settings.widgets.concat(this.settings.clientWidgets) });
            this.renderAreas();

            // Attach child widgets to the layout
            if (this._componentsVisible(["mapToolbar"])) {
                this.topMapRegion = new ContentPane({
                    splitter: false,
                    region: 'top',
                    style: "padding:0px"
                }, this.topMapRegion);

                this._createMainMenuBar();

                this.topMapRegion.startup();
            }

            this.mapRegion = new BorderContainer({
                design: 'headline',
                region: 'center',
                gutters: false
            }, this.mapRegion);
            this.mapRegion.startup();

            // The map should always exist
            var mapContentPane = new ContentPane({
                region: 'center',
                gutters: false,
                style: "padding: 0px"
            }, this.mapNode);
            mapContentPane.startup();

            if (this._componentsVisible(["basemapGallery"])) {
                this.basemapGalleryTitlePane = new TitlePane({
                    title: 'Switch Basemap',
                    closable: false,
                    open: false,
                    toggleable: true
                }, this.basemapGalleryTitlePane);

                // Close the basemap gallery on map click if it's open
                aspect.after(this.basemapGalleryTitlePane, "toggle", function () {
                    if (self.basemapGalleryTitlePane.open) {
                        var hideTitlePane = function () {
                            if (self.basemapGalleryTitlePane.open) {
                                self.basemapGalleryTitlePane.toggle();
                            }
                        };
                        var mouseDown = on.once(map, "mouse-down", function (evt) { hideTitlePane(); });
                        var mouseOver = on.once(map, "mouse-over", function (evt) { hideTitlePane(); });
                    }
                });

                this.basemapGalleryContentPane = new ContentPane({
                }, this.basemapGalleryContentPane);

            }

            if (this._componentsVisible(["coordinateDisplay", "scaleBar"], true)) {
                this.bottomMapRegion = new ContentPane({
                    region: 'bottom'
                }, this.bottomMapRegion);
                this.bottomMapRegion.startup();
            }

            if (this._componentsVisible(["resultsConsole"])) {
                this.bottomPane = new ContentPane({
                    title: 'Results',
                    splitter: true,
                    region: 'bottom'
                }, this.bottomPane);

                this.bottomPane.startup();
            }

            this.inherited(arguments);

            // create the map
            map = new Map(this.mapNode.id, this.settings.map.widgetOptions);

            // make the map public
            this.map = map;

            this.homeButton = new HomeButton({
                map: map
            }, this.mapHomeButton);
            this.homeButton.startup();


            // Create the map widgets

            if (this._componentsVisible(["basemapGallery"])) {
                var basemapGallery = new BasemapGallery(lang.mixin({ map: map }, this.settings.basemapGallery.widgetOptions), this.basemapGalleryNode);
                // We need to actually set the basemap when this event occurs because for some reason the gallery doesn't change the value of the map's basemap......
                this.own(on(basemapGallery, "selection-change", lang.hitch(this, function () {
                    self.basemapGalleryTitlePane.titleNode.innerHTML = basemapGallery.getSelected().title;
                    this.map.setBasemap(basemapGallery.getSelected());
                })));
                basemapGallery.startup();

                self.basemapGalleryTitlePane.titleNode.innerHTML = this.map.getBasemap();
            }

            if (this._componentsVisible(["markup"])) {
                this.markupLayer = new MarkupLayer({ id: "Temporary Markup" });
            }

            if (this._componentsVisible(["overviewMap"])) {
                var overviewMapDijit = new OverviewMap(lang.mixin({ map: map }, this.settings.overviewMap.widgetOptions));
                overviewMapDijit.startup();
            }

            if (this._componentsVisible(["scaleBar"])) {
                var scalebar = new Scalebar(lang.mixin({ map: map }, this.settings.scaleBar.widgetOptions), this.scalebarNode);
            }

            this._loadLayers();

            map.addLayers(this.settings.map.initialLayers);

            // TODO: Get rid of tool container
            this.toolContainer = new ToolContainer({
                map: map,
                mapTool: "Identify"
            }, this.mapToolContainerNode);
            this.toolContainer.startup();

            // Disable the coordinate display if a tool is activated
            if (this._componentsVisible(["coordinateDisplay"])) {
                on(this.toolContainer, "tool-activated", lang.hitch(this, function () {
                    this.coordinateDisplay.disableCoordinateDisplay();
                }));
                on(this.toolContainer, "tool-deactivated", lang.hitch(this, function () {
                    this.coordinateDisplay.enableCoordinateDisplay();
                }));
            }


            on(map, "load", lang.hitch(this, function (evt) {
                if (this._componentsVisible(["resultsConsole"])) {
                    this.results = new ResultsConsole(lang.mixin({ map: map, store: this.store }, this.settings.resultsConsole.widgetOptions), this.resultsConsole);

                    // Open the results pane when the results console has a search result
                    on(this.results, "result-found", lang.hitch(this, function () {
                        var bottomSplitter = this.getSplitter("bottom");
                        if (bottomSplitter.get("state") !== "full") {
                            bottomSplitter._toggle();
                        }
                    }));
                    this.results.startup();
                }

                // create the CoordinateDisplay
                if (this._componentsVisible(["coordinateDisplay"])) {
                    this.coordinateDisplay = new CoordinateDisplay(lang.mixin({ map: map }, this.settings.coordinateDisplay.widgetOptions), this.coordinateDisplayNode);
                    this.coordinateDisplay.startup();
                }

                if (this._componentsVisible(["toc"])) {
                    var cp = new ContentPane({
                        title: "Map Layers"
                    });
                    this.toc = new Toc(lang.mixin({ map: map }, this.settings.toc.widgetOptions));
                    cp.addChild(this.toc);
                    this.leftRegion.addChild(cp);
                    this.toc.startup();
                }

                // Enable client widgets

                array.forEach(this.settings.clientWidgets, function (clientWidget) {
                    if (clientWidget.visible) {
                        if (clientWidget.region === "left") {
                            var cp = new ContentPane({
                                title: clientWidget.displayName
                            });
                            var widget = new clientWidget.ctr(lang.mixin({ map: map }, clientWidget.widgetOptions));
                            this[clientWidget.name] = widget;
                            cp.addChild(widget);
                            this.leftRegion.addChild(cp);
                            widget.startup();
                        }
                    }
                }, this);
            }));

            this.minimize();
        },
        renderArea: function(area){
            // Call the area's load function
            area.load();

            var children = this.widgets.query({ area: area.name });
            array.forEach(children, function (child) {
                this.renderArea(child);
            }, this);
        },
        renderAreas: function(){
            var areas = new Memory({ data: this.settings.areas});

            var rootAreas = area.query({ parent: "" });
            array.forEach(rootAreas, function (area) {
                this.renderArea(area);
            }, this);
        },
        _mixinDefaultWidgetParams: function(paramsObject){
            return lang.mixin({ map: this.map, store: this.store}, paramsObject);
        },
        _renderLeftRegion: function () {
            var childWidgets = this.widgets.query({ area: "leftRegion" });
            if (childWidgets.length == 0) {
                return;
            }
            else if (childWidgets.length == 1) {
                this.leftRegion = new ContentPane({
                    region: 'left',
                    splitter: true,
                    style: "width: 275px;"
                }, this.leftRegion);
                this.leftRegion.startup();
                var instance = new childWidget.ctr(this._mixinDefaultWidgetParams(childWidget.widgetOptions));
                this[childWidget.id] = instance;
                cp.addChild(instance);
                instance.startup();
                this.leftRegion.addChild(cp);
            }
            else if (childWidgets.length > 1) {
                this.leftRegion = new TabContainer({
                    region: 'left',
                    splitter: true,
                    style: "width: 275px;"
                }, this.leftRegion);
                this.leftRegion.startup();
                array.forEach(childWidgets, function (childWidget) {
                    var cp = new ContentPane({ title: childWidget.displayName });
                    var instance = new childWidget.ctr(this._mixinDefaultWidgetParams(childWidget.widgetOptions));
                    this[childWidget.id] = instance;
                    cp.addChild(instance);
                    instance.startup();
                    this.leftRegion.addChild(cp);
                }, this);
            }


        },
        loadWidget: function(widgetObject){

        },
        _setDefaultEvents: function () {
            var toggleFullscreen = function (evt) {
                this.fullscreen = !this.fullscreen;
                if (!this.fullscreen) {
                    this.minimize();
                }
                else {
                    this.maximize();
                }
            };
            this.own(on(this.fullScreenButton, "click", lang.hitch(this, toggleFullscreen)));
        },
        _processSettings: function () {
            var settings = this.settings;

            // Set up map, layers, filters, etc.
            if (!settings.map.initialExtent && !this.getParams) {
                throw new Error("No initial extent or get parameters provided for MapViewer");
            }

            if (this.getParams) {
                settings.map.widgetOptions.center = [parseFloat(this.getParams.lon), parseFloat(this.getParams.lat)];
                settings.map.widgetOptions.scale = parseFloat(this.getParams.s);
            }
            else if (this.savedMap) {
                settings.map.widgetOptions.extent = new Extent(this.savedMap.map.extent);
            }
            else {
                settings.map.widgetOptions.extent = new Extent(settings.map.initialExtent);
            }

            // prepare basemaps and basemap gallery widget
            if (settings.basemapGallery.visible) {
                var basemaps = [],
                    userBasemaps = [];


                if (!settings.basemapGallery.widgetOptions.showArcGISBasemaps && settings.basemapGallery.esriBasemaps.length > 0) {
                    basemaps = basemaps.concat(this._processBasemapForGallery(settings.basemapGallery.esriBasemaps));
                }
                if (settings.globalOptions.basemaps.length > 0) {
                    var userBasemaps = this._processBasemapForGallery(settings.globalOptions.basemaps);
                    basemaps = basemaps.concat(userBasemaps);
                }

                settings.basemapGallery.widgetOptions.basemaps = basemaps;
            }

            var setBasemapFromString = function (string) {
                for (var i = 0; i < settings.basemapGallery.widgetOptions.basemaps.length; i++) {
                    var basemap = settings.basemapGallery.widgetOptions.basemaps[i];
                    if (basemap.title === string) {
                        settings.map.widgetOptions.basemap = basemap;
                    }
                }
            };

            // Map Link basemap -> First user-defined basemap -> default basemap
            if (this.getParams) {
                setBasemapFromString(this.getParams.b);
            }
            else if (this.savedMap) {
                setBasemapFromString(this.savedMap.map.basemap);
            }
            else if (userBasemaps.length > 0) {
                // Take the first defined non-esri basemap if this didn't come from a links
                settings.map.widgetOptions.basemap = userBasemaps[0];
            }
        },
        _loadLayers: function () {
            InfoTemplateManager.setMap(this.map);

            // set up layers
            var layersToAdd = [];

            // We will always want our default layers
            array.forEach(this.settings.globalOptions.layers, function (layer) {
                var instance = this._createLayerFromSettingsObject(layer);
                layersToAdd.push(instance);
            }, this);

            // TODO: Load layers from localStorage/server, overriding duplicates

            if (this.savedMap) {
                array.forEach(this.savedMap.layers, function (layer) {
                    if (layer.type === "MarkupLayer") {
                        var instance = this._createLayerFromSettingsObject(layer);
                        layersToAdd.push(instance);
                    }
                }, this);
            }

            this.settings.map.initialLayers = layersToAdd;
        },
        _processBasemapForGallery: function (basemapArray) {
            var basemaps = [];
            array.forEach(basemapArray, function (basemap) {
                var basemapLayers = []
                array.forEach(basemap.layers, function (basemapLayerOptions) {
                    basemapLayers.push(new BasemapLayer(basemapLayerOptions));
                });
                var basemapOptions = lang.mixin({ layers: basemapLayers }, basemap.options);
                basemaps.push(new Basemap(basemapOptions));
            });

            return basemaps;
        },
        _createLayerFromSettingsObject: function (layerObject) {
            var layer = null;

            switch (layerObject.type) {
                case "ArcGISDynamicMapServiceLayer":
                    layer = new ArcGISDynamicMapServiceLayer(layerObject.url, layerObject.options);
                    break;
                case "FeatureLayer":
                    layer = new FeatureLayer(layerObject.url, layerObject.options);
                    break;
                case "GraphicsLayer":
                    layer = new GraphicsLayer(layerObject.options);
                    break;
                case "GeoRSSLayer":
                    layer = new GeoRSSLayer(layerObject.url, layerObject.options);
                    break;
                case "ArcGISImageServiceLayer":
                    layer = new ArcGISImageServiceLayer(layerObject.url, layerObject.options);
                    break;
                case "MarkupLayer":
                    layer = new MarkupLayer(layerObject.options);
                    array.forEach(layerObject.graphics, function (graphicJson) {
                        var graphic = new Graphic(graphicJson);
                        graphic._type = graphicJson._type;
                        graphic.setEditing(true);
                        graphic.setInfoTemplate(InfoTemplateManager.getTabularAttributeTemplate({ createWidgetsFromGeometry: true }));
                        layer.add(graphic);
                    }, this);
                    break;
                default:
                    throw new Error("an unknown or unsupported type was added to the map");
                    break;
            }

            return layer;
        },
        _processGetParams: function () {
            var uri = window.document.location.search;
            var queryObject = ioQuery.queryToObject(uri.substring(uri.indexOf("?") + 1, uri.length));
            if (queryObject.b && queryObject.lat && queryObject.lon && queryObject.s) {
                queryObject.services = JSON.parse(queryObject.sv);
                this.getParams = queryObject;
            }
            else {
                this.getParams = null;
            }
        },
        _loadFromLocalStorage: function () {
            if (!has("local-storage")) {
                return;
            }
            else {
                if (!localStorage.getItem("m4")) {
                    this.savedMap = null;
                }
                else {
                    var previousMapInstance = JSON.parse(localStorage.getItem("m4"));
                    previousMapInstance.map.extent = new Extent(previousMapInstance.map.extent);
                    this.savedMap = previousMapInstance;
                }
                if (this._componentsVisible(["bookmarks"]) && localStorage.getItem("bookmarks")) {
                    this.settings.bookmarks.savedBookmarks = JSON.parse(localStorage.getItem("bookmarks"));
                }
            }
        },
        _createMainMenuBar: function () {
            var self = this;
            this.mainMenu = new MenuBar({
                region: 'top'
            }, this.mainMenu);

            var mapRegionToolsMenu = new DropDownMenu({});
            

            if (this._componentsVisible(["attributeSearch"])) {
                // Attribute Search Menu Item
                mapRegionToolsMenu.addChild(new MenuItem({
                    label: "Attribute Search",
                    iconClass: 'icon-search',
                    onClick: lang.hitch(this, function () {
                        Common.showWidgetInDialog(AttributeSearch, lang.mixin({ map: this.map, store: this.store }, this.settings.attributeSearch.widgetOptions), { title: "Attribute Search", "class": "nonModal", style: "width:850px;" });
                    })
                }));
            }

            if (this._componentsVisible(["identify"])) {
                // Identify Menu Item
                mapRegionToolsMenu.addChild(new MenuItem({
                    label: "Identify",
                    iconClass: 'icon-info',
                    onClick: lang.hitch(this, this._identifyTool)
                }));
            }

            if (this._componentsVisible(["markup"])) {
                // Markup Menu Item
                mapRegionToolsMenu.addChild(new MenuItem({
                    label: "Markup",
                    iconClass: 'icon-pencil',
                    onClick: lang.hitch(this, this._showMarkupTool)
                }));
            }

            if (this._componentsVisible(["measure"])) {
                // Measure Menu Item
                mapRegionToolsMenu.addChild(new MenuItem({
                    label: "Measure",
                    iconClass: 'icon-resize-full',
                    onClick: lang.hitch(this, this._measureTool)
                }));
            }

            if (this._componentsVisible(["swipe"])) {
                // Swipe Menu Item
                mapRegionToolsMenu.addChild(new MenuItem({
                    label: "Swipe",
                    iconClass: 'icon-move',
                    onClick: lang.hitch(this, this._swipeTool)
                }));
            }


            if (this._componentsVisible(["zoomToXY"])) {
                // Zoom To XY Menu Item
                mapRegionToolsMenu.addChild(new MenuItem({
                    label: "Zoom To XY",
                    iconClass: 'icon-screenshot',
                    onClick: lang.hitch(this, function () {
                        Common.showWidgetInDialog(ZoomToXY, lang.mixin({ map: this.map }, this.settings.zoomToXY.widgetOptions), { title: "Zoom to XY", "class": "nonModal" });
                    })
                }));
            }


            var mapRegionToolsPopupMenuBarItem = new PopupMenuBarItem({ label: "Tools", popup: mapRegionToolsMenu });

            this.mapRegionMenuBar.addChild(mapRegionToolsPopupMenuBarItem);

            if (this._componentsVisible(["bookmarks"])) {
                var bookmarks = this.settings.bookmarks.savedBookmarks,
                    bookmarksMenu = new DropDownMenu({ _bIndex: 0 });

                var addBookmarkMenuItemsFromArray = lang.hitch(this, function (bookmarkArray) {
                    array.forEach(bookmarkArray, function (bookmark) {
                        bookmarksMenu.addChild(new MenuItem({
                            label: bookmark.name,
                            onClick: lang.hitch(this, function () {
                                this.map.setExtent(new Extent(bookmark.extent));
                            }),
                            _bookmark: bookmark
                        }), bookmarksMenu._bIndex);
                        bookmarksMenu._bIndex = bookmarksMenu._bIndex + 1;
                    }, this);
                });

                // Create Bookmarks Menu
                addBookmarkMenuItemsFromArray(bookmarks, bookmarksMenu);

                this.bookmarksMenu = bookmarksMenu;

                bookmarksMenu.addChild(new MenuSeparator({}));
                bookmarksMenu.addChild(new MenuItem({
                    label: "Manage Bookmarks",
                    onClick: lang.hitch(this, function () {
                        var dialog = Common.showWidgetInDialog(BookmarkManager, { map: map, bookmarks: bookmarks }, { title: "Bookmark Manager", "class": "nonModal" });
                        var bookmarkCreated = on(dialog.content, "bookmark-created", lang.hitch(this, function (bookmark) {
                            addBookmarkMenuItemsFromArray([bookmark]);
                            this.saveBookmarks();
                        }));
                        var bookmarkUpdated = on(dialog.content, "bookmark-updated", lang.hitch(this, function (bookmark) {
                            var bookmarkMenuItems = this.bookmarksMenu.getChildren();
                            array.forEach(bookmarkMenuItems, function (bookmarkMenuItem) {
                                if (bookmarkMenuItem._bookmark === bookmark) {
                                    bookmarkMenuItem.set("label", bookmark.name);
                                }
                            });
                            this.saveBookmarks();
                        }));
                        var bookmarkDeleted = on(dialog.content, "bookmark-deleted", lang.hitch(this, function (bookmark) {
                            var bookmarkMenuItems = this.bookmarksMenu.getChildren();
                            array.forEach(bookmarkMenuItems, function (bookmarkMenuItem) {
                                if (bookmarkMenuItem._bookmark === bookmark) {
                                    this.bookmarksMenu.removeChild(bookmarkMenuItem);
                                    this.bookmarksMenu._bIndex = this.bookmarksMenu._bIndex - 1;
                                }
                            }, this);
                            this.saveBookmarks();
                        }));
                    })
                }));
                var bookmarksPopupMenuItem = new PopupMenuBarItem({ label: "Bookmarks", popup: bookmarksMenu });

                this.mapRegionMenuBar.addChild(bookmarksPopupMenuItem);
            }

            // MapManager button
            if (this._componentsVisible(["mapManager"])) {
                this.mapRegionMenuBar.addChild(new MenuBarItem({
                    label: "Map Manager",
                    onClick: lang.hitch(this, this._showMapManager)
                }));
            }

            this.mapRegionMenuBar.startup();
        },
        maximize: function () {
            var layout = this.settings.globalOptions.layout.maximized;
            if (this.settings.toc.visible) {
                var displayProp = layout.leftPane ? "block" : "none";
                var leftSplitter = this.getSplitter("left");
                //if (!layout.leftPane && leftSplitter.get("state") === "full") {
                //    leftSplitter._toggle();
                //}
                domStyle.set(leftSplitter.child.domNode, { display: displayProp });
                domStyle.set(leftSplitter.domNode, { display: displayProp });
            }
            if (this.settings.resultsConsole.visible) {
                var displayProp = layout.resultsPane ? "block" : "none";
                var bottomSplitter = this.getSplitter("bottom");
                //if (!layout.resultsPane && bottomSplitter.get("state") === "full") {
                //    bottomSplitter._toggle();
                //}
                domStyle.set(bottomSplitter.child.domNode, { display: displayProp });
                domStyle.set(bottomSplitter.domNode, { display: displayProp });
            }
            if (this.settings.mapToolbar.visible) {
                var displayProp = layout.mapToolbar ? "block" : "none";
                domStyle.set(this.topMapRegion.domNode, { display: displayProp });
            }
            this.resize();

            domClass.add(this.fullScreenButton, "maximized");
            this.emit("maximized", this);
        },
        minimize: function () {
            var layout = this.settings.globalOptions.layout.minimized;
            if (this.settings.toc.visible) {
                var displayProp = layout.leftPane ? "block" : "none";
                var leftSplitter = this.getSplitter("left");
                //if (!layout.leftPane && leftSplitter.get("state") === "full") {
                //    leftSplitter._toggle();
                //}
                domStyle.set(leftSplitter.child.domNode, { display: displayProp });
                domStyle.set(leftSplitter.domNode, { display: displayProp });
            }
            if (this.settings.resultsConsole.visible) {
                var displayProp = layout.resultsPane ? "block" : "none";
                var bottomSplitter = this.getSplitter("bottom");
                //if (!layout.resultsPane && bottomSplitter.get("state") === "full") {
                //    bottomSplitter._toggle();
                //}
                domStyle.set(bottomSplitter.child.domNode, { display: displayProp });
                domStyle.set(bottomSplitter.domNode, { display: displayProp });
            }
            if (this.settings.mapToolbar.visible) {
                var displayProp = layout.mapToolbar ? "block" : "none";
                domStyle.set(this.topMapRegion.domNode, { display: displayProp });
            }
            this.resize();
            domClass.remove(this.fullScreenButton, "maximized");
            this.emit("minimized", this);
        },
        save: function () {
            if (!has("local-storage")) {
                return;
            }

            var mapState = new MapState(this.map);

            var mapExport = mapState.toJson();

            localStorage.setItem("m4", JSON.stringify(mapExport));
        },
        saveBookmarks: function () {
            if (has("local-storage") && this._componentsVisible(["bookmarks"])) {
                var bookmarks = this.settings.bookmarks.savedBookmarks.slice();
                localStorage.setItem("bookmarks", JSON.stringify(bookmarks));
            }
        },
        _identifyTool: function () {
            this.toolContainer.activateTool('Identify', lang.mixin({ store: this.store }, this.settings.identify.widgetOptions));
        },
        _measureTool: function () {
            this.toolContainer.activateTool('Measure', this.settings.identify.widgetOptions);
        },
        _showMapManager: function () {
            var vs = win.getBox();
            var w = vs.w * .9;
            var h = vs.h * .75;
            var dialog = Common.showWidgetInDialog(MapManager, lang.mixin({
                map: map,
                design: "headline",
                gutters: false,
                style: "width:" + w + "px;height:" + h + "px;"
            }, this.settings.mapManager.widgetOptions), { title: "Map Manager", "class": "nonModal" }, true);
        },
        _swipeTool: function () {
            this.toolContainer.activateTool('Swipe', lang.mixin({ basemapLayers: this.settings.basemapGallery.widgetOptions.basemaps }, this.settings.swipe.widgetOptions));
        },
        _showMarkupTool: function () {
            this.toolContainer.activateTool('Markup', lang.mixin({ markupLayer: this.markupLayer }, this.settings.markup.widgetOptions));
        }
    });
});