﻿//dojo.require("dojo.fx.easing");
//dojo.require("dojox.charting.Chart2D");
//dojo.require("dojox.charting.themes.ThreeD");
//dojo.require("dijit.Tooltip");
//dojo.require("dijit.Dialog");

//dojo.require("esri.map");
//dojo.require("esri.dijit.Measurement");

//dojo.require("dojo.i18n");
//dojo.requireLocalization('studioat', 'template');

define([
    "dojo/_base/declare"
    , "dojo/_base/lang"
    , "dojo/_base/array"
    , "dojo/_base/Color"
    , "dojo/_base/event"
    , "dojo/dom-construct"
    , "dojo/dom-style"
    , "dojo/fx/easing"
    , "dojox/charting/Chart2D"
    , "dojox/charting/themes/ThreeD"
    , "dijit/Tooltip"
    , "dijit/Dialog"
    , "esri/map"
    , "esri/dijit/Measurement"
    , "dojo/_base/json"
    , "dojo/Deferred"
    , "esri/request"
    , "esri/geometry/jsonUtils"
    , "esri/geometry/Polyline"
    , "dojo/on"
    , "esri/geometry/webMercatorUtils"
    , "esri/geometry/Point"
    , "dojo/number"
    , "esri/units"
    , "esri/symbols/SimpleLineSymbol"
    , "esri/symbols/SimpleMarkerSymbol"
    , "esri/graphic"
    , "esri/geometry/geodesicUtils"
    , "dojo/dom-geometry"
    , "dojox/gfx"
    , "dojo/i18n!wvs/libs/nls/soeTemplate"
    , "dojo/domReady!"
], function (
    declare
    , lang
    , array
    , Color
    , event
    , domConstruct
    , domStyle
    , easing
    , Chart2D
    , ThreeD
    , Tooltip
    , Dialog
    , Map
    , Measurement
    , dojo
    , Deferred
    , esriRequest
    , jsonUtils
    , Polyline
    , on
    , webMercatorUtils
    , Point
    , number
    , units
    , SimpleLineSymbol
    , SimpleMarkerSymbol
    , Graphic
    , geodesicUtils
    , domGeom
    , gfx
    , i18n
){
    return declare([], {

        // SOE PROPERTIES //
        _isValid: null,
        name: null,
        mapServiceUrl: null,
        resourceIndex: null,

        // PROFILE CHART //
        _profileChartInfo: null,

        // IS DEBUG //
        _isDebug: null,
        _inputsNode: null,
        _outputsNode: null,
        unitsSlope: {
            PERCENT: 'PERCENT',
            DEGREES: 'DEGREES',
            RADIANS: 'RADIANS'
        },

        unitsAspect: {
            DEGREES: 'DEGREES',
            RADIANS: 'RADIANS'
        },
        planeReferenceType: {
            esriPlaneReferenceAbove: 'esriPlaneReferenceAbove',
            esriPlaneReferenceBelow: 'esriPlaneReferenceBelow'
        },

        /**
            * studioat.SurfaceSOEUtils constructor
            *
            * @constructor
            */
        constructor: function () {
            this._isValid = false;
            this._isDebug = false;

            // GET i18n MESSAGES //
            this.messages = i18n;

            // PROVIDE PROPER CONTEXT FOR METHODS //
            this._invokeOperation = lang.hitch(this, this._invokeOperation);
            this.getElevationAtLonLat = lang.hitch(this, this.getElevationAtLonLat);
            this.getElevations = lang.hitch(this, this.getElevations);
            this.getElevationData = lang.hitch(this, this.getElevationData);
            this.getLineOfSight = lang.hitch(this, this.getLineOfSight);

            this._initProfileChart = lang.hitch(this, this._initProfileChart);
            this.clearProfileChart = lang.hitch(this, this.clearProfileChart);
            this.createProfileChart = lang.hitch(this, this.createProfileChart);
            this._createProfileChart = lang.hitch(this, this._createProfileChart);

            this._getElevationLabel = lang.hitch(this, this._getElevationLabel);
            this._getDistanceLabel = lang.hitch(this, this._getDistanceLabel);
            this._getDisplayLabel = lang.hitch(this, this._getDisplayLabel);
            this._getDisplayValue = lang.hitch(this, this._getDisplayValue);
            this._getDisplayUnits = lang.hitch(this, this._getDisplayUnits);
            this._displayChartLocation = lang.hitch(this, this._displayChartLocation);
        },

        /**
            *
            * @param soeInfo Object SOE INFO
            * @param profileChartParams Object PROFILE CHART PARAMS
            * @return {dojo.Deferred}
            */
        init: function (soeInfo, profileChartParams) {
            var deferred = new Deferred();

            // SET SOE PROPERTIES //
            if (soeInfo) {
                lang.mixin(this, soeInfo);
            } else {
                throw new Error("Missing 'soeInfo' parameter.");
            }

            // GET SOE SCHEMA //
            esriRequest({
                url: lang.replace("{mapServiceUrl}/exts/{name}", this),
                content: { f: 'schema' },
                callbackParamName: "callback"
            }, {
                disableIdentityLookup: true
            }).then(lang.hitch(this, function (schema) {
                this._isValid = true;
                var hasInfoResource = array.some(schema.resources, function (resource) {
                    return (resource.name.toLowerCase() === 'info');
                });
                if (hasInfoResource) {
                    // GET INFO //
                    esriRequest({
                        url: lang.replace("{mapServiceUrl}/exts/{name}/Info", this),
                        content: { f: 'json' },
                        callbackParamName: "callback"
                    }, {
                        disableIdentityLookup: true
                    }).then(lang.hitch(this, function (response) {
                        // SET INFO //
                        lang.mixin(this, response);
                        this._invokeOperation('', {}).then(lang.hitch(this, function (resourceInfo) {
                            this.resourceInfo = lang.mixin(resourceInfo, this.resourceInfo || {});
                            this.resourceInfo.snippet = this.getDataSourceInfo(250);
                            if (profileChartParams) {
                                this._initProfileChart(profileChartParams);
                            }
                            deferred.resolve();
                            console.log(lang.replace("Surface Utility version {soeVersion} initialized.", this));
                        }), deferred.reject);
                    }));
                } else {
                    deferred.reject(new Error("The version of the Surface Utility you are trying to connect to is not supported."));
                }
            }), lang.hitch(this, function (error) {
                deferred.reject(this.messages.errors.InvalidConfiguration);
                console.warn(this.messages.errors.InvalidConfiguration, this);
            }));

            return deferred;
        },

        /**
            *
            * @param soeInfo
            * @param profileChartParams
            * @param inputsNode
            * @param outputsNode
            * @return {dojo.Deferred}
            */
        initDebug: function (soeInfo, profileChartParams, inputsNode, outputsNode) {
            var deferred = new Deferred();
            this._isDebug = true;
            this._inputsNode = inputsNode;
            this._outputsNode = outputsNode;
            dojo.toJsonIndentStr = "  ";
            this.init(soeInfo, profileChartParams).then(lang.hitch(this, function () {
                this._debugInputMessage(this.toString());
                deferred.resolve();
            }), lang.hitch(this, function (error) {
                this._debugInputMessage(dojo.toJson(error, true));
                deferred.reject(error);
            }));
            return deferred;
        },

        /**
            *
            * @param msg
            * @private
            */
        _debugInputMessage: function (msg) {
            domConstruct.create('pre', { innerHTML: msg }, this._inputsNode, 'only');
        },

        /**
            *
            * @param msg
            * @private
            */
        _debugOutputMessage: function (msg) {
            domConstruct.create('pre', { innerHTML: msg }, this._outputsNode, 'only');
        },

        /**
            *
            * @return {String}
            */
        getDataSourceInfo: function (maxChars) {
            var info = lang.replace("{name}: {description}", this.resourceInfo);
            var maxSnippetLength = maxChars || 75;
            if (info.length < maxSnippetLength) {
                return info;
            } else {
                var snippetIdx = info.indexOf(".");
                if ((snippetIdx > -1) && (snippetIdx < maxSnippetLength)) {
                    return info.substr(0, snippetIdx);
                } else {
                    return info.substr(0, maxSnippetLength) + "...";
                }
            }
        },

        /**
            *  MAKE ELEVATIONS SOE REQUEST
            *
            * @param operation
            * @param content
            * @return {dojo.Deferred}
            * @private
            */
        _invokeOperation: function (operation, content) {
            var deferred = new Deferred();
            if (this._isValid) {
                if (this._isDebug) {
                    this._debugInputMessage(dojo.toJson(content, true));
                }
                var operationUrl = lang.replace("{mapServiceUrl}/exts/{name}/SurfaceLayers/{resourceIndex}", this);
                if (operation && (operation.length > 0)) {
                    operationUrl += ("/" + operation);
                }
                // MAKE SOE REQUEST //
                esriRequest({
                    url: operationUrl,
                    content: lang.mixin({ f: 'json' }, content),
                    callbackParamName: "callback"
                }, {
                    disableIdentityLookup: true
                }).then(lang.hitch(this, function (response) {
                    if (this._isDebug) {
                        this._debugOutputMessage(dojo.toJson(response, true));
                    }
                    deferred.resolve(response);
                }), lang.hitch(this, function (error) {
                    if (this._isDebug) {
                        this._debugOutputMessage(dojo.toJson(error, true));
                    }
                    deferred.reject(error);
                }));
            } else {
                deferred.reject('Invalid SOE url or resource index.');
            }
            return deferred;
        },

        /**
            * GET ELEVATION AT LON/LAT
            *
            * @param {esri.geometry.Point} geoPoint
            */
        getElevationAtLonLat: function (geoPoint) {
            return this._invokeOperation('GetElevationAtLonLat', {
                'lon': geoPoint.x,
                'lat': geoPoint.y
            });
        },

        /**
            *
            * GET ELEVATIONS ALONG GEOMETRY
            * @param {esri.geometry.Geometry} geometry
            * @return {dojo.Deferred}
            */
        getElevations: function (geometry) {
            var deferred = new Deferred();

            this._invokeOperation('GetElevations', {
                'geometries': dojo.toJson([geometry.toJson()]),
                'simplify': true
            }).then(lang.hitch(this, function (response) {
                var elevGeom = jsonUtils.fromJson(response.geometries[0]);
                var elevationInfo = {
                    geometry: elevGeom,
                    data: this.getElevationsFromGeometry(elevGeom)
                };
                deferred.resolve(elevationInfo);
            }), deferred.reject);

            return deferred;
        },

        /**
            * GET ELEVATION DATA FOR EXTENT
            *
            * @param {esri.geometry.Extent} extent
            * @param {Number} rows
            * @param {Number} cols
            */
        getElevationData: function (extent, rows, cols) {
            return this._invokeOperation('GetElevationData', {
                'extent': dojo.toJson(extent.toJson()),
                'rows': rows,
                'columns': cols
            });
        },
        /**
            * @param {esri.geometry.Polyline} line
            * @param {Number} offsetObserver
            * @param {Number} offsetTarget
            * @param {bool} applyCurvature
            * @param {bool} applyRefraction
            * @param {Number} refractionFactor
            * @return {dojo.Deferred}
            */
        getLineOfSight: function (line, offsetObserver, offsetTarget, applyCurvature, applyRefraction, refractionFactor) {
            var deferred = new Deferred();

            this._invokeOperation('GetLineOfSight', {
                'geometry': dojo.toJson(line.toJson()),
                'offsetObserver': offsetObserver,
                'offsetTarget': offsetTarget,
                'applyCurvature': applyCurvature,
                'applyRefraction': applyRefraction,
                'refractionFactor': refractionFactor
            }).then(lang.hitch(this, function (response) {

                var pointObstruction = response.pointObstruction;
                if (pointObstruction != null) {
                    pointObstruction = jsonUtils.fromJson(response.pointObstruction);
                }

                var visibleLines = response.visibleLines;
                if (visibleLines != null) {
                    visibleLines = jsonUtils.fromJson(response.visibleLines);
                }

                var invisibleLines = response.invisibleLines;
                if (invisibleLines != null) {
                    invisibleLines = jsonUtils.fromJson(response.invisibleLines);
                }

                var lineOfSight = {
                    pointObstruction: pointObstruction,
                    visibleLines: visibleLines,
                    invisibleLines: invisibleLines,
                    isVisible: response.isVisible
                };
                deferred.resolve(lineOfSight);
            }), deferred.reject);

            return deferred;
        },
        /**
            * @param {esri.geometry.Point} point
            * @return {dojo.Deferred}
            */
        getSteepestPath: function (point) {
            var deferred = new Deferred();

            this._invokeOperation('GetSteepestPath', {
                'geometry': dojo.toJson(point.toJson())
            }).then(lang.hitch(this, function (response) {

                var geometry = response.geometry;
                if (geometry != null) {
                    geometry = jsonUtils.fromJson(response.geometry);
                }

                var steepestPath = {
                    geometry: geometry
                };
                deferred.resolve(steepestPath);
            }), deferred.reject);

            return deferred;
        },
        /**
            * @param {esri.geometry.Point} point
            * @return {dojo.Deferred}
            */
        getContour: function (point) {
            var deferred = new Deferred();

            this._invokeOperation('GetContour', {
                'geometry': dojo.toJson(point.toJson())
            }).then(lang.hitch(this, function (response) {

                var geometry = response.geometry;
                if (geometry != null) {
                    geometry = jsonUtils.fromJson(response.geometry);
                }

                var contour = {
                    geometry: geometry,
                    elevation: response.elevation
                };
                deferred.resolve(contour);
            }), deferred.reject);

            return deferred;
        },
        /**
            * @param {esri.geometry.Point} point
            * @param {studioat.SurfaceSOEUtils.unitsSlope} units
            * @return {dojo.Deferred}
            */
        getSlope: function (point, units) {
            var deferred = new Deferred();

            this._invokeOperation('GetSlope', {
                'geometry': dojo.toJson(point.toJson()), 'units': units
            }).then(lang.hitch(this, function (response) {

                var slope = {
                    slope: response.slope,
                    units: response.units
                };
                deferred.resolve(slope);
            }), deferred.reject);

            return deferred;
        },
        /**
            * @param {esri.geometry.Point} point
            * @param {studioat.SurfaceSOEUtils.unitsAspect} units
            * @return {dojo.Deferred}
            */
        getAspect: function (point, units) {
            var deferred = new Deferred();

            this._invokeOperation('GetAspect', {
                'geometry': dojo.toJson(point.toJson()), 'units': units
            }).then(lang.hitch(this, function (response) {

                var aspect = {
                    aspect: response.aspect,
                    units: response.units
                };
                deferred.resolve(aspect);
            }), deferred.reject);

            return deferred;
        },
        /**
            * @param {esri.geometry.Polyline} polyline
            * @param {Number} stepSize
            * @return {dojo.Deferred}
            */
        getSurfaceLength: function (polyline, stepSize) {
            var deferred = new Deferred();

            this._invokeOperation('GetSurfaceLength', {
                'geometry': dojo.toJson(polyline.toJson()), 'stepSize': stepSize
            }).then(lang.hitch(this, function (response) {

                var surfaceLength = {
                    surfaceLength: response.surfaceLength
                };
                deferred.resolve(surfaceLength);
            }), deferred.reject);

            return deferred;
        },
        /**
            * @param {esri.geometry.Point} point
            * @return {dojo.Deferred}
            */
        getNormal: function (point, units) {
            var deferred = new Deferred();

            this._invokeOperation('GetNormal', {
                'geometry': dojo.toJson(point.toJson())
            }).then(lang.hitch(this, function (response) {

                var geometry = response.geometry;
                if (geometry != null) {
                    geometry = jsonUtils.fromJson(response.geometry);
                }
                var normal = {
                    geometry: geometry,
                    vector3D: response.vector3D
                };
                deferred.resolve(normal);
            }), deferred.reject);

            return deferred;
        },
        /**
            * @param {esri.geometry.Polyline} line
            * @param {Number} offsetFromPoint
            * @param {Number} offsetToPoint
            * @param {Number} hint
            * @return {dojo.Deferred}
            */
        getLocate: function (line, offsetFromPoint, offsetToPoint, hint) {
            var deferred = new Deferred();

            this._invokeOperation('GetLocate', {
                'geometry': dojo.toJson(line.toJson()),
                'offsetFromPoint': offsetFromPoint,
                'offsetToPoint': offsetToPoint,
                'hint': hint
            }).then(lang.hitch(this, function (response) {

                var geometry = response.geometry;
                if (geometry != null) {
                    geometry = jsonUtils.fromJson(response.geometry);
                }
                var locate = {
                    geometry: geometry
                };
                deferred.resolve(locate);
            }), deferred.reject);

            return deferred;
        },
        /**
            * @param {esri.geometry.Polyline} line
            * @param {Number} offsetFromPoint
            * @param {Number} offsetToPoint
            * @param {Number} hint
            * @return {dojo.Deferred}
            */
        getLocateAll: function (line, offsetFromPoint, offsetToPoint, hint) {
            var deferred = new Deferred();

            this._invokeOperation('GetLocateAll', {
                'geometry': dojo.toJson(line.toJson()),
                'offsetFromPoint': offsetFromPoint,
                'offsetToPoint': offsetToPoint,
                'hint': hint
            }).then(lang.hitch(this, function (response) {

                var locateAll = {
                    distances: response.distances
                };
                deferred.resolve(locateAll);
            }), deferred.reject);

            return deferred;
        },
        /**
            * @param {esri.geometry.Polygon} polygon
            * @param {Number} referenceHeight
            * @param {studioat.SurfaceSOEUtils.planeReferenceType} planeReferenceType
            * @return {dojo.Deferred}
            */
        /*
    getVolumeAndArea:function (polygon, referenceHeight, planeReferenceType) {
        var deferred = new dojo.Deferred();
    
        this._invokeOperation('GetVolumeAndArea', {
            'geometry':dojo.toJson(polygon.toJson()),
            'referenceHeight':referenceHeight,
            'planeReferenceType':planeReferenceType
        }).then(lang.hitch(this, function (response) {
    
            var volumeAndArea = {
                volume:response.volume,
                surfaceArea:response.surfaceArea,
                projectedArea:response.projectedArea
            };
            deferred.resolve(volumeAndArea);
        }), deferred.reject);
    
        return deferred;
    },*/
        /**
            *
            * @param geometry
            * @return {*}
            */
        getElevationsFromGeometry: function (geometry) {
            var elevationData = null;
            switch (geometry.type) {
                case 'point':
                    elevationData = [geometry.z];
                    break;
                case 'multipoint':
                    elevationData = this._coordinatesToZs(geometry.points);
                    break;
                case 'polyline':
                    elevationData = this._partsToZs(geometry.paths);
                    break;
                case 'polygon':
                    elevationData = this._partsToZs(geometry.rings);
                    break;
            }
            return elevationData;
        },

        /**
            *
            * @param parts
            * @return {Array}
            * @private
            */
        _partsToZs: function (parts) {
            var elevationPoints = [];
            array.forEach(parts, function (part) {
                elevationPoints = elevationPoints.concat(this._coordinatesToZs(part));
            }, this);
            return elevationPoints;
        },

        /**
            *
            * @param coordinates
            * @return {*}
            * @private
            */
        _coordinatesToZs: function (coordinates) {
            return array.map(coordinates, function (coords) {
                return coords[2];
            }, this);
        },

        /**
            *
            * @param geometry
            * @return {*}
            */
        getPointsFromGeometry: function (geometry) {
            var elevationPoints = null;
            switch (geometry.type) {
                case 'point':
                    elevationPoints = [geometry];
                    break;
                case 'multipoint':
                    elevationPoints = this._coordinatesToPoints(geometry.points, geometry.spatialReference);
                    break;
                case 'polyline':
                    elevationPoints = this._partsToPoints(geometry.paths, geometry.spatialReference);
                    break;
                case 'polygon':
                    elevationPoints = this._partsToPoints(geometry.rings, geometry.spatialReference);
                    break;
            }
            return elevationPoints;
        },

        /**
            *
            * @param parts
            * @param spatialReference
            * @return {Array}
            * @private
            */
        _partsToPoints: function (parts, spatialReference) {
            var elevationPoints = [];
            array.forEach(parts, function (part) {
                elevationPoints = elevationPoints.concat(this._coordinatesToPoints(part, spatialReference));
            }, this);
            return elevationPoints;
        },

        /**
            *
            * @param coordinates
            * @param spatialReference
            * @return {*}
            * @private
            */
        _coordinatesToPoints: function (coordinates, spatialReference) {
            return array.map(coordinates, function (coords) {
                var pnt = new Point(coords, spatialReference);
                pnt.z = coords[2];
                return pnt;
            }, this);
        },

        /**
            *
            * @param polyline
            * @return {esri.geometry.Polyline}
            * @private
            */
        _polylineToSinglePart: function (polyline) {

            // PROFILE GRAPHIC //
            var profilePolyline = new Polyline(polyline.spatialReference);
            // CREATE A SINGLE PATH FROM ALL PATHS                //
            // ISSUE: WHEN MULTIPLE PATHS ARE RETURNED WE HAVE    //
            //        DUPLICATE POINTS AT END AND START OF PATHS, //
            //        NOT A HUGE ISSUE AS ELEVATIONS VALUES MATCH //
            var profilePath = [];
            array.forEach(polyline.paths, function (path) {
                profilePath = profilePath.concat(path);
            }, this);
            profilePolyline.addPath(profilePath);

            return profilePolyline;
        },

        /**
            *
            * @param {Object} profileChartParams
            */
        _initProfileChart: function (profileChartParams) {

            // PROFILE CHART PROPERTIES //
            this._profileChartInfo = lang.mixin(profileChartParams || {}, {
                profileChart: null,
                elevationData: null,
                profilePolyline: null,
                sampleDistMeters: null,
                chartMoveGroup: null,
                chartLocationGraphic: null
            });

            if (!this._profileChartInfo.map) {
                throw new Error("Missing 'map' parameters");
            }

            // POPUP WINDOW EVENTS //
            on(this._profileChartInfo.map.infoWindow, 'selection-change', lang.hitch(this, this.mapFeatureSelectionChange));
            on(this._profileChartInfo.map.infoWindow, 'hide', lang.hitch(this, this.mapInfoWindowHide));


            // CLEAR PREVIOUS CONTENT //
            if (this._profileChartInfo.chartNode != null) {
                this._profileChartInfo.chartNode.innerHTML = '';
            }

            if (profileChartParams.measurement) {
                on(profileChartParams.measurement, 'measure-end', lang.hitch(this, this._onMeasureEnd));
            } else {
                this._profileChartInfo.measurement = new Measurement({
                    map: this._profileChartInfo.map,
                    defaultLengthUnit: units.KILOMETERS
                }, domConstruct.create('div'));
                this._profileChartInfo.measurement.startup();
            }
            this._profileChartInfo.measurement.setTool("distance", true);
            this._profileChartInfo.measurement.setTool("distance", false);

            this._createProfileChart(null);
        },

        /**
            * mapFeatureSelectionChange
            *
            * CALLED WHEN THE SELECTED FEATURE OF THE POPUP WINDOW CHANGES
            */
        mapFeatureSelectionChange: function () {
            var selectedFeature = this._profileChartInfo.map.infoWindow.getSelectedFeature();
            var isPolyline = (selectedFeature && (selectedFeature.geometry.type === 'polyline'));
            if (isPolyline) {
                this.createProfileChart(selectedFeature.geometry)
            } else {
                this.clearProfileChart();
            }
        },

        /**
            * mapInfoWindowHide
            *
            * CALLED WHEN THE POPUP WINDOW IS CLOSED
            */
        mapInfoWindowHide: function () {
            this.clearProfileChart();
            //this.setProfileWindowVisible(false);
        },

        /**
            *
            */
        clearProfileChart: function () {
            if (this._profileChartInfo && (this._profileChartInfo.chartNode != null)) {
                this._createProfileChart(null);
                //this._profileChartInfo.elevationData = null;
            } else {
                throw new Error("Profile Chart is not initialized: 'initProfileChart(...)' must be called before 'clearProfileChart()' method can can be used.");
            }
        },

        /**
            *
            * @param geometry
            * @param chartNode
            */
        createProfileChart: function (geometry, chartNode) {
            var deferred = new Deferred();

            if (this._profileChartInfo) {
                if (chartNode) {
                    this._profileChartInfo.chartMoveGroup = null;
                    this._profileChartInfo.profileChart = null;
                    this._profileChartInfo.chartNode = chartNode;
                    this._profileChartInfo.chartNode.innerHTML = '';
                } else {
                    if (this._profileChartInfo.chartNode == null) {
                        deferred.reject(new Error("Profile chart node not specified."));
                    }
                }
                this.getElevations(geometry).then(lang.hitch(this, function (elevationInfo) {
                    this._createProfileChart(elevationInfo).then(deferred.resolve, deferred.reject);
                }), deferred.reject);
            } else {
                deferred.reject(new Error("Profile Chart is not initialized: 'initProfileChart(...)' must be called before 'createProfileChart()' method can can be used."));
            }

            return deferred;
        },

        /**
            * _onMeasureEnd
            *
            * CALCULATE PROFILE ELEVATIONS FOR GEOMETRIES
            *
            * @param {String} activeToolName
            * @param {esri.geometry.Geometry} geometry
            */
        _onMeasureEnd: function (activeToolName, geometry) {

            switch (activeToolName) {
                case 'area':
                    var distancePolyline = new Polyline(geometry.spatialReference);
                    distancePolyline.addPath(geometry.rings[0]);
                    this.createProfileChart(distancePolyline);
                    break;

                case 'distance':
                    this.createProfileChart(geometry);
                    break;

                case 'location':
                    var geoPoint = webMercatorUtils.webMercatorToGeographic(geometry);
                    this.getElevationAtLonLat(geoPoint).then(lang.hitch(this, function (response) {
                        var elevStr = number.format(response.elevation, { places: 2 });
                        var parentNode = this._profileChartInfo.measurement.resultValue.containerNode;
                        domConstruct.create('div', {
                            'class': 'measureLocationResult',
                            innerHTML: lang.replace(this.messages.locationResultTemplate, [elevStr])
                        }, parentNode, 'last');
                    }), lang.hitch(this, function (error) {
                        console.warn(error.message);
                    }));
                    break;

                default:
                    //...
                    break;
            }
        },

        /**
            *
            * @param elevationInfo
            * @private
            */
        _createProfileChart: function (elevationInfo) {
            var deferred = new Deferred();

            if (elevationInfo && this._profileChartInfo && (this._profileChartInfo.chartNode != null)) {
                // PROFILE POLYLINE //
                this._profileChartInfo.profilePolyline = elevationInfo.geometry;
                // ELEVATION DATA //
                this._profileChartInfo.elevationData = elevationInfo.data;

                var profileLengthMeters = geodesicUtils.geodesicLengths([webMercatorUtils.webMercatorToGeographic(this._profileChartInfo.profilePolyline)], units.METERS)[0];
                this._profileChartInfo.sampleDistMeters = (profileLengthMeters / this._profileChartInfo.elevationData.length);
            } else {
                this._profileChartInfo.elevationData = this._getFilledArray(1, 0.0);
                this._profileChartInfo.sampleDistMeters = 1.0;
            }

            // ADJUSTED MIN/MAX ELEVATIONS //
            var yMinSource = this._getArrayMin(this._profileChartInfo.elevationData);
            var yMaxSource = this._getArrayMax(this._profileChartInfo.elevationData);
            var yRange = (yMaxSource - yMinSource);
            var yMin = yMinSource - (yRange * 0.05);
            var yMax = yMaxSource + (yRange * 0.05);
            var yTickStep = this._adjustYTickStep((yRange / 5.0));

            // FILLED ZERO ARRAY //
            var waterData = this._getFilledArray(this._profileChartInfo.elevationData.length);

            if (this._profileChartInfo.profileChart != null) {
                this._profileChartInfo.chartMoveGroup = null;

                // UPDATE CHART //
                this._profileChartInfo.profileChart.getAxis("y").opt.min = yMin;
                this._profileChartInfo.profileChart.getAxis("y").opt.max = yMax;
                this._profileChartInfo.profileChart.getAxis("y").opt.majorTickStep = yTickStep;
                this._profileChartInfo.profileChart.getAxis("y").opt.title = lang.replace(this.messages.chart.elevationTitleTemplate, [this._getDisplayUnits(true)]);
                this._profileChartInfo.profileChart.getAxis("x").opt.title = lang.replace(this.messages.chart.distanceTitleTemplate, [this._getDisplayUnits(false)]);
                this._profileChartInfo.profileChart.dirty = true;
                this._profileChartInfo.profileChart.updateSeries("Water", waterData);
                this._profileChartInfo.profileChart.updateSeries("Elevation Profile", this._profileChartInfo.elevationData);
                this._profileChartInfo.profileChart.render();

            } else if (this._profileChartInfo && (this._profileChartInfo.chartNode != null)) {

                // ANIMATION OPTIONS //
                var chartAnimationOpts = {
                    duration: 1500,
                    easing: easing.linear
                };

                var nodeCoords = domGeom.position(this._profileChartInfo.chartNode, true);
                if (nodeCoords.h === 0) {
                    deferred.reject(new Error("Height of chart node NOT specified: chart node must have height specified in pixels."));
                }

                // CREATE CHART //
                this._profileChartInfo.profileChart = new Chart2D(this._profileChartInfo.chartNode, {
                    title: "Elevation Profile",
                    titlePos: "top",
                    titleGap: 5,
                    titleFont: this._getFontSize('legendTitle', false),
                    titleFontColor: 'darkgreen',
                    animationType: chartAnimationOpts.easing
                });

                // SET THEME //
                this._profileChartInfo.profileChart.setTheme(ThreeD);
                // OVERRIDE DEFAULTS //
                this._profileChartInfo.profileChart.fill = 'white';
                this._profileChartInfo.profileChart.theme.axis.stroke.width = 2;
                this._profileChartInfo.profileChart.theme.axis.majorTick = {
                    color: Color.named.white.concat(0.5),
                    width: 1.0
                };
                this._profileChartInfo.profileChart.theme.plotarea.fill = {
                    type: "linear",
                    space: "plot",
                    x1: 50, y1: 100, x2: 50, y2: 0,
                    colors: [
                        { offset: 0.0, color: Color.named.powderblue },
                        { offset: 1.0, color: Color.named.steelblue }
                    ]
                };

                // Y AXIS //
                this._profileChartInfo.profileChart.addAxis("y", {
                    min: yMin,
                    max: yMax,
                    majorTickStep: yTickStep,
                    fontColor: 'black',
                    font: this._getFontSize('axisLabel', false),
                    vertical: true,
                    fixLower: "major",
                    fixUpper: "minor",
                    natural: true,
                    fixed: true,
                    includeZero: false,
                    majorTicks: true,
                    majorTick: { color: 'black', length: 4 },
                    minorTicks: false,
                    labelFunc: this._getElevationLabel,
                    title: lang.replace(this.messages.chart.elevationTitleTemplate, [this._getDisplayUnits(true)]),
                    titleGap: 30,
                    titleFont: this._getFontSize('axisTitle', false),
                    titleFontColor: 'green',
                    titleOrientation: 'axis'
                });

                // X AXIS //
                this._profileChartInfo.profileChart.addAxis("x", {
                    fontColor: 'black',
                    font: this._getFontSize('axisLabel', false),
                    fixLower: "none",
                    fixUpper: "none",
                    includeZero: false,
                    natural: false,
                    fixed: true,
                    majorTicks: true,
                    majorTick: { color: 'black', length: 4 },
                    labelFunc: this._getDistanceLabel,
                    minorTicks: true,
                    minorTick: { color: 'black', length: 4 },
                    title: lang.replace(this.messages.chart.elevationTitleTemplate, [this._getDisplayUnits(false)]),
                    titleGap: 5,
                    titleFont: this._getFontSize('axisTitle', false),
                    titleFontColor: 'green',
                    titleOrientation: 'away'
                });

                // GRID //
                this._profileChartInfo.profileChart.addPlot("Grid", {
                    type: "Grid",
                    hMajorLines: true,
                    hMinorLines: false,
                    vMajorLines: false,
                    vMinorLines: false
                });

                // PROFIlE PLOT //
                this._profileChartInfo.profileChart.addPlot("default", {
                    type: "Areas",
                    tension: "X",
                    animate: chartAnimationOpts
                });

                // WATER PLOT //
                this._profileChartInfo.profileChart.addPlot("water", {
                    type: "Areas",
                    animate: chartAnimationOpts
                });

                // WATER DATA //
                this._profileChartInfo.profileChart.addSeries("Water", waterData, {
                    plot: "water",
                    stroke: { color: Color.named.white, width: 2.0 },
                    fill: {
                        type: "linear",
                        space: "plot",
                        x1: 50, y1: 0, x2: 50, y2: 100,
                        colors: [
                            { offset: 0.0, color: Color.named.lightblue.concat(0.5) },
                            { offset: 1.0, color: Color.named.blue.concat(0.5) }
                        ]
                    }
                });

                // PROFILE DATA //
                this._profileChartInfo.profileChart.addSeries("Elevation Profile", this._profileChartInfo.elevationData, {
                    stroke: { color: Color.named.tan, width: 1.5 },
                    fill: {
                        type: "linear",
                        space: "plot",
                        x1: 50, y1: 0, x2: 50, y2: 100,
                        colors: [
                            { offset: 0.0, color: Color.named.saddlebrown },
                            { offset: 1.0, color: Color.named.peru }
                        ]
                    }
                });

                if (this._profileChartInfo.showElevationDifference) {
                    // DISPLAY CHART LOCATION ON MAP //
                    if (!esri.isTouchEnabled) {
                        // MOUSE MOVE EVENT //
                        this._profileChartInfo.profileChart.surface.on("mousemove", lang.hitch(this, function (evt) {
                            event.stop(evt);
                            this._onChartLocationMove(evt.clientX, evt.clientY);
                        }));
                    } else {
                        // TOUCH MOVE EVENT //
                        this._profileChartInfo.profileChart.surface.on("touchmove", lang.hitch(this, function (evt) {
                            event.stop(evt);
                            if (evt.touches && (evt.touches.length > 0)) {
                                this._onChartLocationMove(evt.touches[0].clientX, evt.touches[0].clientY);
                            }
                        }));
                        // TOUCH END EVENT //
                        this._profileChartInfo.profileChart.surface.on("touchend", lang.hitch(this, function (evt) {
                            event.stop(evt);
                            this._onChartLocationMove(0, 0);
                        }));
                    }
                }

                // RENDER CHART //
                this._profileChartInfo.profileChart.render();
            }

            if (this._profileChartInfo.profileChart != null) {
                // ELEVATION INFO //
                var elevDisplayUnits = this._getDisplayUnits(true);

                var elevMinStr = this._getDisplayLabel(yMinSource, elevDisplayUnits);
                var elevMaxStr = this._getDisplayLabel(yMaxSource, elevDisplayUnits);
                var elevFirst = this._profileChartInfo.elevationData[0];
                var elevLast = this._profileChartInfo.elevationData[this._profileChartInfo.elevationData.length - 1];
                var elevStartStr = this._getDisplayLabel(elevFirst, elevDisplayUnits);
                var elevEndStr = this._getDisplayLabel(elevLast, elevDisplayUnits);
                var startElev = this._getDisplayValue(elevFirst, elevDisplayUnits);
                var endElev = this._getDisplayValue(elevLast, elevDisplayUnits);
                var gainloss = (endElev - startElev);
                var gainlossStr = number.format(gainloss, { places: 1 });

                var elevInfo = {
                    min: elevMinStr,
                    max: elevMaxStr,
                    start: elevStartStr,
                    end: elevEndStr,
                    gainloss: gainlossStr
                };

                // ELEVATION INFO TEXT //
                var elevMinMaxMsg = lang.replace(this.messages.chart.gainLossTemplate, elevInfo);
                var chartCoords = this._profileChartInfo.profileChart.getCoords();
                var elevMinMaxMsgText = this._profileChartInfo.profileChart.surface.createText({
                    x: chartCoords.x,
                    y: (chartCoords.h - 5.0),
                    text: elevMinMaxMsg,
                    align: "start"
                }).setFont({ family: "Tahoma", style: "bold", size: "8pt" }).setFill('darkred');
                var maxWidth = (chartCoords.w - elevMinMaxMsgText.getTextWidth());
                var dataSourceInfo = this.getDataSourceInfo(gfx.px2pt(maxWidth) / 9.0);
                // ELEVATION SOURCE TEXT //
                var dataSourceText = this._profileChartInfo.profileChart.surface.createText({
                    x: (chartCoords.w - 5.0),
                    y: (chartCoords.h - 5.0),
                    text: dataSourceInfo,
                    align: "end"
                }).setFont({ family: "Tahoma", style: "bold", size: "8pt" }).setFill('gray');
                dataSourceText.on('click', lang.hitch(this, function () {
                    Tooltip.show(this.getDataSourceInfo(Infinity), dataSourceText.parent._parent, ["above", "below", "after", "before"]);
                }));
                dataSourceText.on('mouseover', lang.hitch(this, function () {
                    domStyle.set(dataSourceText.parent._parent, 'cursor', 'pointer');
                }));
                dataSourceText.on('mouseout', lang.hitch(this, function () {
                    domStyle.set(dataSourceText.parent._parent, 'cursor', 'default');
                    Tooltip.hide(dataSourceText.parent._parent);
                }));

                // TEMPLATE HELP LINK //
                if (this._profileChartInfo.showTemplateHelp) {
                    var helpText = this._profileChartInfo.profileChart.surface.createText({
                        x: (chartCoords.w - 5.0),
                        y: 15.0,
                        text: "Help",
                        align: "end"
                    }).setFont({ family: "Tahoma", weight: "bold", size: "9pt" }).setFill("darkblue").setStroke({ color: "gray", width: 0.25 });
                    helpText.on('mouseover', lang.hitch(this, function () {
                        domStyle.set(helpText.parent._parent, 'cursor', 'pointer');
                    }));
                    helpText.on('mouseout', lang.hitch(this, function () {
                        domStyle.set(helpText.parent._parent, 'cursor', 'default');
                    }));
                    helpText.on('click', lang.hitch(this, function () {
                        var helpDlg = dijit.byId('helpDlg');
                        if (!helpDlg) {

                            var helpContent = domConstruct.create('div', {});
                            var optionsList = domConstruct.create('ul', {}, helpContent);

                            var selectOption = domConstruct.create('li', {
                                innerHTML: lang.replace('{display.selectLine}&nbsp;<a id="showmeselect" target="_blank" href="{display.selectFeatureHelpUrl}">{display.showMe}</a><br/><span style="color:gray;padding-left:6px;">{display.popupRequirement}</span>', this.messages)
                            }, optionsList);

                            var measureOption = domConstruct.create('li', {
                                innerHTML: lang.replace('{display.digitizeDistanceMeasureTool}&nbsp;<a id="showmemeasure" target="_blank" href="{display.measureToolHelpUrl}">{display.showMe}</a>', this.messages)
                            }, optionsList);

                            var hoverInfo = domConstruct.create('div', {
                                style: "margin:15px 10px 5px 10px;",
                                innerHTML: lang.replace('{display.hoverOver}', this.messages)
                            }, helpContent);

                            helpDlg = new Dialog({
                                id: "helpDlg",
                                style: "width:500px;",
                                title: this.messages.display.elevationProfileTitle,
                                content: helpContent
                            });
                            domStyle.set(helpDlg.titleNode, { 'color': 'white', 'fontWeight': 'bolder' });
                        }
                        helpDlg.show();
                    }
                    ));
                }
                deferred.resolve();
            } else {
                deferred.resolve();
            }

            return deferred;
        },

        /**
            * onChartLocationMove
            *
            * DISPLAY INTERACTIVE VERTICAL LINE AND ELEVATION TEXT AT MOUSE/TOUCH LOCATION
            *
            * @param {Number} clientX
            * @param {Number} clientY
            */
        _onChartLocationMove: function (clientX, clientY) {

            if (this._profileChartInfo.elevationData) {
                var chartCoords = dojo.coords(this._profileChartInfo.profileChart.node, true);
                var chartOffsets = this._profileChartInfo.profileChart.offsets;

                // PLOT COORDINATES //
                var plotCoords = {
                    x: (clientX - chartCoords.x),
                    y: (clientY - chartCoords.y),
                    l: (chartCoords.x + chartOffsets.l),
                    r: (chartCoords.x + chartCoords.w - chartOffsets.r),
                    t: (chartCoords.y + chartOffsets.t),
                    b: (chartCoords.y + chartCoords.h - chartOffsets.b)
                };

                if (!this._profileChartInfo.chartMoveGroup) {
                    // CREATE GROUP OF GRAPHICS TO MOVE //
                    this._profileChartInfo.chartMoveGroup = this._profileChartInfo.profileChart.surface.createGroup();
                } else {
                    // REMOVE PREVIOUS //
                    this._profileChartInfo.chartMoveGroup.clear()
                }

                // IS CURSOR OVER CHART PLOT AREA //
                if (((clientX > plotCoords.l) && (clientX < plotCoords.r)) &&
                    ((clientY > plotCoords.t) && (clientY < plotCoords.b))) {

                    var elevationData = this._profileChartInfo.elevationData;

                    // X POINT INDEX //
                    var plotPercentAlongX = ((clientX - plotCoords.l) / (chartCoords.w - plotCoords.l));
                    var pointIndex = parseInt((elevationData.length - 1) * plotPercentAlongX);
                    // DISPLAY CHART LOCATION ON MAP //
                    this._displayChartLocation(pointIndex);

                    // Y COORDINATE //
                    var elev = elevationData[pointIndex];
                    var elevDisplayUnits = this._getDisplayUnits(true);
                    var elevStr = this._getDisplayLabel(elev, elevDisplayUnits);
                    var yAxisBounds = this._profileChartInfo.profileChart.getAxis('y').scaler.bounds;
                    var plotPercentAlongY = ((yAxisBounds.upper - elev) / (yAxisBounds.upper - yAxisBounds.lower));
                    var chartElevationLineY = (plotCoords.t + ((plotCoords.b - plotCoords.t) * plotPercentAlongY)) - chartCoords.y;
                    var chartElevationMiddle = (plotCoords.t + ((plotCoords.b - plotCoords.t) * 0.5)) - chartCoords.y;

                    // ELEVATION CHANGE //
                    var currentElev = this._getDisplayValue(elevationData[pointIndex], elevDisplayUnits);
                    var startElev = this._getDisplayValue(elevationData[0], elevDisplayUnits);
                    var gainloss = (currentElev - startElev);
                    var gainlossStr = gainloss ? number.format(gainloss, { places: 1 }) : "0.0";

                    var plotPercentAlongYChange = ((yAxisBounds.upper - elevationData[0]) / (yAxisBounds.upper - yAxisBounds.lower));
                    var chartElevationLineYChange = (plotCoords.t + ((plotCoords.b - plotCoords.t) * plotPercentAlongYChange)) - chartCoords.y;

                    // VERTICAL LINE //
                    this._profileChartInfo.chartMoveGroup.createLine({
                        x1: plotCoords.x,
                        y1: plotCoords.t - chartCoords.y,
                        x2: plotCoords.x,
                        y2: plotCoords.b - chartCoords.y
                    }).setStroke({ color: "black", width: 1.5 });

                    if (!isNaN(chartElevationLineY)) {

                        // ELEVATION TEXT //
                        var leftOfCenter = (plotCoords.x < (plotCoords.l + ((plotCoords.r - plotCoords.l) * 0.5)));
                        var bottomOfMiddle = (chartElevationLineY > chartElevationMiddle);
                        var textOffset = 10.0;
                        var textX = (leftOfCenter ? (plotCoords.x + textOffset) : (plotCoords.x - textOffset));
                        var textY = (bottomOfMiddle ? (chartElevationLineY - textOffset) : (chartElevationLineY + (textOffset * 3)));

                        this._profileChartInfo.chartMoveGroup.createText({
                            x: textX,
                            y: textY,
                            text: elevStr,
                            align: leftOfCenter ? "start" : "end"
                        }).setFont({ family: "Tahoma", weight: "bold", size: "22pt" }).setFill("black").setStroke({ color: "white", width: 1.5 });

                        // ELEVATION CHANGE //
                        if (this._profileChartInfo.showElevationDifference && (gainloss != 0.0)) {
                            var changeTextX = (!leftOfCenter ? (plotCoords.x + 5.0) : (plotCoords.x - 5.0));
                            var gainlosslabelcolor = ((gainloss > 0.0) ? "green" : "red");

                            this._profileChartInfo.chartMoveGroup.createLine({
                                x1: plotCoords.x,
                                y1: chartElevationLineYChange,
                                x2: plotCoords.x,
                                y2: chartElevationLineY
                            }).setStroke({ color: gainlosslabelcolor, width: 3.0 });

                            var textLength = gainlossStr.length - ((gainloss > 1000) ? 2 : 1);
                            var textWidth = (textLength * 10.0) + 5.0;
                            this._profileChartInfo.chartMoveGroup.createRect({
                                x: leftOfCenter ? (changeTextX - textWidth - 5.0) : changeTextX + 5.0,
                                y: chartElevationLineYChange - 10.0,
                                width: textWidth,
                                height: 20.0,
                                r: 5.0
                            }).setFill(Color.named.white.concat(0.75)).setStroke({ color: gainlosslabelcolor, width: 1 });

                            this._profileChartInfo.chartMoveGroup.createText({
                                x: leftOfCenter ? changeTextX - 10.0 : changeTextX + 10.0,
                                y: chartElevationLineYChange + 5.0,
                                text: gainlossStr,
                                align: leftOfCenter ? "end" : "start"
                            }).setFont({ family: "Tahoma", weight: "normal", size: "10pt" }).setFill(gainlosslabelcolor);
                        }

                        // CIRCLE //
                        this._profileChartInfo.chartMoveGroup.createCircle({
                            cx: plotCoords.x,
                            cy: chartElevationLineY,
                            r: 6
                        }).setStroke({ color: "white", width: 2.0 });

                    }
                } else {
                    this._displayChartLocation(-1);
                }
            }
        },

        /**
            * displayChartLocation
            *
            * DISPLAY CHART LOCATION AS RED X GRAPHIC ON THE MAP
            *
            * @param {Number} pointIndex
            */
        _displayChartLocation: function (pointIndex) {
            if (this._profileChartInfo.map && this._profileChartInfo.elevationData && this._profileChartInfo.profilePolyline) {

                if (!this._profileChartInfo.chartLocationGraphic) {
                    var red = new Color(Color.named.red);
                    var outline = new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, red, 3);
                    var chartLocationSymbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_X, 13, outline, red);
                    this._profileChartInfo.chartLocationGraphic = new Graphic(null, chartLocationSymbol);
                    this._profileChartInfo.map.graphics.add(this._profileChartInfo.chartLocationGraphic);
                }

                if ((pointIndex >= 0) && (pointIndex < this._profileChartInfo.elevationData.length)) {
                    this._profileChartInfo.chartLocationGraphic.setGeometry(this._profileChartInfo.profilePolyline.getPoint(0, pointIndex))
                } else {
                    this._profileChartInfo.chartLocationGraphic.setGeometry(null);
                }
            }
        },

        /**
            *
            * @param yTickStep
            * @return {*}
            * @private
            */
        _adjustYTickStep: function (yTickStep) {
            var newYTickStep = yTickStep;
            var limits = [1000, 100, 10, 1];
            array.some(limits, function (limit) {
                newYTickStep = ((yTickStep + limit) - ((yTickStep + limit) % limit));
                return (yTickStep > limit);
            });
            return newYTickStep;
        },

        /**
            *
            * @param size
            * @param value
            * @return {Array}
            * @private
            */
        _getFilledArray: function (size, value) {
            var dataArray = new Array(size);
            for (var dataIdx = 0; dataIdx < size; ++dataIdx) {
                dataArray[dataIdx] = value || 0;
            }
            return dataArray;
        },

        /**
            *
            * @param chartPart
            * @param sizeOnly
            * @return {*}
            * @private
            */
        _getFontSize: function (chartPart, sizeOnly) {
            var fontSizes = {
                legendTitle: 13,
                axisTitle: 11,
                chartLabel: 9,
                axisLabel: 7
            };
            return sizeOnly ? lang.replace("{0}pt", [fontSizes[chartPart]]) : lang.replace("normal normal bold {0}pt Tahoma", [fontSizes[chartPart]]);
        },

        /**
            *
            * @param array
            * @return {Object}
            * @private
            */
        _getArrayMax: function (array) {
            return Math.max.apply(Math, array);
        },

        /**
            *
            * @param array
            * @return {Object}
            * @private
            */
        _getArrayMin: function (array) {
            return Math.min.apply(Math, array);
        },

        /**
            * getDistanceLabel
            *
            * X-AXIS LABEL FUNCTION
            *
            * @param {String} label
            * @param {Number} val
            */
        _getDistanceLabel: function (label, val) {
            var displayUnits = this._getDisplayUnits(false);
            return this._getDisplayLabel((val * this._profileChartInfo.sampleDistMeters), displayUnits);
        },

        /**
            * getElevationLabel
            *
            * Y-AXIS LABEL FUNCTION
            *
            * @param {String} label
            * @param {Number} val
            */
        _getElevationLabel: function (label, val) {
            var displayUnits = this._getDisplayUnits(true);
            return this._getDisplayLabel(val, displayUnits);
        },

        /**
            * getDisplayLabel
            *
            * GET DISPLAY LABEL GIVEN A VALUE IN METERS AND THE DISPLAY UNITS
            * CONVERT FROM METERS TO MILES THEN FROM MILES TO DISPLAY UNITS
            *
            * @param {Number} valueMeters
            * @param {String} displayUnits
            */
        _getDisplayLabel: function (valueMeters, displayUnits) {
            var displayDistance = this._getDisplayValue(valueMeters, displayUnits);
            return number.format(displayDistance, { 'places': 1 });
        },

        /**
            * getDisplayValue
            *
            * GET DISPLAY VALUE GIVEN A VALUE IN METERS AND THE DISPLAY UNITS
            * CONVERT FROM METERS TO MILES THEN FROM MILES TO DISPLAY UNITS
            *
            * @param {Number} valueMeters
            * @param {String} displayUnits
            */
        _getDisplayValue: function (valueMeters, displayUnits) {
            if (displayUnits === this._profileChartInfo.measurement.units.esriMeters) {
                return valueMeters;
            } else {
                var distanceMiles = (valueMeters / this._profileChartInfo.measurement.unitDictionary[this._profileChartInfo.measurement.units.esriMeters]);
                return (distanceMiles * this._profileChartInfo.measurement.unitDictionary[displayUnits]);
            }
        },

        /**
            * getDisplayUnits
            *
            * GET DISPLAY UNITS FOR AXIS LABELS
            * IF USING A SELECTED FEATURE WE'LL USE THE DEFAULT DISTANCE UNITS BASED ON THE MAP SCALEBAR (OR MILES IF NO SCALEBAR)
            * IF GETTING DISPLAY UNITS FOR ELEVATION WE'LL SWITCH TO FEET OR METERS
            *
            * @param {Boolean} isElevation
            */
        _getDisplayUnits: function (isElevation) {
            var displayUnits = this._profileChartInfo.measurement.unit.label;
            if (isElevation) {
                switch (displayUnits) {
                    case this._profileChartInfo.measurement.units.esriMiles:
                        displayUnits = this._profileChartInfo.measurement.units.esriFeet;
                        break;
                    case this._profileChartInfo.measurement.esriYards:
                        displayUnits = this._profileChartInfo.measurement.esriFeet;
                        break;
                    case this._profileChartInfo.measurement.units.esriKilometers:
                        displayUnits = this._profileChartInfo.measurement.units.esriMeters;
                        break;
                }
            }
            return displayUnits;
        },

        /**
            *
            * @return {*}
            */
        toString: function () {
            return dojo.toJson(this, true);
        }

    });

});