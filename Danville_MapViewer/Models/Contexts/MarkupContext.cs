﻿using Danville_MapViewer.Models.Mappings;
using Danville_MapViewer.Models.Markup;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SQLite;
using System.Linq;
using System.Web;

namespace Danville_MapViewer.Models.Contexts
{
    public class MarkupContext : DbContext
    {
        public MarkupContext()
        {
            Database.SetInitializer<MarkupContext>(null);
        }


        public DbSet<MarkupCollection> MarkupCollections { get; set; }
        public DbSet<MarkupUser> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new MarkupCollectionMap());
            modelBuilder.Entity<MarkupCollection>().Ignore(t => t.type);
        }


    }
}