﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Danville_MapViewer.Models.ViewModels
{
    public class JsonResponse
    {
        public string status { get; set; }
        public String message { get; set; }
        public Object data { get; set; }
    }

    public sealed class ResponseStatus
    {
        public static readonly string SUCCESS = "SUCCESS";
        public static readonly string ERROR = "ERROR";
    }
}