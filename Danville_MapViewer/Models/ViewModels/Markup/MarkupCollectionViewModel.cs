﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Danville_MapViewer.Models.ViewModels.Markup
{
    public class MarkupCollectionViewModel
    {
        public string type { get; set; }

        public long? _id { get; set; }

        public string title { get; set; }

        [Required]
        // TODO?: Make this into a legit serialization object?
        // As of now, we rely on M4 to serialize the 'options' (which is the heart of the markup collection) into a string.
        public string options { get; set; }

    }
}