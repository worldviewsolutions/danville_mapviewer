﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Danville_MapViewer.Models.Export
{
    public class ProjectionInfo
    {
        public string Title { get; set; }
        public int Wkid { get; set; }
    }
}