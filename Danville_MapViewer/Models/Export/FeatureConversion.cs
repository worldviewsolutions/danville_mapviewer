﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Danville_MapViewer.Models.Export
{


    public class FeatureConversion
    {
        public ExportFormat exportFormat { get; set; }
        public string jsonFeature { get; set; }
        public int inputSpatialReference { get; set; }
        public int outputSpatialReference { get; set; }
    }


    /// <summary>
    /// An enum defining our supported export formats. This index starts at 1 because of strange behavior in dojo dijit selects
    /// </summary>
    public enum ExportFormat
    {
        SHAPEFILE = 1,
        KML = 2,
        EXCEL = 3,
        GEOJSON = 4,
        FILEGEODATABASE = 5
    }
}