﻿using Danville_MapViewer.Models.Markup;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Danville_MapViewer.Models.Mappings
{
    public class UserMap : EntityTypeConfiguration<MarkupUser>
    {
        public UserMap()
        {
            this.Property(t => t.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.Name).HasColumnName("Name");
            
            this.ToTable("User");
        }
    }
}