﻿using Danville_MapViewer.Models.Markup;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Danville_MapViewer.Models.Mappings
{
    public class MarkupCollectionMap : EntityTypeConfiguration<MarkupCollection>
    {
        public MarkupCollectionMap()
        {
            // Primary Key
            this.Property(t => t.ID).HasColumnName("ID").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.options).HasColumnName("JSON");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.TITLE).HasColumnName("TITLE");
            
            this.ToTable("MarkupCollection");

            this.HasRequired(t => t.User)
                .WithMany(t => t.MarkupCollections)
                .HasForeignKey(d => d.UserID);
        }
    }
}