﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Danville_MapViewer.Models.Markup
{
    public class MarkupCollection
    {
        // This will always be MarkupLayer but we need it for serialization purposes.
        public string type { get; set; }

        // This will be the unique ID for a given Markup Collection.
        public long ID { get; set; }
        
        // This actually contains our markup data.
        public string options { get; set; }

        public string TITLE { get; set; }

        public long UserID { get; set; }

        public virtual MarkupUser User { get; set; }
    }
}