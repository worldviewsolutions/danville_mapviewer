﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Danville_MapViewer.Models.Markup
{
    public class MarkupUser
    {

        public long ID { get; set; }

        public string Name { get; set; }

        public virtual List<MarkupCollection> MarkupCollections { get; set; }

        public MarkupUser()
        {
            MarkupCollections = new List<MarkupCollection>();
        }
    }
}