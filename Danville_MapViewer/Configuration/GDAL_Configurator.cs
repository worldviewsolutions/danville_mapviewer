﻿using System;
using System.IO;
using Gdal = OSGeo.GDAL.Gdal;
using Ogr = OSGeo.OGR.Ogr;

namespace Danville_MapViewer.Configuration
{
    public static class GDAL_Configurator
    {

        private static bool _ogrConfigured;

        private static string _getPlatform()
        {
            return IntPtr.Size == 4 ? "x86" : "x64";
        }

        static GDAL_Configurator()
        {
            string executingPath = new Uri(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)).LocalPath;
            string gdalPath = Path.Combine(executingPath, "_bin_deployableAssemblies", "gdal", _getPlatform());

            if(string.IsNullOrEmpty(executingPath))
                throw new NullReferenceException("No executing path provided");
            
            Gdal.SetConfigOption("GDAL_HOME", executingPath);


            string gdalData = Path.Combine(gdalPath, "gdal_data");
            Gdal.SetConfigOption("GDAL_DATA", gdalData);

            string driverPath = Path.Combine(gdalPath, "gdal_plugins");
            Gdal.SetConfigOption("GDAL_DRIVER_PATH", driverPath);

            string proj_lib = Path.Combine(gdalPath, "proj_lib");
            Gdal.SetConfigOption("PROJ_LIB", proj_lib);

            Gdal.PushFinderLocation(gdalPath);
        }


        public static void ConfigureOgr()
        {
            if (_ogrConfigured) return;

            Ogr.RegisterAll();

            _ogrConfigured = true;
        }
    }
}