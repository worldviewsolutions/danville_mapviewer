﻿using Ionic.Zip;
using Danville_MapViewer.Configuration;
using Danville_MapViewer.Models.Export;
using Danville_MapViewer.Models.ViewModels;
using OSGeo.OGR;
using OSGeo.OSR;
using System;
using System.Security;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JSToolkit_MVC.Controllers
{
    public class ExportController : Controller
    {

        /// <summary>
        /// Searches the projection database for a projection by wkid or title
        /// </summary>
        /// <param name="Title"></param>
        /// <returns>A list of matching projections</returns>
        public JsonResult GetSupportedWkid(string Title)
        {
            List<ProjectionInfo> projectionInfos = new List<ProjectionInfo>();


            // TODO: dojo's built-in autocomplete uses wildcards that this does not currently support
            Title = Title.Replace("*", "");

            if (!String.IsNullOrEmpty(Title))
            {
                string projectionDB = String.Format("{0}\\DataStores\\Projections.sqlite", new Uri(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)).LocalPath);
                using (SQLiteConnection m_dbConnection = new SQLiteConnection(String.Format("Data Source={0};Version=3;", projectionDB)))
                {
                    m_dbConnection.Open();
                    string sql = String.Format("SELECT * FROM Projections WHERE Wkid LIKE '{0}%' OR Title LIKE '{0}%'", Title);
                    SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                    SQLiteDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        ProjectionInfo pi = new ProjectionInfo();
                        int curWkid = Convert.ToInt32(reader["Wkid"]);
                        pi.Title = String.Format("{0} - {1}", curWkid, (string)reader["Title"]);
                        pi.Wkid = curWkid;
                        projectionInfos.Add(pi);
                    }
                }
            }

            return Json(projectionInfos, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="featureConversion"></param>
        /// <returns></returns>
        [SecuritySafeCritical]
        public JsonResult ConvertFeature(FeatureConversion featureConversion)
        {
            // Create temp file for GeoJSON
            String featurePath = String.Format("{0}\\{1}.json", GetConfigSetting("TempDirectory"), Guid.NewGuid());
            string JsonFeature = featureConversion.jsonFeature.Replace("\\\"", "'");
            StreamWriter feature = System.IO.File.CreateText(featurePath);
            feature.Write(JsonFeature);
            feature.Close();

            // Response
            JsonResponse response = new JsonResponse();

            string outputDir = HttpContext.Server.MapPath(string.Format("~/{0}", GetConfigSetting("OutputDirectory")));

            if (!Directory.Exists(outputDir))
            {
                Directory.CreateDirectory(outputDir);
            }

            GDAL_Configurator.ConfigureOgr();

            // Open our data source
            DataSource dataSource = Ogr.Open(featurePath, 0);

            if (dataSource == null)
            {
                response.status = ResponseStatus.ERROR;
                response.message = "There was an error processing your feature(s)";
                JsonResult res = Json(response, JsonRequestBehavior.AllowGet);
                res.MaxJsonLength = int.MaxValue;
                return res;
            }

            // Load the driver
            Driver drv = dataSource.GetDriver();
            if (drv == null)
            {
                response.status = ResponseStatus.ERROR;
                response.message = "There was an error processing your feature(s)";
                JsonResult res = Json(response, JsonRequestBehavior.AllowGet);
                res.MaxJsonLength = int.MaxValue;
                return res;
            }

            if (dataSource.GetLayerCount() != 1)
            {
                response.status = ResponseStatus.ERROR;
                response.message = "The input must be GeoJSON with only one layer";
                JsonResult res = Json(response, JsonRequestBehavior.AllowGet);
                res.MaxJsonLength = int.MaxValue;
                return res;
            }

            OSGeo.OGR.Layer layer = dataSource.GetLayerByIndex(0);

            if (layer == null)
            {
                response.status = ResponseStatus.ERROR;
                response.message = "There was an error accessing the initial layer in the uploaded GeoJSON.";
                JsonResult res = Json(response, JsonRequestBehavior.AllowGet);
                res.MaxJsonLength = int.MaxValue;
                return res;
            }

            if (layer.GetFeatureCount(0) < 1)
            {
                response.status = ResponseStatus.ERROR;
                response.message = "At least one feature must be provided";
                JsonResult res = Json(response, JsonRequestBehavior.AllowGet);
                res.MaxJsonLength = int.MaxValue;
                return res;
            }

            String outputFilePath = String.Empty;
            String outputFileName = String.Empty;
            Driver outputDriver;
            DataSource outputDataSource;
            switch (featureConversion.exportFormat)
            {
                case ExportFormat.EXCEL:
                    // We do not set coordinate transformations on Excel as there are no geometries
                    outputFileName = String.Format("{0}.xlsx", Guid.NewGuid());
                    outputFilePath = String.Format("{0}\\{1}", outputDir, outputFileName);
                    outputDriver = Ogr.GetDriverByName("XLSX");
                    outputDataSource = outputDriver.CopyDataSource(dataSource, outputFilePath, null);
                    outputDataSource.Dispose();
                    break;
                case ExportFormat.KML:
                    // We do not set coordinate transformations on KML as it will always be in 4326
                    outputFileName = String.Format("{0}.kml", Guid.NewGuid());
                    outputFilePath = String.Format("{0}\\{1}", outputDir, outputFileName);
                    outputDriver = Ogr.GetDriverByName("KML");
                    outputDataSource = outputDriver.CopyDataSource(dataSource, outputFilePath, null);
                    outputDataSource.Dispose();
                    break;
                case ExportFormat.FILEGEODATABASE:
                    outputFileName = String.Format("{0}.gdb", Guid.NewGuid());
                    outputFilePath = String.Format("{0}\\{1}", outputDir, outputFileName);
                    outputDriver = Ogr.GetDriverByName("FileGDB");
                    outputDataSource = outputDriver.CopyDataSource(dataSource, outputFilePath, null);
                    //this.CopyDataSource(dataSource, outputDataSource, featureConversion);
                    break;
                case ExportFormat.SHAPEFILE:
                    // Create our shape file and associated data
                    // Creating a shapefile will create the shape file and its appropriate data in our target folder
                    Guid newFilePath = Guid.NewGuid();
                    outputFileName = String.Format("{0}.zip", newFilePath);
                    string outputFolderPath = String.Format("{0}\\{1}", GetConfigSetting("TempDirectory"), newFilePath);
                    outputFilePath = String.Format("{0}\\{1}", outputDir, outputFileName);
                    outputDriver = Ogr.GetDriverByName("ESRI Shapefile");

                    outputDataSource = outputDriver.CreateDataSource(outputFolderPath, null);
                    this.CopyDataSource(dataSource, outputDataSource, featureConversion);
                    outputDataSource.Dispose();

                    using (ZipFile zip = new ZipFile())
                    {
                        zip.AddDirectory(outputFolderPath);
                        zip.Save(outputFilePath);
                    }
                    break;
                case ExportFormat.GEOJSON:
                    outputFileName = String.Format("{0}.json", Guid.NewGuid());
                    outputFilePath = String.Format("{0}\\{1}", outputDir, outputFileName);
                    outputDriver = Ogr.GetDriverByName("GeoJSON");
                    outputDataSource = outputDriver.CreateDataSource(outputFilePath, null);
                    this.CopyDataSource(dataSource, outputDataSource, featureConversion);
                    outputDataSource.Dispose();
                    break;
            }

            dataSource.Dispose();

            if (System.IO.File.Exists(featurePath))
                System.IO.File.Delete(featurePath);

            response.status = ResponseStatus.SUCCESS;
            response.message = Url.Content(String.Format("~/{0}/{1}", GetConfigSetting("OutputDirectory"), outputFileName));
            JsonResult result = Json(response, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }

        private void CopyDataSource(DataSource sourceDataSource, DataSource targetDataSource, FeatureConversion featureConversionObject)
        {
            if (featureConversionObject.inputSpatialReference != 0 && featureConversionObject.outputSpatialReference != 0)
            {
                // Set out transformation srs
                SpatialReference in_sr = new SpatialReference("PROJCS[\"NAD83/UTMzone18N\",GEOGCS[\"NAD83\",DATUM[\"North_American_Datum_1983\",SPHEROID[\"GRS1980\",6378137,298.257222101,AUTHORITY[\"EPSG\",\"7019\"]],AUTHORITY[\"EPSG\",\"6269\"]],PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]],UNIT[\"degree\",0.01745329251994328,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4269\"]],UNIT[\"metre\",1,AUTHORITY[\"EPSG\",\"9001\"]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"latitude_of_origin\",0],PARAMETER[\"central_meridian\",-75],PARAMETER[\"scale_factor\",0.9996],PARAMETER[\"false_easting\",500000],PARAMETER[\"false_northing\",0],AUTHORITY[\"EPSG\",\"26918\"],AXIS[\"Easting\",EAST],AXIS[\"Northing\",NORTH]]");
                in_sr.ImportFromEPSG(featureConversionObject.inputSpatialReference);
                SpatialReference out_sr = new SpatialReference("PROJCS[\"NAD83/UTMzone18N\",GEOGCS[\"NAD83\",DATUM[\"North_American_Datum_1983\",SPHEROID[\"GRS1980\",6378137,298.257222101,AUTHORITY[\"EPSG\",\"7019\"]],AUTHORITY[\"EPSG\",\"6269\"]],PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]],UNIT[\"degree\",0.01745329251994328,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4269\"]],UNIT[\"metre\",1,AUTHORITY[\"EPSG\",\"9001\"]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"latitude_of_origin\",0],PARAMETER[\"central_meridian\",-75],PARAMETER[\"scale_factor\",0.9996],PARAMETER[\"false_easting\",500000],PARAMETER[\"false_northing\",0],AUTHORITY[\"EPSG\",\"26918\"],AXIS[\"Easting\",EAST],AXIS[\"Northing\",NORTH]]");
                out_sr.ImportFromEPSG(featureConversionObject.outputSpatialReference);

                OSGeo.OGR.Layer sourceLayer = sourceDataSource.GetLayerByIndex(0);
                wkbGeometryType geomType = sourceLayer.GetLayerDefn().GetGeomType();
                OSGeo.OGR.Layer targetLayer = targetDataSource.CreateLayer("Output_Layer", out_sr, geomType, null);
                FeatureDefn featureDfn = targetLayer.GetLayerDefn();
                Feature curFeature = sourceLayer.GetNextFeature();
                CoordinateTransformation ct = new CoordinateTransformation(in_sr, out_sr);

                while (curFeature != null)
                {
                    Geometry geometry = curFeature.GetGeometryRef();
                    geometry.Transform(ct);
                    Feature newFeature = new Feature(featureDfn);
                    newFeature.SetGeometry(geometry);
                    targetLayer.CreateFeature(newFeature);
                    curFeature = sourceLayer.GetNextFeature();
                }
            }
            else
                throw new ArgumentNullException("An input and output spatial reference is required");
        }

        private string GetConfigSetting(String settingName)
        {
            System.Configuration.Configuration webConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration(@"~/Web.config");
            return webConfig.AppSettings.Settings[settingName].Value;
        }
    }
}
