﻿using Danville_MapViewer.BLL;
using Danville_MapViewer.Models.Contexts;
using Danville_MapViewer.Models.Markup;
using Danville_MapViewer.Models.ViewModels;
using Danville_MapViewer.Models.ViewModels.Markup;
using System;
using System.Collections.Generic;
using System.Data.Sql;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Danville_MapViewer.Controllers
{
    [AllowAnonymous]
    public class MarkupController : Controller
    {

        /// <summary>
        /// Gets markup for the current user
        /// </summary>
        /// <returns>A list of MarkupCollection objects</returns>
        public JsonResult Get()
        {
            JsonResponse response = new JsonResponse();

            using (MarkupManager mm = new MarkupManager())
            {
                // We call 'Add' here in case the user does not currently exist. Add will return either the current MarkupUser or the newly created one.
                MarkupUser user = mm.AddMarkupUser(User.Identity.Name);
                Session["UserID"] = user.ID;

                List<MarkupCollectionViewModel> mcs = new List<MarkupCollectionViewModel>();
                foreach (var mc in user.MarkupCollections)
                {
                    mcs.Add(new MarkupCollectionViewModel
                    {
                        _id = mc.ID,
                        options = mc.options,
                        type = "MarkupLayer"
                    });
                }

                response.status = ResponseStatus.SUCCESS;
                response.message = String.Format("{0:d} markup collections returned", user.MarkupCollections.Count);
                response.data = mcs;
            }

            JsonResult res = Json(response, JsonRequestBehavior.AllowGet);
            res.MaxJsonLength = int.MaxValue;
            return res;
        }

        /// <summary>
        /// Retrieves a markup collection based on the id of the markup collection.
        /// </summary>
        /// <param name="id">ID of the Markup Collection to retrieve</param>
        /// <returns>A JSON message that signifies whether the collection was deleted.</returns>
        public JsonResult GetCollection(long id)
        {
            JsonResponse resp = new JsonResponse();

            using (MarkupManager mm = new MarkupManager())
            {
                MarkupCollection mc = mm.GetMarkupCollection(id);
                if (mc != null)
                {
                    resp.status = ResponseStatus.SUCCESS;
                    resp.message = "Markup collection returned";
                    resp.data = new MarkupCollectionViewModel
                    {
                        _id = mc.ID,
                        options = mc.options,
                        type = "MarkupLayer"
                    };
                }
                else
                {
                    resp.status = ResponseStatus.ERROR;
                    resp.message = "There was no collection matching the given id.";
                }

            }
            JsonResult res = Json(resp, JsonRequestBehavior.AllowGet);
            res.MaxJsonLength = int.MaxValue;
            return res;
        }

        /// <summary>
        /// Deletes a markup collection by its ID. If the current User ID matches the markup collection, we can delete.
        /// </summary>
        /// <param name="id">ID of the Markup Collection to delete</param>
        /// <returns>A JSON message that signifies whether the collection was deleted.</returns>
        public JsonResult Delete(long id)
        {
            JsonResponse response = new JsonResponse();

            using (MarkupManager mm = new MarkupManager())
            {
                MarkupCollection mc = mm.GetMarkupCollection(id);
                if (mc != null)
                {
                    if (Session["UserID"] == null)
                    {
                        MarkupUser user = mm.AddMarkupUser(User.Identity.Name);
                        Session["UserID"] = user.ID;
                    }
                    if (Session["UserID"] != null && mm.RemoveMarkupCollection(mc, (long)Session["UserID"]))
                    {
                        response.status = ResponseStatus.SUCCESS;
                        response.message = "Your markup collection has been deleted.";
                    }
                    else
                    {
                        response.status = ResponseStatus.ERROR;
                        response.message = "Failed to remove this markup collection. Do you have permissions?";
                    }
                }
                else
                {
                    response.status = ResponseStatus.ERROR;
                    response.message = "This markup collection does not exist.";
                }
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Commit an edit to an existing markup collection
        /// </summary>
        /// <param name="markupCollection">the markup collection to edit</param>
        /// <returns>a json response signifying success or failure</returns>
        [HttpPost]
        public JsonResult Edit(MarkupCollectionViewModel markupCollection)
        {
            JsonResponse response = new JsonResponse();
            if (ModelState.IsValid)
            {
                if (!markupCollection._id.HasValue)
                {
                    response.status = ResponseStatus.ERROR;
                    response.message = "Cannot update: No ID provided for markup collection";
                }
                else
                {
                    if (Session["UserID"] == null)
                    {
                        MarkupManager mManager = new MarkupManager();
                        MarkupUser user = mManager.AddMarkupUser(User.Identity.Name);
                        Session["UserID"] = user.ID;
                    }
                    using (MarkupManager mm = new MarkupManager((long)Session["UserID"]))
                    {
                        MarkupCollection mc = mm.GetMarkupCollection(markupCollection._id.Value);
                        if (mc == null)
                        {
                            response.status = ResponseStatus.ERROR;
                            response.message = "Could not find the given markup collection";
                        }
                        else
                        {
                            mc.options = markupCollection.options;
                            bool editMC = mm.EditMarkupCollection(mc);
                            if (editMC)
                            {
                                response.status = ResponseStatus.SUCCESS;
                                response.message = "Your markup collection has been updated.";
                            }
                            else
                            {
                                response.status = ResponseStatus.ERROR;
                                response.message = "The given markup collection could not be modified.";
                            }
                        }
                    }
                }
            }
            else
            {
                response.status = ResponseStatus.ERROR;
                response.message = "This is an invalid markup collection.";
            }


            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Add a new markup collection for the given user
        /// </summary>
        /// <param name="markupCollection"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Add(MarkupCollectionViewModel markupCollection)
        {
            JsonResponse response = new JsonResponse();
            if (ModelState.IsValid)
            {
                if (Session["UserID"] == null)
                {
                    using (MarkupManager mm = new MarkupManager())
                    {
                        var newUser = mm.AddMarkupUser(User.Identity.Name);
                        Session["UserID"] = newUser.ID;
                    }
                    //response.status = ResponseStatus.ERROR;
                    //response.message = "No user ID present to add new markup collection";
                }
                using (MarkupManager mm = new MarkupManager((long)Session["UserID"]))
                {
                    MarkupCollection mc = new MarkupCollection();
                    mc.options = markupCollection.options;
                    mc.UserID = (long)Session["UserID"];
                    bool addMc = mm.AddMarkupCollection(mc);
                    if (addMc)
                    {
                        response.status = ResponseStatus.SUCCESS;
                        response.message = "Your markup collection has been saved.";
                        response.data = mc.ID;
                    }
                    else
                    {
                        response.status = ResponseStatus.ERROR;
                        response.message = "Your markup collection could not be saved. Do you have permissions?";
                    }
                }
            }
            else
            {
                response.status = ResponseStatus.ERROR;
                response.message = "This is an invalid markup collection";
            }


            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds a session-based markup collection to be retrieved whenever a user navigates to the appropriately formatted URL which includes
        /// the markup collection id.
        /// </summary>
        /// <param name="mCol"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddSessionCollection(MarkupCollectionViewModel mCol)
        {
            JsonResponse resp = new JsonResponse();
            if (ModelState.IsValid)
            {
                using (MarkupManager mm = new MarkupManager())
                {
                    MarkupCollection mc = new MarkupCollection();
                    mc.options = mCol.options;
                    bool addMc = mm.AddMarkupCollection(mc);
                    if (addMc)
                    {
                        resp.status = ResponseStatus.SUCCESS;
                        resp.message = "Your markup collection has been saved.";
                        resp.data = mc.ID;
                    }
                    else
                    {
                        resp.status = ResponseStatus.ERROR;
                        resp.message = "Your markup collection could not be saved.";
                    }
                }
            }
            else
            {
                resp.status = ResponseStatus.ERROR;
                resp.message = "Invalid markup collection";
            }
            return Json(resp, JsonRequestBehavior.AllowGet);
        }
    }
}

