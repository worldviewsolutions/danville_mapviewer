﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Danville_MapViewer.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public JsonResult GenerateToken()
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(GetConfigSetting("TokenUrl"));
            var postData = "username=" + GetConfigSetting("TokenUsername");
            postData += "&password=" + GetConfigSetting("TokenPassword");
            postData += "&ip=" + GetConfigSetting("TokenRequestIp");
            postData += "&f=json&encrypted=false";
            var data = Encoding.ASCII.GetBytes(postData);
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            req.ContentLength = data.Length;
            
            using (var stream = req.GetRequestStream()) {
                stream.Write(data, 0, data.Length);
            }
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
            var respString = new StreamReader(resp.GetResponseStream()).ReadToEnd();
            JsonResult result = Json(respString, JsonRequestBehavior.AllowGet);
            return result;
        }

        private string GetConfigSetting(String settingName)
        {
            System.Configuration.Configuration webConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration(@"~/Web.config");
            return webConfig.AppSettings.Settings[settingName].Value;
        }
    }
}
