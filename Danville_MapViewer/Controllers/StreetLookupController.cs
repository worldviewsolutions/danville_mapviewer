﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Danville_MapViewer.Controllers
{
    public class StreetLookupController : Controller
    {
        //
        // GET: /StreetLookup/
        [Authorize]
        public JsonResult Index(string prefixText, int count)
        {
            prefixText = prefixText.Replace("+", " ");

            int resultLimit = 50;
            if (prefixText.Contains(" and ") || prefixText.Contains(" & "))
            {
                return Json(SearchCrossStreets(ref prefixText, ref resultLimit));
            }
            else
            {
                return Json(SearchStreets(ref prefixText, ref resultLimit));
            }
        }

        private static bool IsNumeric(string stringToCheck)
        {
            bool isNumber;
            double retNum;
            isNumber = Double.TryParse(stringToCheck, out retNum);
            return isNumber;
        }

        private string[] SearchStreets(ref string prefixText, ref int resultLimit)
        {
            List<string> items = new List<string>();

            string firstFrament = String.Empty;

            String[] fragmentSeparator = new string[] { " " };
            String[] targetFragmentArray = prefixText.Split(fragmentSeparator, StringSplitOptions.RemoveEmptyEntries);

            //check to see if the fragment is numeric. Numeric street names (eg. 5th) are not numeric.
            if (IsNumeric(targetFragmentArray[0]))
            {
                firstFrament = targetFragmentArray[0] + " ";
                prefixText = prefixText.Remove(0, targetFragmentArray[0].Length);
                prefixText = prefixText.Trim();
            }

            // make sure there are at least two characters to search
            if (prefixText.Length < 2) return null;

            prefixText = prefixText + "%";
            string sqlQuery = sqlQuery = "select top " + resultLimit + " street from vStreets WHERE STREET LIKE @Keyword order by street asc";

            using (SqlConnection sqlConn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["StreetFinder"].ConnectionString))
            {
                sqlConn.Open();

                System.Data.SqlClient.SqlCommand sqlCommand = new System.Data.SqlClient.SqlCommand(sqlQuery, sqlConn);
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@Keyword";
                param.Value = prefixText;
                sqlCommand.Parameters.Add(param);
                System.Data.SqlClient.SqlDataReader reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    items.Add(firstFrament + reader[0].ToString());
                }
            }

            return items.ToArray();

        }

        private string[] SearchCrossStreets(ref string prefixText, ref int resultLimit)
        {
            List<string> items = new List<string>();

            string firstFrament = String.Empty;

            String[] separator = new string[] { " and ", " & " };
            String[] textFragmentArray = prefixText.Split(separator, StringSplitOptions.RemoveEmptyEntries);

            // Get the last text fragment. This is the one we want to evaluate
            string street1 = textFragmentArray[0];
            string street2 = String.Empty;

            if (textFragmentArray.Length > 1)
            {
                street2 = textFragmentArray[1];
            }

            string sqlQuery = String.Empty;

            if (street2.Length > 0)
            {
                // add the wildcard to the second street
                street2 += "%";

                sqlQuery = sqlQuery = @"select street1, street2
										from(
										select street as street1, XSTREET as street2 from EnterpriseGIS.sde.Cross_Streets WHERE STREET = @street1 AND XSTREET LIKE @street2
										union
										select XSTREET as street1, street as street2  from EnterpriseGIS.sde.Cross_Streets WHERE STREET LIKE @street2 AND XSTREET = @street1
										) as tmp
										order by street2 asc";
            }
            else
            {
                sqlQuery = sqlQuery = @"select street1, street2
										from(
										select street as street1, XSTREET as street2 from EnterpriseGIS.sde.Cross_Streets WHERE STREET = @street1 
										union
										select XSTREET as street1, street as street2  from EnterpriseGIS.sde.Cross_Streets WHERE XSTREET = @street1
										) as tmp
										order by street2 asc";
            }



            using (SqlConnection sqlConn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["StreetFinder"].ConnectionString))
            {
                sqlConn.Open();


                System.Data.SqlClient.SqlCommand sqlCommand = new System.Data.SqlClient.SqlCommand(sqlQuery, sqlConn);
                sqlCommand.Parameters.Add(new SqlParameter("@street1", street1));
                if (street2.Length > 0)
                {
                    sqlCommand.Parameters.Add(new SqlParameter("@street2", street2));
                }
                System.Data.SqlClient.SqlDataReader reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    items.Add(reader[0].ToString() + " & " + reader[1].ToString());
                }
            }

            return items.ToArray();
        }
    }
}