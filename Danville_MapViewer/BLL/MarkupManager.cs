﻿using Danville_MapViewer.Models.Contexts;
using Danville_MapViewer.Models.Markup;
using System;
using System.Data;
using System.Data.Entity;
using System.Data.Sql;
using System.Data.SQLite;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Danville_MapViewer.BLL
{
   public class MarkupManager : IDisposable
   {

      #region Instance Members

      /// <summary>
      /// Indicates whether or not this instance created an instance of MarkupContext
      /// </summary>
      private bool localDB;

      /// <summary>
      /// Our DBContext instance to perform operations on the Markup DB   
      /// </summary>
      private MarkupContext _ctx;

      private string defaultName;

      private long userID;

      #endregion

      #region Constructors and Init

      public MarkupManager()
      {
         this.localDB = true;
         this.init(new MarkupContext(), -1);
      }

      public MarkupManager(long userID)
      {
         this.localDB = true;
         this.init(new MarkupContext(), userID);
      }

      public MarkupManager(MarkupContext ctx)
      {
         this.init(ctx, -1);
      }

      public MarkupManager(MarkupContext ctx, long userID)
      {
         this.init(ctx, userID);
      }

      private void init(MarkupContext ctx, long userID)
      {
         this._ctx = ctx;
         this.defaultName = "Anonymous";
         this.userID = userID;
      }

      #endregion

      #region User Methods

      public MarkupUser AddMarkupUser(string userName)
      {
         userName = String.IsNullOrEmpty(userName) ? this.defaultName : userName;
         MarkupUser user = this._ctx.Users.Where(u => u.Name == userName).FirstOrDefault();
         if (user != null)
            return user;
         else
         {
            user = new MarkupUser { Name = userName };
            this._ctx.Users.Add(user);
            this._ctx.SaveChanges();
            return user;
         }
      }

      public bool RemoveMarkupUser(long id)
      {
         MarkupUser user = this._ctx.Users.Where(u => u.ID == id).FirstOrDefault();
         if (user != null)
         {
            this._ctx.Users.Remove(user);
            this._ctx.SaveChanges();
            return true;
         }
         else
            return false;
      }

      public MarkupUser GetMarkupUser(long id)
      {
         return this._ctx.Users.Where(u => u.ID == id).FirstOrDefault();
      }

      public MarkupUser GetMarkupUser(string userName)
      {
         return this._ctx.Users.Where(u => u.Name == userName).FirstOrDefault();
      }

      #endregion

      #region MarkupCollection Methods

      public MarkupCollection GetMarkupCollection(long id)
      {
         return this._ctx.MarkupCollections.Where(mc => mc.ID == id).FirstOrDefault();
      }

      public bool AddMarkupCollection(MarkupCollection mc, long userId = -1)
      {
         using (MarkupContext ctx = new MarkupContext())
         {
            userId = userId > -1 ? userId : this.userID;

            if (mc == null)
               return false;

            mc.UserID = userId;
            ctx.MarkupCollections.Add(mc);
            ctx.SaveChanges();
            return true;
         }

         //catch (Exception)
         //{
         //    return false;
         //}
      }

      public bool RemoveMarkupCollection(MarkupCollection mc, long userId = -1)
      {
         userId = userId > -1 ? userId : this.userID;
         if (mc == null || !this.UserIdCanAccessMarkupCollection(mc, userId))
            return false;
         try
         {
            this._ctx.MarkupCollections.Remove(mc);
            this._ctx.SaveChanges();
            return true;
         }
         catch (Exception)
         {
            return false;
         }
      }

      public bool EditMarkupCollection(MarkupCollection mc, long userId = -1)
      {
         userId = userId > -1 ? userId : this.userID;
         if (mc == null || !this.UserIdCanAccessMarkupCollection(mc, userId))
         {
            return false;
         }
         try
         {
            this._ctx.Entry(mc).State = System.Data.Entity.EntityState.Modified;
            this._ctx.SaveChanges();
            return true;
         }
         catch (Exception)
         {
            return false;
         }
      }

      private bool UserIdCanAccessMarkupCollection(MarkupCollection mc, long userId = -1)
      {
         userId = userId > -1 ? userId : this.userID;
         return mc.UserID == userId;
      }


      #endregion

      #region Implemented Interface Methods

      public void Dispose()
      {
         if (this.localDB && this._ctx != null)
         {
            this._ctx.Dispose();
         }
      }

      #endregion


   }
}