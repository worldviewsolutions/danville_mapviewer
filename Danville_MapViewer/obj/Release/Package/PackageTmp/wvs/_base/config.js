console.log("wvs/_base/config.js");
define(["dojo/_base/lang","require"],
    function (lang,require) {  
        var config = {
            defaults: {
                    services: {
                        geometry: "http://vm101.worldviewsolutions.net:6080/arcgis/rest/services/Utilities/Geometry/GeometryServer",
                        print: "http://vm102.worldviewsolutions.net:6080/arcgis/rest/services/Utilities/PrintingTools/GPServer/Export Web Map Task",
                        exportService: null,
                        viewShed: null,
                        markup: require.toUrl("/MapViewer/Markup")
                    },
                    profile: {
                        enabled: false,
                        soe_config: {
                            profileChartParams: {
                                showElevationDifference: false,
                                showTemplateHelp: false
                            },
                            soeInfo: {
                                mapServiceUrl: "http://sit.sistemigis.it/sit/rest/services/Demo/Surface/MapServer",
                                name: "SurfaceUtility",
                                resourceIndex: 0
                            }
                        },
                        widget_config: {
                            enabledTools: []
                        }
                    },
                    whatNearbyEnabled: true,
                    map: null,
                    mapViewerController: null,
                    searchResultStore: null,
                    markupLayer: null,
                    identityManager: null,
                    symbols:{
                        polygon: { "color": [255, 255, 0, 77], "outline": { "color": [0, 0, 255, 128], "width": 4, "type": "esriSLS", "style": "esriSLSSolid" }, "type": "esriSFS", "style": "esriSFSSolid" },
                        point: { "angle": 0, "xoffset": -0.75, "yoffset": 11.25, "type": "esriPMS", "url": require.toUrl('../dijits/images/pushpins/teal.png'), "width": 32, "height": 32 },
                        line: { "color": [0, 0, 255, 128], "width": 4, "type": "esriSLS", "style": "esriSLSSolid" },
                        polyline: { "color": [0, 0, 255, 128], "width": 4, "type": "esriSLS", "style": "esriSLSSolid" },
                        selectionPolygon: { "color": [127, 255, 0, 74], "outline": { "color": [255, 255, 255, 255], "width": 1.5, "type": "esriSLS", "style": "dash" }, "type": "esriSFS", "style": "esriSFSSolid" },
                        highlightPolygon: { "color": [0, 0, 0, 0], "outline": { "color": [0, 255, 255, 1], "width": 1.5, "type": "esriSLS", "style": "solid" }, "type": "esriSFS", "style": "esriSFSSolid" }
                    },
                    hyperlinks: {
                        enabled: false,
                        hyperlinkFieldAliases: [
                            //{
                            //key: "ObjectID",
                            //replacement: "http://www.test.com/${val}", // EXAMPLES: HTTP link -> "http://www.example.com/${val}" File link -> "file:///C:/SomeDir/${val}.txt" Javascipt -> "alert('${layerId} has value ${val}')"
                            //type: 'link' // supported types: link, js
                            ////displayAs: 'Alternate Title'
                            //}
                        ]
                    },
                    showCustomMapIcons: false,
                    globalSpatialFilter: null,
                    buttonWidgetClass: "btn",
                    layers: {
                        ArcGISMapServiceLayer: {
                            // baseUrl: Base URL (Host) of the WCF services.
                            baseUrl: "http://localhost:63301/",

                            // serviceUrl: String
                            //      URL to the JSToolkit WCF service endpoint
                            serviceUrl: "AgsMapService.svc/json/"
                        },
                        templates: {}
                    },
                    proj4js: {
                        "102100": {
                            name: "SR-ORG:6",
                            def:  "+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs"
                        },
                        "4326": {
                            name: "EPSG:4326",
                            def: "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"
                        },
                        "32632": {
                            name: "EPSG:32632",
                            def: "+proj=utm +zone=32 +ellps=WGS84 +datum=WGS84 +units=m +no_defs"

                        },
                        "102747": {
                            name: "EPSG:2284",
                            def: "+proj=lcc +lat_1=37.96666666666667 +lat_2=36.76666666666667 +lat_0=36.33333333333334 +lon_0=-78.5 +x_0=3500000.0001016 +y_0=999999.9998983998 +ellps=GRS80 +datum=NAD83 +to_meter=0.3048006096012192 +no_defs"
                        }
                    }
            }
        };
        lang.setObject("window.wvsConfig", config);
        return config;
    }
 );