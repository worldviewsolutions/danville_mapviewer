﻿define([
    "dojo/_base/lang"
    , "dojo/dom"
    , "esri/units"
], function (
    lang
    , dom
    , Units
) {
    return {

        nauticalMilesPerDegreeLatitude: 60,

        feetPerDegreeLatitude: 365228.16,

        radiusOfEarthInMiles: 3963.1676,

        ellipsoidFactor: 0.0174532925199433,

        milesPerDegreeLonAtEquator: 69.172,

        feetPerMile: 5280,

        feetPerMeter: 3.28084,

        feetPerKilometer: 3280.84,

        clockwiseRotationMatrix: null,

        ctrClockwiseRotationMatrix: null,

        rotationalAngle: (Math.PI / 24),        //7.5 degrees as represented in radian units

        getDegreeDifferencePerFoot: function (initialPoint) {
            var diff = { lat: (1 / this.feetPerDegreeLatitude), lon: null };
            var initialLat = initialPoint.length ? initialPoint[1] : initialPoint.getLatitude();
            diff.lon = 1 / ((Math.cos(initialLat * this.ellipsoidFactor) * (this.milesPerDegreeLonAtEquator)) * this.feetPerMile);
            if (diff.lat < 0) { diff.lat *= -1; }
            if (diff.lon < 0) { diff.lon *= -1; }
            return diff;
        },

        getLineLength: function (polyLine, units) {
            var ptArray = polyLine.paths[0],
                totalLength = 0,
                dist = this.getDegreeDifferencePerFoot(ptArray[0]);
            for (var i = 1; i < ptArray.length; i++) {
                //Use pythagorean theorem to get the distance between two points along the line
                totalLength += Math.sqrt(((Math.abs(ptArray[i][0] - ptArray[i - 1][0]) / dist.lon) * (Math.abs(ptArray[i][0] - ptArray[i - 1][0])) / dist.lon)
                                + ((Math.abs(ptArray[i][1] - ptArray[i - 1][1]) / dist.lat) * (Math.abs(ptArray[i][1] - ptArray[i - 1][1]) / dist.lat)));
            }
            if (units) {
                if (units == Units.METERS) {
                    totalLength /= this.feetPerMeter;
                } else if (units == Units.MILES) {
                    totalLength /= this.feetPerMile;
                } else if (units == Units.KILOMETERS) {
                    totalLength /= this.feetPerKilometer
                }
            }
            return totalLength.toFixed(2);
        }
    };
});