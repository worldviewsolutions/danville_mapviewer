﻿console.log("wvs/dijits/Identify.js");

define([
    "dojo/_base/declare"
    , "dojo/_base/lang"
    , "dojo/_base/array"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dojo/text!./templates/Identify.html"
    , "dojo/dom-construct"
    , "dojo/dom-style"
    , "esri/tasks/IdentifyParameters"
    , "esri/tasks/IdentifyTask"
    , "esri/toolbars/draw"
    , "esri/toolbars/edit"
    , "../common/SearchResultsStore"
    , "../common/SearchResultsAdapter"
    , "dojo/on"
    , "esri/symbols/SimpleFillSymbol"
    , "esri/symbols/SimpleMarkerSymbol"
    , "esri/symbols/SimpleLineSymbol"
    , "dojo/_base/Color"
    , "esri/graphic"
    , "dojo/i18n!esri/nls/jsapi"
    , "dojo/dom-attr"
    , "dojo/dom-class"
    , "dojo/dom"
    , "esri/units"
    , "../common/Common"
    , "esri/geometry/Polygon"
    , "dijit/form/Button"
    , "dijit/form/ToggleButton"
    , "esri/symbols/jsonUtils"
    , "./MapLayerDropDown"
    , "esri/layers/LayerInfo"
    , "esri/layers/ArcGISDynamicMapServiceLayer"
    , "./Buffer"
    , "dijit/form/Select"
    , "../_base/config"
    , "dojo/_base/connect"
], function (declare, lang, array, _WidgetBase, _TemplatedMixin, template, domConstruct, domStyle, IdentifyParameters, IdentifyTask, Draw, Edit, SearchResultsStore, SearchResultsAdapter, on, SimpleFillSymbol, SimpleMarkerSymbol, SimpleLineSymbol, Color, Graphic, bundle, domAttr, domClass, dom, Units, Common, Polygon, Button, ToggleButton, symbolJsonUtils, MapLayerDropDown, LayerInfo, ArcGISDynamicMapServiceLayer, Buffer, Select, wvsConfig, connect) {
    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,

        declaredClass: "wvs.dijits.Identify",

        constructor: function (params, srcNodeRef) {
            if (!params.map) {
                throw new Error("no map for " + this.declaredClass);
            }
            if (!params.store) {
                throw new Error("no store defined for " + this.declaredClass);
            }

            var defaults = {
                _identifySelection: null,
                activeMode: null,
                _identifyParameters: new IdentifyParameters(),
                _identifyGraphic: null,
                _drawEndEvent: null,
                drawToolbarAddPointText: "Click here to identify",
                _searchResultAdapter: new SearchResultsAdapter()
            };

            lang.mixin(this, defaults);
            lang.mixin(this, params);

            this.polygonSymbol = this.polygonSymbol ? symbolJsonUtils.fromJson(this.polygonSymbol) : symbolJsonUtils.fromJson(wvsConfig.defaults.symbols.polygon);
            this.bufferSymbol = this.bufferSymbol ? symbolJsonUtils.fromJson(this.bufferSymbol) : symbolJsonUtils.fromJson(wvsConfig.defaults.symbols.polygon);
            // Save old draw toolbar text and give customs
            this._savedDrawToolbarAddPointText = bundle.toolbars.draw.addPoint;
            bundle.toolbars.draw.addPoint = this.drawToolbarAddPointText;
            this._drawToolbar = new Draw(this.map);
            this._editToolbar = new Edit(this.map);
        },
        postCreate: function () {
            this._createWidgets();
            this._createEvents();
            this.setMode("polygon");

            connect.publish("map-acquire", { target: this, acquired: true });
        },
        _createWidgets: function (){
            var self = this;

            this.layerSelect = new MapLayerDropDown({
                map: this.map,
                includeAllVisible: true,
                showOnlyVisibleLayers: true,
                exclusionList: ["ArcGISTiledMapServiceLayer", "FeatureLayer"],
                style: "display:inline-block;"
            }, this.layerSelect);
            this.layerSelect.startup();


            var buttonOnChange = function(){
                if(self.activeMode === this.get("value")){
                    this.set("checked", true);
                }
            };
            // Create internal widgets
            this.polygonButton = new ToggleButton({
                value: "polygon",
                label: "Polygon",
                onClick: lang.hitch(this, this._setModeFromButton),
                onChange: buttonOnChange
            }, this.polygonButton);

            this.pointButton = new ToggleButton({
                value: "point",
                label: "Point",
                onClick: lang.hitch(this, this._setModeFromButton),
                onChange: buttonOnChange
            }, this.pointButton);

            this.bufferButton = new ToggleButton({
                value: "buffer",
                label: "Buffer",
                onClick: lang.hitch(this, this._setModeFromButton),
                onChange: buttonOnChange
            }, this.bufferButton);
            //Add btn class for toggle button styles
            domClass.add(this.polygonButton.domNode, wvsConfig.defaults.buttonWidgetClass);
            domClass.add(this.pointButton.domNode, wvsConfig.defaults.buttonWidgetClass);
            domClass.add(this.bufferButton.domNode, wvsConfig.defaults.buttonWidgetClass);

            this.pointTolerance = new Select({
                options: [
                        { label: "2", value: 2 },
                        { label: "5", value: 5 },
                        { label: "10", value: 10 }
                    ]
            }, this.pointTolerance);

            this.bufferTool = new Buffer({
                map: this.map
            }, this.bufferTool);
        },
        _createEvents: function () {
            // Watches
            this.watch("_identifySelection", lang.hitch(this, function (name, oldVal, newVal) {
                this.setMode(this.activeMode);
            }));

            // Map Events
            this.own(on(this.layerSelect, "layer-selected", lang.hitch(this, function (layer) {
                    this.set("_identifySelection", layer);
                }))
            );
        },
        destroy: function(){
            bundle.toolbars.draw.addPoint = this._savedDrawToolbarAddPointText;
            this._drawToolbar.deactivate();
            this._editToolbar.deactivate();
            connect.publish("map-acquire", { target: this, acquired: false });
            this.map.graphics.remove(this._identifyGraphic);
            if (this._drawToolbarRemovePreviousGraphic) {
                this._drawToolbarRemovePreviousGraphic.remove();
            }
            this.inherited(arguments);
        },
        setMode: function (mode) {
            // remove existing geometry and draw toolbar event
            if (this._identifyGraphic) {
                this.map.graphics.remove(this._identifyGraphic);
            }
            if (this._drawEndEvent) {
                this._drawEndEvent.remove();
                this._drawEndEvent = null;
            }

            if (this._polygonEditEvent) {
                this._polygonEditEvent.remove();
                this._polygonEditEvent = null;
                this._editToolbar.deactivate();
            }

            if (this._drawToolbarRemovePreviousGraphic) {
                this._drawToolbarRemovePreviousGraphic.remove();
                this._drawToolbarRemovePreviousGraphic = null;
            }
            

            this.identifyError.innerHTML = "";

            this.activeMode = mode;

            switch (mode) {
                case "polygon":
                    domConstruct.place(this.commonToolOptions, this.polygonOptions);
                    domStyle.set(this.polygonOptions, "display","block");
                    domStyle.set(this.pointOptions, "display", "none");
                    domStyle.set(this.bufferOptions, "display", "none");
                    this.pointButton.set("checked", false);
                    this.polygonButton.set("checked", true);
                    this.bufferButton.set("checked", false);
                    break;
                case "point":
                    domConstruct.place(this.commonToolOptions, this.pointOptions);
                    domStyle.set(this.polygonOptions, "display", "none");
                    domStyle.set(this.pointOptions, "display", "block");
                    domStyle.set(this.bufferOptions, "display", "none");
                    this.pointButton.set("checked", true);
                    this.polygonButton.set("checked", false);
                    this.bufferButton.set("checked", false);
                    break;
                case "buffer":
                    domConstruct.place(this.commonToolOptions, this.bufferOptions);
                    domStyle.set(this.polygonOptions, "display", "none");
                    domStyle.set(this.pointOptions, "display", "none");
                    domStyle.set(this.bufferOptions, "display", "block");
                    this.pointButton.set("checked", false);
                    this.polygonButton.set("checked", false);
                    this.bufferButton.set("checked", true);
                    break;
            }

            if (this._identifySelection) {
                this._initiateIdentifyGeometry();
            }
        },
        _initiateIdentifyGeometry: function () {
            var self = this;

            // on draw end, initiate identify
            this._drawEndEvent = on(this._drawToolbar, "draw-end", function (evt) {
                // point and buffer
                if (self._identifyGraphic) {
                    self.map.graphics.remove(self._identifyGraphic);
                }

                var identifyGeometry = evt.geometry;
                
                // add graphic to map if not point
                if (self.activeMode !== "point") {
                    var graphic;
                    if (self.activeMode === "buffer") {
                        try{
                            var buffer = self.bufferTool.createBufferFromPoint(evt.geometry);
                        }
                        catch (e) {
                            Common.errorDialog(e.message);
                            return;
                        }
                        identifyGeometry = buffer.geometry;
                        graphic = buffer.graphic;
                    }
                    else if(self.activeMode === "polygon") {
                        graphic = new Graphic(evt.geometry, self.polygonSymbol);
                        self._editToolbar.activate(Edit.EDIT_VERTICES, graphic);
                        self._polygonEditEvent = on(self._editToolbar, "vertex-move-stop", function (evt) {
                            self._initiateIdentify(evt.graphic.geometry);
                        });
                    }
                    self._identifyGraphic = graphic;

                    // on map click, erase previous geometry. used for polygon tool.
                    if (self.activeMode === "polygon") {
                        self._drawToolbarRemovePreviousGraphic = on.once(self.map, "click", function (evt) {
                            self._polygonEditEvent.remove();
                            self._editToolbar.deactivate();
                            self.map.graphics.remove(self._identifyGraphic);
                        });
                    }

                    self.map.graphics.add(graphic);
                }

                // Identify with geometry
                self._initiateIdentify(identifyGeometry);
            });


            // activate the specified tool
            switch (this.activeMode) {
                case "polygon":
                    this._drawToolbar.activate(Draw.POLYGON);
                    break;
                case "point":
                    this._drawToolbar.activate(Draw.POINT);
                    break;
                case "buffer":
                    this._drawToolbar.activate(Draw.POINT);
                    break;
            }
        },
        _initiateIdentify: function (geometry) {
            var self = this;
            var processIdentify = lang.hitch(this, function (map, geometry, identifySelection) {
                var identifyParameters = new IdentifyParameters();
                identifyParameters.mapExtent = map.extent;
                identifyParameters.height = map.height;
                identifyParameters.width = map.width;
                identifyParameters.geometry = geometry;
                identifyParameters.tolerance = this.activeMode === "point" ? this.pointTolerance.get("value") : 0;
                identifyParameters.returnGeometry = true;
                identifyParameters.layerOption = IdentifyParameters.LAYER_OPTION_ALL;

                var identifyUrl = "";
                var identifyTask;
                var searchResultsAdapter = new SearchResultsAdapter();

                if (identifySelection.layer) {
                    // This means we are identifying on all visible layers within a MapServer
                    identifyUrl = identifySelection.layer.url;
                    identifyParameters.layerIds = array.map(identifySelection.visibleSubLayers, function (visibleSubLayer) {
                        return visibleSubLayer.id;
                    }, this);
                }
                else if (identifySelection.isInstanceOf(LayerInfo)) {
                    var layerIds = [];

                    // Only get visible layers from group layers
                    if (identifySelection.subLayerIds) {
                        layerIds = layerIds.concat(array.map(
                            array.filter(identifySelection._subLayerInfos, function (subLayerInfo) {
                                return subLayerInfo.getVisible();
                            }),
                            function (visibleSubLayerInfo) {
                                return visibleSubLayerInfo.id;
                            }));
                    }
                    else {
                        // If this isn't a group layer, disregard visiblity
                        layerIds.push(identifySelection.id);
                    }
                    var parentLayerInfo = identifySelection._parentLayerInfo;
                    while (!parentLayerInfo.isInstanceOf(ArcGISDynamicMapServiceLayer)) {
                        parentLayerInfo = parentLayerInfo._parentLayerInfo;
                    }

                    identifyUrl = parentLayerInfo.url;
                    identifyParameters.layerIds = layerIds;
                }
                else if (identifySelection.isInstanceOf(ArcGISDynamicMapServiceLayer)) {
                    // Identify all visible non-group layers within the given MapServer
                    identifyUrl = identifySelection.url;
                    var layerIds = array.map(
                        array.filter(identifySelection.layerInfos, function (layerInfo) {
                            return !layerInfo.subLayerIds && layerInfo.visible;
                        }),
                        function (visibleLayerInfo) {
                            return visibleLayerInfo.id;
                        });
                    identifyParameters.layerIds = layerIds;
                }
                else {
                    throw new Error("unsupported identify type for " + this.declaredClass);
                }

                identifyTask = new IdentifyTask(identifyUrl);

                this.store.clear();
                this.store.setLoadingData(true);
                
                identifyTask.execute(identifyParameters,
                    function (identifyResults) {

                        console.warn("identify complete...");
                        console.log(identifyResults);

                        var transformedResults = searchResultsAdapter.identifyToResultSet(identifyResults, identifyUrl);

                        transformedResults.then(
                            function (searchResult) {
                                console.log(searchResult);
                                self.store.put(searchResult);
                            }
                        );
                    },
                    function (error) {
                        self.store.setLoadingData(false);
                    }
                );
            });
            if (this._identifySelection instanceof Array) {
                array.forEach(this._identifySelection, function (selection) {
                    processIdentify(this.map, geometry, selection);
                }, this);
            }
            else {
                processIdentify(this.map, geometry, this._identifySelection);
            }
        },
        _setModeFromButton: function (evt) {
            this.setMode(evt.target.value);
        }
    });

});