﻿console.log("wvs/dijits/Measure.js");

define([
    "dojo/_base/declare"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dojo/text!./templates/Measure.html"
    , "dojo/_base/lang"
    , "dojo/on"
    , "dojo/_base/array"
    , "esri/units"
    , "dijit/form/Button"
    , "dijit/form/ToggleButton"
    , "dijit/form/Select"
    , "esri/toolbars/draw"
    , "esri/toolbars/edit"
    , "dojo/dom-style"
    , "esri/symbols/jsonUtils"
    , "esri/geometry/geodesicUtils"
    , "esri/geometry/webMercatorUtils"
    , "esri/geometry/Polyline"
    , "esri/geometry/Polygon"
    , "esri/geometry/Point"
    , "esri/graphic"
    , "esri/SpatialReference"
    , "dojo/dom-class"
    , "dojo/dom"
    , "../utilities/geoUtils"
    , "../common/Converter"
    , "../_base/config"
    , "dojo/_base/connect"
    , "../extensions/esri/geometry/Polygon"
    , "../extensions/esri/geometry/Polyline"
], function (declare, _WidgetBase, _TemplatedMixin, template, lang, on, array, Units, Button, ToggleButton, Select, Draw, Edit, domStyle, jsonUtils, geodesicUtils, webMercatorUtils, Polyline, Polygon, Point, Graphic, SpatialReference, domClass, dom, geoUtils, Converter, wvsConfig, connect) {
    var Measure = declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,

        declaredClass: "wvs.dijits.Measure",

        constructor: function (params, srcNodeRef) {
            this.inherited(arguments);

            // map: Map
            //      an instance of the map which the measure tool binds to
            this.map = null;

            // Required parameters
            if (!params.map) {
                throw new Error("no map provided for " + this.declaredClass);
            }

            // Constants
            this.lengthUnits = [
                //{ label: "Centimeters", value: "CENTIMETERS" },
                { label: "Feet", value: "FEET" },
                //{ label: "Inches", value: "INCHES" },
                { label: "Kilometers", value: "KILOMETERS" },
                { label: "Meters", value: "METERS" },
                { label: "Miles", value: "MILES", selected: true }
                //{ label: "Millimeters", value: "MILLIMETERS" },
                //{ label: "Nautical Miles", value: "NAUTICAL_MILES" },
                //{ label: "Yards", value: "YARDS" }
            ];
            this.areaUnitsData = [
                { label: "Acres", value: "ACRES", selected: true },
                { label: "Sq. Centimeters", value: "SQUARE_CENTIMETERS" },
                { label: "Sq. Decimeters", value: "SQUARE_DECIMETERS" },
                { label: "Sq. Feet", value: "SQUARE_FEET" },
                { label: "Sq. Inches", value: "SQUARE_INCHES" },
                { label: "Sq. Kilometers", value: "SQUARE_KILOMETERS" },
                { label: "Sq. Meters", value: "SQUARE_METERS" },
                { label: "Sq. Miles", value: "SQUARE_MILES" },
                { label: "Sq. Millimeters", value: "SQUARE_MILLIMETERS" },
                { label: "Sq. Yards", value: "SQUARE_YARDS" }
            ];

            lang.mixin(this, params);

            // Instance variables

            // graphic: Graphic
            //      points to the graphic being drawn or that is drawn 
            this.graphic = null;

            // mode: Integer    
            //      sets the mode for measurement (Measure.AREA or Measure.DISTANCE)
            this.mode = null;

            // perimeter: Number
            //      the perimeter of the drawn polygon (applicable to AREA mode only)
            this.perimeter = 0;

            // area: Number
            //      the area of the drawn polygon (applicable to AREA mode only)
            this.area = 0;

            // segmentLength: Number
            //      the segment length of the current point to the last point (applicable to DISTANCE mode only while drawing)
            this.segmentLength = 0;

            // totalLength: Number
            //      the total length of the drawn polyline (applicable to DISTANCE mode only)
            this.totalLength = 0;

            // perimeterUnits: Unit
            //      the display unit for the perimeter
            this.perimeterUnits = params.perimeterUnits ? Units[params.perimeterUnits.toUpperCase()] : Units.MILES;

            // distanceUnits: Unit
            //      the display unit for the lengths in DISTANCE mode
            this.distanceUnits = params.distanceUnits ? Units[params.distanceUnits.toUpperCase()] : Units.MILES;

            // areaUnits: Unit
            //      the display unit for the area
            this.areaUnits = Units.ACRES;

            // Symbols used for graphics
            this.polygonSymbol = jsonUtils.fromJson({ "color": [255, 255, 0, 77], "outline": { "color": [0, 0, 255, 128], "width": 1.5, "type": "esriSLS", "style": "esriSLSSolid" }, "type": "esriSFS", "style": "esriSFSSolid" });
            this.lineSymbol = jsonUtils.fromJson({ "color": [0, 0, 255, 255], "width": 2.25, "type": "esriSLS", "style": "esriSLSSolid" });

            // _drawToolbar: Draw
            //      an instance of the draw toolbar used to draw the graphics
            this._drawToolbar = new Draw(this.map, { showTooltips: false });

            // _editToolbar: Draw
            //      an instance of the edit toolbar used to edit the graphics
            this._editToolbar = new Edit(this.map);

            // _drawEvents: Event[]
            //      draw events (from dojo/on) are sent here as they are created
            this._drawEvents = [];

            // _editEvents: Event[]
            //      edit events (from dojo/on) are sent here as they are created
            this._editEvents = [];
        },
        postCreate: function () {
            var self = this;

            this.inherited(arguments);

            this._drawToolbar.setFillSymbol(this.polygonSymbol);
            this._drawToolbar.setLineSymbol(this.lineSymbol);
            this._drawToolbar.setMarkerSymbol(jsonUtils.fromJson(wvsConfig.defaults.symbols.point));

            this.distanceButton = new ToggleButton({
                label: "Distance",
                onClick: lang.hitch(this, function () { this.set("mode", Measure.DISTANCE); }),
                onChange: function () {
                    if (self.get("mode") === Measure.DISTANCE) {
                        this.set("checked", true);
                    }
                }
            }, this.distanceButton);
            this.areaButton = new ToggleButton({
                label: "Area",
                onClick: lang.hitch(this, function () { this.set("mode", Measure.AREA); }),
                onChange: function () {
                    if (self.get("mode") === Measure.AREA) {
                        this.set("checked", true);
                    }
                }
            }, this.areaButton);
            //Add btn class for toggle button styles
            domClass.add(this.distanceButton.domNode, wvsConfig.defaults.buttonWidgetClass);
            domClass.add(this.areaButton.domNode, wvsConfig.defaults.buttonWidgetClass);

            this.distanceUnitsSelect = new Select({
                options: this.lengthUnits,
                onChange: function () { self.set("distanceUnits", Units[this.get("value")]); }
            }, this.distanceUnitsSelect);

            this.perimeterUnitsSelect = new Select({
                options: this.lengthUnits,
                onChange: function () { self.set("perimeterUnits", Units[this.get("value")]); }
            }, this.perimeterUnitsSelect);

            this.areaUnitsSelect = new Select({
                options: this.areaUnitsData,
                onChange: function () { self.set("areaUnits", Units[this.get("value")]); }
            }, this.areaUnitsSelect);


            this._connectEvents();
            connect.publish("map-acquire", { target: this, acquired: true });
        },
        startup: function () {
            this.set("mode", Measure.DISTANCE);
            this.distanceUnitsSelect.set("value", Units[this.distanceUnits]);
            this.perimeterUnitsSelect.set("value", Units[this.perimeterUnits]);
            //Make sure to disable mouse events for all markup layers when the tool is active
            for (var i = 0; i < this.map.graphicsLayerIds.length; i++) {
                var lyr = this.map.getLayer(this.map.graphicsLayerIds[i]);
                lyr.disableMouseEvents();
            }
        },
        destroy: function () {
            this.resetMode();
            //Make sure to disable mouse events for all markup layers when the tool is active
            for (var i = 0; i < this.map.graphicsLayerIds.length; i++) {
                var lyr = this.map.getLayer(this.map.graphicsLayerIds[i]);
                lyr.enableMouseEvents();
            }
            connect.publish("map-acquire", { target: this, acquired: false });
            this.inherited(arguments);
        },
        _connectEvents: function () {
            // summary:
            //      binds events and watches for this widget

            // onModeChange
            this.watch("mode", lang.hitch(this, function (name, oldVal, newVal) {
                this.resetMode();


                if (newVal === Measure.DISTANCE) {
                    this.distanceButton.set("checked", true);
                    this.areaButton.set("checked", false);
                    this.enableDistanceMode();
                }
                else {
                    this.distanceButton.set("checked", false);
                    this.areaButton.set("checked", true);
                    this.enableAreaMode();
                }

            }));

            // Value changes

            //onPerimeterChange
            this.watch("perimeter", lang.hitch(this, function (name, oldVal, newVal) {
                this.perimeterResultLabel.innerHTML = newVal;
            }));

            //onAreaChange
            this.watch("area", lang.hitch(this, function (name, oldVal, newVal) {
                this.areaResultLabel.innerHTML = newVal;
            }));

            //onSegmentLengthChange
            this.watch("segmentLength", lang.hitch(this, function (name, oldVal, newVal) {
                this.segmentLengthResultLabel.innerHTML = newVal;
            }));

            // onTotalLengthChange
            this.watch("totalLength", lang.hitch(this, function (name, oldVal, newVal) {
                this.totalLengthResultLabel.innerHTML = newVal;
            }));

            // Unit changing 

            // onPerimeterUnitsChange
            this.watch("perimeterUnits", lang.hitch(this, function (name, oldVal, newVal) {
                if (newVal && this.graphic) {
                    this.set("perimeter", this.getLengthOfLineInStatePlane(new Polyline(this.graphic.geometry.rings), newVal));
                }
            }));

            // onAreaUnitsChange
            this.watch("areaUnits", lang.hitch(this, function (name, oldVal, newVal) {
                if (newVal && this.graphic) {
                    var newPoly = this.reprojectPolygon(this.graphic.geometry);
                    var area = geodesicUtils.geodesicAreas([newPoly], this.areaUnits);
                    area[0] = area[0] < 0 ? area[0] * -1 : area[0];
                    this.set("area", area[0].toFixed(2));
                }
            }));

            // onDistanceUnitsChange
            this.watch("distanceUnits", lang.hitch(this, function (name, oldVal, newVal) {
                if (newVal && this.graphic) {
                    this.set("totalLength", this.getLengthOfLineInStatePlane(this.graphic.geometry, newVal));
                }
            }));
        },

        getLengthOfLineInStatePlane: function (geometry, units) {
            var lineForMeasure = new Polyline({ wkid: 102100 }),
                newPath = [];
            var src = new Proj4js.Proj("EPSG:2284"),
                dest = new Proj4js.Proj("EPSG:4326");
            for (var i = 0; i < geometry.paths[0].length; i++) {
                var pt = new Proj4js.Point(geometry.paths[0][i][0], geometry.paths[0][i][1]);
                Proj4js.transform(src, dest, pt);
                newPath.push([pt.x, pt.y]);
            }
            lineForMeasure.addPath(newPath);
            var length = geoUtils.getLineLength(lineForMeasure, units);
            if (!isNaN(length)) {
                return length;
            } else {
                return 0.00;
            }
        },

        reprojectPolygon: function (geometry) {
            var polyForMeasure = new Polygon({ wkid: 4326 }),
                newRing = [];
            var src = new Proj4js.Proj("EPSG:2284"),
                dest = new Proj4js.Proj("EPSG:4326");
            for (var i = 0; i < geometry.rings[0].length; i++) {
                var pt = new Proj4js.Point(geometry.rings[0][i][0], geometry.rings[0][i][1]);
                Proj4js.transform(src, dest, pt);
                newRing.push([pt.x, pt.y]);
            }
            polyForMeasure.addRing(newRing);
            return polyForMeasure;

        },

        enableAreaMode: function () {
            // summary:
            //      enables area mode
            domStyle.set(this.areaTool, { display: "block" });
            domStyle.set(this.distanceTool, { display: "none" });
            this._drawToolbar.activate(Draw.POLYGON);

            this._drawCount = 0;
            var onDraw = on(this.map.graphics, "graphic-draw", lang.hitch(this, function (evt) {
                var geometry = evt.graphic.geometry;
                if (this._drawCount === 0) {
                    this._initGeometry = geometry;
                }
                else if (this._drawCount % 3 === 0 && this._initGeometry !== geometry) {
                    // Perimeter
                    var target, source;
                    if (geometry instanceof Polyline) {
                        target = new Polyline(geometry.toJson());
                    }
                    if (this._initGeometry instanceof Polygon) {
                        source = new Polygon(this._initGeometry.toJson());
                    } else if (this._initGeometry instanceof Polyline) {
                        source = new Polyline(this._initGeometry.toJson());
                    }
                    if (target instanceof Polyline) {
                        var polyline = new Polyline(source);
                        polyline.addPath(source.rings[0]);
                        polyline.paths[0].push((target.paths[0][1]));
                        var perimeter = this.getLengthOfLineInStatePlane(polyline, this.perimeterUnits);
                        if (!isNaN(perimeter)) {
                            this.set("perimeter", perimeter);
                        }

                        // Area
                        var areaPolygon = new Polygon(source.toJson());
                        areaPolygon.addRing(source.rings[0]);
                        areaPolygon.insertPoint(0, areaPolygon.rings[0].length - 1, new Point(target.paths[0][1]));
                        areaPolygon.insertPoint(0, areaPolygon.rings[0].length - 1, areaPolygon.getPoint(0, 0));
                        var newPoly = this.reprojectPolygon(areaPolygon);
                        var area = geodesicUtils.geodesicAreas([newPoly], this.areaUnits);
                        area[0] = area[0] < 0 ? area[0] * -1 : area[0];
                        if (!isNaN(area)) {
                            this.set("area", area[0].toFixed(2));
                        }
                    }
                }
                this._drawCount++;
            }));
            this._drawEvents.push(onDraw);

            var onDrawEnd = on(this._drawToolbar, "draw-end", lang.hitch(this, function (evt) {
                onDraw.remove();

                setTimeout(lang.hitch(this, function () {
                    // Perimeter
                    var polyline = new Polyline(evt.geometry.rings[0]);
                    var perimeter = this.getLengthOfLineInStatePlane(polyline, this.perimeterUnits);
                    if (!isNaN(perimeter)) {
                        this.set("perimeter", perimeter);
                    }

                    // Area
                    var newPoly = this.reprojectPolygon(evt.geometry);
                    var area = geodesicUtils.geodesicAreas([newPoly], this.areaUnits);
                    area[0] = area[0] < 0 ? area[0] * -1 : area[0];
                    if (!isNaN(area)) {
                        this.set("area", area[0].toFixed(2));
                    }

                    // Create graphic & Add to map
                    var graphic = new Graphic(evt.geometry, this._drawToolbar.fillSymbol);
                    this.graphic = graphic;
                    this.map.graphics.add(graphic);


                    // Stop drawing and start editing
                    this._editToolbar.activate(Edit.EDIT_VERTICES, this.graphic);

                    // When done editing 
                    var editEvent = on(this._editToolbar, "vertex-move-stop", lang.hitch(this, function (evt) {
                        console.log("vertex-move-stop");
                        // Perimeter
                        var polyline = new Polyline(evt.graphic.geometry.rings[0]);
                        var perimeter = this.getLengthOfLineInStatePlane(polyline, this.perimeterUnits);
                        if (!isNaN(perimeter)) {
                            this.set("perimeter", perimeter);
                        }
                        // Area
                        var newPoly = this.reprojectPolygon(evt.graphic.geometry);
                        var area = geodesicUtils.geodesicAreas([newPoly], this.areaUnits);
                        area[0] = area[0] < 0 ? area[0] * -1 : area[0];
                        if (!isNaN(area)) {
                            this.set("area", area[0].toFixed(2));
                        }
                    }));
                    this._drawEvents.push(editEvent);

                    this._drawEvents.push(on.once(this.map, "click", lang.hitch(this, function (evt) {
                        this.resetMode();
                        this.enableAreaMode();
                    })));

                }), 100);
            }));
            this._drawEvents.push(onDrawEnd);
        },
        enableDistanceMode: function () {
            // summary:
            //      enables distance mode
            domStyle.set(this.distanceTool, { display: "block" });
            domStyle.set(this.areaTool, { display: "none" });
            this._drawToolbar.activate(Draw.POLYLINE);

            var onDraw = on(this.map.graphics, "graphic-draw", lang.hitch(this, function (evt) {
                var geometry = evt.graphic.geometry;
                if (this._drawCount === 0) {
                    this._initGeometry = geometry;
                }
                else if (this._drawCount % 3 === 0 && this._initGeometry !== geometry) {
                    // Total length
                    var source = new Polyline(geometry.toJson());
                    var target = new Polyline(this._initGeometry.toJson());
                    target.addPath(source.paths[0]);
                    var totalLength = this.getLengthOfLineInStatePlane(target, this.distanceUnits);
                    if (!isNaN(totalLength)) {
                        this.set("totalLength", totalLength);
                    }

                    // Segment length
                    var segmentLength = this.getLengthOfLineInStatePlane(source, this.distanceUnits);
                    if (!isNaN(segmentLength)) {
                        this.set("segmentLength", segmentLength);
                    }
                }

                this._drawCount++;

            }));
            this._drawEvents.push(onDraw);

            var onDrawEnd = on.once(this._drawToolbar, "draw-end", lang.hitch(this, function (evt) {
                onDraw.remove();

                setTimeout(lang.hitch(this, function () {
                    // Length
                    var length = this.getLengthOfLineInStatePlane(evt.geometry, this.distanceUnits);
                    if (!isNaN(length)) {
                        this.set("totalLength", length);
                    }

                    // Create graphic & Add to map
                    var graphic = new Graphic(evt.geometry, this._drawToolbar.lineSymbol);
                    this.graphic = graphic;
                    this.map.graphics.add(graphic);

                    this.set("segmentLength", "N/A");

                    // Stop drawing and start editing
                    this._editToolbar.activate(Edit.EDIT_VERTICES, this.graphic);

                    // When done editing 
                    var editEvent = on(this._editToolbar, "vertex-move-stop", lang.hitch(this, function (evt) {
                        // Length
                        var length = this.getLengthOfLineInStatePlane(evt.graphic.geometry, this.distanceUnits);
                        if (!isNaN(length)) {
                            this.set("totalLength", length);
                        }
                    }));
                    this._drawEvents.push(editEvent);

                    this._drawEvents.push(on.once(this.map, "click", lang.hitch(this, function (evt) {
                        this.resetMode();
                        this.enableDistanceMode();
                    })));
                }), 100);
            }));

            this._drawEvents.push(onDrawEnd);
        },
        resetMode: function () {
            // summary:
            //      'resets' the mode, meaning everything goes back to defaults

            // Remove the graphic and events
            this._drawToolbar.deactivate();
            this._editToolbar.deactivate();
            array.forEach(this._drawEvents, function (drawEvent) {
                drawEvent.remove();
            });
            this._drawEvents = [];

            // remove the graphic from the map
            if (this.graphic) {
                this.map.graphics.remove(this.graphic);
                this.graphic = null;
            }

            // Reset values
            this.set("perimeter", 0);
            this.set("area", 0);
            this.set("segmentLength", 0);
            this.set("totalLength", 0);
            this._drawCount = 0;
        }
    });

    // DISTANCE: Integer
    //      constant for distance mode
    Measure.DISTANCE = 0;
    // AREA: Integer
    //      constant for area mode
    Measure.AREA = 1;

    return Measure;
});