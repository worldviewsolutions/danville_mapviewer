﻿
define([
    "dojo/_base/declare",
    "dojo/parser",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dojo/text!../templates/FilterManager.html",
    "dijit/registry",
    "./Filter",
    "dijit/form/Form",
    "dijit/Dialog",
    "dojo/_base/array",
    "dojo/store/Memory",
    "dijit/layout/ContentPane",
    "dojo/dom-construct",
    "dojo/dom",
    "dojo/_base/lang",
    "dijit/form/FilteringSelect",
    "dijit/form/Button",
    "esri/tasks/QueryTask",
    "esri/tasks/query",
    "../MapLayerDropDown",
    "esri/layers/LayerInfo",
    "dojo/on",
    "dijit/Tooltip",
    "dojo/mouse",
    "dojo/ready",
    "esri/layers/FeatureLayer",
    "dojo/dom-class",
    "../../common/SearchResultsAdapter"
], function (
    declare,
    parser,
    _WidgetBase,
    _TemplatedMixin,
    template,
    registry,
    Filter,
    Form,
    Dialog,
    array,
    Memory,
    ContentPane,
    domConstruct,
    dom,
    lang,
    FilteringSelect,
    Button,
    QueryTask,
    Query,
    MapLayerDropDown,
    LayerInfo,
    on,
    Tooltip,
    mouse,
    ready,
    FeatureLayer,
    domClass
) {
    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,

        constructor: function (params, srcNodeRef) {
            if (!params.mapService) {
                throw new Error("no map service provided");
            }
            if (!params.mapService.isInstanceOf(FeatureLayer) && !params.subLayerInfo) {
                throw new Error("no sub layer info provided");
            }

            if (params.mapService.isInstanceOf(FeatureLayer)) {
                this.mapServiceId = params.mapService.id;
            }
            else {
                if (params.mapService.version < 10) {
                    throw new Error("setting layer definitions on a dynamic service is only supported in ArcGIS Server 10 or greater");
                }
                this.mapServiceId = params.subLayerInfo.name;
            }

            this.caseSensitive = params.caseSensitive ? params.caseSensitive : false;

            lang.mixin(this, params);

            var instanceVars = {
                filters: [],
                fieldSelectOptions: [],
                joinOperatorSelect: null,
                filterManagerForm: null
            };

            lang.mixin(this, instanceVars);

        },

        postCreate: function () {
            var self = this;
            this.inherited(arguments);

            // Create internal widgets

            this.joinOperatorSelect = new FilteringSelect({
                required: true
            }, this.joinOperatorSelect);

            this.applyButton = new Button({
                onClick: lang.hitch(this, this.applyFilters),
                label: "Apply"
            }, this.applyButton);

            this.clearButton = new Button({
                onClick: lang.hitch(this, this.clearFilters),
                label: "Clear"
            }, this.clearButton);

            this.closeButton = new Button({
                onClick: lang.hitch(this, this.closeManager),
                label: "Close"
            }, this.closeButton);

            this.joinOperatorSelect.set({
                store: new Memory({
                    data: [
                        { name: "Any", id: ' OR ' },
                        { name: "All", id: ' AND ' }
                    ]
                })
            });
            this.filterManagerForm = new Form({
            }, this.filterManagerForm);

            if (this.mapService.isInstanceOf(FeatureLayer) && this.mapService._filters) {
                this.joinOperatorSelect.set("value", this.mapService._filters.joinOperator);
                array.forEach(this.mapService._filters.filters, function (filter) {
                    this.filters.push(filter);
                    filter.placeAt(this.filterManagerForm);
                }, this);
            }
            else if (!this.mapService.isInstanceOf(FeatureLayer) && this.subLayerInfo._filters) {
                this.joinOperatorSelect.set("value", this.subLayerInfo._filters.joinOperator);
                array.forEach(this.subLayerInfo._filters.filters, function (filter) {
                    this.filters.push(filter);
                    filter.placeAt(this.filterManagerForm);
                }, this);
            }
            else
                this.joinOperatorSelect.set("value", ' AND '); // Make All the Default

            if (!this.mapService.isInstanceOf(FeatureLayer) && !this.mapService.fields) {
                domClass.remove(this.addFilterLink, "icon-plus");
                domClass.add(this.addFilterLink, "icon-spin icon-spinner");
                this.mapService.getExtendedLayerInfo(lang.hitch(this, function () {
                    this._registerEventsAndWatches();
                    domClass.remove(this.addFilterLink, "icon-spin icon-spinner");
                    domClass.add(this.addFilterLink, "icon-plus");
                }));
            }
            else {
                this._registerEventsAndWatches();
            }
        },
        startup: function(){
            this.inherited(arguments);

            this.dialog = new Dialog({
                title: "Filter Manager",
                "class": "nonModal"
            }, this.dialog);
        },
        _registerEventsAndWatches: function () {
            // Events
            this.own(on(this.addFilterLink, "click", lang.hitch(this, function (evt) {
                this.addFilter();
            })));
        },
        addFilter: function () {
            // Create new filter and add it to our list of filters
            var filter = new Filter({
                filterManager: this,
                fieldSelectOptions: this.mapService.isInstanceOf(FeatureLayer) ?  this.mapService.fields : this.subLayerInfo.fields
            });
            this.filters.push(filter);
            filter.placeAt(this.filterManagerForm);
            var href = domConstruct.create("a", { href: "#", innerHTML: "<span class='icon-trash icon-large'></span>" }, filter.filterForm.domNode);
            on(href, "click", lang.hitch(this, function (evt) {
                this.removeFilter(filter);
            }));
        },
        clearFilters: function(){
            array.forEach(this.filters, function (filter) {
                this.removeFilter(filter);
            }, this);

            if (this.mapService.isInstanceOf(FeatureLayer)) {
                this.mapService.setDefinitionExpression(this.mapService.defaultDefinitionExpression);
                delete this.mapService._filters;
            }
            else {
                this.mapService.setLayerDefinitions([]);
                this.subLayerInfo.setFilters(null);
            }
        },
        removeFilter: function (filter) {

            // Remove the filter node from the DOM
            domConstruct.destroy(filter.filterContainer);

            // Remove filter
            this.filters = array.filter(this.filters, function (item) {
                return filter.id != item.id;
            });
        },
        applyFilters: function () {
            var whereClause = this.getWhereClause();
            if (whereClause !== null) {
                if (this.mapService.isInstanceOf(FeatureLayer)) {
                    this.mapService.setDefinitionExpression(whereClause);
                    this.mapService._filters = { joinOperator: this.joinOperatorSelect.get("value"), filters: this.filters };
                }
                else {
                    var layerDefs = this.mapService.layerDefinitions ? this.mapService.layerDefinitions.slice() : [];
                    layerDefs[this.subLayerInfo.id] = whereClause;
                    this.mapService.setLayerDefinitions(layerDefs);
                    if (!whereClause) {
                        this.subLayerInfo.setFilters(null);
                    }
                    else {
                        this.subLayerInfo.setFilters({ joinOperator: this.joinOperatorSelect.get("value"), filters: this.filters });
                    }
                }

            }
        },
        getWhereClause: function () {
            var self = this,
                whereClause = '',
                activeLayer = this.activeLayer,
                filtersPassValidation = true;

            // Check that all the filters pass validation
            for (var i = 0 ; i < this.filters.length; i++) {
                if (!this.filters[i].validate()) {
                    filtersPassValidation = false;
                    break;
                }
            }
            if (this.filterManagerForm.validate() && filtersPassValidation) {
                // Build the query
                array.forEach(this.filters, function (filter, index) {
                    var fQuery = filter.getQuery();
                    if (fQuery != null) {
                        if (index < self.filters.length - 1) {
                            whereClause += fQuery + self.joinOperatorSelect.item.id;
                        }
                        else {
                            whereClause += fQuery;
                        }
                    }
                });

                return whereClause;
            }

            return null;
        },

        closeManager: function () {
            this.dialog.hide();
            this.destroyRecursive();
        }

    });
});