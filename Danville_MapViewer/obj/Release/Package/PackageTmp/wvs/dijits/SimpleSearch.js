﻿define([
    "dojo/_base/declare"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dojo/Evented"
    , "dojo/text!./templates/SimpleSearch.html"
    , "dojo/_base/lang"
    , "dojo/_base/array"
    , "dojo/on"
    , "dojo/query"
    , "dojo/dom-style"
    , "dojo/dom-construct"
    , "dojo/dom-class"
    , "dojo/dom-attr"
    , "dijit/form/TextBox"
    , "dijit/form/FilteringSelect"
    , "dijit/form/ComboBox"
    , "dijit/form/Select"
    , "dijit/form/Button"
    , "dojo/store/Memory"
    , "../common/Common"
    // QueryAction imports
    , "esri/tasks/query"
    , "esri/tasks/QueryTask"
    , "dojo/Deferred"
    , "../_base/config"
    , "../common/SearchResultsAdapter"
    , "dojo/has"
    // GeocodeAction imports
    , "esri/request"
    , "esri/tasks/locator"
    , "esri/geometry/Point"
    , "../common/InfoTemplateManager"
    , "esri/graphic"
    , "esri/symbols/jsonUtils"
    , "dojo/_base/connect"
    , "dojo/domReady!"
], function (declare, _WidgetBase, _TemplatedMixin, Evented, template, lang, array, on, query, domStyle, domConstruct, domClass, domAttr, TextBox, FilteringSelect, ComboBox, Select, Button, Memory, Common, Query, QueryTask, Deferred, wvsConfig, SearchResultsAdapter, has, esriRequest, Locator, Point, InfoTemplateManager, Graphic, symbolJsonUtils, connect) {
    // module:
    //      wvs/dijits/SimpleSearch
    var SimpleSearch = declare([_WidgetBase, _TemplatedMixin, Evented], {

        templateString: template,

        declaredClass: "wvs.dijits.SimpleSearch",

        searchLabel: "Search",

        constructor: function (params, srcNodeRef) {
            // actions: Object[]
            //      an array of actions with the following format: { label: NAME_OF_ACTION, value: ActionObject|ActionInstance
            this.actions = [];

            if (!params.map) {
                throw new Error("a map instance is required for " + this.declaredClass);
            }
            else {
                this.map = params.map;
            }

            if (!params.actions && !params.actions instanceof Array && has("local-storage") && !localStorage.getItem("simpleSearch")) {
                throw new Error("an array of actions are required for " + this.declaredClass);
            }
            else {
                if (params.actions) {
                    this._parseActions(params.actions);
                    delete params.actions;
                }
                this._loadFromLocalStorage();
            }

            this.actionOptions = array.map(this.actions, function (action, i) {
                return { label: action.label, value: i };
            }, this);

            this.emptyStore = new Memory({ data: [] });
            this._dropDownOnClick = null;

            lang.mixin(this, params);
        },

        postCreate: function () {
            var self = this;

            if (this.actions.length > 1) {
                for (var i = 0; i < this.actions.length; i++) {
                    on(this.actions[i].value, 'search-result-acquired', function (res) {
                        self.emit('search-result-acquired', res);
                    });
                }
                this.actionSelect = new Select({
                    options: this.actionOptions,
                    onChange: function (value) {
                        var action = self._getCurrentAction();
                        if (!self._spatialFilter && action instanceof _QueryAction && action.useGlobalSpatialFilter && action.requireGlobalSpatialFilter) {
                            self.searchSelect.set("disabled", true);
                            return;
                        }
                        self.searchSelect.set("disabled", false);
                    }
                }, this.actionSelect);

                this.actionSelect.set("value", 0);
            }

            this.searchSelect = new ComboBox({
                store: this.emptyStore,
                autoComplete: false,
                searchAttr: "name",
                hasDownArrow: false,
                invalidMessage: "",
                required: false,
                placeHolder: this.actions.length === 1 ? this.actions[0].label : "",
                autoWidth: true,
                queryExpr: "*${0}*",
                onClick: function () {
                    var action = self._getCurrentAction();
                    if (action instanceof _LookupAction) {
                        action.loadDropdown().then(
                           function (store) {
                               self.searchSelect.set("store", store);
                               self.searchSelect.loadAndOpenDropDown();
                               if (!self._dropDownOnClick) {
                                   self._dropDownOnClick = on(self.searchSelect.dropDown, "click", function (node) {
                                       if (self.searchSelect.item.callback) {
                                           self.searchSelect.item.callback();
                                       }
                                       else {
                                           console.log("this", this);
                                       }
                                   });
                               }
                           },
                           function (err) {
                               Common.errorDialog(err);
                           });
                    }
                },
                onKeyUp: function (value) {
                    var action = self._getCurrentAction();
                    if (value.which == 38 || value.which == 40) {
                        return;
                    }

                    // on ENTER, submit action
                    if (value.which === 13 && this._getDisplayedValueAttr()) {
                        action.submit(this._getDisplayedValueAttr());
                    }

                    var autoCompleteLimit = 4;
                    // Execute action, if at least 4 characters
                    if (action instanceof _LookupAction) {
                        autoCompleteLimit = 1;
                    }
                    if (this._getDisplayedValueAttr().length >= autoCompleteLimit && action.autocompleteEnabled) {
                        action.autoComplete(this._getDisplayedValueAttr()).then(
                            function (store) {
                                self.searchSelect.set("store", store);
                                self.searchSelect.loadAndOpenDropDown();

                                if (!self._dropDownOnClick) {
                                    self._dropDownOnClick = on(self.searchSelect.dropDown, "click", function (node) {
                                        if (self.searchSelect.item.callback) {
                                            self.searchSelect.item.callback();
                                        }
                                        else {
                                            console.log("this", this);
                                        }
                                    });
                                }
                            },
                            function (err) {
                                Common.errorDialog(err);
                            }
                        );
                    }
                    else {
                        this.set("store", self.emptyStore);
                    }
                }
            }, this.searchSelect);

            this.searchButton = new Button({
                label: this.searchLabel,
                onClick: function () {
                    if (self.searchSelect._getDisplayedValueAttr()) {
                        (self._getCurrentAction()).submit(self.searchSelect._getDisplayedValueAttr());
                    }
                }
            }, this.searchButton);

            connect.subscribe("globalSpatialFilterChanged", function (filter) {
                var action = self._getCurrentAction();
                self._spatialFilter = filter;
                if (!filter && action instanceof _QueryAction && action.useGlobalSpatialFilter && action.requireGlobalSpatialFilter) {
                    self.searchSelect.set("disabled", true);
                    return;
                }
                self.searchSelect.set("disabled", false);
            });
        },
        _getCurrentAction: function () {
            if (this.actions.length == 1) {
                return this.actions[0].value;
            }
            else if (this.actions.length > 1)
                return this.actions[this.actionSelect.value].value;
        },

        _parseActions: function (actions) {
            array.forEach(actions, function (action) {
                var actionObject = action.value;
                if (!actionObject.declaredClass) {
                    switch (actionObject.type) {
                        case "query":
                            this.actions.push({ label: action.label, value: new SimpleSearch.QueryAction(lang.mixin({ map: this.map }, actionObject)) });
                            break;
                        case "geocode":
                            this.actions.push({ label: action.label, value: new SimpleSearch.GeocodeAction(lang.mixin({ map: this.map }, actionObject)) });
                            break;
                        case "lookup":
                            this.actions.push({ label: action.label, value: new SimpleSearch.LookupAction(lang.mixin({ map: this.map }, actionObject)) });
                            break;
                    }
                }
                else {
                    this.actions.push(actionObject);
                }
            }, this);
        },
        _loadFromLocalStorage: function () {
            var simpleSearchActions = JSON.parse(localStorage.getItem("simpleSearch"));
            if (simpleSearchActions) {
                this._parseActions(simpleSearchActions);
            }
        }
    });

    var _QueryAction = declare([Evented], {
        declaredClass: "wvs/dijits/QueryAction",

        // autocompleteEnabled: Boolean
        //      determines if this will provide autocomplete results compatible with quick search
        autocompleteEnabled: true,

        // joinOperator: String
        //      the joining type for the query " AND " | " OR "
        joinOperator: " AND ",

        // searchResultLimit: Integer
        //      the limit for providing autocomplete results
        searchResultLimit: 500,

        constructor: function (params) {
            if (!params.query || !params.query.outFields || !params.query.outFields.length) {
                throw new Error("a query is required with AT LEAST an outfields parameter");
            }
            if (!params.map) {
                throw new Error("a map is required");
            }
            if (!params.mainWhere) {
                throw new Error("a main where clause is required");
            }
            if (!params.endPoint) {
                throw new Error("an endpoint for this query action is required");
            }
            if (!params.idProperty) {
                throw new Error("an idProperty for this query action is required");
            }
            if (!params.hiddenFields) {
                this.hiddenFields = [];
            }

            lang.mixin(this, params);

            // we may pass in an actual Query instance or just an object that defines the query
            if (!this.query.declaredClass && this.query.declaredClass !== "esri.tasks.Query") {
                var q = new Query();
                this.query = lang.mixin(q, this.query);
            }

            this.resultsStore = params.resultsStore ? params.resultsStore : wvsConfig.defaults.searchResultStore ? wvsConfig.defaults.searchResultStore : null;

            if (!this.resultsStore) {
                throw new Error("a SearchResultStore is required");
            }

            this.where = this.query.where;
        },
        autoComplete: function (inputText) {
            var self = this,
                deferred = new Deferred();
            this.query.returnGeometry = false;

            this.execute(inputText).then(
                function (featureSet) {
                    // transform our result to autocomplete
                    var store = new Memory({ data: [] });
                    var results = [];
                    if (featureSet.features.length) {
                        if (featureSet.features.length > self.searchResultLimit) {
                            store.setData([{ name: "Too many results for '" + inputText + "'", id: "Too many Results", callback: null }]);
                            deferred.resolve(store);
                            return;
                        }
                        // count results
                        array.forEach(featureSet.features, function (feature) {
                            var attributes = feature.attributes,
                                existingResult = array.filter(results, function (result) { return result.id === attributes[self.idProperty]; });
                            if (existingResult.length) {
                                existingResult[0].count = existingResult[0].count + 1;
                            }
                            else {
                                results.push({ id: attributes[self.idProperty], count: 1 });
                            }
                        });

                        // Sort descending by count first, and then ascending by id property
                        results.sort(function (a, b) {
                            if (a.count > b.count) {
                                return -1;
                            }
                            else if (a.count < b.count) {
                                return 1;
                            }
                            else {
                                var aId = a.id.toLowerCase(),
                                    bId = b.id.toLowerCase();
                                if (aId > bId) {
                                    return 1;
                                }
                                if (aId < bId) {
                                    return -1;
                                }
                                return 0;
                            }
                        });

                        var queryCallback = function (query, endPoint, resultsStore) {
                            query.returnGeometry = true;
                            query.outFields = ["*"];

                            resultsStore.clear();
                            resultsStore.setLoadingData(true);

                            var qt = QueryTask(endPoint);
                            qt.execute(query,
                                // callback
                                function (featureSet) {
                                    var adapter = new SearchResultsAdapter();
                                    adapter.featureSetToResultSet(featureSet, endPoint).then(function (searchResult) {
                                        if (featureSet.features.length && featureSet.features.length > 0) {
                                            self.emit("search-result-acquired", featureSet.features[0]);
                                        }
                                        resultsStore.put(searchResult);
                                    });
                                },
                                // errback
                                function (error) {
                                    Commom.errorDialog(error.message);
                                }
                            );
                        };


                        var storeData = array.map(results, function (result) {
                            var query = new Query();
                            query.where = self.where ?
                                self.where.concat(self.joinOperator + self.idProperty + " = '" + result.id + "'").replace(/%s/g, result.id)
                                : self.idProperty + " = '" + result.id + "'".replace(/%s/g, result.id);

                            query.returnGeometry = true;
                            query.outSpatialReference = self.map.spatialReference;

                            var callback = lang.hitch(self, queryCallback, query, self.endPoint, self.resultsStore);
                            return { id: result.id, name: result.count > 1 ? result.id + " (" + result.count + " results)" : result.id, callback: callback };
                        });
                        store.setData(storeData);

                        // resolve deferred

                        deferred.resolve(store);
                    }
                    else {
                        deferred.resolve(store);
                    }

                },
                function (error) {
                    deferred.reject(error);
                }
            );

            return deferred.promise;
        },
        submit: function (inputText) {
            var self = this,
                resultsStore = this.resultsStore;

            this.query.returnGeometry = true;
            this.query.outFields = ["*"];

            resultsStore.clear();
            resultsStore.setLoadingData(true);

            this.execute(inputText).then(
                // callback
                function (featureSet) {
                    var adapter = new SearchResultsAdapter();
                    adapter.featureSetToResultSet(featureSet, self.endPoint, self.hiddenFields).then(function (searchResult) {
                        if (featureSet.features.length && featureSet.features.length > 0) {
                            self.emit("search-result-acquired", featureSet.features[0]);
                        }
                        resultsStore.put(searchResult);
                    });
                },
                // errback
                function (error) {
                    Commom.errorDialog(error.message);
                }
            );

        },
        execute: function (inputText) {
            var self = this;

            if (this.useGlobalSpatialFilter && wvsConfig.defaults.globalSpatialFilter) {
                this.query.geometry = wvsConfig.defaults.globalSpatialFilter;
            }

            // construct our where clause
            this.query.where = this.where.length > 0 ? this.where.concat(this.joinOperator + this.mainWhere).replace(/%s/g, inputText) : this.mainWhere.replace(/%s/g, inputText);
            this.query.outSpatialReference = this.map.spatialReference;

            // perform query task
            var qt = new QueryTask(this.endPoint);
            return executedQuery = qt.execute(this.query);
        }
    });
    var _LookupAction = declare([Evented], {
        declaredClass: "wvs/dijits/LookupAction",

        // autocompleteEnabled: Boolean
        //      determines if this will provide autocomplete results compatible with quick search
        autocompleteEnabled: true,

        // joinOperator: String
        //      the joining type for the query " AND " | " OR "
        joinOperator: " AND ",

        // searchResultLimit: Integer
        //      the limit for providing autocomplete results
        searchResultLimit: 500,

        constructor: function (params) {
            if (!params.query || !params.query.outFields || !params.query.outFields.length) {
                throw new Error("a query is required with AT LEAST an outfields parameter");
            }
            if (!params.map) {
                throw new Error("a map is required");
            }
            if (!params.mainWhere) {
                throw new Error("a main where clause is required");
            }
            if (!params.endPoint) {
                throw new Error("an endpoint for this query action is required");
            }
            if (!params.idProperty) {
                throw new Error("an idProperty for this query action is required");
            }
            if (!params.hiddenFields) {
                this.hiddenFields = [];
            }

            lang.mixin(this, params);

            // we may pass in an actual Query instance or just an object that defines the query
            if (!this.query.declaredClass && this.query.declaredClass !== "esri.tasks.Query") {
                var q = new Query();
                this.query = lang.mixin(q, this.query);
            }

            this.resultsStore = params.resultsStore ? params.resultsStore : wvsConfig.defaults.searchResultStore ? wvsConfig.defaults.searchResultStore : null;

            if (!this.resultsStore) {
                throw new Error("a SearchResultStore is required");
            }

            this.where = this.query.where;
        },

        loadDropdown: function () {
            var self = this,
                deferred = new Deferred();
            this.query.returnGeometry = false;

            this.execute().then(
                function (featureSet) {
                    // transform our result to autocomplete
                    var store = new Memory({ data: [] });
                    var results = [];
                    if (featureSet.features.length) {
                        if (featureSet.features.length > self.searchResultLimit) {
                            store.setData([{ name: "Too many results for '" + inputText + "'", id: "Too many Results", callback: null }]);
                            deferred.resolve(store);
                            return;
                        }
                        // count results
                        array.forEach(featureSet.features, function (feature) {
                            var attributes = feature.attributes,
                                existingResult = array.filter(results, function (result) { return result.id === attributes[self.idProperty]; });
                            if (existingResult.length) {
                                existingResult[0].count = existingResult[0].count + 1;
                            }
                            else {
                                results.push({ id: attributes[self.idProperty], count: 1 });
                            }
                        });

                        // Sort descending by count first, and then ascending by id property
                        results.sort(function (a, b) {
                            if (a.count > b.count) {
                                return -1;
                            }
                            else if (a.count < b.count) {
                                return 1;
                            }
                            else {
                                var aId = a.id.toLowerCase(),
                                    bId = b.id.toLowerCase();
                                if (aId > bId) {
                                    return 1;
                                }
                                if (aId < bId) {
                                    return -1;
                                }
                                return 0;
                            }
                        });

                        var queryCallback = function (query, endPoint, resultsStore) {
                            query.returnGeometry = true;
                            query.outFields = ["*"];

                            resultsStore.clear();
                            resultsStore.setLoadingData(true);

                            var qt = QueryTask(endPoint);
                            qt.execute(query,
                                // callback
                                function (featureSet) {
                                    var adapter = new SearchResultsAdapter();
                                    adapter.featureSetToResultSet(featureSet, endPoint).then(function (searchResult) {
                                        if (featureSet.features.length && featureSet.features.length > 0) {
                                            self.emit("search-result-acquired", featureSet.features[0]);
                                        }
                                        resultsStore.put(searchResult);
                                    });
                                },
                                // errback
                                function (error) {
                                    Commom.errorDialog(error.message);
                                }
                            );
                        };


                        var storeData = array.map(results, function (result) {
                            var query = new Query();
                            query.where = self.where ?
                                self.where.concat(self.joinOperator + self.idProperty + " = '" + result.id + "'").replace(/%s/g, result.id)
                                : self.idProperty + " = '" + result.id + "'".replace(/%s/g, result.id);

                            query.returnGeometry = true;
                            query.outSpatialReference = self.map.spatialReference;

                            var callback = lang.hitch(self, queryCallback, query, self.endPoint, self.resultsStore);
                            return { id: result.id, name: result.count > 1 ? result.id + " (" + result.count + " results)" : result.id, callback: callback };
                        });
                        store.setData(storeData);

                        // resolve deferred

                        deferred.resolve(store);
                    }
                    else {
                        deferred.resolve(store);
                    }

                },
                function (error) {
                    deferred.reject(error);
                }
            );

            return deferred.promise;
        },

        autoComplete: function (inputText) {
            var self = this,
                deferred = new Deferred();
            this.query.returnGeometry = false;

            this.execute(inputText).then(
                function (featureSet) {
                    // transform our result to autocomplete
                    var store = new Memory({ data: [] });
                    var results = [];
                    if (featureSet.features.length) {
                        if (featureSet.features.length > self.searchResultLimit) {
                            store.setData([{ name: "Too many results for '" + inputText + "'", id: "Too many Results", callback: null }]);
                            deferred.resolve(store);
                            return;
                        }
                        // count results
                        array.forEach(featureSet.features, function (feature) {
                            var attributes = feature.attributes,
                                existingResult = array.filter(results, function (result) { return result.id === attributes[self.idProperty]; });
                            if (existingResult.length) {
                                existingResult[0].count = existingResult[0].count + 1;
                            }
                            else {
                                results.push({ id: attributes[self.idProperty], count: 1 });
                            }
                        });

                        // Sort descending by count first, and then ascending by id property
                        results.sort(function (a, b) {
                            if (a.count > b.count) {
                                return -1;
                            }
                            else if (a.count < b.count) {
                                return 1;
                            }
                            else {
                                var aId = a.id.toLowerCase(),
                                    bId = b.id.toLowerCase();
                                if (aId > bId) {
                                    return 1;
                                }
                                if (aId < bId) {
                                    return -1;
                                }
                                return 0;
                            }
                        });

                        var queryCallback = function (query, endPoint, resultsStore) {
                            query.returnGeometry = true;
                            query.outFields = ["*"];

                            resultsStore.clear();
                            resultsStore.setLoadingData(true);

                            var qt = QueryTask(endPoint);
                            qt.execute(query,
                                // callback
                                function (featureSet) {
                                    var adapter = new SearchResultsAdapter();
                                    adapter.featureSetToResultSet(featureSet, endPoint).then(function (searchResult) {
                                        if (featureSet.features.length && featureSet.features.length > 0) {
                                            self.emit("search-result-acquired", featureSet.features[0]);
                                        }
                                        resultsStore.put(searchResult);
                                    });
                                },
                                // errback
                                function (error) {
                                    Commom.errorDialog(error.message);
                                }
                            );
                        };


                        var storeData = array.map(results, function (result) {
                            var query = new Query();
                            query.where = self.where ?
                                self.where.concat(self.joinOperator + self.idProperty + " = '" + result.id + "'").replace(/%s/g, result.id)
                                : self.idProperty + " = '" + result.id + "'".replace(/%s/g, result.id);

                            query.returnGeometry = true;
                            query.outSpatialReference = self.map.spatialReference;

                            var callback = lang.hitch(self, queryCallback, query, self.endPoint, self.resultsStore);
                            return { id: result.id, name: result.count > 1 ? result.id + " (" + result.count + " results)" : result.id, callback: callback };
                        });
                        store.setData(storeData);

                        // resolve deferred

                        deferred.resolve(store);
                    }
                    else {
                        deferred.resolve(store);
                    }

                },
                function (error) {
                    deferred.reject(error);
                }
            );

            return deferred.promise;
        },
        submit: function (inputText) {
            var self = this,
                resultsStore = this.resultsStore;

            this.query.returnGeometry = true;
            this.query.outFields = ["*"];

            resultsStore.clear();
            resultsStore.setLoadingData(true);

            this.execute(inputText).then(
                // callback
                function (featureSet) {
                    var adapter = new SearchResultsAdapter();
                    adapter.featureSetToResultSet(featureSet, self.endPoint, self.hiddenFields).then(function (searchResult) {
                        resultsStore.put(searchResult);
                    });
                },
                // errback
                function (error) {
                    Commom.errorDialog(error.message);
                }
            );

        },
        execute: function (inputText) {
            var self = this;

            if (this.useGlobalSpatialFilter && wvsConfig.defaults.globalSpatialFilter) {
                this.query.geometry = wvsConfig.defaults.globalSpatialFilter;
            }

            // construct our where clause
            if (inputText) {
                this.query.where = this.where.length > 0 ? this.where.concat(this.joinOperator + this.mainWhere).replace(/%s/g, inputText) : this.mainWhere.replace(/%s/g, inputText);
            } else {
                this.query.where = "1=1"
            }
            this.query.outSpatialReference = this.map.spatialReference;

            // perform query task
            var qt = new QueryTask(this.endPoint);
            return executedQuery = qt.execute(this.query);
        }
    });

    var _GeocodeAction = declare([Evented], {
        declaredClass: "wvs/dijits/GeocodeAction",

        // autocompleteEnabled: Boolean
        //      determines if this will provide autocomplete results compatible with quick search
        autocompleteEnabled: false,

        // singleLineSearch: Boolean
        //      whether the geocoder will use a single line for finding the results or will we manually parse the input text for querying
        singleLineSearch: true,

        // singleLineFieldName: String
        //      the name of the Geocoder service field that is used for single line address searches
        singleLineFieldName: "singleLine",

        // minimumResultScore: Integer
        //      the minimum score needed to actually show the point on the map
        minimumResultScore: 90,

        // endPoint: String
        //      the URL of the Geocoder service
        endPoint: "",

        constructor: function (params) {
            // map: Map
            //      an instance of an esri Map
            this.map = null;

            // outFields: String[]
            //      a string array representing the outFields for the geocoder search
            this.outFields = ["*"];

            if (!params.endPoint) {
                throw new Error("endPoint required for " + this.declaredClass);
            }
            if (!params.map) {
                throw new Error("map required for " + this.declaredClass);
            }

            lang.mixin(this, params);
        },
        submit: function (inputText) {
            var self = this;

            this.execute(inputText).then(
                function (addressCandidates) {
                    if (addressCandidates.length) {
                        self._showAddressCandidateOnMap(addressCandidates[0]);
                    }
                },
                function (error) {
                    Common.errorDialog(error.message);
                }
            );
        },
        execute: function (inputText) {
            var locator = new Locator(this.endPoint),
                address = {};


            locator.setOutSpatialReference(this.map.spatialReference);
            if (this.singleLineSearch) {
                address[this.singleLineFieldName] = inputText;
            }
            else {
                // TODO: Implement parsing input text
                throw new Error("not implemented");
            }

            return locator.addressToLocations(address);
        },
        autoComplete: function (inputText) {
            var deferred = new Deferred(),
                self = this;

            if (this.autocompleteEnabled) {
                this.getAutocompleteValues(inputText).then(
                    function (addressCandidates) {
                        var store = new Memory({ data: [] });
                        if (addressCandidates != null && addressCandidates.length) {
                            store.setData(array.map(addressCandidates, function (addressCandidate) {
                                return { id: addressCandidate.address, name: addressCandidate.address, callback: lang.hitch(self, self._selectAddressCandidateFromDropdown, addressCandidate.address) };
                            }));
                            deferred.resolve(store);
                        }
                        else {
                            store.setData([{ name: "No results found for " + inputText, id: "noResults", callback: null }]);
                            deferred.resolve(store);
                        }
                    },
                    function (error) {
                        deferred.reject(error);
                    }
                );
            }

            return deferred;
        },
        getAutocompleteValues: function (inputText) {
            var dfd = new Deferred(),
                self = this;
            esriRequest({
                url: this.streetLookupUrl,
                content: {
                    prefixText: inputText,
                    count: 25
                }
            }, { usePost: true }
            ).then(function (result) {
                if (result != null && result.length) {
                    for (var i = 0; i < result.length; i++) {
                        result[i] = { address: result[i] };
                    }
                    dfd.resolve(result);
                } else {
                    dfd.resolve(null);
                }
            }, function (err) {
                dfd.reject(err);
            });
            return dfd;
        },
        _selectAddressCandidateFromDropdown: function (addressCandidate) {
            this.execute(addressCandidate).then(lang.hitch(this, this._showAddressCandidateOnMap), function (err) { console.warn(err); });
        },
        _showAddressCandidateOnMap: function (addressCandidate, wkid) {
            if (addressCandidate.score >= this.minimumResultScore) {
                var self = this;
                var graphic = new Graphic(addressCandidate.location, symbolJsonUtils.fromJson(wvsConfig.defaults.symbols.point), { address: addressCandidate.address }, InfoTemplateManager.getTabularAttributeTemplate({ title: "Geocode Result", createWidgetsFromGeometry: true }));
                graphic.setEditing(true);
                this.map.graphics.add(graphic);
                var infoWindow = self.map.infoWindow;
                infoWindow.clearFeatures();
                this._addMinimizeButton();
                var minButton = domConstruct.create()
                infoWindow.hide();
                infoWindow.setContent(graphic.getContent());
                infoWindow.setTitle(graphic.getTitle());
                on.once(infoWindow, "hide", this._removeGraphic.bind(this, graphic));
                self.map.centerAndZoom(graphic.geometry, self.map.getNumLevels()).then(function () {
                    infoWindow.show(graphic.geometry);
                });
            }
        },

        _addMinimizeButton: function () {
            var infoWindow = this.map.infoWindow;
            var prevButton = query(".wvsM4 .esriPopup .titleButton.prev")[0];
            var nextButton = query(".wvsM4 .esriPopup .titleButton.next")[0];
            domStyle.set(prevButton, "right", "76px");
            domStyle.set(nextButton, "right", "58px");
            var minButton = domConstruct.toDom('<div class="titleButton" style="right: 44px;" title="Minimize" id="infoWindowMinBtn"><span style="font-color: white; font-weight: bold;">_</span></div>');
            domConstruct.place(minButton, nextButton, "after");
            on(minButton, "click", this._minimizePopup.bind(this));
        },

        _removeMinimizeButton: function () {
            var infoWindow = this.map.infoWindow;
            var prevButton = query(".wvsM4 .esriPopup .titleButton.prev")[0];
            var nextButton = query(".wvsM4 .esriPopup .titleButton.next")[0];
            domStyle.set(prevButton, "right", "64px");
            domStyle.set(nextButton, "right", "46px");
            domConstruct.destroy("infoWindowMinBtn");
        },

        _minimizePopup: function (graphic) {
            domStyle.set(this.map.infoWindow.domNode, "visibility", "hidden");
            on.once(map.graphics, "click", this._restorePopup.bind(this));
        },

        _restorePopup: function (evt) {
            domStyle.set(this.map.infoWindow.domNode, "visibility", "visible");
        },

        _removeGraphic: function (graphic) {
            self.map.graphics.remove(graphic);
            this._removeMinimizeButton();
        }
        
    });

    SimpleSearch.QueryAction = _QueryAction;
    SimpleSearch.GeocodeAction = _GeocodeAction;
    SimpleSearch.LookupAction = _LookupAction;

    return SimpleSearch;
});