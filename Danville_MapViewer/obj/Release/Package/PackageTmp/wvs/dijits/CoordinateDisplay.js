console.log("wvs/dijits/CoordinateDisplay.js");

define([
    "dojo/_base/declare"
    , "dojo/_base/lang"
    , "dojo/on"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dojo/_base/array"
    , "wvs/libs/proj4js/main"
    , "esri/SpatialReference"
    , "require"
    , "wvs/common/Converter"
    , "dojo/_base/connect"
    , "../utilities/usng-amd"
    , "../extensions/esri/SpatialReference"
], function (declare, lang, on, _WidgetBase, _TemplatedMixin, array, Proj4js, SpatialReference, require, Converter, connect, usng) {

    // module:
    //      wvs/dijits/CoordinateDisplay

    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: '<div><span data-dojo-attach-point="activeCoordinates"></span><img src="${icon}" data-dojo-attach-point="iconNode" /><span data-dojo-attach-point="unitsNode"></span><div>',

        declaredClass: 'wvs.dijits.CoordinateDisplay',

        /*@function constructor
        *       Creates an isntance of the CoordinateDisplay widget, which is used to dynamically display the Lat/Lon coordinates of the mouse pointer
        *       as it moves over the map area of the application.
        * @param params: Object
        *       The object hash for the parameters that may be passed into the widget and whose properties will be mixed into the instance.
        * @param srcNodeRef: DOM Node
        *       The dom node to which this widget will be tied when it is created and rendered.
        */
        constructor: function (params, srcNodeRef) {
            // mixin defaults
            lang.mixin(this, {
                _displayUnits: "dd"
                , _displayUnitLabel: "Decimal Degrees"
                , _eventHandles: []
                , _eventCount: 0
                // Describes how many events should fire before the coordinate display is updated. 
                , refreshRate: 3
                , _destProj: null
                , _sourceProj: null
                , icon: require.toUrl("./images/bullet_arrow_down.png")
                , _wgs84SpatialReference: new SpatialReference({ wkid: 4326 })
                , unitsMap: [ { label: "Decimal Degrees", unit: "dd"}
                            , { label: "Degrees Min Sec", unit: "dms" }
                            , { label: "Degrees & Min", unit: "dm" }
                            , { label: "Native Map Units", unit: "mu" }
                            , { label: "MGRS", unit: "mgrs" }
                            , { label: "UTM", unit: "utm" }
                ],
                _subscriptions: []
            });

            params = params || {};
            if (!params.map) {
                throw new Error('map not defined in params for wvs/dijits/CoordinateDisplay');
            }
            lang.mixin(this, params);

            this._destProj = this._wgs84SpatialReference.toProj4js();
        },

        /*@function postCreate
        *       Standard life-cycle function. Initializes the components who require the complete creation of this widget
        *       while not necessarily requiring it having been rendered to markup.
        */
        postCreate: function () {
            this._createMenu();
            this._registerEvents();
            

            this._subscriptions.push(connect.subscribe("map-acquire", lang.hitch(this, function (topic) {
                if (topic.acquired)
                    this.disableCoordinateDisplay();
                else
                    this.enableCoordinateDisplay();
            })));
        },

        /*@function destroy
        *       Handles memory cleanup for removing the widget from the dom.
        */
        destroy: function(){
            array.forEach(this._subscriptions, function (subscription) {
                subscription.remove();
            });
            this.inherited(arguments);
        },

        /*@function _createMenu
        *       
        */
        _createMenu: function () {
            var self = this;
            this._menuUnitItems = [];

            //Create the context menu that allows the user to switch measurement units
            this._menu = new dijit.Menu({
                leftClickToOpen: true
            });

            array.forEach(this.unitsMap, function (entry, index) {
                var menuItem = new dijit.MenuItem({
                    label: entry.label,
                    displayUnit: entry.unit,
                    onClick: function () { self._setDisplayUnit(this); }
                });
                self._menuUnitItems.push(menuItem);
                self._menu.addChild(menuItem);
            });
            this._menu.bindDomNode(this.iconNode);
            this._menu.startup();
        },
        _setDisplayUnit: function(menuUnitItem){
            var self = this;
            self._displayUnits = menuUnitItem.get("displayUnit");
            self._displayUnitLabel = menuUnitItem.get("label");
        },
        _registerEvents: function(){
            // connect to the map onLayerAdd/Remove events

            this._eventHandles = this.own(
                    on(this.map, "mouse-move", lang.hitch(this, "_onMouseMove")),
                    on(this.map, "layer-add", lang.hitch(this, "_mapLayerChanged")),
                    on(this.map, "layer-remove", lang.hitch(this, "_mapLayerChanged")));

            
        },
        disableCoordinateDisplay: function(){
            array.forEach(this._eventHandles, function (eventHandle) {
                eventHandle.remove();
            });
            this.unitsNode.innerHTML = "Disabled";
        },
        enableCoordinateDisplay: function () {
            this._registerEvents();
            this.unitsNode.innerHTML = "";
        },
        _mapLayerChanged: function () {
            this._sourceProj = this.map.spatialReference.toProj4js();
        },
        _onMouseMove: function (evt) {
            //TODO: Disable this when any map tool is enabled
            if (this._sourceProj === null || ++this._eventCount % this.refreshRate !== 0)
                return;
            //get mapPoint from event and display the mouse coordinates
            var p = {
                        x: evt.mapPoint.x,
                        y: evt.mapPoint.y
                    };
            if (this._displayUnits != 'mu') {
                Proj4js.transform(this._sourceProj, this._destProj, p);
            }
            this._formatPoint(p);
        },
        _formatPoint: function (point) {

            switch (this._displayUnits) {
                case "dms":
                    this.unitsNode.innerHTML = "Lon (X):" + Converter.DDtoDegreesMinutesSeconds(point.x, "x") + "&nbsp;&nbsp;Lat (Y):" + Converter.DDtoDegreesMinutesSeconds(point.y, "y");
                    break;
                case "dm":
                    this.unitsNode.innerHTML = "Lon (X):" + Converter.DDtoDegreesDecimalMinutes(point.x, "x") + "&nbsp;&nbsp;Lat (Y):" + Converter.DDtoDegreesDecimalMinutes(point.y, "y");
                    break;
                case "dd":
                    this.unitsNode.innerHTML = "Lon (X): " + point.x.toFixed(5) + "&nbsp;Lat (Y): " + point.y.toFixed(5);
                    break;
                case "mu":
                    this.unitsNode.innerHTML = "x: " + point.x.toFixed(5) + "&nbsp;  y: " + point.y.toFixed(5);
                    break;
                case "mgrs":
                    this._usngPrecision = 5;
                    if (this.map._mapUnitsPerPixelX > 10) {
                        this._usngPrecision = 4;
                    }
                    if (this.map._mapUnitsPerPixelX > 100) {
                        this._usngPrecision = 3;
                    }
                    this.unitsNode.innerHTML = usng.LLtoUSNG(point.y, point.x, this._usngPrecision);
                    break;
                case "utm":
                    this.unitsNode.innerHTML = usng.LLtoUTMString(point.y, point.x);
                    break;
            }

            this.activeCoordinates.innerHTML = this._displayUnitLabel;
        }
    }
    );
});