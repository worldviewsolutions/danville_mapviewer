console.log("wvs/dijits/TocWebMapNode.js");

define([
    "dojo/_base/declare"
    , "dojo/text!./templates/_TocNode.html"
    , "dojo/_base/lang"
    , "./_TocNode"
    , "dijit/form/HorizontalSlider"
    , "dojo/dom-style"
    , "dojo/ready"
    , "dojo/dom-construct"
    , "dojo/dom-attr"
    , "dojo/on"
    , "dijit/Menu"
    , "dijit/MenuItem"
    , "dijit/MenuSeparator"
    , "dijit/PopupMenuItem"
    , "dijit/TooltipDialog"
    , "dojo/_base/array"
    , "./TocMapServiceLayerNode"
    , "dojo/dnd/Source"
    , "dojo/query"
    , "dojo/dom-class"
    , "dojo/has"
    , "dojo/dom"
    , "esri/layers/KMLLayer"
    , "esri/layers/GeoRSSLayer"
    , "esri/layers/FeatureLayer"
    , "esri/layers/ArcGISImageServiceLayer"
    , "esri/layers/ArcGISDynamicMapServiceLayer"
    , "./TocMapServiceNode"
    , "./TocImageServiceNode"
    , "./TocFeatureLayerNode"
    , "./TocGroupLayerNode"
    , "dojo/_base/sniff"
], function (
    declare
    , template
    , lang
    , _TocNode
    , HorizontalSlider
    , domStyle
    , ready
    , domConstruct
    , domAttr
    , on
    , Menu
    , MenuItem
    , MenuSeparator
    , PopupMenuItem
    , TooltipDialog
    , array
    , TocMapServiceLayerNode
    , dndSource
    , query
    , domClass
    , has
    , dom
    , KMLLayer
    , GeoRSSLayer
    , FeatureLayer
    , ArcGISImageServiceLayer
    , ArcGISDynamicMapServiceLayer
    , TocMapServiceNode
    , TocImageServiceNode
    , TocFeatureLayerNode
    , TocGroupLayerNode
    ) {
    return declare([_TocNode], {

        templateString: template,

        // refreshDelay: Integer
        //      Number of milliseconds to delay between clicking a checkbox and refreshing the map service. A longer delay allow the user to check/uncheck multiple
        //      layers before refreshing the map service."dojo/ready",
        refreshDelay: 500,

        // useSpriteImage: Boolean
        //      Use the custom sprite image for the legend icons. When false, the "Legend" REST service will be used for 10.01 and newer services.
        useSpriteImage: true,

        declaredClass: "wvs.dijits.TocWebMapNode",

        // title:  String
        //      the title for the Web Map
        title: null,

        hasDropDown: true,

        isGroupNode: true,


        constructor: function (params, srcNodeRef) {

            params = params || {};
            if (!params.mapService) {
                throw new Error('mapService not defined in params for ' + this.declaredClass);
            }
            if (!params._toc) {
                throw new Error('no reference to the toc found for ' + this.declaredClass);
            }

            lang.mixin(this, params);

            if (!this.title) {
                if (this.mapService.name) {
                    this.title = this.mapService.name;
                } else {
                    this.title = this.mapService.getServiceName();
                }
            }
        },
        postCreate: function () {
            domClass.remove(this.expandoNode, "jstoolkit-hidden");
            domClass.add(this.labelNode, "group-title");
            this._createChildNodes();
            this._createContextMenu();
            this.inherited(arguments);
        },
        setVisible: function (visible) {
            // return if the visibility has not changed
            if (this._state.visible == visible) return;

            this._state.visible = visible;

            // update the map service
            if (visible) {
                this.mapService.show();
                this._showSwatch();
                this._enableChildNodes();
            } else {
                this.mapService.hide();
                this._hideSwatch();
                this._disableChildNodes();
            }
        },
        _checkboxOnClick: function (e) {
            this.setVisible(this._checkbox && this._checkbox.checked);
        },

        _refreshLayer: function () {
            if (this._refreshTimer) {
                window.clearTimeout(this._refreshTimer);
                this._refreshTimer = null;
            }

            var mapService = this.mapService;

            this._refreshTimer = window.setTimeout(function () {
                mapService.refresh();
            }, this.refreshDelay);
        },
        _createChildNodes: function () {
            var self = this;
            array.forEach(this.mapService.layers, function (layer) {
                var node;
                if (layer.isInstanceOf(ArcGISDynamicMapServiceLayer)) {
                    node = new TocMapServiceNode({
                        mapService: layer,
                        refreshDelay: this.refreshDelay,
                        useSpriteImage: this.useSpriteImage,
                        _parent: self,
                        isFromWebMap: true
                    });
                }
                else if (layer.isInstanceOf(ArcGISImageServiceLayer)) {
                    node = new TocImageServiceNode({
                        mapService: layer,
                        refreshDelay: this.refreshDelay,
                        useSpriteImage: this.useSpriteImage,
                        _parent: self,
                        isFromWebMap: true
                    });
                }
                else if (layer.isInstanceOf(FeatureLayer)) {
                    node = new TocFeatureLayerNode({
                        mapService: layer,
                        featureLayerType: 'featureLayer',
                        refreshDelay: this.refreshDelay,
                        useSpriteImage: this.useSpriteImage,
                        toc: self._toc,
                        _parent: self._toc,
                        isFromWebMap: true
                    });
                }
                else if (layer.declaredClass == "esri.layers.KMLLayer" || layer.declaredClass == "esri.layers.GeoRSSLayer" || layer.declaredClass == "wvs.layers.GroupLayer") {
                    node = new TocGroupLayerNode({
                        mapService: layer,
                        refreshDelay: this.refreshDelay,
                        useSpriteImage: this.useSpriteImage,
                        toc: self._toc,
                        isFromWebMap: true
                    });
                }
                else {
                    //throw new Error(layer.declaredClass + ' is not a supported TOC layer type');
                }
                if (node) {
                    node.placeAt(self.childrenNode);
                    node.startup();
                    self.nodes.push(node);
                }
            });

            this._updateVerticalAlignment();
        },
        _createContextMenu: function () {
            console.log(this.declaredClass + ' _createContextMenu');

            var mapServiceOptionsMenu = new Menu({
                leftClickToOpen: true
            });

            // Transparency
            mapServiceOptionsMenu.addChild(_TocNode.ContextMenuItems.Transparency(this));

            mapServiceOptionsMenu.addChild(new MenuSeparator());
            // Remove
            mapServiceOptionsMenu.addChild(_TocNode.ContextMenuItems.Remove(this));

            mapServiceOptionsMenu.bindDomNode(this.dropDownMenuIcon);
            mapServiceOptionsMenu.startup();
            this.mapServiceOptionsMenu = mapServiceOptionsMenu;
        }

    });
});