﻿console.log("wvs/dijits/RoutingTool.js");

define([
      "dojo/_base/declare",
      "dojo/_base/array",
      "dojo/dom-attr",
      "dojo/dom-style",
      "dojo/dom",
      "dojo/on",
      "dojo/topic",
      "dojo/Evented",
      "dijit/_WidgetBase",
      "dijit/_TemplatedMixin",
      "dijit/_WidgetsInTemplateMixin",
      "dojo/text!./templates/RoutingTool.html",
      "dojo/_base/lang",
      "dojo/dom-construct",
      "esri/request",
      "esri/graphic",
      "esri/Color",
      "esri/InfoTemplate",
      "esri/tasks/locator",
      "esri/layers/GraphicsLayer",
      "esri/geometry/Point",
      "esri/symbols/SimpleMarkerSymbol",
      "esri/symbols/SimpleLineSymbol",
      "wvs/common/Common"
], function (
        declare,
        array,
        domAttr,
        domStyle,
        dom,
        on,
        topic,
        Evented,
        _WidgetBase,
        _TemplatedMixin,
        _WidgetsInTemplateMixin,
        template,
        lang,
        domConstruct,
        esriRequest,
        Graphic,
        Color,
        InfoTemplate,
        Locator,
        GraphicsLayer,
        Point,
        SimpleMarkerSymbol,
        SimpleLineSymbol,
        Common
    ) {
    // module:
    //      wvs/dijits/RoutingTool
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, Evented], {

        templateString: template,

        declaredClass: "wvs.dijits.RoutingTool",

        //Used to identify the geolocation 'watch' operation so that it can be cleared when a sufficiently accurate position is returned.
        _currentWatchId: -1,

        //originCoords: Object
        //  {lat, lon} object representing the lat/long coords for the starting point of the route task.
        originCoords: null,

        //destCoords: Object
        //  {lat, lon} object representing the lat/long coords for the destination point of the route task.
        destCoords: null,

        //originAddress: String
        //  The address of the starting point for the route task. Will be geocoded and transformed into originCoords
        originAddress: null,

        //destAddress: String
        //  The address of the destination point to be geocoded and transformed into destCoords
        destAddress: null,

        //maximumValidAccuracy: Number
        //Used to define the maximum range for what would be considered a useable location for the device.
        //Any higher values will be thrown out and the 'watchPosition' cycle will continue.
        maximumValidAccuracy: 50,

        //maxLocationAttempts: Number
        //Used to determine how many attempts should be made to get an acceptably accurate position from the browser.
        maxLocationAttempts: 25,

        //currentLocationAttempts: Number
        //Counter variable to indicate the current number of attempts to retrieve a valid location.
        currentLocationAttempts: 0,

        //_defaultGeocodeServer: URL
        //The default server for Geocode services. Uses the ESRI Geocoder.
        _defaultGeocodeServer: "http://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer",

        //geocodeServer: URL
        //The url for the geocoder that will be used to geolocate addresses.
        geocodeServer: null,

        //_routingGraphicLayer: GraphicsLayer
        //The graphics layer to be added to the map in which we will place the start and end points as well
        //as the route when it has been determined.
        _routingGraphicsLayer: null,

        //_errorLockRelease: bool
        //Indicates that locks were released by the geolocation error handler and that there is no reliable
        //information to use for the routing task.
        _errorLockRelease: false,

        //_dropOriginHandler
        //Handle for the drop origin marker event.
        _dropOriginHandler: null,

        //_originGeocodeLock: bool
        //Indicates whether the origin point is being actively geocoded or not so the routing task can
        //reliably use the Lat/Lon values of the input boxes for the origin of the route.
        _originGeocodeLock: false,

        //_originMarker: Graphic
        //The graphic that will be added to/removed from the map for the origin of the route.
        _originMarker: null,

        //_originMarkerSymbol: SimpleMarkerSymbol
        //Default marker symbol for display of origin point
        _originMarkerSymbol: null,

        //_originInfoTemplate: InfoTemplate
        //The info template used to name and indicate the origin.
        _originInfoTemplate: null,

        //_dropDestinationHandler
        //Handle for the drop destination marker event.
        _dropDestinationHandler: null,

        //_destGeocodeLock:
        //Indicates whether the destination point is being actively geocoded or not to eliminate
        //a race condition if the onBlur event happens in response to a click of the "start route" button
        _destGeocodeLock: false,

        //_destinationMarker
        //The Graphic object that will be added to/removed from the map for the destinatio of the route.
        _destinationMarker: null,

        //_destinationMarkerSymbol: SimpleMarkerSymbol
        //Default marker symbol for display of destination point
        _destinationMarkerSymbol: null,

        //_destinationInfoTemplate
        //The info template used to name and indicate the destination for the route.
        _destinationInfoTemplate: null,

        //default spatial reference for the XY coordinates when a new point object is created.
        _spatRef: null,

        //map: Map
        //The application's map onto which this widget will drop route graphics.
        map: null,

        /********************************************************************************************
        * constructor
        *       Initializes the widget by incorporating any user-defined parameters and setting a
        *       reference to the map for the widget to add its internal graphics layer to.
        *@param: Object
        *       Params hash of properties to be mixed in to the widget. Currently only explicitly
        *       requires the user to define a 'map' property, however this can also be used to set
        *       the maximumValidAccuracy property as well as the maxLocationAttempts properties to
        *       define acceptable behavior for the 'Use Current Location' function
        *********************************************************************************************/
        constructor: function (params, srcNodeRef) {
            lang.mixin(this, {
                _eventHandles: [],
                nodes: []
            });

            params = params || {};
            if (!params.map) {
                throw new Error('Map not defined in params for Routing Tool');
            }

            if (!navigator.geolocation) {
                domConstruct.destroy(this.useLocationBtn);
            }

            this.map = params.map;
            this._routingGraphicsLayer = new GraphicsLayer({ id: "Routing" });
            this.map.addLayer(this._routingGraphicsLayer);
            this._spatRef = this.map.spatialReference;
            this.geocodeServer = params.geocodeServer || this._defaultGeocodeServer;

            //Initialize markers with default values and info windows.
            this._initializeMarkers();
            lang.mixin(this, params);

        },

        /********************************************************************************************
        * _initializeMarkerSymbols
        *       Creates the marker symbols for the start and destination markers.
        ********************************************************************************************/
        _initializeMarkers: function () {
            this._originMarkerSymbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 12,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color([0, 0, 0, 1]), 1),
                new Color([0, 120, 255, 1]));
            this._destinationMarkerSymbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 12,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color([0, 0, 0, 1]), 1),
                new Color([255, 0, 0, 1]));
            this._originInfoTemplate = new InfoTemplate("Route Start", "<span>Origin point for the route directions</span>");
            this._destinationInfoTemplate = new InfoTemplate("Route End", "<span>Destination point for the route directions</span>");
        },

        /********************************************************************************************
        * destroy
        *       Life-cycle function for cleaning up the widget before destruction. Removes the
        *       widget's graphics layer from the map.
        *********************************************************************************************/
        destroy: function () {
            this.map.removeLayer(this.map.getLayer(this._routingGraphicsLayer.id));
            this.inherited(arguments);
        },

        /*******************************************************************************************
        * _toggleOriginInputType
        *       Handles changes to the "Use Address"/"Use Coordinates" radio buttons for the starting
        *       point of the route by toggling the display of the relevant input fields.
        *@param useAddress: Event
        *       The onChange event for the 'Use Address' radio button. Is returned as a boolean
        *       indicating the 'selected' value of the Use Address button.
        ********************************************************************************************/
        _toggleOriginInputType: function (useAddress) {
            if (useAddress) {
                domStyle.set(this.originByAddressInputContainer, 'display', 'block');
                domStyle.set(this.originByCoordsInputContainer, 'display', 'none');
            } else {
                domStyle.set(this.originByAddressInputContainer, 'display', 'none');
                domStyle.set(this.originByCoordsInputContainer, 'display', 'block');
            }
        },

        /*******************************************************************************************
        * _toggleDestInputType
        *       Handles changes to the "Use Address"/"Use Coordinates" radio buttons for the
        *       destination point of the routing function, toggling the relevant input fields.
        *@param useAddress: Event
        *       The onChange event for the 'Use Address' radio button. 'true' if the address button
        *       is selected, 'false' if the coords button is.
        ********************************************************************************************/
        _toggleDestInputType: function (useAddress) {
            if (useAddress) {
                domStyle.set(this.destByAddressInputContainer, 'display', 'block');
                domStyle.set(this.destByCoordsInputContainer, 'display', 'none');
            } else {
                domStyle.set(this.destByAddressInputContainer, 'display', 'none');
                domStyle.set(this.destByCoordsInputContainer, 'display', 'block');
            }
        },

        /*******************************************************************************************
        * _getCurrentLocation
        *       Starts a watch on the current position of the user as determined by their browser's
        *       geolocation native functions. Sets the callback and errback for the watch, and
        *       stores the watch ID in the instance property _currentWatchId
        *******************************************************************************************/
        _getCurrentLocation: function () {
            this._currentWatchId = navigator.geolocation.watchPosition(this._locationRetrieved.bind(this), this._locationError, { enableHighAccuracy: true });
            this.useLocationBtn.set('iconClass', "icon-spinner icon-spin");
            this.useLocationBtn.set('disabled', true);
        },

        /*******************************************************************************************
        * _locationRetrieved
        *       Callback function for the watchPosition async function. Checks the accuracy of the
        *       identified location and if the accuracy is high enough, will populate the lat/long
        *       inputs with the user's current location and stop the watchPosition function. Else
        *       it will iterate the timeout counter and display an error if the timeout counter has
        *       maxed out.
        *@param pos: Position
        *       A position object returned by the 'getCurrentPosition'/'watchPosition' native functions
        *       which contains a 'coords' property with lat/long and accuracy values.
        *******************************************************************************************/
        _locationRetrieved: function (pos) {
            //If the accuracy of the retrieved position falls within the radius of acceptable accuracy,
            //populate the coordinate inputs and cancel the watch.
            if (pos.coords.accuracy <= this.maximumValidAccuracy) {
                if (this.useAddrStartRadio.get('checked') == 'checked' || this.useAddrStartRadio.get('checked') == true) {
                    this.useCoordStartRadio.set('checked', 'checked');
                }
                this.originByCoordsLatInput.set('value', pos.coords.latitude);
                this.originByCoordsLonInput.set('value', pos.coords.longitude);
                this.currentLocationAttempts = 0;
                navigator.geolocation.clearWatch(this._currentWatchId);
                this.useLocationBtn.set('iconClass', "");
                this.useLocationBtn.set('disabled', false);
                this._createOriginPointFromCoords();

                //If the accuracy is not and the number of attempts to get an acceptably accurate positions
                //exceeds the max allowable attempts, throw an error, cancel the watch, and reset the attempts.
            } else if (++this.currentLocationAttempts >= this.maxLocationAttempts) {
                this.currentLocationAttempts = 0;
                navigator.geolocation.clearWatch(this._currentWatchId);
                this.useLocationBtn.set('iconClass', "");
                this.useLocationBtn.set('disabled', false);
                this._locationError("Unable to determine an acceptably accurate location.");
            }
        },

        /*******************************************************************************************
        * _geolocateOrigin
        *       onBlur handler for the address input of the starting point. Will automatically try
        *       to geocode the address and notify the user if there was trouble geocoding.
        ********************************************************************************************/
        _geolocateOrigin: function (evt) {
            if (this.originByAddressInput.get('value') == "" || this.originByAddressInput.get('value') == null) {
                return;
            }
            var loc = new Locator(this.geocodeServer),
                addrParam = {
                    "SingleLine": this.originByAddressInput.get('value')
                };
            var locParams = {
                address: addrParam,
                maxLocations: 3,
                outFields: ["*"]
            };
            this._originGeocodeLock = true;
            loc.addressToLocations(locParams, this._processOriginGeolocation.bind(this), this._geolocationError.bind(this));
        },

        /*******************************************************************************************
        * _processOriginGeolocation
        *       The on-complete handler for geolocating the origin point by address.
        *@param locations: Array
        *       An array of geolocation results that can be parsed to populate the lat/lon inputs
        *       to be used when getting a route.
        ********************************************************************************************/
        _processOriginGeolocation: function (locations) {
            if (locations.length < 1) {
                this._geolocationError("No location results returned for starting address. "
                    + "Please re-enter a starting address or place a marker on the map.").bind(this);
                return;
            }
            locations.sort(function (a, b) { return b.score - a.score; });
            this._originGeocodeLock = false;
            this._addOriginGraphicFromPoint(locations[0].location);
        },

        /*******************************************************************************************
        * _dropOriginPoint
        *       The event handler for when a user requests to drop an origin point to route from.
        *******************************************************************************************/
        _dropOriginPoint: function () {
            if (this._dropOriginHandler == null) {
                topic.publish("map-acquire", { target: this, acquired: true });
                this._dropOriginHandler = on.once(this.map, 'click', this._createOriginFromMapClick.bind(this));
            }
        },

        /*******************************************************************************************
        * _addGraphicFromMapClick
        *       Gets and sends the mapPoint property of an on-click event from the map to the
        *       _addOriginGraphicFromPoint function.
        *@param evt: Event
        *       The map's on-click event which returns the map point of a mouse click.
        ********************************************************************************************/
        _createOriginFromMapClick: function (evt) {
            var pt = evt.mapPoint;
            if (this.useAddrStartRadio.get('checked') == 'checked' || this.useAddrStartRadio.get('checked') == true) {
                this.useCoordStartRadio.set('checked', 'checked');
            }
            this._addOriginGraphicFromPoint(pt);
            this._dropOriginHandler = null;
            topic.publish("map-acquire", { target: this, acquired: false });
        },

        /*******************************************************************************************
        * _createOriginPointFromCoords
        *       Gets the coordinates placed in the input values and then drops a point on the map.
        ********************************************************************************************/
        _createOriginPointFromCoords: function () {
            var pt = new Point(this.originByCoordsLonInput.get('value'), this.originByCoordsLatInput.get('value'));
            this._addOriginGraphicFromPoint(pt);
        },

        /*******************************************************************************************
        * _addOriginGraphicFromPoint
        *       Once a Point geometry object has been constructed, this function will add the origin
        *       marker to the map at the designated point and toggle the necessary UI buttons.
        *@param pt: Point
        *       An esri Point object that is used to place the origin marker.
        ********************************************************************************************/
        _addOriginGraphicFromPoint: function (pt) {
            if (this._originMarker != null) {
                this._clearStartPoint();
            }
            this.originByCoordsLatInput.set('value', pt.getLatitude());
            this.originByCoordsLonInput.set('value', pt.getLongitude());
            this._originMarker = new Graphic(pt, this._originMarkerSymbol, null, this._originInfoTemplate);
            this._routingGraphicsLayer.add(this._originMarker);
            domStyle.set(this.placeStartBtnContainer, 'display', 'none');
            domStyle.set(this.clearStartBtnContainer, 'display', 'block');
        },

        /*******************************************************************************************
        * _clearStartPoint
        *       Removes the current origin marker from the map, sets the marker to null, and clears
        *       out the Lat/Lon input boxes.
        ********************************************************************************************/
        _clearStartPoint: function () {
            this._routingGraphicsLayer.remove(this._originMarker);
            this._originMarker = null;
            this.originByCoordsLatInput.set('value', '');
            this.originByCoordsLonInput.set('value', '');
            domStyle.set(this.clearStartBtnContainer, 'display', 'none');
            domStyle.set(this.placeStartBtnContainer, 'display', 'block');
        },

        /*******************************************************************************************
        * _geolocateDestination
        *       onBlur handler for the destination address input which will take the user-entered
        *       address and attempt a geocode for it.
        ********************************************************************************************/
        _geolocateDestination: function (evt) {
            if (this.destByAddressInput.get('value') == "" || this.destByAddressInput.get('value') == null) {
                return;
            }
            var loc = new Locator(this.geocodeServer),
                addrParam = {
                    "SingleLine": this.destByAddressInput.get('value')
                };
            var locParams = {
                address: addrParam,
                maxLocations: 3,
                outFields: ["*"]
            };
            this._destGeocodeLock = true;
            loc.addressToLocations(locParams, this._processDestGeolocation.bind(this), this._geolocationError.bind(this));
        },

        /*******************************************************************************************
        * _processDestGeolocation
        *       The on-complete handler for the geolocation task for the destination point.
        *@param locations: Array
        *       An array of location results after a successful attempt to geocode an address.
        ********************************************************************************************/
        _processDestGeolocation: function (locations) {
            if (locations.length < 1) {
                this._geolocationError("No location results returned for starting address. "
                    + "Please re-enter a starting address or place a marker on the map.").bind(this);
                return;
            }
            locations.sort(function (a, b) { return b.score - a.score; });
            this._destGeocodeLock = false;
            this._addDestGraphicFromPoint(locations[0].location);
        },

        /*******************************************************************************************
        * _dropDestPoint
        *       Allows the user to drop a destination point onto the map and adds the appropriate
        *       graphic, and populates the Lat/Lon inputs.
        ********************************************************************************************/
        _dropDestPoint: function () {
            if (this._dropDestinationHandler == null) {
                topic.publish("map-acquire", { target: this, acquired: true });
                this._dropDestinationHandler = on.once(this.map, 'click', this._createDestFromMapClick.bind(this));
            }
        },

        /********************************************************************************************
        * _addDestGraphicFromMapClick
        *       Gets a point from the map onClick event and then passes that geometry to
        *      the function used to add a destination marker to the map.
        *@param evt: Map Event
        *       The onClick event passed in by the map.
        *********************************************************************************************/
        _createDestFromMapClick: function (evt) {
            var pt = evt.mapPoint;
            if (this.useAddrDestRadio.get('checked') == 'checked' || this.useAddrDestRadio.get('checked') == true) {
                this.useCoordDestRadio.set('checked', 'checked');
            }
            this._addDestGraphicFromPoint(pt);
            this._dropDestinationHandler = null;
            topic.publish("map-acquire", { target: this, acquired: false });
        },

        /*******************************************************************************************
        * _addDestGraphicFromPoint
        *       Accepts a Point geometry object and builds a destination marker from it, then adds
        *       that marker to the map, toggling the relevant UI components.
        *@param pt: Point
        *       The Point geometry object representing the destination for the route.
        ********************************************************************************************/
        _addDestGraphicFromPoint: function (pt) {
            if (this._destinationMarker != null) {
                this._clearDestPoint();
            }
            this.destByCoordsLatInput.set('value', pt.getLatitude());
            this.destByCoordsLonInput.set('value', pt.getLongitude());
            this._destinationMarker = new Graphic(pt, this._destinationMarkerSymbol, null, this._destinationInfoTemplate);
            this._routingGraphicsLayer.add(this._destinationMarker);
            domStyle.set(this.placeDestBtnContainer, 'display', 'none');
            domStyle.set(this.clearDestBtnContainer, 'display', 'block');
        },

        /*******************************************************************************************
        * _clearDestPoint
        *       Removes the destination marker from the map, clears out the lat/lon inputs and toggles
        *       the "Clear"/"Choose" buttons for destination marker.
        ********************************************************************************************/
        _clearDestPoint: function () {
            this._routingGraphicsLayer.remove(this._destinationMarker);
            this._destinationMarker = null;
            this.destByCoordsLatInput.set('value', '');
            this.destByCoordsLonInput.set('value', '');
            domStyle.set(this.clearDestBtnContainer, 'display', 'none');
            domStyle.set(this.placeDestBtnContainer, 'display', 'block');
        },

        /*******************************************************************************************
        * _startRouteTask
        *       Will initialize the routing task and send off the request to the server resource,
        *       defining the callback and errback functions for the response.
        *@param evt: Event
        *       The on click event starting the task.
        ********************************************************************************************/
        _startRouteTask: function () {
            if (this._errorLockRelease) {
                this._errorLockRelease = false;
                return;
            } else if (this._originGeocodeLock || this._destGeocodeLock) {
                setTimeout(this._startRouteTask.bind(this), 500);
                return;
            }
        },

        /*******************************************************************************************
        * _geolocationError
        *       The error handler for failed attempts to process the geolocation results for the
        *       origin point.
        *@param err: String
        *       The error object for the failed geolocation event.
        *******************************************************************************************/
        _geolocationError: function (err) {
            if (typeof (err) == "string") {
                Common.errorDialog(err, "Geocoding Error");
            } else {
                Common.errorDialog("There was an error geocoding an address point."
                    + " Please re-enter the addresses and try again.", "Geocoding Error");
            }
            this._originGeocodeLock = false;
            this._destGeocodeLock = false;
            this._errorLockRelease = true;
        },

        /*******************************************************************************************
        * _locationError
        *       Error handler for an inability to retrieve the location from the browser's geolocation
        *       object.
        ********************************************************************************************/
        _locationError: function (err) {
            Common.errorDialog(err, "Location Error");
        }

    });
});