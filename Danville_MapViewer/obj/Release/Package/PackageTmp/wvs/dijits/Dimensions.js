﻿define([
    "dojo/_base/declare"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dojo/text!./templates/Dimensions.html"
    , "dojo/_base/lang"
    , "esri/units"
    , "dijit/form/Select"
    , "esri/geometry/Polygon"
    , "esri/geometry/Polyline"
    , "esri/geometry/Point"
    , "dojo/dom-style"
    , "dojo/on"
    , "esri/SpatialReference"
    , "wvs/libs/proj4js/main"
    , "../common/Converter"
    , "dijit/form/Textarea"
    , "../utilities/usng-amd"
], function (declare, _WidgetBase, _TemplatedMixin, template, lang, Units, Select, Polygon, Polyline, Point, domStyle, on, SpatialReference, Proj4js, Converter, Textarea, usng) {
    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,

        declaredClass: "wvs.dijits.Dimensions",

        constructor: function (params, srcNodeRef) {
            
            // graphic: Graphic
            //      the graphic to get dimensions on
            this.graphic = null;

            if (!params.graphic && !params.geometry) {
                throw new Error("no graphic or geometry defined for " + this.declaredClass);
            }
            
            lang.mixin(this, params || {});

            // geometry: Geometry
            //      the geometry to get dimensions on
            this.geometry = this.geometry ? this.geometry : this.graphic ? this.graphic.geometry : null;

            // Constants

            // lengthUnits: Object[]
            //      an array containing select values for all permissible length units (total length for polyline and perimeter for polygon)
            this.lengthUnits = [
                //{ label: "Centimeters", value: "CENTIMETERS" },
                { label: "Feet", value: Units.FEET },
                //{ label: "Inches", value: "INCHES" },
                { label: "Kilometers", value: Units.KILOMETERS },
                { label: "Meters", value: Units.METERS },
                { label: "Miles", value: Units.MILES, selected: true }
                //{ label: "Millimeters", value: "MILLIMETERS" },
                //{ label: "Nautical Miles", value: "NAUTICAL_MILES" },
                //{ label: "Yards", value: "YARDS" }
            ];

            // areaUnitsData: Object[]
            //      an array containing select values for all permissible area units (used for polygons)
            this.areaUnitsData = [
                { label: "Acres", value: Units.ACRES, selected: true },
                { label: "Sq. Centimeters", value: Units.SQUARE_CENTIMETERS },
                { label: "Sq. Decimeters", value: Units.SQUARE_DECIMETERS },
                { label: "Sq. Feet", value: Units.SQUARE_FEET },
                { label: "Sq. Inches", value: Units.SQUARE_INCHES },
                { label: "Sq. Kilometers", value: Units.SQUARE_KILOMETERS },
                { label: "Sq. Meters", value: Units.SQUARE_METERS },
                { label: "Sq. Miles", value: Units.SQUARE_MILES },
                { label: "Sq. Millimeters", value: Units.SQUARE_MILLIMETERS },
                { label: "Sq. Yards", value: Units.SQUARE_YARDS }
            ];

            // coordinateUnits: Object[]
            //      an array containing select values for all permissible coordinate units (used for points)
            this.coordinateUnits = [
                { label: "Degrees & Minutes", value: "dm", selected: true},
                { label: "Decimal Degrees", value: "dd" },
                { label: "Degrees Min Sec", value: "dms" },
                { label: "Map Units", value: "mu" },
                { label: "MGRS", value: "mgrs" },
                { label: "UTM", value: "utm" }
            ];

            // Instance variables

            // perimeter: Number
            //      represents the current perimeter for a Polygon
            this.perimeter = 0;
            // area: Number
            //      represents the current area for a Polygon
            this.area = 0;
            // totalLength: Number
            //      represents the total length for a Polyline
            this.totalLength = 0;

            // perimeterUnits: Unit
            //      represents the current units for the perimeter (polygon)
            this.perimeterUnits = Units.MILES;
            // distanceUnits: Unit
            //      represents the current units for the total length (polyline)
            this.distanceUnits = Units.MILES;
            // areaUnits: Unit
            //      represents the current units for the area (polygon)
            this.areaUnits = Units.ACRES;
        },
        postCreate: function () {
            this._initialize();
        },
        startup: function () {
            var geometry = this.geometry;

            // set default values 
            if (geometry.isInstanceOf(Polyline)) {
                this.set("totalLength", geometry.getLength(this.distanceUnits));
            }
            else if (geometry.isInstanceOf(Polygon)) {
                this.set("perimeter", geometry.getPerimeter(this.perimeterUnits));
                this.set("area", geometry.getArea(this.areaUnits));
            }
            else if (geometry.isInstanceOf(Point)) {
                this.set("coordinateUnit", "dm");
            }
        },
        _initialize: function () {
            // summary:
            //      initializes the widget, watches and creating child widgets based on geometry types
            var self = this,
                geometry = this.geometry;

            if (geometry.isInstanceOf(Polyline)) {
                this.totalLengthUnitSelect = new Select({
                    options: this.lengthUnits,
                    onChange: function () { self.set("distanceUnits", this.get("value")); }
                }, this.totalLengthUnitSelect);

                this.watch("totalLength", lang.hitch(this, function (name, oldVal, newVal) {
                    this.polylineLengthLabel.innerHTML = newVal;
                }));

                // Watch distance units
                this.watch("distanceUnits", lang.hitch(this, function (name, oldVal, newVal) {
                    this.set("totalLength", this.graphic.geometry.getLength(newVal));
                }));

                // Set up GUI
                domStyle.set(this.polylineDimensions, { display: "block" });
                domStyle.set(this.polygonDimensions, { display: "none" });
                domStyle.set(this.pointCoordinates, { display: "none" });
                this.own(on(this.geometryLink, "click", function () { self.viewGeometryDetails(); }));
            }
            else if (geometry.isInstanceOf(Polygon)) {
                this.perimeterUnitSelect = new Select({
                    options: this.lengthUnits,
                    onChange: function () { self.set("perimeterUnits", this.get("value")); }
                }, this.perimeterUnitSelect);

                this.areaUnitSelect = new Select({
                    options: this.areaUnitsData,
                    onChange: function () { self.set("areaUnits", this.get("value")); }
                }, this.areaUnitSelect);

                // Set up watches
                this.watch("perimeter", lang.hitch(this, function (name, oldVal, newVal) {
                    this.perimeterLabel.innerHTML = newVal;
                }));

                // Watch perimeter units
                this.watch("perimeterUnits", lang.hitch(this, function (name, oldVal, newVal) {
                    this.set("perimeter", this.graphic.geometry.getPerimeter(newVal));
                }));

                this.watch("area", lang.hitch(this, function (name, oldVal, newVal) {
                    this.areaLabel.innerHTML = newVal;
                }));

                // Watch area units
                this.watch("areaUnits", lang.hitch(this, function (name, oldVal, newVal) {
                    this.set("area", this.graphic.geometry.getArea(newVal));
                }));

                // Set up GUI
                domStyle.set(this.polygonDimensions, { display: "block" });
                domStyle.set(this.polylineDimensions, { display: "none" });
                domStyle.set(this.pointCoordinates, { display: "none" });

                this.own(on(this.geometryLink, "click", function () { self.viewGeometryDetails(); }));
            }
            else if (geometry.isInstanceOf(Point)) {
                
                this.pointCoordinateUnitSelect = new Select({
                    options: this.coordinateUnits,
                    onChange: function () { self.set("coordinateUnit", this.get("value")); }
                }, this.pointCoordinateUnitSelect);

                this.pointCoordinatesTextArea = new Textarea({
                    value: ""
                }, this.pointCoordinatesTextArea);

                this.watch("coordinateUnit", lang.hitch(this, function (name, oldVal, newVal) {
                    var point = new Point(geometry.x, geometry.y, geometry.spatialReference);

                    if (newVal != 'mu') {
                        Proj4js.transform(this.map.spatialReference.toProj4js(), new SpatialReference({ wkid: 4326 }).toProj4js(), point);
                    }
                    var output;
                    switch (newVal) {
                        case "dms":
                            output = "Lat:" + Converter.DDtoDegreesMinutesSeconds(point.y, "y", true) + "\nLon: " + Converter.DDtoDegreesMinutesSeconds(point.x, "x", true);
                            break;
                        case "dm":
                            output = "Lat: " + Converter.DDtoDegreesDecimalMinutes(point.y, "y", true, true) + "\nLon: " + Converter.DDtoDegreesDecimalMinutes(point.x, "x", true, true);
                            break;
                        case "dd":
                            output = "Lat: " + point.y.toFixed(5) + "\nLon: " + point.x.toFixed(5);
                            break;
                        case "mu":
                            output = "x: " + point.x.toFixed(5) + "\ny: " + point.y.toFixed(5);
                            break;
                        case "mgrs":
                            this._usngPrecision = 5;
                            if (this.map._mapUnitsPerPixelX > 10) {
                                this._usngPrecision = 4;
                            }
                            if (this.map._mapUnitsPerPixelX > 100) {
                                this._usngPrecision = 3;
                            }
                            output = usng.LLtoUSNG(point.y, point.x, this._usngPrecision);
                            break;
                        case "utm":
                            output = usng.LLtoUTMString(point.y, point.x);
                            break;
                        default:
                            throw new Error("unsupported coordinate unit");
                    }

                    this.pointCoordinatesTextArea.set("value", output);
                }));

                domStyle.set(this.pointCoordinates, { display: "block" });
                domStyle.set(this.polylineDimensions, { display: "none" });
                domStyle.set(this.polygonDimensions, { display: "none" });
                domStyle.set(this.geometryLink, { display: "none" });
            }
            else {
                throw new Error("unsupported geometry type for " + this.declaredClass);
            }
        },
        viewGeometryDetails: function () {
            // summary:
            //      provide the geometry json in an alert window
            var jsonString = JSON.stringify(this.geometry.toJson());
            alert(jsonString);
        }
    });

});