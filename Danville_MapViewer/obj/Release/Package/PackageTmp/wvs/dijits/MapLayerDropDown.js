﻿console.log("wvs/dijits/MapLayerDropDown.js");

define([
    "dojo/_base/declare"
    , "dojo/_base/lang"
    , "dijit/_WidgetBase"
    , "esri/dijit/LayerSwipe"
    , "dijit/form/Button"
    , "dijit/form/Select"
    , "dojo/_base/array"
    , "dijit/form/DropDownButton"
    , "dijit/Tree"
    , "esri/layers/ArcGISTiledMapServiceLayer"
    , "esri/layers/ArcGISDynamicMapServiceLayer"
    , "esri/layers/FeatureLayer"
    , "dojo/store/Observable"
    , "dojo/store/Memory"
    , "dijit/tree/ObjectStoreModel"
    , "dojo/on"
    , "dojo/Evented"
], function (declare, lang, _WidgetBase, LayerSwipe, Button, Select, array, DropDownButton, Tree, ArcGISTiledMapServiceLayer, ArcGISDynamicMapServiceLayer, FeatureLayer, Observable, Memory, ObjectStoreModel, on, Evented) {
    return declare([_WidgetBase, Evented], {

        declaredClass: "wvs.dijits.MapLayerDropDown",

        constructor: function (params, srcNodeRef) {
            // configurable params

            // exclusionList: String[]
            //      an array of strings representing layer types to exclude from the drop down
            this.exclusionList = [];

            
            // includeAllVisible: Boolean
            //      Places << All Visible >> at the top of the tree to filter out visible layers only
            this.includeAllVisible = false;

            // showOnlyVisibleLayers: Boolean
            //      Only show layers that are currently visible
            this.showOnlyVisibleLayers = false;

            // onLayerSelect: Function
            //      An overridable property to define what happens when a tree node is clicked
            this.onLayerSelect = null;

            lang.mixin(this, params || {});

            // instance vars

            // store: Memory
            //      The store to be used by the model for the tree
            this.store = new Observable(new Memory({
                data: [{ id: 'root', name: 'Root Node', type: 'root' }],
                getChildren: function (object) {
                    return this.query({ parent: object.id });
                }
            }));

            // model: ObjectStoreModel
            //      The model that feeds the tree
            this.model = new ObjectStoreModel({
                store: this.store,
                query: { id: 'root' },
                mayHaveChildren: this._mayHaveChildren
            });

            // _ids: Integer
            //      Used internally to assign ids to tree nodes
            this._ids = 0;
        },
        postCreate: function () {
            this.inherited(arguments);

            // A 'hack' used to render HTML within tree nodes
            var MyTreeNode = declare(Tree._TreeNode, {
                _setLabelAttr: { node: "labelNode", type: "innerHTML" }
            });

            // tree: Tree
            //  The Tree dijit that populates the DropDownButton
            this.tree = new Tree({
                model: this.model,
                showRoot: false,
                openOnClick: false,
                _createTreeNode: function(args){
                    return new MyTreeNode(args);
                },
                style:"width:400px;height:300px;background:white;",
                // Override icon classes based on our types
                getIconClass: this._getIconClass,
                getLabel: this._getLabel
            });

            
            if (this.includeAllVisible) {
                this.store.put({ parent: "root", type: "VisibleLayers", layer: null, id: "VisibleLayers", name: "<< All Visible >>" });
            }
            
            this.loadAllMapLayers();

            // Bind tree/widget events

            //  represents a node click
            this.own(on(this.tree, "click", lang.hitch(this, this._onTreeClick)));

            //  fires when a layer is added to the map
            this.own(on(this.map, "layer-add", lang.hitch(this,function (evt) {
                this.loadMapLayer(evt.layer);
            })));

            //  fires when a layer is removed from the map
            this.own(on(this.map, "layer-remove", lang.hitch(this, function (evt) {
                this._removeItemByLayerId(evt.layer.id);
            })));
        },
        startup: function () {
            this.inherited(arguments);
            this.dropdownButton = new DropDownButton({
                dropDown: this.tree,
                label: this.includeAllVisible ? "<< All Visible >>" : "Layers"
            });
            this.dropdownButton.placeAt(this.domNode);
            this._toggleFirstNode();
        },
        _toggleFirstNode: function () {
            if (this.includeAllVisible) {
                // TODO: Remove stupid hack to trigger node click in MapLayerDropDown
                var ls = this;
                var n = this.tree.getNodesByItem(this.store.query({ id: "VisibleLayers" })[0])[0];
                var e = { stopPropagation: function () { }, preventDefault: function () { }, target: n.domNode };
                setTimeout(function () { ls.tree._onClick(n, e); }, 100);
            }
        },
        _mayHaveChildren: function (item) {
            // summary:
            //      a function used in the tree model to determine if an item type may have child nodes
            //  returns: 
            //      whether the node may have children
            var canHaveChildren;

            switch (item.type) {
                case "VisibleLayers":
                    canHaveChildren = false;
                    break;
                case "FeatureLayer":
                    canHaveChildren = false;
                    break;
                case "ArcGISTiledMapServiceLayer":
                    canHaveChildren = false;
                    break;
                case "ArcGISDynamicMapServiceLayer":
                    canHaveChildren = true;
                    break;
                case "Basemap":
                    canHaveChildren = false;
                    break;
                case "ArcGISDynamicMapServiceGroupLayer":
                    canHaveChildren = true;
                    break;
                case "ArcGISDynamicMapServiceSubLayer":
                    canHaveChildren = false;
                    break;
                case "root":
                    canHaveChildren = true;
                    break;
                default:
                    canHaveChildren = true;
                    break;
            }
            return canHaveChildren; // Boolean
        },
        _getIconClass: function (item, opened) {
            // summary:
            //      determines the iconClass property of a tree node based on an item type
            //  returns: 
            //      the icon class for an item type

            // TODO: Implement special icons for layer types
            return ""; // String
        },
        _getLabel: function (item, opened) {
            // summary:
            //      generates a label for a tree node based on item type and/or its opened status
            // item: Object
            //      the store item which is associated with the tree
            // opened: Boolean
            //      is 
            //  returns: 
            //      a label string

            var label;
            switch (item.type) {
                case "VisibleLayers":
                    label = item.name;
                    break;
                case "FeatureLayer":
                    label = item.name;
                    break;
                case "ArcGISTiledMapServiceLayer":
                    label = item.name;
                    break;
                case "ArcGISDynamicMapServiceLayer":
                    label = "<strong>" + item.name + "</strong>";
                    break;
                case "Basemap":
                    label = item.name
                    break;
                case "ArcGISDynamicMapServiceGroupLayer":
                    label = "<strong>" + item.name + "</strong>";
                    break;
                case "ArcGISDynamicMapServiceSubLayer":
                    label = item.name;
                    break;
                case "root":
                    label = "";
                    break;
                default:
                    label = ""
                    break;
            }
            
            return label; // String
        },
        loadAllMapLayers: function () {
            // summary:
            //      loads all layers from the map into the tree
            var map = this.map;

            array.forEach(map.layerIds, function (layerId) {
                var layer = map.getLayer(layerId);
                this.loadMapLayer(layer);
            }, this);
        },
        loadMapLayer: function (layer) {
            // summary:
            //      loads a layer into the tree, if it is permissible
            // layer: Layer
            //      the layer to add
            if (this._isLayerShown(layer)) {
                if (layer.isInstanceOf(ArcGISDynamicMapServiceLayer)) {
                    this._loadDynamicService(layer);
                }
                else if (layer.isInstanceOf(ArcGISTiledMapServiceLayer)) {
                    this.store.put({ parent: "root", type: "ArcGISTiledMapServiceLayer", layer: layer, id: layer.id, name: layer.id });
                }
                else if (layer.isInstanceOf(FeatureLayer)) {
                    this.store.put({ parent: "root", type: "FeatureLayer", layer: layer, id: layer.id, name: layer.id });
                }
            }
        },
        _onTreeClick: function (item, node, evt) {
            // summary:
            //      event handler for a when a node is clicked within the tree
            // item: Object
            //      the item attached to the node
            // node: _TreeNode
            //      the tree node
            // evt: Object
            //      the mouse event
            if (this.onLayerSelect) {
                this.onLayerSelect(item, node, evt);
            }
            else {
                this.dropdownButton.set("label", item.name);
                this.dropdownButton.closeDropDown();
                if (item.type === "VisibleLayers") {
                    this.emit("layer-selected", this._getAllVisibleLayers());
                }
                else {
                    this.emit("layer-selected", item.layer);
                }
            }
        },
        _removeItemByLayerId: function (id) {
            // summary:
            //      recursive delete of store items given a map layer id
            var childNodes = this.store.query({ parent: id });

            array.forEach(childNodes, function (child) {
                this._removeItemByLayerId(child.id);
            }, this);

            this.store.remove(id);
        },
        _getAllVisibleLayers: function () {
            // summary:
            //      get all visible layers from the map for those permissible to be in the tree
            // returns:
            //      an array of layers that are visible
            var layers = [];
            var layerIds = this.map.layerIds;
            array.forEach(layerIds, function (layerId) {
                var layer = this.map.getLayer(layerId);
                if (this._isLayerShown(layer)) {
                    if (layer.isInstanceOf(ArcGISTiledMapServiceLayer) && layer.getVisible()) {
                        layers.add(layer);
                    }
                    else if (layer.isInstanceOf(ArcGISDynamicMapServiceLayer) && layer.visible) {
                        var mapService = { layer: layer, visibleSubLayers: [] };
                        array.forEach(layer.layerInfos, function (layerInfo) {
                            // Ignore group layers
                            if (layerInfo.subLayerIds === null && layerInfo.getVisible()) {
                                mapService.visibleSubLayers.push(layerInfo);
                            }
                        }, this);

                        if (mapService.visibleSubLayers.length > 0) {
                            layers.push(mapService);
                        }
                    }
                }
            }, this);

            var graphicLayerIds = this.map.graphicLayerIds;
            array.forEach(graphicLayerIds, function (graphicLayerId) {
                if (this._isLayerShown(layer)) {
                    if (layer.isInstanceOf(FeatureLayer) && layer.visible) {
                        layers.add(layer);
                    }
                }
            }, this);

            return layers; // Layer[]
        },
        _loadDynamicService: function (layer) {
            // summary:
            //      Loads a ArcGISDynamicMapServiceLayer into the tree
            //  layer: ArcGISDynamicMapServiceLayer
            //      the layer to initiate
            var self = this;
            this.store.put({ parent: "root", type: "ArcGISDynamicMapServiceLayer", layer: layer, id: layer.id, name: layer.getServiceName() });

            var getLayerInfoTreeId = function (layerInfo) {
                var id;
                if (layerInfo && !layerInfo.isInstanceOf(ArcGISDynamicMapServiceLayer)) {
                    var parentLayerInfo = layerInfo.getParentLayerInfo();
                    id = layerInfo.id;
                    if (parentLayerInfo) {
                        while (!parentLayerInfo.isInstanceOf(ArcGISDynamicMapServiceLayer)) {
                            id = parentLayerInfo.id + "_" + id;
                            parentLayerInfo = parentLayerInfo.getParentLayerInfo();
                        }
                    }
                    id = layer.id + "_" + id;
                }
                else {
                    id = layer.id;
                }
                return id;
            };

            layer.getExtendedLayerInfo(function () {
                array.forEach(layer.layerInfos, function (layerInfo) {
                    if (!self.showOnlyVisibleLayers || (self.showOnlyVisibleLayers && layerInfo.visible)) {
                        layerInfo.url = layer.url + "/" + layerInfo.id;
                        var subLayerIds = layerInfo.subLayerIds,
                            parentLayerId = layerInfo.parentLayerId;
                        self.store.put({ parent: getLayerInfoTreeId(parentLayerId === -1 ? layer : layer.getLayer(parentLayerId)), name: layerInfo.name, id: getLayerInfoTreeId(layerInfo), layer: layerInfo, type: subLayerIds ? "ArcGISDynamicMapServiceGroupLayer" : "ArcGISDynamicMapServiceSubLayer" });
                    }
                });
            });
        },
        _getId: function () {
            // summary:
            //      Generates an id for a tree node
            // returns:
            //      an id
            return this.ids++; // Integer
        },
        _isLayerShown: function (layer) {
            // summary:
            //      Determines if the layer is to be shown in the tree
            //  layer: Layer
            //      the layer to show or not
            // returns:
            //      whether the layer is visible or not
            var exclusionList = this.exclusionList,
                shown = false;

            if (this.showOnlyVisibleLayers && !layer.visible) {
                return false;
            }

            if (layer.isInstanceOf(ArcGISDynamicMapServiceLayer) && !array.some(exclusionList, function(exclusion) { return exclusion === "ArcGISDynamicMapServiceLayer" })) {
                shown = true;
            }
            else if (layer.isInstanceOf(ArcGISTiledMapServiceLayer) && !array.some(exclusionList, function (exclusion) { return exclusion === "ArcGISTiledMapServiceLayer" })) {
                shown = true;
            }
            else if (layer.isInstanceOf(FeatureLayer) && !array.some(exclusionList, function (exclusion) { return exclusion === "FeatureLayer" })) {
                shown = true;
            }

            return shown; // Boolean
        }
    });
});