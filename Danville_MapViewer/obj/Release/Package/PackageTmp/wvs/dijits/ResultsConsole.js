﻿console.log("wvs/dijits/ResultsConsole.js");

define([
    "dojo/_base/declare"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dojo/text!./templates/ResultsConsole.html"
    , "dojo/_base/lang"
    , "dojo/on"
    , "dojo/_base/array"
    , "dojo/query"
    , "dgrid/Grid"
    , "dgrid/extensions/ColumnResizer"
    , "dgrid/extensions/ColumnHider"
    , "dgrid/extensions/Pagination"
    , "dijit/layout/TabContainer"
    , "dijit/layout/ContentPane"
    , "dojo/store/Memory"
    , "require"
    , "dojo/dom-construct"
    , "dojo/dom-attr"
    , "dojo/dom-class"
    , "../common/SearchResultsStore"
    , "dojo/dom-style"
    , "dijit/DropDownMenu"
    , "dijit/MenuItem"
    , "dijit/form/DropDownButton"
    , "dijit/form/Button"
    , "../_base/config"
    , "esri/symbols/jsonUtils"
    , "esri/symbols/PictureMarkerSymbol"
    , "esri/symbols/SimpleLineSymbol"
    , "esri/geometry/Point"
    , "esri/geometry/Polygon"
    , "esri/geometry/Polyline"
    , "esri/geometry/Multipoint"
    , "esri/InfoTemplate"
    , "esri/graphic"
    , "esri/graphicsUtils"
    , "esri/layers/GraphicsLayer"
    , "wvs/common/Common"
    , "esri/lang"
    , "./FormatExport"
    , "../extensions/esri/graphicsUtils"
], function (declare, _WidgetBase, _TemplatedMixin, template, lang, on, array, query, Grid, ColumnResizer, ColumnHider, Pagination, TabContainer, ContentPane, Memory, require, domConstruct, domAttr, domClass, SearchResultsStore, domStyle, DropDownMenu, MenuItem, DropDownButton, Button, wvsConfig, symbolUtils, PictureMarkerSymbol, SimpleLineSymbol, Point, Polygon, Polyline, Multipoint, InfoTemplate, Graphic, graphicsUtils, GraphicsLayer, Common, esriLang, FormatExport) {
    // module:
    //      wvs/dijits/ResultsConsole
    return declare([_WidgetBase, _TemplatedMixin], {

        templateString: template,

        declaredClass: "wvs.dijits.ResultsConsole",

        // defaultColumnsShown: Integer
        //      The default number of columns to show in a dgrid (others will be hidden by default)
        defaultColumnsShown: 10,

        // exportService: String
        //      the service to perform export actions against
        exportService: null,

        // _selectAllLimit: Integer
        //      the number of features that can be selected (highlighted) at once
        _selectAllLimit: 100,

        // bindToStore: Boolean
        //      signifies whether or not this results console will listen to the input store
        bindToStore: true,

        constructor: function (params, srcNodeRef) {
            // Array of integers declaring paging options
            this.pagingSizeOptions = [10, 25, 50];

            // Required parameters
                if (!params.store && !params.bindToStore) {
                    throw new Error("no store defined for " + this.declaredClass);
                }
            lang.mixin(this, params || {});

            // Instance variables
            this.polygonSymbol = this.polygonSymbol ? symbolUtils.fromJson(this.polygonSymbol) : symbolUtils.fromJson(wvsConfig.defaults.symbols.polygon);
            this.pointSymbol = this.pointSymbol ? symbolUtils.fromJson(this.pointSymbol) : symbolUtils.fromJson(wvsConfig.defaults.symbols.point);
            this.lineSymbol = this.lineSymbol ? symbolUtils.fromJson(this.lineSymbol) : symbolUtils.fromJson(wvsConfig.defaults.symbols.line);


            // tracks the currently highlighted/selected features
            this.featureHighlights = [];
            this.gridMap = {};
            this.gridIds = 0;
            this.graphicsLayer = new GraphicsLayer();
            this.graphicsLayer._visibleInToc = false;
            this.exportService = wvsConfig.defaults.services.exportService ? wvsConfig.defaults.services.exportService : null;
        },
        postCreate: function () {
            domConstruct.create("img", { src: require.toUrl("./images/loading_big.gif"), display: "none" }, this.resultsLoadingMessage);

            // Create a custom dgrid with the desired extensions
            this.CustomGrid = declare([Grid, Pagination, ColumnResizer, ColumnHider]);

            this.map.addLayer(this.graphicsLayer);

            if (this.bindToStore) {
                // get all results
                this.resultSets = this.store.query();

                // observe the result set from our store
                var observeHandler = function (object, removedFrom, insertedInto) {
                    if (removedFrom > -1) {
                        this._removeSearchResult(object);
                    }
                    if (insertedInto > -1) {
                        this._addResultSetToDropdown(object);
                    }
                };
                this._observeHandle = this.resultSets.observe(lang.hitch(this, observeHandler));

                this.own(
                    on(this.store, "data-loading", lang.hitch(this, function () {
                        this.clear();
                        domStyle.set(this.noResultsMessage, "display", "none");
                        domStyle.set(this.resultsLoadingMessage, "display", "block");
                    })),
                    on(this.store, "data-loaded", lang.hitch(this, function () {
                        domStyle.set(this.resultsTabs.domNode, "display", "block");
                        domStyle.set(this.resultsLoadingMessage, "display", "none");
                    }))
                );
            }
        },
        startup: function () {
            this.resultsTabs = new TabContainer({
                style: "height: 100%; width: 100%;"
            }, this.resultsTabs);
            this.resultsTabs.startup();
            this.inherited(arguments);
        },
        destroy: function () {
            this.map.removeLayer(this.graphicsLayer);
            this.inherited(arguments);
        },
        resize: function () {
            // summary:
            //      Resizes the results console (used for when the results console is a child of a layout widget)
            // returns:
            //      undefined
            this.resultsTabs.resize();
        },
        _showResults: function (resultsFound) {
            // summary:
            //      Controls the visual representation of the results console based upon wheter or not a result was found
            // returns:
            //      undefined
            domStyle.set(this.resultsLoadingMessage, "display", "none");

            if (resultsFound) {
                domStyle.set(this.resultsTabs.domNode, "display", "block");
                domStyle.set(this.noResultsMessage, "display", "none");
                domStyle.set(this.resultsLoadingMessage, "display", "none");
                this.emit("result-found");
            }
            else {
                this.clear();
                domStyle.set(this.noResultsMessage, "display", "block");
            }
        },
        clear: function(){
            this._clearHighlights();
            this._clearGrids();
            domStyle.set(this.resultsTabs.domNode, "display", "none");
        },
        _zoomToFeature: function (feature) {
            // summary:
            //      Zooms to a given feature
            // description:
            //      Zooms to a given feature. Polygon, Polyline, and Point have been tested. Multipoint has not been tested.
            // returns:
            //      undefined
            var geometry = feature.geometry,
                extent = null;

            if (geometry.isInstanceOf(Point)) {
                extent = graphicsUtils.pointToExtent(geometry);
            }
            else if (geometry.isInstanceOf(Multipoint)) {
                extent = geometry.points.length > 1 ? geometry.getExtent() : graphicsUtils.pointToExtent(geometry.points[0]);
            }
            else {
                extent = geometry.getExtent();
            }

            if (extent != null) {
                this.map.setExtent(extent, true);
            }
            else {
                console.warn("a null extent was found for " + feature);
            }
        },
        _clearHighlights: function () {
            // summary:
            //      Clear highlight graphics from the result console's graphic layers
            // returns:
            //      undefined
            this.graphicsLayer.clear();
            this.featureHighlights = [];
        },
        _clearGrids: function () {
            // summary:
            //      Destroys all grid instances
            // returns:
            //      undefined
            for (var id in this.gridMap) {
                this._destroyGrid(this.gridMap[id]);
            }
            gridMap = {};
        },
        _toggleGeometryHighlight: function (feature, highlight) {
            // summary:
            //      Adds/removes a highlight graphic for a particular feature.
            // description:
            //      Adds/removes a highlight graphic for a particular feature. Polygon, Polyline, and Point have been tested. Multipoint has not been tested.
            // returns:
            //      undefined
            var geometry = feature.geometry,
                highlightGraphic = null;

            // highlight the feature if highlight is strue and the feature is not already highlighted
            if (highlight && !array.some(this.featureHighlights, function (featureHighlight) { return featureHighlight.feature === feature })) {
                if (geometry.isInstanceOf(Point)) {
                    highlightGraphic = new Graphic(geometry, this.pointSymbol);
                }
                else if (geometry.isInstanceOf(Polygon)) {
                    highlightGraphic = new Graphic(geometry, this.polygonSymbol);
                }
                else if (geometry.isInstanceOf(Polyline)) {
                    highlightGraphic = new Graphic(geometry, this.lineSymbol);
                }

                if (highlightGraphic != null) {
                    highlightGraphic.attributes = feature.attributes;
                    highlightGraphic.setInfoTemplate(this.infoTemplate);
                    this.featureHighlights.push({ feature: feature, highlight: highlightGraphic });
                    this.graphicsLayer.add(highlightGraphic);
                }
            } else if(!highlight) {
                // Otherwise, find & remove the highlight
                var featureIndex = -1,
                    featureHighlights = this.featureHighlights
                for (var i = 0, il = featureHighlights.length; i < il; i++) {
                    if (featureHighlights[i].feature === feature) {
                        this.graphicsLayer.remove(featureHighlights[i].highlight);
                        featureIndex = i;
                        break;
                    }
                }
                if (featureIndex !== -1) {
                    this.featureHighlights.splice(featureIndex, 1);
                }
            }
        },
        _addResultSetToDropdown: function (searchResult) {
            // summary:
            //      Takes a SearchResult from the SearchResultStore and creates a grid/tab for the result
            // returns:
            //      undefined

            // if there are results, show the proper GUI elements and create the tab/grid
            if (searchResult.searchResult.resultsSet.length > 0) {
                this._showResults(true);
                array.forEach(searchResult.searchResult.resultsSet, function (resultSet) {
                    this._createGrid(resultSet);
                    this.gridIds++;
                }, this);
            }
            else {
                this._showResults(false);
            }
        },
        add: function(searchResult){
            // summary:
            //      Takes a SearchResult and creates a grid/tab for the result
            // returns:
            //      undefined

            // if there are results, show the proper GUI elements and create the tab/grid
            if (searchResult.resultsSet.length > 0 || searchResult.length > 0) {
                this._showResults(true);
                array.forEach(searchResult.resultsSet, function (resultSet) {
                    this._createGrid(resultSet);
                    this.gridIds++;
                }, this);
            }
            else {
                this._showResults(false);
            }
        },
        _removeSearchResult: function (searchResult) {
            // summary:
            //      Removes a tab/grid based upon the given SearchResult
            // returns:
            //      undefined
            var gridMap = this.gridMap;

            array.forEach(searchResult.searchResult.resultsSet, function(resultSet){
                for (var id in gridMap) {
                    var gridObject = gridMap[id];
                    if (gridObject.resultSet === resultSet) {
                        this._destroyGrid(id);
                    }
                }
            }, this);
        },
        _destroyGrid: function (id) {
            // summary:
            //      Destroys a grid instance
            // description:
            //      Destroys a grid instance. Removes grid events, destroys the dgrid, removes the associated tab and deletes the object from the grid map.
            // returns:
            //      undefined
            var gridInfo = this.gridMap[id];
            if (gridInfo) {
                array.forEach(gridInfo.events, function (gridEvent) {
                    gridEvent.remove();
                });
                gridInfo.grid.destroy();
                this.resultsTabs.removeChild(gridInfo.contentPane);
                delete this.gridMap[id];
            }
        },
        _createActionMenuItemForGrid: function (grid, hasGeometry) {
            // summary:
            //      Creates an 'action bar' for a given grid
            // returns:
            //      undefined
            var containingDiv = domConstruct.create("div");

            var selectDropDown = new DropDownMenu({ style: "display:none;" });

            // Select/highlight all features for the given list (within _selectAllLimit)
            var selectAll = new MenuItem({
                label: "All",
                onClick: lang.hitch(this, function () {
                    // Mark all graphics as checked
                    array.forEach(grid.store.data, function (item) {
                        item.resultGraphic._checked = true;
                    }, this);

                    // Refresh list
                    grid.refresh();
                })
            });

            selectDropDown.addChild(selectAll);

            // Clear/unhighlight all features for the given list
            var clearSelection = new MenuItem({
                label: "None",
                onClick: lang.hitch(this, function () {
                    array.forEach(grid.store.data, function (item) {
                        item.resultGraphic._checked = false;
                    }, this);

                    grid.refresh();
                })
            });

            selectDropDown.addChild(clearSelection);

            var selectDropDownButton = new DropDownButton({
                dropDown: selectDropDown,
                label: "Select",
                disabled: !hasGeometry,
                tooltip: !hasGeometry ? "The following data does not have associated geometry and cannot be selected" : null
            });

            selectDropDownButton.placeAt(containingDiv);

            if (this.exportService) {
                // Export menu
                var dropdownMenu = new DropDownMenu({ style: "display:none;" });

                dropdownMenu.addChild(new MenuItem({
                    label: "Export All",
                    onClick: lang.hitch(this, function () {
                        var features = array.map(grid.store.data, function (item) {
                            return new Graphic(item.resultGraphic.geometry, item.resultGraphic.symbol, esriLang.filter(item.resultGraphic.attributes, function (value) { return typeof value !== "object"; }));
                        });
                        Common.showWidgetInDialog(FormatExport, { features: features });
                    })
                }));

                // Export Selected
                dropdownMenu.addChild(new MenuItem({
                    label: "Export Selected",
                    onClick: lang.hitch(this, function () {
                        var features = array.map(array.filter(grid.store.data, function (i) {
                            return i.resultGraphic._checked;
                        }), function (item) {
                            return new Graphic(item.resultGraphic.geometry, item.resultGraphic.symbol, esriLang.filter(item.resultGraphic.attributes, function (value) { return typeof value !== "object"; }));
                        });
                        if (features.length)
                            Common.showWidgetInDialog(FormatExport, { features: features });
                        else
                            Common.errorDialog("Please make a selection");
                    })
                }));


                var dropdownButton = new DropDownButton({
                    dropDown: dropdownMenu,
                    label: "Export",
                    disabled: !hasGeometry,
                    tooltip: !hasGeometry ? "The following data does not have associated geometry and cannot be exported" : null
                    
                });
                dropdownButton.placeAt(containingDiv);
            }

            return containingDiv; // DomNode
        },
        _createGrid: function (resultSet) {
            // summary:
            //      Creates a grid for a given SearchResult
            // returns:
            //      undefined
            var self = this;

            // If our given result has a geometry, add the appropriate columns and their events
            var colModel = resultSet.hasGeometry ? {
                toolkitSelectGraphic: 
                    {
                        label: " ",
                        unhidable: true,
                        sortable: false,
                        renderCell: function (object, value, node, options) {
                            var checkbox = domConstruct.create("input", { type: "checkbox" }, node);
                            var grid = this.grid;
                            var onClick = function () {
                                var checked = this.checked;
                                object.resultGraphic._checked = checked;
                                var row = grid.row(object);
                                if (checked) {
                                    domClass.add(row.element, "dgrid-selected");
                                }
                                else {
                                    domClass.remove(row.element, "dgrid-selected");
                                }
                                self._toggleGeometryHighlight(object.resultGraphic, checked);
                            };
                            on(checkbox, "click", onClick);
                            if (object.resultGraphic._checked) {
                                domAttr.set(checkbox, "checked", "checked");
                                setTimeout(function () {
                                    var row = grid.row(object);
                                    if (object.resultGraphic._checked) {
                                        domClass.add(row.element, "dgrid-selected");
                                    }
                                    else {
                                        domClass.remove(row.element, "dgrid-selected");
                                    }
                                }, 100);
                            }
                    }
                },
                toolkitZoomTo: {
                    label: " ",
                    unhidable: true,
                    sortable: false,
                    renderCell: function (object, value, node, options) {
                        var img = domConstruct.create("img", { src: require.toUrl('./images/zoom.gif'), title: "Zoom To", style: "cursor:pointer;"}, node);
                        on(img, "click", function (evt) {
                            self._zoomToFeature(object.resultGraphic);
                        });
                    }
                }
            } : {};

            // TODO: Apply formatting based upon field types
            array.forEach(resultSet.fields, function (field, index) {
                // TODO: Remove this in future versions of dgrid (which currently has problems with names that have '()' in them
                var name = field.alias.replace(/\(\)/, "");
                var hideByDefault = index >= this.defaultColumnsShown;
                colModel[name] = {
                    label: field.alias, hidden: hideByDefault,
                    renderCell: function (object, value, node, options) {
                        var parsedVal = Common.getHyperlink(name, value);
                        if (!parsedVal || typeof parsedVal !== "object") {
                            node.innerHTML = parsedVal;
                        }
                        else
                            domConstruct.place(parsedVal, node);
                    }
                };
            }, this);

            if (resultSet.hasGeometry) {
                colModel.resultGraphic = { label: "resultGraphic", hidden: true, unhidable: true };
                array.forEach(resultSet.data, function (dataRow, index) {
                    dataRow["toolkitSelectGraphic"] = index;
                    dataRow["toolkitZoomTo"] = index;
                });
            }

            var defaultIdProperty = "OBJECTID";
            var propertyFound = false;
            for (var col in colModel) {
                if (col === defaultIdProperty) {
                    propertyFound = true;
                }
            }

            if (!propertyFound) {
                // Set first column as idProperty
                for (var col in colModel) {
                    if (col !== "toolkitSelectGraphic") {
                        defaultIdProperty = col;
                        break;
                    }
                }
            }
            var gridId = this.gridIds;
            var resultContentPane = new ContentPane({
                title: resultSet.name,
                closable: true,
                style: "padding:0px;height:100%;width:100%;",
                onShow: lang.hitch(this, function () {
                    this.gridMap[gridId].grid.resize();
                }),
                onClose: lang.hitch(this, function () {
                    this._destroyGrid(gridId);
                })
            });

            // Create grid
            var grid = new this.CustomGrid({
                columns: colModel,
                pagingTextBox: false,
                firstLastArrows: true,
                pageSizeOptions: this.pagingSizeOptions,
                store: new Memory({ data: array.map(resultSet.data, function(d) { return d.attributes;}), idProperty: defaultIdProperty })
            });

            // Custom styles for the grid
            grid.styleColumn("toolkitSelectGraphic", "width:25px;");
            grid.styleColumn("toolkitZoomTo", "width:40px;");

            if (resultSet.hasGeometry) {
                grid.on("dgrid-refresh-complete", function (evt) {
                    array.forEach(grid.store.data, function (item) {
                        self._toggleGeometryHighlight(item.resultGraphic, typeof item.resultGraphic._checked !== "undefined" ? item.resultGraphic._checked : false);
                    }, this);

                    // show
                    array.forEach(evt.results, function (item) {
                        var checked = typeof item.resultGraphic._checked !== "undefined" ? item.resultGraphic._checked : false;
                        var row = grid.row(item);
                        if (checked) {
                            domClass.add(row.element, "dgrid-selected");
                        }
                        else {
                            domClass.remove(row.element, "dgrid-selected");
                        }
                    }, this);
                });
            }


            domConstruct.place(this._createActionMenuItemForGrid(grid, resultSet.hasGeometry), resultContentPane.containerNode);
            resultContentPane.addChild(grid);

            this.gridMap[gridId] = {
                grid: grid,
                resultSet: resultSet,
                contentPane: resultContentPane
            };
            grid.startup();

            this.resultsTabs.addChild(resultContentPane);
        }
    });
});