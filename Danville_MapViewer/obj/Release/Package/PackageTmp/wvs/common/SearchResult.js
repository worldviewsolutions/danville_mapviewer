﻿console.log("wvs/common/SearchResult.js");

define(["dojo/_base/declare", "dojo/_base/lang", "dojo/_base/array"],
    function (declare, lang, array, selector) {
        return declare("wvs.common.SearchResult", null, {
        
            /**
                url - url for search result endpoint (layer for query; mapservice for identify)
                resultsSet - [
                    Object {
                        hasGeometry: [boolean] indicating whether or not the given result set includes a geometry
                        name: name of the given layer used for display
                        fields: the fields JSON array returned from esri's REST for a given layer (for queries, this will only include the layers specified in the where; currently, identify includes everything)
                        data: the actual results (attributes key-value pairs)
                    }
                ]
            */
            constructor: function (url, resultsSet) {
                if (!url) {
                    throw new Error("no url defined for " + this.declaredClass);
                }
                if (!resultsSet) {
                    throw new Error("no results set defined for " + this.declaredClass);
                }
                this.url = url;
                this.resultsSet = resultsSet;
            }
    });
});