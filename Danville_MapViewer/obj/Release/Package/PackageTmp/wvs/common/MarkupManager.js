﻿define([
    "dojo/_base/lang"
    , "dojo/_base/declare"
    , "../_base/config"
    , "esri/request"
    , "./Common"
    , "dojo/Deferred"
], function (
    lang
    , declare
    , wvsConfig
    , esriRequest
    , Common
    , Deferred
) {

    var MarkupManager = declare([], {

        markupService: null,

        /*@function constructor
        *       Initialize the instance of MarkupManager with a markup service string.
        * @param markupService: String
        *       The url for the markup service to which user-created markup will be saved.
        */
        constructor: function (markupService) {
            this.markupService = markupService ? markupService : wvsConfig.defaults.services.markup ? wvsConfig.defaults.services.markup : null;

            if (!this.markupService) {
                throw new Error("Markup Manager cannot be created without a markup service");
            }
        },

        /*@function _getUniqueParam
        *       Generates a string of characters based on the current time just to make sure that saves and edits will always go through
        *       to server instead of pulling the cached result from the last call to the markup service.
        */
        _getUniqueParam: function () {
            var d = (new Date()).getTime();
            return d + "=" + d;
        },

        /*@function getMarkup
        *       This function will retrieve markup from the markup service.
        * @return dfd: Deferred
        *       The deferred object which will resolve when the ajax request is returned from server and
        *       will return the markup collection as a json string.
        */
        getMarkup: function () {
            var deferred = new Deferred();
            var dfd = esriRequest({
                url: this.markupService + "/Get?" + this._getUniqueParam()
            });

            dfd.then(
                function (response) {
                    if (response.status === "SUCCESS") {
                        deferred.resolve(response.data);
                    }
                    else {
                        deferred.reject(response.message);
                    }
                },

                function (error) {
                    deferred.reject(error.message);
                }
            );

            return deferred; // Deferred
        },

        /*@function getMarkupCollection
        *       Returns a collection of markup based on the id for that markup collection
        *       as stored on server.
        * @param id: String
        *       The id for the markup collection.
        * @return deferred: Deferred
        *       The deferred object which, when the XHR to server is resolved, will return
        *       the markup collection as a json string.
        */
        getMarkupCollection: function (id) {
            var deferred = new Deferred();
            var dfd = esriRequest({
                url: this.markupService + "/GetCollection?id=" + id
            });

            dfd.then(
                function (response) {
                    if (response.status === "SUCCESS") {
                        deferred.resolve(response.data);
                    }
                    else {
                        deferred.reject(response.message);
                    }
                },

                function (error) {
                    deferred.reject(error.message);
                }
            );

            return deferred; // Deferred
        },

        /*@function saveMarkupCollection
        *       This function will accept a markupLayer object and save it to server, either
        *       updating an existing markup collection or adding a new one.
        * @param markupLayer: MarkupLayer
        *       The markup layer to be saved to server.
        * @return Deferred
        *       The deferred which will handle the server result when it comes back indicating
        *       either success or failure of the save attempt.
        */
        saveMarkupCollection: function (markupLayer) {
            console.log("markupManager.saveMarkupCollection");
            // summary:
            //      a facade method that chooses whether or not to add or edit a markup collection
            if (typeof markupLayer._id === "number") {
                return this._editMarkupCollection(markupLayer); // Deferred
            }
            else {
                return this._addMarkupCollection(markupLayer); // Deferred
            }
        },

        /*@function _addMarkupCollection
        *       This function will save a markup collection to server, returning a deferred which, when resolved
        *       will indicate the success or failure of the save function, as well as an id to the collection.
        * @param markupLayer: MarkupLayer
        *       The MarkupLayer containing the collection to be saved.
        * @return deferred: Deferred
        *       The deferred result of the ajax call to the server which, when resolved, will indicate success or failure.
        */
        _addMarkupCollection: function (markupLayer) {
            console.log("markupManager._addMarkupCollection");
            var deferred = new Deferred(),
                markupLayerJson = markupLayer.toJson();


            markupLayerJson.options = JSON.stringify(markupLayerJson.options);
            var dfd = esriRequest({
                url: this.markupService + "/Add?" + this._getUniqueParam(),
                content: markupLayerJson
            }, { usePost: true });

            dfd.then(
                function (response) {

                    if (response.status === "SUCCESS") {
                        console.log("markupManager._addMarkupCollection was a success");
                        markupLayer._id = response.data;
                        deferred.resolve(response.message);
                    }
                    else {
                        console.log("markupManager._addMarkupCollection FAILED.  response status was->" + response.status);
                        deferred.reject(response.message);
                    }
                },

                function (error) {
                    console.log("markupManager._addMarkupCollection FAILED");
                    deferred.reject(error.message);
                }
            );

            return deferred; // Deferred
        },

        /*@function storeSessionMarkup
        *       Stores the current state of user-created markup as 'session markup' to be retrieved when
        *       a user navigates to the application with the markup collection id as a query parameter to the url.
        *       For use with the MapLink widget to share out the current map state.
        * @param markupLayer: MarkupLayer*
        *       Pointer to the markup layer representing the current state of user markup to be saved to
        *       the server for subsequent retrieval. Because it's passed by reference, we can directly manipulate
        *       the object within the function without having to return it back to the caller.
        * @return deferred: Deferred()
        *       The deferred result of the ajax call to server indicating success or failure of the save attempt.
        */
        storeSessionMarkup: function (markupLayer) {
            var deferred = new Deferred(),
                markupLayerJson = markupLayer.toJson();


            markupLayerJson.options = JSON.stringify(markupLayerJson.options);
            var dfd = esriRequest({
                url: this.markupService + "/AddSessionCollection?" + this._getUniqueParam(),
                content: markupLayerJson
            }, { usePost: true });


            dfd.then(
                function (response) {

                    if (response.status === "SUCCESS") {
                        console.log("markupManager._addMarkupCollection was a success");
                        markupLayer._id = response.data;
                        deferred.resolve(response.message);
                    }
                    else {
                        console.log("markupManager._addMarkupCollection FAILED.  response status was->" + response.status);
                        deferred.reject(response.message);
                    }
                },

                function (error) {
                    console.log("markupManager._addMarkupCollection FAILED");
                    deferred.reject(error.message);
                }
            );

            return deferred; // Deferred
        },

        /*@function deleteMarkupCollection
        *       Deletes the collection from the server. This is permanent.
        * @param id: String
        *       The id of the markup collection to be deleted from the server
        * @return deferred: Deferred
        *       The deferred result of the attempted ajax call to server requesting the
        *       deletion of the collection. Resolution will indicate success or failure.
        */
        deleteMarkupCollection: function (id) {
            var deferred = new Deferred(),
                dfd = esriRequest({
                    url: this.markupService + "/Delete?id=" + id + "&" + this._getUniqueParam()
                });


            dfd.then(
                function (response) {
                    if (response.status === "SUCCESS") {
                        deferred.resolve(response);
                    }
                    else {
                        deferred.reject(response);
                    }
                },

                function (error) {
                    deferred.reject(error);
                }
            );

            return deferred; // Deferred
        },

        /*@function _editMarkupCollection
        *       This will edit an existing markup collection, updating any features that have been
        *       changed and leaving the rest unaltered. It will not update or change the id of the collection.
        * @param markupLayer: MarkupLayer*
        *       Reference to the MarkupLayer object that represents the altered markup collection.
        * @return deferred: Deferred
        *       The deferred result of the attempt to update the collection.
        */
        _editMarkupCollection: function (markupLayer) {
            var deferred = new Deferred(),
                markupLayerJson = markupLayer.toJson();

            markupLayerJson.options = JSON.stringify(markupLayerJson.options);

            var dfd = esriRequest({
                url: this.markupService + "/Edit?" + this._getUniqueParam(),
                content: markupLayerJson
            }, { usePost: true });


            dfd.then(
                function (response) {
                    if (response.status === "SUCCESS") {
                        deferred.resolve(response.message);
                    }
                    else {
                        deferred.reject(response.message);
                    }
                },

                function (error) {
                    deferred.reject(error.message);
                }
            );

            return deferred; // Deferred
        }


    });

    return MarkupManager;

});