﻿define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/_base/array",
    "esri/InfoTemplate",
    "esri/dijit/PopupTemplate",
    "dojo/dom-construct",
    "dojo/dom-style",
    "dojo/query",
    "dojo/request",
    "../dijits/Dimensions",
    "dojo/on",
    "esri/graphic",
    "esri/geometry/Polygon",
    "esri/geometry/Polyline",
    "esri/geometry/Point",
    "esri/symbols/jsonUtils",
    "../dijits/GraphicEditor",
    "dojo/dom",
    "dojo/string",
    "../dijits/WhatNearby",
    "../dijits/ViewShed",
    "esri/lang",
    "../_base/config",
    "dijit/form/Select",
    "dojo/store/Memory",
    "esri/dijit/PopupRenderer",
    "./Common",
    "esri/tasks/RelationshipQuery",
    "require",
    "dojo/promise/all",
    "dgrid/Grid",
    "dgrid/extensions/Pagination",
    "./SearchResultsAdapter",
    "../dijits/ResultsConsole",
    "esri/layers/FeatureLayer",
    "../dijits/Surface"
], function (
    declare,
    lang,
    array,
    InfoTemplate,
    PopupTemplate,
    domConstruct,
    domStyle,
    query,
    request,
    Dimensions,
    on,
    Graphic,
    Polygon,
    Polyline,
    Point,
    jsonUtils,
    GraphicEditor,
    dom,
    string,
    WhatNearby,
    ViewShed,
    esriLang,
    wvsConfig,
    Select,
    Memory,
    PopupRenderer,
    Common,
    RelationshipQuery,
    require,
    all,
    Grid,
    Pagination,
    SearchResultsAdapter,
    ResultsConsole,
    FeatureLayer,
    Surface
) {

    var InfoTemplateManager = {
        map: null,
        templateDirectory: null,
        lastSelectedSearchResult: null,
        _cachedFeatureLayers: [],
        _activeWidgets: [],
        hiddenAttributesObject: {},
        toolConstructors: {
            Dimensions: {
                "constructor": Dimensions,
                name: function (geometryType) {
                    return geometryType.isInstanceOf(Point) ? "Coordinates" : "Dimensions"
                }
            },
            WhatNearby: {
                "constructor": WhatNearby,
                name: "What's Nearby"
            },
            ViewShed: {
                "constructor": ViewShed,
                name: function (geometryType) {
                    return geometryType.isInstanceOf(Point) ? "Viewshed" : "Line of Sight"
                }
            },
            Profile: {
                "constructor": Surface,
                name: "Profile"
            }
        },

        /*@prop actions
        *       The object whose properties define the actions that can be made available to the popup window
        *       if and when specific conditions are met by the graphic currently associated with the popup.
        */
        actions: {
            /*@function Edit
            *       Action which could be made available to the popup window based on whether or not the feature
            *       is editable.
            */
            Edit: {
                name: "Edit",
                iconClass: "",
                action: function (graphic, map) {
                    var domNode = dom.byId("wvsGraphicEditContainer");
                    domStyle.set(domNode, { display: "block" });
                    var _graphicEditor = new GraphicEditor({ map: map, graphic: graphic });
                    _graphicEditor.startup();
                    _graphicEditor.placeAt(dom.byId("wvsGraphicEditContainer"));
                }
            },
            /*@function SaveToMarkup
            *       Action which would automatically save the current graphic as a clone into the Temporary Markup layer
            */
            SaveToMarkup: {
                name: "Save to Markup",
                iconClass: "",
                /*@function SaveToMarkup
                *       The specific action by which a graphic is cloned and stored to the client-side Temporary Markup layer.
                * @param graphic: Graphic
                *       The graphic to be cloned and stored locally.
                * @param featureLayer: FeatureLayer
                *       The feature layer whose "loaded" property will tell us whether or not there is enough information about
                *       the graphic to begin creating the new markup item to save locally.
                */
                action: function (graphic, featureLayer) {
                    var createNewMarkup = function () {
                        var newItem = new Graphic(graphic.toJson()),
                            geometryType = "";

                        //Determine the general geometry type of the graphic to figure out which default symbol to use.
                        if (newItem.geometry.type) {
                            for (var prop in wvsConfig.defaults.symbols) {
                                if (newItem.geometry.type.toLowerCase().indexOf(prop) > -1) {
                                    geometryType = prop;
                                    break;
                                }
                            }
                        } else if (newItem.geometry.declaredClass) {
                            for (var prop in wvsConfig.defaults.symbols) {
                                if (newItem.geometry.declaredClass.toLowerCase().indexOf(prop) > -1) {
                                    geometryType = prop;
                                    break;
                                }
                            }
                        }
                        newItem.setSymbol(jsonUtils.fromJson(wvsConfig.defaults.symbols[geometryType]));

                        //Set the info template to match the info template of the existing feature perfectly.
                        newItem.setInfoTemplate(new InfoTemplate(graphic.getInfoTemplate()));

                        //Set the name to indicate the new markup is a clone of an existing feature.
                        newItem.infoTemplate.setTitle("Clone of " + graphic.getTitle());

                        newItem.setEditing(true);
                        newItem.save();
                        //Make sure the markupLayer exists, and if not, add the layer. Then add our new markup graphic.
                        if (wvsConfig.defaults.mapViewerController.mapViewer.map.getLayer(wvsConfig.defaults.markupLayer.id) == null) {
                            wvsConfig.defaults.mapViewerController.mapViewer.map.addLayer(wvsConfig.defaults.markupLayer);
                        }
                        wvsConfig.defaults.markupLayer.addGraphic(newItem);
                    };

                    //We cannot access the renderer information from the featureLayer unless it is loaded. Check that it is,
                    //otherwise, create the new markup after the "onLoad" event completes.
                    if (featureLayer.loaded) {
                        createNewMarkup();
                    } else {
                        on(featureLayer, "load", createNewMarkup);
                    }
                }
            }
        },

        /*@function _getDefaultTitle
        *       This function is used to generate a title for the Summary tab of the info window when one isn't
        *       explicitly passed in to the function call. Typically this will be the layer name.
        * @param graphic: Graphic
        *       The graphic for which a title will be generated.
        */
        _getDefaultTitle: function (graphic) {
            if (graphic.attributes.title) {
                return graphic.attributes.title;
            }
            if (graphic._type) {
                return graphic._type.charAt(0).toUpperCase() + graphic._type.substr(1).toLowerCase();
            }
            if (graphic._graphicsLayer) {
                return graphic._graphicsLayer.getServiceName();
            }

            return "Unnamed Graphic";
        },

        /*@function _getWidgetsFromGeometry
        *       This function will determine which tabs and widgets will be available to the popup window
        *       based on the geometry type for the graphic.
        * @param graphic: Graphic
        *       The graphic object whose geometry will determine which tabs and widgets will be made available.
        * @return widgets: Array
        *       The array of widget objects available to the current instance of the InfoTemplateManager for the graphic.
        */
        _getWidgetsFromGeometry: function (graphic) {
            var widgets = [],
                geometry = graphic.geometry;

            if (geometry.isInstanceOf(Polygon)) {
                widgets = ["Dimensions"];
                if (wvsConfig.defaults.whatNearbyEnabled) {
                    widgets.push("WhatNearby");
                }
                if (wvsConfig.defaults.profile.enabled) {
                    widgets.push("Profile");
                }
            } else if (geometry.isInstanceOf(Polyline)) {
                widgets = ["Dimensions"];
                if (wvsConfig.defaults.whatNearbyEnabled) {
                    widgets.push("WhatNearby");
                }
                if (wvsConfig.defaults.services.viewShed) {
                    widgets.push("ViewShed");
                }
                if (wvsConfig.defaults.profile.enabled) {
                    widgets.push("Profile");
                }
            } else if (geometry.isInstanceOf(Point)) {
                widgets = ["Dimensions"];
                if (wvsConfig.defaults.whatNearbyEnabled) {
                    widgets.push("WhatNearby");
                }
                if (wvsConfig.defaults.services.viewShed) {
                    widgets.push("ViewShed");
                }
                if (wvsConfig.defaults.profile.enabled) {
                    widgets.push("Profile");
                }
            }
            //else if (geometry.isInstanceOf(Circle)) {
            //    widgets = ["Dimensions", "WhatNearby"];
            //}
            return widgets;
        },

        /*@function _getActionsFromGraphic
        *       Gets a list of actions that will be available to the popup window as determined by the specific properties of the graphic.
        * @param graphic: Graphic
        *       The graphic whose properties will determine which actions are available.
        * @return actions: Array
        *       The array list of action names made available to the popup window based on properties of the graphic.
        */
        _getActionsFromGraphic: function (graphic) {
            var actions = [];
            for (var prop in InfoTemplateManager.actions) {
                if (!(prop === "Edit" && !graphic.isEditable()) && !(prop === "SaveToMarkup" && graphic._markupLayer)) {
                    actions.push(prop);
                }
            }
            return actions;
        },

        /*@function setMap
        *       Mutator function to set the map to which the InfoTemplateManager will be associated, and define some necessary maintenance actions on map behavior.
        * @param map: Map
        *       The map to associate with the called instance of the InfoTemplateManager.
        */
        setMap: function (map) {
            var self = this;
            this.map = map;
            on(this.map.infoWindow, "hide", function () {
                array.forEach(self._activeWidgets, function (activeWidget) {
                    activeWidget.destroy();
                });
                self._activeWidgets = [];
            });
        },

        /*@function setTemplateDirectory
        *       Accessor function to set the directory for templates that may or may not be available for a given feature layer.
        * @param dir: String
        *       The directory to be used for sending ajax requests built based on string names. Can be relative or absolute.
        */
        setTemplateDirectory: function (dir) {
            this.templateDirectory = dir;
        },

        /*@function setGlobalAttributeRestrictions
        *       Defines an array of attributes which should not be displayed in the popup window for any layer in the ToC
        * @param restrictionList: Array
        *       The array of strings which, if present anyware in an attribute name, should prevent that attribute
        *       from being displayed in any popup window for any layer.
        */
        setGlobalAttributeRestrictions: function (restrictionsList) {
            if (!this.hiddenAttributesObject.globalRestrictions) {
                this.hiddenAttributesObject.globalRestrictions = [];
            }
            this.hiddenAttributesObject.globalRestrictions = this.hiddenAttributesObject.globalRestrictions.concat(restrictionsList);
        },

        /*@function addAttributeDisplayRestrictions
        *       Defines display restrictions on particular attributes for particular layers. These display restrictions will only apply
        *       to the bubble windows of features from that layer.
        * @param layerObj: Object | Array
        *       Depending on the structure of the object, it could either be an object defining layer IDs and the attributes to exclude
        *       for those particular layers, or a list of attributes in an Array which should not be displayed for the particular layer.
        */
        addAttributeDisplayRestrictions: function (layerObj) {
            var lyrRootUrl = layerObj.url;
            if (layerObj.type === "ArcGISDynamicMapServiceLayer") {

                //Add a trailing slash symbol to the url for checking against urls defined in the hiddenAttibute object.
                if (layerObj.url.charAt(layerObj.url.length - 1) != '/') {
                    lyrRootUrl += '/';
                }

                for (var i = 0; i < layerObj.hiddenAttributeSettings.length; i++) {
                    var lyrUrl = lyrRootUrl + layerObj.hiddenAttributeSettings[i].layerId;
                    if (!this.hiddenAttributesObject[lyrUrl]) {
                        this.hiddenAttributesObject[lyrUrl] = [];
                    }
                    this.hiddenAttributesObject[lyrUrl] = this.hiddenAttributesObject[lyrUrl].concat(layerObj.hiddenAttributeSettings[i].hiddenFields);
                }
            } else if (layerObj.type === "FeatureLayer") {
                if (layerObj.hiddenAttributeSettings && layerObj.hiddenAttributeSettings.hiddenFields) {
                    for (var i = 0; i < layerObj.hiddenAttributeSettings.hiddenFields.length; i++) {
                        if (!this.hiddenAttributesObject[lyrRootUrl]) {
                            this.hiddenAttributesObject[lyrRootUrl] = [];
                        }
                        this.hiddenAttributesObject[lyrRootUrl] = this.hiddenAttributesObject[lyrRootUrl].concat(layerObj.hiddenAttributeSettings.hiddenFields);
                    }
                }
            }
        },

        /*@function _getSummary
        *       Function to retrieve a div containing summary or default information for a particular layer. Will return either the html defined in a template for
        *       the layer or a generic table of name/value pairs listing each attribute of the feature, and excluding any display-restricted attributes.
        * @param graphic: Graphic
        *       The specific graphic for which the popup is being constructed, containing all of the attribute information to display.
        * @param title: String
        *       The title to display at the top of the popup window. If not present, the title will simply be the name of the layer to which the feature belongs.
        * @param featureLayer: FeatureLayer
        *       The FeatureLayer object from which the graphic was retrieved.
        * @return containingDiv: DOM Node
        *       THe DOM node containing all of the display markup to place into the popup window whenever the Summary tab is activate.
        */
        _getSummary: function (graphic, title, featureLayer) {
            var dir = this.templateDirectory,
                self = this,
                tempStr = null;

            //Define the dom nodes to be passed into the asynch functions and populated when the asynch functions resolve.
            var title = title && typeof title === "string" ? title : self._getDefaultTitle(graphic),
                attributes = graphic.attributes,
                containingDiv = domConstruct.create("div", { "class": "graphicAttributeSummary" }),
                titleHeader = domConstruct.create("h3", { innerHTML: title }, containingDiv);

            //Function to build the default summary template onto the container div in the event of no template being returned for the layer.
            var createDefaultSummaryPopup = function (containingDiv) {
                var table = domConstruct.create("table", null, containingDiv);

                for (var name in attributes) {
                    //First determine if the element has any attributes that should not be displayed as defined in either the global attribute restrictions
                    //or on a layer by layer basis.
                    var skipAttr = false;
                    if (self.hiddenAttributesObject) {
                        if (self.hiddenAttributesObject.globalRestrictions && self.hiddenAttributesObject.globalRestrictions.length) {
                            for (var i = 0; i < self.hiddenAttributesObject.globalRestrictions.length; i++) {
                                if (self.hiddenAttributesObject.globalRestrictions[i] && name.toLowerCase().indexOf(self.hiddenAttributesObject.globalRestrictions[i].toLowerCase()) >= 0) {
                                    console.log("Skipping attribute: " + name);
                                    skipAttr = true;
                                }
                            }
                        }
                        if (!skipAttr && featureLayer && self.hiddenAttributesObject[featureLayer.url] && self.hiddenAttributesObject[featureLayer.url].length) {
                            for (var i = 0; i < self.hiddenAttributesObject[featureLayer.url].length; i++) {
                                if (self.hiddenAttributesObject[featureLayer.url][i] && self.hiddenAttributesObject[featureLayer.url][i].toLowerCase() == name.toLowerCase()) {
                                    console.log("Skipping attribute: " + name);
                                    skipAttr = true;
                                }
                            }
                        }
                        if (skipAttr) {
                            continue;
                        }
                    }

                    //Build table row for the attribute name/value pair table.
                    var tr = domConstruct.create("tr", null, table);
                    var attrNameTD = domConstruct.create("td", { innerHTML: name }, tr);
                    var attrValueTD = domConstruct.create("td", {}, tr);
                    var link = Common.getHyperlink(name, attributes[name]);
                    if (!link || typeof link !== "object") {
                        attrValueTD.innerHTML = link;
                    } else {
                        domConstruct.place(link, attrValueTD);
                    }
                }
            }

            //Function to retrieve any templates for the bubble window, to be executed iff the featureLayer is defined and loaded.
            var attemptTemplateRequest = function (fl) {
                if (dir != null && dir != undefined) {
                    request(dir + fl.name + '.html').then(
                        function (result) {
                            tempStr = string.substitute(result.toString(), graphic.attributes);
                            domConstruct.place(tempStr, containingDiv, 'only');
                        },
                        function (err) {
                            createDefaultSummaryPopup(containingDiv);
                        });
                } else {
                    createDefaultSummaryPopup(containingDiv);
                }
            }

            if (featureLayer) {
                var processFL = lang.hitch(self, function () {
                    //Wait to request the template until the feature layer is loaded and we can access the name property of the feature layer.
                    if (featureLayer.hasOwnProperty('name') && dir) {
                        attemptTemplateRequest(featureLayer);
                    }
                    else {
                        createDefaultSummaryPopup(containingDiv);
                    }

                    if (featureLayer.relationships.length) {
                        domConstruct.place(self._getRelationships(featureLayer, graphic), containingDiv);
                    }
                    // Support getting attachments in bubble
                    if (featureLayer.hasAttachments) {
                        domConstruct.place(self._getAttachments(featureLayer, graphic), containingDiv);
                    }
                });
                if (featureLayer.loaded) {
                    processFL();
                } else {
                    featureLayer.on("load", function () { processFL(); });
                }
            }
            else {
                createDefaultSummaryPopup(containingDiv);
            }
            return containingDiv;
        },

        /*@function _loadFeatureLayer
        *       loads or retrieves a cached featureLayer for more information (such as loading attachments/relationships)
        * @param featureLayer: FeatureLayer | String
        *       a feature layer represented as an actual instance or a URL endpoint
        * @return fl: FeatureLayer
        *       The loaded FeatureLayer instance.
        */
        _loadFeatureLayer: function (featureLayer) {
            var url = typeof featureLayer === "string" ? featureLayer : featureLayer.url,
                fl = null;
            for (var i = 0; i < this._cachedFeatureLayers.length; i++) {
                if (this._cachedFeatureLayers[i].url === url) {
                    fl = this._cachedFeatureLayers[i];
                }
            }

            if (!fl) {
                fl = new FeatureLayer(url);
                this._cachedFeatureLayers.push(fl);
            }
            return fl; // FeatureLayer
        },

        /*@function _getAttachments
        *       Will fetch any attachments associated with the featureLayer and display them with the popup window.
        * @param featureLayer: FeatureLayer
        *       The FeatureLayer object which defines what attachments are associated with the graphic based on the object id of the graphic.
        * @param graphic: Graphic
        *       The graphic whose attachments need to be retrieved from the feature layer.
        * @return attachments: DOM Node
        *       The dom node which displays information about the retrieved attachments.
        */
        _getAttachments: function (featureLayer, graphic) {
            var attachments = domConstruct.create("div", { innerHTML: '<img src="' + require.toUrl("../dijits/images/loading_small.gif") + '" /> <strong>Loading Attachments</strong>' });

            //Callback function on successful load of attachment information.
            var generateAttachmentNode = function (info) {
                attachments.innerHTML = "<strong><u>Attachments</u></strong><br />";
                if (info.length) {
                    array.forEach(info, function (i) {
                        domConstruct.create("a", { href: i.url, innerHTML: i.name + "<br />", target: "_blank" }, attachments);
                    });
                } else {
                    domConstruct.create("span", { innerHTML: "<em>None</em>" }, attachments);
                }
            };

            //Errback handler for issues with loading attachment info.
            var generateErrorNode = function (err) {
                // TODO: Handle
                attachments.innerHTML = "<strong><u>Attachments</u></strong><br /><em>Error retrieving attachments</em>";
            };
            featureLayer.queryAttachmentInfos(graphic.attributes[featureLayer.objectIdField], generateAttachmentNode, generateErrorNode);

            return attachments;
        },

        /*@function _getRelationships
        *       Will get and return all related data for a specific object when it is to be displayed.
        * @param featureLayer: FeatureLayer
        *       The feature layer from which the related data will be loaded.
        * @param graphic: Graphic
        *       The graphic whose related data is being pulled from the feature layer and added to the popup.
        * @return relationships: DOM Node
        *       The dom node containing all of the appropriate display information on the related data.
        */
        _getRelationships: function (featureLayer, graphic) {
            var relationships = domConstruct.create("div", { innerHTML: '<img src="' + require.toUrl("../dijits/images/loading_small.gif") + '" /> <strong>Loading Related Data</strong>' });

            //Callback function which populates the relationships dom node with the related data.
            var callback = function (relationship, relatedFeatures) {
                var count = 0;
                var cols = {};
                var data = [];
                for (var key in relatedFeatures) {
                    var featureSet = relatedFeatures[key];
                    for (var i = 0, il = featureSet.features.length; i < il; i++) {
                        var feature = featureSet.features[i];
                        if (i === 0) {
                            for (var fieldKey in feature.attributes) {
                                cols[fieldKey] = { label: fieldKey };
                            }
                        }
                        data.push(feature.attributes);
                    }
                    count++;
                }
                if (count) {
                    var curRelationship = domConstruct.create("span", { "class": "popup-action", innerHTML: "<strong>" + relationship.name + "</strong><br/>" }, relationships);
                    on(curRelationship, "click", function () {
                        var adapter = new SearchResultsAdapter();
                        var url = featureLayer.url.replace(/\/[0-9]+$/g, "") + "/" + relationship.relatedTableId;
                        adapter.featureSetToResultSet(featureSet, url).then(function (searchResult) {
                            var rc = wvsConfig.defaults.mapViewerController.getWidget(ResultsConsole, "bottom", { bindToStore: false });
                            wvsConfig.defaults.mapViewerController.openBottomRegion();
                            rc.clear();
                            rc.add(searchResult);
                        });
                    });

                }
            };

            //Errback function which handles errors loading related data.
            var errback = function (relationship, err) {
                var curRelationship = domConstruct.create("div", { innerHTML: "<strong>" + relationship.name + "</strong><br/>" }, relationships);
                domConstruct.create("span", { innerHTML: "<em>" + err.message + "</em>" }, curRelationship);
            };

            var dfds = [];

            array.forEach(featureLayer.relationships, function (r) {
                var rQuery = new RelationshipQuery();
                rQuery.outFields = ["*"];
                rQuery.relationshipId = r.id;
                rQuery.returnGeometry = true;
                rQuery.objectIds = [graphic.attributes[featureLayer.objectIdField]];
                var dfd = featureLayer.queryRelatedFeatures(rQuery);
                dfd.then(lang.partial(callback, r), lang.partial(errback, r));
            });

            all(dfds).then(function (results) {
                relationships.innerHTML = "<strong><u>Related Data</u></strong><br />";
            });
            return relationships;
        },

        /*@function _getMiniAccordion
        *       This function will return a mini-accordion dropdown based on a search.
        * @param store: MemoryStore
        *       The objservable store that will hold the data and be inspected for updates by the select.
        * @return select: Select
        *       The select dropdown that will be inspecting a memory store for results as they are acquired.
        */
        _getMiniAccordion: function (store) {
            store = store || wvsConfig.defaults.searchResultStore;
            if (!store) {
                throw new Error("no search results store for wvs.common.InfoTemplateManager");
            }

            var memoryStore = new Memory({ data: [] });
            var self = this;

            var select = new Select({
                options: [{ label: "---Select Result---", value: "" }],
                onChange: function (value) {
                    array.forEach(select.options, function (option) {
                        if (value && value === option.value && value != self.lastSelectedSearchResult) {
                            self.lastSelectedSearchResult = value;
                            var features = array.map(option._resultSet.data, function (data) { return data; });
                            self.map.infoWindow.setFeatures(features);
                        }
                    });
                }
            });
            // get all results
            var resultSets = store.query();

            var removeSearchResult = function (searchResult) {
                var options = select.options.slice();
                array.forEach(searchResult.searchResult.resultsSet, function (resultSet) {
                    array.forEach(options, function (option, i) {
                        if (option && option._resultSet === resultSet) {
                            options.splice(i, 1);
                        }
                    });
                });
                select.set("options", options);
            };

            var addSearchResult = function (searchResult) {
                if (searchResult.searchResult.resultsSet.length > 0) {
                    array.forEach(searchResult.searchResult.resultsSet, function (resultSet) {
                        select.set("options", select.options.concat({ label: resultSet.name, value: resultSet.name, _resultSet: resultSet }));
                    });
                }
            };

            array.forEach(resultSets, function (result) {
                addSearchResult(result);
            });

            if (this.lastSelectedSearchResult) {
                select.set("value", this.lastSelectedSearchResult);
            }

            // observe the result set from our store
            var observeHandler = function (object, removedFrom, insertedInto) {
                if (removedFrom > -1) {
                    var options = select.options.slice();
                    removeSearchResult(object);
                    select.set("options", options);
                }
                if (insertedInto > -1) {
                    if (object.searchResult.resultsSet.length > 0) {
                        addSearchResult(object);
                    }
                }
            };
            resultSets.observe(observeHandler);
            this.storeSelect = select;

            return select.domNode;
        },

        /*@function getTabularAttributeTemplate
        *       Generates a table of objects whose values are displayed row by row.
        * @param options: Object
        *       Object hash containing expected/optional properties.
        *   @prop title: String
        *       The title to be displayed at the top of the table.
        *   @prop widgets: Array
        *       The array of widgets names to include in the bottom of the info window.
        *   @prop createWidgetsFromGeometry: bool
        *       Flag to indicate whether or not to create widgets based on the geometry type for the active item.
        */
        getTabularAttributeTemplate: function (options) {
            var self = this,
                options = options || {},
                title = typeof options.title === "string" && options.title ? options.title : this._getDefaultTitle,
                popupInfo = options.popupInfo,
                usePopupInfo = typeof options.usePopupInfo === "boolean" ? options.usePopupInfo : true,
                widgets = options.widgets || [],
                actions = [],
                featureLayer = typeof options.featureLayer === "object" || typeof options.featureLayer === "string" ? options.featureLayer : null,
                createWidgetsFromGeometry = typeof options.createWidgetsFromGeometry === "boolean" ? options.createWidgetsFromGeometry : true,
                useGlobalSearchStore = options.useGlobalSearchStore ? true : false,
                searchResultsStore = options.searchResultsStore || wvsConfig.defaults.searchResultStore,
                separatorText = "&nbsp;|&nbsp;";

            this._activeWidgets = [];

            if (featureLayer) {
                featureLayer = this._loadFeatureLayer(featureLayer);
            }

            var getTextContent = function (actions, widgets, graphic) {
                if (createWidgetsFromGeometry) {
                    actions = actions.concat(InfoTemplateManager._getActionsFromGraphic(graphic));
                    widgets = widgets.concat(InfoTemplateManager._getWidgetsFromGeometry(graphic));
                }
                var container = domConstruct.create("div"),
                    mainContent = domConstruct.create("div", {}, container),
                    actionBar = query('.actionList')[0],
                    actionDivs = [];
                var createWidgetLink = function (name, domNode) {
                    // separator
                    if (actionBar.childNodes.length > 0) {
                        if (actionBar.childNodes.length == 1) {
                            separatorText = "|&nbsp;";
                        } else {
                            separatorText = "&nbsp;|&nbsp;";
                        }
                        domConstruct.create("span", { innerHTML: separatorText }, actionBar);
                    }
                    var actionLink = domConstruct.create("span", { innerHTML: name, "class": "popup-action" }, actionBar);
                    on(actionLink, "click", function () {
                        showPane(domNode);
                    });
                };

                var createActionLink = function (actionKey) {
                    //Necessary to distinguish so that the proper parameters are passed to the action handling method.
                    if (actionKey != "SaveToMarkup") {
                        // separator
                        if (actionBar.childNodes.length > 0) {
                            if (actionBar.childNodes.length == 1) {
                                separatorText = "|&nbsp;";
                            } else {
                                separatorText = "&nbsp;|&nbsp;";
                            }
                            domConstruct.create("span", { innerHTML: separatorText }, actionBar);
                        }

                        var curAction = InfoTemplateManager.actions[actionKey];

                        var actionLink = domConstruct.create("span", { innerHTML: curAction.name, "class": curAction.iconClass + " popup-action" }, actionBar);
                        on(actionLink, "click", lang.hitch(self, curAction.action, graphic, InfoTemplateManager.map));
                        //Make a check to see if the graphic is an instance of a markup layer object (cannot be saved to markup layer).
                    } else {
                        // separator
                        if (actionBar.childNodes.length > 0) {
                            if (actionBar.childNodes.length == 1) {
                                separatorText = "|&nbsp;";
                            } else {
                                separatorText = "&nbsp;|&nbsp;";
                            }
                            domConstruct.create("span", { innerHTML: separatorText }, actionBar);
                        }

                        var curAction = InfoTemplateManager.actions[actionKey];
                        var actionLink = domConstruct.create("span", { innerHTML: curAction.name, "class": curAction.iconClass + " popup-action" }, actionBar);
                        on(actionLink, "click", lang.hitch(self, curAction.action, graphic, featureLayer));
                    }
                };

                var showPane = function (domNode) {
                    array.forEach(actionDivs, function (actionDiv) {
                        domStyle.set(actionDiv, { display: actionDiv === domNode ? "block" : "none" });
                    });
                }
                for (var i = actionBar.childNodes.length - 1; i > 0; i--) {
                    domConstruct.destroy(actionBar.childNodes[i]);
                }

                // Default content
                // Create summary div and link
                var summaryDiv;
                if (popupInfo && usePopupInfo) {
                    var template = new PopupRenderer({ graphic: graphic, template: new PopupTemplate(popupInfo) });
                    template.startup();
                    summaryDiv = template.domNode;
                } else {
                    summaryDiv = self._getSummary(graphic, title, featureLayer);
                }

                createWidgetLink("Summary", summaryDiv);
                actionDivs.push(summaryDiv);
                domConstruct.place(summaryDiv, mainContent);

                // Create "actions" (basically does something with the graphic but not inside the bubble window)
                if (actions.length > 0) {
                    array.forEach(actions, function (actionName, i) {
                        createActionLink(actionName);
                    }, this);
                }

                if (widgets.length > 0) {
                    array.forEach(widgets, function (widgetKey, index) {
                        var instance = new InfoTemplateManager.toolConstructors[widgetKey].constructor({ map: self.map, graphic: graphic, _parent: InfoTemplateManager, style: "display:none;" });
                        var widgetName = typeof InfoTemplateManager.toolConstructors[widgetKey].name === "string" ? InfoTemplateManager.toolConstructors[widgetKey].name : InfoTemplateManager.toolConstructors[widgetKey].name(graphic.geometry);
                        instance.startup();
                        self._activeWidgets.push(instance);
                        domConstruct.place(instance.domNode, mainContent);
                        actionDivs.push(instance.domNode);

                        createWidgetLink(widgetName, instance.domNode);
                    }, this);
                }
                console.log(container);
                return container;
            };

            var template = new PopupTemplate();
            template.setTitle(title);
            template.setContent(lang.hitch(this, getTextContent, actions, widgets));

            return template;
        }
    };

    return InfoTemplateManager;
});