﻿console.log("wvs/common/esriUtilities.js");

define(["dojo/_base/declare", "dojo/_base/lang", "dojo/_base/array", "esri/request", "dojo/Deferred", "require" ],
    function (declare, lang, array, esriRequest, Deferred, require) {
        return declare("wvs.common.esriUtilities", null, {
            getLayerInfoFromUrl: function (url) {
                var layerInfoRequest = esriRequest({
                    url: url,
                    content: { f: "json" },
                    handleAs: "json",
                    callbackParamName: "callback"
                });

                return layerInfoRequest;
            },
            // This function lazily loads requested classes and provides an instance
            // layerJson includes the following properties
            // url - the endpoint for the specified layer (needed in most types)
            // type - determines which constructor to call (REQUIRED)
            // options - paramaters for the constructor of the given type (optional)
            layerFactory: function (layerJson) {
                var type = layerJson.type,
                    url = layerJson.url,
                    options = layerJson.options || {},
                    deferred = new Deferred();

                switch(type){
                    case "ArcGISDynamicMapServiceLayer":
                        require(["esri/layers/ArcGISDynamicMapServiceLayer"], function (ArcGISDynamicMapServiceLayer) {
                            instance = new ArcGISDynamicMapServiceLayer(url, options);
                            deferred.resolve(instance);
                        });
                        break;
                    case "ArcGISImageServiceLayer":
                        require(["esri/layers/ArcGISImageServiceLayer"], function (ArcGISImageServiceLayer) {
                            instance = new ArcGISImageServiceLayer(url, options);
                            deferred.resolve(instance);
                        });
                        break;
                    case "ArcGISTiledMapServiceLayer":
                        require(["esri/layers/ArcGISTiledMapServiceLayer"], function (ArcGISTiledMapServiceLayer) {
                            instance = new ArcGISTiledMapServiceLayer(url, options);
                            deferred.resolve(instance);
                        });
                        break;
                    case "FeatureLayer":
                        require(["esri/layers/FeatureLayer"], function (FeatureLayer) {
                            instance = new FeatureLayer(url, options);
                            deferred.resolve(instance);
                        });
                        break;
                    case "GeoRSS":
                        require(["esri/layers/GeoRSSLayer"], function (GeoRSSLayer) {
                            instance = new GeoRSSLayer(url, options);
                            deferred.resolve(instance);
                        });
                        break;
                    case "KML":
                        require(["esri/layers/KMLLayer"], function (KMLLayer) {
                            instance = new KMLLayer(url, options);
                            deferred.resolve(instance);
                        });
                        break;
                    case "Web Map":
                        require(["wvs/extensions/esri/layers/ArcGISWebMapLayer"], function (WebMapLayer) {
                            instance = new WebMapLayer(url, options);
                            deferred.resolve(instance);
                        });
                        break;
                }

                return deferred.promise;
            },
            getRelatedFeaturesFromEndpoint: function (url, relationshipQuery, callback, errback) {
                require(["esri/layers/FeatureLayer"], function (FeatureLayer) {
                    instance = new FeatureLayer(url);
                    instance.on("load", function () {
                        var dfd = instance.queryRelatedFeatures(relationshipQuery, callback, errback);
                    });
                    
                });
            }
        });
    });