﻿define([
"require"
, "dojo/dom-construct"
, "wvs/utilities/events/QuickIdentify"
, "wvs/utilities/events/ContextMenu"
, "dojo/text!./resources/header.html"
], function (
require
, domConstruct
, QuickIdentify
, ContextMenu
, header) {
    return {
        alwaysUseProxy: false,
        proxyUrl: require.toUrl("/proxy.ashx"),
        templateDirectory: require.toUrl("/Templates/"),
        // Defaults overrides any declared properties from wvs/_base/config.js
        defaults: {
            services: {
                print: "http://138.0.10.140/arcgis/rest/services/Utilities/PrintingTools/GPServer/Export%20Web%20Map%20Task",
                exportService: require.toUrl("/Export"),
                geometry: "http://vm101.worldviewsolutions.net:6080/arcgis/rest/services/Utilities/Geometry/GeometryServer",
                markupService: require.toUrl("/Markup"),
                markup: require.toUrl("/Markup"),
                viewShed: null
            }
        },
        globalOptions: {
            globalAttributeRestrictions: [],
            basemaps: [
                 {
                     options: {
                         title: "Streets"
                     },
                     layers: [
                         {
                             url: "http://138.0.10.140/arcgis/rest/services/Caches/Roads/MapServer"
                         }
                     ]
                 },
                 {
                     options: {
                         title: "Hillshade 2009"
                     },
                     layers: [
                         {
                             url: "http://138.0.10.140/arcgis/rest/services/Caches/HillShade2009/MapServer"
                         }
                     ]
                 },
                 {
                     options: {
                         title: "Aerials 2012"
                     },
                     layers: [
                         {
                             url: "http://138.0.10.140/arcgis/rest/services/Caches/Aerials2012/MapServer"
                         }
                     ]
                 },
                 {
                     options: {
                         title: "Aerials 2011"
                     },
                     layers: [
                         {
                             url: "http://138.0.10.140/arcgis/rest/services/Caches/Aerials2011/MapServer"
                         }
                     ]
                 },
                 {
                     options: {
                         title: "Aerials 2009"
                     },
                     layers: [
                         {
                             url: "http://138.0.10.140/arcgis/rest/services/Caches/Aerials2009/MapServer"
                         }
                     ]
                 },
                 {
                     options: {
                         title: "Aerials 2007"
                     },
                     layers: [
                         {
                             url: "http://138.0.10.140/arcgis/rest/services/Caches/Aerials2007/MapServer"
                         }
                     ]
                 },
                 {
                     options: {
                         title: "Aerials 2005"
                     },
                     layers: [
                         {
                             url: "http://138.0.10.140/arcgis/rest/services/Caches/Aerials2005/MapServer"
                         }
                     ]
                 },
                 {
                     options: {
                         title: "Aerials 2002"
                     },
                     layers: [
                         {
                             url: "http://138.0.10.140/arcgis/rest/services/Caches/Aerials2002/MapServer"
                         }
                     ]
                 },
                 {
                     options: {
                         title: "Aerials 1998"
                     },
                     layers: [
                         {
                             url: "http://138.0.10.140/arcgis/rest/services/Caches/Aerials1998/MapServer"
                         }
                     ]
                 },
                 {
                     options: {
                         title: "Aerials 1991"
                     },
                     layers: [
                         {
                             url: "http://138.0.10.140/arcgis/rest/services/Caches/Aerials1991/MapServer"
                         }
                     ]
                 },
                 {
                     options: {
                         title: "Aerials 1981"
                     },
                     layers: [
                         {
                             url: "http://138.0.10.140/arcgis/rest/services/Caches/Aerials1981/MapServer"
                         }
                     ]
                 },
                 {
                     options: {
                         title: "Aerials 1962"
                     },
                     layers: [
                         {
                             url: "http://138.0.10.140/arcgis/rest/services/Caches/Aerials1962/MapServer"
                         }
                     ]
                 },
                 {
                     options: {
                         title: "Aerials 1948"
                     },
                     layers: [
                         {
                             url: "http://138.0.10.140/arcgis/rest/services/Caches/Aerials1948/MapServer"
                         }
                     ]
                 }
            ],
            layers: [
                {
                    url: "http://138.0.10.140/arcgis/rest/services/WVTest/Main_WVTest/MapServer", // <required>
                    type: "ArcGISDynamicMapServiceLayer", // <required>
                    // optional
                    options: {
                        id: "danville_main",
                        _hiddenLayers: [
                            "buildings",
                            "flood"
                        ],
                    },
                    hiddenFields: [
                       "OBJECTID",
                       "OBJECT_ID",
                       "OID"
                    ]
                }
            ],
            proxyReplacements: [
                //{ oldText: "http://sampleserver1.arcgisonline.com", newText: "http://localhost" }
            ],
            proxyRules: [
                // Use custom proxy (other than the one packaged with M4
                //  { proxyUrl: "http://someproxy", urlPrefix: "http://someprefix.com/prefix" }
                { proxyUrl: "/proxy.ashx", urlPrefix: "file://" }
                // or use the default proxy and just provide a url prefix
                // { urlPrefix: "" }
            ],
            layout: {
                embedded: false,
                header: {
                    visible: true,
                    style: 'background-color: #2f5993; repeat-x; width: 100%; height: 42px; color: #000; padding: 0px; margin: 0px; border-bottom: 1px solid #1676b1;',//
                    // define dom nodes to be added, in order
                    content: [
                        domConstruct.create("img", { src: require.toUrl('../app/resources/danville_logo.gif'), style: { "padding-top": "4px", background: require.toUrl("../app/resources/images/ui-bg_highlight-hard_20_000066_1x100.png") + ' repeat-x;' } })
                    ]
                },
                minimized: {
                    headerRegion: true,
                    leftRegion: true,
                    bottomPane: true,
                    mapToolbar: true
                },
                maximized: {
                    headerRegion: false,
                    leftRegion: false,
                    bottomPane: false,
                    mapToolbar: false
                }
            },
            // Instead of having the mapviewer having too much knowledge of map events. We pass in the map event that we want by loading it via AMD.
            mapEvents: [
                {
                    ctr: QuickIdentify,
                    options: { tolerance: 5, showSingleResult: false }
                },
                {
                    ctr: ContextMenu,
                    options: { menuItems: [ContextMenu.MenuItems.PushPin] }
                }
            ],

            autoSave: {
                // if we have a save endpoint available (currently, only local storage if browser supports it,) then auto-save
                enabled: false,
                // auto-save when one of these topics is published; an empty array is equivalent to disabling auto-save
                // "map/extent/change", "map/basemap/change", "map/layer/add", "map/layer/remove"
                topics: ["map/basemap/change", "markup/item/modify", "markup/item/add", "markup/item/remove", "markup/item/move", "markup/item/copy"]
            },
            showLoadingOverlay: false,
            loadingOverlayImage: null,
            topMenu: {
                Tools: { // without label, the key of the object is used
                    type: "submenu",
                    items: [
                        {
                            type: "item",
                            label: "Search",
                            iconClass: "",
                            moduleName: "attributeSearch",
                            options: {

                            }
                        }
                    ]
                },
                mapManager: {
                    type: "item",
                    label: "Map Manager",
                    iconClass: "",
                    moduleName: "mapManager", // moduleName tells it to look up the module by name
                    target: "", // options: dialog, toolContainer; defaults to dialog
                    options: {

                    }
                }
            }
        },
        map: {
            widgetOptions: {
                extent: {
                    "xmin": -8853971.125099845, "ymin": 4373999.607502269, "xmax": -8823625.624870673, "ymax": 4389936.727899706, "spatialReference": { "wkid": 102100 }
                }, // Required
                sliderStyle: 'small',
                basemap: "gray",
                displayGraphicsOnPan: true
            }
        },
        identify: {
            visible: true
        },
        toc: {
            visible: true
        },
        mapManager: {
            visible: true,
            widgetOptions: {
                enableArcGISOnline: true,
                mapServicesTitle: "Map Services",
                items: [],
                ////List of services that will always be available to users.
                requiredItems: [
				   { "type": "esriMapService", "url": "http://138.0.10.140/arcgis/rest/services/WVTest/Public_Safety_WVTest/MapServer", "name": "Danville Public Safety", "source": "defaults" },
				   { "type": "esriMapService", "url": "http://138.0.10.140/arcgis/rest/services/WVTest/Infrastructure_WVTest/MapServer", "name": "Danville Infrastructure", "_hiddenLayers": ["boundary"], "source": "defaults" },
				   { "type": "esriMapService", "url": "http://138.0.10.140/arcgis/rest/services/WVTest/Comm_Dev_WVTest/MapServer", "name": "Danville Comm Dev", "source": "defaults" },
                   { "type": "arcGisOnlineWebMap", "url": "https://vdemgis.maps.arcgis.com/home/webmap/viewer.html?webmap=bf0e747df23b45c3862c4d0ab0bad7a1", "name": "VIPER", "source": "defaults" },
                   { "type": "esriImageService", "url": "http://gis.apfo.usda.gov/ArcGIS/rest/services/NAIP/Virginia_2012_1m_NC/ImageServer", "name": "Virginia NAIP Imagery 2012", "source": "defaults" },
                   { "type": "esriMapService", "url": "http://gis.srh.noaa.gov/arcgis/rest/services/RIDGERadar/MapServer", "name": "NOAA NWS Radar", "source": "defaults" },
                   { "type": "esriMapService", "url": "http://gis.srh.noaa.gov/arcgis/rest/services/watchwarn/MapServer", "name": "NOAA NWS Watches/Warnings", "source": "defaults" }
                ]
            }
        },
        markup: {
            visible: true
        },
        accordion: {
            visible: true,
            widgetOptions: {}
        },
        attributeSearch: {
            visible: true,
            widgetOptions: {
                enableSaveAsQuickSearch: true,
                useGlobalSpatialFilter: true,
                requireGlobalSpatialFilter: true,
                globalSpatialFilter: { "type": "extent", "xmin": 11175181.148985026, "ymin": 3356940.671700512, "xmax": 11261625.59342947, "ymax": 3406996.227256068, "spatialReference": { "wkid": 102747, "latestWkid": 2284 } },
                noGlobalSpatialFilterMessage: "<strong>One</strong> base must be in view to perform this search"
            },
            menuTitle: "Search",
            widgetTitle: "Search"
        },
        simpleSearch: {
            visible: true,
            widgetOptions: {
                actions: [
                   {
                       label: "Addresses",
                       value: {
                           type: "geocode",
                           query: { "where": "", "outFields": ["*"] },
                           mainWhere: "(UPPER(ADDRESS) LIKE UPPER('%%s%') OR UPPER(ADDRESS2) LIKE UPPER('%%s%') OR UPPER(HN_LABEL) LIKE UPPER('%%s%'))",
                           idProperty: "ADDRESS",
                           joinOperator: " AND ",
                           hiddenFields: [
                              "OBJECTID"
                           ],
                           autoCompleteMinLength: 4,
                           autocompleteEnabled: true,
                           endPoint: "http://138.0.10.140/arcgis/rest/services/Locators/Centerline_Locator/GeocodeServer",
                           streetLookupUrl: require.toUrl("/StreetLookup/Index")
                       }
                   },
                   {
                       label: "City Parcels",
                       value: {
                           type: "query",
                           query: { "where": "", "outFields": ["*"] },
                           mainWhere: "(UPPER(ACCOUNT) LIKE UPPER('%%s%') OR UPPER(ADDRESS) LIKE UPPER('%%s%') OR UPPER(OWNER1) LIKE UPPER('%%s%') OR UPPER(OWNER2) LIKE UPPER('%%s%') OR UPPER(OWNER3) LIKE UPPER('%%s%') OR UPPER(OWNER4) LIKE UPPER('%%s%') OR UPPER(MAILING1) LIKE UPPER('%%s%'))",
                           idProperty: "ACCOUNT",
                           joinOperator: " AND ",
                           hiddenFields: [
                              "OBJECTID"
                           ],
                           autoCompleteMinLength: 4,
                           autocompleteEnabled: true,
                           endPoint: "http://138.0.10.140/arcgis/rest/services/WVTest/Main_WVTest/MapServer/30"
                       }
                   }
                ]
            }
        },
        measure: {
            visible: true
        },
        swipe: {
            visible: true
        },
        azimuth: {
            visible: true,
            widgetOptions: {}
        },
        zoomToXY: {
            visible: true
        },
        basemapGallery: {
            visible: true,
            widgetOptions: {
                showArcGISBasemaps: false
            }
        },
        coordinateDisplay: {
            visible: true
        },
        scaleBar: {
            visible: true
        },
        resultsConsole: {
            visible: false
        },
        routingTool: {
            visible: false
        },
        overviewMap: {
            visible: true
        },
        mapToolbar: {
            visible: true
        },
        bookmarks: {
            visible: true,
            savedBookmarks: []
        },
        viewIn: {
            visible: true
        },
        exportTool: {
            visible: true,
            menuTitle: "Export",
            widgetTitle: "Export Map",
            widgetOptions: {
                tokenUrl: require.toUrl("/Home/GenerateToken"),
                supportedImageFormats: [
                    { label: 'PNG', value: 'PNG32' },
                    { label: 'GIF', value: 'GIF' },
                    { label: 'JPG', value: 'JPG' },
                    { label: 'EPS', value: 'EPS' },
                    { label: 'SVG', value: 'SVG' }
                ],
                supportedTemplatedFormats: [
                    { label: 'PDF', value: 'PDF' }
                ]
            }
        },
        mapLink: {
            visible: true,
            widgetOptions: {
                enableShortUrls: false,
                autoUpdateLink: true
            }
        },
        infoWindow: {
            visible: true,
            widgetOptions: {

            }
        },
        clientWidgets: [
        ]
    };
});