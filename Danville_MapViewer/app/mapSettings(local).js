﻿define([
"require"
, "dojo/dom-construct"
, "wvs/utilities/events/QuickIdentify"
, "wvs/utilities/events/ContextMenu"
, "dojo/text!./resources/header.html"
], function (
require
, domConstruct
, QuickIdentify
, ContextMenu
, header) {
    return {
        alwaysUseProxy: false,
        proxyUrl: "/proxy.ashx",
        templateDirectory: "../../Templates/",
        // Defaults overrides any declared properties from wvs/_base/config.js
        defaults: {
            services: {
                print: "http://vm102.worldviewsolutions.net:6080/arcgis/rest/services/PrintingExtended/GPServer/Printing",
                //exportService: "http://vm101.worldviewsolutions.net/vgis2/Danville_MapViewer.Web.Services.Export.ashx",
                geometry: "http://vm101.worldviewsolutions.net:6080/arcgis/rest/services/Utilities/Geometry/GeometryServer",
                markupService: require.toUrl("/Markup"),
                markup: require.toUrl("/Markup"),
                viewShed: null
            }
        },
        globalOptions: {
            globalAttributeRestrictions: [],
            layers: [
                {
                    url: "http://gis.danville-va.gov/arcgis/rest/services/Main/MapServer", // <required>
                    type: "ArcGISDynamicMapServiceLayer", // <required>
                    // optional
                    options: {
                        id: "danville_safety",
                        _hidden: [6]
                    },
                    hiddenFields: [
                       "OBJECTID",
                       "OBJECT_ID",
                       "OID"
                    ]
                }
            ],
            proxyReplacements: [
                //{ oldText: "http://sampleserver1.arcgisonline.com", newText: "http://localhost" }
            ],
            proxyRules: [
                // Use custom proxy (other than the one packaged with M4
                //  { proxyUrl: "http://someproxy", urlPrefix: "http://someprefix.com/prefix" }
                // or use the default proxy and just provide a url prefix
                // { urlPrefix: "" }
            ],
            layout: {
                embedded: false,
                header: {
                    visible: true,
                    style: 'background-color: #2f5993; repeat-x; width: 100%; height: 42px; color: #000; padding: 0px; margin: 0px; border-bottom: 1px solid #1676b1;',//
                    // define dom nodes to be added, in order
                    content: [
                        domConstruct.create("img", { src: require.toUrl('../app/resources/danville_logo.gif'), style: { "padding-top": "4px", background: require.toUrl("../app/resources/images/ui-bg_highlight-hard_20_000066_1x100.png") + ' repeat-x;' } })
                    ]
                },
                minimized: {
                    headerRegion: true,
                    leftRegion: true,
                    bottomPane: true,
                    mapToolbar: true
                },
                maximized: {
                    headerRegion: false,
                    leftRegion: false,
                    bottomPane: false,
                    mapToolbar: false
                }
            },
            // Instead of having the mapviewer having too much knowledge of map events. We pass in the map event that we want by loading it via AMD.
            mapEvents: [
                {
                    ctr: QuickIdentify,
                    options: { tolerance: 5, showSingleResult: false }
                },
                {
                    ctr: ContextMenu,
                    options: { menuItems: [ContextMenu.MenuItems.PushPin] }
                }
            ],

            autoSave: {
                // if we have a save endpoint available (currently, only local storage if browser supports it,) then auto-save
                enabled: false,
                // auto-save when one of these topics is published; an empty array is equivalent to disabling auto-save
                // "map/extent/change", "map/basemap/change", "map/layer/add", "map/layer/remove"
                topics: ["map/basemap/change", "markup/item/modify", "markup/item/add", "markup/item/remove", "markup/item/move", "markup/item/copy"]
            },
            showLoadingOverlay: false,
            loadingOverlayImage: null,
            topMenu: {
                Tools: { // without label, the key of the object is used
                    type: "submenu",
                    items: [
                        {
                            type: "item",
                            label: "Search",
                            iconClass: "",
                            moduleName: "attributeSearch",
                            options: {

                            }
                        }
                    ]
                },
                mapManager: {
                    type: "item",
                    label: "Map Manager",
                    iconClass: "",
                    moduleName: "mapManager", // moduleName tells it to look up the module by name
                    target: "", // options: dialog, toolContainer; defaults to dialog
                    options: {

                    }
                }
            }
        },
        map: {
            widgetOptions: {
                extent: {
                    "xmin": -8853971.125099845, "ymin": 4373999.607502269, "xmax": -8823625.624870673, "ymax": 4389936.727899706, "spatialReference": { "wkid": 102100 }
                }, // Required
                sliderStyle: 'small',
                basemap: "gray",
                displayGraphicsOnPan: true
            }
        },
        identify: {
            visible: true
        },
        toc: {
            visible: true
        },
        mapManager: {
            visible: true,
            widgetOptions: {
                enableArcGISOnline: true,
                mapServicesTitle: "Map Services",
                items: [],
                ////List of services that will always be available to users.
                requiredItems: [
                   { "type": "arcGisOnlineWebMap", "url": "https://vdemgis.maps.arcgis.com/home/webmap/viewer.html?webmap=bf0e747df23b45c3862c4d0ab0bad7a1", "name": "VIPER", "source": "defaults" },
                   { "type": "esriImageService", "url": "http://gis.apfo.usda.gov/ArcGIS/rest/services/NAIP/Virginia_2012_1m_NC/ImageServer", "name": "Virginia NAIP Imagery 2012", "source": "defaults" },
                   { "type": "esriMapService", "url": "http://gis.srh.noaa.gov/arcgis/rest/services/RIDGERadar/MapServer", "name": "NOAA NWS Radar", "source": "defaults" },
                   { "type": "esriMapService", "url": "http://gis.srh.noaa.gov/arcgis/rest/services/watchwarn/MapServer", "name": "NOAA NWS Watches/Warnings", "source": "defaults" }
                ]
            }
        },
        markup: {
            visible: true
        },
        accordion: {
            visible: true,
            widgetOptions: {}
        },
        attributeSearch: {
            visible: true,
            widgetOptions: {
                enableSaveAsQuickSearch: true,
                useGlobalSpatialFilter: true,
                requireGlobalSpatialFilter: true,
                noGlobalSpatialFilterMessage: "<strong>One</strong> base must be in view to perform this search"
            },
            menuTitle: "Search",
            widgetTitle: "Search"
        },
        simpleSearch: {
            visible: true,
            widgetOptions: {
                actions: [
                   {
                       label: "Point of Interest",
                       value: {
                           type: "lookup",
                           query: { "where": "", "outFields": ["*"] },
                           mainWhere: "(UPPER(NAME) LIKE UPPER('%%s%') OR UPPER(ADDRESS) LIKE UPPER('%%s%'))",
                           idProperty: "NAME",
                           joinOperator: " AND ",
                           hiddenFields: [
                              "OBJECTID"
                           ],
                           autocompleteEnabled: true,
                           endPoint: "http://gis.danville-va.gov/arcgis/rest/services/Main/MapServer/0"
                       }
                   },
                    {
                        label: "Street Names",
                        value: {
                            type: "lookup",
                            query: { "where": "", "outFields": ["*"] },
                            mainWhere: "(UPPER(ST_LABEL) LIKE UPPER('%%s%'))",
                            idProperty: "ST_LABEL",
                            joinOperator: " AND ",
                            hiddenFields: [
                               "OBJECTID"
                            ],
                            autocompleteEnabled: true,
                            endPoint: "http://gis.danville-va.gov/arcgis/rest/services/Main/MapServer/8"
                        }
                    },
                    {
                        label: "Trails",
                        value: {
                            type: "lookup",
                            query: { "where": "", "outFields": ["*"] },
                            mainWhere: "(UPPER(TRAIL_NAME) LIKE UPPER('%%s%'))",
                            idProperty: "TRAIL_NAME",
                            joinOperator: " AND ",
                            hiddenFields: [
                               "OBJECTID"
                            ],
                            autocompleteEnabled: true,
                            endPoint: "http://gis.danville-va.gov/arcgis/rest/services/Main/MapServer/7"
                        }
                    }
                ]
            }
        },
        measure: {
            visible: true
        },
        swipe: {
            visible: true
        },
        azimuth: {
            visible: false,
            widgetOptions: {}
        },
        zoomToXY: {
            visible: true
        },
        basemapGallery: {
            visible: true
        },
        coordinateDisplay: {
            visible: true
        },
        scaleBar: {
            visible: true
        },
        resultsConsole: {
            visible: false
        },
        overviewMap: {
            visible: true
        },
        mapToolbar: {
            visible: true
        },
        bookmarks: {
            visible: true,
            savedBookmarks: [
                {
                    name: "Test saved bookmark",
                    extent: {
                        "xmin": -8626203.080283865,
                        "ymin": 4514236.311763747,
                        "xmax": -8618760.0246544,
                        "ymax": 4518249.255748823,
                        "spatialReference": { "wkid": 102100 }
                    }
                }
            ]
        },
        viewIn: {
            visible: true
        },
        exportTool: {
            visible: true,
            menuTitle: "Export",
            widgetTitle: "Export Map",
            widgetOptions: {
                supportedImageFormats: [
                    { label: 'PNG', value: 'png' },
                    { label: 'JPG', value: 'jpg' },
                    { label: 'GIF', value: 'gif' },
                    { label: 'EPS', value: 'eps' },
                    { label: 'SVG', value: 'svg' }
                ],
                supportedTemplatedFormats: [
                    { label: 'PDF', value: 'pdf' }
                ]
            }
        },
        mapLink: {
            visible: true,
            widgetOptions: {
                enableShortUrls: false,
                autoUpdateLink: true
            }
        },
        infoWindow: {
            visible: true,
            widgetOptions: {

            }
        },
        clientWidgets: [
        ]
    };
});