﻿define([
    "dojo/_base/lang"
    , "dojo/string"
    , "dojo/_base/declare"
    , "./_commonArcGISMethods"
    , "esri/layers/TileInfo"
    , "esri/SpatialReference"
    , "esri/layers/WebTiledLayer"
], function (lang, string, declare, common, TileInfo, SpatialReference, WebTiledLayer) {
   return declare("wvs.extensions.layers.ArcGISWebTiledLayer", [WebTiledLayer],
      {
         subDomainIndex: 0,

         roadsUrl: "http://${subDomain}.tiles.virtualearth.net/tiles/r${quadKey}?g=507&mkt=${culture}&shading=hill",

         aerialsUrl: "http://${subDomain}.tiles.virtualearth.net/tiles/a${quadKey}?g=507&mkt=${culture}",

         hybridsUrl: "http://${subDomain}.tiles.virtualearth.net/tiles/h${quadKey}?g=507&mkt=${culture}",

         mapStyle: null,

         culture: "en-US",

         getTileUrl: function (level, row, column) {
            var quadKey = "", d, mask;

            for (var i = level; i > 0; i--) {
               d = "0"; mask = 1 << (i - 1);
               if ((column & mask) != 0) {
                  d++;
               }
               if ((row & mask) != 0) {
                  d++; d++;
               }
               quadKey = quadKey + d;
            }
            var returnString = string.substitute(this.url, { quadKey: quadKey, culture: this.culture, subDomain: this.subDomains[this.subDomainIndex] });
            this.subDomainIndex++;
            if (this.subDomainIndex >= this.subDomains.length) {
               this.subDomainIndex = 0;
            }
            return returnString;
         }
      });
});