﻿
define([
    "dojo/_base/declare"
    , "dojo/_base/lang"
    , "dojo/_base/array"
    , "esri/layers/FeatureLayer"
    , "esri/request"
    , "esri/graphic"
    , "esri/dijit/PopupTemplate"
    , "wvs/utilities/jsonConverters"
    , "esri/geometry/jsonUtils"
    , "wvs/common/InfoTemplateManager"
], function (
    declare
    , lang
    , array
    , FeatureLayer
    , request
    , Graphic
    , PopupTemplate
    , jsonConverters
    , geometryJsonUtils
    , InfoTemplateManager
) {
    var DeferredFeatureLayer = declare([FeatureLayer], {

        declaredClass: "esri.layers.DeferredFeatureLayer",

        serviceUrl: null,

        feedUrl: null,

        _layerLoadedFromService: false,

        constructor: function (featureCollection, options) {
            if ((!options.feedType || options.feedType !== "GeoJSON") && !options.serviceUrl) {
                throw "No serviceUrl for " + this.declaredClass;
            }
            if (!options.feedUrl) {
                throw "No feedUrl for " + this.declaredClass;
            }


            this._visibleInToc = true;

            if (options.id) {
                this.title = options.id;
            }
            this.feedType = options.feedType || "GeoRSS";
            this.serviceUrl = options.serviceUrl || "";
            this.feedUrl = options.feedUrl || "";

            if (!options.infoTemplate && featureCollection.popupInfo) {
                this.infoTemplate = new PopupTemplate(featureCollection.popupInfo);
            }
            if (options.infoTemplate) {
                this.infoTemplate = options.infoTemplate;
            }

            if (this.visible) {
                this._loadFromEndPoint();
            }
        },
        _loadFromEndPoint: function () {
            if (this.feedType === "GeoRSS") {
                this._loadFromGeoRSS();
            }
            else if (this.feedType === "GeoJSON") {
                this._loadGeoJSONFromUrl();
            }
        },
        _loadGeoJSONFromUrl: function(){
            var feedRequest = request({
                url: this.feedUrl,
                handleAs: "text"
            });

            feedRequest.then(
                lang.hitch(this, function (response) {
                    console.log(response);
                    var s = response.indexOf("{"),
                        e = response.lastIndexOf("}");
                    if (s === -1 || e === -1) {
                        throw "Invalid GeoJSON for " + this.declaredClass;
                    }
                    var _transformedResponse = response.substring(s, e + 1),
                        converter = new jsonConverters.geoJsonConverter();
                    _transformedResponse = converter.toEsri(JSON.parse(_transformedResponse));
                    this._layerLoadedFromService = true;
                    console.log(_transformedResponse);
                    
                    if (_transformedResponse && _transformedResponse.features) {
                        var features = _transformedResponse.features;
                        for (var i = 0; i < features.length; i++) {
                            if (i === 0) {
                                
                            }
                            this.add(new Graphic(geometryJsonUtils.fromJson(features[i].geometry), null, features[i].attributes));
                        }
                    }
                    //var layers = response.featureCollection.layers;
                    //for (var i = 0; i < layers.length; i++) {
                    //    var layer = layers[i];
                    //    if (layer.featureSet.features.length && layer.featureSet.geometryType === this.geometryType) {
                    //        var features = layer.featureSet.features;
                    //        for (var j = 0; j < features.length; j++) {
                    //            this.add(new Graphic(features[j]));
                    //        }
                    //    }
                    //}
                })
            );
        },
        _loadFromGeoRSS: function(){
            var feedRequest = request({
                url: this.serviceUrl,
                content: { url: this.feedUrl },
                callbackParamName: "callback"
            });

            feedRequest.then(
                lang.hitch(this, function (response) {
                    this._layerLoadedFromService = true;
                    var layers = response.featureCollection.layers;
                    for (var i = 0; i < layers.length; i++) {
                        var layer = layers[i];
                        if (layer.featureSet.features.length && layer.featureSet.geometryType === this.geometryType) {
                            var features = layer.featureSet.features;
                            for (var j = 0; j < features.length; j++) {
                                this.add(new Graphic(features[j]));
                            }
                        }
                    }
                })
            );
        },
        setVisibility: function (visible) {
            this.inherited(arguments);
            if (visible && !this._layerLoadedFromService) {
                this._loadFromEndPoint();
            }
        },
        refresh: function () {
            if (this.visible) {
                this._layerLoadedFromService = false;
                this.clear();
                this._loadFromEndPoint();
            }
        }

    });

    DeferredFeatureLayer._fcBase = {
        "layerDefinition": {
            "geometryType": null,
            "extent": {
                "xmin": -180,
                "ymin": -90,
                "xmax": 180,
                "ymax": 90,
                "spatialReference": {
                    "wkid": 4326
                }
            },
            "objectIdField": "id",
            "visibilityField": "visibility",
            "displayFieldName": "name",
            "fields": [{
                "name": "id",
                "alias": "id",
                "type": "esriFieldTypeOID"
            },
            {
                "name": "visibility",
                "alias": "visibility",
                "type": "esriFieldTypeInteger"
            },
            {
                "name": "name",
                "alias": "name",
                "type": "esriFieldTypeString"
            },
            {
                "name": "description",
                "alias": "description",
                "type": "esriFieldTypeString"
            },
            {
                "name": "copyright",
                "alias": "copyright",
                "type": "esriFieldTypeString"
            },
            {
                "name": "title",
                "alias": "title",
                "type": "esriFieldTypeString"
            },
            {
                "name": "category",
                "alias": "category",
                "type": "esriFieldTypeString"
            },
            {
                "name": "guid",
                "alias": "guid",
                "type": "esriFieldTypeString"
            },
            {
                "name": "pubDate",
                "alias": "pubDate",
                "type": "esriFieldTypeString"
            }],
            "drawingInfo": {
                "renderer": null
            },
            "capabilities": "Query,Data",
            "types": []
        },
        "featureSet": {
            "geometryType": null,
            "spatialReference": {
                "wkid": 4326
            },
            "features": []
        },
        "popupInfo": {
            "title": "{name}",
            "description": "{description}"
        }
    };

    DeferredFeatureLayer.getPointTemplate = function (options) {
        var renderer = {
            "type": "simple",
            "symbol": {
                "angle": 0,
                "xoffset": 0,
                "yoffset": 0,
                "type": "esriPMS",
                "url": options.imageSymbolUrl,
                "width": 22,
                "height": 22,
                "size": 22
            }
        },
            fc = lang.clone(DeferredFeatureLayer._fcBase);

        fc.layerDefinition.drawingInfo.renderer = renderer;
        fc.featureSet.geometryType = "esriGeometryPoint";
        fc.layerDefinition.geometryType = "esriGeometryPoint";
        if (options.fields) {
            fc.layerDefinition.fields = options.fields;
        }
        if (options.noDefaultPopup) {
            delete fc.popupInfo;
        }
        lang.setObject("window.__simba", renderer.symbol);
        return fc;
    };

    DeferredFeatureLayer.getLineTemplate = function (options) {
        var renderer = {
                "type": "simple",
                "label": "",
                "description": "",
                "symbol": {
                    "width": 1.5,
                    "style": "esriSLSSolid",
                    "color": [243,
                    110,
                    32,
                    255],
                    "type": "esriSLS"
                }
            },
            fc = lang.clone(DeferredFeatureLayer._fcBase);


        fc.layerDefinition.drawingInfo.renderer = renderer;
        fc.featureSet.geometryType = "esriGeometryPolyline";
        fc.layerDefinition.geometryType = "esriGeometryPolyline";
        if (options.lineSymbol) {
            renderer.symbol = options.lineSymbol;
        }
        if (options.fields) {
            fc.layerDefinition.fields = options.fields;
        }
        if (options.noDefaultPopup) {
            delete fc.popupInfo;
        }
        return fc;
    };

    DeferredFeatureLayer.getPolygonTemplate = function (options) {
        var renderer = {
                "type": "simple",
                "label": "",
                "description": "",
                "symbol": {
                    "outline": {
                        "width": 1.5,
                        "style": "esriSFSSolid",
                        "color": [243,
                        110,
                        32,
                        255],
                        "type": "esriSLS"
                    },
                    "style": "esriSFSSolid",
                    "color": [243,
                    110,
                    32,
                    64],
                    "type": "esriSFS"
                }
            },
            fc = lang.clone(DeferredFeatureLayer._fcBase);


        fc.layerDefinition.drawingInfo.renderer = renderer;
        fc.featureSet.geometryType = "esriGeometryPolygon";
        fc.layerDefinition.geometryType = "esriGeometryPolygon";
        if (options.polygonSymbol) {
            renderer.symbol = options.polygonSymbol;
        }
        if (options.fields) {
            fc.layerDefinition.fields = options.fields;
        }
        if (options.noDefaultPopup) {
            delete fc.popupInfo;
        }
        return fc;
    };

    return DeferredFeatureLayer;
});