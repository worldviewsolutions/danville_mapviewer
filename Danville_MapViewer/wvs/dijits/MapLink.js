﻿// TODO: Ignore all things that come from ArcGISOnline

define([
    "dojo/_base/declare"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dojo/text!./templates/MapLink.html"
    , "dojo/_base/lang"
    , "dojo/_base/array"
    , "dojo/on"
    , "dijit/form/Button"
    , "dijit/form/SimpleTextarea"
    , "dijit/form/TextBox"
    , "dojo/dom-style"
    , "../common/Common"
    , "dojo/_base/window"
    , "dojo/string"
    , "dojo/dom-attr"
    , "esri/layers/ArcGISDynamicMapServiceLayer"
    , "../common/MapState"
    , "../common/MarkupManager"
    , "../extensions/esri/layers/MarkupLayer"
    , "dijit/form/CheckBox"
    , "dojo/fx/Toggler"
    , "dojo/fx"
    , "../_base/config"
], function (declare, _WidgetBase, _TemplatedMixin, template, lang, array, on, Button, SimpleTextarea, TextBox, domStyle, Common, win, string, domAttr, ArcGISDynamicMapServiceLayer, MapState, MarkupManager, MarkupLayer, CheckBox, Toggler, coreFx, wvsConfig) {
    return declare([_WidgetBase, _TemplatedMixin], {

        templateString: template,

        declaredClass: "wvs.dijits.MapLink",

        /* linkTemplate: String
        *      the template used to create the map link from the map state
        */
        linkTemplate: "?sv=${services}&b=${basemap}&lon=${lon}&lat=${lat}&s=${scale}&mcId=${markupCollectionId}",

        altLinkTemplate: "?sv=${services}&b=${basemap}&extent=${extent}&s=${scale}&mcId=${markupCollectionId}",

        _mapLink: null,

        markupService: null,

        markupLayer: null,

        /*@function constructor
        *       Initializes a new instance of the MapLink widget.
        * @param params: Object
        *       Contains any properties and settings to be mixed in to the new instance.
        * @param srcNodeRef: DOM Node
        *       Reference to the DOM node in which the widget will be created.
        */
        constructor: function (params, srcNodeRef) {
            // required params
            if (!params.map) {
                throw new Error("map required for " + this.declaredClass);
            }
            this.markupService = wvsConfig.defaults.services.markup;

            // optional parameters
            this.enableShortUrls = true;
            this.autoUpdateLink = true;

            lang.mixin(this, params);


            // instance vars
            this._showOptions = false;
            this.mapState = new MapState(this.map);
        },

        /*@function postCreate
        *       Handles creating all instance objects that depend on the instance being registered to the dom
        *       before they can reliably be created.
        */
        postCreate: function () {
            this.inherited(arguments);

            this.linkTextarea = new SimpleTextarea({
                style: "width:100%",
                rows: 10,
                selectOnClick: true
                //style: "width:350px;display:block;font-size:12px;"
            }, this.linkTextarea);

            this.updateLinkButton = new Button({
                label: "Update Link",
                onClick: lang.hitch(this, this.updateLink)
            }, this.updateLinkButton);

            if (wvsConfig.defaults.markupLayer || this.markupLayer) {
                this.serializeMarkup = new CheckBox({
                    checked: false,
                    onChange: lang.hitch(this, this._onMarkupCheckboxChange)
                }, this.serializeMarkup);

                this.optionsToggler = new Toggler({
                    node: this.options,
                    showFunc: coreFx.wipeIn,
                    hideFunc: coreFx.wipeOut
                });
            }

        },

        /*@function startup
        *       The initialization function for all processes which require that the widget has been placed in the dom and started.
        */
        startup: function () {
            var self = this;
            if (this.autoUpdateLink) {
                this.own(on(this.map, "extent-change", function () { self.updateLink(); }));
                this.own(on(this.map, "basemap-change", function () { self.updateLink(); }));
                domStyle.set(this.updateLinkButton.domNode, { display: "none" });
            }

            if (wvsConfig.defaults.markupLayer || this.markupLayer) {
                this.own(on(this.optionsLink, "click", lang.hitch(this, function () {
                    this._showOptions = !this._showOptions;
                    this.optionsLink.innerHTML = this._showOptions ? "Hide Options" : "Show Options";
                    if (this._showOptions)
                        this.optionsToggler.show();
                    else
                        this.optionsToggler.hide();
                })
                ));
            }
            else {
                domStyle.set(this.optionsLink, { display: "none" });
            }

            this._onMarkupCheckboxChange();

            this.own(on(this.openLink, "click", function () {
                window.open(self._mapLink, "_blank");
            }));
        },
        /* End Overrides */

        /*@function updateLink
        *       This function will create a new link to the map based on changes to the map or the requirements of what to display
        *       when the link is opened (e.g.- showing user-created markup).
        */
        updateLink: function () {
            var currentHref = window.document.location.protocol + "//" + window.document.location.host + window.document.location.pathname,
                mapState = this.mapState.toJson(),
                mapLinkTemplate,
                mapLink;
            if (mapState.map.center[0] == undefined) {
                mapLinkTemplate = currentHref + this.altLinkTemplate;
                mapLink = string.substitute(mapLinkTemplate, { scale: mapState.map.scale, extent: JSON.stringify(mapState.map.extent), basemap: mapState.map.basemap, services: JSON.stringify(mapState.layers), markupCollectionId: this.markupLayer._id || '-1' });
            } else {
                mapLinkTemplate = currentHref + this.linkTemplate;
                mapLink = string.substitute(mapLinkTemplate, { scale: mapState.map.scale, lat: mapState.map.center[1], lon: mapState.map.center[0], basemap: mapState.map.basemap, services: JSON.stringify(mapState.layers), markupCollectionId: this.markupLayer._id || '-1' });
            }
            this.linkTextarea.set("value", mapLink);

            this._mapLink = mapLink;
        },

        /*@function _onMarkupCheckboxChange
        *       The function to handle users toggling the "Include Markup" checkbox. On check, it will save the markup as a collection to the
        *       server, and return an ID to retrieve that collection and include in the link with the mcId parameter. On uncheck, it resets the
        *       mcId query parameter to -1.
        */
        _onMarkupCheckboxChange: function () {
            var self = this,
                checked = this.serializeMarkup.get('checked');
            this.markupLayer = new MarkupLayer({ title: "Shared Markup", id: "sharedMarkup" });
            array.forEach(this.map.layerIds, function (layerId) {
                var layer = this.map.getLayer(layerId);
                if (layer.declaredClass === "wvs.layers.MarkupLayer") {
                    if (checked) {
                        for (var j = 0; j < layer.items.data.length; j++) {
                            if (layer.items.data[j].type === "folder") {
                                this.markupLayer.addFolder(layer.items.data[j].name, layer.items.data[j].parent);
                            } else {
                                this.markupLayer.addGraphic(layer.items.data[j].graphic, layer.items.data[j].parent);
                            }
                        }
                    } else {
                        this.mapState.addExclusion(layerId);
                    }
                }
            }, this);
            if (checked) {
                domStyle.set(this.spinner, 'display', 'block');
                domStyle.set(this.linksButtons, 'display', 'none');
                var mm = new MarkupManager();
                mm.saveMarkupCollection(this.markupLayer).then(
                    function (message) {
                        domStyle.set(self.spinner, 'display', 'none');
                        domStyle.set(self.linksButtons, 'display', 'block');
                        self.updateLink();
                    },
                    function (message) {
                        Common.errorDialog(message, "Markup Layer Action");
                        domStyle.set(self.spinner, 'display', 'none');
                        domStyle.set(self.linksButtons, 'display', 'block');
                        self.updateLink();
                    }
                );
                this.updateLink();
            } else {
                this.updateLink();
            }
        }
    });
});