console.log("wvs/dijits/TocGroupLayerNode.js");

define([
    "dojo/_base/declare"
    , "dojo/text!./templates/_TocNode.html"
    , "dojo/_base/lang"
    , "./_TocNode"
    , "dijit/form/HorizontalSlider"
    , "dojo/dom-style"
    , "dojo/ready"
    , "dojo/dom-construct"
    , "dojo/dom-attr"
    , "dojo/on"
    , "dijit/Menu"
    , "dijit/MenuItem"
    , "dijit/MenuSeparator"
    , "dijit/PopupMenuItem"
    , "dijit/TooltipDialog"
    , "dojo/_base/array"
    , "./TocFeatureLayerNode"
    , "dojo/dom-class"
    , "dojo/has"
    , "dojo/dom"
    , "dojo/_base/sniff"
], function (declare, template, lang, _TocNode, HorizontalSlider, domStyle, ready, domConstruct, domAttr, on, Menu, MenuItem, MenuSeparator, PopupMenuItem, TooltipDialog, array, TocFeatureLayerNode, domClass, has, dom) {
    return declare([_TocNode], {

        templateString: template,

        // refreshDelay: Integer
        //      Number of milliseconds to delay between clicking a checkbox and refreshing the map service. A longer delay allow the user to check/uncheck multiple
        //      layers before refreshing the map service."dojo/ready",
        refreshDelay: 500,

        // useSpriteImage: Boolean
        //      Use the custom sprite image for the legend icons. When false, the "Legend" REST service will be used for 10.01 and newer services.
        useSpriteImage: false,

        declaredClass: "wvs.dijits.TocGroupLayerNode",

        hasDropDown: true,

        isGroupLayer: true,

        constructor: function (params, srcNodeRef) {
            console.log(this.declaredClass + " constructor");
            params = params || {};
            if (!params.mapService) {
                throw new Error('mapService not defined in params for ' + this.declaredClass);
            }
            if (!params.toc) {
                throw new Error('no reference to the toc found for ' + this.declaredClass);
            }

            lang.mixin(this, params);

            if (this.title === null) {
                if (this.mapService.name) {
                    this.title = this.mapService.name;
                } else {
                    // call the extension method in jstoolkit/layers/Layer.js to get the service name
                    this.title = this.mapService.getServiceName();
                }
            }

            
        },
        // extension point called by framework
        postCreate: function () {
            this._createChildNodes();
            this.set('transparency', 1 - this.mapService.opacity);
            this._createContextMenu();
            this.inherited(arguments);
        },
        setVisible: function (visible) {
            console.log(this.declaredClass + "->setVisible->" + visible);
            // return if the visibility has not changed
            //if (this._state.visible == visible) return;

            this._state.visible = visible;

            // update the map service
            if (visible) {
                this.mapService.show();
                this._showSwatch();
                //this._enableChildNodes();
                console.log("node.setVisible(true)->adjusting children...");
                this._adjustToMapScale();
                
            } else {
                this.mapService.hide();
                this._hideSwatch();
                //this._disableChildNodes();
                console.log("node.setVisible(false)->adjusting children...");
                this._adjustToMapScale();
            }
        },
        _checkboxOnClick: function (e) {
            this.setVisible(this._checkbox && this._checkbox.checked);
        },

        _refreshLayer: function () {
            if (this._refreshTimer) {
                window.clearTimeout(this._refreshTimer);
                this._refreshTimer = null;
            }

            var mapService = this.mapService;

            this._refreshTimer = window.setTimeout(function () {
                mapService.refresh();
            }, this.refreshDelay);
        },
        _createContextMenu: function () {
            var self = this;

            var mapServiceOptionsMenu = new Menu({
                leftClickToOpen: true
            });
            mapServiceOptionsMenu.bindDomNode(this.dropDownMenuIcon);
            // NOTE: esri doesn't support changing the opacity of feature layers in IE8 and below. Stoopid.
            if (!has("ie") ||  has("ie") > 8) {
                mapServiceOptionsMenu.addChild(_TocNode.ContextMenuItems.Transparency(this));
                if(!this.isFromWebMap)
                    mapServiceOptionsMenu.addChild(new MenuSeparator());
            }

            if (!this.isFromWebMap) {
                mapServiceOptionsMenu.addChild(new MenuItem({
                    label: "Remove",
                    iconClass: "jstoolkit-icon jstoolkit-icon-delete",
                    onClick: lang.hitch(self.toc, self.toc._removeServiceLayer, self.mapService)
                }));
            }


            /*
            NOTES: For the time being, Feature layers, GeoRSS layers, and KML layers will be at the top of the TOC and not movable.
            This is due to esri's convoluted way of storing and identifying layers in the map.
            
            var _moveUpMenuItem = new MenuItem({
                label: "Move Up",
                iconClass: "jstoolkit-icon jstoolkit-icon-arrow-up",
                onClick: lang.hitch(self.toc, self.toc._moveNodeUp, self)
            });

            mapServiceOptionsMenu.addChild(_moveUpMenuItem);

            var _moveDownMenuItem = new MenuItem({
                label: "Move Down",
                iconClass: "jstoolkit-icon jstoolkit-icon-arrow-down",
                onClick: lang.hitch(self.toc, self.toc._moveNodeDown, self)
            });
            mapServiceOptionsMenu.addChild(_moveDownMenuItem);
            */

            mapServiceOptionsMenu.startup();
            this.mapServiceOptionsMenu = mapServiceOptionsMenu;
        },
        _createChildNodes: function () {
            console.debug(this.declaredClass + ": _createChildNodes()");
            var featureLayers, featureLayerType;

            switch (this.mapService.declaredClass) {
                case "esri.layers.KMLLayer":
                    featureLayers = this.mapService.getLayers();
                    featureLayerType = "kml";
                    break;
                case "esri.layers.GeoRSSLayer":
                    featureLayers = this.mapService.getFeatureLayers();
                    featureLayerType = "georss";
                    break;
                case "wvs.layers.GroupLayer":
                    featureLayers = this.mapService.getLayers();
                    featureLayerType = "group";
                    break;
                default:
                    throw new Error("Unsupported layer type for " + this.declaredClass);
           } 
            array.forEach(featureLayers, function (featureLayer) {
                var node = new TocFeatureLayerNode({
                    _parent: this,
                    mapService: featureLayer,
                    featureLayerType: featureLayerType,
                    refreshDelay: this.refreshDelay,
                    useSpriteImage: this.useSpriteImage
                });
                node.placeAt(this.childrenNode);
                node.startup();
                this.nodes.push(node);
            }, this);

            this._updateVerticalAlignment();
        }

    });
});