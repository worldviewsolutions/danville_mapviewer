﻿define([
    "dojo/_base/declare"
    , "dojo/_base/lang"
    , "dojo/_base/array"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dojo/text!./templates/Geoprocessor.html"
    , "dojo/Evented"
    , "esri/tasks/Geoprocessor"
    , "esri/request"
    , "dojo/store/Memory"
    , "../common/Common"
    , "dojo/dom-style"
    , "dojo/dom-construct"
    , "dojo/Deferred"
    // GP Types
    , "esri/tasks/LinearUnit"
    // Form widgets
    , "dijit/form/TextBox"
    , "dijit/form/ComboBox"
    , "dijit/form/DateTextBox"
    , "dijit/form/Button"
    , "dijit/form/CheckBox"
    , "dijit/form/Select"
    , "dijit/form/Form"
    , "dijit/form/NumberTextBox"
    , "dijit/form/ValidationTextBox"
], function (
    declare
    , lang
    , array
    , _WidgetBase
    , _TemplatedMixin
    , template
    , Evented
    , esriGeoprocessor
    , esriRequest
    , Memory
    , Common
    , domStyle
    , domConstruct
    , Deferred
    // GP Types
    , LinearUnit
    // Form widgets
    , TextBox
    , ComboBox
    , DateTextBox
    , Button
    , CheckBox
    , Select
    , Form
    , NumberTextBox
    , ValidationTextBox
){

    var Geoprocessor = declare([_WidgetBase, _TemplatedMixin, Evented], {
        templateString: template,

        declaredClass: "wvs.dijits.Geoprocessor",

        // GPParameterMap: Object
        //      maps Geoprocessor types to Javascript Objects
        GPParameterMap: {
            GPBoolean: "boolean",
            GPDataFile: "DataFile",
            GPDate: "Date",
            GPDouble: "number",
            GPFeatureRecordSetLayer: "FeatureSet",
            GPLinearUnit: "LinearUnit",
            GPLong: "number",
            GPRasterData: "RasterData",
            GPRasterLayer: "RasterData",
            GPRecordSet: "FeatureSet",
            GPString: "string"
        },



        mode: null,

        constructor: function (params, srcNodeRef) {

            var gpServicesArray = params.gpServices || [];



            this.gpServices = new Memory({ data: gpServicesArray, idProperty: "url" });
            delete params.gpServices;

            this.inputParams = {};
        },
        postCreate: function () {

            this.gpServiceSelect = new ComboBox({
                store: this.gpServices,
                autoComplete: false,
                autoWidth: true,
                searchAttr: "name",
                width:"250px",
                onChange: lang.hitch(this, this._onServiceSelect)
            }, this.gpServiceSelect);

            this.gpServiceAddNewButton = new Button({
                label: "Add New",
                iconClass: "icon-plus-sign",
                onClick: lang.hitch(this, function () { this.set("mode", Geoprocessor.ADD_GP); })
            }, this.gpServiceAddNewButton);

            // Create the form buttons for adding new geoprocessing services
            this.newServiceNameTextBox = new TextBox({}, this.newServiceNameTextBox);

            this.newServiceUrlTextBox = new TextBox({}, this.newServiceUrlTextBox);

            this.newServiceUrlAddButton = new Button({
                label: "Add",
                onClick: lang.hitch(this, function(){
                    if (this.newServiceNameTextBox.value && this.newServiceUrlTextBox.value) {
                        this.registerGPService({ url: this.newServiceUrlTextBox.value, name: this.newServiceNameTextBox.value }).then(
                            lang.hitch(this, function (gpService) {
                                this.set("mode", Geoprocessor.USE_GP);
                            })
                        );
                    }
                })
            }, this.newServiceUrlAddButton);

            this.newServiceUrlCancelButton = new Button({
                label: "Cancel",
                onClick: lang.hitch(this, function () {
                    this.set("mode", Geoprocessor.USE_GP);
                })
            }, this.newServiceUrlCancelButton);

            this.gpServiceExecuteButton = new Button({
                label: "Execute",
                iconClass: "icon-exclamation-sign",
                disabled: true,
                onClick: lang.hitch(this, this.executeGPTask)
            }, this.gpServiceExecuteButton);

            this._registerWatches();

            this.set("mode", Geoprocessor.USE_GP);
        },
        _registerWatches: function(){
            // Watch the mode
            this.watch("mode", lang.hitch(this, function (object, oldVal, newVal) {
                this._setMode(newVal);
            }));
        },
        _setMode: function(mode){
            if (mode === Geoprocessor.ADD_GP) {
                this.newServiceNameTextBox.set("value", "");
                this.newServiceUrlTextBox.set("value", "");
                this.gpServiceExecuteButton.set("disabled", true);
                domStyle.set(this.gpService, { display: "none" });
                domStyle.set(this.addNewServiceArea, { display: "block" });
            }
            else {
                var gpServiceSelected = this.gpServiceSelect.value;
                domStyle.set(this.noServiceMessage, !gpServiceSelected ? { display: "inline" } : { display: "none" });
                if (gpServiceSelected) {
                    // Register widgets for type
                    this.registerGPService(this.gpServiceSelect.item).then(
                        lang.hitch(this, function (gpService) {
                            this._createInputsForGPService(gpService);
                            this.gpServiceExecuteButton.set("disabled", false);
                        })
                    );
                    domStyle.set(this.gpServiceParams, { display: "block" });
                }
                domStyle.set(this.gpService, { display: "block" });
                domStyle.set(this.addNewServiceArea, { display: "none" });
            }
        },
        registerGPService: function (gpService) {
            // summary:
            //      registers a GP Service to be added to the selection list
            // gpService: Object
            //      an object describing the Geoprocessing service { name: "MyName", url: "http://myendpoint/GPServer"}
            // returns:
            //      a deferred object

            var deferred = new Deferred();


            // Check if we have already loaded this service
            var gpServiceStore = this.gpServices.get(gpService.url);
            if (gpServiceStore && gpServiceStore.serviceInfo) {
                deferred.resolve(gpServiceStore);
                return deferred;
            }


            // Verify GP valid & get Info
            var dfd = esriRequest({
                url: gpService.url,
                content: { f: "json" },
                handleAs: "json",
                callbackParamName: "callback"
            });

            dfd.then(
                // callback
                lang.hitch(this, function (response, ioArgs) {
                    // We need at least these to build our form
                    if (response.executionType && response.parameters && (response.name || response.displayName)) {
                        gpService.serviceInfo = response;
                        if(!gpServiceStore)
                            this.gpServices.put(gpService);
                        deferred.resolve(gpService);
                    }
                }),
                // errback
                lang.hitch(this, function (response, ioArgs) {
                    deferred.reject(response);
                    Common.errorDialog("Could not load the Geoprocessing service");
                    this.emit("add-error", response);
                })
            );

            return deferred;
        },
        executeGPTask: function () {
            if (this.gpServiceSelect.item && this.currentForm) {

                // TODO: Validate
                if (!this.currentForm.validate()) {
                    return;
                }

                var gpService = this.gpServiceSelect.item;
                var gp = new esriGeoprocessor(gpService.url);
                

                // Synchronous
                if (gpService.serviceInfo.executionType === "esriExecutionTypeSynchronous") {
                    gp.execute(this.inputParams,

                        lang.hitch(this, function (messages, results) {


                        }),
                        lang.hitch(this, function (error) {

                        })
                    );
                }
                // Asynchronous
                else {

                }
            }
        },
        _onServiceSelect: function(value){
            if (this.gpServiceSelect.item) {
                if (this.mode !== Geoprocessor.USE_GP) {
                    this.set("mode", Geoprocessor.USE_GP);
                }
                else
                    this._setMode(Geoprocessor.USE_GP);
            }
        },
        _createInputsForGPService: function (gpService) {
            if (gpService.serviceInfo) {
                var serviceInfo = gpService.serviceInfo;
                this.inputParams = {};
                domConstruct.empty(this.gpServiceParams);
                this.currentForm = null;

                var paramDiv = domConstruct.create("div", {}, this.gpServiceParams);
                array.forEach(serviceInfo.parameters, function (parameter) {
                    if (parameter.direction === "esriGPParameterDirectionInput") {
                        var labelDiv = domConstruct.create("div", { "class": "colspan4" }, paramDiv);
                        var label = domConstruct.create("label", { innerHTML: parameter.displayName || parameter.name }, labelDiv);
                        var inputDiv = domConstruct.create("div", { "class": "colspan8" }, paramDiv);
                        var input = this._createInputFromGpType(parameter);
                        if (input) {
                            if (input.declaredClass)
                                input.placeAt(inputDiv);
                            else
                                domConstruct.place(input, inputDiv);
                        }
                    }
                }, this);

                this.currentForm = new Form({}, paramDiv);
                this.currentForm.connectChildren();
            }
        },
        _createInputFromGpType: function (parameter) {
            parameter.parameterType = "esriGPParameterTypeRequired";
            
            this.inputParams[parameter.name] = null;

            if (parameter.choiceList) {
                return new Select({
                    options: array.map(parameter.choiceList, function(choice) { return { label: choice, value: choice};}),
                    onChange: lang.hitch(this, function (value) { this.inputParams[parameter.name] = value; }),
                    required: parameter.parameterType === "esriGPParameterTypeRequired" ? true : false
                });
            }
            else if (parameter.dataType === "GPBoolean") {
                return new CheckBox({
                    checked: false,
                    onChange: lang.hitch(this, function (date) { console.log(date); this.inputParams[parameter.name] = date; }),
                    required: parameter.parameterType === "esriGPParameterTypeRequired" ? true : false
                });
                this.inputParams[parameter.name] = false;
            }
            else if (parameter.dataType === "GPDate") {
                return new DateTextBox({
                    onChange: lang.hitch(this, function (value) { this.inputParams[parameter.name] = value; }),
                    required: parameter.parameterType === "esriGPParameterTypeRequired" ? true : false
                });
            }
            else if (parameter.dataType === "GPDouble" || parameter.dataType === "GPLong") {
                return new NumberTextBox({
                    onChange: lang.hitch(this, function (value) { console.log(value); this.inputParams[parameter.name] = value; }),
                    required: parameter.parameterType === "esriGPParameterTypeRequired" ? true : false
                });
            }
            else if (parameter.dataType === "GPString") {
                return new ValidationTextBox({
                    onChange: lang.hitch(this, function (value) { console.log(value); this.inputParams[parameter.name] = value; }),
                    required: parameter.parameterType === "esriGPParameterTypeRequired" ? true : false
                });
            }
            else if (parameter.dataType === "GPFeatureRecordSetLayer" || parameter.dataType === "GPRecordSet") {
                // TODO: Determine where to get these params
                // Brainstorms:
                //      FeatureSet | Graphic
                //      Event | Topic
                return null;
            }
            else if (parameter.dataType === "GPLinearUnit") {
                var containingDiv = domConstruct.create("div");
                this.inputParams[parameter.name] = new LinearUnit();
                var distanceTextBox = new NumberTextBox({
                    onChange: lang.hitch(this, function (value) { console.log(value); 
                        this.inputParams[parameter.name].distance = value;
                    }),
                    required: parameter.parameterType === "esriGPParameterTypeRequired" ? true : false
                }).placeAt(containingDiv);
                var unitSelect = new Select({ options: [ { label: "Meters", value: "esriMeters"}, { label: "Miles", value: "esriMiles"}, { label: "Kilometers", value: "esriKilometers"}],
                    onChange: lang.hitch(this, function (value) { console.log(value);
                        if(!this.inputParams[parameter.name]){
                            this.inputParams[parameter.name] = new LinearUnit();
                        }
                        this.inputParams[parameter.name].units = value;
                    }),
                    required: parameter.parameterType === "esriGPParameterTypeRequired" ? true : false
                }).placeAt(containingDiv);
                this.inputParams[parameter.name].units = unitSelect.value;
                return containingDiv;
            }

            throw new Error("We have encountered an unsupported parameter data type. This Geoprocessing may not be loaded");

            return null;
        }

    });

    Geoprocessor.USE_GP = 0;
    Geoprocessor.ADD_GP = 1;

    return Geoprocessor;
});