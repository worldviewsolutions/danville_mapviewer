﻿console.log("wvs/dijits/Swipe.js");

define([
    "dojo/_base/declare"
    , "dojo/_base/lang"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dojo/text!./templates/SwipeTool.html"
    , "esri/dijit/LayerSwipe"
    , "dijit/form/ToggleButton"
    , "dijit/form/Select"
    , "dojo/dom-class"
    , "dojo/dom"
    , "dojo/_base/array"
    , "esri/layers/ArcGISTiledMapServiceLayer"
    , "esri/layers/ArcGISDynamicMapServiceLayer"
    , "wvs/common/bingMapsUtil"
    , "../_base/config"
], function (declare, lang, _WidgetBase, _TemplatedMixin, template, LayerSwipe, ToggleButton, Select, domClass, dom, array, ArcGISTiledMapServiceLayer, ArcGISDynamicMapServiceLayer, bingMapsUtil, wvsConfig) {
    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,

        declaredClass: "wvs.dijits.Swipe",

        constructor: function (params, srcNodeRef) {

            // map: Map
            //      the map to attach to the Swipe tool
            this.map = null;

            // layers: Layer[]
            //      Layers from the map 
            this.layers = null;

            // layers: BasemapLayer[]
            //      Basemapfrom the map 
            this.basemapLayers = null;

            // required params
            if (!params.map) {
                throw new Error("no map provided for " + this.declaredClass);
            }
            if (!params.layers && !params.basemapLayers) {
                throw new Error("no layers provided for " + this.declaredClass);
            }

            // swipeOptions: Object
            //      an object representing params for esri's swipe tool
            this.swipeOptions = {};

            // optional params per https://developers.arcgis.com/en/javascript/jsapi/layerswipe-amd.html
            // clip - The number of pixels to clip the swipe tool. Default value is 9.
            // enabled - If the widget is enabled and layers can be swiped. Default value is true.
            // left - The number of pixels to place the tool from the left of the map. By default it uses center for scope and 1/4 map width for vertical.
            // theme - Class used for styling the widget. Default value is "LayerSwipe"
            // top - The number of pixels to place the tool from the top of the map. By default it uses center for scope and 1/4 map height for horizontal.
            // type - Type of swipe tool to use. Default value is "vertical". Available options are "vertical", "horizontal" and "scope".
            // visible - Whether the widget is visible by default. Default value is true.

            lang.mixin(this.swipeOptions, params);

            // swipeTool: LayerSwipe
            //      instance of esri's Swipe tool
            this.swipeTool = null;

            // activeLayer: BasemapLayer
            //      The active basemap for the swipe tool
            this.activeLayer = null;

        },
        postCreate: function () {
            var self = this;
            this._registerWatches();

            // OnChange for both mode buttons
            var buttonOnChange = function () {
                if (this.get("value") === self.swipeOptions.type) {
                    this.set("checked", true);
                }
            };

            // OnClick for both mode buttons
            var buttonOnClick = function () {
                self.setMode(this.get("value"));
            };

            this.horizontalButton = new ToggleButton({
                label: "Horizontal",
                value: "horizontal",
                onClick: buttonOnClick,
                onChange: buttonOnChange
            }, this.horizontalButton);


            this.verticalButton = new ToggleButton({
                label: "Vertical",
                value: "vertical",
                checked: true,
                onClick: buttonOnClick,
                onChange: buttonOnChange
            }, this.verticalButton);

            //Add btn class for toggle button styles
            domClass.add(this.horizontalButton.domNode, wvsConfig.defaults.buttonWidgetClass);
            domClass.add(this.verticalButton.domNode, wvsConfig.defaults.buttonWidgetClass);

            // Add basemap layers to select list
            var selectData = array.map(this.basemapLayers, function (basemapLayer) {
                return { label: '<span style="margin-left:5px;">' + basemapLayer.title + '</span>', value: basemapLayer.id };
            });
            selectData[0].selected = true;

            // The dijit Select used to represent the swipeable layers
            this.layerSelect = new Select({
                options: selectData,
                onChange: function () { self.set("activeLayer", this.get("value")); }
            }, this.layerSelect);
        },
        startup: function () {
            this.inherited(arguments);
            // Set default layer
            this.set("activeLayer", this.layerSelect.options[0].value);
        },
        destroy: function () {
            if (this.swipeTool) {
                array.forEach(this.swipeOptions.layers, function (layer) {
                    this.swipeOptions.map.removeLayer(layer);
                }, this);
                this.swipeTool.destroy();
            }
            this.inherited(arguments);
        },
        _registerWatches: function () {
            // summary:
            //      register watches for this widget
            this.watch("activeLayer", lang.hitch(this, function (name, oldVal, newVal) {
                // We need to destroy the swipe tool
                if (oldVal) {
                    this.swipeTool.disable();
                    array.forEach(this.swipeOptions.layers, function (layer) {
                        this.swipeOptions.map.removeLayer(layer);
                    }, this);
                }

                // We need to create the swipe tool
                if (newVal) {
                    this._setActiveLayer(newVal);
                }
            }));
        },
        _setActiveLayer: function (layerId) {
            // summary:
            //      set the active layer for the swipe tool
            array.forEach(this.basemapLayers, function (basemapLayer) {
                if (basemapLayer.id === layerId) {
                    var layers = array.map(basemapLayer.layers, function (layer) {
                        if (layer.url) {
                            return new ArcGISTiledMapServiceLayer(layer.url);
                        } else if (layer.type.toLowerCase().indexOf("bing") >= 0) {
                            return bingMapsUtil.getBingMapFromType(layer.type);
                        }
                    });
                    array.forEach(layers, function (layer) {
                        this.swipeOptions.map.addLayer(layer);
                    }, this);
                    this.swipeOptions.layers = layers;
                    if (!this.swipeTool) {
                        this.swipeTool = new LayerSwipe(this.swipeOptions);
                        this.swipeTool.startup();
                    }
                    else {
                        this.swipeTool.layers = layers;
                        this.swipeTool.enable();
                    }
                }
            }, this);
        },
        setMode: function (mode) {
            // setMode:
            //      sets the mode of the swipe tool
            if (this.swipeOptions.type === mode) {
                return;
            }
            this.swipeOptions.type = mode;

            if (mode === "horizontal") {
                this.horizontalButton.set("checked", true);
                this.verticalButton.set("checked", false);
            }
            else {
                this.horizontalButton.set("checked", false);
                this.verticalButton.set("checked", true);
            }

            if (!this.swipeOptions.layers) {
                return;
            }
            this.swipeTool.destroy();
            this.swipeTool = new LayerSwipe(this.swipeOptions);
            this.swipeTool.startup();
        }
    });
});