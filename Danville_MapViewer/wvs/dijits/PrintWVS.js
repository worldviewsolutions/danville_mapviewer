﻿define([
    "dojo/_base/declare"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dijit/_WidgetsInTemplateMixin"
    , "dojo/text!./templates/PrintWVS.html"
    , "dojo/Evented"
    , "dojo/_base/lang"
    , "dojo/_base/array"
    , "dojo/on"
    , "dojo/promise/all"
    , "esri/tasks/PrintParameters"
    , "esri/tasks/PrintTemplate"
    , "esri/tasks/PrintTask"
    , "esri/tasks/query"
    , "dijit/form/Select"
    , "dijit/form/RadioButton"
    , "dijit/form/CheckBox"
    , "dijit/form/TextBox"
    , "dijit/form/Button"
    , "../_base/config"
    , "dojo/dom-style"
    , "dojo/dom-class"
    , "dojo/dom"
    , "dojo/dom-attr"
    , "esri/request"
    , "dojo/dom-construct"
    , "../common/Common"
    , "../common/ObjUtil"
    , "esri/lang"
    , "esri/graphic"
    , "esri/symbols/jsonUtils"
    , "esri/geometry/Point"
    , "esri/geometry/Extent"
    , "esri/geometry/ScreenPoint"
    , "dijit/Fieldset"
    , "esri/SpatialReference"
], function (declare, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, template, Evented, lang, array, on, all, PrintParameters, PrintTemplate, PrintTask, Query, Select, RadioButton, CheckBox, TextBox, Button, wvsConfig, domStyle, domClass, dom, domAttr, esriRequest, domConstruct, Common, ObjUtil, esriLang, Graphic, symbolJsonUtils, Point, Extent, ScreenPoint, Fieldset, SpatialReference) {
    // module:
    //      wvs/dijits/Print

    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, Evented], {

        templateString: template,

        declaredClass: "wvs.dijits.Print",

        // GPServer: String
        //      points the geoprocessing print service that this widget will use (either custom or out-of-box)
        GPServer: null,

        // replaceMap: Array
        //      the print service must be able to access the urls for the webmap so sometimes we need to manipulate the request before it gets executed
        //      Example: [ { oldText: 'http://www.myservice.com:6080', newText: 'http://localhost'
        replaceMap: [],

        // qualityDef: Object(Enum)
        //      The quality specifications for printing maps.
        qualityDef: {
            Standard: 'standard',
            Medium: 'better',
            Best: 'best'
        },

        // textElements: Object(Enum)
        //      The enumerated type of text elements that may be used with templates.
        textElementsDef: {
            NAME: "Name",
            TITLE: "PageTitle"
        },

        //templates: Array
        //      The array of template objects returned from server.
        templates: null,

        // width: Number
        //      the width (inches) of the exported map
        width: 4,

        // height: Number
        //      the height (inches) of the exported map
        height: 4,

        // dpi: Number
        //      the dpi of the exported map
        dpi: 96,

        // useCustomMapScale: Boolean
        //      determines whether or not this map is to use a custom map scale
        useCustomMapScale: false,

        //overlayEnabled: Boolean
        //      Indicates whether the widget is in view and whether to indicate the extent of the printed result.
        overlayEnabled: false,

        // _exportLink: String
        //      the link to the exported image
        _exportLink: null,

        //provideMapLink: Bool
        //      Indicates whether the widget should provide an anchor tag linking to the result of the export or allow the user to handle the response.
        provideExportLink: false,

        _serviceErrorMessage: "<h3>Could not resolve the geoprocessing service</h3><p>This widget will now close. Please try again later or check the service and/or your settings</p>",

        /*************************************************************************************
        * constructor
        *       Initializes the Print widget with the parameters defined either in the mapSettings file,
        *       or explicitly passed in the instantiation of the widget.
        ***************************************************************************************/
        constructor: function (params, srcNodeRef) {
            // required params
            if (!params.map) {
                throw new Error("map required for " + this.declaredClass);
            }

            this.GPServer = params.GPServer ? params.GPServer : wvsConfig.defaults.services.print ? wvsConfig.defaults.services.print : null;

            if (!this.GPServer) {
                throw new Error("GPServer required for " + this.declaredClass);
            }

            this._onExtentChangeHandler = null;
        },

        /*************************************************************************************
        * postCreate
        *       Life-cycle function that fires when the instance has been fully initialized 
        *       and rendered, but not necessarily included in the DOM. Used here to set values
        *       to the properties and internal widgets for the function, determines whether to
        *       show the overlay for the print area, andattach to the 'Export' button click evt.
        ****************************************************************************************/
        postCreate: function () {
            //Apply uniform styling classes
            this.templates = [];
            domClass.add(this.widthTextbox.domNode, "span1");
            domClass.add(this.heightTextbox.domNode, "span1");

            for (var prop in this.qualityDef) {
                this.basemapQualitySelect.addOption({ label: prop, value: this.qualityDef[prop] });
            }

            //Initialize values
            this.widthTextbox.set("value", this.width);
            this.heightTextbox.set("value", this.height);
            this.enableCustomScaleCheckbox.set("checked", this.useCustomMapScale);
            this.customScaleTextBox.set("disabled", !this.useCustomMapScale);
            this.customScaleTextBox.set("value", Math.ceil(this.map.getScale()));

            this.own(on(this.exportLink, "click", lang.hitch(this, function () {
                this.exportButton.set("iconClass", "");
                this.exportButton.set("disabled", false);
                domStyle.set(this.exportButton.domNode, { display: "inline-block" });
                domStyle.set(this.exportLink, { display: "none" });
                window.open(this._exportLink, '_blank');
            })));

            //Sets the internal flag variable 'overlayEnabled' to the initializer property "showOverlayOnStartup", then
            //sets the 'Show Overlay' checkbox of the UI to correspond to the state of 'overlayEnabled'.
            this.overlayEnabled = this.showOverlayOnStartup;
            this._setOverlayCheckbox();

            this._customScaleCheckboxChange(this.useCustomMapScale);
            this._setDimensionSelection();

            this._placeSelectionExtent(this.width, this.height);

            this.watch("overlayEnabled", lang.hitch(this, function (name, oldVal, newVal) {
                if (!newVal && this._selectionExtent) {
                    this._removeSelectionExtent();
                }
                else if (newVal && this.width && this.height) {
                    this._placeSelectionExtent(this.width, this.height);
                }
                this.showOverlayCheckbox.set('checked', newVal);
            }));
        },

        /************************************************************************************************
        * startup
        *       The function called when the creation of the widget and its children is complete. Used
        *       to request the information about the GeoProcessing server and populate the internal 
        *       'templates' array property with the JSON template definitions returned by the server.
        *************************************************************************************************/
        startup: function () {
            var self = this;

            // set a loading icon just in case
            var loadingSpan = domConstruct.create("span", { "class": "icon-spinner icon-spin" }, this.templateSelect, 'after');
            // Get our templates from the REST endpoint
            var request = new esriRequest({
                url: this.GPServer,
                content: { f: "json" },
                handleAs: "json"
            });

            //Request templates, populate the internal array 'templates', and create dropdown options.
            request.then(
                function (response) {
                    if (response.templates) {
                        var options = [];
                        for (var i = 0; i < response.templates.length; i++) {
                            self.templates.push(response.templates[i]);
                            options.push({ label: response.templates[i].name, value: response.templates[i].name });
                        }
                    }
                    domConstruct.destroy(loadingSpan);
                    self.templateSelect.set("options", options);
                    self.templateSelect.set("value", options[0]);
                },
                function (error) {
                    domConstruct.destroy(loadingSpan);
                    Common.errorDialog(self._serviceErrorMessage)
                        .then(function () { self.destroy(); });

                    throw new Error("Could not resolve the geoprocessing service for " + self.declaredClass);
                }
            );

            this.own(on(this.map, "extent-change", lang.hitch(this, function () {
                if (!this.useCustomMapScale && this._selectionExtent && this.width && this.height) {
                    this._placeSelectionExtent(this.width, this.height);
                }
            })));

            this._placeSelectionExtent(this.width, this.height);
        },

        /******************************************************************************************************
        * _setOverlayCheckbox
        *   Sets the value of the 'Show Overlay' checkbox to 'checked' or 'unchecked' based on the widget
        *   setting 'showOverlayOnStartup'.
        ******************************************************************************************************/
        _setOverlayCheckbox: function () {
            this.showOverlayCheckbox.set('checked', this.overlayEnabled);
        },

        /******************************************************************************************************
        * _toggleOverlay
        *   Toggles whether or not the overlay is displayed based on users changing the value of the 'Show
        *   Overlay' checkbox and the availability of the 'width' and 'height' properties.
        ******************************************************************************************************/
        _toggleOverlay: function () {
            this.overlayEnabled = this.showOverlayCheckbox.get('checked');
            if (this.width && this.height) {
                this._placeSelectionExtent(this.width, this.height);
            } else {
                this._removeSelectionExtent();
            }
        },

        /************************************************************************************************
        * _customScaleCheckboxChange
        *       Event handler for the 'Use Custom Scale' checkbox. Toggles the 'on-extent-change' event
        *       handler as necessary and places a Selection extent (if that is enabled).
        *@param checked : bool
        *       Indicates whether or not the check box is checked.
        *************************************************************************************************/
        _customScaleCheckboxChange: function (checked) {
            this.customScaleTextBox.set("disabled", !checked);
            this.useCustomMapScale = checked;
            if (!checked) {
                if (!this._onExtentChangeHandler) {
                    this._onExtentChangeHandler = on(this.map, "extent-change", lang.hitch(this, this._onExtentChange));
                    this.own(this._onExtentChangeHandler);
                }
                if (this.width && this.height) {
                    this._placeSelectionExtent(this.width, this.height);
                }
            }
            else {
                if (this._onExtentChangeHandler) {
                    this._onExtentChangeHandler.remove();
                    this._onExtentChangeHandler = null;
                }
                this._removeSelectionExtent();
            }
        },

        /************************************************************************************************
        * _setDimensionSelection
        *       Event handler for when a user changes the value of the radio buttons where they can select
        *       whether to define the size of the output or use a template.
        *************************************************************************************************/
        _setDimensionSelection: function () {
            // summary:
            //      toggles between template and size mode based on form values
            if (this.specifySizeRadio.get("checked")) {
                this._onWidthHeightChange();
                domStyle.set(this.sizeOptions, { display: "block" });
                domStyle.set(this.templateOptions, { display: "none" });
                this.createImageExportFormatOptions();
            }
            else {
                this._onTemplateChange();
                domStyle.set(this.sizeOptions, { display: "none" });
                domStyle.set(this.templateOptions, { display: "block" });
                this.createTemplateExportFormatOptions();
            }
        },

        /************************************************************************************************
        * _onExtentChange
        *       Event handler for when the extent of the map has changed. Used to re-place the selection
        *       extent and re-define the boundaries of the area to be exported.
        *@param evt : MapEvent
        *       The 'on-extent-change' event object generated by the map when the extent is changed.
        *************************************************************************************************/
        _onExtentChange: function (evt) {
            if (evt.levelChange) {
                this.customScaleTextBox.set("value", Math.ceil(this.map.getScale()));
            }
        },

        /************************************************************************************************
        * destroy
        *       Lifecycle function overridden to be able to remove the export area graphic from the map
        *       before the widget is destroyed.
        *************************************************************************************************/
        destroy: function () {
            this.map.graphics.remove(this._selectionExtent);
            this.inherited(arguments);
        },

        /***********************************************************************************************
        * createImageExportFormatOptions()
        *   Populates the "Export As:" dropdown list with the supported image formats as defined in the
        *   mapSettings.js file or the default settings passed into the widget.
        ***********************************************************************************************/
        createImageExportFormatOptions: function () {
            this.exportFormatSelect.removeOption(this.exportFormatSelect.getOptions());
            for (var i = 0; i < this.supportedImageFormats.length; i++) {
                this.exportFormatSelect.addOption(this.supportedImageFormats[i]);
            }
        },

        /***********************************************************************************************
        * createTemplateExportFormatOptions()
        *   Populates the "Export As:" dropdown list with the supported image formats as defined for
        *   templates returned by the server for export.
        ***********************************************************************************************/
        createTemplateExportFormatOptions: function () {
            this.exportFormatSelect.removeOption(this.exportFormatSelect.getOptions());
            for (var i = 0; i < this.supportedTemplatedFormats.length; i++) {
                this.exportFormatSelect.addOption(this.supportedTemplatedFormats[i]);
            }
        },

        /************************************************************************************************
        * _onTemplateChange
        *   Handles the 'onChange' event for the template select drop down. Populates the 'textElementsList'
        *   node with input elements that will match the text elements for the selected template, then
        *   emits a 'template-changed' event that passes out the name of the selected template as well as
        *   the array of TextBox nodes that were generated.
        *************************************************************************************************/
        _onTemplateChange: function () {
            var textBoxElements = [];
            if (!this.specifySizeRadio.get("checked")) {
                var curValue = this.templateSelect.get("value"),
                    rowString = "<div class=\"row-fluid\"></div>",
                    labelDivString = "<div class=\"colspan3\"></div>",
                    textBoxDivString = "<div class=\"colspan9\"></div>";
                domConstruct.empty(this.textElementsList);

                //This loop iterates through the templates stored in the instance property "this.templates" to find the
                //properties for the template the user has selected.
                for (var i = 0; i < this.templates.length; i++) {
                    if (curValue === this.templates[i].name) {
                        this._placeSelectionExtent(this.templates[i].mapWidth, this.templates[i].mapHeight);

                        //This loop will go through each text element in the template and generate a label and input field
                        //to allow for user input on each field.
                        for (var j = 0; j < this.templates[i].textElements.length; j++) {
                            var element = this.templates[i].textElements[j];
                            var rowNode = domConstruct.place(rowString, this.textElementsList, "last");
                            var labelNode = domConstruct.place(labelDivString, rowNode, "first");
                            var textBoxNode = domConstruct.place(textBoxDivString, rowNode, "last");

                            //This internal for loop is used to generate human-readable labels with spaced, capitalized
                            //members, instead of a single camel-cased string.
                            var textElem = element;
                            for (var k = 1; k < textElem.length; k++) {
                                if ((textElem.charAt(k) != ' ' && textElem.charAt(k - 1) != ' ')
                                    && (textElem.charAt(k) == textElem.charAt(k).toUpperCase())
                                    && (textElem.charAt(k - 1) == textElem.charAt(k - 1).toLowerCase())) {
                                    textElem = textElem.substring(0, k) + " " + textElem.substring(k);
                                    k--;
                                }
                            }
                            domConstruct.place("<label>" + textElem + ": </label>", labelNode, 'first');
                            this[this.templates[i].textElements[j]] = new TextBox({}, textBoxNode);
                            textBoxElements.push({ name: this.templates[i].textElements[j], node: this[this.templates[i].textElements[j]] });
                        }
                    }
                }
            }
            this.emit('template-changed', { newTemplateName: this.templateSelect.get("value"), textElementNodes: textBoxElements });
        },

        /************************************************************************************************
        * _onWidthHeightChange
        *   Event handler for the 'onKeyUp' event of the Width and Height text boxes.
        *   Sets the selection extent to the new aspect ratio
        *************************************************************************************************/
        _onWidthHeightChange: function () {
            this._placeSelectionExtent(parseFloat(this.widthTextbox.get('displayedValue')), parseFloat(this.heightTextbox.get('displayedValue')));
        },

        /************************************************************************************************
        * _removeSelectionExtent
        *   Removes the export area graphic from the map and sets the "_selectionExtent" property to null
        *************************************************************************************************/
        _removeSelectionExtent: function () {
            if (this._selectionExtent) {
                this.map.graphics.remove(this._selectionExtent);
                this._selectionExtent = null;
            }
        },

        /*******************************************************************************************************
        * _placeSelectionExtent
        *       This function accepts a width and height in uniform units (though the specific units passed in
        *       to the function is actually irrelevant). It determines the width/height ratio and builds a
        *       geometry of equal ratios that is entirely within the visible extent of the map that is then
        *       passed to the print function.
        *@param w
        *       The width of the geometry (generally in inches)
        *@param h
        *       The height of the geometry (also in inches).
        *******************************************************************************************************/
        _placeSelectionExtent: function (w, h) {
            var dpi = this.dpi,
                rawWidth = w,
                rawHeight = h;

            if (isNaN(w) || isNaN(h) || w < 0 || h < 0) {
                return;
            }

            this.width = w;
            this.height = h;
            var centerPt = this.map.toScreen(this.map.extent.getCenter()),
                ext = this.map.extent,
                spatRef = this.map.spatialReference,
                leftX, rightX, bottomY, topY;

            //Determine which side is wider and set the appropriate aspect ratio of the geometry.
            if (w > h) {

                //Get the bounding 'x' values of the extent in terms of screen points.
                leftX = this.map.toScreen(new Point(ext.xmin, ext.ymin, spatRef)).x + dpi;
                rightX = this.map.toScreen(new Point(ext.xmax, ext.ymin, spatRef)).x - dpi;

                //Determine the width in terms of screen points and the height as the value
                //in screen points which preserves the aspect ratio.
                w = rightX - leftX;
                h = this.height * (w / this.width);
            } else {

                //Get the bounding 'y' values of the extent in terms of screen points.
                bottomY = this.map.toScreen(new Point(ext.xmin, ext.ymin, spatRef)).y - dpi;
                topY = this.map.toScreen(new Point(ext.xmin, ext.ymax, spatRef)).y + dpi;

                //Determine the height in terms of screen points and the width as the value
                //in screen points which preserves the aspect ratio.
                h = bottomY - topY;
                w = this.width * (h / this.height);
            }

            //Define the bottom-left and top-right points for the geometry of the overlay
            var blExtentPt = this.map.toMap(new ScreenPoint(centerPt.x - (w / 2), centerPt.y + (h / 2))),
                trExtentPt = this.map.toMap(new ScreenPoint(centerPt.x + (w / 2), centerPt.y - (h / 2)));


            this.width = w = rawWidth;
            this.height = h = rawHeight;

            //If the x-min of the geometry extent is out of bounds of the map extent, then the aspect
            //ratio of the map is such that the aspect ratio of the selected geometry should be scaled down
            if (blExtentPt.x < this.map.extent.xmin) {

                //Get the bounding 'x' values of the extent in terms of screen points.
                leftX = this.map.toScreen(new Point(ext.xmin, ext.ymin, spatRef)).x + dpi;
                rightX = this.map.toScreen(new Point(ext.xmax, ext.ymin, spatRef)).x - dpi;

                //Determine the width in terms of screen points and the height as the value
                //in screen points which preserves the aspect ratio.
                w = rightX - leftX;
                h = this.height * (w / this.width);

                //Redefine the bottom-left and top-right points for the geometry of the overlay such that
                //the overlay is contained entirely within the visible map extent
                blExtentPt = this.map.toMap(new ScreenPoint(centerPt.x - (w / 2), centerPt.y + (h / 2)));
                trExtentPt = this.map.toMap(new ScreenPoint(centerPt.x + (w / 2), centerPt.y - (h / 2)));
            } else if (blExtentPt.y < this.map.extent.ymin) {

                //Get the bounding 'y' values of the extent in terms of screen points.
                bottomY = this.map.toScreen(new Point(ext.xmin, ext.ymin, spatRef)).y - dpi;
                topY = this.map.toScreen(new Point(ext.xmin, ext.ymax, spatRef)).y + dpi;

                //Determine the height in terms of screen points and the width as the value
                //in screen points which preserves the aspect ratio.
                h = bottomY - topY;
                w = this.width * (h / this.height);

                //Redefine the bottom-left and top-right points for the geometry of the overlay such that
                //the overlay is contained entirely within the visible map extent
                blExtentPt = this.map.toMap(new ScreenPoint(centerPt.x - (w / 2), centerPt.y + (h / 2)));
                trExtentPt = this.map.toMap(new ScreenPoint(centerPt.x + (w / 2), centerPt.y - (h / 2)));
            }

            //If the _selectionExtent property is null, then a new Graphic object must be instatiated with the
            //appropriate geometry. Otherwise, the object exists and its geometry only needs to be updated.
            if (!this._selectionExtent) {
                this._selectionExtent = new Graphic();
                this._selectionExtent.setSymbol(symbolJsonUtils.fromJson(wvsConfig.defaults.symbols.selectionPolygon));
                this._selectionExtent.setGeometry(new Extent(blExtentPt.x, blExtentPt.y, trExtentPt.x, trExtentPt.y, blExtentPt.spatialReference));
            } else {
                this._selectionExtent.setGeometry(new Extent(blExtentPt.x, blExtentPt.y, trExtentPt.x, trExtentPt.y, blExtentPt.spatialReference));
                //HACK: Because we know that a _selectionExtent exists, and we don't necessarily know what called the _placeSelectionExtent function,
                //      it is safest just to remove the graphic initially, then add it if and only if the overlayEnabled property is true.
                this.map.graphics.remove(this._selectionExtent);
            }
            if (this.overlayEnabled) {
                this.map.graphics.add(this._selectionExtent);
            }
        },

        /*******************************************************************************************************
        * _startExport
        *   This function will begin the export by defining the url for the specific type of export being
        *   performed (template vs. image), then defining the parameters for the export task. If there is a
        *   feature layer, the export is deferred until the result of a feature query is resolved and the
        *   features can be sent over as graphics (as of now, the SOE does not support FeatureLayer resources)
        ********************************************************************************************************/
        _startExport: function () {
            var self = this,
                params = { f: 'json' },
                targetUrl = this.GPServer,
                mapLayers = this.map.getLayersVisibleAtScale(this.map.getScale());
            
            self.emit('export-start', {});

            this.exportButton.set("iconClass", "icon-spinner icon-spin");
            this.exportButton.set("disabled", true);

            //First, determine what kind of export to execute -- "ExportImage" or a template based export.
            if (this.specifySizeRadio.get("checked")) {
                targetUrl += "/ExportImage";
                params.size = [(this.width * this.dpi), (this.height * this.dpi)];
                params.format = this.exportFormatSelect.get("value");
            } else {
                var templateId;

                for (var i = 0; i < this.templates.length; i++) {
                    if (this.templates[i].name === this.templateSelect.get("value")) {
                        templateId = this.templates[i].id;
                    }
                }
                params.textElements = {};
                for (var i = 0; i < this.templates.length; i++) {
                    if (this.templates[i].name === this.templateSelect.get("value")) {
                        for (var j = 0; j < this.templates[i].textElements.length; j++) {
                            params.textElements[this.templates[i].textElements[j]] = this[this.templates[i].textElements[j]].get("value");
                        }
                    }
                }
                targetUrl += "/Templates/" + templateId + "/Export";
            }

            //Build out the request params with bounding box, spatial reference, scale, format.
            params.bbox = this._selectionExtent.geometry.xmin + ", ";
            params.bbox += this._selectionExtent.geometry.ymin + ", ";
            params.bbox += this._selectionExtent.geometry.xmax + ", ";
            params.bbox += this._selectionExtent.geometry.ymax;
            params.bboxSR = this._selectionExtent.geometry.spatialReference.toJson();
            params.scale = this.map.getScale();
            params.format = this.exportFormatSelect.get("value");
            params.resources = [];
            params.graphics = [];

            //To be used in case there is an unsupported feature layer whose features will need to be represented
            //as a graphics array to print correctly.
            var dfds = [],
                renderers = [];

            //Iterate through all of the layers in the map and build out the 'resource' objects for the export task.
            for (var i = 0; i < mapLayers.length; i++) {
                if (mapLayers[i].url) {
                    var newResource = {
                        name: mapLayers[i].getServiceName().trim(),
                        restUrl: mapLayers[i].url,
                        opacity: mapLayers[i].opacity
                    };
                    if (mapLayers[i].visibleLayers) {
                        for (var j = 0; j < mapLayers[i].layerInfos.length; j++) {
                            if (mapLayers[i].layerInfos[j].visible) {
                                if (!newResource.layers) {
                                    newResource.layers = [];
                                }
                                var subLayerID = mapLayers[i].layerInfos[j].id;
                                if (mapLayers[i].layerDefinitions && mapLayers[i].layerDefinitions[subLayerID]) {
                                    console.log("QueryExpression for layer " + subLayerID + " - " + mapLayers[i].layerDefinitions[subLayerID]);
                                    newResource.layers.push({ id: subLayerID, visible: true, definitionExpression: mapLayers[i].layerDefinitions[subLayerID] });
                                }
                                else {
                                    newResource.layers.push({ id: subLayerID, visible: true });
                                }
                            }
                        }
                    }
                    if (mapLayers[i].declaredClass) {
                        if (mapLayers[i].declaredClass.indexOf("ArcGISDynamicMapServiceLayer") >= 0) {
                            newResource.type = "ArcGISDynamicMapService";
                            newResource.token = null;
                        } else if (mapLayers[i].declaredClass.indexOf("ArcGISTiledMapServiceLayer") >= 0 ||
                            mapLayers[i].declaredClass.indexOf("VETiledLayer") >= 0) {
                            newResource.type = "ArcGISTiledMapService";
                        } else if (mapLayers[i].declaredClass.indexOf("FeatureLayer") >= 0) {
                            dfds.push(this._buildGraphicsArrayFromFeatureLayer(mapLayers[i]));
                            renderers.push(mapLayers[i].renderer);
                            continue;
                        }
                    }
                    params.resources.push(newResource);
                } else if (mapLayers[i].declaredClass && mapLayers[i].declaredClass.indexOf("GraphicsLayer") >= 0) {
                    if (!mapLayers[i].graphics) {
                        continue;
                    }
                    for (var j = 0; j < mapLayers[i].graphics.length; j++) {
                        params.graphics.push(mapLayers[i].graphics[j].toJson());
                    }
                }
            }

            //Sort the 'resources' objects so that the ArcGISDynamicMapService type objects are all in the front
            //of the array so that they are added to the map in the appropriate order for display.
            params.resources.sort(function (a, b) {
                if (a.type == "ArcGISDynamicMapService" && b.type != "ArcGISDynamicMapService") {
                    return -1;
                } else if (a.type != "ArcGISDynamicMapService" && b.type == "ArcGISDynamicMapService") {
                    return 1;
                } else {
                    return 0;
                }
            });

            //If there has was a FeatureLayer, get the features as a graphics array and then
            //send those across in the 'graphics' property of the params object. 
            if (dfds.length > 0) {
                var self = this;
                all(dfds).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        for (var j = 0; j < results[i].features.length; j++) {
                            var gfc = results[i].features[j];
                            if (renderers[i].symbol) {
                                gfc.symbol = renderers[i].symbol;
                            }
                            params.graphics.push(gfc);
                        }
                    }
                    params.graphics = self._getGraphicsForMapExport(params.graphics);
                    self._executeExport(params, targetUrl);
                }, function (err) {
                    Common.errorDialog("There was a problem processing your feature layers, they will not be available in your map export.", "Export Error");
                    self._executeExport(params, targetUrl);
                });
            } else {
                this._executeExport(params, targetUrl);
            }
        },

        /*******************************************************************************************************
        * _executeExport
        *   Called after all of the parameter manipulation and setting has been performed by the 'startExecute'
        *   function. Runs an 'esriRequest' task to the GPServer url and returns the result.
        *@param params : Object
        *   The POST parameters to be passed out with the esriRequest.
        *@param targetUrl: String
        *   The url for the GeoProcessing server.
        ********************************************************************************************************/
        _executeExport: function (params, targetUrl) {
            var self = this;
            for (var prop in params) {
                if (typeof (params[prop]) == "object") {
                    params[prop] = JSON.stringify(params[prop]);
                }
            }
            esriRequest({
                content: params,
                url: targetUrl
            }, {
                usePost: true
            }).then(function (response) {
                var urlRoot = self.GPServer;
                if (self.GPServer.indexOf('/rest') >= 0) {
                    urlRoot = self.GPServer.substring(0, self.GPServer.indexOf('/rest'));
                }
                self._exportLink = urlRoot + response.url;
                if (self.provideExportLink) {
                    domStyle.set(self.exportButton.domNode, { display: "none" });
                    domStyle.set(self.exportLink, { display: "inline" });
                } else {
                    self.exportButton.set("iconClass", "");
                    self.exportButton.set("disabled", false);
                }
                response.url = self._exportLink;
                self.emit("export-complete", response);
                // TODO: Uncomment this line when the esri bug is gone
                //if (self.replaceMap.length || typeof customScale === "number")
            }, function (error) {
                self.emit("export-error", error);
            });
        },

        /*******************************************************************************************************
        * _buildGraphicsArrayFromFeatureLayer
        *   Returns the deferred result of a queryFeatures task executed on the FeatureLayer.
        *@param fLayer: FeatureLayer
        *   The FeatureLayer whose features will be returned as an array of Graphics objects.
        ********************************************************************************************************/
        _buildGraphicsArrayFromFeatureLayer: function (fLayer) {
            var qry = new Query();
            qry.geometry = this._selectionExtent.geometry;
            qry.where = fLayer.getDefinitionExpression();
            return fLayer.queryFeatures(qry);
        },

        /*******************************************************************************************************
        * _getGraphicsForMapExport
        *   Takes an array of esri-defined Graphics objects and converts them into a set of graphics that can
        *   be understood and properly interpreted by the PrintSOE.
        *@param esriGraphics: Array
        *   The array of esri Graphics objects to be converted prior to sending across to the PrintSOE
        ********************************************************************************************************/
        _getGraphicsForMapExport: function (esriGraphics) {

            //graphics (optional)
            var graphics = "[";
            var graphicsCount = 0;

            if (!esriGraphics.length) {
                return [];
            }

            for (var i = 0; i < esriGraphics.length; i++) {
                var graphic = esriGraphics[i];

                graphicsCount++;
                if (graphicsCount > 1) {
                    graphics += ",";
                }
                graphics += "{";
                graphics += "\"name\":\"feature\"";

                var geometry = JSON.stringify(graphic.geometry);

                graphics += ",\"geometry\": " + geometry;

                var style = "{";

                if (graphic.geometry.type == "point" || graphic.geometry.type == "multipoint") {

                    // HACK: Print service only supports icon for point symbology
                    // so until the print service can handle vector markup for points
                    // all points on map will have same icon in export
                    if (graphic.symbol instanceof esri.symbol.PictureMarkerSymbol) {

                        style += "\"iconStyle\":{";

                        //style += "\"url\":\"" + graphic.symbol.url + "\",";
                        style += "\"url\":\"http://files.worldviewsolutions.net/outgoing/public/wvs/images/blue.png\",";
                        //style += "\"url\":\"" + url.content("images/red_push_pin.png") + "\",";
                        style += "\"offsetX\":\"" + graphic.symbol.xoffset + "\",";
                        style += "\"offsetY\":\"" + graphic.symbol.yoffset + "\"";
                        style += "}"
                    }

                } else if (graphic.geometry.type == "polyline") {

                    var lineStyle = "solid";
                    switch (graphic.symbol.style) {
                        case "dash":
                            lineStyle = "longdash";
                            break;
                        case "dashdot":
                        case "dashdotdot":
                            lineStyle = "dashdot";
                            break;
                        case "dot":
                            lineStyle = "dot";
                            break;
                        case "null":
                            lineStyle = "solid";
                            break;
                        default:
                            lineStyle = "solid";
                            break;
                    }

                    style += "\"lineType\":\"" + lineStyle + "\",";
                    style += "\"lineWidth\":\"" + graphic.symbol.width + "\",";

                    // returns the Array [255,0,0,1]
                    var rgbaColor = graphic.symbol.color.toRgba();
                    var opacity = 1;

                    if (rgbaColor.length == 4) {
                        opacity = rgbs[3];
                    }

                    // returns the String "#ff0000"
                    var hexColor = graphic.symbol.color.toHex();

                    style += "\"lineColor\":\"" + hexColor + "\",";
                    style += "\"lineOpacity\":\"" + opacity + "\"";

                } else if (graphic.geometry.type == "polygon") {

                    var lineStyle = "solid";
                    switch (graphic.symbol.outline.style) {
                        case "dash":
                            lineStyle = "longdash";
                            break;
                        case "dashdot":
                        case "dashdotdot":
                            lineStyle = "dashdot";
                            break;
                        case "dot":
                            lineStyle = "dot";
                            break;
                        case "null":
                            lineStyle = "solid";
                            break;
                        default:
                            lineStyle = "solid";
                            break;
                    }

                    style += "\"lineType\":\"" + lineStyle + "\",";
                    style += "\"lineWidth\":\"" + graphic.symbol.outline.width + "\",";

                    // returns the Array [255,0,0,1]
                    var rgbaColor = graphic.symbol.outline.color.toRgba();
                    var opacity = 1;

                    if (rgbaColor.length == 4) {
                        opacity = rgbaColor[3];
                    }

                    // returns the String "#ff0000"
                    var hexColor = graphic.symbol.outline.color.toHex();

                    style += "\"lineColor\":\"" + hexColor + "\",";
                    style += "\"lineOpacity\":\"" + opacity + "\",";

                    // returns the Array [255,0,0,1]
                    var fillRgbaColor = graphic.symbol.color.toRgba();
                    var fillOpacity = 1;

                    if (fillRgbaColor.length == 4) {
                        fillOpacity = fillRgbaColor[3];
                    }

                    // returns the String "#ff0000"
                    var fillHexColor = graphic.symbol.color.toHex();


                    style += "\"fillColor\":\"" + fillHexColor + "\",";
                    style += "\"fillOpacity\":\"" + fillOpacity + "\"";

                }


                style += "}";
                graphics += ",\"style\":" + style;


                graphics += "}";
            }

            graphics += "]";

            console.log("Graphics:");
            console.log(graphics);

            return JSON.parse(graphics);
        }
    });
});