console.log("wvs/dijits/Toc.js");

define([
    "dojo/_base/declare"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dojo/text!./templates/Toc.html"
    , "dojo/_base/lang"
    , "dojo/on"
    , "dojo/dom-style"
    , "dojo/dom-construct"
    , "dojo/_base/array"
    , "esri/layers/FeatureLayer"
    , "esri/layers/StreamLayer"
    , "esri/layers/ArcGISImageServiceLayer"
    , "esri/layers/ArcGISDynamicMapServiceLayer"
    , "esri/layers/GeoRSSLayer"
    , "esri/layers/GraphicsLayer"
    , "../extensions/esri/layers/ArcGISWebMapLayer"
    , "./TocMapServiceNode"
    , "./TocImageServiceNode"
    , "./TocFeatureLayerNode"
    , "./TocStreamLayerNode"
    , "./TocMapServiceLayerNode"
    , "./TocGroupLayerNode"
    , "./TocLegendNode"
    , "./TocWebMapNode"
    , "./TocMarkupLayerNode"
    , "./TocGraphicsLayerNode"
    , "dojo/dom-class"
    , "dojo/dom-geometry"
    , "../extensions/esri/layers/Layer"
    , "../extensions/esri/layers/ArcGISTiledMapServiceLayer"
    , "../extensions/esri/layers/ArcGISDynamicMapServiceLayer"
    , "../extensions/esri/layers/LayerInfo"
], function (declare, _WidgetBase, _TemplatedMixin, template, lang, on, domStyle, domConstruct, array, FeatureLayer, StreamLayer, ArcGISImageServiceLayer, ArcGISDynamicMapServiceLayer, GeoRSSLayer, GraphicsLayer, ArcGISWebMapLayer, TocMapServiceNode, TocImageServiceNode, TocFeatureLayerNode, TocStreamLayerNode, TocMapServiceLayerNode, TocGroupLayerNode, TocLegendNode, TocWebMapNode, TocMarkupLayerNode, TocGraphicsLayerNode, domClass, domGeom) {
    return declare('wvs.dijits.Toc', [_WidgetBase, _TemplatedMixin], {
        zoomTimer: null,

        templateString: template,

        // refreshDelay: Integer
        //      Number of milliseconds to delay between clicking a checkbox and refreshing the map service. A longer delay allow the user to check/uncheck multiple
        //      layers before refreshing the map service.
        refreshDelay: 750,

        // showTiledServices: Boolean
        //      Include tiled (cached) services in the Toc
        showTiledServices: false,

        // useSpriteImage: Boolean
        //      Use the custom sprite image for the legend icons. When false, the "Legend" REST service will be used for 10.01 and newer services.
        useSpriteImage: true,

        //  enableImageServiceLayers: Boolean
        //      Include image service layers in the Toc
        enableImageServiceLayers: true,

        //  enableFeatureLayers: Boolean
        //      Include feature layers in the Toc
        enableFeatureLayers: true,

        //  enableGeoRSSLayers: Boolean
        //      Include GeoRSS layers in the Toc
        enableGeoRSSLayers: true,

        //  enableKmlLayers: Boolean
        //      Include KML layers in the Toc
        enableKmlLayers: true,

        enableArcGISWebMaps: true,

        //  enableMarkupLayers: Boolean
        //      Include Markup layers in the Toc
        enableMarkupLayers: true,

        enableFeatureLayerFilters: true,

        //just to simplify calls from child nodes
        //todo: revisit
        getVisible: function () {
            return true;
        },

        constructor: function(/*Object?*/params, /*DomNode|String?*/srcNodeRef) {
            console.log(this.declaredClass + ' constructor');
            this.inherited(arguments);

            // mixin params
            params = params || {};
            if (!params.map) {
                throw new Error('Map not defined in params for Toc');
            }
            

            lang.mixin(this, params);

            this.nodes = [];
        },

        postCreate: function() {
            domClass.add(this.domNode, "jstoolkit-toc");

            // clear contents of the node
            domConstruct.empty(this.GraphicsLayerNodes);
            domConstruct.empty(this.OtherLayersNodes);

            // construct TOC from existing layers
            var layerIds = this.map.layerIds,
                graphicsLayerIds = this.map.graphicsLayerIds;

            array.forEach(layerIds, function (layerId) {
                this._addLayerNode({ layer: this.map.getLayer(layerId) });
            }, this);

            array.forEach(graphicsLayerIds, function (graphicsLayerId) {
                this._addLayerNode({ layer: this.map.getLayer(graphicsLayerId) });
            }, this);


            // connect to the map events
            this.own(
                on(this.map, "layer-add", lang.hitch(this, this._addLayerNode))
                , on(this.map, "layer-remove", lang.hitch(this, this._removeServiceLayerFromEvent))
                , on(this.map, "zoom-end", lang.hitch(this, this._checkOnZoomEnd))
                , on(this.map, "layers-reordered", lang.hitch(this, this._onLayersReordered))
            );
        },
        startup: function () {
            this.inherited(arguments);
            this.resize();
            setTimeout(lang.hitch(this, function () {
                this._onZoomEnd();
            }), 500);
            // Emit the 'load' event which signifies the TOC has finished loading
            this.emit("load", this);
        },
        addNode: function (node) {
            console.log(this.declaredClass + ' addNode');
            var self = this,
                scale = this.map.getScale(),
                tocDiv = this._isEsriGraphicsLayerType(node.mapService) ? this.GraphicsLayerNodes : this.OtherLayersNodes;
            node.placeAt(tocDiv);
            this.nodes.push(node);
            node.startup();

            //removed by aric.  i added this to the end of each node's postCreate.
            //setTimeout(function () { node._adjustToMapScale(scale); }, 1000);
            this._onLayersReordered({ layerIds: this.map.layerIds, target: this.map });

            // Emits the node-add event
            this.emit("node-add", node);
        },
        resize: function (width, height) {
            console.log(this.declaredClass + ' resize');
            // if width or height is undefined then use the parent nodes size
            if (!width || !height || width < 0 || height < 0) {
                var box = domGeom.getContentBox(this.domNode.parentNode);
                width = box.w;
                height = box.h;
            }
            if (!width || !height || width < 0 || height < 0)
                return;
            this.domNode.style.width = width + "px";
            this.domNode.style.height = height + "px";
        },
        isTopNode: function (node) {
            console.log(this.declaredClass + ' isTopNode');
            var layer = node.mapService,
                nodePos = this.map.getLayerPosition(layer).pos;
            return nodePos === "top" || nodePos === "alone";
        },
        isBottomNode: function (node) {
            console.log(this.declaredClass + ' isBottomNode');
            var layer = node.mapService,
                nodePos = this.map.getLayerPosition(layer).pos;
            return nodePos === "bottom" || nodePos === "alone";
        },
        // Returns the index of a given layer in the TOC
        _indexOfLayer: function (layer) {
            console.log(this.declaredClass + ' _indexOfLayer');
            for (var i = 0, len = this.nodes.length; i < len; i++) {
                var tocLayer = this.nodes[i].mapService;
                if (tocLayer && ( tocLayer === layer || ( tocLayer.url && tocLayer.url === layer.url) )) {
                    return i;
                }
            }
            return -1;
        },
        // Returns a boolean indicating whether a given layer exists (based on the URL of the layer)
        _layerExists: function (layer) {
            console.log(this.declaredClass + ' _layerExists');
            return this._indexOfLayer(layer) === -1 ? false : true;
        },
        _removeServiceLayerByID: function (id) {
            console.log(this.declaredClass + ' _removeServiceLayerByID');
            var node = this.nodes[id];

            // Remove DOM node from nodes array
            this.nodes.splice(id, 1);

            // Remove layer from map
            this.map.removeLayer(node.mapService);

            // Destroy the DOM node and its children
            node.destroy();
            //domConstruct.destroy(node.tocNode); Removed 4/16/2014
        },
        _moveNodeUp: function (node) {
            console.log(this.declaredClass + ' _moveNodeUp');
            this.map.moveLayerUp(node.mapService);
        },
        _moveNodeDown: function (node) {
            console.log(this.declaredClass + ' _moveNodeDown');
            this.map.moveLayerDown(node.mapService);
        },
        _moveNodeToTop: function (node) {
            while (!this.isTopNode(node)) {
                this.map.moveLayerUp(node.mapService);
            }
        },
        _moveNodeToBottom: function (node) {
            while (!this.isBottomNode(node)) {
                this.map.moveLayerDown(node.mapService);
            }
        },
        _getLayerPosition: function(node, reverse){
            console.log(this.declaredClass + ' _getLayerPosition');
            var self = this,
                index = -1,
                layer = node.mapService,
                layerIds;
        },
        // Map Event Handlers //
        _removeServiceLayer: function (layer) {
            console.log(this.declaredClass + ' _removeServiceLayer');
            var layerIndex = this._indexOfLayer(layer);

            if (layerIndex !== -1) {
                this._removeServiceLayerByID(layerIndex);
            }
        },
        _removeServiceLayerFromEvent: function(evt){
            this._removeServiceLayer(evt.layer);
        },
        _addLayerNode: function (evt) {
            console.log(this.declaredClass + ' _addLayer');
            var layer = evt.layer,
                node;

            if (!this._layerExists(layer) && !layer._webmap && !layer._hidden) {
                if (!layer.loaded) {
                    on.once(layer, "load", lang.hitch(this, function () { this._addLayerNode({ layer: layer }); }));
                    return;
                }
                if (layer.isInstanceOf(ArcGISDynamicMapServiceLayer)) {
                    if (this.showTiledServices === false && layer._basemapGalleryLayerType && layer._basemapGalleryLayerType === "basemap") {
                        return;
                    }
                    node = new TocMapServiceNode({
                        mapService: layer,
                        refreshDelay: this.refreshDelay,
                        useSpriteImage: this.useSpriteImage,
                        _parent: this
                    });
                    this.addNode(node);
                }
                else if (this.enableImageServiceLayers && layer.isInstanceOf(ArcGISImageServiceLayer)) {
                    node = new TocImageServiceNode({
                        mapService: layer,
                        refreshDelay: this.refreshDelay,
                        useSpriteImage: this.useSpriteImage,
                        _parent: this,
                        toc: this
                    });
                    this.addNode(node);
                }
                // TODO: Examine later if this is adequate. For example, a markup tool could easily be a Feature Layer created for client-side editing.
                // Only add feature layers with urls to the TOC.
                // I.E, a GeoRSS layer will add a feature layer for every geometry type found in the feed. These feature layers have no URLs.
                else if (this.enableFeatureLayers && (layer.isInstanceOf(FeatureLayer) && (layer.url || layer._visibleInToc))) {
                    if (layer.socket || layer.socketUrl) {
                        node = new TocStreamLayerNode({
                            mapService: layer,
                            featureLayerType: 'streamLayer',
                            refreshDelay: this.refreshDelay,
                            useSpriteImage: this.useSpriteImage,
                            toc: this,
                            filtersEnabled: false,
                            _parent: this
                        });
                    } else {
                        node = new TocFeatureLayerNode({
                            mapService: layer,
                            featureLayerType: 'featureLayer',
                            refreshDelay: this.refreshDelay,
                            useSpriteImage: this.useSpriteImage,
                            toc: this,
                            filtersEnabled: this.enableFeatureLayerFilters,
                            _parent: this
                        });
                    }
                    this.addNode(node);
                }
                else if (!layer.isInstanceOf(FeatureLayer) && !layer._markupLayer && layer.isInstanceOf(GraphicsLayer) && layer._visibleInToc !== false) {
                    node = new TocGraphicsLayerNode({
                        mapService: layer,
                        refreshDelay: this.refreshDelay,
                        useSpriteImage: this.useSpriteImage,
                        _parent: this,
                        map: map
                    });
                    this.addNode(node);
                }
                else if ((this.enableGeoRSSLayers && layer.declaredClass === "esri.layers.GeoRSSLayer") || (this.enableKmlLayers && layer.declaredClass === "esri.layers.KMLLayer") || (layer.declaredClass === "wvs.layers.GroupLayer")) {
                    node = new TocGroupLayerNode({
                        mapService: layer,
                        refreshDelay: this.refreshDelay,
                        useSpriteImage: this.useSpriteImage,
                        toc: this
                    });
                    var subLayers = layer.declaredClass === "esri.layers.GeoRSSLayer" ? layer.getFeatureLayers() : layer.getLayers();
                    this.map.groupLayers = this.map.groupLayers.concat(array.map(subLayers, function (fl) { return fl.id; }));
                    this.addNode(node);
                }
                else if (this.enableArcGISWebMaps && layer.isInstanceOf(ArcGISWebMapLayer)) {
                    node = new TocWebMapNode({
                        mapService: layer,
                        refreshDelay: this.refreshDelay,
                        useSpriteImage: this.useSpriteImage,
                        _toc: this,
                        _parent: this
                    });
                    this.addNode(node);
                }
                else if (this.enableMarkupLayers && layer.declaredClass === "wvs.layers.MarkupLayer") {
                    node = new TocMarkupLayerNode({
                        mapService: layer,
                        _toc: this,
                        _parent: this,
                        map: this.map
                    });
                    this.addNode(node);
                }
                else {
                    //throw new Error(layer.declaredClass + ' is not a supported TOC layer type');
                }
            }
        },
        //uses a timer to prevent rapid firing of the actual _zoomEnd
        _checkOnZoomEnd: function () {
            if (this.zoomTimer != null) {
                clearTimeout(this.zoomTimer);
            }

            var me = this;
            this.zoomTimer = setTimeout(function () { me._onZoomEnd(); }, 750);
        },
        _onZoomEnd: function () {
            this.zoomTimer = null;

            console.log(this.declaredClass + ' _onZoomEnd');
            var scale = this.map.getScale();
            for (var i = 0; i < this.nodes.length; i++) {
                this.nodes[i]._adjustToMapScale(scale);
            }
        },
        _onLayersReordered: function (layerIds) {
            console.log(this.declaredClass + ' _onLayersReordered');
            var nodes = this.nodes;
            for (var i = 0, len = nodes.length; i < len; i++) {
                var node = nodes[i],
                    layer = node.mapService,
                    layerPos = this.map.getLayerPosition(layer, true);
                node.placeAt(this._isEsriGraphicsLayerType(layer) ? this.GraphicsLayerNodes : this.OtherLayersNodes, layerPos.index);
            }

            // Emit/Create layer-reorder event
            this.emit("layer-reorder", this.nodes);
        },
        _isEsriGraphicsLayerType: function (layer) {
            return layer.isInstanceOf(FeatureLayer) || layer.isGroupLayer() || layer.isInstanceOf(GraphicsLayer);
        }

    });
});