﻿define([
	"dojo/_base/lang",
    "dojo/_base/declare",
    "dojo/_base/array",
    "dojo/on",
	"dojo/dom",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dijit/registry",
    "dijit/focus",
    "dijit/layout/BorderContainer",
    "dojox/layout/ToggleSplitter",
    "dijit/_TemplatedMixin",
    "dojo/text!./templates/MapViewer.html",
    "esri/map",
    "esri/geometry/Extent",
    "esri/dijit/Scalebar",
    "esri/dijit/OverviewMap",
    "esri/dijit/HomeButton",
    "./Swipe",
    "esri/dijit/BasemapGallery",
	"esri/layers/ArcGISTiledMapServiceLayer",
	"esri/layers/ArcGISDynamicMapServiceLayer",
	"esri/layers/FeatureLayer",
    "esri/layers/GraphicsLayer",
    "esri/layers/StreamLayer",
    "esri/layers/GeoRSSLayer",
    "esri/layers/ArcGISImageServiceLayer",
    "esri/virtualearth/VETiledLayer",
    "./ResultsConsole",
    "./CoordinateDisplay",
    "./Toc",
    "./Filter/AttributeSearch",
    "./ZoomToXY",
    "./RoutingTool",
    "./Identify",
    "./Markup",
    "./MapLink",
    "../extensions/esri/layers/MarkupLayer",
    "./ToolContainer",
    "../common/SearchResultsStore",
    "dijit/layout/TabContainer",
    "dijit/layout/ContentPane",
    "dijit/Menu",
    "dijit/DropDownMenu",
	"dijit/MenuBar",
	"dijit/MenuItem",
    "dijit/PopupMenuBarItem",
	"dijit/PopupMenuItem",
    "dijit/MenuBarItem",
    "dijit/MenuSeparator",
	"dijit/TitlePane",
    "esri/dijit/Basemap",
    "esri/dijit/BasemapLayer",
    "require",
    "esri/dijit/Popup",
    "dojo/aspect",
    "dojo/dom-style",
    "./MapManager",
    "dijit/Dialog",
    "dojo/window",
    "../_base/config",
    "esri/config",
    "dojo/string",
    "./Print",
    "../common/Common",
    "../common/Bookmark",
    "../common/MarkupManager",
    "dojo/query",
    "dojo/io-query",
    "dojo/has",
    "esri/graphic",
    "dojo/_base/unload",
    "../common/InfoTemplateManager",
    "../common/MapState",
    "./BookmarkManager",
    "esri/symbols/jsonUtils",
    "esri/symbols/SimpleFillSymbol",
    "esri/symbols/SimpleLineSymbol",
    "esri/Color",
    "esri/renderers/SimpleRenderer",
    "esri/geometry/Point",
    "esri/geometry/Multipoint",
    "esri/geometry/Polyline",
    "esri/geometry/Polygon",
    "esri/SpatialReference",
    "./SimpleSearch",
    "dojo/_base/url",
    "./Azimuth",
    "./Accordion",
    "esri/IdentityManager",
    "esri/urlUtils",
    "../extensions/esri/layers/ArcGISWebMapLayer",
    "esri/request",
    "dojo/_base/connect",
    "wvs/dijits/SimpleButton",
    "./BasemapDropdown",
    "wvs/extensions/esri/layers/GoogleMapsLayer",
    "esri/tasks/GeometryService",
    "esri/tasks/QueryTask",
    "esri/tasks/query",
    "wvs/common/SearchResultsAdapter",
    "dojo/Evented",
    "esri/graphicsUtils",
    "esri/layers/layer",
    "wvs/dijits/ViewIn",
    "dojo/fx",
    "dojo/store/Memory",
    // Extensions/Plug-ins (these are behavioral; they do not import into anything)
    "../extensions/esri/Map",
    "../extensions/esri/layers/ArcGISDynamicMapServiceLayer",
    "../extensions/esri/layers/LayerInfo",
    "../extensions/dojo/_base/lang",
    'dojo/domReady!'
], function (
	lang,
    declare,
    array,
	on,
    dom,
    domClass,
    domConstruct,
    registry,
    focusUtil,
    BorderContainer,
    ToggleSplitter,
    _TemplatedMixin,
    template,
    Map,
    Extent,
    Scalebar,
    OverviewMap,
    HomeButton,
    Swipe,
    BasemapGallery,
	ArcGISTiledMapServiceLayer,
	ArcGISDynamicMapServiceLayer,
    FeatureLayer,
    GraphicsLayer,
    StreamLayer,
    GeoRSSLayer,
    ArcGISImageServiceLayer,
    VETiledLayer,
    ResultsConsole,
	CoordinateDisplay,
    Toc,
    AttributeSearch,
    ZoomToXY,
    RoutingTool,
    Identify,
    Markup,
    MapLink,
    MarkupLayer,
    ToolContainer,
    SearchResultsStore,
    TabContainer,
    ContentPane,
    Menu,
    DropDownMenu,
    MenuBar,
    MenuItem,
    PopupMenuBarItem,
    PopupMenuItem,
    MenuBarItem,
    MenuSeparator,
    TitlePane,
    Basemap,
    BasemapLayer,
    require,
    Popup,
    aspect,
    domStyle,
    MapManager,
    Dialog,
    win,
    wvsConfig,
    esriConfig,
    string,
    Print,
    Common,
    Bookmark,
    MarkupManager,
    query,
    ioQuery,
    has,
    Graphic,
    baseUnload,
    InfoTemplateManager,
    MapState,
    BookmarkManager,
    symbolJsonUtils,
    SimpleFillSymbol,
    SimpleLineSymbol,
    Color,
    SimpleRenderer,
    Point,
    Multipoint,
    Polyline,
    Polygon,
    SpatialReference,
    SimpleSearch,
    url,
    Azimuth,
    Accordion,
    IdentityManager,
    urlUtils,
    ArcGISWebMapLayer,
    esriRequest,
    connect,
    SimpleButton,
    BasemapDropdown,
    GoogleMapsLayer,
    GeometryService,
    QueryTask,
    Query,
    SearchResultsAdapter,
    Evented,
    graphicsUtils,
    Layer,
    ViewIn
) {
    var MapViewer = declare([BorderContainer, _TemplatedMixin, Evented], {

        templateString: template,

        constructor: function (params, srcNodeRef) {

            has.add("local-storage", function (global, document, anElement) {
                if (global.localStorage) {
                    return true;
                }
                else {
                    return false;
                }
            });

            this.settings = {};
            this.design = 'headline';
            this._splitterClass = ToggleSplitter;
            this.fullscreen = false;
            this._mapEvents = [];
            this._subscriptions = [];

            this.domNodeToComponentMappings = {
                bottomMapRegion: ["coordinateDisplay", "scaleBar"],
                basemapGalleryContainer: ["basemapGallery"],
                bottomPane: ["resultsConsole"],
                leftRegion: ["toc"],
                mapRegionMenuBar: ["mapToolbar"]
            };

            // setting should be passed in, do a null check
            var defaultSettings = {
                proxyUrl: "",
                alwaysUseProxy: false,
                // TODO: potentially rename this property from global
                // some options may affect more than one widget (like basemaps affecting the gallery and the map)
                globalOptions: {
                    basemaps: [],
                    layers: [],
                    layout: {
                        embedded: false,
                        header: {
                            visible: false,
                            content: []
                        },
                        minimized: {
                            headerRegion: true,
                            leftRegion: true,
                            bottomPane: true,
                            mapToolbar: true
                        },
                        maximized: {
                            headerRegion: false,
                            leftRegion: false,
                            bottomPane: false,
                            mapToolbar: false
                        }
                    },
                    proxyReplacements: [],
                    proxyRules: [
                        //{ proxyUrl: "", urlPrefix: "" }
                    ],
                    mapEvents: [
                        // Example: 
                        //{
                        //    ctr: eventConstructor,
                        //    options: {

                        //    }
                        //}
                    ],

                    autoSave: {
                        // if we have a save endpoint available (currently, only local storage if browser supports it,) then auto-save
                        enabled: false,
                        // auto-save when one of these topics is published; an empty array is equivalent to disabling auto-save
                        topics: ["map/extent/change", "map/basemap/change", "map/layer/add", "map/layer/remove", "markup/item/modify", "markup/item/add", "markup/item/remove", "markup/item/move", "markup/item/copy"]
                    },
                    showLoadingOverlay: true,
                    loadingOverlayImage: require.toUrl("./images/WVSLogo-Load.gif")
                },
                map: {
                    widgetOptions: {
                        logo: false,
                        sliderStyle: 'large',
                        slider: true,
                        basemap: "topo",
                        sliderPosition: "top-right"
                    }
                },
                mapToolbar: {
                    visible: true
                },
                bookmarks: {
                    visible: true,
                    menuTitle: "Bookmarks",
                    widgetTitle: "Bookmark Manager",
                    dependencies: ["mapToolbar"]
                },
                viewIn: {
                    visible: true,
                    dependencies: ["mapToolbar"]
                },
                profile: {
                    visible: false,
                    menuTitle: "Profile",
                    widgetTitle: "Profile",
                    widgetOptions: {}
                },
                exportTool: {
                    visible: true,
                    widgetOptions: {},
                    menuTitle: "Print",
                    widgetTitle: "Export Map & Print",
                    dependencies: ["mapToolbar"]
                },
                basemapGallery: {
                    visible: true,
                    widgetOptions: {
                        // If this is set to true, the basemap gallery is automatically loaded by esri (recommended to avoid for now for more control)
                        showArcGISBasemaps: true
                    },
                    esriBasemaps: [
                        {
                            options: {
                                thumbnailUrl: require.toUrl("./images/light_gray_canvas_thumbnail.png"),
                                title: "Light Gray Canvas"
                            },
                            layers: [
                                {
                                    url: "https://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer"
                                },
                                {
                                    url: "https://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Reference/MapServer"
                                }
                            ]
                        },
                        {
                            options: {
                                thumbnailUrl: require.toUrl("./images/imagery_thumbnail.jpg"),
                                title: "Imagery"
                            },
                            layers: [
                                {
                                    url: "https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer"
                                }
                            ]
                        },
                        {
                            options: {
                                thumbnailUrl: require.toUrl("./images/imagery_labels_thumbnail.png"),
                                title: "Imagery with Labels"
                            },
                            layers: [
                                {
                                    url: "https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer"
                                },
                                {
                                    url: "https://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places/MapServer"
                                }
                            ]
                        },
                        {
                            options: {
                                thumbnailUrl: require.toUrl("./images/steets_thumbnail.jpg"),
                                title: "Streets"
                            },
                            layers: [
                                {
                                    url: "https://services.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer"
                                }
                            ]
                        },
                        {
                            options: {
                                thumbnailUrl: require.toUrl("./images/topo_thumbnail.jpg"),
                                title: "Topographic"
                            },
                            layers: [
                                {
                                    url: "https://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer"
                                }
                            ]
                        },
                        {
                            options: {
                                thumbnailUrl: require.toUrl("./images/terrain_labels.png"),
                                title: "Terrain with Labels"
                            },
                            layers: [
                                {
                                    url: "https://services.arcgisonline.com/ArcGIS/rest/services/World_Terrain_Base/MapServer"
                                },
                                {
                                    url: "https://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Reference_Overlay/MapServer"
                                }
                            ]
                        },
                        {
                            options: {
                                thumbnailUrl: require.toUrl("./images/natgeo_thumbnail.jpg"),
                                title: "National Geographic"
                            },
                            layers: [
                                {
                                    url: "https://services.arcgisonline.com/ArcGIS/rest/services/NatGeo_World_Map/MapServer"
                                }
                            ]
                        },
                        {
                            options: {
                                thumbnailUrl: require.toUrl("./images/oceans_thumbnail.jpg"),
                                title: "Oceans"
                            },
                            layers: [
                                {
                                    url: "https://services.arcgisonline.com/ArcGIS/rest/services/Ocean_Basemap/MapServer"
                                }
                            ]
                        }
                        // TODO: Add in OpenStreetMaps
                    ]
                },
                identify: {
                    visible: true,
                    widgetOptions: {
                        mapServiceLabelText: "Map Services",
                        subLayerLabelText: "Sub-Layer"
                    },
                    menuTitle: "Identify",
                    widgetTitle: "Identify",
                    dependencies: ["mapToolbar"]
                },
                toc: {
                    visible: true,
                    widgetOptions: {
                        useSpriteImage: false,
                        // Support Feature Layers
                        enableFeatureLayers: true,
                        // Support ImageService Layers
                        enableImageServiceLayers: true,
                        // Support GeoRSS Layers
                        enableGeoRSSLayers: true,
                        showTiledServices: false
                    },
                    menuTitle: "Map Layers",
                    widgetTitle: "Map Layers"
                },
                mapManager: {
                    visible: true,
                    widgetOptions: {

                    },
                    menuTitle: "Map Manager",
                    widgetTitle: "Map Manager"
                },
                azimuth: {
                    visible: true,
                    widgetOptions: {},
                    menuTitle: "Azimuth",
                    widgetTitle: "Azimuth Tool",
                    dependencies: ["mapToolbar"]
                },
                markup: {
                    visible: true,
                    widgetOptions: {

                    },
                    menuTitle: "Markup",
                    widgetTitle: "Markup",
                    dependencies: ["mapToolbar"]
                },
                attributeSearch: {
                    visible: true,
                    widgetOptions: {
                        enableSaveAsQuickSearch: true
                    },
                    menuTitle: "Attribute Search",
                    widgetTitle: "Attribute Search",
                    dependencies: ["mapToolbar"]
                },
                measure: {
                    visible: true,
                    widgetOptions: {

                    },
                    menuTitle: "Measure",
                    widgetTitle: "Measure",
                    dependencies: ["mapToolbar"]
                },
                swipe: {
                    visible: true,
                    widgetOptions: {},
                    menuTitle: "Swipe",
                    widgetTitle: "Swipe",
                    dependencies: ["mapToolbar"]
                },
                zoomToXY: {
                    visible: true,
                    widgetOptions: {},
                    menuTitle: "Zoom To XY",
                    widgetTitle: "Zoom To XY",
                    dependencies: ["mapToolbar"]
                },
                routingTool: {
                    visible: true,
                    widgetOptions: {},
                    menuTitle: "Routing Tool",
                    widgetTitle: "Routing Tool",
                    dependencies: ["mapToolbar"]
                },
                coordinateDisplay: {
                    visible: true,
                    widgetOptions: {

                    }
                },
                simpleSearch: {
                    visible: true,
                    widgetOptions: {

                    }
                },
                mapLink: {
                    visible: true,
                    widgetOptions: {
                        enableShortUrls: true,
                        autoUpdateLink: true
                    },
                    menuTitle: "Share",
                    widgetTitle: "Create Map Link",
                    dependencies: ["mapToolbar"]
                },
                scaleBar: {
                    visible: true,
                    widgetOptions: {
                        // "dual" displays both miles and kilmometers
                        // "english" is the default, which displays miles
                        // "metric" for kilometers
                        scalebarUnit: "english"
                    }
                },
                resultsConsole: {
                    visible: false
                },
                accordion: {
                    visible: true,
                    widgetOptions: {}
                },
                overviewMap: {
                    visible: true,
                    widgetOptions: {
                        attachTo: "bottom-right",
                        expandFactor: 3
                    }
                },
                clientWidgets: []
            };

            lang.mixin(this.settings, defaultSettings);

            // mixin in user settings
            if (params.userSettings) {
                lang.mixinMissing(this.settings, params.userSettings);
            }

            if (this.settings.defaults) {
                lang.mixin(wvsConfig.defaults, this.settings.defaults);
            }
            Common.applySettings(this.settings);


            this.controller = new MapViewerController(this);
            wvsConfig.defaults.mapViewerController = this.controller;

            // Enable Map Overlay (if set)
            if (this.settings.globalOptions.showLoadingOverlay) {
                var mapLoadOverlay = domConstruct.create("div", { id: "mapLoadOverlay" });
                var loadingImage = domConstruct.create("img", { src: this.settings.globalOptions.loadingOverlayImage }, mapLoadOverlay);
                domConstruct.create("br", {}, mapLoadOverlay);
                var loadingThrobber = domConstruct.create("img", { src: require.toUrl("./images/mapLoadOverlay-throb.GIF") }, mapLoadOverlay);
                this.mapLoadOverlay = mapLoadOverlay;
                domConstruct.place(mapLoadOverlay, window.document.body);
            }

            if (this.settings.proxyUrl) {
                esriConfig.defaults.io.proxyUrl = this.settings.proxyUrl;// DEPRECTATED: Not supported in IE10 this.settings.proxyUrl.match(/^\//) ? window.location.origin + this.settings.proxyUrl : this.settings.proxyUrl;
            }
            esri.config.defaults.io.corsEnabled = true;
            array.forEach(this.settings.defaults.corsEnabledServers, function (item) {
                esri.config.defaults.io.corsEnabledServers.push(item);
            });

            esriConfig.defaults.io.alwaysUseProxy = this.settings.alwaysUseProxy;
            esriConfig.defaults.geometryService = wvsConfig.defaults.services.geometry ? new GeometryService(wvsConfig.defaults.services.geometry) : null;

            this._processSettings();
            this._setUpProxy();

        },

        startup: function () {
            if (this.loaded) {
                return;
            }

            var self = this;
            this.store = new SearchResultsStore();
            wvsConfig.defaults.searchResultStore = this.store;

            urlUtils.addProxyRule({
                proxyUrl: this.settings.proxyUrl,
                urlPrefix: "138.0.10.140/arcgis/rest/services/Caches"
            });

            // Attach child widgets to the layout

            if (this.settings.globalOptions.layout.header.visible) {
                var headerSettings = this.settings.globalOptions.layout.header;
                if (headerSettings.template) {
                    domConstruct.destroy(this.headerRegion);
                    this.headerRegion = domConstruct.toDom(headerSettings.template);
                    this.simpleSearch = domConstruct.create("div", {}, this.headerRegion);
                    domConstruct.place(this.headerRegion, this.domNode, "first");
                }
                this.headerRegion = new ContentPane({
                    title: 'header',
                    splitter: false,
                    region: 'top',
                    style: headerSettings.style ? headerSettings.style : ""
                }, this.headerRegion);

                if (headerSettings.content && headerSettings.content.length) {
                    array.forEach(headerSettings.content, function (e) {
                        domConstruct.place(e, self.headerRegion.domNode);
                    });
                }
            }

            if (this._componentsVisible(["toc", "accordion"])) {
                this.leftRegion = new TabContainer({
                    region: 'left',
                    splitter: true,
                    style: "width: 350px;"
                }, this.leftRegion);
                this.leftRegion.startup();
            }
            else if (this._componentsVisible(["toc"]) && !this._componentsVisible(["accordion"])) {
                this.leftRegion = new ContentPane({
                    region: 'left',
                    tabStrip: false,
                    splitter: true,
                    style: "width: 350px;"
                }, this.leftRegion);
                this.leftRegion.startup();
            }

            if (this._componentsVisible(["mapToolbar"])) {
                this.topMapRegion = new ContentPane({
                    title: 'Results',
                    splitter: false,
                    region: 'top',
                    style: "padding:0px;height:40px;overflow:hidden;"
                }, this.topMapRegion);
                this.topMapRegion.startup();
            }

            this.mapRegion = new BorderContainer({
                design: 'headline',
                region: 'center',
                gutters: false
            }, this.mapRegion);
            this.mapRegion.startup();

            // The map should always exist
            var mapContentPane = new ContentPane({
                region: 'center',
                gutters: false,
                style: "padding: 0px"
            }, this.mapNode);
            mapContentPane.startup();

            if (this._componentsVisible(["coordinateDisplay", "scaleBar"], true)) {
                this.bottomMapRegion = new ContentPane({
                    region: 'bottom'
                }, this.bottomMapRegion);
                this.bottomMapRegion.startup();
            }

            //if (this._componentsVisible(["resultsConsole"])) {
            this.bottomPane = new ContentPane({
                title: 'Results',
                splitter: true,
                region: 'bottom'
            }, this.bottomPane);

            this.bottomPane.startup();
            //}

            // Remove dom nodes whose presence is dependent on its child widget(s) being visible (at least one must be visible)
            //this._parseDomNodeMappings();

            this.inherited(arguments);

            // For the time being, the infowindow highlight will be turned off. Ask Eddie for further info
            var popup = new Popup({}, domConstruct.create("div"));


            if (this.settings.map.widgetOptions.basemap.id && this.settings.map.widgetOptions.basemap.id.toLowerCase().indexOf("google") > -1) {
                this.settings.map.widgetOptions.basemap = "Imagery";
            }

            // create the map
            this.map = new Map(this.mapNode.id, lang.mixin({ infoWindow: popup }, this.settings.map.widgetOptions));

            this._createMainMenuBar();
            // make the map public
            lang.setObject("window.map", this.map);
            var mapLayers = this.settings.globalOptions.layers;
            InfoTemplateManager.setMap(this.map);
            InfoTemplateManager.setTemplateDirectory(this.settings.templateDirectory);
            InfoTemplateManager.setGlobalAttributeRestrictions(this.settings.globalOptions.globalAttributeRestrictions);
            for (var i = 0; i < mapLayers.length; i++) {
                if (mapLayers[i].hiddenAttributeSettings) {
                    InfoTemplateManager.addAttributeDisplayRestrictions(mapLayers[i]);
                }
            }

            this.homeButton = new HomeButton({
                map: this.map
            }, this.mapHomeButton);
            this.homeButton.startup();

            // Create the map widgets
            if (this._componentsVisible(["simpleSearch"])) {
                this.simpleSearch = new SimpleSearch(lang.mixin({ map: this.map }, this.settings.simpleSearch.widgetOptions), this.simpleSearch);
                this.simpleSearch.startup();
            }

            if (this._componentsVisible(["overviewMap"]) && this.map.getBasemap()) {
                var overviewMapDijit = new OverviewMap(lang.mixin({ map: this.map }, this.settings.overviewMap.widgetOptions));
                overviewMapDijit.startup();
            }

            if (this._componentsVisible(["basemapGallery"])) {
                var basemapGallery = new BasemapDropdown(lang.mixin({ map: this.map, basemapIds: this.map.basemapLayerIds }, this.settings.basemapGallery.widgetOptions), this.basemapGalleryNode);
                basemapGallery.startup();
                this.basemapGallery = basemapGallery;
            }

            if (this._componentsVisible(["scaleBar"])) {
                var scalebar = new Scalebar(lang.mixin({ map: this.map }, this.settings.scaleBar.widgetOptions), this.scalebarNode);
            }


            this._loadLayers();
            this._setupMapEvents();
            this.map.addLayers(this.settings.map.initialLayers);

            this.toolContainer = new ToolContainer({
                map: this.map,
                mapTool: "Identify"
            }, this.mapToolContainerNode);
            this.toolContainer.startup();

            on(this.map, "load", lang.hitch(this, function (evt) {
                if (this._componentsVisible(["resultsConsole"])) {
                    this.results = new ResultsConsole(lang.mixin({ map: this.map, store: this.store }, this.settings.resultsConsole.widgetOptions), this.resultsConsole);

                    // Open the results pane when the results console has a search result
                    on(this.results, "result-found", lang.hitch(this, function () {
                        var bottomSplitter = this.getSplitter("bottom");
                        if (bottomSplitter.get("state") !== "full") {
                            bottomSplitter._toggle();
                        }
                        if (this.bottomPane.h < 200) {
                            this.bottomPane.resize({ w: this.bottomPane.w, h: 200 });
                            this.resize();
                        }
                    }));
                    this.results.startup();
                }

                // create the CoordinateDisplay
                if (this._componentsVisible(["coordinateDisplay"])) {
                    this.coordinateDisplay = new CoordinateDisplay(lang.mixin({ map: this.map }, this.settings.coordinateDisplay.widgetOptions), this.coordinateDisplayNode);
                    this.coordinateDisplay.startup();
                }

                if (this._componentsVisible(["toc", "accordion"])) {
                    var cp = new ContentPane({
                        title: "Map Layers",
                        style: "overflow:auto;"
                    });
                    this.toc = new Toc(lang.mixin({ map: this.map }, this.settings.toc.widgetOptions));
                    cp.addChild(this.toc);
                    this.leftRegion.addChild(cp);
                    this.toc.startup();
                }
                else if (this._componentsVisible(["toc"]) && !this._componentsVisible(["accordion"])) {
                    this.toc = new Toc(lang.mixin({ map: this.map }, this.settings.toc.widgetOptions), this.tocNode);
                    this.toc.startup();
                }

                if (this._componentsVisible(["accordion"])) {
                    var accordionCp = new ContentPane({
                        title: "Results",
                        style: "height:100%;width:100%;"
                    });
                    this.accordion = new Accordion(lang.mixin({ map: this.map, store: this.store }, this.settings.accordion.widgetOptions));
                    this.accordion.configureHiddenAttributes(this.settings.globalOptions);
                    accordionCp.addChild(this.accordion);
                    this.leftRegion.addChild(accordionCp);
                    // Open the accordion tab when the accordion has a search result
                    on(this.accordion, "result-found", lang.hitch(this, function () {
                        console.log("result found!");
                        var leftSplitter = this.getSplitter("left");
                        console.log("leftSplitter", leftSplitter);
                        if (leftSplitter.get("state") !== "full") {
                            console.log("toggling leftSplitter");
                            leftSplitter._toggle();
                        }
                        console.log("selected accordion content pane", accordionCp);
                        this.leftRegion.selectChild(accordionCp);
                    }));
                }

                if (this._componentsVisible(["simpleSearch"])) {
                    on(this.simpleSearch, "search-result-acquired", this.zoomToSelectedSearchResult.bind(this));
                }
                // Enable client widgets

                array.forEach(this.settings.clientWidgets, function (clientWidget) {
                    if (clientWidget.visible) {
                        if (clientWidget.region === "left") {
                            var cp = new ContentPane({
                                title: clientWidget.displayName
                            });
                            var widget = new clientWidget.ctr(lang.mixin({ map: this.map }, clientWidget.widgetOptions));
                            this[clientWidget.name] = widget;
                            cp.addChild(widget);
                            this.leftRegion.addChild(cp);
                        }
                    }
                }, this);

                if (this.settings.globalOptions.showLoadingOverlay) {
                    domConstruct.destroy(this.mapLoadOverlay);
                }
                this._setupAutoSave();
                this.loaded = true;
                this.emit("mapviewer-loaded", this);
            }));

            this._setDefaultEvents();
        },

        destroy: function () {
            var allEvents = this._mapEvents.concat(this._subscriptions);
            array.forEach(allEvents, function (event) {
                event.remove();
            });
            this.inherited(arguments);
        },
        resize: function (newSize, currentSize) {
            var self = this;
            if (this.settings.globalOptions.layout.embedded && !this.loaded) {
                on.once(this, "mapviewer-loaded", function () {
                    self.resize(newSize, currentSize);
                });
                return;
            }
            else
                this.inherited(arguments);
        },
        _setDefaultEvents: function () {
            var self = this,
                evtHandle;


            this.own(on(this.fullScreenButton, "click", lang.hitch(this, function (evt) {
                if (this.fullscreen) {
                    this.minimize();
                }
                else {
                    this.maximize();
                }
            })));
        },

        zoomToSelectedSearchResult: function (res) {
            var geometry = res.geometry,
                extent = null,
                self = this;

            if (geometry.isInstanceOf(Point)) {
                extent = graphicsUtils.pointToExtent(geometry);
            }
            else if (geometry.isInstanceOf(Multipoint)) {
                extent = geometry.points.length > 1 ? geometry.getExtent() : graphicsUtils.pointToExtent(geometry.points[0]);
            }
            else {
                extent = geometry.getExtent();
            }

            if (extent != null) {
                this.map.setExtent(extent, true);
                res.setInfoTemplate(InfoTemplateManager.getTabularAttributeTemplate({ title: "Search Result", createWidgetsFromGeometry: true }));
                var infoWindow = self.map.infoWindow;
                infoWindow.hide();
                infoWindow.setContent(res.getContent());
                infoWindow.setTitle(res.getTitle());
                res.setSymbol(wvsConfig.defaults.symbols.highlightPolygon);
                self.map.graphics.add(res);
                res.show();
                on.once(infoWindow, "hide", function () {
                    self.map.graphics.remove(res);
                });
                infoWindow.show(res.geometry.getExtent().getCenter());
            }
        },

        _processSettings: function () {
            // We will save credentials to local storage on every successful login attempt
            // This is hacky, but there is no 'complete' event for the identity manager and it's silly to add in the same logic in x modules
            aspect.after(IdentityManager, "signIn", function (deferred) {
                var initCredentials = IdentityManager.credentials ? IdentityManager.credentials.length : 0;
                deferred.then(function () {
                    var resolveTime = Date.now();
                    var saveCredential = setInterval(function () {
                        if (IdentityManager.credentials.length > initCredentials) {
                            // Save credentials to local storage
                            if (has("local-storage"))
                                localStorage.setItem("credentials", JSON.stringify(IdentityManager.toJson()));
                        }
                        if ((Date.now() - resolveTime > 10000) || IdentityManager.credentials.length > initCredentials) {
                            clearInterval(saveCredential);
                        }
                    }, 2000);
                });

                return deferred;
            });

            // Check 1: Is there a map link?
            var hasGetParams = this._processGetParams();
            // Check 2: Do we have local storage items? 
            var hasLocalStorage = this._loadFromLocalStorage();


            var settings = this.settings;

            // Set up map, layers, filters, etc.
            if (!settings.map.widgetOptions.extent && !settings.map.widgetOptions.center) {
                throw new Error("No extent or center point provided for map");
            }

            if (!hasGetParams && !hasLocalStorage) {
                settings.map.widgetOptions.extent = new Extent(settings.map.widgetOptions.extent);
            }

            //if (this.getParams) {
            //    settings.map.widgetOptions.center = [parseFloat(this.getParams.lon), parseFloat(this.getParams.lat)];
            //    settings.map.widgetOptions.scale = parseFloat(this.getParams.s);
            //}
            //else if (this.savedMap) {
            //    settings.map.widgetOptions.extent = new Extent(this.savedMap.map.extent);
            //}
            //else {
            //    settings.map.widgetOptions.extent = new Extent(settings.map.initialExtent);
            //}

            // prepare basemaps and basemap gallery widget
            if (settings.basemapGallery.visible) {
                var basemaps = [],
                    userBasemaps = [];

                if (settings.globalOptions.basemaps.length > 0) {
                    var userBasemaps = this._processBasemapForGallery(settings.globalOptions.basemaps);
                    basemaps = basemaps.concat(userBasemaps);
                }
                if (settings.basemapGallery.widgetOptions.showArcGISBasemaps && settings.basemapGallery.esriBasemaps.length > 0) {
                    basemaps = basemaps.concat(this._processBasemapForGallery(settings.basemapGallery.esriBasemaps));
                }

                settings.basemapGallery.widgetOptions.basemaps = basemaps;
            }

            var setBasemapFromString = function (string) {
                for (var i = 0; i < settings.basemapGallery.widgetOptions.basemaps.length; i++) {
                    var basemap = settings.basemapGallery.widgetOptions.basemaps[i];
                    if (basemap.title === string) {
                        settings.map.widgetOptions.basemap = basemap;
                    }
                }
            };

            // Map Link basemap -> First user-defined basemap -> default basemap
            if (userBasemaps.length > 0) {
                settings.map.widgetOptions.basemap = userBasemaps[0];
            } else {
                setBasemapFromString(settings.map.widgetOptions.basemap);
            }
        },
        _processGetParams: function () {
            var self = this;
            var uri = window.document.location.search;
            var queryObject = ioQuery.queryToObject(uri.substring(uri.indexOf("?") + 1, uri.length));
            if (queryObject.sv) {
                queryObject.sv = JSON.parse(queryObject.sv);
                var settings = this.settings;
                // Set up map params
                if (queryObject.lat && queryObject.lon && queryObject.s && settings.map.widgetOptions.extent) {
                    delete settings.map.widgetOptions.extent;
                }


                if (queryObject.lat && queryObject.lon) {
                    settings.map.widgetOptions.center = [parseFloat(queryObject.lon), parseFloat(queryObject.lat)];
                } else if (queryObject.extent) {
                    settings.map.widgetOptions.extent = new Extent(JSON.parse(queryObject.extent));
                }
                if (queryObject.s) {
                    settings.map.widgetOptions.scale = parseFloat(queryObject.s);
                }
                if (queryObject.b) {
                    settings.map.widgetOptions.basemap = queryObject.b;
                }

                // We do not use the defaults if we have layer info in the map link
                if (queryObject.sv.length > 0) {
                    settings.globalOptions.layers = [];
                }

                if (queryObject.mcId && queryObject.mcId != -1) {
                    var mm = new MarkupManager();
                    mm.getMarkupCollection(queryObject.mcId)
                        .then(
                            function (resp) {
                                var markup;
                                if (resp.options.options) {
                                    markup = JSON.parse(resp.options.options);
                                } else {
                                    markup = JSON.parse(resp.options);
                                }
                                settings.globalOptions.layers.push(markup);
                                var lyr = new MarkupLayer(markup);
                                if (!self.map.loaded) {
                                    on(self.map, 'load', function () {
                                        self.map.addLayer(lyr);
                                    });
                                } else {
                                    self.map.addLayer(lyr);
                                }
                            }, function (err) {
                                console.warn(err);
                            }
                        );
                }

                for (var i = 0; i < queryObject.sv.length; i++) {
                    var service = queryObject.sv[i];
                    settings.globalOptions.layers.push(service);
                    if (service.qw && typeof service.qid === "number") {
                        on(self, "mapviewer-loaded", lang.hitch(self, function () {
                            var store = wvsConfig.defaults.searchResultStore,
                                queryUrl = service.url + "/" + service.qid,
                                queryTask = new QueryTask(queryUrl),
                                curQuery = new Query();

                            // clear the store and set it loading
                            store.clear();
                            store.setLoadingData(true);

                            // set up query
                            curQuery.where = service.qw.replace(/_eq_/g, '=');
                            curQuery.outFields = ["*"];
                            curQuery.returnGeometry = true;
                            //execute querytask
                            queryTask.execute(curQuery,
                                function (featureSet) {
                                    // zoom to the extent of the returned features
                                    if (featureSet.features && featureSet.features.length) {
                                        var extent = graphicsUtils.graphicsExtent(featureSet.features);
                                        if (extent) {
                                            self.map.setExtent(extent);
                                        }
                                    }

                                    var adapter = new SearchResultsAdapter();
                                    adapter.featureSetToResultSet(featureSet, queryUrl).then(function (searchResult) {
                                        store.put(searchResult);

                                        setTimeout(function () {
                                            if (self.accordion && self.accordion.gridMap[0]) {
                                                self.accordion.gridMap[0].grid.selectAll();
                                            }
                                        }, 2000);
                                    });
                                },
                                function (error) {
                                    store.setLoadingData(false);
                                }
                            );
                        }, service)
                        );
                    }
                }

                return queryObject.s && ((queryObject.lat && queryObject.lon) || (queryObject.extent)); // Boolean
            }
            return false; // Boolean
        },

        _loadFromLocalStorage: function () {
            // summary:
            //      loads saved data from local storage
            // returns:
            //      whether or not there is saved map data
            var self = this,
             settings = self.settings;

            //Creates an internal 'sessionBookmarks' property that will be used to generate the complete list of items
            self.settings.bookmarks.sessionBookmarks = [];

            var pushBookmarks = function (bookmarkArray, bookmarkList) {
                for (var i in bookmarkList) {
                    var bookmark = new Bookmark(bookmarkList[i]);
                    if (bookmark.hasChildren()) {
                        var newChildren = [];
                        pushBookmarks(newChildren, bookmarkList[i].children);
                        bookmark.children = newChildren;
                    }
                    bookmarkArray.push(bookmark);
                }
            };

            if (has("local-storage")) {
                var savedMap = localStorage.getItem("m4") ? JSON.parse(localStorage.getItem("m4")) : null;

                if (savedMap) {
                    settings.map.widgetOptions.extent = new Extent(savedMap.map.extent);
                    settings.map.widgetOptions.basemap = savedMap.map.basemap;
                    // We do not use the defaults if we have layer info in local storage
                    if (savedMap.layers.length > 0) {
                        settings.globalOptions.layers = [];
                    }
                    for (var i = 0; i < savedMap.layers.length; i++) {
                        settings.globalOptions.layers.push(savedMap.layers[i]);
                    }
                    self.savedMap = savedMap;
                }
                if (self._componentsVisible(["bookmarks"]) && localStorage.getItem("bookmarks")) {
                    var bookmarkList = JSON.parse(localStorage.getItem("bookmarks"));
                    //Adding in a property to store the concatenated list of saved bookmarks from mapSettings.js and
                    //the user-created bookmarks stored in local storage.
                    pushBookmarks(self.settings.bookmarks.sessionBookmarks, bookmarkList);
                } else {
                    pushBookmarks(self.settings.bookmarks.sessionBookmarks, self.settings.bookmarks.savedBookmarks);
                }
                if (localStorage.getItem("credentials")) {
                    IdentityManager.initialize(JSON.parse(localStorage.getItem("credentials")));
                }
                return savedMap ? true : false; // Boolean
            } else {
                pushBookmarks(self.settings.bookmarks.sessionBookmarks, self.settings.bookmarks.savedBookmarks);
            }
            return false; // Boolean
        },

        _loadLayers: function () {
            // summary:
            //      create the array of layers to be added to the map

            // set up layers
            var layersToAdd = [];

            var hasMarkup = false;
            // We will always want our default layers
            array.forEach(this.settings.globalOptions.layers, function (layer) {
                var instance = this._createLayerFromSettingsObject(layer);
                if (layer.type === "MarkupLayer") {
                    this.markupLayer = wvsConfig.defaults.markupLayer = instance;
                    hasMarkup = true;
                }
                layersToAdd.push(instance);
            }, this);
            if (!hasMarkup && this._componentsVisible(["markup"])) {
                this.markupLayer = new MarkupLayer({ title: "Temporary Markup", id: "markupLayer" });
                wvsConfig.defaults.markupLayer = this.markupLayer;
                layersToAdd.push(this.markupLayer);
            }

            this.settings.map.initialLayers = layersToAdd;
        },
        _processBasemapForGallery: function (basemapArray) {
            var basemaps = [],
                self = this;
            array.forEach(basemapArray, function (basemap) {
                var basemapLayers = [];
                array.forEach(basemap.layers, function (basemapLayerOptions) {
                    if (basemapLayerOptions.type === "VETiledLayer") {
                        basemapLayers.push(new VETiledLayer({
                            bingMapsKey: self.settings.globalOptions.bingMapsKey,
                            mapStyle: basemapLayerOptions.mapStyle
                        }));
                    } else {
                        basemapLayers.push(new BasemapLayer(basemapLayerOptions));;
                    }
                });
                var basemapOptions = lang.mixin({ layers: basemapLayers }, basemap.options);
                var bml = new Basemap(basemapOptions);
                if (basemapOptions.googleLayerControls) {
                    bml.googleLayerControls = basemapOptions.googleLayerControls;
                }
                basemaps.push(bml);
            });

            return basemaps;
        },
        _setupMapEvents: function () {
            // summary:
            //      binds the passed in map events.
            // description:
            //      'Map events' are objects that encapsulate a map event and add a few common behaviors if necessary.
            //      Instead of the mapviewer itself having direct knowledge of these events, we can extend the _MapEventMixIn module
            //      and add in our own custom events as we like. This results in cleaner, more modular code.
            array.forEach(this.settings.globalOptions.mapEvents, function (event) {
                var mapEvent = new event.ctr(this.map, event.options || {});
                this._mapEvents.push(mapEvent);
                this.own(mapEvent.initialize());
            }, this);
        },
        _createLayerFromSettingsObject: function (layerObject) {
            var layer = null;

            if (layerObject instanceof Layer) {
                return layerObject;
            }

            switch (layerObject.type) {
                case "ArcGISDynamicMapServiceLayer":
                    layer = new ArcGISDynamicMapServiceLayer(layerObject.url, layerObject.options || {});
                    break;
                case "FeatureLayer":
                    // are we accessing a feature service?
                    if (layerObject.url) {
                        layer = new FeatureLayer(layerObject.url, layerObject.options || {});
                    }
                    else if (layerObject.featureCollection) {
                        layer = new FeatureLayer(layerObject.featureCollection, layerObject.options || {});
                    }
                    on.once(layer, "load", function () {
                        layer.setInfoTemplate(InfoTemplateManager.getTabularAttributeTemplate({ featureLayer: layer, createWidgetsFromGeometry: true }));
                    });
                    break;
                case "ArcGISWebMapLayer":
                    layer = new ArcGISWebMapLayer(layerObject.url, layerObject.options || {});
                    break;
                case "StreamLayer":
                    if (layerObject.url) {
                        layer = new StreamLayer(layerObject.url, layerObject.options || {});
                        on.once(layer, "load", function () {
                            layer.connect();
                            layer.setInfoTemplate(InfoTemplateManager.getTabularAttributeTemplate({ featureLayer: layer, createWidgetsFromGeometry: true }));
                        });
                    } else if (layerObject.featureCollection) {
                        layer = new StreamLayer(layerObject.featureCollection, layerObject.options || {});
                    }
                    layer._visibleInToc = true;
                    break;
                case "GraphicsLayer":
                    layer = new GraphicsLayer(layerObject.options || {});
                    break;
                case "GeoRSSLayer":
                    layer = new GeoRSSLayer(layerObject.url, layerObject.options || {});
                    break;
                case "ArcGISImageServiceLayer":
                    layer = new ArcGISImageServiceLayer(layerObject.url, layerObject.options || {});
                    break;
                case "MarkupLayer":
                    layer = new MarkupLayer(layerObject.options || {});
                    break;
                case "GoogleMapsLayer":
                    layer = new GoogleMapsLayer(layerObject.options || {});
                    break;
                default:
                    //throw new Error("an unknown or unsupported type was added to the map");
                    break;
            }
            lang.mixin(layer, layerObject.options);

            return layer;
        },
        _setUpProxy: function () {
            // summary:
            //      sets up proxy rules by looking at layer endpoints and proxy rules defined in the configuration. If there are any proxy replacements (i.e. mapping a server to an "accessible" one for the proxy), 
            var proxyUrl = esriConfig.defaults.io.proxyUrl;
            // Look through proxy rules
            array.forEach(this.settings.globalOptions.proxyRules, function (proxyRule) {
                urlUtils.addProxyRule({ proxyUrl: proxyRule.proxyUrl || proxyUrl, urlPrefix: proxyRule.urlPrefix });
            });
            var parseLayerForProxy = function (layer) {
                var layerUrl = new url(layer.url);
                if (layerUrl.scheme !== "https" && layerUrl.scheme !== location.protocol.replace(/:/, "")) {
                    urlUtils.addProxyRule({ proxyUrl: proxyUrl, urlPrefix: layerUrl.scheme + "://" + layerUrl.authority });
                }
            };

            // Look through layers
            array.forEach(this.settings.globalOptions.layers, function (layer) { parseLayerForProxy(layer); });

            // Look through basemaps
            array.forEach(this.settings.basemapGallery.widgetOptions.basemaps, function (basemapLayer) {
                array.forEach(basemapLayer.layers, function (layer) { parseLayerForProxy(layer); });
            });

            var replaceUrl = function (ioArgs) {
                if (ioArgs != undefined && ioArgs != null && ioArgs.content != undefined && ioArgs != null) {
                    var content = ioArgs.content;
                    for (var key in content) {
                        var item = content[key];
                        array.forEach(replaceMap, function (r) {
                            item = item.replace(new RegExp(r.oldText.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), "g"), r.newText);
                        });
                        ioArgs.content[key] = item;
                    }
                }
                return ioArgs;
            };

            if (this.settings.globalOptions.proxyReplacements.length > 0) {
                var proxyReplacements = this.settings.globalOptions.proxyReplacements;
                var replaceUrl = function (ioArgs) {
                    if (ioArgs != undefined && ioArgs != null && ioArgs.content != undefined && ioArgs != null) {
                        var content = ioArgs.content;
                        for (var key in content) {
                            var item = content[key];
                            if (typeof item === "string") {
                                array.forEach(proxyReplacements, function (r) {
                                    item = item.replace(new RegExp(r.oldText.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), "g"), r.newText);
                                });
                                ioArgs.content[key] = item;
                            }
                        }
                    }
                    return ioArgs;
                };
                esriRequest.setRequestPreCallback(replaceUrl);
            }
        },
        _setupAutoSave: function () {
            // summary:
            //      sets up auto save based on map settings
            var autoSaveSettings = this.settings.globalOptions.autoSave;
            if (autoSaveSettings.enabled && autoSaveSettings.topics.length) {
                var handler = lang.hitch(this, function () { this.save(); });
                array.forEach(autoSaveSettings.topics, function (topic) {
                    // The mapviewer will be reponsible for setting up the map-specific 'topics' (in this case, these are easy-to-remember names for direct event bindings; other
                    switch (topic) {
                        case "map/extent/change":
                            this.own(on(this.map, "extent-change", handler));
                            break;
                        case "map/basemap/change":
                            this.own(on(this.map, "basemap-change", handler));
                            break;
                        case "map/layer/add":
                            this.own(on(this.map, "layer-add", handler));
                            break;
                        case "map/layer/remove":
                            this.own(on(this.map, "layer-remove", handler));
                            break;
                        default: this._subscriptions.push(connect.subscribe(topic, handler)); break;
                    }


                }, this);
            }
        },
        _createMainMenuBar: function () {
            var self = this;

            this.mapRegionMenuBar = new MenuBar({
                region: 'top'
            }, this.mapRegionMenuBar);

            var mapRegionToolsMenu = new DropDownMenu({});

            if (this._componentsVisible(["attributeSearch"])) {
                // Attribute Search Menu Item
                mapRegionToolsMenu.addChild(new MenuItem({
                    label: this.settings.attributeSearch.menuTitle,
                    iconClass: 'icon-search',
                    onClick: lang.hitch(this, function () {
                        if (!this._componentsVisible(["simpleSearch"])) {
                            this.settings.attributeSearch.widgetOptions.enableSaveAsQuickSearch = false;
                        }
                        Common.showWidgetInDialog(AttributeSearch, lang.mixin({ map: this.map, store: this.store }, this.settings.attributeSearch.widgetOptions), { title: this.settings.attributeSearch.widgetTitle, "class": "nonModal", style: "width:850px;" });
                    })
                }));
            }

            if (this._componentsVisible(["identify"])) {
                // Identify Menu Item
                mapRegionToolsMenu.addChild(new MenuItem({
                    label: this.settings.identify.menuTitle,
                    iconClass: 'icon-info-sign',
                    onClick: lang.hitch(this, this._identifyTool)
                }));
            }

            if (this._componentsVisible(["markup"])) {
                // Markup Menu Item
                mapRegionToolsMenu.addChild(new MenuItem({
                    label: this.settings.markup.menuTitle,
                    iconClass: 'icon-pencil',
                    onClick: lang.hitch(this, this._showMarkupTool)
                }));
            }

            if (this._componentsVisible(["measure"])) {
                // Measure Menu Item
                mapRegionToolsMenu.addChild(new MenuItem({
                    label: this.settings.measure.menuTitle,
                    iconClass: 'icon-resize-full',
                    onClick: lang.hitch(this, this._measureTool)
                }));
            }

            if (this._componentsVisible(["azimuth"])) {
                // Measure Menu Item
                mapRegionToolsMenu.addChild(new MenuItem({
                    label: this.settings.azimuth.menuTitle,
                    iconClass: 'icon-resize-full',
                    onClick: lang.hitch(this, function () {
                        Common.showWidgetInDialog(Azimuth, lang.mixin({ map: this.map }, this.settings.azimuth.widgetOptions), { title: this.settings.azimuth.widgetTitle, "class": "nonModal", style: "width:600px;" });
                    })
                }));
            }

            if (this._componentsVisible(["swipe"])) {
                // Swipe Menu Item
                mapRegionToolsMenu.addChild(new MenuItem({
                    label: this.settings.swipe.menuTitle,
                    iconClass: 'icon-move',
                    onClick: lang.hitch(this, this._swipeTool)
                }));
            }


            if (this._componentsVisible(["zoomToXY"])) {
                // Zoom To XY Menu Item
                mapRegionToolsMenu.addChild(new MenuItem({
                    label: this.settings.zoomToXY.menuTitle,
                    iconClass: 'icon-screenshot',
                    onClick: lang.hitch(this, function () {
                        var dialog = Common.showWidgetInDialog(ZoomToXY, lang.mixin({ map: this.map }, this.settings.zoomToXY.widgetOptions), { title: this.settings.zoomToXY.widgetTitle, "class": "nonModal" });

                        on(dialog.content, "point-created", lang.hitch(this, function (pt) {
                            var graphic = new Graphic(pt, symbolJsonUtils.fromJson(wvsConfig.defaults.symbols.point), {}, InfoTemplateManager.getTabularAttributeTemplate({ title: "XY Location", createWidgetsFromGeometry: true }));
                            if (this.markupLayer) {
                                graphic.setEditing(true);
                                graphic.save();
                                this.markupLayer.add(graphic);
                            }
                            else {
                                this.map.graphics.add(graphic);
                            }

                            this.map.infoWindow.setContent(graphic.getContent());
                            this.map.infoWindow.setTitle(graphic.getTitle());
                            this.map.centerAndZoom(pt, this.map.getNumLevels()).then(lang.hitch(this, function () { this.map.infoWindow.show(pt); }));
                        }));
                    })
                }));
            }


            if (this._componentsVisible(["routingTool"])) {
                // Zoom To XY Menu Item
                mapRegionToolsMenu.addChild(new MenuItem({
                    label: this.settings.routingTool.menuTitle,
                    iconClass: 'icon-compass',
                    onClick: lang.hitch(this, function () {
                        var dialog = Common.showWidgetInDialog(RoutingTool, lang.mixin({ map: this.map, geocodeServer: this.settings.defaults.services.geocoder, routingServer: this.settings.defaults.services.routing }, this.settings.routingTool.widgetOptions), { title: this.settings.routingTool.widgetTitle, "class": "nonModal", style: "width: 700px;" });
                    })
                }));
            }


            if (this._componentsVisible(["viewIn"])) {
                var viewIn = new ViewIn(lang.mixin({ map: this.map, parentMenu: mapRegionToolsMenu }, this.settings.viewIn.widgetOptions || {}));
            }

            var mapRegionToolsPopupMenuBarItem = new PopupMenuBarItem({ label: "Tools", popup: mapRegionToolsMenu });

            this.mapRegionMenuBar.addChild(mapRegionToolsPopupMenuBarItem);

            if (self._componentsVisible(["bookmarks"])) {
                var bookmarks = self.settings.bookmarks.sessionBookmarks,
                    bookmarksMenu = new DropDownMenu({ _bIndex: 0, _fIndex: 0 });

                for (var i = 0; i < bookmarks.length; i++) {
                    bookmarks[i] = new Bookmark(bookmarks[i]);
                }
                //Maintain a _bIndex to keep track of the menu item index for bookmarks and a _fIndex for
                //the current index to insert folders into. This maintains a Folders-on-top/Bookmarks-on-bottom
                //paradigm for the menu.

                var addBookmarkMenuItemsFromArray = function (bookmarkArray, menu) {
                    if (bookmarkArray.length == 0) {
                        //If the current array is empty, indicate as such with a blank menu item.
                        menu.addChild(new MenuItem({
                            label: "(Empty)"
                        }), menu._bIndex);
                        menu._bIndex++;
                    } else {
                        for (var i = 0; i < bookmarkArray.length; i++) {
                            var bMark = bookmarkArray[i];
                            if (bMark.hasChildren()) {
                                //If the bookmark has children, it is a folder. Create a popout menu for all of it's child nodes and
                                //recursively run this method on that new menu.
                                var newMenu = new DropDownMenu({
                                    _bIndex: 0,
                                    _fIndex: 0
                                });
                                menu.addChild(new PopupMenuItem({
                                    label: bMark.name,
                                    iconClass: "icon-folder-close",
                                    popup: newMenu,
                                    _bookmark: bMark
                                }), menu._fIndex);
                                addBookmarkMenuItemsFromArray(bMark.children, newMenu);
                                //Increment the _fIndex and the _bIndex because folders can and will be added to the middle of the menu
                                //whereas bookmarks should only always be added to the bottom.
                                menu._fIndex++;
                                menu._bIndex++;
                            } else {
                                //If the bookmark has no children, it is a leaf in the bookmarks tree and can be added to the bottom
                                //of the current menu with an onClick event that will zoom to the bookmarked extent.
                                var menuItem = new MenuItem({
                                    label: bMark.name,
                                    iconClass: "icon-bookmark",
                                    _bookmark: bMark
                                });
                                //We have to explicitly add the 'mousedown' handler and hitch it to the context of the MenuItem
                                //so that we can access the instance property 'this._bookmark'. Otherwise the menu items will all
                                //set the map to the extent of the last bookmark added because the value in their onMouseDown functions
                                //will change with each iteration through the loop.
                                on(menuItem, "mouseDown", lang.hitch(menuItem, function () {
                                    self.map.setExtent(new Extent(this._bookmark.extent));
                                }));
                                menu.addChild(menuItem, menu._bIndex);
                                //Only increment the _bIndex since the _fIndex will stay the same so folders will always be added
                                //to the end of the folder list.
                                menu._bIndex++;
                            }
                        }
                    }
                };

                // Create Bookmarks Menu
                addBookmarkMenuItemsFromArray(bookmarks, bookmarksMenu);

                //Function to write a new bookmark tree to the menu dropdown
                var updateBookmarksMenu = function (bookmarks) {
                    self.settings.bookmarks.sessionBookmarks = bookmarks;
                    bookmarksMenu.destroyDescendants(false);
                    bookmarksMenu._bIndex = 0;
                    bookmarksMenu._fIndex = 0;
                    addBookmarkMenuItemsFromArray(bookmarks, bookmarksMenu);
                    addBookmarkManagerItem();
                    self.saveBookmarks();
                };

                self.bookmarksMenu = bookmarksMenu;

                //Function to add the bookmark manager node to the bottom of the menu dropdown
                var addBookmarkManagerItem = function () {
                    bookmarksMenu.addChild(new MenuSeparator({}));
                    bookmarksMenu.addChild(new MenuItem({
                        label: "Manage Bookmarks",
                        onClick: function () {
                            var dialog = Common.showWidgetInDialog(BookmarkManager, { map: map, bookmarks: bookmarks }, { title: "Bookmark Manager", "class": "nonModal" });
                            on(dialog.content, "bookmarks-updated", updateBookmarksMenu);
                        }
                    }));
                };

                addBookmarkManagerItem();

                var bookmarksPopupMenuItem = new PopupMenuBarItem({ label: "Bookmarks", popup: bookmarksMenu });

                self.mapRegionMenuBar.addChild(bookmarksPopupMenuItem);
            }

            // MapManager button
            if (this._componentsVisible(["mapManager"])) {
                this.mapRegionMenuBar.addChild(new MenuBarItem({
                    label: this.settings.mapManager.menuTitle,
                    onClick: lang.hitch(this, this._showMapManager)
                }));
            }

            // Print button
            if (this._componentsVisible(["exportTool"])) {
                this.mapRegionMenuBar.addChild(new MenuBarItem({
                    label: this.settings.exportTool.menuTitle,
                    iconClass: "icon-print",
                    onClick: lang.hitch(this, function () {
                        Common.showWidgetInDialog(Print, lang.mixin({ map: this.map }, this.settings.exportTool.widgetOptions), { title: this.settings.exportTool.widgetTitle, "class": "nonModal" });
                    })
                }));
            }

            if (this._componentsVisible(["mapLink"])) {
                this.mapRegionMenuBar.addChild(new MenuBarItem({
                    label: this.settings.mapLink.menuTitle,
                    iconClass: "icon-share",
                    onClick: lang.hitch(this, function () {
                        Common.showWidgetInDialog(MapLink, lang.mixin({ map: this.map }, this.settings.mapLink.widgetOptions), { title: this.settings.mapLink.widgetTitle, "class": "nonModal" });
                    })
                }));
            }
            //Help button
            this.mapRegionMenuBar.addChild(
                new MenuBarItem({
                    label: "Help",
                    onClick: function () {
                        window.open(self.settings.defaults.services.help, "_blank");
                    }
                })
            );

            this.mapRegionMenuBar.startup();
        },
        maximize: function () {
            var layout = this.settings.globalOptions.layout.maximized;
            if (this.settings.globalOptions.layout.header.visible) {
                var headerProp = layout.headerRegion ? "block" : "none";
                domStyle.set(this.headerRegion.domNode, { display: headerProp });
            }
            if (this.settings.toc.visible) {
                var displayProp = layout.leftRegion ? "block" : "none";
                var leftSplitter = this.getSplitter("left");
                domStyle.set(leftSplitter.child.domNode, { display: displayProp });
                domStyle.set(leftSplitter.domNode, { display: displayProp });
            }
            if (this.settings.resultsConsole.visible) {
                var displayProp = layout.bottomPane ? "block" : "none";
                var bottomSplitter = this.getSplitter("bottom");
                domStyle.set(bottomSplitter.child.domNode, { display: displayProp });
                domStyle.set(bottomSplitter.domNode, { display: displayProp });
            }
            if (this.settings.mapToolbar.visible) {
                var displayProp = layout.mapToolbar ? "block" : "none";
                domStyle.set(this.topMapRegion.domNode, { display: displayProp });
            }
            this.resize();

            this.fullscreen = true;
            domClass.add(this.fullScreenButton, "maximized");
            this.emit("maximized", this);
        },
        minimize: function () {
            var layout = this.settings.globalOptions.layout.minimized;
            if (this.settings.globalOptions.layout.header.visible) {
                var headerProp = layout.headerRegion ? "block" : "none";
                domStyle.set(this.headerRegion.domNode, { display: headerProp });
            }
            if (this.settings.toc.visible) {
                var displayProp = layout.leftRegion ? "block" : "none";
                var leftSplitter = this.getSplitter("left");
                domStyle.set(leftSplitter.child.domNode, { display: displayProp });
                domStyle.set(leftSplitter.domNode, { display: displayProp });
            }
            if (this.settings.resultsConsole.visible) {
                var displayProp = layout.bottomPane ? "block" : "none";
                var bottomSplitter = this.getSplitter("bottom");
                domStyle.set(bottomSplitter.child.domNode, { display: displayProp });
                domStyle.set(bottomSplitter.domNode, { display: displayProp });
            }
            if (this.settings.mapToolbar.visible) {
                var displayProp = layout.mapToolbar ? "block" : "none";
                domStyle.set(this.topMapRegion.domNode, { display: displayProp });
            }
            this.resize();

            this.fullscreen = false;
            domClass.remove(this.fullScreenButton, "maximized");
            this.emit("minimized", this);
        },
        _componentsVisible: function (componentNames, any) {
            var settings = this.settings,
                any = any ? true : false,
                visible = any ? false : true;


            array.forEach(componentNames, function (componentName) {
                var componentSetting = settings[componentName];

                visible = any ? componentSetting.visible || visible : componentSetting.visible && visible;

                if (componentSetting.dependencies) {
                    array.forEach(componentSetting.dependencies, function (dependency) {
                        var dependentVisible = settings[dependency].visible;
                        if (!dependentVisible) {
                            console.warn("the component, " + componentName + ", was not loaded because its dependency, " + dependency + ", is set not to load");
                        }
                        visible = any ? dependentVisible || visible : dependentVisible && visible;
                    });
                }
            });

            return visible;
        },
        save: function () {
            if (!has("local-storage")) {
                return;
            }

            var mapState = new MapState(this.map);
            var mapExport = mapState.toJson();
            localStorage.setItem("m4", JSON.stringify(mapExport));

            this.saveBookmarks();
        },
        saveBookmarks: function () {
            if (has("local-storage") && this._componentsVisible(["bookmarks"])) {
                var bookmarks = this.settings.bookmarks.sessionBookmarks.slice();
                localStorage.setItem("bookmarks", JSON.stringify(bookmarks));
            }
        },
        _parseDomNodeMappings: function () {
            var domNodeToComponentMappings = this.domNodeToComponentMappings;

            for (var domNode in domNodeToComponentMappings) {
                var domNodeConstructed = false;
                array.forEach(domNodeToComponentMappings[domNode], function (componentName) {
                    domNodeConstructed = domNodeConstructed || this._componentsVisible([componentName]);
                }, this);

                if (!domNodeConstructed) {
                    domConstruct.destroy(this[domNode]);
                }
            }
        },
        _identifyTool: function () {
            this.toolContainer.activateTool('Identify', lang.mixin({ store: this.store }, this.settings.identify.widgetOptions), this.settings.identify.widgetTitle);
        },
        _measureTool: function () {
            this.toolContainer.activateTool('Measure', this.settings.measure.widgetOptions, this.settings.measure.widgetTitle);
        },
        _showMapManager: function () {
            var vs = win.getBox();
            var w = vs.w * .9;
            var h = vs.h * .75;
            var dialog = Common.showWidgetInDialog(MapManager, lang.mixin({
                map: this.map,
                design: "headline",
                gutters: false,
                style: "width:" + w + "px;height:" + h + "px;"
            }, this.settings.mapManager.widgetOptions), { title: this.settings.mapManager.widgetTitle, "class": "nonModal" }, true);
        },
        _swipeTool: function () {
            this.toolContainer.activateTool('Swipe', lang.mixin({ basemapLayers: this.settings.basemapGallery.widgetOptions.basemaps }, this.settings.swipe.widgetOptions), this.settings.swipe.widgetTitle);
        },
        _showMarkupTool: function () {
            this.toolContainer.activateTool('Markup', lang.mixin({ markupLayer: this.markupLayer }, this.settings.markup.widgetOptions), this.settings.markup.widgetTitle);
        }
    });

    var MapViewerController = new declare([], {
        constructor: function (mapViewer) {
            this.mapViewer = mapViewer;
        },
        getWidget: function (ctr, region, options) {

            if (!ctr || !region)
                throw new Error("A constructor and a region are required");

            var mv = this.mapViewer,
                instance = null;
            for (var key in mv) {
                var prop = mv[key];
                if (prop && typeof prop === "object" && typeof prop.isInstanceOf === "function" && prop.isInstanceOf(ctr)) {
                    instance = prop;
                }
            }
            if (!instance) {
                instance = new ctr(lang.mixin({ map: mv.map, store: mv.store }, options || {}));
                this.sendToRegion(instance, region);
            }
            return instance;
        },
        sendToRegion: function (widgetOrHtml, region) {
            // summary:
            //      send data to widget
            // widgetOrHtml: Object | DomNode | String
            //      the content to send to the specified section
            // sectionName: String
            //      the name of the section where the content should appear

            var cp;
            switch (region) {
                case "bottom":
                    cp = this.mapViewer.bottomPane;
                    break;
                case "header":
                    //cp = this.mapViewer.bottomPane;
                    break;

                case "left":
                    //cp = this.mapViewer.bottomPane;
                    break;
            }

            //var currentContent = cp.get("content");

            //var backButton = new SimpleButton({
            //    label: "",
            //    tooltip: "Return to Content",
            //    leftIconClass: "icon-circle-arrow-left",
            //    style: "font-size:24px;",
            //    onClick: lang.hitch(this, function () { cp.set("content", currentContent); })
            //});

            cp.set("content", widgetOrHtml);
            //backButton.placeAt(cp.domNode, 'first');
        },

        openBottomRegion: function () {
            domStyle.set(this.mapViewer.bottomPane.domNode, "height", "300px");
            registry.byId(this.mapViewer.containerNode.id).resize();
            this.mapViewer.bottomPane.resize();
            if (this.mapViewer.bottomPane._splitterWidget.state === "closed") {
                this.mapViewer.bottomPane._splitterWidget._toggle();
            }
        }
    });


    return MapViewer;
});