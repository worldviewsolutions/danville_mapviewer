﻿define([
    "dojo/_base/declare"
    , "dijit/_WidgetBase"
    , "dijit/_TemplatedMixin"
    , "dijit/_WidgetsInTemplateMixin"
    , "dojo/text!./templates/Print.html"
    , "dojo/_base/lang"
    , "dojo/_base/array"
    , "dojo/on"
    , "esri/tasks/PrintParameters"
    , "esri/tasks/PrintTemplate"
    , "esri/tasks/PrintTask"
    , "dijit/form/Select"
    , "dijit/form/RadioButton"
    , "dijit/form/CheckBox"
    , "dijit/form/TextBox"
    , "dijit/form/Button"
    , "../_base/config"
    , "dojo/dom-style"
    , "dojo/dom-class"
    , "dojo/dom"
    , "dojo/dom-attr"
    , "esri/request"
    , "dojo/dom-construct"
    , "../common/Common"
    , "../common/ObjUtil"
    , "esri/lang"
    , "esri/urlUtils"
    , "esri/graphic"
    , "esri/symbols/jsonUtils"
    , "esri/tasks/LegendLayer"
    , "esri/geometry/Point"
    , "esri/geometry/Extent"
    , "esri/geometry/ScreenPoint"
    , "dijit/Fieldset"
    , "esri/SpatialReference"
], function (declare, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, template, lang, array, on, PrintParameters, PrintTemplate, PrintTask, Select, RadioButton, CheckBox, TextBox, Button, wvsConfig, domStyle, domClass, dom, domAttr, esriRequest, domConstruct, Common, ObjUtil, esriLang, urlUtils, Graphic, symbolJsonUtils, LegendLayer, Point, Extent, ScreenPoint, Fieldset, SpatialReference) {
    // module:
    //      wvs/dijits/Print

    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {

        templateString: template,

        declaredClass: "wvs.dijits.Print",

        // GPServer: String
        //      points the geoprocessing print service that this widget will use (either custom or out-of-box)
        GPServer: null,

        // replaceMap: Array
        //      the print service must be able to access the urls for the webmap so sometimes we need to manipulate the request before it gets executed
        //      Example: [ { oldText: 'http://www.myservice.com:6080', newText: 'http://localhost'
        replaceMap: [],

        // width: Number
        //      the width (inches) of the exported map
        width: 4,

        // height: Number
        //      the height (inches) of the exported map
        height: 4,

        // dpi: Number
        //      the dpi of the exported map
        dpi: 96,

        // useCustomMapScale: Boolean
        //      determines whether or not this map is to use a custom map scale
        useCustomMapScale: false,

        // isExtendedService: Boolean
        //      is this GP service extended to provide size information for templates?
        isExtendedService: false,

        //_defaultImageFormats: Array
        //      The default list of supported formats for image exports
        _defaultImageFormats: [
            { label: 'PNG', value: 'PNG' },
            { label: 'JPG', value: 'JPG' },
            { label: 'GIF', value: 'GIF' },
            { label: 'EPS', value: 'EPS' },
            { label: 'SVG', value: 'SVG' }
        ],

        //_defaultTemplateFormats: Array
        //      The default list of supported template output formats.
        _defaultTemplateFormats: [
            { label: 'PDF', value: 'PDF' }
        ],

        // _exportLink: String
        //      the link to the exported image
        _exportLink: null,

        // overlayEnabled: Boolean
        //      determines whether or not the overlay will draw on extent-change. Does not remove the extent-change event binding, just doesn't draw.
        overlayEnabled: true,

        _serviceErrorMessage: "<h3>Could not resolve the geoprocessing service</h3><p>This widget will now close. Please try again later or check the service and/or your settings</p>",

        constructor: function (params, srcNodeRef) {
            // required params
            if (!params.map) {
                throw new Error("map required for " + this.declaredClass);
            }

            this.GPServer = params.GPServer ? params.GPServer : wvsConfig.defaults.services.print ? wvsConfig.defaults.services.print : null;

            if (!this.GPServer) {
                throw new Error("GPServer required for " + this.declaredClass);
            }

            this.supportedImageFormats = params.supportedImageFormats || this._defaultImageFormats;
            this.supportedTemplatedFormats = params.supportedTemplatedFormats || this._defaultTemplateFormats;

            this._onExtentChangeHandler = null;
        },
        postCreate: function () {
            var self = this;

            // create widgets

            // Currently DISABLED:
            // There is a bug in esri's ExportWebMap spec that does not work with the print task
            // When scalebar units are changed, the result is only changed scalebar unit labels resulting in a wildly inaccurate scalebar
            //this.scaleBarUnitSelect = new Select({
            //    options: [
            //        { label: "Meters", value: "Meters" },
            //        { label: "Feet", value: "Feet" },
            //        { label: "Miles", value: "Miles" },
            //        { label: "Kilometers", value: "Kilometers" }
            //    ]
            //}, this.scaleBarUnitSelect);

            this.specifySizeRadio = new RadioButton({
                checked: true,
                name: "dimensionSelection",
                value: "specifySize",
                onChange: lang.hitch(this, this._setDimensionSelection)
            }, this.specifySizeRadio);

            this.specifyTemplateRadio = new RadioButton({
                checked: false,
                name: "dimensionSelection",
                value: "specifyTemplate"
            }, this.specifyTemplateRadio);

            this.widthTextbox = new TextBox({
                onKeyUp: lang.hitch(this, this._onWidthHeightChange),
                value: this.width
            }, this.widthTextbox);

            this.heightTextbox = new TextBox({
                onKeyUp: lang.hitch(this, this._onWidthHeightChange),
                value: this.height
            }, this.heightTextbox);

            this.templateSelect = new Select({
                options: [{ label: "Loading from Server", value: null }],
                onChange: lang.hitch(this, this._onTemplateChange)
            }, this.templateSelect);

            this.titleTextbox = new TextBox({
                //style: "width:150px"
            }, this.titleTextbox);

            //Apply uniform styling classes
            domClass.add(this.widthTextbox.domNode, "span1");
            domClass.add(this.heightTextbox.domNode, "span1");
            domClass.add(this.titleTextbox.domNode, "span2");

            this.includeLegend = new CheckBox({
                checked: false
            }, this.includeLegend);

            this.enableCustomScaleCheckbox = new CheckBox({
                checked: this.useCustomMapScale,
                onChange: lang.hitch(this, this._customScaleCheckboxChange)
            }, this.enableCustomScaleCheckbox);

            this.customScaleTextBox = new TextBox({
                disabled: !this.useCustomMapScale,
                value: Math.ceil(this.map.getScale())
            }, this.customScaleTextBox);

            this.advancedTemplateOptions = new Fieldset({
                title: "Advanced Options"
            }, this.advancedTemplateOptions);



            this.exportButton = new Button({
                label: "Export",
                onClick: lang.hitch(this, this._startExport)
            }, this.exportButton);

            this.own(on(this.exportLink, "click", lang.hitch(this, function () {
                this.exportButton.set("iconClass", "");
                this.exportButton.set("disabled", false);
                domStyle.set(this.exportButton.domNode, { display: "inline-block" });
                domStyle.set(this.exportLink, { display: "none" });
                window.open(self._exportLink, '_blank');
            })));

            this._customScaleCheckboxChange(this.useCustomMapScale);
            this._setDimensionSelection();

            this.watch("overlayEnabled", lang.hitch(this, function (name, oldVal, newVal) {
                if (!newVal && this._selectionExtent) {
                    this._removeSelectionExtent();
                }
                else if (newVal && this.width && this.height) {
                    this._placeSelectionExtent(this.width, this.height);
                }
            }));
        },
        _customScaleCheckboxChange: function (checked) {
            this.customScaleTextBox.set("disabled", !checked);
            this.useCustomMapScale = checked;
            if (!checked) {
                if (!this._onExtentChangeHandler) {
                    this._onExtentChangeHandler = on(this.map, "extent-change", lang.hitch(this, this._onExtentChange));
                    this.own(this._onExtentChangeHandler);
                }
                if (this.width && this.height) {
                    this._placeSelectionExtent(this.width, this.height);
                }
            }
            else {
                if (this._onExtentChangeHandler) {
                    this._onExtentChangeHandler.remove();
                    this._onExtentChangeHandler = null;
                }
                this._removeSelectionExtent();
            }
        },
        _setDimensionSelection: function () {
            // summary:
            //      toggles between template and size mode based on form values
            if (this.specifySizeRadio.get("checked")) {
                this._onWidthHeightChange();
                domStyle.set(this.sizeOptions, { display: "block" });
                domStyle.set(this.templateOptions, { display: "none" });
                this.createImageExportFormatOptions();
            }
            else {

                if (this.isExtendedService) {
                    this._onTemplateChange();
                }
                else {
                    this._removeSelectionExtent();
                }
                this._onTemplateChange();
                domStyle.set(this.sizeOptions, { display: "none" });
                domStyle.set(this.templateOptions, { display: "block" });
                this.createTemplateExportFormatOptions();
            }
        },
        _onExtentChange: function (evt) {
            if (evt.levelChange) {
                this.customScaleTextBox.set("value", Math.ceil(this.map.getScale()));
            }
        },

        /***********************************************************************************************
        * createImageExportFormatOptions()
        *   Populates the "Export As:" dropdown list with the supported image formats as defined in the
        *   mapSettings.js file or the default settings passed into the widget.
        ***********************************************************************************************/
        createImageExportFormatOptions: function () {
            this.exportFormatSelect.removeOption(this.exportFormatSelect.getOptions());
            for (var i = 0; i < this.supportedImageFormats.length; i++) {
                this.exportFormatSelect.addOption(this.supportedImageFormats[i]);
            }
        },

        /***********************************************************************************************
        * createTemplateExportFormatOptions()
        *   Populates the "Export As:" dropdown list with the supported image formats as defined for
        *   templates returned by the server for export.
        ***********************************************************************************************/
        createTemplateExportFormatOptions: function () {
            this.exportFormatSelect.removeOption(this.exportFormatSelect.getOptions());
            for (var i = 0; i < this.supportedTemplatedFormats.length; i++) {
                this.exportFormatSelect.addOption(this.supportedTemplatedFormats[i]);
            }
        },

        startup: function () {
            var self = this;

            // set a loading icon just in case
            var loadingSpan = domConstruct.create("span", { "class": "icon-spinner icon-spin" }, this.templateSelect, 'after');
            // Get our templates from the REST endpoint
            var request = new esriRequest({
                url: this.GPServer,
                content: { f: "json" },
                handleAs: "json"
            });

            request.then(
                function (response) {
                    var options = [];
                    // We will implement our own extended GP service that will give us size information in a fudged fashion.
                    var isExtendedService = array.some(response.parameters, function (p) { return p.name === "Size_Array"; });
                    self.isExtendedService = isExtendedService;
                    array.forEach(response.parameters, function (parameter) {
                        if (!isExtendedService && parameter.name === "Layout_Template") {
                            options = array.map(array.filter(parameter.choiceList, function (choice) {
                                return choice !== "MAP_ONLY";
                            }), function (templateName) {
                                return { label: templateName, value: templateName, size: null };
                            });
                        }
                        if (isExtendedService && parameter.name === "Size_Array") {
                            var str = parameter.choiceList;
                            for (var i = 0; i < str.length; i++) {
                                var e = str[i].match(/(.+?)\s-\s(.+)x(.+)/);
                                if (e)
                                    options.push({ label: e[1], value: e[1], size: { width: parseFloat(e[2]), height: parseFloat(e[3]) } });
                            }
                        }
                    });
                    domConstruct.destroy(loadingSpan);
                    self.templateSelect.set("options", options);
                    self.templateSelect.set("value", options[0]);
                },
                function (error) {
                    domConstruct.destroy(loadingSpan);
                    Common.errorDialog(self._serviceErrorMessage)
                        .then(function () { self.destroy(); });

                    throw new Error("Could not resolve the geoprocessing service for " + self.declaredClass);
                }
            );

            this.own(on(this.map, "extent-change", lang.hitch(this, function () {
                if (!this.useCustomMapScale && this._selectionExtent && this.width && this.height) {
                    this._placeSelectionExtent(this.width, this.height);
                }
            })));

            this._placeSelectionExtent(this.width, this.height);

        },
        destroy: function () {
            this.map.graphics.remove(this._selectionExtent);
            this.inherited(arguments);
        },
        _onTemplateChange: function () {
            if (!this.specifySizeRadio.get("checked") && this.isExtendedService) {
                var curSize,
                    curValue = this.templateSelect.value;
                array.forEach(this.templateSelect.options, function (option) {
                    if (option.value === curValue)
                        curSize = option.size;
                }, this);
                if (curSize) {
                    this._placeSelectionExtent(curSize.width, curSize.height);
                    this.sizeInfo.innerHTML = curSize.width + "x" + curSize.height + " (inches)";
                }
            }
        },
        _onWidthHeightChange: function () {
            this._placeSelectionExtent(parseFloat(this.widthTextbox._getDisplayedValueAttr()), parseFloat(this.heightTextbox._getDisplayedValueAttr()));
        },
        _removeSelectionExtent: function () {
            if (this._selectionExtent) {
                this.map.graphics.remove(this._selectionExtent);
                this._selectionExtent = null;
            }
        },
        _placeSelectionExtent: function (w, h) {
            var dpi = this.dpi,
                rawWidth = w,
                rawHeight = h;

            if (isNaN(w) || isNaN(h) || w < 0 || h < 0) {
                return;
            }

            this.width = w;
            this.height = h;
            var centerPt = this.map.toScreen(this.map.extent.getCenter()),
                ext = this.map.extent,
                spatRef = this.map.spatialReference,
                leftX, rightX, bottomY, topY;

            if (!this.specifySizeRadio.get("checked")) {
                //Determine which side is wider and set the appropriate aspect ratio of the geometry.
                if (w > h) {
                    //Get the bounding 'x' values of the extent in terms of screen points.
                    leftX = this.map.toScreen(new Point(ext.xmin, ext.ymin, spatRef)).x + dpi;
                    rightX = this.map.toScreen(new Point(ext.xmax, ext.ymin, spatRef)).x - dpi;

                    //Determine the width in terms of screen points and the height as the value
                    //in screen points which preserves the aspect ratio.
                    w = rightX - leftX;
                    h = this.height * (w / this.width);
                } else {
                    //Get the bounding 'y' values of the extent in terms of screen points.
                    bottomY = this.map.toScreen(new Point(ext.xmin, ext.ymin, spatRef)).y - dpi;
                    topY = this.map.toScreen(new Point(ext.xmin, ext.ymax, spatRef)).y + dpi;

                    //Determine the height in terms of screen points and the width as the value
                    //in screen points which preserves the aspect ratio.
                    h = bottomY - topY;
                    w = this.width * (h / this.height);
                }
            }
            //Define the bottom-left and top-right points for the geometry of the overlay
            var blExtentPt = this.map.toMap(new ScreenPoint(centerPt.x - ((w * dpi) / 2), centerPt.y + ((h * dpi) / 2))),
                trExtentPt = this.map.toMap(new ScreenPoint(centerPt.x + ((w * dpi) / 2), centerPt.y - ((h * dpi) / 2)));


            this.width = w = rawWidth;
            this.height = h = rawHeight;

            if (!this.specifySizeRadio.get("checked")) {
                //If the x-min of the geometry extent is out of bounds of the map extent, then the aspect
                //ratio of the map is such that the aspect ratio of the selected geometry should be scaled down
                if (blExtentPt.x < this.map.extent.xmin) {

                    //Get the bounding 'x' values of the extent in terms of screen points.
                    leftX = this.map.toScreen(new Point(ext.xmin, ext.ymin, spatRef)).x + dpi;
                    rightX = this.map.toScreen(new Point(ext.xmax, ext.ymin, spatRef)).x - dpi;

                    //Determine the width in terms of screen points and the height as the value
                    //in screen points which preserves the aspect ratio.
                    w = rightX - leftX;
                    h = this.height * (w / this.width);

                    //Redefine the bottom-left and top-right points for the geometry of the overlay such that
                    //the overlay is contained entirely within the visible map extent
                    blExtentPt = this.map.toMap(new ScreenPoint(centerPt.x - (w / 2), centerPt.y + (h / 2)));
                    trExtentPt = this.map.toMap(new ScreenPoint(centerPt.x + (w / 2), centerPt.y - (h / 2)));
                } else if (blExtentPt.y < this.map.extent.ymin) {

                    //Get the bounding 'y' values of the extent in terms of screen points.
                    bottomY = this.map.toScreen(new Point(ext.xmin, ext.ymin, spatRef)).y - dpi;
                    topY = this.map.toScreen(new Point(ext.xmin, ext.ymax, spatRef)).y + dpi;

                    //Determine the height in terms of screen points and the width as the value
                    //in screen points which preserves the aspect ratio.
                    h = bottomY - topY;
                    w = this.width * (h / this.height);

                    //Redefine the bottom-left and top-right points for the geometry of the overlay such that
                    //the overlay is contained entirely within the visible map extent
                    blExtentPt = this.map.toMap(new ScreenPoint(centerPt.x - (w / 2), centerPt.y + (h / 2)));
                    trExtentPt = this.map.toMap(new ScreenPoint(centerPt.x + (w / 2), centerPt.y - (h / 2)));
                }
            }

            //If the _selectionExtent property is null, then a new Graphic object must be instatiated with the
            //appropriate geometry. Otherwise, the object exists and its geometry only needs to be updated.
            if (!this._selectionExtent) {
                this._selectionExtent = new Graphic();
                this._selectionExtent.setSymbol(symbolJsonUtils.fromJson(wvsConfig.defaults.symbols.selectionPolygon));
                this._selectionExtent.setGeometry(new Extent(blExtentPt.x, blExtentPt.y, trExtentPt.x, trExtentPt.y, blExtentPt.spatialReference));
            } else {
                this._selectionExtent.setGeometry(new Extent(blExtentPt.x, blExtentPt.y, trExtentPt.x, trExtentPt.y, blExtentPt.spatialReference));
                //HACK: Because we know that a _selectionExtent exists, and we don't necessarily know what called the _placeSelectionExtent function,
                //      it is safest just to remove the graphic initially, then add it if and only if the overlayEnabled property is true.
                this.map.graphics.remove(this._selectionExtent);
            }
            if (this.overlayEnabled) {
                this.map.graphics.add(this._selectionExtent);
            }
        },
        _startExport: function () {
            var self = this;
            esriRequest({
                url: self.tokenUrl
            }).then(function (resp) {
                self.token = JSON.parse(resp).token;
                self._export();
            });
        },

        _export: function () {
            // summary:
            //      takes all of the form values and begins the print export
            var self = this;
            // create template
            var template = new PrintTemplate();
            if (this.specifySizeRadio.get("checked")) {
                var width = parseInt(this.widthTextbox.get("value")),
                    height = parseInt(this.heightTextbox.get("value")),
                    dpi = this.dpi;
                if (isNaN(width) || isNaN(height) || width <= 0 || height <= 0) {
                    throw new Error("Invalid width or height. They must be numbers above 0");
                }
                else {
                    // Convert inches to pixels
                    width = width * dpi;
                    height = height * dpi;
                }

                template.exportOptions = {
                    width: width,
                    height: height,
                    dpi: dpi
                };
                template.layout = "MAP_ONLY";
            }
            else {
                template.layout = this.templateSelect.get("value");
                template.layoutOptions = {};
                //template.layoutOptions.scalebarUnit = this.scaleBarUnitSelect.get("value");
                template.layoutOptions.legendLayers = [];
                var lyrArray = [];
                if (this.includeLegend.get("checked")) {
                    var lyrs = this.map.getLayersVisibleAtScale();
                    for (var i = 0; i < lyrs.length; i++) {
                        lyrArray.push(lyrs[i].id);
                    }
                    for (var i = 0; i < lyrs.length; i++) {
                        console.warn(lyrs[i]);
                        if (lyrs[i].declaredClass == "wvs.layers.MarkupLayer") {
                            for (var j = 0; j < lyrs[i].items.data.length; j++) {
                                if (lyrs[i].items.data[j].graphic != undefined &&
                                    lyrs[i].items.data[j].graphic != null) {
                                    var mkupId = lyrs[i].items.data[j].graphic.getLayer().id;
                                    for (var k = 0; k < lyrArray.length; k++) {
                                        if (lyrArray[k] === mkupId) {
                                            lyrArray.splice(k, 1);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    for (var i = 0; i < lyrArray.length; i++) {
                        var lyr = new LegendLayer();
                        lyr.layerId = lyrArray[i];
                        template.layoutOptions.legendLayers.push(lyr);
                    }
                }
                console.warn(template.layoutOptions.legendLayers);
                if (this.titleTextbox.get("value")) {
                    template.layoutOptions.titleText = this.titleTextbox.get("value");
                }
            }

            template.format = this.exportFormatSelect.get("value");

            // create params
            var params = new PrintParameters();
            params.map = this.map;
            params.outSpatialReference = this.map.spatialReference; //new SpatialReference(4326);//
            params.template = template;

            console.log("print params = ", params);

            var customScale = parseFloat(this.customScaleTextBox.get("value"));
            customScale = this.useCustomMapScale && !isNaN(customScale) && customScale <= this.map.getMinScale() && customScale >= this.map.getMaxScale() ? customScale : null;


            //In order to make sure that both point and text graphics print, we need to hold the two feature types in two separate layers in a featureCollection.
            var ensureTextSymbolsSupported = function (webmap) {

                //The new layer to be added to the featureCollection for operationalLayers that have text features and point features.
                var newOperationalLayer = null;
                array.forEach(webmap.operationalLayers, function (operationalLayer) {
                    if (operationalLayer.featureCollection && operationalLayer.featureCollection.layers.length) {
                        array.forEach(operationalLayer.featureCollection.layers, function (fl) {
                            if ((!fl.layerDefinition.fields || !fl.layerDefinition.fields.length) && fl.featureSet.features.length && array.some(fl.featureSet.features, function (feature) { return (typeof feature.attributes !== "undefined" && typeof feature.attributes.description !== "undefined"); })) {
                                fl.layerDefinition.fields = [{ name: "description", alias: "description", type: "esriFieldTypeString" }];
                            }

                            //If the layer is a "Point" layer, then we need to separate out text features from point features, or else the print will not display properly.
                            if (fl.layerDefinition.geometryType && fl.layerDefinition.geometryType === "esriGeometryPoint") {
                                //Cycle through the feature set to find any that are of type "esriTS"
                                for (var i = 0; i < fl.featureSet.features.length; i++) {
                                    if (fl.featureSet.features[i].symbol && fl.featureSet.features[i].symbol.type === "esriTS") {
                                        //If there are TextSymbols in the layer, and the new layer hasn't been created yet, create it as a clone of the current layer,
                                        //but with no features in the featureSet.
                                        if (newOperationalLayer === null) {
                                            newOperationalLayer = ObjUtil.clone(fl);
                                            newOperationalLayer.featureSet.features = [];
                                            newOperationalLayer.layerDefinition.name = "textLayer";
                                        }
                                        //Push the Text feature into the new dummy layer.
                                        newOperationalLayer.featureSet.features.push(ObjUtil.clone(fl.featureSet.features[i]));
                                        //Remove the Text feature from the original feature layer.
                                        fl.featureSet.features.splice(i, 1);
                                    }
                                }
                            }
                        });

                        //If there were Text features, the newOperationalLayer will have been created and populated, and so should be added to the featureCollection layers array.
                        if (newOperationalLayer !== null) {
                            operationalLayer.featureCollection.layers.push(newOperationalLayer);
                        }
                    }
                });
            };


            if (this.replaceMap.length) {
                var replaceMap = this.replaceMap;
                var replaceUrl = function (ioArgs) {
                    if (ioArgs == undefined || ioArgs == null) { return; }
                    if (esriLang.isDefined(ioArgs.content["Web_Map_as_JSON"])) {
                        var webmapJson = ioArgs.content["Web_Map_as_JSON"],
                            token;
                        array.forEach(replaceMap, function (r) {
                            webmapJson = webmapJson.replace(new RegExp(r.oldText.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), "g"), r.newText);
                        });
                        if (typeof customScale === "number") {
                            var webmap = JSON.parse(webmapJson);
                            webmap.mapOptions.scale = customScale;
                            webmapJson = JSON.stringify(webmap);
                        } else {
                            var webmap = JSON.parse(webmapJson);
                            webmap.mapOptions.extent = self._selectionExtent;
                            webmapJson = JSON.stringify(webmap);
                        }
                        ensureTextSymbolsSupported(webmap);
                        for (var i = 0; i < webmap.operationalLayers.length; i++) {
                            if (webmap.operationalLayers[i].url == undefined) { continue; }
                            var proxyRule = urlUtils.getProxyRule(webmap.operationalLayers[i].url);
                            if (proxyRule != undefined) {
                                webmap.operationalLayers[i].url += "?token=" + self.token;
                            }
                        }
                        ioArgs.content["Web_Map_as_JSON"] = webmapJson;
                        return ioArgs;
                    }
                    return ioArgs;
                };
                esriRequest.setRequestPreCallback(replaceUrl);
            }
            else if (typeof customScale === "number") {
                var updateScale = function (ioArgs) {
                    if (ioArgs == undefined || ioArgs == null) { return; }
                    if (esriLang.isDefined(ioArgs.content["Web_Map_as_JSON"])) {
                        var webmap = JSON.parse(ioArgs.content["Web_Map_as_JSON"]);
                        ensureTextSymbolsSupported(webmap);
                        webmap.mapOptions.scale = customScale;
                        for (var i = 0; i < webmap.operationalLayers.length; i++) {
                            if (webmap.operationalLayers[i].url == undefined) { continue; }
                            var proxyRule = urlUtils.getProxyRule(webmap.operationalLayers[i].url);
                            if (proxyRule != undefined) {
                                webmap.operationalLayers[i].url += "?token=" + self.token;
                            }
                        }
                        ioArgs.content["Web_Map_as_JSON"] = JSON.stringify(webmap);
                        return ioArgs;
                    }
                    return ioArgs;
                }
                esriRequest.setRequestPreCallback(updateScale);
            }
                // TODO: Remove this when we find a better way to handle this.
                // The bug occurs when the "webmap" is serialized and sent over the pipe
                // A GraphicsLayer (which markup uses) becomes a FeatureCollection. This works fine.
                // However, if there is a text symbol then Server complains (rather the PrintTask functions) that a layerDefinition is required
                // We will add these field definitions where appropriate.
            else {
                var updateScale = function (ioArgs) {
                    if (ioArgs == undefined || ioArgs == null) { return; }
                    if (esriLang.isDefined(ioArgs.content["Web_Map_as_JSON"])) {
                        var webmap = JSON.parse(ioArgs.content["Web_Map_as_JSON"]);
                        for (var i = 0; i < webmap.operationalLayers.length; i++) {
                            if (webmap.operationalLayers[i].url == undefined) { continue; }
                            var proxyRule = urlUtils.getProxyRule(webmap.operationalLayers[i].url);
                            if (proxyRule != undefined) {
                                webmap.operationalLayers[i].url += "?token=" + self.token;
                            }
                        }
                        ensureTextSymbolsSupported(webmap);
                        ioArgs.content["Web_Map_as_JSON"] = JSON.stringify(webmap);
                        return ioArgs;
                    }
                    return ioArgs;
                }
                esriRequest.setRequestPreCallback(updateScale);
            }


            // create & execute print task

            var printCallback = function (dataFile) {
                domStyle.set(self.exportButton.domNode, { display: "none" });
                self._exportLink = dataFile.url;
                domStyle.set(self.exportLink, { display: "inline" });
                // TODO: Uncomment this line when the esri bug is gone
                //if (self.replaceMap.length || typeof customScale === "number")
                esriRequest.setRequestPreCallback(function (ioArgs) { return ioArgs; });
            };

            var printErrback = function (error) {
                self.exportButton.set("iconClass", "");
                self.exportButton.set("disabled", false);
                Common.errorDialog(error.message);
                // TODO: Uncomment this line when the esri bug is gone
                //if (self.replaceMap.length || typeof customScale === "number")
                esriRequest.setRequestPreCallback(function () { return ioArgs; });
            };


            this.exportButton.set("iconClass", "icon-spinner icon-spin");
            this.exportButton.set("disabled", true);
            printTask = PrintTask(this.GPServer);
            printTask.execute(params, printCallback, printErrback);
        }
    });
});