console.log("wvs/dijits/TocImageServiceNode.js");

define([
    "dojo/_base/declare"
    , "dojo/text!./templates/_TocNode.html"
    , "dojo/_base/lang"
    , "./_TocNode"
    , "dijit/form/HorizontalSlider"
    , "dojo/dom-style"
    , "dojo/ready"
    , "dojo/dom-construct"
    , "dojo/dom-attr"
    , "dojo/on"
    , "dijit/Menu"
    , "dijit/MenuItem"
    , "dijit/MenuSeparator"
    , "dijit/PopupMenuItem"
    , "dijit/TooltipDialog"
    , "dojo/_base/array"
    , "dojo/dom"
    , "dojo/has"
    , "dojo/dom-class"
], function (declare, template, lang, _TocNode, HorizontalSlider, domStyle, ready, domConstruct, domAttr, on, Menu, MenuItem, MenuSeparator, PopupMenuItem, TooltipDialog, array, dom, has, domClass) {
    return declare([_TocNode], {

        templateString: template,

        declaredClass: "wvs.dijits.TocImageServiceNode",

        // refreshDelay: Integer
        //      Number of milliseconds to delay between clicking a checkbox and refreshing the map service. A longer delay allow the user to check/uncheck multiple
        //      layers before refreshing the map service."dojo/ready",
        refreshDelay: 500,

        // useSpriteImage: Boolean
        //      Use the custom sprite image for the legend icons. When false, the "Legend" REST service will be used for 10.01 and newer services.
        useSpriteImage: true,

        hasDropDown: true,

        constructor: function (params, srcNodeRef) {
            params = params || {};
            if (!params.mapService) {
                throw new Error('mapService not defined in params for ' + this.declaredClass);
            }
            if (!params._parent) {
                throw new Error('no reference to the toc found for ' + this.declaredClass);
            }

            lang.mixin(this, params);

            if (this.title === null) {
                if (this.mapService.name) {
                    this.title = this.mapService.name;
                } else {
                    // call the extension method in jstoolkit/layers/Layer.js to get the service name
                    this.title = this.mapService.getServiceName();
                }
            }
            this.maxScale = this.mapService.maxScale;
            this.minScale = this.mapService.minScale;
        },
        // extension point called by framework
        postCreate: function () {
            // Set transparency
            var transparency = (1 - this.mapService.opacity).toPrecision(1);
            this.transparency = transparency;
            // Context menu
            this._createContextMenu();
            // Call super
            this.inherited(arguments);
        },
        setVisible: function (visible) {
            // return if the visibility has not changed
            if (this._state.visible == visible) return;

            this._state.visible = visible;

            // update the map service
            if (visible) {
                this.mapService.show();
                this._showSwatch();
                this._enableChildNodes();
            } else {
                this.mapService.hide();
                this._hideSwatch();
                this._disableChildNodes();
            }
            this._toggleCheckbox();
        },
        _checkboxOnClick: function (e) {
            this.setVisible(this._checkbox && this._checkbox.checked);
        },
        _refreshLayer: function () {
            if (this._refreshTimer) {
                window.clearTimeout(this._refreshTimer);
                this._refreshTimer = null;
            }

            var mapService = this.mapService;

            this._refreshTimer = window.setTimeout(function () {
                mapService.refresh();
            }, this.refreshDelay);
        },
        _createContextMenu: function () {
            var self = this;

            var mapServiceOptionsMenu = new Menu({
                leftClickToOpen: true,
                onFocus: this.isFromWebMap ? function() {} : function () {
                    var children = this.getChildren(),
                        nodeBottom = self.toc.isBottomNode(self),
                        nodeTop = self.toc.isTopNode(self);

                    array.forEach(children, function (entry, i) {
                        if (entry.label) {
                            switch (entry.label) {
                                case "Move Up":
                                    entry.set("disabled", nodeTop); break;
                                case "Move Down":
                                    entry.set("disabled", nodeBottom); break;
                                case "Move To Top":
                                    entry.set("disabled", nodeTop); break;
                                case "Move To Bottom":
                                    entry.set("disabled", nodeBottom); break;
                            }
                        }
                    });
                }
            });


            mapServiceOptionsMenu.addChild(_TocNode.ContextMenuItems.Transparency(this));


            if (!this.isFromWebMap) {
                mapServiceOptionsMenu.addChild(new MenuSeparator());
                mapServiceOptionsMenu.addChild(new MenuItem({
                    label: "Remove",
                    iconClass: "jstoolkit-icon jstoolkit-icon-delete",
                    onClick: lang.hitch(this.toc, this.toc._removeServiceLayer, this.mapService)
                }));

                var _moveUpMenuItem = new MenuItem({
                    label: "Move Up",
                    iconClass: "jstoolkit-icon jstoolkit-icon-arrow-up",
                    onClick: lang.hitch(self.toc, self.toc._moveNodeUp, self)
                });

                mapServiceOptionsMenu.addChild(_moveUpMenuItem);

                var _moveToTopMenuItem = new MenuItem({
                    label: "Move To Top",
                    iconClass: "jstoolkit-icon jstoolkit-icon-double-angle-up",
                    onClick: lang.hitch(self.toc, self.toc._moveNodeToTop, self)
                });

                mapServiceOptionsMenu.addChild(_moveToTopMenuItem);


                var _moveDownMenuItem = new MenuItem({
                    label: "Move Down",
                    iconClass: "jstoolkit-icon jstoolkit-icon-arrow-down",
                    onClick: lang.hitch(self.toc, self.toc._moveNodeDown, self)
                });
                mapServiceOptionsMenu.addChild(_moveDownMenuItem);

                var _moveToBottomMenuItem = new MenuItem({
                    label: "Move To Bottom",
                    iconClass: "jstoolkit-icon jstoolkit-icon-double-angle-down",
                    onClick: lang.hitch(self.toc, self.toc._moveNodeToBottom, self)
                });

                mapServiceOptionsMenu.addChild(_moveToBottomMenuItem);
            }

            mapServiceOptionsMenu.bindDomNode(this.dropDownMenuIcon);
            mapServiceOptionsMenu.startup();
            this.mapServiceOptionsMenu = mapServiceOptionsMenu;
        }

    });
});