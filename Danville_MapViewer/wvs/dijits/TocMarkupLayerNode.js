/// <reference path="../../app/mapSettings.js" />
console.log("wvs/dijits/TocMarkupLayerNode.js");

define([
    "dojo/_base/declare"
    , "dojo/text!./templates/_TocNode.html"
    , "dojo/_base/lang"
    , "dojo/_base/connect"
    , "./_TocNode"
    , "dijit/form/HorizontalSlider"
    , "dojo/dom-style"
    , "dojo/dom-construct"
    , "dojo/dom-attr"
    , "dojo/on"
    , "../common/Common"
    , "../common/ObjUtil"
    , "dijit/Menu"
    , "dijit/MenuItem"
    , "dijit/MenuSeparator"
    , "dijit/PopupMenuItem"
    , "dijit/TooltipDialog"
    , "dojo/_base/array"
    , "dojo/dom-class"
    , "dojo/has"
    , "dojo/dom"
    , "esri/geometry/Polygon"
    , "esri/geometry/Polyline"
    , "esri/geometry/Point"
    , "esri/graphicsUtils"
    , "../extensions/esri/graphicsUtils"
    , "dijit/form/TextBox"
    , "dojox/gfx"
    , "../common/MarkupManager"
    , "dojo/_base/sniff"
], function (declare, template, lang, connect, _TocNode, HorizontalSlider, domStyle, domConstruct, domAttr, on, Common, ObjUtil, Menu, MenuItem, MenuSeparator, PopupMenuItem, TooltipDialog, array, domClass, has, dom, Polygon, Polyline, Point, graphicsUtils, TextBox, gfx, MarkupManager) {
    var _TocFolderNode,
        _TocGraphicsNode;
    // There seem to be garbage collection issues with using a module inside a nested declare.
    // These requires ensure that references don't get lost. They are at the top as well so this isn't a real conditional require and the dependencies would go into the build
    require(["wvs/common/Common", "dijit/form/TextBox", "esri/graphicsUtils", "dojox/gfx", "wvs/common/MarkupManager"], function (Common, TextBox, graphicsUtils, gfx, MarkupManager) {
        _TocFolderNode = declare([_TocNode], {

            declaredClass: 'wvs.dijits._TocFolderNode',

            isRoot: false,

            hasDropDown: true,

            hasMarkupService: null,

            constructor: function (params, srcNodeRef) {
                console.log(this.declaredClass + " constructor");

                params = params || {};

                if (!params.markupLayer && !params.mapService) {
                    throw new Error('markup layer not defined in params for ' + this.declaredClass);
                }
                if (params.item && params.item.type !== "root" && !params.markupLayer) {
                    throw new Error('item must be defined in params if this is not the root for ' + this.declaredClass);
                }
                // We on our root node
                if (params.mapService && !params.item) {
                    this.markupLayer = params.mapService;
                    this.item = params.mapService.items.query({ id: "root" })[0];
                }
                lang.mixin(this, params);
                this.title = this.item.name;

                this.isRoot = this.item.id === "root" ? true : false;

                // Check if we have a markup service
                // Markup Manager throws an error if there is no global markup service defined
                this.hasMarkupService = false;
                try {
                    var mm = new MarkupManager();
                    this.hasMarkupService = true;
                }
                catch (err) {
                    this.hasMarkupService = false;
                }
            },
            postCreate: function () {
                console.log(this.declaredClass + " postCreate");
                this.inherited(arguments);

                // We have an icon
                domClass.remove(this.iconNode, "jstoolkit-hidden");

                // Create an input to edit label
                // On the Enter Key: Update the labelNode and the item and display it
                var editLabelNode = new TextBox({
                    style: "display:none;",
                    trim: true,
                    onKeyPress: lang.hitch(this, function (event) {
                        // On Enter & has a value: Rename the node
                        if (event.which === 13) {
                            var curValue = editLabelNode._getDisplayedValueAttr();
                            if (!curValue) {
                                Common.errorDialog("This item must have a name!");
                            }
                            else {
                                this.item.rename(curValue);
                                domStyle.set(this.editLabelNode.domNode, { display: "none" });
                                this.labelNode.innerHTML = curValue;
                                domStyle.set(this.labelNode, { display: "inline" });
                            }
                        }
                    })

                });
                this.editLabelNode = editLabelNode;
                this.editLabelNode.placeAt(this.labelNode, 'after');

                // Create the children
                this._createChildNodes();

                // Create the context menu
                domClass.remove(this.dropDownMenuIcon, "jstoolkit-hidden");
                this._createContextMenu();

                this.setNodeStyle();
                this._setEvents();
                this._initiateSubscriptions();
            },
            destroy: function () {
                array.forEach(this._subscriptions, function (sub) {
                    sub.remove();
                });
                this.inherited(arguments);
            },
            _setEvents: function () {
                var self = this;
                var graphicRemove = on(this.markupLayer, "graphic-remove", function (evt) {
                    var item = evt.graphic;
                    for (var i = 0, il = self.nodes.length; i < il; i++) {
                        if (item.id === self.nodes[i].item.id) {
                            self.nodes[i].destroy();
                            self.nodes.splice(i, 1);
                            break;
                        }
                    }
                });
                var graphicAdd = on(this.markupLayer, "graphic-add", function (evt) {
                    var item = evt.graphic;
                    if (item.parent === self.item.id) {
                        var node = new _TocGraphicsNode({
                            _parent: self,
                            item: item,
                            markupLayer: self.markupLayer,
                            map: self.map
                        });
                        node.startup();
                        node.placeAt(self.childrenNode);
                        self.nodes.push(node);
                    }
                });

                this.own(graphicRemove, graphicAdd);
            },
            _initiateSubscriptions: function () {
                var self = this;
                var onPaste = connect.subscribe("markup/item/paste", function (obj) {
                    // Our buffer has been cleared due to a cut/paste
                    if (self.markupLayer.operationBuffer.operation === "paste") {
                        self.paste.set("disabled", true);
                        var item = self.markupLayer.operationBuffer.item,
                            pastedNode = self.markupLayer.operationBuffer.node;
                        for (var i = 0, il = self.nodes.length; i < il; i++) {
                            var node = self.nodes[i];
                            if (node === pastedNode && node.parent !== self.item.id) {
                                self.nodes.splice(i, 1);
                                break;
                            }
                        }
                    }
                });

                var onCopyCut = function (obj) {
                    if (obj.item !== self.item) {
                        self.paste.set("disabled", false);
                    }
                };


                var onCopy = connect.subscribe("markup/item/copy", onCopyCut);
                var onCut = connect.subscribe("markup/item/cut", onCopyCut);
                this._subscriptions = [onCopy, onCut, onPaste];
            },
            _toggleChildrenNode: function () {
                this.inherited(arguments);
                var en = this.expandoNode;
                if (domClass.contains(en, 'jstoolkit-icon-minus-sign')) {
                    domClass.add(this.iconNode, "icon-folder-open");
                    domClass.remove(this.iconNode, "icon-folder-close");
                }
                else {
                    domClass.add(this.iconNode, "icon-folder-close");
                    domClass.remove(this.iconNode, "icon-folder-open");
                }

            },
            /* Start Composite Interface Functions*/
            setNodeStyle: function () {
                // Show our folder status
                if (!this.isRoot) {
                    if (this.nodes.length) {
                        domClass.remove(this.expandoNode, "jstoolkit-hidden");
                        domClass.add(this.iconNode, "icon-folder-open");
                    }
                    else
                        domClass.add(this.iconNode, "icon-folder-close");
                }
                else
                    domClass.remove(this.expandoNode, "jstoolkit-hidden");
            },
            renameItem: function () {
                // Display & Focus the Edit Label
                domStyle.set(this.editLabelNode.domNode, { display: "inline-block" });
                domStyle.set(this.labelNode, { display: "none" });
                this.editLabelNode.focus();
            },
            addItem: function () {
                // Add a Folder
                var nf = this.markupLayer.addFolder("New Folder", this.item.id);

                // Create the Toc Node
                var node = new _TocFolderNode({
                    item: nf,
                    _parent: this,
                    markupLayer: this.markupLayer,
                    map: this.map
                });
                node.startup();
                node.placeAt(this.childrenNode, 'first');
                this.nodes.push(node);


                // Display & Focus the Edit Label
                node.renameItem();
            },

            zoomToItem: function () {
                // Get all graphics that are children/ancestors of this folder
                var graphics = array.map(
                    array.filter(this.markupLayer._getAncestors(this.item.id), function (child) {
                        return child.type === "graphic";
                    }), function (filteredItem) { return filteredItem.graphic; });
                if (graphics.length > 0) {
                    if (graphics.length === 1 && graphics[0].geometry.type === "point")
                        this.map.setExtent(graphicsUtils.pointToExtent(graphics[0].geometry), true);
                    else
                        this.map.setExtent(graphicsUtils.graphicsExtent(graphics), true);
                }
            },
            cutItem: function () {
                // Publish markup/item/cut topic (this will be handled by MarkupLayerNode and whoever else is interested. The map, eh? )
                connect.publish("markup/item/cut", { layer: this.markupLayer, item: this.item, node: this });
                // Apply styling to the node
            },
            copyItem: function () {
                // Publish markup/item/copy topic (this will be handled by MarkupLayerNode and whoever else is interested. The map, eh? )
                connect.publish("markup/item/copy", { layer: this.markupLayer, item: this.item, node: this });
            },
            pasteItem: function () {
                // if we have an item in the buffer
                var op = this.markupLayer.operationBuffer,
                    self = this;
                if (op) {
                    if (op.item.type === "folder") {
                        //Recursively build folder nodes for each child folder being copied over as a member
                        //of the parent folder.
                        //Something about the way the folders/parents are being removed. If you rename or remove a graphic in a copied folder structure, everything works just fine
                        //however if you remove the folder, the items are removed from the directory that the removed folder was copied FROM as well.
                        //TODO: Fix this bug.
                        var createFolderStructure = function (folderNode, parentNode, root) {
                            var newItem = ObjUtil.clone(folderNode.item);
                            var node = new _TocFolderNode({
                                item: newItem,
                                _parent: folderNode._parent,
                                markupLayer: self.markupLayer,
                                map: self.map
                            });
                            node.startup();
                            node.placeAt(parentNode, 'last');
                            root.nodes.push(node);
                            var children = folderNode.nodes;

                            //Necessary to use array.forEach here to prevent any scoping issues with for-loop variables
                            //in recursive function calls.
                            array.forEach(children, function (child) {
                                if (child.declaredClass === "wvs.dijits._TocFolderNode") {
                                    createFolderStructure(child, node.childrenNode, folderNode);
                                }
                            }, this);
                        }
                        createFolderStructure(op.node, this.childrenNode, this);
                        self.markupLayer.pasteFolder(op.item, op.layer, self.item.id);
                        if (op.operation === "cut") {
                            // Move the folder in our data structure
                            // Destroy the original node
                            op.node.destroy();
                            //Manage data structures within the MarkupLayer objects.
                            op.layer.removeFolder(op.item.id);
                        }
                    } else if (op.item.type === "graphic") {
                        if (op.operation === "cut") {
                            // Move the folder in our data structure
                            op.layer.removeGraphic(op.item.id);
                            this.markupLayer.pasteGraphic(op.item.graphic, this.item.id);
                        } else if (op.operation === "copy") {
                            // Copy the folder in our data structure
                            this.markupLayer.pasteGraphic(op.item.graphic, this.item.id);
                        }
                    }
                }
            },
            saveItem: function () {
                if (this.isRoot) {
                    var children = this.markupLayer.items.query({ parent: this.item.id });
                    if (children.length === 0) {
                        Common.errorDialog("There is nothing to save");
                    } else {
                        var mm = new MarkupManager();
                        mm.saveMarkupCollection(this.markupLayer).then(
                            function (message) {
                                alert(message);
                            },
                            function (message) {
                                Common.errorDialog(message, "Markup Layer Action");
                            }
                        );
                    }
                }
            },

            removeItem: function () {
                var removeMessage = "Are you sure you want to remove this from the map?";
                // Remove the folder
                Common.confirmDialog(removeMessage).then(
                    lang.hitch(this, function (confirmed) {
                        if (confirmed) {
                            if (this.isRoot) {
                                this._parent._removeServiceLayer(this.markupLayer);
                            }
                            else {
                                this.item.remove();
                            }
                            this.destroy();
                        }
                    }));
            },

            deleteItem: function () {

                var deleteMessage = "Are you sure you want to delete this item?";
                if (this.hasMarkupService && typeof this.markupLayer._id === "number")
                    deleteMessage += " This will also remove this markup collection from the server.";

                // Remove the folder
                Common.confirmDialog(deleteMessage).then(
                    lang.hitch(this, function (confirmed) {
                        if (confirmed) {
                            if (this.isRoot) {
                                if (this.hasMarkupService && typeof this.markupLayer._id === "number") {
                                    var mm = new MarkupManager();
                                    mm.deleteMarkupCollection(this.markupLayer._id).then(
                                        lang.hitch(this, function (response) {
                                            if (response.status === "SUCCESS")
                                                this._parent._removeServiceLayer(this.markupLayer);
                                            else
                                                Common.errorDialog(response.message);
                                        }),
                                        lang.hitch(this, function (err) {
                                            Common.errorDialog(err.message);
                                        })
                                      )
                                }
                                else {
                                    this._parent._removeServiceLayer(this.markupLayer);
                                }

                            }
                            else {
                                this.item.remove();
                            }
                            this.destroy();
                        }
                    }));
            },
            /* End Composite Interface Functions*/

            setVisible: function (visible) {
                // return if the visibility has not changed
                if (this._state.visible == visible) return;

                this._state.visible = visible;

                // update the map service
                if (visible) {
                    this.item.show();
                    this._enableChildNodes();
                    this._showSwatch();
                } else {
                    this.item.hide();
                    this._disableChildNodes();
                    this._hideSwatch();
                }
            },
            _createChildNodes: function () {
                var children = this.markupLayer.items.query({ parent: this.item.id });
                array.forEach(children, function (child) {
                    if (child.type === "folder") {
                        var node = new _TocFolderNode({
                            item: child,
                            _parent: this,
                            markupLayer: this.markupLayer,
                            map: this.map
                        });
                    }
                    else {
                        var node = new _TocGraphicsNode({
                            _parent: this,
                            item: child,
                            markupLayer: this.markupLayer,
                            map: this.map
                        });
                    }
                    node.startup();
                    node.placeAt(this.childrenNode);
                    this.nodes.push(node);
                }, this);
            },
            _checkboxOnClick: function (e) {
                this.setVisible(this._checkbox && this._checkbox.checked);
            },
            _createContextMenu: function () {
                var self = this;

                var mapServiceOptionsMenu = new Menu({
                    leftClickToOpen: true
                });

                if (this.item.type === "folder") {
                    // New Folder
                    // Status: Complete
                    var newFolder = new MenuItem({
                        label: "New Folder",
                        iconClass: "jstoolkit-icon icon-folder-close",
                        title: "",
                        onClick: function () {
                            self.addItem();
                        }
                    });
                    mapServiceOptionsMenu.addChild(newFolder);
                    this.newFolder = newFolder;
                }

                // Rename
                // Status: Complete
                var rename = new MenuItem({
                    label: "Rename",
                    iconClass: "jstoolkit-icon icon-edit",
                    title: "",
                    onClick: function () {
                        self.renameItem();
                    }
                });
                mapServiceOptionsMenu.addChild(rename);
                this.rename = rename;

                // Menu Separator
                mapServiceOptionsMenu.addChild(new MenuSeparator({}));

                // Zoom to
                // Disabled When: if no graphics
                var zoomTo = new MenuItem({
                    label: "Zoom To",
                    iconClass: "jstoolkit-icon icon-search",
                    title: "",
                    onClick: function () {
                        self.zoomToItem();
                    }
                });
                this.zoomTo = zoomTo;
                mapServiceOptionsMenu.addChild(zoomTo);

                // Menu Separator
                mapServiceOptionsMenu.addChild(new MenuSeparator({}));


                if (!this.isRoot) {
                    // Cut
                    var cut = new MenuItem({
                        label: "Cut",
                        iconClass: "jstoolkit-icon icon-cut",
                        title: "",
                        onClick: function () {
                            self.cutItem();
                        }
                    });
                    mapServiceOptionsMenu.addChild(cut);
                    this.cut = cut;

                    // Copy
                    var copy = new MenuItem({
                        label: "Copy",
                        iconClass: "jstoolkit-icon icon-copy",
                        title: "",
                        onClick: function () {
                            self.copyItem();
                        }
                    });
                    mapServiceOptionsMenu.addChild(copy);
                    this.copy = copy;
                }

                if (this.item.type === "folder") {
                    // Paste
                    var paste = new MenuItem({
                        label: "Paste",
                        disabled: self.markupLayer.operationBuffer ? false : true,
                        iconClass: "jstoolkit-icon icon-paste",
                        title: "",
                        onClick: function () {
                            self.pasteItem();
                        }
                    });
                    mapServiceOptionsMenu.addChild(paste);
                    this.paste = paste;
                }

                // Separator
                mapServiceOptionsMenu.addChild(new MenuSeparator({}));


                if (this.hasMarkupService && this.isRoot) {
                    // Save
                    // Complete
                    var save = new MenuItem({
                        label: "Save",
                        iconClass: "jstoolkit-icon jstoolkit-icon-save",
                        title: "Save this markup collection to the server",
                        onClick: function () {
                            self.saveItem();
                        }
                    });
                    mapServiceOptionsMenu.addChild(save);
                    this.save = save;
                }

                var remove = new MenuItem({
                    label: "Remove",
                    iconClass: "jstoolkit-icon jstoolkit-icon-minus",
                    title: "Remove this markup from the map",
                    onClick: function () {
                        self.removeItem();
                    }
                });

                mapServiceOptionsMenu.addChild(remove);
                this.remove = remove;

                // Remove
                // Complete
                var deleteMenuItem = new MenuItem({
                    label: "Delete",
                    iconClass: "jstoolkit-icon jstoolkit-icon-delete",
                    title: "Delete this folder from the map and server",
                    onClick: function () {
                        self.deleteItem();
                    }
                });
                mapServiceOptionsMenu.addChild(deleteMenuItem);
                this.deleteMenuItem = deleteMenuItem;

                mapServiceOptionsMenu.bindDomNode(this.dropDownMenuIcon);
                mapServiceOptionsMenu.startup();
                this.mapServiceOptionsMenu = mapServiceOptionsMenu;
            }
        });

        _TocGraphicsNode = declare([_TocFolderNode], {
            declaredClass: 'wvs.dijits._TocGraphicsNode',
            // We have no events
            _setEvents: function () { },
            // We have no subscriptions
            _initiateSubscriptions: function () {
                var self = this;
                var onModify = function (topic) {

                    console.log("tocMarkupLayerNode.onModify->'markup/item/modify' received");

                    var graphics = self.markupLayer.items.query(function (obj) {
                        return obj.id === self.item.id && obj.type === "graphic" && obj.graphic === topic.item;
                    });
                    if (graphics.length) {
                        self.item.rename(graphics[0].graphic.getTitle());
                        self.labelNode.innerHTML = graphics[0].graphic.getTitle();
                        self._createIcon();
                    }
                };
                var onModify = connect.subscribe("markup/item/modify", onModify);
                this._subscriptions = [onModify];
            },
            /* Start OVERRIDEN Composite Interface Functions*/
            setNodeStyle: function () {
                this._createIcon();
            },
            getName: function () {
                return this.item.name;
            },
            renameItem: function () {
                // Display & Focus the Edit Label
                domStyle.set(this.editLabelNode.domNode, { display: "inline-block" });
                domStyle.set(this.labelNode, { display: "none" });
                this.editLabelNode.focus();
            },
            // Not implemented for 'leaf' nodes
            addItem: function () { return; },
            zoomToItem: function () {
                if (this.item.graphic.geometry.declaredClass === "esri.geometry.Point") {
                    this.map.setExtent(graphicsUtils.pointToExtent(this.item.graphic.geometry), true);
                }
                else {
                    this.map.setExtent(this.item.graphic.geometry.getExtent(), true);
                }
            },
            // Not implemented for 'leaf' nodes
            pasteItem: function () { return; }
            /* End OVERRIDEN Composite Interface Functions*/,
            _createIcon: function () {
                var geometry = this.item.graphic.geometry,
                    symbol = this.item.graphic.symbol;

                domConstruct.empty(this.iconNode);
                // If we have an image, just make the image from the source URL
                if (symbol.declaredClass === "esri.symbol.PictureMarkerSymbol") {
                    domConstruct.create("img", { src: symbol.url }, this.iconNode);
                    domClass.remove(this.iconNode, "jstoolkit-hidden");
                    return;
                }
                else {
                    var w = 20,
                        h = 20,
                        symbolDiv = domConstruct.create("div", { display: "inline" }),
                        surface = gfx.createSurface(symbolDiv, w, h);

                    if (geometry.isInstanceOf(Polygon)) {
                        var rect = surface.createRect({ x: 0, y: 0, width: w, height: h });
                        rect.setFill(symbol.getFill());
                        rect.setStroke(symbol.getStroke());
                    }
                    else if (geometry.isInstanceOf(Polyline) && !has("ie") || has("ie") > 8) {
                        var line = surface.createLine({ x1: 0, y1: h / 2, x2: w, y2: h / 2 });
                        line.setStroke(symbol.getStroke());
                    }
                    else if (geometry.isInstanceOf(Point)) {
                        // TODO: Finish implemnting point
                        var circle = surface.createCircle({ cx: w / 2, cy: h / 2, r: w / 2 });
                        circle.setFill(symbol.getFill());
                        circle.setStroke(symbol.getStroke());
                    }
                    domConstruct.place(symbolDiv, this.iconNode);
                    domClass.remove(this.iconNode, "jstoolkit-hidden");
                }
            }
        });
    });

    return _TocFolderNode;
});