﻿define([
   "dojo/_base/lang"
], function (lang) {

   return {
      getQuery: function (url) {
         var queryString = url.split('?')[1] ? url.split('?')[1] : '';
         var queryParameters = queryString.split('&');
         var queryHash = {};
         for (var i = 0; i < queryParameters.length; i++) {
            var keyValueArray = queryParameters[i].split('=');
            if (keyValueArray[1]) {
               queryHash[keyValueArray[0]] = keyValueArray[1];
            }
         }
         return queryHash;
      }
   }
});