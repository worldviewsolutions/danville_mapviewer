﻿console.log("wvs/common/Bookmark.js");

define(["dojo/_base/declare"
    , "dojo/_base/lang"
], function (declare, lang) {

    return declare([], {
        name: null,
        extent: null,
        mapState: null,
        children: null,

        /*@function constructor
        *       Mixes in any passed parameters, including, possibly, children.
        * @param params: Object
        *       Any object properties that need to be mixed in with the newly created instance.
        */
        constructor: function (params) {
            lang.mixin(this, params);
        },

        /*@function hasChildren
        *       Indicates whether or not the bookmark has child bookmarks (indicating a 'folder').
        * @return bool
        *       The truth value indicating whether or not the bookmark has children.
        */
        hasChildren: function () {
            return (this.children != null);
        },

        /*@function hasExtent
        *       Indicates whether or not the bookmark has an extent.
        * @return bool
        *       The truth value indicating whether the bookmark has children or not.
        */
        hasExtent: function () {
            return (this.extent == null);
        },

        /*@function toJson
        *       Returns a json representation of the object.
        * @return returnObj: Object
        *       The json representation of the object.
        */
        toJson: function () {
            var returnObj = {
                name: this.name,
                extent: this.extent,
                children: this.children
            };
            if (this.mapState) {
                returnObj.mapState = this.mapState;
            }
            return returnObj;
        }
    });

});