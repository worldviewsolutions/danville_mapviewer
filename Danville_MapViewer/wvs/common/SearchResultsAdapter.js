﻿console.log("wvs/common/SearchResultsAdapter.js");

define(["dojo/_base/declare"
    , "dojo/_base/lang"
    , "dojo/_base/array"
    , "dojo/Deferred"
    , "./SearchResult"
    , "./esriUtilities"
    , "esri/urlUtils"
    , "dojo/promise/all"],
    function (declare, lang, array, Deferred, SearchResult, esriUtilities, urlUtils, all) {
        // module: 
        //      wvs/common/SearchResultsAdapter
        // description:
        //      converts an identify or query task result to a SearchResult
        return declare([], {
            declaredClass: "wvs.common.SearchResultsAdapter",
            featureSetToResultSet: function (featureSet, url) {
                // summary:
                //      converts a feature set (result form QueryTask) to a SearchResult
                // featureSet: Object
                //      the feature set to convert
                // url: String
                //      the endpoint where the QueryTask was performed
                var self = this,
                    deferred = new Deferred(),
                    layerName = null,
                    utilities = new esriUtilities();

                if (featureSet.features.length) {
                    data = featureSet.features;


                    // We need to return a deferred since we have to make an AJAX call to get the layer name
                    // We resolve the outermost deferred once we have retrieved the layer name
                    var layerNameDeferred = utilities.getLayerInfoFromUrl(url).then(
                        function (layerInfo) {
                            var fields = featureSet.fields || layerInfo.fields,
                                i = j = 0;

                            // we will set up a temporary field 'map'
                            // this will allow us to only iterate over our result data once (and an inner loop to check the keys)
                            var fieldMap = {};

                            // we need to format our results accordingly. date fields, codedvaluedomains
                            // TODO: we DO NOT currently support Range domain values
                            for (i = 0; i < layerInfo.fields.length; i++) {
                                var field = layerInfo.fields[i];
                                fieldMap[field.name] = { domain: {}, type: field.type, alias: field.alias };
                                if (field.domain) {
                                    // CodedValueDomain
                                    if (field.domain.codedValues) {
                                        fieldMap[field.name].domain.codedValues = {};
                                        for (j = 0; j < field.domain.codedValues.length; j++) {
                                            fieldMap[field.name].domain.codedValues[field.domain.codedValues[j].code] = field.domain.codedValues[j].name;
                                        }
                                    }
                                }
                                if (field.alias != field.name) {
                                    fieldMap[field.alias] = fieldMap[field.name];
                                }
                            }

                            for (i = 0; i < data.length; i++) {
                                var result = data[i].attributes;
                                for (var key in result) {
                                    // map our coded values if they exist
                                    if (fieldMap[key].domain.codedValues) {
                                        result[key] = fieldMap[key].domain.codedValues[result[key]];
                                    }
                                        // format to shortdate if it's a date
                                    else if (fieldMap[key].type === "esriFieldTypeDate") {
                                        result[key] = (new Date(result[key])).toLocaleDateString()
                                    }
                                    //Turn all urls into anchor tags with an href pointing to the url.
                                    if (result[key].toString().indexOf("\\\\") == 0 || result[key].toString().indexOf("//") == 0 || result[key].toString().indexOf("http") == 0) {
                                        var linkUrl = result[key].toString();
                                        result[key] = "<a href=\"" + linkUrl + "\" target=\"blank\">" + result[key] + "</a>";
                                    }
                                    // replace the key with its alias
                                    if (fieldMap[key].alias !== key) {
                                        result[fieldMap[key].alias] = result[key];
                                        delete result[key];
                                    }
                                    //}

                                }
                            }
                            layerInfo.displayField = fieldMap[layerInfo.displayField].alias;
                            var searchResult = new SearchResult(url, [{ name: layerInfo.name, relationships: layerInfo.relationships, id: layerInfo.id, fields: fields, data: data, displayField: layerInfo.displayField, url: url }]);
                            deferred.resolve(searchResult);
                        },
                        function (error) {
                            throw new Error("error retrieving layer info from " + url + " in " + self.declaredClass);
                        }
                    );

                }
                setTimeout(function () {
                    deferred.resolve(new SearchResult(url, []));
                }, 1000);
                return deferred;
            },
            identifyToResultSet: function (identifyResults, url) {
                // summary:
                //      converts an array of IdentifyResult (result form IdentifyTask) to a SearchResult
                // identifyResults: IdentifyResult[]
                //      the identify results to convert
                // url: String
                //      the endpoint where the IdentifyTask was performed
                var deferred = new Deferred(),
                    deferreds = [],
                    searchResults = [],
                    utilities = new esriUtilities();
                if (identifyResults.length) {
                    // create grid-compatible object
                    var searchResults = [];

                    array.forEach(identifyResults, function (identifyResult) {
                        var layerId = identifyResult.layerId,
                            layerIdIndex = -1,
                            feature = identifyResult.feature;
                        for (var i = 0, il = searchResults.length; i < il; i++) {
                            if (searchResults[i].name === identifyResult.layerName) {
                                layerIdIndex = i;
                            }
                        }

                        // TODO: Only return fields that are specified by the optional definition expressions
                        //var hasGeometry = identifyResult.geometryType ? true : false
                        var dataRow = feature; //hasGeometry ? lang.mixin(feature.attributes, { resultGraphic: feature }) : feature.attributes;

                        if (layerIdIndex === -1) {
                            searchResults.push({ displayField: identifyResult.displayFieldName, name: identifyResult.layerName, id: identifyResult.layerId, fields: null, data: [dataRow] });
                            newLayerIdIndex = searchResults.length - 1;
                            // We need to return a deferred since we have to make an AJAX call to get the layer name
                            // We resolve the outermost deferred once we have retrieved the layer name
                            var callback = function (layerIndex, layerInfo) {
                                searchResults[layerIndex].fields = layerInfo.fields;
                                searchResults[layerIndex].relationships = layerInfo.relationships;
                                searchResults[layerIndex].url = url + "/" + layerInfo.id;
                            };
                            var layerFieldsDeferred = utilities.getLayerInfoFromUrl(url + "/" + layerId).then(
                                lang.partial(callback, newLayerIdIndex),
                                function (error) {
                                    throw new Error("error retrieving layer info from " + url + " in " + self.declaredClass);
                                }
                            );

                            deferreds.push(layerFieldsDeferred);
                        }
                        else {
                            searchResults[layerIdIndex].data.push(dataRow);
                        }


                    });

                    all(deferreds).then(function (results) {
                        var searchResult = new SearchResult(url, searchResults);
                        deferred.resolve(searchResult);
                    });

                    return deferred;
                }
                else {
                    setTimeout(function () {
                        deferred.resolve(new SearchResult(url, []));
                    }, 1000);
                    return deferred;
                }
            }
        });
    });